package edu.northwestern.websail.core.util;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class TimerUtil {

	private final ArrayList<Long> lapseTime;
	private long startTime;
	private long endTime;
	
	public TimerUtil() {
		lapseTime = new ArrayList<Long>();
	}
	
	public long start(){
		this.startTime = System.currentTimeMillis();
		return startTime;
	}
	
	public long end(){
		this.endTime = System.currentTimeMillis();
		return endTime;
	}
	
	public long lapse(){
		lapseTime.add(System.currentTimeMillis());
		return lapseTime.get(lapseTime.size()-1);
	}
	
	public void reset(){
		lapseTime.clear();
		startTime = System.currentTimeMillis();
		endTime = startTime;
	}

	public double totalSecond(){
		long difference = this.endTime - this.startTime;
		return TimeUnit.MILLISECONDS.toSeconds(difference);
	}

    public double totalMilliSecond() {
        long difference = this.endTime - this.startTime;
        return TimeUnit.MILLISECONDS.toMillis(difference);
    }

    public double second(){
		long difference = System.currentTimeMillis() - this.startTime;
		return TimeUnit.MILLISECONDS.toSeconds(difference);
	}
	
	
	public String totalTime(){
		long difference = this.endTime - this.startTime;
		return "time:"
				+ String.format(
						"%d min, %d sec",
						TimeUnit.MILLISECONDS.toMinutes(difference),
						TimeUnit.MILLISECONDS.toSeconds(difference)
								- TimeUnit.MINUTES
										.toSeconds(TimeUnit.MILLISECONDS
												.toMinutes(difference)));
	}
	
	public int lapseCount(){
		return this.lapseTime.size();
	}
	
	public String lapseTime(int l){
		long difference = this.lapseTime.get(l) - this.startTime;
		return "time:"
				+ String.format(
						"%d min, %d sec",
						TimeUnit.MILLISECONDS.toMinutes(difference),
						TimeUnit.MILLISECONDS.toSeconds(difference)
								- TimeUnit.MINUTES
										.toSeconds(TimeUnit.MILLISECONDS
												.toMinutes(difference)));
	}
	
}
