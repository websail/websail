package edu.northwestern.websail.core.pipeline.config;

import java.util.HashMap;

/**
 * @author NorThanapon
 * @since 11/21/14
 */
//XXX: Hack for resource loaders. We need to use annotation framework to dynamically populate this map
public class LoaderMap {

    public final static HashMap<PEVResourceType, String> map = new HashMap<PEVResourceType, String>();

    static {
        /* text */
        map.put(PEVResourceType.TOKENIZER,
                "edu.northwestern.websail.text.ploader.TokenizerPLoader.tokenizer");
        map.put(PEVResourceType.TOKENIZER_POOL,
                "edu.northwestern.websail.text.ploader.TokenizerPLoader.tokenizers");
        map.put(PEVResourceType.S_SENTENCE_SPLITER,
                "edu.northwestern.websail.text.ploader.TokenizerPLoader.sentenceSpliter");
        map.put(PEVResourceType.S_SENTENCE_SPLITER_POOL,
                "edu.northwestern.websail.text.ploader.TokenizerPLoader.sentenceSpliters");
        map.put(PEVResourceType.S_CHUNKER,
                "edu.northwestern.websail.text.ploader.TokenizerPLoader.chunker");
        map.put(PEVResourceType.S_CHUNKER_POOL,
                "edu.northwestern.websail.text.ploader.TokenizerPLoader.chunkers");
        map.put(PEVResourceType.NGRAM_COUNT,
                "edu.northwestern.websail.text.ploader.NGramPLoader.nGramCountMap");
        map.put(PEVResourceType.CORPUS_NGRAM_MODEL,
                "edu.northwestern.websail.text.ploader.NGramPLoader.loadNGramModel");
        map.put(PEVResourceType.CORPUS_NGRAM_COUNT,
                "edu.northwestern.websail.text.ploader.NGramPLoader.corpusNGramCountMap");
        map.put(PEVResourceType.WEB1T_NGRAM_COUNT,
                "edu.northwestern.websail.text.ploader.NGramPLoader.web1TNGramCountMap");
        map.put(PEVResourceType.TOPIC_DIST_MAP,
                "edu.northwestern.websail.text.ploader.CorpusDataPLoader.loadTopicDistMap");
        map.put(PEVResourceType.INDEX_READER,
                "edu.northwestern.websail.text.ploader.CorpusDataPLoader.loadIndexDR");
        map.put(PEVResourceType.INDEX_ID_MAP,
                "edu.northwestern.websail.text.ploader.CorpusDataPLoader.loadIndexDocIdMap");
        map.put(PEVResourceType.STC,
                "edu.northwestern.websail.text.ploader.STCLoader.loadSuffixTreeClusters");
        map.put(PEVResourceType.STC_CASE,
                "edu.northwestern.websail.text.ploader.STCLoader.loadSuffixTreeClustersCaseSensitive");
        /* KPE */
        map.put(PEVResourceType.KPE_DICT,
                "edu.northwestern.websail.kpe.data.DictPLoader.dictionary");
    }

}
