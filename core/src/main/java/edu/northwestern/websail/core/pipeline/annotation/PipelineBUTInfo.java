package edu.northwestern.websail.core.pipeline.annotation;

import edu.northwestern.websail.core.pipeline.buck.BuckUnitTypes;

/**
 * @author NorThanapon
 * @since 7/13/15
 */
public @interface PipelineBUTInfo {
    Class<? extends BuckUnitTypes.UnitTypeKey>[] consumes() default {};

    Class<? extends BuckUnitTypes.UnitTypeKey>[] produces() default {};
}
