/**
 * @author NorThanapon
 */
package edu.northwestern.websail.core.pipeline.buck;

import java.util.ArrayList;

public class TextBuck extends Buck {

    private BuckUnit unit;
    private String text;

    public TextBuck(String source, String text) {
        super(source);
        this.setText(text);
        unit = new BuckUnit();
        unit.set(BuckUnitTypes.TextBUT.class, this.text);
        unit.set(BuckUnitTypes.TextBeginOffsetBUT.class, 0);
        unit.set(BuckUnitTypes.TextEndOffsetBUT.class, this.text.length());
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        if(text == null) this.text = "";
    }

    @Override
    public Iterable<BuckUnit> units() {
        ArrayList<BuckUnit> units = new ArrayList<BuckUnit>();
        units.add(unit);
        return units;
    }

    @Override
    public BuckUnit unit() {
        return unit;
    }

    @Override
    public void addUnit(BuckUnit unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return this.unit.get(BuckUnitTypes.TextBUT.class);
    }

    @Override
    public void removeUnit(BuckUnit unit) {
        if (this.unit == unit) this.unit = null;
    }
}
