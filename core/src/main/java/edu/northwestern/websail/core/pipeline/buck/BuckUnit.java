/**
 * @author NorThanapon
 */
package edu.northwestern.websail.core.pipeline.buck;

import edu.northwestern.websail.core.pipeline.buck.BuckUnitTypes.UnitTypeKey;

import java.util.HashMap;
import java.util.Map.Entry;

public class BuckUnit {
    private final HashMap<Class<? extends UnitTypeKey<?>>, Object> values;
    private boolean isOkay = true;

    public BuckUnit() {
        this.values = new HashMap<Class<? extends UnitTypeKey<?>>, Object>();
    }

    @SuppressWarnings("unchecked")
    public <TYPE> TYPE get(Class<? extends UnitTypeKey<TYPE>> type) {
        Object value = values.get(type);
        return (TYPE) value;
    }

    public <TYPE> TYPE set(Class<? extends UnitTypeKey<TYPE>> type, TYPE value) {
        TYPE oldValue = this.get(type);
        this.values.put(type, value);
        return oldValue;
    }

    public <TYPE> boolean has(Class<? extends UnitTypeKey<TYPE>> type) {
        return values.containsKey(type);
    }

    public boolean isNotOkay() {
        return !isOkay;
    }

    public void setOkay(boolean isOkay) {
        this.isOkay = isOkay;
    }

    public BuckUnit shallowClone() {
        BuckUnit newUnit = new BuckUnit();
        for (Entry<Class<? extends UnitTypeKey<?>>, Object> p : values.entrySet()) {
            newUnit.values.put(p.getKey(), p.getValue());
        }
        return newUnit;
    }

}
