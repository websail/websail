package edu.northwestern.websail.core.pipeline;

import edu.northwestern.websail.core.pipeline.arch.SubPipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.config.PEVResourceLoader;
import edu.northwestern.websail.core.pipeline.config.PipelineConfig;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.core.pipeline.config.PipelineUnit;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;
import java.util.Properties;

/**
 * @author csbhagav on 4/13/14.
 */
public class SubPipelineExecutor implements SubPipelineExecutable {

    Properties config;
    private int threadId;

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {

        try {
            String subPipelineConfigFile = config.getProperty("subPipelineConfig");
            PipelineConfig subPipeConfig = PipelineConfig
                    .getInstance(subPipelineConfigFile);
            PipelineEnvironment subPipeEnv = new PipelineEnvironment(subPipeConfig);
            subPipeEnv.initGlobal();
            PEVResourceLoader.populateSubpipelineResources(env, subPipeEnv);

            PipelineUnit[] units = subPipeConfig.getPipeline();

            for (PipelineUnit unit : units) {
                subPipeEnv.initLocal(unit, false);
                SubPipelineExecutable plExec = (SubPipelineExecutable) Class.forName(
                        unit.getClassName()).newInstance();
                if (unit.getUnitConfig() != null) {
                    plExec.init(subPipeEnv, unit.getUnitConfig());
                }
                plExec.setThreadId(this.threadId);
                plExec.execute(subPipeEnv, prev);
                subPipeEnv.destroyResources(false);
                plExec.close(subPipeEnv, unit.getUnitConfig());
            }

        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return false;
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        this.config = config;
        return true;
    }

    @Override
    public int getThreadId() {
        return 0;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        return true;
    }
}
