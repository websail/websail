package edu.northwestern.websail.core.pipeline.unit.par;

import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.config.PEVResourceLoader;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.*;
import java.util.logging.Logger;

public class ParallelPLManager implements Closeable {

    public static final Logger logger = Logger.getLogger(ParallelPLManager.class
            .getName());
    public static final String LEADER_LATCH = "_common_._leader_latch_";
    public static final String ALL_LATCH_1 = "_common_._all_latch_1_";
    public static final String ALL_LATCH_2 = "_common_._all_latch_2_";
    protected final PipelineEnvironment env;
    protected int numThreads = 4;
    protected ArrayBlockingQueue<Runnable> queue;
    protected ThreadPoolExecutor executor;
    protected ArrayList<Future<Boolean>> futures;
    public ParallelPLManager(PipelineEnvironment environment) {
        this.env = environment;
        this.numThreads = PEVResourceLoader.getPoolSize(this.env.getPipelineConfig().getGlobalConfig(), "par.numThreads");
        logger.info("Running each pipeline unit with " + numThreads
                + " threads in parallel");
        this.initThreadPool();
    }

    public static void countDownLatch(PipelineEnvironment env, String key) {
        if (env.getPipelineGlobalOuputs().containsKey(key)) {
            CountDownLatch cl = (CountDownLatch) env.getPipelineGlobalOuputs().get(key);
            cl.countDown();
        }
    }

    public static void awaitLatch(PipelineEnvironment env, String key) {
        if (env.getPipelineGlobalOuputs().containsKey(key)) {
            CountDownLatch cl = (CountDownLatch) env.getPipelineGlobalOuputs().get(key);
            try {
                cl.await();
            } catch (InterruptedException e) {
                logger.severe("Latch await interrupted!");
                e.printStackTrace();
            }
        }
    }

    private void initThreadPool() {
        queue = new ArrayBlockingQueue<Runnable>(numThreads * 3);
        executor = new ThreadPoolExecutor(numThreads, numThreads, 5L,
                TimeUnit.MINUTES, queue,
                new ThreadPoolExecutor.CallerRunsPolicy());
    }

    public <T extends Buck> boolean run(List<T> bucks,
                                        Class<? extends PipelineExecutable> type, Properties config)
            throws InstantiationException, IllegalAccessException {
        ArrayList<ParallelPLUnitWorker> workers;
        try {
            workers = partition(bucks, type, config);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        ArrayList<Future<Boolean>> localFutures = new ArrayList<Future<Boolean>>();
        env.getPipelineGlobalOuputs().put(LEADER_LATCH, new CountDownLatch(1));
        env.getPipelineGlobalOuputs().put(ALL_LATCH_1, new CountDownLatch(this.numThreads));
        env.getPipelineGlobalOuputs().put(ALL_LATCH_2, new CountDownLatch(this.numThreads));
        for (ParallelPLUnitWorker worker : workers) {
            localFutures.add(executor.submit(worker));
        }
        boolean error = false;
        boolean oneSuccess = false;
        for (Future<Boolean> future : localFutures) {
            try {
                oneSuccess = future.get() || oneSuccess;
            } catch (ExecutionException e) {
                logger.severe("A PipelineExecutable throws an exception!. This should not happen.");
                e.printStackTrace();
                error = true;
            } catch (InterruptedException e) {
                logger.severe("Main thread was interrupted");
                e.printStackTrace();
                error = true;
            }
        }
        this.futures = localFutures;
        env.getPipelineGlobalOuputs().remove(LEADER_LATCH);
        env.getPipelineGlobalOuputs().remove(ALL_LATCH_1);
        env.getPipelineGlobalOuputs().remove(ALL_LATCH_2);
        workers.clear();
        return !error && oneSuccess;

    }

    protected <T extends Buck> ArrayList<ParallelPLUnitWorker> partition(
            List<T> bucks, Class<? extends PipelineExecutable> type,
            Properties config) throws Exception {
        ArrayList<ParallelPLUnitWorker> parts = new ArrayList<ParallelPLUnitWorker>();
        int totalCount = bucks.size();
        int each = totalCount / this.numThreads;
        int rest = totalCount % this.numThreads;
        int current = 0;
        for (int i = 0; i < this.numThreads; i++) {
            PipelineExecutable unit = type.newInstance();
            unit.setThreadId(i);
            int len = each;
            if (rest > 0)
                len++;
            List<T> subBucks = bucks.subList(current, current + len);
            current += len;
            rest--;
            parts.add(new ParallelPLUnitWorker(env, unit, config, subBucks));
        }
        return parts;
    }

    public int getNumThreads() {
        return numThreads;
    }

    public void shutdown() throws InterruptedException {
        this.executor.shutdown();
        this.executor.awaitTermination(5L, TimeUnit.MINUTES);
    }

    @Override
    public void close() throws IOException {
        try {
            shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new IOException(e.getMessage());
        }
    }
}
