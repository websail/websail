package edu.northwestern.websail.core.io;


import java.io.*;
import java.util.*;
import java.util.logging.Logger;

/**
 * User: csbhagav
 * Date: 9/27/13
 */
public class FileReadUtils {

    static final Logger log = Logger.getLogger(FileReadUtils.class.getName());

    public static Set<String> getFileLinesAsSet(String fileName) throws IOException {
        return getFileLinesAsSet(fileName, false);
    }

    public static Set<String> getFileLinesAsSet(String fileName, boolean caseInsensitive) throws IOException {

        log.info("Reading file:" + fileName);
        BufferedReader in = new BufferedReader(new FileReader(new File(fileName)));
        HashSet<String> itemSet = new HashSet<String>();
        String line;
        while ((line = in.readLine()) != null) {
            if (caseInsensitive)
                itemSet.add(line.toLowerCase());
            else{
                itemSet.add(line);
            }
        }

        in.close();
        log.info("*** DONE ***");
        return itemSet;
    }
    
    public static List<String> getFileLinesAsList(String fileName) throws IOException {

        BufferedReader in = new BufferedReader(new FileReader(new File(fileName)));
        List<String> itemSet = new ArrayList<String>();
        String line;
        while ((line = in.readLine()) != null) {
            itemSet.add(line);
        }

        in.close();
        return itemSet;
    }

    public static HashMap<String, Integer> getMapStringToInt(String fileName) throws IOException {
        log.info("Reading file:" + fileName);
        BufferedReader in = new BufferedReader(new InputStreamReader(
                new FileInputStream(fileName), "UTF8"));
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        String line;
        while ((line = in.readLine()) != null) {
            String[] parts = line.split("\t");
            map.put(parts[0], Integer.valueOf(parts[1]));
        }

        in.close();
        log.info("*** DONE ***");
        return map;
    }

    public static HashMap<Integer, String> getMapIntToString(String idToTitleMap) throws IOException {
        log.info("Reading file:" + idToTitleMap);
        BufferedReader in = new BufferedReader(new InputStreamReader(
                new FileInputStream(idToTitleMap), "UTF8"));
        HashMap<Integer, String> map = new HashMap<Integer, String>();
        String line;
        while ((line = in.readLine()) != null) {
            String[] parts = line.split("\t");
            map.put(Integer.valueOf(parts[0]), parts[1]);
        }
        in.close();
        log.info("*** DONE ***");
        return map;
    }

    public static HashSet<String> subtractFilesByFields(String file1, String file2, Integer[] file1Fields,
                                                        Integer[] file2Fields) throws IOException {

        BufferedReader in = new BufferedReader(new FileReader(new File(file1)));

        HashSet<String> keysFile1 = new HashSet<String>();
        String line;
        while ((line = in.readLine()) != null) {
            String[] parts = line.split("\t");
            StringBuilder keyB = new StringBuilder();
            for (Integer file1Field : file1Fields) {
                keyB.append(parts[file1Field]);
                keyB.append("-");
            }
            keyB.deleteCharAt(keyB.length() - 1);

            String key = keyB.toString();
            keysFile1.add(key);
        }

        in.close();

        in = new BufferedReader(new FileReader(new File(file2)));
        while ((line = in.readLine()) != null) {
            String[] parts = line.split("\t");
            StringBuilder keyB = new StringBuilder();
            for (Integer file2Field : file2Fields) {
                keyB.append(parts[file2Field]);
                keyB.append("-");
            }
            keyB.deleteCharAt(keyB.length() - 1);

            String key = keyB.toString();
            keysFile1.remove(key);
        }
        in.close();
        return keysFile1;
    }

}
