package edu.northwestern.websail.core.pipeline.annotation;

import edu.northwestern.websail.core.pipeline.config.PEVResourceType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author NorThanapon
 * @since 11/7/14
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PipelineConfigInfo {
    PEVResourceType[] requiredResources() default {};

    PEVResourceType[] optionalResources() default {};

    String[] requiredSettings() default {};

    String[] optionalSettings() default {};

}
