package edu.northwestern.websail.core.pipeline.arch;

import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;

/**
 * @author csbhagav on 4/13/14.
 */
@PipelineConfigInfo()
public interface SubPipelineExecutable extends PipelineExecutable {
}
