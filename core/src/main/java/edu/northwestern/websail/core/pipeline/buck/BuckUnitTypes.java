/**
 * @author NorThanapon
 */
package edu.northwestern.websail.core.pipeline.buck;


@SuppressWarnings("unchecked")
public class BuckUnitTypes {
    public interface UnitTypeKey<TYPE> {
        Class<TYPE> getType();
    }

    // ==================================================================//
    // BASIC INFO
    // ==================================================================//

    public static class TextBUT implements UnitTypeKey<String> {
        public Class<String> getType() {
            return String.class;
        }
    }

    public static class TextBeginOffsetBUT implements UnitTypeKey<Integer> {
        @Override
        public Class<Integer> getType() {
            return Integer.class;
        }
    }

    public static class TextEndOffsetBUT implements UnitTypeKey<Integer> {
        @Override
        public Class<Integer> getType() {
            return Integer.class;
        }
    }
}
