package edu.northwestern.websail.core.pipeline.unit.par;

import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

public class ParallelPLUnitWorker implements Callable<Boolean> {
    public static final Logger logger = Logger.getLogger(ParallelPLUnitWorker.class.getName());
    protected final PipelineExecutable unit;
    protected final List<? extends Buck> bucks;
    protected final PipelineEnvironment env;
    protected final Properties config;

    public ParallelPLUnitWorker(PipelineEnvironment env,
                                PipelineExecutable unit, Properties config,
                                List<? extends Buck> bucks) {
        this.unit = unit;
        this.env = env;
        this.bucks = bucks;
        this.config = config;
    }

    @Override
    public Boolean call() {
        if (!unit.init(env, config)) {
            logger.severe(
                    "Cannot initialize every pipeline unit. Please check log file for missing resources and configuration.");
            return false;
        }
        Boolean oneExeSuccess = false;
        for (Buck b : bucks) {
            oneExeSuccess = unit.execute(env, b) || oneExeSuccess;
        }
        if (!oneExeSuccess) logger.severe("This unit failed to execute all of the bucks.");
        if (!unit.close(env, config)) {
            logger.severe("Fail to close this unit.");
            return false;
        }
        return oneExeSuccess;
    }
}
