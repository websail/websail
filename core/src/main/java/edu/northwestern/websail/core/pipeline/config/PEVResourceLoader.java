package edu.northwestern.websail.core.pipeline.config;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 */

public class PEVResourceLoader {
    public static final int WORDINDEXER_SIZE = 132158174;
    public static final Logger logger = Logger
            .getLogger(PEVResourceLoader.class.getName());
    public static final HashMap<PEVResourceType, Class> loaderMap = new HashMap<PEVResourceType, Class>();
    // --------------------------------------------------------------------------------
    // Common methods
    // --------------------------------------------------------------------------------

    public static void populateSubpipelineResources(PipelineEnvironment env,
                                                    PipelineEnvironment subpipeEnv) {
        subpipeEnv.getGlobalResources().putAll(env.getGlobalResources());
        subpipeEnv.getGlobalResources().putAll(env.getLocalResources());
    }

    public static Object loadResource(PipelineEnvironment env,
                                      PEVResourceType type, Properties config, boolean isGlobal)
            throws Exception {
        logger.info("RESOURCE\tLoading: " + type.toString() + " ; isGlobal : "
                + isGlobal);
        String methodRef = LoaderMap.map.get(type);
        String classRef = methodRef.substring(0, methodRef.lastIndexOf('.'));
        String methodName = methodRef.substring(methodRef.lastIndexOf('.') + 1);
        Method method = Class.forName(classRef).getMethod(methodName, PipelineEnvironment.class, Properties.class, Boolean.class, Boolean.class);
        return method.invoke(null, env, config, isGlobal, true);
    }

    public static Object setResource(PipelineEnvironment env,
                                      PEVResourceType type, boolean isGlobal, Object resource) {
        if (isGlobal)
            return env.setGlobalReource(type, resource);
        else
            return env.setLocalReource(type, resource);
    }

    // --------------------------------------------------------------------------------
    // Utils
    // --------------------------------------------------------------------------------

    public static int getPoolSize(Properties config, String key) {
        if (config.containsKey(key)) {
            return Integer.parseInt(config.getProperty(key));
        } else if (config.containsKey("common.numThreads")) {
            return Integer.parseInt(config.getProperty("common.numThreads"));
        } else {
            return 1;
        }
    }

}
