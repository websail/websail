package edu.northwestern.websail.core.pipeline.unit.common;

import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;

import java.util.Properties;

/**
 * @author NorThanapon
 * @since 11/20/14
 */
@PipelineConfigInfo()
public class NOPUnit implements PipelineExecutable {
    int threadId = 0;

    public NOPUnit() {
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public int getThreadId() {
        return threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }
}
