/**
 * @author NorThanapon
 */
package edu.northwestern.websail.core.pipeline.buck;


public abstract class Buck {
    private String id;

    public Buck(String source) {
        this.id = source;
    }

    public String getSource() {
        return id;
    }

    public void setSource(String source) {
        this.id = source;
    }

    public abstract Iterable<BuckUnit> units();

    public abstract BuckUnit unit();

    public abstract void addUnit(BuckUnit unit);

    public abstract void removeUnit(BuckUnit unit);
}
