package edu.northwestern.websail.core.pipeline;

import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.config.*;
import edu.northwestern.websail.core.pipeline.unit.par.ParallelPLManager;
import edu.northwestern.websail.core.util.TimerUtil;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 *         <p/>
 *         Run user-defined pipeline workers
 */
public class PipelineExecutor implements Closeable {
    public static final Logger logger = Logger.getLogger(PipelineExecutor.class
            .getName());
    public static final String CUR_NUM_THREADS = "_current_._num_threads_";
    protected PipelineConfig pipelineConfig;
    protected PipelineEnvironment env;
    protected PipelineUnit[] plUnits;
    protected PipelineUnit currentUnit;
    protected boolean inheritResources = false;

    public PipelineExecutor(String pipelineConfigFile) throws Exception {
        TimerUtil timer = new TimerUtil();
        timer.start();
        logger.info("Initializing environment.");
        pipelineConfig = PipelineConfig.getInstance(pipelineConfigFile);
        env = new PipelineEnvironment(pipelineConfig);
        env.initGlobal();
        plUnits = pipelineConfig.getPipeline();
        timer.end();
        logger.info("Finish initialization\nTime: " + timer.totalTime());
    }

    public PipelineExecutor(String pipelineConfigFile,
                            PipelineExecutor sharedResourceWikifier) throws Exception {
        TimerUtil timer = new TimerUtil();
        timer.start();
        logger.info("Initializing environment.");
        pipelineConfig = PipelineConfig.getInstance(pipelineConfigFile);
        env = new PipelineEnvironment(pipelineConfig);
        if (sharedResourceWikifier != null) {
            this.env.getGlobalResources().putAll(
                    sharedResourceWikifier.env.getGlobalResources());
            this.env.getPipelineGlobalOuputs().putAll(
                    sharedResourceWikifier.env.getPipelineGlobalOuputs());

            ArrayList<PEVResourceType> globalResourceList = pipelineConfig
                    .getGlobalResourceReq();
            for (PEVResourceType type : globalResourceList) {
                this.env.getGlobalResources().remove(type);
            }
        }
        env.initGlobal();
        plUnits = pipelineConfig.getPipeline();
        timer.end();
        logger.info("Finish initialization\nTime: " + timer.totalTime());
        this.inheritResources = true;
    }

    public Buck run(Buck b) throws Exception {
        return this.run(b, 0);
    }

    public Buck run(Buck b, int threadId) throws Exception {
        TimerUtil timer = new TimerUtil();
        timer.start();
        this.setCurrentNumThreads(1);
        for (PipelineUnit unit : plUnits) {
            logger.info("Start Unit: " + unit.getName());
            this.currentUnit = unit;
            for (PEVResourceType rType : unit
                    .getPreRequisiteResources()) {
                PEVResourceLoader.loadResource(env, rType,
                        unit.getUnitConfig(), false);
            }
            PipelineExecutable plExec = (PipelineExecutable) Class.forName(
                    unit.getClassName()).newInstance();
            if (threadId != -1)
                plExec.setThreadId(threadId);
            boolean initSuccess = plExec.init(env, unit.getUnitConfig());
            if (!initSuccess) {
                throw new Exception("Could not initialize "
                        + unit.getClassName());
            }
            plExec.execute(env, b);
            plExec.close(env, unit.getUnitConfig());
            logger.info("Destroy Unit: " + unit.getName());
            this.env.destroyResources(false);
        }
        timer.end();
        logger.info("Finish running pipeline\nTotal time: " + timer.totalTime());
        return b;
    }

    public <T extends Buck> List<T> run(List<T> bucks) throws Exception {
        return this.run(bucks, -1);
    }

    public <T extends Buck> List<T> run(List<T> bucks, int threadId)
            throws Exception {
        TimerUtil timer = new TimerUtil();
        timer.start();
        this.setCurrentNumThreads(1);
        for (PipelineUnit unit : plUnits) {
            logger.info("Start Unit: " + unit.getName());
            this.currentUnit = unit;
            for (PEVResourceType rType : unit
                    .getPreRequisiteResources()) {
                PEVResourceLoader.loadResource(env, rType,
                        unit.getUnitConfig(), false);
            }
            PipelineExecutable plExec = (PipelineExecutable) Class.forName(
                    unit.getClassName()).newInstance();
            if (threadId != -1)
                plExec.setThreadId(threadId);
            boolean initSuccess = plExec.init(env, unit.getUnitConfig());
            if (!initSuccess) {
                throw new Exception("Could not initialize "
                        + unit.getClassName());
            }
            for (Buck buck : bucks) {
                plExec.execute(env, buck);
            }
            plExec.close(env, unit.getUnitConfig());
            logger.info("Destroy Unit: " + unit.getName());
            this.env.destroyResources(false);

            if (this.env.getPipelineGlobalOuputs().containsKey("breakPipeline")
                    && (Boolean) env.getPipelineGlobalOuputs().get(
                    "breakPipeline")) {
                logger.info("Found Break in PipelineUnit");
                break;
            }
        }
        timer.end();
        logger.info("Finish running pipeline\nTotal time: " + timer.totalTime());
        return bucks;
    }

    public <T extends Buck> List<T> parRun(List<T> bucks) throws Exception {
        TimerUtil timer = new TimerUtil();
        timer.start();
        ParallelPLManager parallelPLManager = new ParallelPLManager(env);
        this.setCurrentNumThreads(parallelPLManager.getNumThreads());
        int count = 0;
        for (PipelineUnit unit : plUnits) {
            logger.info("Running pipeline: " + unit.getName());
            try {
                PipelineExecutable executable = (PipelineExecutable) Class
                        .forName(unit.getClassName()).newInstance();
                env.initLocal(unit, false);
                boolean success = parallelPLManager.run(bucks,
                        executable.getClass(), unit.getUnitConfig());
                if (success)
                    count++;
            } finally {
                this.env.destroyResources(false);
            }
        }
        parallelPLManager.close();
        timer.end();
        if (count != plUnits.length)
            logger.warning("Only " + count
                    + " thread(s) successfully ran this pipeline unit.");
        logger.info("Finish running pipeline\nTotal time: " + timer.totalTime());
        return bucks;
    }

    public PipelineConfig getPipelineConfig() {
        return pipelineConfig;
    }

    public PipelineEnvironment getEnv() {
        return env;
    }

    public PipelineUnit[] getPlUnits() {
        return plUnits;
    }

    @Override
    public void close() throws IOException {
        try {
            env.destroyResources(true, !this.inheritResources);
        } catch (Exception e) {
            e.printStackTrace();
            throw new IOException(e.getMessage());
        }
    }

    private void setCurrentNumThreads(int nt) {
        env.getPipelineGlobalOuputs().put(CUR_NUM_THREADS, nt);
    }


}
