package edu.northwestern.websail.core.pipeline.config;

import java.io.Closeable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

public class PipelineEnvironment {

    public static final ArrayList<PEVResourceType> connectionResources = new ArrayList<PEVResourceType>();

    static {
        connectionResources.add(PEVResourceType.TRIE_DB_CONN);
        connectionResources.add(PEVResourceType.TRIE_DB_CONN_POOL);
        connectionResources.add(PEVResourceType.MONGO_CLIENT);
    }

    protected final HashMap<String, Object> pipelineGlobalOuputs = new HashMap<String, Object>();
    protected PipelineConfig pipelineConfig;
    protected HashMap<PEVResourceType, Object> globalResources = new HashMap<PEVResourceType, Object>();
    protected HashMap<PEVResourceType, Object> localResources = new HashMap<PEVResourceType, Object>();

    public PipelineEnvironment(PipelineConfig pipelineConfig) {
        this.pipelineConfig = pipelineConfig;
    }

    public static void logResourceNotFound(PEVResourceType type, Logger logger) {
        logger.severe("Cannot find "
                + type
                + " in the environment");
    }

    public PipelineConfig getPipelineConfig() {
        return pipelineConfig;
    }

    public void setPipelineConfig(PipelineConfig pipelineConfig) {
        this.pipelineConfig = pipelineConfig;
    }

    public Object getResource(PEVResourceType resourcesType) {
        Object resource = this.localResources.get(resourcesType);
        if (resource != null)
            return resource;
        return this.globalResources.get(resourcesType);
    }

    public Object getPoolResource(PEVResourceType poolType, int index,
                                  PEVResourceType secondary) {

        Object[] pool = (Object[]) this.getResource(poolType);
        if (pool == null) {
            //System.out.println("Request: " + poolType + " at threadId: " + index + " but pool is null.");
            return this.getResource(secondary);
        } else {
            //System.out.println("Request: " + poolType + " at threadId: " + index);
            return pool[index];
        }
    }

    public Object setGlobalReource(PEVResourceType resourcesType,
                                   Object resource) {
        Object oldResource = this.globalResources.get(resourcesType);
        this.globalResources.put(resourcesType, resource);
        return oldResource;
    }

    public Object setLocalReource(PEVResourceType resourcesType,
                                  Object resource) {
        Object oldResource = this.localResources.get(resourcesType);
        this.localResources.put(resourcesType, resource);
        return oldResource;
    }

    public HashMap<PEVResourceType, Object> clearLocalResources() {
        HashMap<PEVResourceType, Object> local = this.localResources;
        this.localResources = new HashMap<PEVResourceType, Object>();
        return local;
    }

    public void destroyResources(boolean isGlobal)
            throws Exception {
        this.destroyResources(isGlobal, true);
    }

    public void destroyResources(boolean isGlobal, boolean closeConnection)
            throws Exception {
        HashMap<PEVResourceType, Object> resources = this.localResources;
        if (isGlobal)
            resources = this.globalResources;
        if (closeConnection) {
            for (PEVResourceType type : connectionResources) {
                Object resource = resources.get(type);
                if (resource == null)
                    continue;
                if (type == PEVResourceType.TRIE_DB_CONN_POOL) {
                    Connection[] cs = (Connection[]) resource;
                    for (Connection c : cs) {
                        c.close();
                    }
                    continue;
                }
                if (type == PEVResourceType.TRIE_DB_CONN) {
                    Connection c = (Connection) resource;
                    c.close();
                }
            }
        }
        for (Object resource : resources.values()) {
            if (resource == null)
                continue;
            if (resource instanceof java.io.Closeable) {
                ((Closeable) resource).close();
            }
        }
        if (isGlobal) {
            this.globalResources.clear();
            this.localResources.clear();
        } else {
            this.localResources.clear();
        }

    }

    public HashMap<PEVResourceType, Object> getGlobalResources() {
        return globalResources;
    }

    public void setGlobalResources(
            HashMap<PEVResourceType, Object> globalResources) {
        this.globalResources = globalResources;
    }

    public HashMap<PEVResourceType, Object> getLocalResources() {
        return localResources;
    }

    public void setLocalResources(
            HashMap<PEVResourceType, Object> localResources) {
        this.localResources = localResources;
    }

    public ArrayList<PEVResourceType> getCloseableResources() {
        return connectionResources;
    }

    public HashMap<String, Object> getPipelineGlobalOuputs() {
        return pipelineGlobalOuputs;
    }

    public void initGlobal() throws Exception {

        ArrayList<PEVResourceType> globalResources = pipelineConfig
                .getGlobalResourceReq();
        for (PEVResourceType resourcesType : globalResources) {
//			if (this.globalResources.containsKey(resourcesType))
//				continue;
            PEVResourceLoader.loadResource(this, resourcesType,
                    this.pipelineConfig.getGlobalConfig(), true);
        }
    }

    public void initLocal(PipelineUnit unit,
                          Boolean removeExistingLocalResources) throws Exception {
        if (removeExistingLocalResources)
            this.destroyResources(false);
        ArrayList<PEVResourceType> resources = unit
                .getPreRequisiteResources();
        for (PEVResourceType resourcesType : resources) {
            PEVResourceLoader.loadResource(this, resourcesType,
                    unit.getUnitConfig(), false);
        }
    }
}