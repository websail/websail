package edu.northwestern.websail.core.pipeline.config;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * @author csbhagav on 3/20/14.
 */
@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public PipelineConfig createCustomer() {
        return new PipelineConfig();
    }

}
