package edu.northwestern.websail.core.io;

import java.io.*;

public class InputFileManager implements Closeable {

    public final Boolean isLowerCase = false;
    public BufferedReader in = null;

    public InputFileManager(String fileName) {
        try {
            InputStreamReader reader = new InputStreamReader(new FileInputStream(
                    fileName), "UTF-8");
            in = new BufferedReader(reader);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String readFileText(String fileName) throws IOException {
        File file = new File(fileName);
        InputStreamReader reader = new InputStreamReader(new FileInputStream(
                file));
        int character;
        StringBuilder output = new StringBuilder();
        while ((character = reader.read()) != -1) {
            output.append((char) character);
        }
        reader.close();
        return output.toString();
    }

    public static String readFileText(String fileName, String encoding)
            throws IOException {
        File file = new File(fileName);
        InputStreamReader reader = new InputStreamReader(new FileInputStream(
                file), encoding);
        int character;
        StringBuilder output = new StringBuilder();
        while ((character = reader.read()) != -1) {
            output.append((char) character);
        }
        reader.close();
        return output.toString();
    }

    public static boolean isExist(String directory) {
        File file = new File(directory);
        return file.exists();
    }

    public static Object deserializeObjectFromFile(String fileName)
            throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(
                new FileInputStream(fileName)));
        Object object = ois.readObject();
        ois.close();
        return object;
    }

    public static Object deserializeObjectFromFile(ObjectInputStream ois)
            throws IOException, ClassNotFoundException {
        return ois.readObject();
    }

    public String readLine() {
        try {
            String line = in.readLine();
            if (line == null)
                return null;
            if (isLowerCase)
                return line.toLowerCase().trim();
            return line;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void close() {
        try {
            this.in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
