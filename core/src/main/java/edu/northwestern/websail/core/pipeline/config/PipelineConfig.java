package edu.northwestern.websail.core.pipeline.config;

import org.jetbrains.annotations.NotNull;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author csbhagav on 3/20/14.
 */
@XmlRootElement
public class PipelineConfig {
    public static final Logger logger = Logger.getLogger(PipelineConfig.class.getName());
    Properties globalConfig;
    String globalConfigFile;
    ArrayList<PEVResourceType> globalResourceReq = new ArrayList<PEVResourceType>();
    PipelineUnit[] pipeline;

    public PipelineConfig(PipelineUnit[] pipeline) {
        this.pipeline = pipeline;
    }

    public PipelineConfig() {

    }

    public static PipelineConfig getInstance(String configFile) throws JAXBException, FileNotFoundException {
        JAXBContext jc = JAXBContext.newInstance(PipelineConfig.class.getPackage().getName());
        Unmarshaller u = jc.createUnmarshaller();
        logger.info("Load configuration from " + configFile);
        PipelineConfig plConfig = (PipelineConfig) u.unmarshal(new FileInputStream(configFile));
        plConfig.setGlobalResourceReq(dedupReqResources(plConfig.getGlobalResourceReq()));
        for (PipelineUnit unit : plConfig.getPipeline()) {
            unit.setPreRequisiteResources(dedupReqResources(unit.getPreRequisiteResources()));
        }
        inheritGlobalConfigToUnitConfigs(plConfig);
        return plConfig;
    }

    private static void inheritGlobalConfigToUnitConfigs(PipelineConfig plConfig) {
        for (PipelineUnit unit : plConfig.getPipeline()) {
            if (plConfig.getGlobalConfig() != null)
                unit.merge(plConfig.getGlobalConfig());
        }
    }

    private static ArrayList<PEVResourceType> dedupReqResources(ArrayList<PEVResourceType> req) {
        ArrayList<PEVResourceType> dReq = new ArrayList<PEVResourceType>();
        HashSet<PEVResourceType> reqSet = new HashSet<PEVResourceType>();
        for (PEVResourceType r : req) {
            if (reqSet.contains(r)) {
                continue;
            }
            reqSet.add(r);
            dReq.add(r);
        }
        return dReq;
    }

    public static String switchKey(Properties config, @NotNull String key) {
        String value = config.getProperty(key, "");
        if (value.startsWith("key:")) {
            String configKey = value.substring(4);
            logger.fine("'key:' detected, now switch to " + configKey);
            return config.getProperty(configKey, "");
        } else {
            return config.getProperty(key, "");
        }
    }

    public PipelineUnit[] getPipeline() {
        return pipeline;
    }

    public void setPipeline(PipelineUnit[] pipeline) {
        this.pipeline = pipeline;
    }

    public Properties getGlobalConfig() {
        return globalConfig;
    }

    public void setGlobalConfig(Properties globalConfig) {
        this.globalConfig = globalConfig;
    }

    public String getGlobalConfigFile() {
        return globalConfigFile;
    }

    public void setGlobalConfigFile(String globalConfigFile) throws IOException {
        this.globalConfigFile = globalConfigFile;
        initConfig(this.globalConfigFile);
    }

    public void initConfig(String configFile) throws IOException {
        globalConfig = new Properties();
        globalConfig.load(new FileInputStream(configFile));
    }

    public ArrayList<PEVResourceType> getGlobalResourceReq() {
        return globalResourceReq;
    }

    public void setGlobalResourceReq(ArrayList<PEVResourceType> globalResourceReq) {
        this.globalResourceReq = globalResourceReq;
    }
}

