package edu.northwestern.websail.core.pipeline.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Set;

/**
 * @author csbhagav on 3/20/14.
 */
public class PipelineUnit {
    String name;
    String className;
    String configFile;
    Properties unitConfig;
    ArrayList<PEVResourceType> preRequisiteResources = new ArrayList<PEVResourceType>();

    public PipelineUnit() {
    }

    public PipelineUnit(String name, String className) {
        this.name = name;
        this.className = className;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getConfigFile() {
        return configFile;
    }

    public void setConfigFile(String configFile) throws IOException {
        this.configFile = configFile;
        initConfig(this.configFile);
    }

    public Properties getUnitConfig() {
        return unitConfig;
    }

    public void setUnitConfig(Properties unitConfig) {
        this.unitConfig = unitConfig;
    }

    public ArrayList<PEVResourceType> getPreRequisiteResources() {
        return preRequisiteResources;
    }

    public void setPreRequisiteResources(ArrayList<PEVResourceType> preRequisiteResources) {
        this.preRequisiteResources = preRequisiteResources;
    }

    public void initConfig(String configFile) throws IOException {
        unitConfig = new Properties();
        unitConfig.load(new FileInputStream(configFile));
    }

    public void merge(Properties globalConfig) {
        Properties mergedConfig = new Properties();

        Set<Object> keys = globalConfig.keySet();
        for (Object key : keys) {
            mergedConfig.put(key, globalConfig.get(key));
        }
        if (this.unitConfig != null) {
            keys = this.unitConfig.keySet();
            for (Object key : keys) {
                mergedConfig.put(key, this.unitConfig.get(key));
            }
        }
        this.setUnitConfig(mergedConfig);
    }
}
