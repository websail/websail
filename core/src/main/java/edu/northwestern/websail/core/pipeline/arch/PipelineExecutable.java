package edu.northwestern.websail.core.pipeline.arch;


import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;

import java.util.Properties;

@PipelineConfigInfo()
public interface PipelineExecutable {
    boolean init(PipelineEnvironment env, Properties config);

    boolean execute(PipelineEnvironment env, Buck prev);

    boolean close(PipelineEnvironment env, Properties config);

    int getThreadId();

    void setThreadId(int id);
}
