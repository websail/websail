package edu.northwestern.websail.core.pipeline.unit.common;

import edu.northwestern.websail.core.io.OutputFileManager;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.core.pipeline.unit.par.ParallelPLManager;

import java.io.*;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 * @since 11/20/14
 */
public abstract class WriteAndConcatFileUnit implements PipelineExecutable {

    public static final Logger logger = Logger.getLogger(WriteAndConcatFileUnit.class.getName());
    private static final Object lock = new Object();

    protected int threadId = 0;
    private OutputFileManager localOut;
    private String filename;
    private String localFilename;
    public WriteAndConcatFileUnit() {
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        filename = config.getProperty(outFileConfigKey(), "websail_output");

        localFilename = "temp." + filename + getThreadIdStr();
        try {
            if (threadId == 0) {
                synchronized (lock) {
                    boolean result = OutputFileManager.createFile(filename);
                    if (!result) {
                        logger.warning(filename + " exists! The file will be overwritten.");
                        File f = new File(filename);
                        boolean deleted = f.delete();
                        if (!deleted) {
                            logger.warning(filename + " cannot be overwritten. Output will append this file.");
                        }
                    }
                    ParallelPLManager.countDownLatch(env, ParallelPLManager.LEADER_LATCH);
                }
            }
            boolean result = OutputFileManager.createFile(localFilename);
            if (!result) {
                logger.warning(localFilename + " exists! The file will be overwritten.");
                File f = new File(localFilename);
                boolean deleted = f.delete();
                if (!deleted) {
                    logger.warning(localFilename + " cannot be overwritten. Output will append this file.");
                }
            }
            localOut = new OutputFileManager(localFilename);
        } catch (IOException e) {
            logger.severe("Cannot create output file.");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        return write(env, prev, localOut);
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        localOut.close();
        ParallelPLManager.awaitLatch(env, ParallelPLManager.LEADER_LATCH);
        synchronized (lock) {
            try {
                OutputStream out = new BufferedOutputStream(new FileOutputStream(filename, true));
                InputStream in = new BufferedInputStream(new FileInputStream(localFilename));
                int b;
                byte[] buf = new byte[1 << 20];
                while ((b = in.read(buf)) >= 0) {
                    out.write(buf, 0, b);
                }
                in.close();
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        File file = new File(localFilename);
        file.deleteOnExit();
        return true;
    }

    @Override
    public int getThreadId() {
        return this.threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }

    public String getThreadIdStr() {
        return "@tId:" + this.threadId;
    }

    protected abstract String outFileConfigKey();

    protected abstract boolean write(PipelineEnvironment env, Buck prev, OutputFileManager out);

}
