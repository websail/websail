# Wikipedia Semantic Relatedness
This project helps generating Wikipedia semantic relatedness meassures and also provide web interface.

## Dependencies
* Java
* Maven
* Tomcat apache (to run web services)

## Prerequisites
* Wikipedia SQL dump in MySQL (use these [scripts](https://bitbucket.org/websail/wikipediasqldumputil))
* Parsed Wikipedia in MongoDB (use this [project](https://bitbucket.org/websail/websail/src/77d5d32ffdaf8ff794a8e71356c309591f71ced9/wikiparser/?at=develop))

## How to build SR
1. Package and optionally upload to the running machine
    1. Go to `websail` directory (parent of this project)
    2. Run `mvn clean package -am -pl wikisr`
        * This will generate a couple jar files in `wikiparser/target` directory
        * We need to use `websail-wikisr-0.0.1-jar-with-dependencies.jar`
    3. Use `scp` to upload the jar file
        * On Unix: `scp wikisr/target/websail-wikisr-0.0.1-jar-with-dependencies.jar <username>@<host>:`
        * On Windows: use [WinSCP](http://winscp.net/eng/download.php)
        * Preferably a server that host MySQL with Wikipedia SQL dump.
2. Generate redirect map
    * Run `java -XX:+UseG1GC -Xmx40G -cp <path-to-jar> edu.northwestern.websail.wikisr.link.GenRedirectMapFile <out redirect file> <mysql user> <mysql pwd> <mysql host> <wiki dump db>`
3. Generate link file
    * Run `java -XX:+UseG1GC -Xmx40G -cp <path-to-jar> edu.northwestern.websail.wikisr.link.GenLinkFile <redirect file> <mongo host> <mongo port> <mongo db> <mongo user> <mongo pwd> <out link file> <num threads> false`
    * `<redirect file>` from the previous step
    * Mongo DB default port is 27017
4. Precompute SR
    * Run `java -XX:+UseG1GC -Xmx40G -cp <path-to-jar> edu.northwestern.websail.wikisr.PrecomputeSR <link file> <out directory>`
    * `<link file>` from step 3
5. Build SR random access file
    * Run `java -XX:+UseG1GC -Xmx40G -cp <path-to-jar> edu.northwestern.websail.wikisr.SemanticRelatedness <SR directory> <out directory>`
    * `<SR directory>` from step 4
    * This will not create a new random access file if the file is already existed.

## How to deploy SR services
1. Change SR directory in code
    * `edu.northwestern.websail.wikisr.ws.SRLoader`, line 26-32
    * `edu.northwestern.websail.wikisr.ws.SemanticRelatednessService`, line 97-103
2. Package and rename war file
    1. Go to `websail` directory (parent of this project)
    2. Run `mvn clean package -am -pl wikisr`
    3. Rename the war file `mv wikisr/target/websail-wikisr-0.0.1.war wikisr/target/wwsr.war`
3. Deploy to any Apache Tomcat server

## How to change SR version
1. Make sure there is a directory of the version i.e. `v1`
2. Use a version service
    * `http://path-to-service/wwsr/sr/version/<version>`