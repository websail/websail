package edu.northwestern.websail.wikisr.util;

//converts arrays of positive integers into and out of a variable-byte (compressed) format

//note: uses -1 as a field delimiter in the int array, and 0x0000000001 as a five-byte delimiter in the byte array

public class VariableByteInteger {

    public static final byte[] _delim = {0, 0, 0, 0, 1};

    //extraBuffSpace allows you to create a larger output array, for later storage of other stuff
    public static byte[] encode(int[] in, int extraBuffSpace) {
        //compute the size:
        int len = 0;
        for (int anIn : in) {
            if (anIn == -1)
                len += 5; //note, actually the same result as below but explicit here for clarity
            else
                len += 1 + ((31 - Integer.numberOfLeadingZeros(anIn)) / 7);
        }
        len += extraBuffSpace;
        //encode:
        byte[] out = new byte[len];
        int idx = 0;
        for (int anIn : in) {
            int t = anIn;
            if (t == -1) {
                for (int j = 0; j < 4; j++) {
                    out[idx++] = (byte) 0x00;
                }
                out[idx++] = (byte) 0x01;
            } else {
                while (t > 127) {
                    out[idx++] = (byte) (t % 128);
                    t = t >> 7;
                }
                out[idx++] = (byte) (0x80 | (t % 128));
            }
        }
        return out;

    }

    public static byte[] encode(int[] in) {
        return encode(in, 0);
    }

    //only decode numBytesToDecode bytes from array
    public static int[] decode(byte[] in, int numBytesToDecode) {
        //compute the size:
        int len = 0;
        for (int i = 0; i < numBytesToDecode; i++) {
            if (in[i] < 0) {//continuation bit set to 1 = end of int
                len++;
            } else if (in[i] == (byte) 0x00) {
                boolean field = true;
                for (int j = 1; j < 4; j++) {
                    if (i + j >= numBytesToDecode || in[i + j] != (byte) 0x00) {
                        field = false;
                        break;
                    }
                }
                if (field) {
                    if (i + 4 < numBytesToDecode && in[i + 4] == (byte) 0x01) { //there's a field starting at i
                        i += 4;
                        len++;
                    }
                }
            }
        }
        //decode:
        int[] out = new int[len];
        int idx = 0;
        int shifter = 0;
        int current = 0;
        for (int i = 0; i < numBytesToDecode; i++) {
            if (in[i] < 0) {
                out[idx] = current + ((in[i] & 0x7F) << shifter);
                shifter = 0;
                idx++;
                current = 0;
            } else if (in[i] == (byte) 0x00) {
                boolean field = true;
                for (int j = 1; j < 4; j++) {
                    if (i + j >= numBytesToDecode || in[i + j] != (byte) 0x00) {
                        field = false;
                        break;
                    }
                }
                if (field) {
                    if (i + 4 < numBytesToDecode && in[i + 4] == (byte) 0x01) { //there's a field starting at i
                        i += 4;
                        out[idx++] = -1;
                        if (current != 0 || shifter != 0)
                            System.err.println("Warning: Decoding error!");
                    }
                } else {
                    current += ((in[i] & 0x7F) << shifter);
                    shifter += 7;
                }
            } else {
                current += ((in[i] & 0x7F) << shifter);
                shifter += 7;
            }
        }
        return out;

    }

    public static int[] decode(byte[] in) {
        return decode(in, in.length);
    }

    public static void main(String[] args) throws Exception {
        int[] input = new int[50000000];
        for (int i = 0; i < 50000000; i++) {
            if (i % 7 == 0)
                input[i] = i;
            else if (i % 10 == 0)
                input[i] = -1;
            else
                input[i] = 25;
        }
        input[0] = 640;
//		int [] input = new int[] {1, 4, 6, 119, 19798, 82, 376, 26177};
//		System.out.println(Arrays.toString(input));
        long start = System.currentTimeMillis();
        byte[] encoded = encode(input);
        //System.out.println(Arrays.toString(encoded));
        int[] decoded = decode(encoded);
        long end = System.currentTimeMillis();
//		System.out.println(Arrays.toString(decoded));
        for (int i = 0; i < input.length; i++) {
            if (input[i] != decoded[i]) {
                System.out.println("Error");
                System.exit(-1);
            }
        }

        System.out.println("Success, compressed is " + encoded.length + " bytes vs. original of " + decoded.length * 4 + " bytes.");
        System.out.println("Time elapsed (ms): " + (end - start));
    }
}