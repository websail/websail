package edu.northwestern.websail.wikisr.ws;

import edu.northwestern.websail.wikisr.SemanticRelatedness;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;
import java.io.PrintStream;

/**
 * Copied from old project
 */
public class SRLoader implements ServletContextListener {

    public void contextDestroyed(ServletContextEvent sce) {
        ServletContext context = sce.getServletContext();
        PrintStream out = (PrintStream) context.getAttribute(SemanticRelatednessService.srLogOut);
        if (out != null) {
            out.close();
        }
    }

    public void contextInitialized(ServletContextEvent sce) {

        ServletContext context = sce.getServletContext();

        try {

            String version = "v5";
            SemanticRelatedness sr = new SemanticRelatedness(new File(
                    "/disk2/SR/" + version +
                            "/mssr2.dat").getAbsolutePath(), new File(
                    "/disk2/SR/" + version +
                            "/mwsrQuantiles.txt").getAbsolutePath(),
                    "/disk2/SR/" + version +
                            "/raf",
                    "/disk2/SR/" + version + "/spatialIds.txt");

            context.setAttribute("srObj", sr);
            context.setAttribute("srVersion", version);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}