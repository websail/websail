package edu.northwestern.websail.wikisr.adapter;


import edu.northwestern.websail.wikisr.SemanticRelatedness;

import java.io.IOException;
import java.util.HashMap;

public class RAFWikiSRAdapter extends WikiSRAdapter {

    private RAFSR srRaf;
    private String dir = "/home/websail/common/sr/sr-testSkippedPlusEasyLinks/";
    private String milneWittenFile = "mwsr.dat";
    private String milneWittenQuantilesFile = "mwsrQuantiles.txt";
    private String rafDirectory = "raf";
    private int poolSize;
    public RAFWikiSRAdapter() {
        this(1);
    }

    public RAFWikiSRAdapter(int poolSize) {
        super();
        this.poolSize = poolSize;
    }
    
    public RAFWikiSRAdapter(String directory){
    	this(directory, 1);
    }
    
    public RAFWikiSRAdapter(String directory, int poolSize){
    	super();
    	if(directory.charAt(directory.length()-1)!='/')
    		directory = directory + "/";
    	this.dir = directory;
    	this.poolSize = poolSize;
    }
    
    public RAFWikiSRAdapter(String milneWittenFile,
                            String milneWittenQuantilesFile,
                            String rafDirectory) throws Exception {
        super();
        this.milneWittenFile = milneWittenFile;
        this.milneWittenQuantilesFile = milneWittenQuantilesFile;
        this.rafDirectory = rafDirectory;
    }

    private RAFSR getSrRaf(String milneWittenFile,
                                 String milneWittenQuantilesFile,
                                 String rafDirectory, int poolSize) throws Exception {
        return new RAFSR(milneWittenFile, milneWittenQuantilesFile, rafDirectory, poolSize);
    }

    @Override
    public HashMap<Integer, Double> getSRs(int langId, int pageId)
            throws IOException {
        HashMap<Integer, Double> result = new HashMap<Integer, Double>();
        try {
            SemanticRelatedness.Postings p = srRaf.getSR(pageId, langId);
            if (p == null || p.ids == null) {
                logger.warning("No SR for pageId: " + pageId);
            } else {
                for (int i = 0; i < p.ids.length; i++) {
                    result.put(p.ids[i], p.rels[i]);
                }
            }
        } catch (Exception e) {
            logger.severe("Error while retrieving SR for pageId: " + pageId);
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public SemanticRelatedness.Postings getSRPostings(int langId, int pageId) throws IOException {
        try {
            return srRaf.getSR(pageId, langId);
        } catch (Exception e) {
            logger.severe("Error while retrieving SR for pageId: " + pageId);
            e.printStackTrace();
        }
    	return null;
	}
    
    @Override
    public void close() throws IOException {
        srRaf.close();
    }

    public void setSrRaf(RAFSR srRaf) {
        this.srRaf = srRaf;
    }

    @Override
    public void init() {
        try {
            this.srRaf = getSrRaf(dir+milneWittenFile, dir+milneWittenQuantilesFile, dir+rafDirectory, this.poolSize);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
