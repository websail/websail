package edu.northwestern.websail.wikisr.util;

import edu.northwestern.websail.wikisr.SemanticRelatedness;

import java.util.Arrays;

/**
 * @author NorThanapon
 */

public class OptimizedSRCompressor extends SRCompressor {

	@Override
	public SemanticRelatedness.Postings unCompressSRs(CompressedSRs in) {
		SemanticRelatedness.Postings out = new SemanticRelatedness.Postings();
		out.rels = new double[in.numSRs];
		out.ids = VariableByteInteger.decode(in.data, in.data.length
                - in.numSRs);
        for (int i = 1; i < out.ids.length; i++)
            out.ids[i] += out.ids[i - 1];
		for (int i = 0; i < out.rels.length; i++) {
			int j = i + (in.data.length - out.ids.length);
			out.rels[i] = unquantize(in.data[j]);
		}
		return out;
	}

	public SemanticRelatedness.Postings unCompressSRs(CompressedSRs in,
			int[] targets) {

		int[] ids = VariableByteInteger.decode(in.data, in.data.length
				- in.numSRs);
		int[] indexes = new int[targets.length];
		int[] foundIds = new int[targets.length];
		int iTarget = 0;
		int foundCount = 0;
		for (int i = 1; i < ids.length; i++) {
			if (ids[i - 1] == targets[iTarget]) {
				foundIds[foundCount] = ids[i - 1];
				indexes[foundCount] = i - 1;
				foundCount++;
				iTarget++;
				if (iTarget >= targets.length)
					break;
			} else if (ids[i - 1] > targets[iTarget]) {
				iTarget++;
				if (iTarget >= targets.length)
					break;
			}
			ids[i] += ids[i - 1];
		}

		SemanticRelatedness.Postings out = new SemanticRelatedness.Postings();
		if (foundCount == 0) {
			out.ids = new int[1];
			out.rels = new double[1];
		}
		out.ids = Arrays.copyOfRange(foundIds, 0, foundCount);
		out.rels = new double[foundCount];
		for (int i = 0; i < foundCount; i++) {
			int j = indexes[i] + (in.data.length - ids.length);
			out.rels[i] = unquantize(in.data[j]);
		}
		return out;
	}
}
