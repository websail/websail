package edu.northwestern.websail.wikisr;

import edu.northwestern.websail.wikisr.util.SRCompressor;
import gnu.trove.iterator.TIntIterator;
import gnu.trove.iterator.TLongIterator;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.set.hash.TIntHashSet;
import gnu.trove.set.hash.TLongHashSet;

import java.io.*;
import java.util.*;

public class PrecomputeSR {
    private static final double _C = 118.4617066;
    private static final double _D = 4261.034367;
    private static final double _E = 21.70568873;
    private static final double _F = 218.3160739;
    private static final double _G = 5.142456232;
    private static final double _H = 5.942309594;
    private static final double _I = 0.861530211;
    private static final double _J = 1.286304311;
    private static final double _K = 173.0366482;
    private static final double _L = 267.7121074;
    private static final double _M = 0.01702185;
    private static final double _N = -0.001984029;
    final SRCompressor _src = new SRCompressor();
    HashMap<Long, TIntHashSet> _outLinks; //keys always lang adjusted and gloss adjusted (id times -1 if in gloss)
    HashMap<Long, TIntHashSet> _inLinks; //values never lang adjusted or gloss adjusted
    TLongHashSet _uniqueConcepts; //lang adjusted and positive, only pages with at least one inlink
    //private static final double _O = 1.0;

    //W is estimate of num pages
    public PrecomputeSR(String inFile, int numSamples, int W) throws Exception {
        readLinksDump(inFile, W);
        initCompressor(numSamples, this._uniqueConcepts.size());
    }

    private static double zeroLog(double d) {
        if (d <= 0)
            return 0;
        else
            return Math.log(d);
    }

    public static double mw(int ag, int ang, int agnbg, int agnbng, int angnbg, int angnbng, int bg, int bng, int W) {
        if (ag + ang > bg + bng) {
            return (_C * zeroLog(_D * ag + _E * ang) - _F * zeroLog(_G * agnbg + _H * agnbng + _I * angnbg + _J * angnbng)) /
                    (_K * zeroLog(W) - _L * zeroLog(_M * bg + _N * bng));
        } else
            return (_C * zeroLog(_D * bg + _E * bng) - _F * zeroLog(_G * agnbg + _H * angnbg + _I * agnbng + _J * angnbng)) /
                    (_K * zeroLog(W) - _L * zeroLog(_M * ag + _N * ang));
    }

    private static void incrementMap(TIntIntHashMap hm, int key, int inc) {
        if (!hm.containsKey(key)) {
            hm.put(key, inc);
        } else {
            hm.put(key, hm.get(key) + inc);
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.out.println("Usage: SemanticRelatednessPrecomputer <links file> <outDirectory>");
            return;
        }
        PrecomputeSR srp =
                new PrecomputeSR(args[0], 1500000, 3000000);
        System.out.println("outlinks has size " + srp._outLinks.size());
        System.out.println("inlinks has size " + srp._inLinks.size());
//		System.out.println(System.currentTimeMillis());
//		SemanticRelatedness.Postings p = srp.getRelatednessPostings(12L + (long) Integer.MAX_VALUE, 4000000);
//		System.out.println(Arrays.toString(p.ids));
//		System.out.println(Arrays.toString(p.rels));
//		SRCompressor.CompressedSRs csrs = srp._src.compressSRs(p);
//		p = srp._src.unCompressSRs(csrs);
//		System.out.println(Arrays.toString(p.ids));
//		System.out.println(Arrays.toString(p.rels));

//		if(true)
//			return;
        //HACK: the file stuff is to handle windows and unix:
        srp.computeAllRelatednesses(new File(args[1], "mwsr.dat").getAbsolutePath());

        srp._src.writeQuantilesToFile(new File(args[1], "mwsrQuantiles.txt").getAbsolutePath());
//		srp.writeRelatedness(55040L + (long)Integer.MAX_VALUE, bwOut, 4000000);
//		System.out.println(System.currentTimeMillis());
//		srp.writeRelatedness(624L + (long)Integer.MAX_VALUE, bwOut, 4000000);
        System.out.println(System.currentTimeMillis());
        //System.out.println(s);
    }

    private void addToMap(long a, int b, HashMap<Long, TIntHashSet> hm) {
        TIntHashSet ts = hm.get(a);
        if (ts == null) {
            ts = new TIntHashSet();
            hm.put(a, ts);
        }
        ts.add(b);
    }

    private void addLink(int from, int to, byte lang, boolean gloss) {
        long langAdj = ((long) lang * Integer.MAX_VALUE);
        long glossAdj = gloss ? -1 : 1;
        long fromID = ((long) from + langAdj) * glossAdj;

        long toID = ((long) to + langAdj) * glossAdj;

        addToMap(fromID, to, _outLinks);
        addToMap(toID, from, _inLinks);
        this._uniqueConcepts.add(toID * glossAdj); //always positive
    }

    //inits the inlinks and outlinks maps using info in file
    public void readLinksDump(String inFile, int W) throws Exception {
        BufferedReader brIn = new BufferedReader(new InputStreamReader(new FileInputStream(inFile), "UTF8"));
        String sLine;
        _outLinks = new HashMap<Long, TIntHashSet>(W);
        _inLinks = new HashMap<Long, TIntHashSet>(W);
        _uniqueConcepts = new TLongHashSet(W);
        int lines = 0;
        //lines are idFrom\tidTo\tlanguage\tis_gloss (0 | 1)
        while ((sLine = brIn.readLine()) != null) {
            String[] fields = sLine.split("\t");
            int idFrom = Integer.parseInt(fields[0]);
            int idTo = Integer.parseInt(fields[1]);
            byte lang = Byte.parseByte(fields[2]);
            boolean gloss = (Integer.parseInt(fields[3]) == 1);
            addLink(idFrom, idTo, lang, gloss);
//			if(++lines == 50000000) {
//				System.out.print(lines + "\r\n");
//				break;
//			}
            if (++lines % 100000 == 0)
                System.out.print(lines + "..");
            if (lines % 1000000 == 0)
                System.out.print("\r\n");
        }
        brIn.close();
    }

    private void incrementForAll(TIntIntHashMap hm, TIntHashSet s, TreeSet<Integer> ts) {
        TIntIterator itJ = s.iterator();
        while (itJ.hasNext()) {
            int j = itJ.next();
            incrementMap(hm, j, 1);
            ts.add(Math.abs(j));
        }

    }

    //increments hash maps with in-link co-occurrences with conceptID
    //also adds new co-occurring concepts to ts
    private void incrementMapsForConceptID(TIntIntHashMap hmGloss, TIntIntHashMap hmNotGloss,
                                           TreeSet<Integer> ts, long conceptID) {
        TIntHashSet froms = _inLinks.get(conceptID);
        if (froms == null)
            return;
        long langAdj = Integer.MAX_VALUE * (Math.abs(conceptID) / Integer.MAX_VALUE);

        TIntIterator itI = froms.iterator();
        while (itI.hasNext()) {
            int i = itI.next();
            //sum links from i, not in gloss:
            long id = langAdj + (long) Math.abs(i);
            if (_outLinks.get(id) != null) {
                incrementForAll(hmNotGloss, _outLinks.get(id), ts);
            }
            //sum links from i, in gloss:
            id = id * -1L;
            if (_outLinks.get(id) != null) {
                incrementForAll(hmGloss, _outLinks.get(id), ts);
            }
        }
    }

    //returns each concept with shared inlinks between itself and fromConcept, with # of such inlinks
    //fills up passed-in HashMaps (assumed to be empty)
    //crazy hashmap syntax handles glosses.
    //candidates are all positive.
    private TreeSet<Integer> getCandidates(long conceptA, TIntIntHashMap inAinB, TIntIntHashMap inAoutB,
                                           TIntIntHashMap outAinB, TIntIntHashMap outAoutB) throws Exception {
        if (conceptA < 0)
            throw new Exception("Don't ask for gloss ID candidates.");
        TreeSet<Integer> out = new TreeSet<Integer>();
        incrementMapsForConceptID(outAinB, outAoutB, out, conceptA);
        incrementMapsForConceptID(inAinB, inAoutB, out, -1L * conceptA);
        return out;
    }

    @SuppressWarnings("ConstantConditions")
    public SemanticRelatedness.Postings getRelatednessPostings(long fromConcept, int W,
                                                               boolean top10000) throws Exception {
        ///TODO: make this step more space efficient?
        // specific ideas:
        //    use only one hash table with four numbers (four hash hits -> one hash hit)
        //    avoid size computation for all b's by storing that size within the outlinks structure and returning with cands

        TIntIntHashMap inAinB = new TIntIntHashMap();
        TIntIntHashMap inAoutB = new TIntIntHashMap();
        TIntIntHashMap outAinB = new TIntIntHashMap();
        TIntIntHashMap outAoutB = new TIntIntHashMap();
        TreeSet<Integer> cands = getCandidates(fromConcept, inAinB, inAoutB, outAinB, outAoutB);

        int ang = 0;
        if (this._inLinks.get(fromConcept) != null)
            ang = this._inLinks.get(fromConcept).size();

        int ag = 0;
        if (this._inLinks.get(fromConcept * -1L) != null)
            ag = this._inLinks.get(fromConcept * -1L).size();
        if (ag + ang == 0)
            return null;
        if (fromConcept == (88671L + (long) Integer.MAX_VALUE)) {
            System.out.println("88671 has ag=" + ag + " and ang=" + ang);
        }
        if (fromConcept == (88680L + (long) Integer.MAX_VALUE)) {
            System.out.println("88680 has ag=" + ag + " and ang=" + ang);
        }
        long langAdj = Integer.MAX_VALUE * (fromConcept / Integer.MAX_VALUE);
        SemanticRelatedness.Postings p = new SemanticRelatedness.Postings();
        p.ids = new int[cands.size()];
        p.rels = new double[cands.size()];
        if (cands.size() == 0)
            System.out.println("here.");
//		StringBuffer sb = new StringBuffer();
//		sb.append(fromConcept);
//		System.out.println("cands has size " + cands.size());
        int idx = 0;
        for (int i : cands) {
            int agnbg = inAinB.containsKey(i) ? inAinB.get(i) : 0;
            int agnbng = inAoutB.containsKey(i) ? inAoutB.get(i) : 0;
            int angnbg = outAinB.containsKey(i) ? outAinB.get(i) : 0;
            int angnbng = outAoutB.containsKey(i) ? outAoutB.get(i) : 0;
            long idNotGloss = (long) i + langAdj;

            int bng = (_inLinks.get(idNotGloss) != null) ? _inLinks.get(idNotGloss).size() : 0;
            int bg = (this._inLinks.get(idNotGloss * -1L) != null) ? this._inLinks.get(idNotGloss * -1L).size() : 0;

            double sr = 1.0 - mw(ag, ang, agnbg, agnbng, angnbg, angnbng, bg, bng, W);
            if (sr < 0.0)
                sr = 0.0;
            p.ids[idx] = i;
            p.rels[idx++] = sr;
//			sb.append(",");
//			sb.append(i);
//			sb.append(",");
//			sb.append(sr);

        }
        if (p.rels.length > 10000 && top10000) {
            System.out.println("Pruning to at most 10000: " + p.rels.length);
            double[] rels2 = Arrays.copyOf(p.rels, p.rels.length);
            Arrays.sort(rels2);
            double thresh = rels2[rels2.length - 10000];
            ArrayList<Integer> alIds = new ArrayList<Integer>();
            ArrayList<Double> alRels = new ArrayList<Double>();
            for (int i = 0; i < p.rels.length; i++) {
                if (p.rels[i] > thresh) {
                    alIds.add(p.ids[i]);
                    alRels.add(p.rels[i]);
                }
            }
            p.rels = new double[alRels.size()];
            p.ids = new int[alRels.size()];
            for (int i = 0; i < p.rels.length; i++) {
                p.rels[i] = alRels.get(i);
                p.ids[i] = alIds.get(i);
            }
        }
        return p;
    }

    //appends all non-zero relatednesses for fromConcept to bwOut
    public void writeRelatedness(long fromConcept, DataOutputStream dosOut, int W) throws Exception {
        if (fromConcept < 0)
            throw new Exception("Don't ask for gloss ID candidates.");
        SemanticRelatedness.Postings p = getRelatednessPostings(fromConcept, W, false);
        dosOut.writeLong(fromConcept);
        SRCompressor.CompressedSRs csrs = this._src.compressSRs(p);
        SRCompressor.writeCompressedSRsToFile(dosOut, csrs);
    }

    public void computeAllRelatednesses(String outFile) throws Exception {
        DataOutputStream dosOut = new DataOutputStream(new FileOutputStream(outFile));
        //BufferedWriter bwOut = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "UTF8"));
        int computations = 0;
        //dosOut.writeInt(10000);

        dosOut.writeInt(this._uniqueConcepts.size());

        //need to process relatedness in sorted order:
        //TODO: make more space/time efficient (using unsorted hashmap b/c treemaps too large)
        long[] concepts = new long[this._uniqueConcepts.size()];
        TLongIterator it = _uniqueConcepts.iterator();
        int idx = 0;
        while (it.hasNext())
            concepts[idx++] = it.next();
        Arrays.sort(concepts);
        for (idx = 0; idx < concepts.length; idx++) {
            long i = concepts[idx];
            if (i < 0)
                continue;
            writeRelatedness(i, dosOut, this._uniqueConcepts.size());
            if (++computations % 100 == 0) {
                System.out.print(computations + "\r\n");
            }
//			if(computations % 10000 ==0)
//				break;
        }
        System.out.println("computed " + computations + " srs.");
        dosOut.close();
    }

    //assumes inLinks already created
    public void initCompressor(int numSamples, int W) throws Exception {
        double[] srs = new double[numSamples];
        int idx = 0;
        Random r = new Random(4);
        if (_inLinks.size() == 0)
            return;
        Iterator<Long> it = _inLinks.keySet().iterator();
        while (idx < srs.length) {
            int skip = r.nextInt(100);
            for (int i = 0; i < skip; i++) {
                if (!it.hasNext()) // loop back around
                    it = _inLinks.keySet().iterator();
                it.next();
            }
            if (!it.hasNext()) // loop back around
                it = _inLinks.keySet().iterator();
            long concept = it.next();
            if (concept < 0)
                continue;
            SemanticRelatedness.Postings p = getRelatednessPostings(concept, W, false);
            for (int i = 0; i < p.rels.length && idx < srs.length; i++) {
                srs[idx++] = p.rels[i];
            }
        }
        this._src.setQuantiles(srs);
    }
}
