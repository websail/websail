package edu.northwestern.websail.wikisr.ws;

import edu.northwestern.websail.core.util.TimerUtil;
import edu.northwestern.websail.wikisr.SemanticRelatedness;
import edu.northwestern.websail.wikisr.util.SRComparators;

import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Chandra, NorThanapon, Xiaofeng
 */
@Path("/sr/")
public class SemanticRelatednessService {

    public static final String srLogOut = "SR_LOG_OUT";
    private static final DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final DateFormat fullDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private static final Object contextLock = new Object();
    //Changed here!
    @Context
    ServletContext context;
    //Changed here!

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/q")
    public HashMap<Integer, Double> getSRMapWithQueryString(
            @QueryParam("sID") Integer sID,
            @QueryParam("langID") Integer langID,
            @DefaultValue("false") @QueryParam("spatial") Boolean spatial,
            @DefaultValue("-1") @QueryParam("top") Integer top) {
        TimerUtil timer = new TimerUtil();
        timer.start();
        SemanticRelatedness srObj = (SemanticRelatedness) context
                .getAttribute("srObj");
        HashMap<Integer, Double> result = new HashMap<Integer, Double>();

        try {
            SemanticRelatedness.Postings p = srObj.getSR(sID, langID, spatial);
            if (top > 0) {
                Integer[] orders = new Integer[p.ids.length];
                for (int i = 0; i < p.ids.length; i++) {
                    orders[i] = i;
                }
                SRComparators.ArrayIndexComparator comparator = new SRComparators.ArrayIndexComparator(p.rels);
                Arrays.sort(orders, comparator);
                for (int i = 0; i < orders.length; i++) {
                    if (i > top) break;
                    int idx = orders[i];
                    result.put(p.ids[idx], p.rels[idx]);
                }
            } else {
                for (int i = 0; i < p.ids.length; i++) {
                    result.put(p.ids[i], p.rels[i]);
                }
            }
            timer.end();
            log("sID=" + sID +
                    "&langID=" + langID +
                    "&spatial=" + spatial +
                    "&top=" + top +
                    ", Total time: " + timer.totalMilliSecond() + " millis");
        } catch (Exception e) {
            System.out.println("Failed to get SR Map for sid = " + sID
                    + " / langId = " + langID);
            e.printStackTrace();
        }

        return result;

    }

    private void log(String msg) throws FileNotFoundException {
        Calendar cal = Calendar.getInstance();
        int today = cal.get(Calendar.DAY_OF_MONTH);
        Integer logDay = (Integer) context.getAttribute("SR_LOG_DAY");
        boolean newLog = logDay == null || today != logDay;
        if(newLog) {
            synchronized (contextLock) {
                PrintStream out = new PrintStream(
                        new FileOutputStream("/disk2/SR/ws_logs/wwsr-" + simpleDateFormat.format(cal.getTime())
                                + ".log", true));
                context.setAttribute("SR_LOG_DAY", today);
                context.setAttribute(srLogOut, out);
            }
        }
        PrintStream out = (PrintStream) context.getAttribute(srLogOut);
        out.println(fullDateFormat.format(cal.getTime()) + ":" + msg);
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/version/")
    public String getVersion() {

        String result = (String) context.getAttribute("srVersion");
        if (result.equalsIgnoreCase("v1")) {
            result += ": Original Version. Built on old data";
        } else if (result.equalsIgnoreCase("v2")) {
            result += ": Latest Version. Built on all links extracted from Wikipedia - Feb '14";
        }
        return result;
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/version/{version}")
    public String setVersion(@PathParam("version") String version) {

        SemanticRelatedness sr;
        try {
            sr = new SemanticRelatedness(new File(
                    "/disk2/SR/" + version +
                            "/mwsr.dat").getAbsolutePath(), new File(
                    "/disk2/SR/" + version +
                            "/mwsrQuantiles.txt").getAbsolutePath(),
                    "/disk2/SR/" + version +
                            "/raf",
                    "/disk2/SR/" + version + "/spatialIds.txt");
            context.setAttribute("srObj", sr);
            context.setAttribute("srVersion", version);
            return "SUCCESS : Set SR to new version";
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "FAILED TO SET NEW VERSION";
    }

    /*====================================================================================*/
    /*  BELOW IS LEGACY CODE FOR BACKWARD COMPATABILITY                                   */
    /*====================================================================================*/


    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/sID/{sID}/langID/{langID}")
    public HashMap<String, ArrayList<Posting>> getSRPostings(
            @PathParam("sID") Integer sID, @PathParam("langID") Integer langID) {

        SemanticRelatedness srObj = (SemanticRelatedness) context
                .getAttribute("srObj");
        HashMap<String, ArrayList<Posting>> result = new HashMap<String, ArrayList<Posting>>();
        ArrayList<Posting> pMap = new ArrayList<Posting>();
        result.put("result", pMap);

        try {
            SemanticRelatedness.Postings p = srObj.getSR(sID, langID);

            for (int i = 0; i < p.ids.length; i++) {
                pMap.add(new Posting(p.ids[i], p.rels[i]));
            }

        } catch (Exception e) {
            System.out.println("Failed to get SR for sid = " + sID
                    + " / langId = " + langID);
            e.printStackTrace();
        }

        result.put("result", pMap);

        return result;

    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/sID/{sID}/langID/{langID}/spatial/{spatial}")
    public HashMap<String, ArrayList<Posting>> getSRPostings(
            @PathParam("sID") Integer sID, @PathParam("langID") Integer langID, @PathParam("spatial") Boolean spatial) {

        SemanticRelatedness srObj = (SemanticRelatedness) context
                .getAttribute("srObj");
        HashMap<String, ArrayList<Posting>> result = new HashMap<String, ArrayList<Posting>>();
        ArrayList<Posting> pMap = new ArrayList<Posting>();
        result.put("result", pMap);

        try {
            SemanticRelatedness.Postings p = srObj.getSR(sID, langID, spatial);

            for (int i = 0; i < p.ids.length; i++) {
                pMap.add(new Posting(p.ids[i], p.rels[i]));
            }

        } catch (Exception e) {
            System.out.println("Failed to get SR for sid = " + sID
                    + " / langId = " + langID);
            e.printStackTrace();
        }

        result.put("result", pMap);

        return result;

    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/map/sID/{sID}/langID/{langID}")
    public HashMap<Integer, Double> getSRMap(
            @PathParam("sID") Integer sID, @PathParam("langID") Integer langID) {

        SemanticRelatedness srObj = (SemanticRelatedness) context
                .getAttribute("srObj");
        HashMap<Integer, Double> result = new HashMap<Integer, Double>();

        try {
            SemanticRelatedness.Postings p = srObj.getSR(sID, langID);

            for (int i = 0; i < p.ids.length; i++) {
                result.put(p.ids[i], p.rels[i]);
            }

        } catch (Exception e) {
            System.out.println("Failed to get SR Map for sid = " + sID
                    + " / langId = " + langID);
            e.printStackTrace();
        }

        return result;

    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/map/sID/{sID}/langID/{langID}/spatial/{spatial}")
    public HashMap<Integer, Double> getSRMap(
            @PathParam("sID") Integer sID, @PathParam("langID") Integer langID, @PathParam("spatial") Boolean spatial) {

        SemanticRelatedness srObj = (SemanticRelatedness) context
                .getAttribute("srObj");
        HashMap<Integer, Double> result = new HashMap<Integer, Double>();

        try {
            SemanticRelatedness.Postings p = srObj.getSR(sID, langID, spatial);

            for (int i = 0; i < p.ids.length; i++) {
                result.put(p.ids[i], p.rels[i]);
            }

        } catch (Exception e) {
            System.out.println("Failed to get SR Map for sid = " + sID
                    + " / langId = " + langID);
            e.printStackTrace();
        }

        return result;

    }

    //Changed here!
    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/sID/{sID}/langID/{langID}/top/{top}")
    public HashMap<String, ArrayList<Posting>> getSRPostings(
            @PathParam("sID") Integer sID, @PathParam("langID") Integer langID, @PathParam("top") Integer top) {

        SemanticRelatedness srObj = (SemanticRelatedness) context
                .getAttribute("srObj");
        HashMap<String, ArrayList<Posting>> result = new HashMap<String, ArrayList<Posting>>();
        ArrayList<Posting> pMap = new ArrayList<Posting>();
        //Changed here!
        ArrayList<Posting> topNpMap = new ArrayList<Posting>();
        //Changed here!
        result.put("result", pMap);// Why do we add an empty ArrayList here?

        try {
            SemanticRelatedness.Postings p = srObj.getSR(sID, langID);

            for (int i = 0; i < p.ids.length; i++) {
                pMap.add(new Posting(p.ids[i], p.rels[i]));
            }
            //Changed here!
            if (top < 0) top = 0;
            if (top > p.ids.length) top = p.ids.length;
            Collections.sort(pMap, new SortBysrMeasure());
            for (int j = 0; j < top; j++) {
                topNpMap.add(pMap.get(j));
            }
        } catch (Exception e) {
            System.out.println("Failed to get SR for sid = " + sID
                    + " / langId = " + langID + " / top = " + top);
            e.printStackTrace();
        }
        //Changed here!
        result.put("result", topNpMap);
        //result.put("result", pMap);
        //Changed here!

        return result;

    }


    /**
     * @author Xiaofeng
     * @return compare two Posting objects based on their srMeasure values;
     * return 1 if object o1's value is larger
     */
    public static class SortBysrMeasure implements Comparator<Posting> {
        public int compare(Posting s1, Posting s2) {
            if (s1.getSrMeasure() <= s2.getSrMeasure())
                return 1;
            else return -1;
        }
    }

    public class Posting {

        Integer wikiPageId;
        Double srMeasure;

        public Posting(Integer wikiPageId, Double srMeasure) {
            super();
            this.wikiPageId = wikiPageId;
            this.srMeasure = srMeasure;
        }

        public Integer getWikiPageId() {
            return wikiPageId;
        }

        public void setWikiPageId(Integer wikiPageId) {
            this.wikiPageId = wikiPageId;
        }

        public Double getSrMeasure() {
            return srMeasure;
        }

        public void setSrMeasure(Double srMeasure) {
            this.srMeasure = srMeasure;
        }

    }


}