package edu.northwestern.websail.wikisr.adapter;

import edu.northwestern.websail.core.util.TimerUtil;
import edu.northwestern.websail.wikisr.SemanticRelatedness;
import edu.northwestern.websail.wikisr.util.SRCompressor;
import gnu.trove.map.hash.TLongLongHashMap;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * @author Lei and Sai
 *         <p>
 *         revised Chandra Thread-safe
 *         revised NorThanapon Create RAF Pool
 *         </p>
 */

// make sure you configured enough memory space before running it. Approximately
// 100 MB memory needed
public class RAFSR implements Closeable {

    private final RAFDescriptor[] rafPool;
    private final Random rand = new Random();
    private final SRCompressor _src;
    private final String SRdataDir;
    public TLongLongHashMap idToPointer;

    public RAFSR(String milneWittenFile, String milneWittenQuantilesFile,
                 String rafDirectory) throws Exception {
        this(milneWittenFile, milneWittenQuantilesFile, rafDirectory, 1);
    }

    public RAFSR(String milneWittenFile, String milneWittenQuantilesFile,
                 String rafDirectory, int poolSize) throws Exception {
        // idToPointer = new TLongLongHashMap();
        // Step1: init the directory that will hold our data
        // create a folder "big_data" if it does not exist
        File dataFolder = new File(rafDirectory);
        if (!dataFolder.exists()) {
            boolean result = dataFolder.mkdir();
            if (!result) {
                System.out.println("Failed to create directory!");
            }
        }
        // get current working directory
        // String current = System.getProperty("user.dir");
        // System.out.println("Current working directory in Java : " + current);
        SRdataDir = rafDirectory + "/";
        // step 2: read quantiles number from file
        _src = new SRCompressor();
        _src.readQuantilesFromFile(milneWittenQuantilesFile);
        // step 3: create necessiry files if first time use, and load hash
        // map<long long> into memory
        readMWFile(milneWittenFile);
        rafPool = this.initializeRAFPool(poolSize, SRdataDir + "raf");
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.out
                    .println("Usage: SemanticRelatedness <inputDir> <rafDir>"
                            + "\r\ninputDir should contain all SR files and quantiles, output by SemanticRelatednessPrecomputer."
                            + "\r\nrafDir is a directory for random access file (can be empty)");
            return;
        }

        RAFSR sr = new RAFSR(new File(args[0], "mwsr.dat").getAbsolutePath(),
                new File(args[0], "mwsrQuantiles.txt").getAbsolutePath(),
                args[1], 20);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("Enter concept id (q to quit): ");
            String sId = br.readLine();
            int id, lang;
            if (sId.equalsIgnoreCase("q"))
                break;
            else {
                try {
                    id = Integer.parseInt(sId);
                } catch (Exception e) {
                    System.err.println("parse error " + e.getMessage());
                    continue;
                }
            }
            System.out.print("Enter language: ");
            sId = br.readLine();
            if (sId.equalsIgnoreCase("q"))
                break;
            else {
                try {
                    lang = Integer.parseInt(sId);
                } catch (Exception e) {
                    System.err.println("parse error " + e.getMessage());
                    continue;
                }
            }
            System.out.println("id: " + id + " lang: " + lang);
            TimerUtil timer = new TimerUtil();
            timer.start();
            Thread t1 = new Thread(new SRWorker(sr, 0));
            Thread t2 = new Thread(new SRWorker(sr, 1000000));
            Thread t3 = new Thread(new SRWorker(sr, 2000000));
            t1.start();
            t2.start();
            t3.start();
            t1.join();
            t2.join();
            t3.join();
            timer.end();
            System.out.println(timer.totalTime());
            // for (int i = 0; i < p.ids.length; i++) {
            // System.out.println(p.ids[i] + "\t" + p.rels[i]);
            // }
        }
        sr.close();
    }

    public SemanticRelatedness.Postings getSR(int conceptID, int langID) {
        long lid = conceptID + (long) langID * Integer.MAX_VALUE;
        SemanticRelatedness.Postings p = null;
        try {
            p = getSRLongID(lid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return p;
    }

    public SemanticRelatedness.Postings getSRLongID(long conceptID) throws Exception {
        RAFDescriptor[] pool = rafPool;
        if (pool == null) {
            throw new NullPointerException(
                    "RAF Pool has not been initialized properly. Please check your raf file.");
        }
        if (this.idToPointer == null) {
            throw new NullPointerException(
                    "Position Map has not been initialized properly. Please check your pos file.");
        }
        long pos = this.idToPointer.get(conceptID);
        if (pos == -1) {
            return null;
        }
        RAFDescriptor desc = this.getMinUsageDescriptor(pool, pos);
        return desc.read(pos);
    }

    private synchronized RAFDescriptor getMinUsageDescriptor(
            RAFDescriptor[] pool, long position) {
        long min = Long.MAX_VALUE;
        int minIndex;
        ArrayList<Integer> availableIndexes = new ArrayList<Integer>();
        for (RAFDescriptor desc : pool) {
            if (desc.curPos == position) {
                return desc;
            }
            if (desc.usage <= min) {
                min = desc.usage;
            }
        }
        for (int i = 0; i < pool.length; i++) {
            if (pool[i].usage <= min) {
                availableIndexes.add(i);
            }
        }
        minIndex = availableIndexes.get(rand.nextInt(availableIndexes.size()));
        return pool[minIndex];
    }

    // See SemanticRelatednessPrecomputer for companion writing code
    public void readMWFile(String inFile) throws Exception {
        DataInputStream dis = new DataInputStream(new FileInputStream(inFile));
        int numConcepts = dis.readInt();
        long rafPointer = 0;
        // if datafile and hashmap does not exist, create them
        boolean createFiles = true;
        if ((new File(SRdataDir + "raf").exists())
                && (new File(SRdataDir + "positionMap").exists()))
            createFiles = false;
        // String createFiles = "n";
        // Scanner in = new Scanner(System.in);
        // System.out.println("Do you want to recreate data files? (y/n)");
        // createFiles = in.nextLine();
        if (createFiles)// createFiles.equalsIgnoreCase("y"))
        {
            DataOutputStream file = new DataOutputStream(
                    new BufferedOutputStream(new FileOutputStream(SRdataDir
                            + "raf")));
            // RandomAccessFile file = new RandomAccessFile(SRdataDir+"raf",
            // "rw");
            DataOutputStream hashMap = new DataOutputStream(
                    new BufferedOutputStream(new FileOutputStream(SRdataDir
                            + "positionMap")));
            for (int i = 0; i < numConcepts; i++) {
                if (i % 10000 == 0) {
                    System.out.println("Processed " + i + " files.");
                }
                long conceptNum = dis.readLong();
                SRCompressor.CompressedSRs csrs = SRCompressor
                        .readCompressedSRsFromFile(dis); // _mwSRs[i];
                SemanticRelatedness.Postings p = _src.unCompressSRs(csrs);
                int count = csrs.numSRs;// calculate how many lines in out file
                file.writeInt(count);
                for (int j = 0; j < p.rels.length; j++) {
                    file.writeInt(p.ids[j]);
                    file.writeDouble(p.rels[j]);
                }
                hashMap.writeLong(conceptNum); // write the id
                hashMap.writeLong(rafPointer);
                rafPointer += count * 12 + 4; // count * 4 + count *8 + 4(count
                // itself)
            }
            file.close();
            hashMap.close();
        }
        /*
         * //For test use, bounch of heap size check // Get current size of heap
		 * in bytes long heapSize = Runtime.getRuntime().totalMemory(); // Get
		 * maximum size of heap in bytes. The heap cannot grow beyond this size.
		 * // Any attempt will result in an OutOfMemoryException. long
		 * heapMaxSize = Runtime.getRuntime().maxMemory(); // Get amount of free
		 * memory within the heap in bytes. This size will increase // after
		 * garbage collection and decrease as new objects are created. long
		 * heapFreeSize = Runtime.getRuntime().freeMemory();
		 * System.out.println("Current heap size: "+heapSize +
		 * " Max heap size: "+heapMaxSize + " Heap free size: "+heapFreeSize);
		 */
        // now read in the hash map
        DataInputStream mapFile = new DataInputStream(new BufferedInputStream(
                new FileInputStream(SRdataDir + "positionMap")));// buffered
        // input
        // stream
        idToPointer = new TLongLongHashMap(4000000, 0.5f, -1, -1);// new HashMap<Long,
        // Long>();//TLongLongHashMap(4000000);
        // idToPointer.capacity());
        for (int j = 0; j < numConcepts; j++)// numConcepts
        {
            long key = mapFile.readLong();
            long value = mapFile.readLong();
            idToPointer.put(key, value);
        }
        mapFile.close();
    }

    private RAFDescriptor[] initializeRAFPool(int poolSize, String rafFile)
            throws IOException {
        RAFDescriptor[] pool = new RAFDescriptor[poolSize];
        for (int i = 0; i < poolSize; i++) {
            pool[i] = new RAFDescriptor(rafFile);
        }
        return pool;
    }

    @Override
    public void close() throws IOException {
        if (this.rafPool != null) {
            for (RAFDescriptor raf : rafPool) {
                raf.close();
            }
        }
    }

    private static final class RAFDescriptor implements Closeable {
        final RandomAccessFile raf;
        long curPos;
        long usage;
        SemanticRelatedness.Postings current;

        public RAFDescriptor(String rafFile) throws IOException {
            raf = new RandomAccessFile(rafFile, "r");
            usage = 0;
            curPos = -1;
        }

        public SemanticRelatedness.Postings read(long position) throws IOException {
            if (current != null && curPos == position) {
                return current;
            }
            curPos = -2;
            current = null;
            fetchCurrent(position);

            return current;
        }

        private synchronized void fetchCurrent(long position)
                throws IOException {
            usage++;
            current = this.getSRLongID(position);
            curPos = position;
            usage--;
        }

        private synchronized SemanticRelatedness.Postings getSRLongID(long pointerPosi)
                throws IOException {
            SemanticRelatedness.Postings pTmp = new SemanticRelatedness.Postings();
            raf.seek(pointerPosi);
            // how to fix this leak...
            @SuppressWarnings("resource")
            DataInputStream buff = new DataInputStream(new BufferedInputStream(
                    new FileInputStream(raf.getFD())));
            int count = buff.readInt();
            pTmp.ids = new int[count];
            pTmp.rels = new double[count];
            for (int i = 0; i < count; i++) {
                pTmp.ids[i] = buff.readInt();
                pTmp.rels[i] = buff.readDouble();
            }
            //buff.close();
            return pTmp;
        }

        @Override
        public void close() throws IOException {
            raf.close();
        }

    }

    private static class SRWorker implements Runnable {
        final RAFSR sr;
        final int start;

        public SRWorker(RAFSR sr, int start) {
            this.sr = sr;
            this.start = start;
        }

        @Override
        public void run() {
            for (int i = start; i < start + 1000000; i++) {
                sr.getSR(i, 1);
            }
        }

    }
}
