package edu.northwestern.websail.wikisr.adapter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import edu.northwestern.websail.wikisr.SemanticRelatedness;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class WebServiceWikiSRAdapter extends WikiSRAdapter {

    private String serviceEndPoint = "http://downey-n2.cs.northwestern.edu:8080/wikisr/sr/map/sID/@sID/langID/@langID";
    //private Object lock = new Object();

    public WebServiceWikiSRAdapter() {
    }

    public WebServiceWikiSRAdapter(String endPoint) {
        this.serviceEndPoint = endPoint;
    }

    public static void main(String[] args) throws IOException {
        WebServiceWikiSRAdapter sra = new WebServiceWikiSRAdapter();
        System.out.println(sra.getSRs(12));
    }

    private String getRequestURL(int srId, int langId) {
        return this.serviceEndPoint.replaceFirst("@sID", srId + "")
                .replaceFirst("@langID", langId + "");
    }

    @Override
    public HashMap<Integer, Double> getSRs(int langId, int pageId) throws IOException {
        //synchronized(lock){
        String endPoint = this.getRequestURL(pageId, langId);
        //System.out.println(endPoint);
        URL u = new URL(endPoint);
        HttpURLConnection uc = (HttpURLConnection) u.openConnection();

        uc.connect();
        int responseCode = uc.getResponseCode();
        Gson g = new Gson();
        Type type = new TypeToken<HashMap<Integer, Double>>() {
        }.getType();
        if (200 == responseCode) {
            return g.fromJson(
                    new InputStreamReader(uc.getInputStream()), type);
        }
        //}
        return null;
    }

    @Override
    public void close() throws IOException {

    }

    @Override
    public void init() {

    }

	@Override
    public SemanticRelatedness.Postings getSRPostings(int langId, int pageId) throws IOException {
        // TODO: Convert SR JSON to Postings
        return null;
	}


}
