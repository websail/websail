package edu.northwestern.websail.wikisr.util;

//Class for compressing SRs to optimize storage in memory or on disk
//Performs quantization of SR values, and diff-based storing of arrays of indexes
//Always assumes SR values are quantized into 256 bins (for storage in one byte)


import edu.northwestern.websail.wikisr.SemanticRelatedness;

import java.io.*;
import java.util.Arrays;

public class SRCompressor {

    public final double[] quantiles = new double[256];

    public static void writeCompressedSRsToFile(DataOutputStream dosOut, CompressedSRs csrs) throws Exception {
        dosOut.writeInt(csrs.numSRs);
        dosOut.writeInt(csrs.data.length);
        dosOut.write(csrs.data);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static CompressedSRs readCompressedSRsFromFile(DataInputStream dosIn) throws Exception {
        CompressedSRs out = new CompressedSRs();
        out.numSRs = dosIn.readInt();
        out.data = new byte[dosIn.readInt()];
        dosIn.read(out.data);
        return out;
    }

    public static void main(String[] args) {
        int a = 200;
        byte b = (byte) a;
        int c = 256 + (int) b;
        System.out.println(c);
    }

    public void writeQuantilesToFile(String outFile) throws Exception {
        BufferedWriter bwOut = new BufferedWriter(new FileWriter(outFile));

        for (double quantile : quantiles) bwOut.write(quantile + "\r\n");
        bwOut.close();
    }

    public void readQuantilesFromFile(String inFile) throws Exception {
        BufferedReader brIn = new BufferedReader(new FileReader(inFile));
        for (int i = 0; i < quantiles.length; i++)
            quantiles[i] = Double.parseDouble(brIn.readLine());
        brIn.close();
    }

    //takes in a random sample of SR values, and sets SR quantiles such that SR values
    //are spaced in linearly decreasing distance as SR increases (to give more resolution at high SR values).
    public void setQuantiles(double[] sampleSRValues) {
        Arrays.sort(sampleSRValues);
        int curIdx = 0;
        for (int i = 0; i < 256; i++) {
            long num = ((256 - i) * 2L * sampleSRValues.length);
            num /= (long) (256 * 257);
            int step = (int) num;
            int halfstep = step / 2;
            curIdx += step;
            quantiles[i] = sampleSRValues[curIdx - halfstep];
        }
    }

    public byte quantize(double d) {
        int idx = Arrays.binarySearch(quantiles, d);
        if (idx >= 0)
            return (byte) idx;
        else {
            int ret = -(idx + 1);
            if (ret > 255)
                ret = 255;
            return (byte) ret;
        }

    }

    public double unquantize(byte b) {
        int i = b;
        if (i < 0)
            i += 256; //HACK to allow unsigned ints
        return quantiles[i];
    }

    //compresses the given list of SR values and ids
    public CompressedSRs compressSRs(SemanticRelatedness.Postings ps) throws Exception {
        double[] srs = ps.rels;
        int[] ids = ps.ids;
        if (srs.length != ids.length)
            throw new Exception("sr and id array lengths must match.");
        if (srs.length == 0)
            return null;
        //convert to diff-based storage:
        //TODO: avoid this temporary array creation:
        int[] diffs = new int[ids.length];
        diffs[0] = ids[0];
        for (int i = 1; i < ids.length; i++) {
            diffs[i] = ids[i] - ids[i - 1];
        }
        //compress with variable-byte encoding:
        byte[] idBytes = VariableByteInteger.encode(diffs, srs.length); //add extra buff space forsrs.
        //quantize to bins:
        for (int i = idBytes.length - srs.length; i < idBytes.length; i++) {
            int j = i - (idBytes.length - srs.length);
            idBytes[i] = quantize(srs[j]);
        }
        CompressedSRs out = new CompressedSRs();
        out.data = idBytes;
        out.numSRs = srs.length;
        return out;
    }

    public SemanticRelatedness.Postings unCompressSRs(CompressedSRs in) {
        SemanticRelatedness.Postings out = new SemanticRelatedness.Postings();
        out.ids = new int[in.numSRs];
        out.rels = new double[in.numSRs];
        out.ids = VariableByteInteger.decode(in.data, in.data.length - out.ids.length);
        for (int i = 1; i < out.ids.length; i++)
            out.ids[i] += out.ids[i - 1];
        for (int i = 0; i < out.rels.length; i++) {
            int j = i + (in.data.length - out.ids.length);
            out.rels[i] = unquantize(in.data[j]);
        }
        return out;
    }

    public static class CompressedSRs {
        public byte[] data;
        public int numSRs;
    }

}
