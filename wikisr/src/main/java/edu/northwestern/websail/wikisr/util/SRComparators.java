package edu.northwestern.websail.wikisr.util;

import java.util.Comparator;

/**
 * @author NorThanapon, Xiaofeng
 * @since 6/25/15
 */
public class SRComparators {

    public static class ArrayIndexComparator implements Comparator<Integer> {
        private final double[] array;

        public ArrayIndexComparator(double[] array) {
            this.array = array;
        }

        @Override
        public int compare(Integer index1, Integer index2) {
            return ((Double) array[index2]).compareTo(array[index1]);
        }
    }

}
