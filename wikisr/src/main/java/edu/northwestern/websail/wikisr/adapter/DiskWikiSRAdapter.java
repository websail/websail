package edu.northwestern.websail.wikisr.adapter;

import edu.northwestern.websail.core.util.TimerUtil;
import edu.northwestern.websail.wikisr.SemanticRelatedness;
import edu.northwestern.websail.wikisr.util.OptimizedSRCompressor;
import edu.northwestern.websail.wikisr.util.SRCompressor;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

public class DiskWikiSRAdapter extends WikiSRAdapter {
    public static final SemanticRelatedness.Postings EMPTY_POSTING = new SemanticRelatedness.Postings();

    static {
		EMPTY_POSTING.ids = new int[1];
		EMPTY_POSTING.rels = new double[1];
	}

	final String directory;
    SRWorker.LightSemanticRelatedness sr;

    public DiskWikiSRAdapter(String directory) {
		this.directory = directory;
	}

	public static void main(String[] args) throws Exception {
		System.out.println("Loading SR...");
		DiskWikiSRAdapter adapter = new DiskWikiSRAdapter("/websail/common/wikification/data/en/sr/srFiles-textSkippedLinks/");
		adapter.init();

		int[] targets = new int[4];
		targets[0] = 6886;
		targets[1] = 6890;
		targets[2] = 45434;
		targets[3] = 1243545;
        SemanticRelatedness.Postings p = adapter.getSRPostings(1, 6886, targets);

		for (int i = 0; i < p.ids.length; i++) {
			System.out.println(p.ids[i] + ": " + p.rels[i]);
		}

		Thread[] ts = new Thread[32];
		for (int i = 0; i < 32; i++) {
			ts[i] = new Thread(new SRWorker(adapter, i * 100000));
		}
		System.out.println("Start testing...");
		TimerUtil timer = new TimerUtil();
		timer.start();
		for (int i = 0; i < 32; i++) {
			ts[i].start();
		}
		for (int i = 0; i < 32; i++) {
			ts[i].join();
		}
		timer.end();
		System.out.println(timer.totalTime());
	}
	
	@Override
	public HashMap<Integer, Double> getSRs(int langId, int pageId)
			throws IOException {
		HashMap<Integer, Double> result = new HashMap<Integer, Double>();
		try {
            SemanticRelatedness.Postings p = sr.getSR(pageId, langId);
            if (p == null || p.ids == null) {
				logger.warning("No SR for pageId: " + pageId);
			} else {
				for (int i = 0; i < p.ids.length; i++) {
					result.put(p.ids[i], p.rels[i]);
				}
			}
		} catch (Exception e) {
			logger.severe("Error while retrieving SR for pageId: " + pageId);
			e.printStackTrace();
		}

		return result;
	}

	@Override
    public SemanticRelatedness.Postings getSRPostings(int langId, int pageId) throws IOException {
        try{
			return sr.getSR(pageId, langId);
		} catch (Exception e) {
			logger.severe("Error while retrieving SR for pageId: " + pageId);
			e.printStackTrace();
		}
		return null;
	}

	@Override
    public SemanticRelatedness.Postings getSRPostings(int langId, int pageId, int[] sortedTargets) throws IOException {
        try{
			if(sortedTargets.length <= 0) return EMPTY_POSTING;
			return sr.getSR(pageId, langId, sortedTargets);
		} catch (Exception e) {
			logger.severe("Error while retrieving SR for pageId: " + pageId);
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void close() throws IOException {
	}

	@Override
	public void init() {
		try {
            sr = new SRWorker.LightSemanticRelatedness(
                    new File(directory, "mwsr.dat").getAbsolutePath(),
					new File(directory, "mwsrQuantiles.txt").getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static class SRWorker implements Runnable{

        final DiskWikiSRAdapter sr;
        final int start;
		final Random rand = new Random();
		public SRWorker(DiskWikiSRAdapter sr, int start){
			this.sr = sr;
			this.start = start;
		}

        @Override
        public void run() {
			for (int i = start; i < start + 100000; i++) {
				try {
					int[] targets = new int[50];
					for(int j = 0; j < 50; j++){
						targets[j] = rand.nextInt(9999998);
					}
					Arrays.sort(targets);
					sr.getSRPostings(1, i, targets);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

        // XXX: HACK! SemanticRelatedness original version
        private static class LightSemanticRelatedness {

            private SRCompressor.CompressedSRs[] _mwSRs;
            private long[] _concepts; // assumed in sorted order
            private OptimizedSRCompressor _src;

            public LightSemanticRelatedness(String milneWittenFile,
                                            String milneWittenQuantilesFile) throws Exception {
                readMWFile(milneWittenFile);
                _src = new OptimizedSRCompressor();
                _src.readQuantilesFromFile(milneWittenQuantilesFile);
            }

            // See SemanticRelatednessPrecomputer for companion writing code
            public void readMWFile(String inFile) throws Exception {
                DataInputStream dis = new DataInputStream(new FileInputStream(
                        inFile));
                // dis.readInt();
                int numConcepts = dis.readInt();
                _concepts = new long[numConcepts];
                _mwSRs = new SRCompressor.CompressedSRs[numConcepts];
                for (int i = 0; i < numConcepts; i++) {
                    _concepts[i] = dis.readLong();
                    _mwSRs[i] = SRCompressor.readCompressedSRsFromFile(dis);
                }
            }

            public edu.northwestern.websail.wikisr.SemanticRelatedness.Postings getSR(int conceptID, int langID) {
                long lid = (long) conceptID + (long) langID * Integer.MAX_VALUE;
                return getSRLongID(lid);
            }

            public edu.northwestern.websail.wikisr.SemanticRelatedness.Postings getSR(int conceptID, int langID, int[] sortedTargets) {
                long lid = (long) conceptID + (long) langID * Integer.MAX_VALUE;
                return getSRLongID(lid, sortedTargets);
            }

            public edu.northwestern.websail.wikisr.SemanticRelatedness.Postings getSRLongID(long conceptID) {
                int i = Arrays.binarySearch(_concepts, conceptID);
                if (i < 0)
                    return null;
                SRCompressor.CompressedSRs csrs = _mwSRs[i];
                return _src.unCompressSRs(csrs);
            }

            public edu.northwestern.websail.wikisr.SemanticRelatedness.Postings getSRLongID(long conceptID, int[] sortedTargets) {
                int i = Arrays.binarySearch(_concepts, conceptID);
                if (i < 0)
                    return null;
                SRCompressor.CompressedSRs csrs = _mwSRs[i];
                return _src.unCompressSRs(csrs, sortedTargets);
            }
        }

    }
}
