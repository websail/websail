package edu.northwestern.websail.wikisr.link;

import edu.northwestern.websail.utils.MySQLQueryHandler;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

public class GenRedirectMapFile {

    public static void main(String[] args) throws IOException, IllegalAccessException, ClassNotFoundException, InstantiationException, SQLException {
        long start_time = System.currentTimeMillis();

        String outputFile = args[0];
        String dbUser = args[1];
        String dbPwd = args[2];
        String dbHost = args[3];
        String dbName = args[4];

        System.out.println("Start processing with ");
        System.out.print("Connecting to DB...");
        MySQLQueryHandler handler = new MySQLQueryHandler(
                dbHost,
                dbName,
                dbUser,
                dbPwd);
        Connection conn = handler.getConn();

        MySQLQueryHandler streamHandler = new MySQLQueryHandler(
                dbHost,
                dbName,
                dbUser,
                dbPwd);
        Connection streamConn = streamHandler.getConn();
        System.out.println("Success!");

        System.out.print("Creating output file...");
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(outputFile), "UTF-8"));
        System.out.println("Success!");

        System.out.println("Starting processing, this will take a few minutes...");
        HashMap<String, Integer> titleRedirIdMap =
                getTitleRedirectsIDMapsFromDB(streamConn, conn, "page", "redirect");
        for (Entry<String, Integer> e : titleRedirIdMap.entrySet()) {
            out.write(e.getKey() + "\t" + e.getValue() + "\n");
        }
        out.close();
        System.out.println("Success!");

        long end_time = System.currentTimeMillis();

        long difference = end_time - start_time;

        System.out.println("time:" + String.format("%d min, %d sec",
                TimeUnit.MILLISECONDS.toMinutes(difference),
                TimeUnit.MILLISECONDS.toSeconds(difference) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(difference))
        ));
    }

    public static HashMap<String, Integer> getTitleRedirectsIDMapsFromDB(
            Connection streamConn, Connection conn, String pageTable,
            String redirectTable) throws UnsupportedEncodingException,
            SQLException {
        HashMap<String, Integer> titleRedirIdMap = new HashMap<String, Integer>(
                18000000);
        putAllTitleIds(conn, titleRedirIdMap, pageTable);
        putAllRedirectIds(streamConn, conn, titleRedirIdMap, pageTable,
                redirectTable);
        return titleRedirIdMap;

    }

    public static void putAllTitleIds(Connection conn,
                                      HashMap<String, Integer> titleIdMap, String pageTable)
            throws SQLException, UnsupportedEncodingException {

        PreparedStatement allPageTitleIdPs = conn.prepareStatement(
                "SELECT page_title , page_id " + "FROM " + pageTable
                        + " WHERE page_namespace = 0 "
                        + "AND page_is_redirect = 0",
                ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        allPageTitleIdPs.setFetchSize(Integer.MIN_VALUE);

        ResultSet allPageTitleIdRs = allPageTitleIdPs.executeQuery();
        while (allPageTitleIdRs.next()) {

            titleIdMap.put(new String(allPageTitleIdRs.getBytes("page_title"),
                    "UTF-8"), allPageTitleIdRs.getInt("page_id"));
        }

        allPageTitleIdRs.close();
        allPageTitleIdPs.close();

    }

    public static void putAllRedirectIds(Connection streamConn,
                                         Connection conn, HashMap<String, Integer> titleRedirIdMap,
                                         String pageTable, String redirectTable) throws SQLException,
            UnsupportedEncodingException {

        PreparedStatement redirPs = streamConn
                .prepareStatement(
                        "select p1.page_title p1Title , p2.page_id , p2.page_is_redirect , p2.page_title p2Title "
                                + "FROM "
                                + pageTable
                                + " p1 , "
                                + redirectTable
                                + " rd , "
                                + pageTable
                                + " p2 "
                                + "WHERE p1.page_namespace =0 AND p1.page_id = rd.rd_from "
                                + "AND rd.rd_title = p2.page_title AND p2.page_namespace = 0",
                        ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
        redirPs.setFetchSize(Integer.MIN_VALUE);
        ResultSet redirRs = redirPs.executeQuery();

        String fromTitle;
        String toTitle;
        Integer toId;

        while (redirRs.next()) {

            fromTitle = new String(redirRs.getBytes("p1Title"), "UTF-8");
            toTitle = new String(redirRs.getBytes("p2Title"), "UTF-8");
            if (titleRedirIdMap.containsKey(toTitle)) {
                titleRedirIdMap.put(fromTitle, titleRedirIdMap.get(toTitle));
            } else {

                if (redirRs.getInt("page_is_redirect") == 0) {
                    toId = redirRs.getInt("page_id");
                } else {
                    toId = getFinalRedirectId(conn, redirRs.getInt("page_id"),
                            pageTable, redirectTable);
                    if (toId == -1)
                        toId = redirRs.getInt("page_id");
                }
                titleRedirIdMap.put(fromTitle, toId);
            }
        }
        redirPs.close();
        redirRs.close();

    }

    public static Integer getFinalRedirectId(Connection conn, int toId,
                                             String pageTable, String redirectTable) throws SQLException {

        PreparedStatement ps = conn
                .prepareStatement("SELECT rd.rd_from , p.page_id , p.page_is_redirect FROM "
                        + redirectTable
                        + " rd , "
                        + pageTable
                        + " p WHERE rd.rd_title  = p.page_title AND p.page_namespace = 0 AND rd_from = ? ");
        ResultSet rs;

        int idx = 0;
        boolean done = false;
        do {
            ps.setInt(1, toId);
            rs = ps.executeQuery();
            while (rs.next()) {
                toId = rs.getInt("page_id");
                if (rs.getInt("page_is_redirect") == 0) {
                    done = true;
                }
            }
        } while (!done && idx++ < 10);
        if (idx >= 10)
            return -1;
        return toId;

    }

}
