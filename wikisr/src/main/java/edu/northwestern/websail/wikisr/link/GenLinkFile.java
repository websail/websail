package edu.northwestern.websail.wikisr.link;

import com.google.gson.Gson;
import com.mongodb.*;
import edu.northwestern.websail.core.util.TimerUtil;
import edu.northwestern.websail.wikiparser.model.WikiExtractedPage;
import edu.northwestern.websail.wikiparser.model.WikiExtractedPageAdapter;
import edu.northwestern.websail.wikiparser.model.element.WikiLink;
import edu.northwestern.websail.wikiparser.model.element.WikiPageLocationType;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author csbhagav
 */

public class GenLinkFile {

    private static int NTHREDS = 10;
    private static HashMap<String, Integer> redirectMap;
    private static MongoClient mongoClient;
    private static WikiExtractedPageAdapter adapter;
    private static String outputFile;
    private static Boolean isTest;

    public static void main(String[] args) throws IOException, InterruptedException {
        TimerUtil timer = new TimerUtil();
        timer.start();
        initialize(args);
        int count = 30;
        if (!isTest) {
            count = adapter.getAllNonRedirectedPageCursor().count();
        }
        ArrayList<LinkExtrJob> jobs = partitionJobs(count);

        BufferedWriter out = new BufferedWriter(new FileWriter(new File(outputFile)));

        ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
        for (int i = 0; i < NTHREDS; i++) {
            LinkExtractionWorker worker = new LinkExtractionWorker(i, jobs.get(i), out);
            executor.execute(worker);
        }
        executor.shutdown();
        executor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);

        if (mongoClient != null) mongoClient.close();
        out.close();
        timer.end();
        System.out.println("Done " + timer.totalTime());
    }

    private static void initialize(String[] args) throws IOException {
        String redirectMapFile = args[0];
        String mongoHost = args[1];
        String mongoPort = args[2];
        String mongoDB = args[3];
        String mongoUser = args[4];
        String mongoPwd = args[5];
        outputFile = args[6];
        NTHREDS = Integer.valueOf(args[7]);
        isTest = Boolean.valueOf(args[8]);

        redirectMap = getTitleRedirectsIDMapsFromFile(redirectMapFile);
        System.out.println("Connecting to mongodb");

        MongoClientOptions.Builder builder = MongoClientOptions.builder();
        builder.autoConnectRetry(true);
        builder.connectionsPerHost(NTHREDS);
        builder.connectTimeout(3600000);
        builder.socketTimeout(3600000);

        mongoClient = new MongoClient(Arrays.asList(
                new ServerAddress(mongoHost, Integer.parseInt(mongoPort))),
                builder.build());
        //mongoClient.setReadPreference(ReadPreference.secondaryPreferred());

        DB db = mongoClient.getDB(mongoDB);
        db.authenticate(mongoUser, mongoPwd.toCharArray());
        adapter = new WikiExtractedPageAdapter(db);
    }

    private static ArrayList<LinkExtrJob> partitionJobs(int totalCount) {
        ArrayList<LinkExtrJob> jobs = new ArrayList<LinkExtrJob>();
        int each = totalCount / NTHREDS;
        int rest = totalCount % NTHREDS;
        int current = 0;
        for (int i = 0; i < NTHREDS; i++) {
            LinkExtrJob job = new LinkExtrJob(current, each, adapter.getAllNonRedirectedPageCursor());
            if (rest > 0) job.length++;
            current += job.length;
            rest--;
            jobs.add(job);
        }

        return jobs;
    }

    public static HashMap<String, Integer> getTitleRedirectsIDMapsFromFile(
            String wikiPageIdMapFile)
            throws IOException {

        BufferedReader in = new BufferedReader(new InputStreamReader(
                new FileInputStream(wikiPageIdMapFile), "UTF-8"));
        HashMap<String, Integer> wikiPageIdMap = new HashMap<String, Integer>();
        String line;
        String[] parts;
        while ((line = in.readLine()) != null) {
            parts = line.split("\t");
            String title = parts[0];
            wikiPageIdMap.put(title, Integer.valueOf(parts[1]));
        }

        in.close();
        return wikiPageIdMap;
    }

    private static class LinkExtrJob {
        int start;
        int length;
        DBCursor cursor;

        public LinkExtrJob(int start, int length, DBCursor cursor) {
            this.start = start;
            this.length = length;
            this.cursor = cursor;
        }
    }

    private static class LinkExtractionWorker implements Runnable {
        private final BufferedWriter out;
        private LinkExtrJob job;
        private int id;
        private int batchSize = 1000;

        public LinkExtractionWorker(int id, LinkExtrJob job, BufferedWriter out) {
            this.job = job;
            this.id = id;
            this.job.cursor.skip(this.job.start);
            this.out = out;
        }

        @Override
        public void run() {
            System.out.println(id + ": start extracting links");

            int count = 0;
            Gson gson = new Gson();
            try {

                StringBuilder sb = new StringBuilder();
                while (job.cursor.hasNext() && count < job.length) {
                    WikiExtractedPage p = gson.fromJson(job.cursor.next().toString(), WikiExtractedPage.class);

                    ArrayList<WikiLink> links = p.getInternalLinks();

                    for (WikiLink link : links) {


                        if (link.getTarget().getNamesapce() == 0) {

                            int from = p.getTitle().getId();
                            if (!redirectMap.containsKey(link.getTarget().getTitle())) {
                                continue;
                            }
                            int to = redirectMap.get(link.getTarget().getTitle());

                            int gloss = 0;
                            if (link.getLocType() == WikiPageLocationType.OVERVIEW) {
                                gloss = 1;
                            }
                            sb.append(from).append("\t").append(to).append("\t1\t").append(gloss).append("\n");
                        }
                    }
                    count++;

                    if (count % batchSize == 0) {
                        System.out.println("Thread #" + id + "-" + count);
                        String linksStr = sb.toString();
                        synchronized (out) {
                            out.write(linksStr);
                        }
                        sb.setLength(0);
                    }

                }

                System.out.println("Thread #" + id + "-" + count);
                String linksStr = sb.toString();
                synchronized (out) {
                    out.write(linksStr);
                }
                sb.setLength(0);


                System.out.println("Thread #" + id + "-" + count + " - DONE !!!");


            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                job.cursor.close();
            }

        }
    }
}
