package edu.northwestern.websail.wikisr.adapter;


import edu.northwestern.websail.wikisr.SemanticRelatedness;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;

public abstract class WikiSRAdapter {
	public static final Logger logger = Logger.getLogger(WikiSRAdapter.class.getName());
	protected final int defaultLangId = 1;
	public WikiSRAdapter(){}
	
	public abstract HashMap<Integer, Double> getSRs(int langId, int pageId) throws IOException;

    public abstract SemanticRelatedness.Postings getSRPostings(int langId, int pageId) throws IOException;

    public abstract void close() throws IOException;

    public HashMap<Integer, Double> getSRs(int pageId) throws IOException{
		return this.getSRs(defaultLangId, pageId);
	}

    public SemanticRelatedness.Postings getSRPostings(int pageId) throws IOException {
        return this.getSRPostings(defaultLangId, pageId);
    }

    public SemanticRelatedness.Postings getSRPostings(int langId, int pageId, int[] sortedTargets) throws IOException {
        return getSRPostings(langId, pageId);
    }
    public abstract void init();
}
