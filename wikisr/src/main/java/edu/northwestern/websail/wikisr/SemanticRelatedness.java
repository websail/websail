package edu.northwestern.websail.wikisr;

import edu.northwestern.websail.wikisr.util.SRCompressor;
import gnu.trove.map.hash.TLongLongHashMap;
import gnu.trove.set.hash.TIntHashSet;

import java.io.*;

/**
 * @author Doug, Lei & Sai, NorThanapon
 *         Copied from old project
 */
// make sure you configured enough memory space before running it. Approximately
// 100 MB memory needed
public class SemanticRelatedness {

    public final RandomAccessFile _wikiRaf;
    private final SRCompressor _src;
    private final String _SRdataDir;
    public TLongLongHashMap _idToPointer;
    public TIntHashSet _spatialIds;
    private long _FirstKey = 0;

    public SemanticRelatedness(String milneWittenFile,
                               String milneWittenQuantilesFile,
                               String rafDirectory) throws Exception {
        // idToPointer = new TLongLongHashMap();
        // Step1: init the directory that will hold our data
        // create a folder "big_data" if it does not exist
        File dataFolder = new File(rafDirectory);
        if (!dataFolder.exists()) {
            boolean result = dataFolder.mkdir();
            if (!result) {
                System.out.println("Failed to create directory!");
            }
        }
        // get current working directory
        //String current = System.getProperty("user.dir");
        // System.out.println("Current working directory in Java : " + current);
        _SRdataDir = rafDirectory + "/";
        // step 2: read quantiles number from file
        _src = new SRCompressor();
        _src.readQuantilesFromFile(milneWittenQuantilesFile);
        // step 3: create necessiry files if first time use, and load hash
        // map<long long> into memory
        readMWFile(milneWittenFile);
        // open random access file, and keep it open
        _wikiRaf = new RandomAccessFile(_SRdataDir + "raf", "r");
    }

    public SemanticRelatedness(String milneWittenFile,
                               String milneWittenQuantilesFile,
                               String rafDirectory,
                               String spatialFile) throws Exception {
        this(milneWittenFile, milneWittenQuantilesFile, rafDirectory);
        readSpatialFile(spatialFile);
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.out.println("Usage: SemanticRelatedness <inputDir> <rafDir>" +
                    "\r\ninputDir should contain all SR files and quantiles, output by SemanticRelatednessPrecomputer, " +
                    "\r\nalong with a tab-delimited spatialIds file with the spatial ID in the first field." +
                    "\r\nrafDir is a directory for random access file (can be empty)");
            return;
        }

        // HACK: the file stuff is to handle windows and unix:
//        SemanticRelatedness sr = new SemanticRelatedness(new File(args[0],
//                "mwsr.dat").getAbsolutePath(), new File(args[0],
//                "mwsrQuantiles.txt").getAbsolutePath(), args[1]);
        SemanticRelatedness sr;
        File spatialFile = new File(args[0], "spatialIds.txt");
        boolean spExisted = spatialFile.exists();
        if (spExisted) {
            sr = new SemanticRelatedness(new File(args[0],
                    "mwsr.dat").getAbsolutePath(), new File(args[0],
                    "mwsrQuantiles.txt").getAbsolutePath(), args[1],
                    new File(args[0],
                            "spatialIds.txt").getAbsolutePath());
        } else {
            sr = new SemanticRelatedness(new File(args[0],
                    "mwsr.dat").getAbsolutePath(), new File(args[0],
                    "mwsrQuantiles.txt").getAbsolutePath(), args[1]);
            System.out.println("Spatial file not found");
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        // int res = sr.testSRSet(1000);
        // System.out.println(res + " of " + 1000 + " tests passed.");

        while (true) {
            System.out.print("Enter concept id (q to quit): ");
            String sId = br.readLine();
            int id, lang;
            if (sId.equalsIgnoreCase("q"))
                break;
            else {
                try {
                    id = Integer.parseInt(sId);
                } catch (Exception e) {
                    System.err.println("parse error " + e.getMessage());
                    continue;
                }
            }
            System.out.print("Enter language: ");
            sId = br.readLine();
            if (sId.equalsIgnoreCase("q"))
                break;
            else {
                try {
                    lang = Integer.parseInt(sId);
                } catch (Exception e) {
                    System.err.println("parse error " + e.getMessage());
                    continue;
                }
            }
            boolean spatial = false;
            if (spExisted) {
                System.out.print("Scope to spatial? (y/n): ");

                sId = br.readLine();
                if (sId.equalsIgnoreCase("q"))
                    break;
                else {
                    try {
                        if (sId.equalsIgnoreCase("y")) {
                            spatial = true;
                        }
                    } catch (Exception e) {
                        System.err.println("parse error " + e.getMessage());
                        continue;
                    }
                }
            }
            Postings p = sr.getSR(id, lang, spatial);
            for (int i = 0; i < p.ids.length; i++) {
                System.out.println(p.ids[i] + "\t" + p.rels[i]);
            }
        }
        sr._wikiRaf.close(); //design decision: keep the random access file open
    }

    //TODO: consider storing spatial entities in a separate file, to increase efficiency for that case
    public Postings getSR(int conceptID, int langID, boolean spatialOnly) {
        long lid = conceptID + (long) langID * Integer.MAX_VALUE;
        Postings p = null;
        try {
            // System.out.println(lid);//test
            p = getSRLongID(lid);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
        if (p == null) return null;
        if (spatialOnly) {
            int newSize = 0;
            for (int i = 0; i < p.ids.length; i++) {
                if (_spatialIds.contains(p.ids[i]))
                    newSize++;
            }
            //ugh:
            Postings newP = new Postings();
            newP.ids = new int[newSize];
            newP.rels = new double[newSize];
            int idx = 0;
            for (int i = 0; i < p.ids.length; i++) {
                if (_spatialIds.contains(p.ids[i])) {
                    newP.rels[idx] = p.rels[i];
                    newP.ids[idx++] = p.ids[i];
                }
            }
            p = newP;
        }
        return p;
    }

    public int readSpatialFile(String inFile) throws Exception {
        BufferedReader brIn = new BufferedReader(
                new InputStreamReader(new FileInputStream(inFile), "UTF8"));
        String sLine;
        _spatialIds = new TIntHashSet();
        while ((sLine = brIn.readLine()) != null) {
            String[] fields = sLine.split(",");
            if (fields.length < 2)
                continue;
            int id = Integer.parseInt(fields[0]);
            _spatialIds.add(id);
        }
        brIn.close();
        System.out.println("Read: " + _spatialIds.size() + " spatial ids.");
        return _spatialIds.size();
    }

    // See SemanticRelatednessPrecomputer for companion writing code
    public void readMWFile(String inFile) throws Exception {
        DataInputStream dis = new DataInputStream(new FileInputStream(inFile));
        int numConcepts = dis.readInt();
        long rafPointer = 0;
        // if datafile and hashmap does not exist, create them
        boolean createFiles = true;
        if ((new File(_SRdataDir + "raf").exists())
                && (new File(_SRdataDir + "positionMap").exists()))
            createFiles = false;
        // String createFiles = "n";
        // Scanner in = new Scanner(System.in);
        // System.out.println("Do you want to recreate data files? (y/n)");
        // createFiles = in.nextLine();
        if (createFiles)// createFiles.equalsIgnoreCase("y"))
        {
            DataOutputStream file = new DataOutputStream(
                    new BufferedOutputStream(new FileOutputStream(_SRdataDir
                            + "raf")));
            // RandomAccessFile file = new RandomAccessFile(SRdataDir+"raf",
            // "rw");
            DataOutputStream hashMap = new DataOutputStream(
                    new BufferedOutputStream(new FileOutputStream(_SRdataDir
                            + "positionMap")));
            for (int i = 0; i < numConcepts; i++) {
                if (i % 10000 == 0) {
                    System.out.println("Processed " + i + " files.");
                }
                long conceptNum = dis.readLong();
                SRCompressor.CompressedSRs csrs = SRCompressor
                        .readCompressedSRsFromFile(dis); // _mwSRs[i];
                SemanticRelatedness.Postings p = _src.unCompressSRs(csrs);
                int count = csrs.numSRs;// calculate how many lines in out file
                file.writeInt(count);
                for (int j = 0; j < p.rels.length; j++) {
                    file.writeInt(p.ids[j]);
                    file.writeDouble(p.rels[j]);
                }
                hashMap.writeLong(conceptNum); // write the id
                hashMap.writeLong(rafPointer);
                rafPointer += count * 12 + 4; // count * 4 + count *8 + 4(count
                // itself)
            }
            file.close();
            hashMap.close();
        }
        /*
         * //For test use, bounch of heap size check // Get current size of heap
		 * in bytes long heapSize = Runtime.getRuntime().totalMemory(); // Get
		 * maximum size of heap in bytes. The heap cannot grow beyond this size.
		 * // Any attempt will result in an OutOfMemoryException. long
		 * heapMaxSize = Runtime.getRuntime().maxMemory(); // Get amount of free
		 * memory within the heap in bytes. This size will increase // after
		 * garbage collection and decrease as new objects are created. long
		 * heapFreeSize = Runtime.getRuntime().freeMemory();
		 * System.out.println("Current heap size: "+heapSize +
		 * " Max heap size: "+heapMaxSize + " Heap free size: "+heapFreeSize);
		 */
        // now read in the hash map
        DataInputStream mapFile = new DataInputStream(new BufferedInputStream(
                new FileInputStream(_SRdataDir + "positionMap")));// buffered
        // input
        // stream
        _idToPointer = new TLongLongHashMap();// new HashMap<Long,
        // Long>();//TLongLongHashMap(4000000);
        // idToPointer.capacity());
        for (int j = 0; j < numConcepts; j++)// numConcepts
        {
            long key = mapFile.readLong();
            long value = mapFile.readLong();
            if (j == 0) {
                _FirstKey = key;
                System.out.println("key: " + _FirstKey + " id: " + value);
            }
            _idToPointer.put(key, value);
        }
        mapFile.close();
    }

    public Postings getSR(int conceptID, int langID) {
        return getSR(conceptID, langID, false);
    }

    public Postings getSRLongID(long conceptID) throws Exception {
        long pointerPosi = _idToPointer.get(conceptID);
        Postings pTmp = new Postings();
        if ((pointerPosi > 0) || conceptID == _FirstKey) {
            DataInputStream buff;
            synchronized (_wikiRaf) {
                _wikiRaf.seek(pointerPosi);
                buff = new DataInputStream(new BufferedInputStream(
                        new FileInputStream(_wikiRaf.getFD())));
                int count = buff.readInt();
                // System.out.println(count);
                pTmp.ids = new int[count];
                pTmp.rels = new double[count];
                for (int i = 0; i < count; i++) {
                    pTmp.ids[i] = buff.readInt();
                    pTmp.rels[i] = buff.readDouble();
                }
            }
            // buff.close();//do not close the buff, it will cause the random
            // access file be closed
        }
        return pTmp;
    }

    // ensures SRs are symmetric
    /*
     * public int testSRSet(int numTests) throws Exception { Random r = new
	 * Random(); int correct=0; for(int i=0; i<numTests;i++) { // int idx =
	 * r.nextInt(_concepts.length); int idx = 2000; int startConcept =
	 * (int)(_concepts[idx] - Integer.MAX_VALUE); Postings p =
	 * this.getSR(startConcept, 1); idx = r.nextInt(p.ids.length); double
	 * srToMatch = p.rels[idx]; int endConcept = p.ids[idx]; p =
	 * getSR(endConcept, 1); idx = Arrays.binarySearch(p.ids, startConcept);
	 * if(idx < 0) System.err.println("Concept " + startConcept + " related to "
	 * + endConcept + " sr=" + srToMatch + " but converse not found."); else
	 * if(p.rels[idx]!=srToMatch) { System.err.println("Concept " + startConcept
	 * + " related to " + endConcept + " sr=" + srToMatch + " but converse is "
	 * + p.rels[idx]); } else correct++; } return correct; }
	 */

    public static class Postings {
        public int[] ids;
        public double[] rels;
    }
}