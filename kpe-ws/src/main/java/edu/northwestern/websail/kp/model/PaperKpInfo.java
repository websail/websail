package edu.northwestern.websail.kp.model;

/**
 * @author csbhagav on 2/22/15.
 */
public class PaperKpInfo {
    String paperId;
    Integer numKeyphrases;
    Integer numUniqueAnotators;

    public PaperKpInfo(String paperId, Integer numKeyphrases, Integer numUniqueAnotators) {
        this.paperId = paperId;
        this.numKeyphrases = numKeyphrases;
        this.numUniqueAnotators = numUniqueAnotators;
    }

    public String getPaperId() {
        return paperId;
    }

    public void setPaperId(String paperId) {
        this.paperId = paperId;
    }

    public Integer getNumKeyphrases() {
        return numKeyphrases;
    }

    public void setNumKeyphrases(Integer numKeyphrases) {
        this.numKeyphrases = numKeyphrases;
    }

    public Integer getNumUniqueAnotators() {
        return numUniqueAnotators;
    }

    public void setNumUniqueAnotators(Integer numUniqueAnotators) {
        this.numUniqueAnotators = numUniqueAnotators;
    }
}
