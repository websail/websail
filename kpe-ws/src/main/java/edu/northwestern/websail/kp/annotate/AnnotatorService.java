package edu.northwestern.websail.kp.annotate;

import edu.northwestern.websail.core.ws.model.Response;
import edu.northwestern.websail.core.ws.model.ResponseFactory;
import edu.northwestern.websail.kp.adapter.KpDataAdapter;
import edu.northwestern.websail.kp.context.GlobalResourceLoader;
import edu.northwestern.websail.kp.model.KeyPhrase;
import edu.northwestern.websail.kp.model.PaperKpInfo;
import edu.northwestern.websail.kp.model.RelatedKeyPhrase;
import edu.northwestern.websail.utils.MySQLQueryHandler;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

/**
 * @author csbhagav on 2/4/15.
 */

@Path("/kp/")

public class AnnotatorService {

    @Context
    ServletContext context;

    @Context
    HttpServletRequest request;

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/session")
    public Response getSession() {
        HttpSession s = request.getSession();
        System.out.println("obj=" + s);

        Enumeration enumeration = s.getAttributeNames();
        while (enumeration.hasMoreElements())

        {
            System.out.println(enumeration.nextElement());
        }

        System.out.println(s.getAttributeNames());
        System.out.println("id=" + s.getId());

        return ResponseFactory.ok(s.getId());
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/random")
    public Response getRandomPaperId() {

        MySQLQueryHandler handler = (MySQLQueryHandler) context.getAttribute(GlobalResourceLoader.MYSQL_HANDLER);
        Connection conn = handler.getConnection();
        String paperId;
        try {
            paperId = KpDataAdapter.getRandomPaperId(conn);
            return ResponseFactory.ok(paperId);
        } catch (SQLException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        }
    }


    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/find")
    public HashMap<String, Object> searchKp(@QueryParam("query") String query) {
        HashMap<String, Object> autoCompleteResponse = new HashMap<String, Object>();
        autoCompleteResponse.put("query", query);
        autoCompleteResponse.put("suggestions", new HashMap<String, String>());
        try {
            MySQLQueryHandler handler = (MySQLQueryHandler) context.getAttribute(GlobalResourceLoader.MYSQL_HANDLER);
            Connection conn = handler.getConnection();
            ArrayList<KeyPhrase> autoCompleteKps = KpDataAdapter.search(conn, query, true);

            autoCompleteResponse.put("suggestions", autoCompleteKps);

            return autoCompleteResponse;
        } catch (IOException e) {
            e.printStackTrace();
            return autoCompleteResponse;
        } catch (SQLException e) {
            e.printStackTrace();
            return autoCompleteResponse;
        }
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/findRelated/paperId/{paperId}/query/{query}/prevKeyId/{prevKeyId}/user/{user}")
    public Response getRelatedKps(@PathParam("paperId") String paperId, @PathParam("query") String query,
                                  @PathParam("prevKeyId") Integer prevKeyId, @PathParam("user") String user) {
        try {
            MySQLQueryHandler handler = (MySQLQueryHandler) context.getAttribute(GlobalResourceLoader.MYSQL_HANDLER);
            Connection conn = handler.getConnection();
            List<RelatedKeyPhrase> relatedKeyPhrases = KpDataAdapter.searchRelated(conn, paperId, query, prevKeyId,
                    user);
            if (relatedKeyPhrases.size() > 6) {
                relatedKeyPhrases = relatedKeyPhrases.subList(0, 6);
            }
            return ResponseFactory.ok(relatedKeyPhrases);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        }
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/add/paperId/{paperId}/surface/{surface}/user/{user}")
    public Response addKp(@PathParam("paperId") String paperId,
                          @PathParam("surface") String surface,
                          @PathParam("user") String user) {

        MySQLQueryHandler handler = (MySQLQueryHandler) context.getAttribute(GlobalResourceLoader.MYSQL_HANDLER);
        Connection conn = handler.getConnection();
        try {
            int kpId = KpDataAdapter.addKpForPaper(conn, paperId, surface, user);
            HashMap<String, Integer> values = new HashMap<String, Integer>();
            values.put("id", kpId);
            return ResponseFactory.ok(values);
        } catch (SQLException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        }
    }


    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/superclass/{surface}")
    public Response getSuperClasses(@PathParam("surface") String surface) {

        ArrayList<String> results = new ArrayList<String>();
        try {
            MySQLQueryHandler handler = (MySQLQueryHandler) context.getAttribute(GlobalResourceLoader.MYSQL_HANDLER);
            Connection conn = handler.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT kp2.surface FROM keyphrase kp , superclass s, " +
                    "keyphrase kp2 WHERE kp.surface = ? AND kp.id = s.surface_id AND s.super_id = kp2.id");
            ps.setString(1, surface);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(new String(rs.getBytes("surface"), "UTF-8"));
            }
            rs.close();
            ps.close();
            return ResponseFactory.ok(results);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        }
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/subclass/{surface}")
    public Response getSubClasses(@PathParam("surface") String surface) {

        ArrayList<String> results = new ArrayList<String>();
        try {
            MySQLQueryHandler handler = (MySQLQueryHandler) context.getAttribute(GlobalResourceLoader.MYSQL_HANDLER);
            Connection conn = handler.getConnection();

            PreparedStatement ps = conn.prepareStatement("SELECT kp2.surface FROM keyphrase kp , subclass s, " +
                    "keyphrase kp2 WHERE kp.surface = ? AND kp.id = s.surface_id AND s.sub_id = kp2.id");
            ps.setString(1, surface);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(new String(rs.getBytes("surface"), "UTF-8"));
            }
            rs.close();
            ps.close();
            return ResponseFactory.ok(results);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        }
    }


    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/addSuperClass/surface/{surface}/superClass/{superclass}")
    public Response addKpSuperclass(@PathParam("surface") String surface, @PathParam("superclass") String superclass) {

        MySQLQueryHandler handler = (MySQLQueryHandler) context.getAttribute(GlobalResourceLoader.MYSQL_HANDLER);
        Connection conn = handler.getConnection();
        try {
            KpDataAdapter.addKpSuperClass(conn, surface, superclass);
            return ResponseFactory.ok("Added " + superclass + " as superclass of " + surface);
        } catch (SQLException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        }
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/addSubClass/surface/{surface}/subClass/{subclass}")
    public Response addKpSubclass(@PathParam("surface") String surface, @PathParam("subclass") String subclass) {

        MySQLQueryHandler handler = (MySQLQueryHandler) context.getAttribute(GlobalResourceLoader.MYSQL_HANDLER);
        Connection conn = handler.getConnection();
        try {
            KpDataAdapter.addKpSubClass(conn, surface, subclass);
            return ResponseFactory.ok("Added " + subclass + " as subclass of " + surface);
        } catch (SQLException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        }
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/count/paper/{paperId}")
    public Response getPaperNumKps(@PathParam("paperId") String paperId) {

        MySQLQueryHandler handler = (MySQLQueryHandler) context.getAttribute(GlobalResourceLoader.MYSQL_HANDLER);
        Connection conn = handler.getConnection();
        try {
            int count = KpDataAdapter.getPaperKpCount(conn, paperId);
            return ResponseFactory.ok(count);
        } catch (SQLException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        }
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/all/paper/{paperId}/user/{user}")
    public Response getPaperKpByUser(@PathParam("paperId") String paperId, @PathParam("user") String user) {

        MySQLQueryHandler handler = (MySQLQueryHandler) context.getAttribute(GlobalResourceLoader.MYSQL_HANDLER);
        Connection conn = handler.getConnection();
        try {
            ArrayList<KeyPhrase> keyPhrases = KpDataAdapter.getPaperKpsByUser(conn, paperId, user);
            return ResponseFactory.ok(keyPhrases);
        } catch (SQLException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        }
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/add/paper/{paperId}/user/{user}/kpId/{kpId}/relatedKpId/{relKpId}/relType/{relType}")
    public Response addKpRelations(@PathParam("paperId") String paperId, @PathParam("user") String user,
                                   @PathParam("kpId") Integer kpId, @PathParam("relKpId") Integer relKpId,
                                   @PathParam("relType") String relType) {

        MySQLQueryHandler handler = (MySQLQueryHandler) context.getAttribute(GlobalResourceLoader.MYSQL_HANDLER);
        Connection conn = handler.getConnection();
        try {

            if (relType.equalsIgnoreCase("e") || relType.equalsIgnoreCase("p") || relType.equalsIgnoreCase("c") ||
                    relType.equalsIgnoreCase("none") || relType.equalsIgnoreCase("g")) {
                boolean done = KpDataAdapter.addRelations(conn, paperId, user, kpId, relKpId, relType.toUpperCase());
                if (!done) {
                    return ResponseFactory.error("Failed DB Insert");
                }
                return ResponseFactory.ok("");
            } else {
                return ResponseFactory.error("\"relType\" must be one of E | C | P | G | NONE");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        }
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/paperAnnInfo/user/{user}")
    public Response getPaperDetailsByUser(@PathParam("user") String user) {

        MySQLQueryHandler handler = (MySQLQueryHandler) context.getAttribute(GlobalResourceLoader.MYSQL_HANDLER);
        Connection conn = handler.getConnection();
        try {
            ArrayList<PaperKpInfo> paperKpInfoList = KpDataAdapter.getPaperDetailsByUser(conn, user);
            return ResponseFactory.ok(paperKpInfoList);
        } catch (SQLException e) {
            e.printStackTrace();
            return ResponseFactory.error(e.getMessage());
        }
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/removeKp/paper/{paperId}/user/{user}/kpId/{kpId}")
    public Response deletePaperKpForUser(@PathParam("paperId") String paperId, @PathParam("user") String user,
                                         @PathParam("kpId") Integer kpId) {

        MySQLQueryHandler handler = (MySQLQueryHandler) context.getAttribute(GlobalResourceLoader.MYSQL_HANDLER);
        Connection conn = handler.getConnection();
        try {
            KpDataAdapter.deletePaperKpByUser(conn, paperId, user, kpId);
            return ResponseFactory.ok("Delete from DB");
        } catch (SQLException e) {
            e.printStackTrace();
            return ResponseFactory.ok("Deletion from DB Failed");
        }
    }


}
