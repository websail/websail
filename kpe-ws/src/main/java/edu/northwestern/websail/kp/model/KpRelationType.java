package edu.northwestern.websail.kp.model;

/**
 * @author csbhagav on 2/22/15.
 */
public enum KpRelationType {
    P, // SUPER CLASS
    C, // SUB   CLASS
    E, // Equivalent CLASS
    G, // Good Keyphrase
    NONE
}
