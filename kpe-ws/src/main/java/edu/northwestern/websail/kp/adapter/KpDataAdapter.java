package edu.northwestern.websail.kp.adapter;

import edu.northwestern.websail.kp.model.KeyPhrase;
import edu.northwestern.websail.kp.model.KpRelationType;
import edu.northwestern.websail.kp.model.PaperKpInfo;
import edu.northwestern.websail.kp.model.RelatedKeyPhrase;
import edu.northwestern.websail.utils.MySQLQueryHandler;
import org.tartarus.snowball.ext.EnglishStemmer;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

enum KpHierarchy {
    SUPER,
    SUB
}

/**
 * @author csbhagav on 2/5/15.
 */
public class KpDataAdapter {
    private final static String VERY_LONG_STRING = "                                " +
            "                                            " +
            "                                             " +
            "                                             ";

    public static int getKpId(Connection conn, String surface) throws SQLException, UnsupportedEncodingException {

        PreparedStatement ps = conn.prepareStatement("SELECT * FROM keyphrase WHERE UPPER(surface) = ?");
        ps.setBytes(1, surface.toUpperCase().getBytes("UTF-8"));
        ResultSet rs = ps.executeQuery();
        int id = -1;
        while (rs.next()) {
            id = rs.getInt("id");
        }

        rs.close();
        ps.close();
        return id;
    }

    public static KeyPhrase getKp(Connection conn, Integer id) throws SQLException, UnsupportedEncodingException {

        PreparedStatement ps = conn.prepareStatement("SELECT * FROM keyphrase WHERE id = ?");
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();

        String surface = "";
        while (rs.next()) {
            surface = new String(rs.getBytes("surface"), "UTF-8");
        }

        rs.close();
        ps.close();
        return new KeyPhrase(id, surface);
    }

    public static void addKp(Connection conn, String surface) throws SQLException {
        PreparedStatement ps = conn.prepareStatement("INSERT IGNORE INTO keyphrase (surface) VALUES (?)");
        ps.setString(1, surface);
        ps.execute();
        ps.close();
    }

    public static int getOrAddKpId(Connection conn, String surface) throws SQLException, UnsupportedEncodingException {

        int id = getKpId(conn, surface);
        if (id == -1) {
            addKp(conn, surface);
            return getKpId(conn, surface);
        } else return id;
    }

    public static void addKpSuperClass(Connection conn, String surface, String superclass)
            throws SQLException, UnsupportedEncodingException {
        int kpId = KpDataAdapter.getOrAddKpId(conn, surface);
        int superClassId = KpDataAdapter.getOrAddKpId(conn, superclass);
        addKpHierarchy(conn, kpId, superClassId, KpHierarchy.SUPER);
    }

    public static void addKpSubClass(Connection conn, String surface, String subclass)
            throws SQLException, UnsupportedEncodingException {
        int kpId = KpDataAdapter.getOrAddKpId(conn, surface);
        int superClassId = KpDataAdapter.getOrAddKpId(conn, subclass);
        addKpHierarchy(conn, kpId, superClassId, KpHierarchy.SUB);
    }

    public static void addKpHierarchy(Connection conn, Integer kpId, Integer classId, KpHierarchy h) throws
            SQLException {

        String tableName = h == KpHierarchy.SUPER ? "superclass" : "subclass";

        PreparedStatement ps = conn.prepareStatement("INSERT INTO " + tableName + " VALUES (? , ? )");
        ps.setInt(1, kpId);
        ps.setInt(2, classId);
        ps.execute();
        ps.close();
    }

    public static String getRandomPaperId(Connection conn) throws SQLException {
        PreparedStatement ps = conn.prepareStatement("SELECT paperId FROM randomPapers ORDER BY RAND() LIMIT 1");
        ResultSet rs = ps.executeQuery();
        String paperId = "";
        while (rs.next()) {
            paperId = rs.getString("paperId");
        }
        rs.close();
        ps.close();
        return paperId;
    }

    public static int getPaperKpCount(Connection conn, String paperId) throws SQLException {

        PreparedStatement ps = conn.prepareCall("SELECT COUNT(DISTINCT kpId) as c FROM paperKp WHERE paperId = ?");
        ps.setString(1, paperId);
        ResultSet rs = ps.executeQuery();
        int count = 0;
        while (rs.next()) {
            count = rs.getInt("c");
        }

        rs.close();
        ps.close();
        return count;
    }

    public static ArrayList<KeyPhrase> getPaperKpsByUser(Connection conn, String paperId, String user) throws SQLException {

        ArrayList<KeyPhrase> kps = new ArrayList<KeyPhrase>();

        PreparedStatement ps = conn.prepareStatement("select k.id , k.surface from paperKp p, keyphrase k " +
                "WHERE p.paperId = ? AND p.user = ? AND p.kpId = k.id ");
        ps.setString(1, paperId);
        ps.setString(2, user);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            kps.add(new KeyPhrase(rs.getInt("id"), rs.getString("surface")));
        }
        ps.close();
        rs.close();
        return kps;
    }

    public static boolean addRelations(Connection conn, String paperId, String user, Integer kpId, Integer relKpId,
                                       String relType) throws SQLException {

        PreparedStatement ps = conn.prepareStatement("REPLACE INTO relatedKps VALUES(?, ? , ? , ? , ?)");
        ps.setString(1, paperId);
        ps.setString(2, user);
        ps.setInt(3, kpId);
        ps.setInt(4, relKpId);
        ps.setString(5, KpRelationType.valueOf(relType).toString());
        ps.execute();
        ps.close();
        return true;
    }

    public static ArrayList<KeyPhrase> search(Connection conn, String query, boolean prefixOnly) throws SQLException,
            UnsupportedEncodingException {
        ArrayList<KeyPhrase> results = new ArrayList<KeyPhrase>();
        PreparedStatement ps = conn.prepareStatement("SELECT id, surface FROM keyphrase WHERE UPPER(surface) like ?");
        if (prefixOnly) {
            ps.setString(1, query.toUpperCase() + "%");
        } else {
            ps.setString(1, "%" + query.toUpperCase() + "%");
        }

        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            results.add(new KeyPhrase(rs.getInt("id"), new String(rs.getBytes("surface"), "UTF-8")));
        }
        rs.close();
        ps.close();
        return results;
    }

    public static List<KeyPhrase> findRelated(Connection conn, String query) throws SQLException, UnsupportedEncodingException {
        List<KeyPhrase> results = new ArrayList<KeyPhrase>();
        ArrayList<Double> scores = new ArrayList<Double>();

        // TODO: improve this linear search and filter
        PreparedStatement ps = conn.prepareStatement("SELECT id, surface FROM keyphrase;");
        ResultSet rs = ps.executeQuery();
        EnglishStemmer stemmer = new EnglishStemmer();
        String[] qParts = query.split("\\s+");
        stem(qParts, stemmer);
        String longest = "";
        int longestId = -1;
        String shortest = VERY_LONG_STRING;
        int shortestId = -1;

        while (rs.next()) {
            String phrase = new String(rs.getBytes("surface"), "UTF-8");
            String[] pParts = phrase.split("\\s+");
            stem(pParts, stemmer);

            int count = countSameWords(qParts, pParts);
            if (count == 0) continue;
            int id = rs.getInt("id");
            if (phrase.length() > longest.length() && pParts.length < 6) {
                longest = phrase;
                longestId = id;
            }
            if (phrase.length() < shortest.length()) {
                shortest = phrase;
                shortestId = id;
            }
            results.add(new KeyPhrase(id, phrase));
            scores.add(count / (pParts.length * 1.0));
        }
        rs.close();
        ps.close();
        if (results.size() <= 4) return results;
        results = sortKPs(results, scores);
        int len = 4;
        boolean shortIn = false;
        boolean longIn = false;
        for (int i = 0; i < 4; i++) {
            if (results.get(i).getId() == shortestId) {
                shortIn = true;
                len++;
            }
            if (results.get(i).getId() == longestId) {
                longIn = true;
                len++;
            }
        }
        if (results.size() > len) results = results.subList(0, len);
        if (!shortIn) results.add(new KeyPhrase(shortestId, shortest));
        if (!longIn) results.add(new KeyPhrase(longestId, longest));
        Collections.shuffle(results);
        return results;
    }

    private static List<KeyPhrase> sortKPs(List<KeyPhrase> kps, ArrayList<Double> scores) {
        Integer[] orders = new Integer[scores.size()];
        for (int i = 0; i < orders.length; i++) {
            orders[i] = i;
        }
        ArrayIndexComparator comparator = new ArrayIndexComparator(scores);
        Arrays.sort(orders, comparator);

        ArrayList<KeyPhrase> sorted = new ArrayList<KeyPhrase>();
        for (Integer index : orders) {
            sorted.add(kps.get(index));
        }
        return sorted;
    }

    private static void stem(String[] parts, EnglishStemmer stemmer) {
        for (int i = 0; i < parts.length; i++) {
            stemmer.setCurrent(parts[i]);
            stemmer.stem();
            parts[i] = stemmer.getCurrent();
        }
    }

    private static int countSameWords(String[] qParts, String[] pParts) {
        int count = 0;
        for (String q : qParts) {
            for (String p : pParts) {
                if (q.equalsIgnoreCase(p)) count++;
            }
        }
        return count;
    }

    public static int addKpForPaper(Connection conn, String paperId, String surface, String user)
            throws UnsupportedEncodingException, SQLException {

        int kpId = KpDataAdapter.getOrAddKpId(conn, surface);
        PreparedStatement ps = conn.prepareStatement("INSERT IGNORE INTO paperKp VALUES(?, ? , ?)");
        ps.setString(1, paperId);
        ps.setInt(2, kpId);
        ps.setString(3, user);
        ps.execute();
        ps.close();
        return kpId;
    }

    public static ArrayList<PaperKpInfo> getPaperDetailsByUser(Connection conn, String user) throws SQLException {

        ArrayList<PaperKpInfo> paperInfoList = new ArrayList<PaperKpInfo>();
        PreparedStatement detailsPs = conn.prepareStatement("select count(kpId) as kpCount , count(DISTINCT user) as " +
                "userCount FROM paperKp where  paperId = ?");
        PreparedStatement paperIdPs = conn.prepareStatement("SELECT DISTINCT paperId FROM paperKp WHERE user = ?");
        paperIdPs.setString(1, user);
        ResultSet rs = paperIdPs.executeQuery();
        ResultSet detailsRs;
        while (rs.next()) {
            String paperId = rs.getString("paperId");

            detailsPs.setString(1, paperId);
            detailsRs = detailsPs.executeQuery();
            while (detailsRs.next()) {
                paperInfoList.add(new PaperKpInfo(paperId, detailsRs.getInt("kpCount"), detailsRs.getInt("userCount")));
            }
        }
        rs.close();
        paperIdPs.close();
        return paperInfoList;
    }

    public static void deletePaperKpByUser(Connection conn, String paperId, String user, Integer kpId) throws SQLException {


        PreparedStatement ps = conn.prepareStatement("DELETE FROM paperKp WHERE paperId = ? AND kpId = ? AND user = ?");
        ps.setString(1, paperId);
        ps.setInt(2, kpId);
        ps.setString(3, user);
        ps.execute();
        ps.close();
    }

    public static ArrayList<RelatedKeyPhrase> searchRelated(Connection conn, String paperId, String query, Integer prevKeyId, String user) throws UnsupportedEncodingException, SQLException {

        ArrayList<RelatedKeyPhrase> results = new ArrayList<RelatedKeyPhrase>();

        //ArrayList<KeyPhrase> relatedKps = KpDataAdapter.search(conn, query, false);
        List<KeyPhrase> relatedKps = KpDataAdapter.findRelated(conn, query);
        int kpId = KpDataAdapter.getOrAddKpId(conn, query);
        KeyPhrase mainKp = new KeyPhrase(kpId, query);

        if (prevKeyId != -1 && !isFoundInList(prevKeyId, results)) {
            KeyPhrase prevKp = KpDataAdapter.getKp(conn, prevKeyId);
            relatedKps.add(0, prevKp);
        }

        PreparedStatement relationPs = conn.prepareStatement("SELECT type FROM relatedKps " +
                "WHERE paperId = ? AND user = ? AND kpId = ? AND relKpId = ? ");
        ResultSet relationRs = null;

        for (KeyPhrase relatedKp : relatedKps) {
            relationPs.setString(1, paperId);
            relationPs.setString(2, user);
            relationPs.setInt(3, mainKp.getId());
            relationPs.setInt(4, relatedKp.getId());
            relationRs = relationPs.executeQuery();
            RelatedKeyPhrase relatedKeyPhrase = new RelatedKeyPhrase(mainKp, relatedKp, KpRelationType.NONE);

            while (relationRs.next()) {
                relatedKeyPhrase.setType(KpRelationType.valueOf(relationRs.getString("type").toUpperCase()));
            }
            results.add(relatedKeyPhrase);
        }
        relationPs.close();
        if (relationRs != null) relationRs.close();
        return results;
    }

    private static boolean isFoundInList(Integer prevKeyId, ArrayList<RelatedKeyPhrase> results) {
        for (RelatedKeyPhrase kp : results) {
            if (kp.getRelatedKp().getId() == prevKeyId) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) throws IllegalAccessException, ClassNotFoundException, InstantiationException, SQLException, UnsupportedEncodingException {
        String host = "downey-n2.cs.northwestern.edu";//config.getProperty("db.host");
        String db = "kpe";
        String user = "kgridUser";//config.getProperty("db.user");
        String pwd = "kgridPwd";//config.getProperty("db.password");
        MySQLQueryHandler handler = new MySQLQueryHandler(host, db, user,
                pwd);

        for (KeyPhrase kp : findRelated(handler.getConn(), "expression")) {
            System.out.println(kp.getSurface());
        }

        handler.getConn().close();
    }

    private static class ArrayIndexComparator implements Comparator<Integer> {
        private final ArrayList<Double> array;

        public ArrayIndexComparator(ArrayList<Double> array) {
            this.array = array;
        }

        @Override
        public int compare(Integer index1, Integer index2) {
            return array.get(index2).compareTo(array.get(index1));
        }
    }
}