package edu.northwestern.websail.kp.model;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author csbhagav on 2/19/15.
 */
public class KeyPhrase {
    @JsonProperty("data")
    int id;
    @JsonProperty("value")
    String surface;

    public KeyPhrase(int kpId, String surface) {
        this.id = kpId;
        this.surface = surface;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurface() {
        return surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }
}
