package edu.northwestern.websail.kp.model;

/**
 * @author csbhagav on 2/22/15.
 */
public class RelatedKeyPhrase {

    KeyPhrase mainKp;
    KeyPhrase relatedKp;
    KpRelationType type;


    public RelatedKeyPhrase(KeyPhrase mainKp, KeyPhrase relatedKp, KpRelationType type) {
        this.mainKp = mainKp;
        this.relatedKp = relatedKp;
        this.type = type;
    }

    public KeyPhrase getMainKp() {
        return mainKp;
    }

    public void setMainKp(KeyPhrase mainKp) {
        this.mainKp = mainKp;
    }

    public KeyPhrase getRelatedKp() {
        return relatedKp;
    }

    public void setRelatedKp(KeyPhrase relatedKp) {
        this.relatedKp = relatedKp;
    }

    public KpRelationType getType() {
        return type;
    }

    public void setType(KpRelationType type) {
        this.type = type;
    }
}
