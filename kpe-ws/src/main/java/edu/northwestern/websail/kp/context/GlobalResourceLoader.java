package edu.northwestern.websail.kp.context;

import edu.northwestern.websail.utils.MySQLQueryHandler;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.SQLException;

/**
 * @author csbhagav on 2/4/15.
 */
public class GlobalResourceLoader implements ServletContextListener {

    public static final String MYSQL_HANDLER = "mysql";

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        ServletContext context = servletContextEvent.getServletContext();
        //Properties config = new Properties();
        try {
            //config.load(this.getClass().getResourceAsStream("/config/mysql.config"));
            String host = "downey-n2.cs.northwestern.edu";//config.getProperty("db.host");
            String db = "kpe";
            String user = "kgridUser";//config.getProperty("db.user");
            String pwd = "kgridPwd";//config.getProperty("db.password");
            MySQLQueryHandler handler = new MySQLQueryHandler(host, db, user,
                    pwd);
            context.setAttribute(MYSQL_HANDLER, handler);

//        } catch (IOException e) {
//            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

        ServletContext context = servletContextEvent.getServletContext();
        MySQLQueryHandler handler = (MySQLQueryHandler) context.getAttribute(MYSQL_HANDLER);
        try {
            if (!handler.getConn().isClosed()) {
                handler.getConn().close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
