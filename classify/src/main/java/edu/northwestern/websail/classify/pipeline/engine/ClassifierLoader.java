package edu.northwestern.websail.classify.pipeline.engine;

import edu.northwestern.websail.classify.arch.ClassifierUnit;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;

import java.util.Properties;

/**
 * @author NorThanapon
 * @since 11/18/14
 */
@PipelineConfigInfo(
        requiredSettings = {"classifierLoader.model=env:*|(classifierLoader.classifier=*, classifierLoader.model=*)"},
        optionalSettings = {"classifierLoader.output=*"}
)
@PipelineBUTInfo()
public class ClassifierLoader extends ClassifierUnit {
    public ClassifierLoader() {
    }

    @Override
    protected String configPrefix() {
        return "classifierLoader";
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        return this.output(env, config);
    }
}
