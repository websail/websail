package edu.northwestern.websail.classify.pipeline.engine;

import edu.northwestern.websail.classify.arch.ClassifierUnit;
import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;

import java.util.Properties;

/**
 * @author NorThanapon
 * @since 11/18/14
 */
@PipelineConfigInfo(
        requiredSettings = {"classifierBuilder.classifier=*"},
        optionalSettings = {"classifierBuilder.output=*", "classifierBuilder.features=*"}
)
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class}
)
public class ClassifierBuilder extends ClassifierUnit {
    Instances instances;

    public ClassifierBuilder() {
    }

    @Override
    protected String configPrefix() {
        return "classifierBuilder";
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        for (BuckUnit unit : prev.units()) {
            if (unit.isNotOkay()) {
                continue;
            }
            Instances unitInstances = unit.get(ClassifyBUT.BuckInstances.class);
            if (instances == null) {
                instances = new Instances(unitInstances.getFeatures());
            }
            instances.merge(unitInstances);
        }
        if (instances == null) {
            logger.severe("No training instance.");
            return false;
        }
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        try {
            classifier.train(instances, fc);
        } catch (Exception e) {
            logger.severe("Unexpected error during training a classifier.");
            e.printStackTrace();
            return false;
        }
        return this.output(env, config);
    }
}
