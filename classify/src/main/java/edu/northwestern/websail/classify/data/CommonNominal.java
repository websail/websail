package edu.northwestern.websail.classify.data;

import edu.northwestern.websail.ds.map.BiMap;
import edu.northwestern.websail.ds.map.HashBiMap;

/**
 * @author NorThanapon
 * @since 11/19/14
 * <p/>
 * Do not modify element outside.
 */
public class CommonNominal {
    public final static BiMap<String, Double> boolMap = new HashBiMap<String, Double>();
    public final static double boolMapTrueValue = 1d;
    public final static double boolMapFalseValue = 0d;

    static {
        boolMap.put("true", boolMapTrueValue);
        boolMap.put("false", boolMapFalseValue);
    }
}
