package edu.northwestern.websail.classify.pipeline.engine;

import edu.northwestern.websail.classify.arch.RankerUnit;
import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 11/18/14
 */
@PipelineConfigInfo(
        requiredSettings = {"rankerBuilder.classifier=*"},
        optionalSettings = {"rankerBuilder.output=*", "rankerBuilder.features=*", "rankerBuilder.mergeUnitInstances={true|false}"}
)
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class}
)
public class RankerBuilder extends RankerUnit {
    List<Instances> instancesList;
    boolean mergeUnitInstances = false;
    public RankerBuilder() {
    }

    @Override
    protected String configPrefix() {
        return "rankerBuilder";
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config)) return false;
        instancesList = new ArrayList<Instances>();
        mergeUnitInstances = Boolean.parseBoolean(config.getProperty("rankerBuilder.mergeUnitInstances", "false"));
        return true;
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        Instances instances = null;
        for (BuckUnit unit : prev.units()) {
            if (unit.isNotOkay()) {
                continue;
            }
            Instances unitInstances = unit.get(ClassifyBUT.BuckInstances.class);
            if (mergeUnitInstances) {
                if (instances == null) {
                    instances = unitInstances;
                } else {
                    instances.merge(unitInstances);
                }
            } else {
                //System.out.println("Added Unit Instance for " + prev.getSource());
                instancesList.add(unitInstances);
            }
        }
        if (instances != null)
            instancesList.add(instances);
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        try {
            ranker.train(instancesList, fc);
        } catch (Exception e) {
            logger.severe("Unexpected error during training a classifier.");
            e.printStackTrace();
            return false;
        }
        return this.output(env, config);
    }
}
