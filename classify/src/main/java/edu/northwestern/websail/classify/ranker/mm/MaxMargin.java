package edu.northwestern.websail.classify.ranker.mm;

import edu.northwestern.websail.classify.arch.Ranker;
import edu.northwestern.websail.classify.data.*;
import edu.northwestern.websail.classify.ranker.RankerUtils;
import edu.northwestern.websail.classify.result.RankingResult;
import edu.northwestern.websail.ds.indexer.HashIndexer;
import edu.northwestern.websail.ds.indexer.Indexer;
import edu.northwestern.websail.ds.tuple.IntegerPair;
import edu.northwestern.websail.ds.tuple.IntegerTriplet;
import org.apache.commons.cli.*;
import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.CommonOps;

import java.io.Serializable;
import java.util.*;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author NorThanapon
 * @since 3/2/15
 * <p/>
 * AUC Max Margin Training
 * XXX: this only works with 1 output class, not multiclasses
 */
public class MaxMargin implements Ranker, Serializable {

    public static final Logger logger = Logger.getLogger(MaxMargin.class.getName());
    private static final long serialVersionUID = -6504641846593065207L;
    int patienceIncrease = 2;
    double improvementThreshold = 0.995;
    int minEpochs = 1;
    // model training hyper-parameters
    double initLearningRate = 0.13;
    double learningRate = 0.13;
    double fixedLearningRateIter = Double.POSITIVE_INFINITY;
    double learningRateUpdateAlpha = 1.0;
    double l2Coef = 0.0001;
    double l1Coef = 0.0;
    float validatePercent = 0.20f;
    double margin = 0.5;
    double dropOutPercent = 0.0;
    String maxMarginFunction = "edu.northwestern.websail.classify.ranker.mm.SigmoidUnit";
    // training stopping criteria
    int nEpochs = 1000;
    int miniBatchSize = 32;
    int patience = 5000;
    // feature mapping
    FeatureConfig config;
    Indexer<Feature> featureMap;
    MaxMarginFunction f;

    public MaxMargin() {
    }

    /**
     * Test method
     *
     * @param args is not used
     */
    public static void main(String[] args) {
        ConsoleHandler handler = new ConsoleHandler();
        MaxMargin.logger.setLevel(Level.FINE);
        handler.setLevel(Level.FINE);
        MaxMargin.logger.setUseParentHandlers(false);
        MaxMargin.logger.addHandler(handler);
        FeatureConfig featureConfig = new FeatureConfig();
        HashIndexer<Feature> features = new HashIndexer<Feature>();
        Feature fTestNumber = new Feature("test", "number", Feature.Type.NUMBER);
        Feature fTestNumber2 = new Feature("test", "number2", Feature.Type.NUMBER);
        Feature fTestNominal = new Feature("test", "nominal", Feature.Type.NOMINAL, CommonNominal.boolMap);
        Feature fTestClass = new Feature("test", "class", Feature.Type.NOMINAL, CommonNominal.boolMap);
        features.add(fTestNumber);
        features.add(fTestNumber2);
        features.add(fTestNominal);
        features.add(fTestClass);
        Random rand = new Random();
        int nInstancesList = 200;
        ArrayList<Instances> instancesList = new ArrayList<Instances>();
        for (int i = 0; i < nInstancesList; i++) {
            int nInstances = rand.nextInt(20);
            Instances instances = new Instances();
            instances.setFeatures(features);
            instances.setClassIndex(features.indexOf(fTestClass));
            for (int j = 0; j < nInstances; j++) {
                Instance<String> instance = new Instance<String>(features);
                boolean label = rand.nextBoolean();
                if (label) {
                    instance.setValue(fTestNominal, rand.nextBoolean() ? 1d : 0d);
                    instance.setValue(fTestNumber, rand.nextDouble());
                    instance.setValue(fTestNumber2, rand.nextDouble());
                    instance.setValue(fTestClass, 1d);
                } else {
                    instance.setValue(fTestNominal, rand.nextBoolean() ? 1d : 0d);
                    instance.setValue(fTestNumber, rand.nextDouble());
                    instance.setValue(fTestNumber2, -rand.nextDouble());
                    instance.setValue(fTestClass, 0d);
                }

                instances.getInstances().add(instance);
            }
            instancesList.add(instances);
        }
        MaxMargin mm = new MaxMargin();
        mm.init(new String[]{"-minEpochs", "100"});
        mm.train(instancesList, featureConfig);
        RankingResult rr = mm.rank(instancesList.get(0));
        System.out.println("Sample Output: ");
        for (Integer order : rr.getOrders()) {
            System.out.println(rr.getScores()[order] + ": " + rr.getInstances().getInstances().get(order).getValue(fTestClass));
        }
    }

    /**
     * Initialize a new MaxMargin ranker
     *
     * @param args as the options
     * @return true if every is okay
     */
    @Override
    public boolean init(String[] args) {
        processOptions(args);
        return true;
    }

    @Override
    public void train(List<Instances> instancesList, FeatureConfig config) {
        if (instancesList.size() == 0) {
            logger.warning("No training instance provided. Skip training.");
            return;
        }
        // create feature mapping
        this.config = config;
        featureMap = DataPointUtils.denseFeatureMap(instancesList.get(0).getFeatures(), config, instancesList.get(0).getClassIndex());
        f.init(featureMap.size() + 1, 1); //plus bias
        // create matrix that represent feature value + bias
        HashMap<IntegerPair, Integer> instMap = new HashMap<IntegerPair, Integer>(); //pair: (instances index, instance index) -> matrix index
        DenseMatrix64F allX = DataPointUtils.matrix(instancesList, featureMap, true, instMap);
        // sample training pairs and validating pairs (higher-rank (correct) and lower-rank (incorrect))
        List<IntegerTriplet> trainingPairs = new ArrayList<IntegerTriplet>(); // triplet: (instances index, higher instance index, lower instance index)
        List<IntegerTriplet> validatingPairs = new ArrayList<IntegerTriplet>();
        createSplit(instancesList, trainingPairs, validatingPairs, validatePercent);
        // create validating set
        int validationSize = validatingPairs.size();
        HashMap<IntegerPair, Integer> validatingInstMap = new HashMap<IntegerPair, Integer>();
        DenseMatrix64F validatingX = RankerUtils.subMatrix(allX, validatingPairs, instMap, validatingInstMap);
        // set up training
        int epoch = 0;
        int nBatches = trainingPairs.size() / miniBatchSize;
        //patience = trainingPairs.size();
        int validationFrequency = Math.min(nBatches, patience / 2);
        double bestValidationError = Double.POSITIVE_INFINITY;
        boolean doneLooping = false;
        MaxMarginFunction bestF = f.copy();
        updateLearningRate(0);
        //start training
        // \[T]/   Praise the Sun !
        //  |_|
        //  | |
        logger.info("Training: " + trainingPairs.size() + " pairs" +
                ", Validating: " + validatingPairs.size() + " pairs, Start training...");
        while (epoch < nEpochs && !doneLooping) {
            epoch++;
            updateLearningRate(epoch);
            Collections.shuffle(trainingPairs);
            double validationError;
            for (int iBatch = 0; iBatch < nBatches; iBatch++) {
                double trainingError = trainModel(trainingPairs, allX, iBatch, instMap);
                int iter = (epoch - 1) * nBatches + iBatch;
                // occasionally validate
                if ((iter + 1) % validationFrequency == 0 && validationSize > 0) {
                    validationError = validateModel(validatingPairs, validatingX, validatingInstMap);
                    logger.fine("Epoch: " + epoch + ", minibatch " + (iBatch + 1) + "/" + nBatches +
                            ", training error: " + trainingError +
                            ", validation error: " + validationError);
                    if (validationError < bestValidationError) {
                        // good update patience if significant
                        if (validationError < bestValidationError * improvementThreshold) {
                            patience = Math.max(patience, iter * patienceIncrease);
                        }
                        bestValidationError = validationError;
                        bestF = f.copy();
                    }
                }
                if (patience <= iter && epoch > minEpochs) {
                    // done
                    doneLooping = true;
                    break;
                }
            }
        }
        f = bestF;
        if (epoch == nEpochs) {
            logger.info("... reached max epoch.");
        }
        logger.info("Done training at epoch:" + epoch + "" +
                ", training error: " + validateModel(trainingPairs, allX, instMap) +
                ", best validation error: " + bestValidationError +
                "(" + bestValidationError * validationSize + ")" +
                "\n" + this.toString());
    }

    /**
     * Train model for a batch
     *
     * @param trainingPairs  is a list of triplet: (instances index, higher instance index, lower instance index)
     * @param allX           is a matrix containing all features + bias of all instances
     * @param miniBatchIndex is an index of minibatch to train
     * @param instMap        is a map of (instances index, instance index) -> matrix index
     * @return number of pairs which has a wrong ordering score
     */
    private double trainModel(List<IntegerTriplet> trainingPairs, DenseMatrix64F allX,
                              int miniBatchIndex, HashMap<IntegerPair, Integer> instMap) {
        int n = trainingPairs.size();
        int from = miniBatchIndex * miniBatchSize;
        int to = (miniBatchIndex + 1) * miniBatchSize;

        if (to > n) to = n;
        // subselect data from the matrix (all point in the pairs)
        List<IntegerTriplet> miniBPairs = trainingPairs.subList(from, to);
        HashMap<IntegerPair, Integer> miniBMap = new HashMap<IntegerPair, Integer>();
        DenseMatrix64F miniBX = RankerUtils.subMatrix(allX, miniBPairs, instMap, miniBMap);
        maskDownInput(miniBX);
        // compute activation
        DenseMatrix64F miniBY = f.compute(miniBX);
        DenseMatrix64F miniBG = new DenseMatrix64F(featureMap.size() + 1, 1);

        int marginErrorCount = 0;
        int errorCount = 0;

        for (IntegerTriplet pair : miniBPairs) {
            IntegerPair highPair = new IntegerPair(pair.getFirst(), pair.getSecond());
            int highIndex = miniBMap.get(highPair);
            int lowIndex = miniBMap.get(new IntegerPair(pair.getFirst(), pair.getThird()));

            double highY = miniBY.get(highIndex, 0);
            double lowY = miniBY.get(lowIndex, 0);
            if (lowY >= highY) errorCount++;
            // compute gradient if error
            // max(0, margin-f(x+,W)+f(x-,W))
            if (margin - highY + lowY <= 0) continue;
            // f'(x-,W)
            DenseMatrix64F subMiniBXLow = RankerUtils.subSelectRow(miniBX, lowIndex);
            DenseMatrix64F subMiniBYLow = RankerUtils.subSelectRow(miniBY, lowIndex);
            DenseMatrix64F grad = f.grad(subMiniBXLow, subMiniBYLow);
            // f'(x-,W) - f'(x+,W)
            DenseMatrix64F subMiniBXHigh = RankerUtils.subSelectRow(miniBX, highIndex);
            DenseMatrix64F subMiniBYHigh = RankerUtils.subSelectRow(miniBY, highIndex);
            CommonOps.subEquals(grad, f.grad(subMiniBXHigh, subMiniBYHigh));
            // sum gradients
            CommonOps.addEquals(miniBG, grad);
            marginErrorCount++;
        }
        if (marginErrorCount == 0) return 0;
        // update
        DenseMatrix64F miniBL2SqrG = f.gradL2Sqr();
        DenseMatrix64F miniBL1G = f.gradL1();
        // compute regularization
        CommonOps.scale(l2Coef * miniBPairs.size(), miniBL2SqrG);
        CommonOps.scale(l1Coef * miniBPairs.size(), miniBL1G);
        CommonOps.addEquals(miniBG, miniBL2SqrG);
        CommonOps.addEquals(miniBG, miniBL1G);
        // learning rate * average gradient
        CommonOps.scale(-learningRate / miniBPairs.size(), miniBG);
        f.update(miniBG);
        return ((double) errorCount) / miniBPairs.size();
    }

    /**
     * Compute validation error of the current model
     *
     * @param validatingPairs   is a list of triplet: (instances index, higher instance index, lower instance index)
     * @param validatingX       is a matrix containing all features + bias of validating instances
     * @param validatingInstMap is a map of (instances index, instance index) -> matrix index of validating instances
     * @return number of pairs which has a wrong ordering score
     */
    private double validateModel(List<IntegerTriplet> validatingPairs, DenseMatrix64F validatingX, HashMap<IntegerPair, Integer> validatingInstMap) {
        DenseMatrix64F validatingY = f.compute(validatingX);
        double error = 0;
        for (IntegerTriplet pair : validatingPairs) {
            int highIndex = validatingInstMap.get(new IntegerPair(pair.getFirst(), pair.getSecond()));
            int lowIndex = validatingInstMap.get(new IntegerPair(pair.getFirst(), pair.getThird()));
            double highY = validatingY.get(highIndex, 0);
            double lowY = validatingY.get(lowIndex, 0);
            //if (highY - lowY < margin)
            //error+=(margin - (highY - lowY));
            if (lowY >= highY)
                error++;
        }
        return error / validatingPairs.size();
    }

    @Override
    public RankingResult rank(Instances instances) {
        // create matrix that represent feature value + bias
        DenseMatrix64F allX = DataPointUtils.matrix(instances, featureMap, true);
        DenseMatrix64F allY = f.compute(allX);
        Double[] scores = new Double[allY.getNumRows()];
        Integer orders[] = new Integer[allY.getNumRows()];
        for (int i = 0; i < allY.getNumRows(); i++) {
            scores[i] = allY.get(i, 0);
            orders[i] = i;
        }
        ArrayIndexComparator comparator = new ArrayIndexComparator(scores);
        Arrays.sort(orders, comparator);
        return new RankingResult(instances, orders, scores);
    }

    public boolean processOptions(String[] args) {
        if (args.length == 1 && args[0] == null) {
            args[0] = "";
        }
        Options options = getOptions();
        CommandLineParser parser = new GnuParser();
        boolean argsNotOk = false;
        try {
            CommandLine cmd = parser.parse(options, args);
            setup(cmd);
            try {
                f = (MaxMarginFunction) Class.forName(maxMarginFunction).newInstance();
            } catch (InstantiationException e) {
                logger.severe(maxMarginFunction + " is not constructable (abstract, or no empty constructor).");
                e.printStackTrace();
                argsNotOk = true;
            } catch (IllegalAccessException e) {
                logger.severe(maxMarginFunction + " is null or does not have an empty constructor.");
                e.printStackTrace();
                argsNotOk = true;
            } catch (ClassNotFoundException e) {
                logger.severe(maxMarginFunction + " cannot be found.");
                e.printStackTrace();
                argsNotOk = true;
            }
        } catch (ParseException e) {
            argsNotOk = true;
        }
        if (argsNotOk) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("MaxMargin [options]", options);
        }
        return !argsNotOk;
    }

    protected Options getOptions() {
        Options options = new Options();
        options.addOption("learningRate", true, "Initial learning rate. Default: " + this.learningRate);
        options.addOption("l2sqr", true, "L2 Square regularization coefficient (ridge parameter). Default: " + l2Coef);
        options.addOption("l1", true, "L1 regularization (lasso). Default: " + l1Coef);
        options.addOption("validatePercent", true, "Percentage of training pairs removed for validation. Default: " + validatePercent);
        options.addOption("margin", true, "Margin between higher and lower rank instances for training. Default: " + margin);
        options.addOption("function", true, "MaxMarginFunction. Default: " + maxMarginFunction);
        options.addOption("epoch", true, "Number of epoches to train. Default: " + nEpochs);
        options.addOption("batchSize", true, "Number of pairs for each minibatch. Default: " + miniBatchSize);
        options.addOption("fixLearningRateIters", true, "Number of iterations that will not update the learning rate. Default: " + fixedLearningRateIter);
        options.addOption("learningRateAlpha", true, "rate <- rate/(iter^alpha). Default: " + learningRateUpdateAlpha);
        options.addOption("dropOut", true, "Dropout percentage (randomly make input to be zero). Default: " + dropOutPercent);
        options.addOption("patience", true, "Try <arg> interations after finding a significantly better model. Default: " + patience);
        options.addOption("patienceIncrease", true, "Increase patience by <arg> when an improvement found. Default: " + patienceIncrease);
        options.addOption("improvementThreshold", true, "Consider an improvement if (valid error / best valid error) > <arg>. Default: " + improvementThreshold);
        options.addOption("minEpochs", true, "Run for <arg> regardless of stopping criteria. Default: " + minEpochs);
        return options;
    }

    protected void setup(CommandLine cmd) {
        initLearningRate = Double.parseDouble(cmd.getOptionValue("learningRate", this.learningRate + ""));
        l2Coef = Double.parseDouble(cmd.getOptionValue("l2sqr", this.l2Coef + ""));
        l1Coef = Double.parseDouble(cmd.getOptionValue("l1", this.l1Coef + ""));
        validatePercent = Float.parseFloat(cmd.getOptionValue("validatePercent", this.validatePercent + ""));
        margin = Double.parseDouble(cmd.getOptionValue("margin", this.margin + ""));
        nEpochs = Integer.parseInt(cmd.getOptionValue("epoch", this.nEpochs + ""));
        miniBatchSize = Integer.parseInt(cmd.getOptionValue("batchSize", this.miniBatchSize + ""));
        maxMarginFunction = cmd.getOptionValue("function", this.maxMarginFunction);
        fixedLearningRateIter = Double.parseDouble(cmd.getOptionValue("fixLearningRateIters", this.fixedLearningRateIter + ""));
        learningRateUpdateAlpha = Double.parseDouble(cmd.getOptionValue("learningRateAlpha", this.learningRateUpdateAlpha + ""));
        dropOutPercent = Double.parseDouble(cmd.getOptionValue("dropOut", this.dropOutPercent + ""));
        patience = Integer.parseInt(cmd.getOptionValue("patience", this.patience + ""));
        patienceIncrease = Integer.parseInt(cmd.getOptionValue("patienceIncrease", this.patienceIncrease + ""));
        improvementThreshold = Double.parseDouble(cmd.getOptionValue("improvementThreshold", this.improvementThreshold + ""));
        minEpochs = Integer.parseInt(cmd.getOptionValue("minEpochs", this.minEpochs + ""));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Feature Map:\n----------------------------------------\n");
        for (int i = 0; i < featureMap.size(); i++) {
            sb.append(i).append(": ").append(featureMap.get(i).fullName()).append("\n");
        }
        sb.append(featureMap.size())
                .append(": bias\n========================================\nParameters:\n----------------------------------------\n");
        sb.append(this.f.toString());
        sb.append("========================================");
        return sb.toString();
    }

    private void createSplit(List<Instances> instancesList, List<IntegerTriplet> trainingPairs, List<IntegerTriplet> validatingPairs, double validationPercent) {
        Random random = new Random();
        for (int k = 0; k < instancesList.size(); k++) {
            List<IntegerTriplet> pairs = RankerUtils.createAllPairs(instancesList.get(k), k);
            for (IntegerTriplet pair : pairs) {
                if (random.nextDouble() < validationPercent) {
                    validatingPairs.add(pair);
                } else {
                    trainingPairs.add(pair);
                }
            }
        }
    }

    protected void maskDownInput(DenseMatrix64F miniBX) {
        if (dropOutPercent == 0.0) return;
        Random rand = new Random();
        int len = miniBX.getNumCols();
        HashSet<Integer> markColumns = new HashSet<Integer>();
        while (markColumns.size() < (len - 1) * dropOutPercent) {
            markColumns.add(rand.nextInt(len - 1));
        }
        for (Integer j : markColumns) {
            for (int i = 0; i < miniBX.getNumRows(); i++) {
                miniBX.set(i, j, 0);
            }
        }
    }

    protected void updateLearningRate(int epoch) {
        if (epoch < this.fixedLearningRateIter) learningRate = initLearningRate;
        learningRate = initLearningRate / Math.pow(epoch, learningRateUpdateAlpha);
    }

    protected static class ArrayIndexComparator implements Comparator<Integer> {
        private final Double[] array;

        public ArrayIndexComparator(Double[] array) {
            this.array = array;
        }

        @Override
        public int compare(Integer index1, Integer index2) {
            return array[index2].compareTo(array[index1]);
        }
    }

}
