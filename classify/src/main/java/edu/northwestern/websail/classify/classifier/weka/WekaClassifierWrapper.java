package edu.northwestern.websail.classify.classifier.weka;

import edu.northwestern.websail.classify.arch.Classifier;
import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.FeatureConfig;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.classify.result.ClassificationResult;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import weka.core.FastVector;
import weka.core.converters.ArffSaver;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 * @since 11/15/14
 */
public class WekaClassifierWrapper implements Classifier {
    public static final Logger logger = Logger.getLogger(WekaClassifierWrapper.class.getName());
    private static final long serialVersionUID = 9127496222181129276L;
    protected FastVector attrs;
    protected weka.classifiers.Classifier classifier;
    protected int classIndex;

    /**
     * Need to call init(String[] args) afterward
     */
    public WekaClassifierWrapper() {
    }

    /**
     * Create a wrapper with args
     *
     * @param args: <code>args[0]</code> is the target weka's classifier full class name. <code>args[1]</code> is the option (optional).
     */
    public WekaClassifierWrapper(String[] args) {
        this.init(args);
    }

    /**
     * Init a wrapper with args
     *
     * @param args: <code>args[0]</code> is the target weka's classifier full class name. <code>args[1]</code> is the option (optional).
     */
    @Override
    public boolean init(@NotNull String[] args) {
        return this.init(args[0], args.length > 1 ? args[1] : null);
    }

    private boolean init(@NotNull String classifierName, @Nullable String options) {
        try {
            this.classifier = (weka.classifiers.Classifier) Class.forName(classifierName).newInstance();
        } catch (InstantiationException e) {
            logger.severe(classifierName + " is not constructable (abstract, or no empty constructor).");
            e.printStackTrace();
            return false;
        } catch (IllegalAccessException e) {
            logger.severe(classifierName + " is null or does not have an empty constructor.");
            e.printStackTrace();
            return false;
        } catch (ClassNotFoundException e) {
            logger.severe(classifierName + " cannot be found.");
            e.printStackTrace();
            return false;
        }
        if (options != null) {
            try {
                this.classifier.setOptions(options.split(" "));
            } catch (Exception e) {
                logger.severe(options + " is invalid.");
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void train(Instances instances, FeatureConfig config) throws Exception {
        this.attrs = WekaUtil.attrs(instances.getFeatures(), config);
        this.classIndex = instances.getClassIndex();
        weka.core.Instances dataset = WekaUtil.dataset(instances, attrs);
        for (Instance<Object> instance : instances.getInstances()) {
            dataset.add(WekaUtil.datapoint(dataset, instance));
        }
        this.classifier.buildClassifier(dataset);
        logger.info(this.classifier.toString());
        if (logger.getLevel() != null && logger.getLevel().intValue() <= Level.FINE.intValue()) {
            ArffSaver saver = new ArffSaver();
            saver.setInstances(dataset);
            saver.setFile(new File("train.arff"));
            saver.setDestination(new File("train.arff"));
            saver.writeBatch();
        }
    }

    @Override
    public <T> ClassificationResult classify(Instance<T> instance) throws Exception {
        weka.core.Instance datapoint = WekaUtil.datapoint(this.attrs, instance, this.classIndex);
        double dist[] = this.classifier.distributionForInstance(datapoint);
        double value = Instance.MISSING;
        double score = Instance.MISSING;
        Feature classFeature = instance.getFeatures().get(this.classIndex);
        if (classFeature.getType() == Feature.Type.NUMBER) {
            value = dist[0];
            score = dist[0];
        } else if (classFeature.getType() == Feature.Type.NOMINAL) {
            double max = 0;
            int maxIndex = 0;
            for (int i = 0; i < dist.length; i++) {
                if (dist[i] > max) {
                    maxIndex = i;
                    max = dist[i];
                }
            }
            if (max > 0) {
                String nominalValue = datapoint.classAttribute().value(maxIndex);
                value = classFeature.getNominalMap().getValue(nominalValue);
                score = dist[maxIndex];
            }
        }

        return new ClassificationResult(instance, classFeature, value, score);
    }

    public FastVector getAttrs() {
        return attrs;
    }

    public weka.classifiers.Classifier getClassifier() {
        return classifier;
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.writeObject(attrs);
        oos.writeObject(classIndex);
        try {
            saveWekaModel(oos);
        } catch (Exception e) {
            logger.severe("Unexpected error while writing weka's classifer.");
            e.printStackTrace();
        }
    }

    private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
        attrs = (FastVector) ois.readObject();
        classIndex = (Integer) ois.readObject();
        try {
            classifier = loadWekaModel(ois);
        } catch (Exception e) {
            logger.severe("Unexpected error while loading weka's classifer.");
            e.printStackTrace();
        }
    }

    protected void saveWekaModel(ObjectOutputStream out) throws Exception {
        //SerializationHelper.write(out, classifier);
        out.writeObject(classifier);
    }

    protected weka.classifiers.Classifier loadWekaModel(ObjectInputStream in) throws Exception {
        return (weka.classifiers.Classifier) in.readObject(); //SerializationHelper.read(in);
    }


}
