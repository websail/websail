package edu.northwestern.websail.classify.pipeline.filter;

import edu.northwestern.websail.classify.arch.FeatureFilter;
import edu.northwestern.websail.classify.data.CommonNominal;
import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.ds.indexer.Indexer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 3/20/15
 */
@PipelineConfigInfo(
        requiredSettings = {"discreteFeature.targets"},
        optionalSettings = {"discreteFeature.numBins"}
)
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class}
)
public class DiscreteFeature extends FeatureFilter {

    private static final HashMap<Feature, Double[]> globalMinMaxMap = new HashMap<Feature, Double[]>();
    private static final HashMap<Feature, Double[]> globalRanges = new HashMap<Feature, Double[]>();
    private static final HashMap<Feature, Feature[]> globalFeatureMap = new HashMap<Feature, Feature[]>();
    private static final Object lock = new Object();
    private static boolean computed = false;
    private final HashMap<Feature, Double[]> localMinMaxMap;
    public int numBins = 5;

    public DiscreteFeature() {
        localMinMaxMap = new HashMap<Feature, Double[]>();
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config)) return false;
        numBins = Integer.parseInt(config.getProperty(prefixConfig() + ".numBins", "5"));
        return true;
    }

    @Override
    protected String prefixConfig() {
        return "discreteFeature";
    }

    @Override
    protected void collect() {
        for (Instances instances : instancesList) {
            for (Instance instance : instances.getInstances()) {
                for (Feature f : targets) {
                    double fv = instance.getValue(f);
                    if (!localMinMaxMap.containsKey(f)) {
                        localMinMaxMap.put(f, new Double[]{fv, fv});
                    } else {
                        Double[] minMax = localMinMaxMap.get(f);
                        if (minMax[0] > fv) {
                            minMax[0] = fv;
                        }
                        if (minMax[1] < fv) {
                            minMax[1] = fv;
                        }
                    }
                }
            }
        }
        for (Map.Entry<Feature, Double[]> p : localMinMaxMap.entrySet())
            logger.fine("Local@" + threadId + ", " + p.getKey() + ": " + Arrays.toString(p.getValue()));
    }

    @Override
    protected void exchange() {
        synchronized (lock) {
            for (Feature f : targets) {
                if (!globalMinMaxMap.containsKey(f)) {
                    globalMinMaxMap.put(f, localMinMaxMap.get(f));
                } else {
                    Double[] localMinMax = localMinMaxMap.get(f);
                    Double[] globalMinMax = globalMinMaxMap.get(f);
                    if (globalMinMax[0] > localMinMax[0]) {
                        globalMinMax[0] = localMinMax[0];
                    }
                    if (globalMinMax[1] < localMinMax[1]) {
                        globalMinMax[1] = localMinMax[1];
                    }
                }
            }
        }
        for (Map.Entry<Feature, Double[]> p : globalMinMaxMap.entrySet())
            logger.fine("Local@" + threadId + ", " + p.getKey() + ": " + Arrays.toString(p.getValue()));
    }

    @Override
    protected boolean update() {
        computeRangesAndMakeFeatures();
        Indexer<Feature> featureIndexer = null;
        for (Instances instances : instancesList) {
            featureIndexer = register(instances, featureIndexer);
            for (Instance instance : instances.getInstances()) {
                for (Feature f : targets) {
                    double fv = instance.getValue(f);
                    int bin = bin(fv, globalRanges.get(f));
                    for (int i = 0; i < numBins; i++) {
                        instance.setValue(globalFeatureMap.get(f)[i],
                                bin == i ? CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue);
                    }
                }
            }
        }
        return true;
    }

    private void computeRangesAndMakeFeatures() {
        synchronized (lock) {
            if (computed) return;
            for (Feature f : targets) {
                Double[] minMax = globalMinMaxMap.get(f);
                Double[] fRanges = new Double[numBins - 1];
                Feature[] features = new Feature[numBins];
                double r = (minMax[1] - minMax[0]) / numBins;
                for (int i = 1; i <= fRanges.length; i++) {
                    fRanges[i - 1] = minMax[0] + (r * i);
                }
                for (int i = 0; i < numBins; i++) {
                    features[i] = new Feature(f.getGroup(), f.getName() + "_bin_" + i,
                            Feature.Type.NOMINAL, CommonNominal.boolMap);
                }
                globalRanges.put(f, fRanges);
                globalFeatureMap.put(f, features);
            }
            for (Map.Entry<Feature, Double[]> p : globalRanges.entrySet())
                logger.fine("Range, " + p.getKey() + ": " + Arrays.toString(p.getValue()));
            computed = true;
        }
    }

    private Indexer<Feature> register(Instances instances, Indexer<Feature> featureIndexer) {
        if (featureIndexer == null || featureIndexer != instances.getFeatures()) {
            featureIndexer = instances.getFeatures();
            synchronized (lock) {
                for (Map.Entry<Feature, Feature[]> p : globalFeatureMap.entrySet()) {
                    for (Feature f : p.getValue())
                        instances.getFeatures().add(f);
                }
            }
        }
        return featureIndexer;
    }

    private int bin(double fv, Double[] ranges) {
        int bin = 0;
        for (Double d : ranges) {
            if (fv < d) return bin;
            bin++;
        }
        return bin;
    }
}
