package edu.northwestern.websail.classify.classifier.weka;

import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.FeatureConfig;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.ds.indexer.Indexer;
import weka.core.Attribute;
import weka.core.FastVector;

/**
 * @author NorThanapon
 * @since 11/15/14
 * <p/>
 * Convert websail-classify's instance to weka's instance
 * nominal value will be handled by weka
 */
public class WekaUtil {
    public static FastVector attrs(Indexer<Feature> features, FeatureConfig config) {
        FastVector attrs = new FastVector(features.size());
        for (int i = 0; i < features.size(); i++) {
            Feature feature = features.get(i);
            if (config.notAllow(feature)) continue;
            if (feature.getType() == Feature.Type.NUMBER) {
                Attribute attr = new Attribute(feature.fullName());
                attrs.addElement(attr);
            } else if (feature.getType() == Feature.Type.NOMINAL) {
                FastVector nominalValues = new FastVector(feature.getNominalMap().size());
                for (Object nominalValue : feature.getNominalMap().keySet()) {
                    nominalValues.addElement(nominalValue);
                }
                Attribute attr = new Attribute(feature.fullName(), nominalValues);
                attrs.addElement(attr);
            }
        }
        return attrs;
    }

    public static weka.core.Instances dataset(Instances instances, FeatureConfig config) {
        FastVector attrs = attrs(instances.getFeatures(), config);
        return dataset(instances, attrs);
    }

    public static weka.core.Instances dataset(Instances instances, FastVector attrs) {
        weka.core.Instances dataset = new weka.core.Instances("", attrs, instances.getInstances().size());
        dataset.setClassIndex(instances.getClassIndex());
        return dataset;
    }

    public static <T> weka.core.Instance datapoint(weka.core.Instances dataset, Instance<T> instance) {
        weka.core.Instance datapoint = new weka.core.Instance(dataset.numAttributes());
        datapoint.setDataset(dataset);
        for (int i = 0; i < dataset.numAttributes(); i++) {
            Attribute attr = dataset.attribute(i);
            Feature feature = instance.getFeatures().get(new Feature(attr.name()));
            if (feature.getType() == Feature.Type.NOMINAL) {
                String value = instance.getNominalValue(feature);
                datapoint.setValue(i, value);
            } else {
                double value = instance.getValue(feature);
                datapoint.setValue(i, value);
            }
        }
        return datapoint;
    }

    public static <T> weka.core.Instance datapoint(FastVector attrs, Instance<T> instance, int classIndex) {
        weka.core.Instances dataset = new weka.core.Instances("", attrs, 1);
        dataset.setClassIndex(classIndex);
        return datapoint(dataset, instance);
    }
}
