package edu.northwestern.websail.classify.pipeline.engine;

import edu.northwestern.websail.classify.arch.RankerUnit;
import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.classify.result.RankingResult;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;

import java.util.Properties;

/**
 * @author NorThanapon
 * @since 11/18/14
 */
@PipelineConfigInfo(
        requiredSettings = {"rankerRunner.model=env:*|(rankerRunner.classifier=*, rankerRunner.model=*)"},
        optionalSettings = {"rankerRunner.mergeUnitInstances={true|false}"}
)
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class},
        produces = {ClassifyBUT.RankingResults.class}
)
public class RankerRunner extends RankerUnit {

    boolean mergeUnitInstances = false;

    public RankerRunner() {
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config)) return false;
        mergeUnitInstances = Boolean.parseBoolean(config.getProperty("rankerRunner.mergeUnitInstances", "false"));
        return true;
    }

    @Override
    protected String configPrefix() {
        return "rankerRunner";
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        boolean oneSuccess = false;
        Instances instances = null;
        for (BuckUnit unit : prev.units()) {
            if (unit.isNotOkay())
                continue;
            Instances unitInstances = unit.get(ClassifyBUT.BuckInstances.class);
            if (mergeUnitInstances) {
                if (instances == null) {
                    instances = unitInstances;
                } else {
                    instances.merge(unitInstances);
                }
            } else {
                try {
                    RankingResult result = this.ranker.rank(unitInstances);
                    unit.set(ClassifyBUT.RankingResults.class, result);
                    oneSuccess = true;
                } catch (Exception e) {
                    logger.severe("Unexpected error while ranking instances");
                    e.printStackTrace();
                    unit.setOkay(false);
                }
            }
        }
        if (mergeUnitInstances) {
            try {
                RankingResult result = this.ranker.rank(instances);
                for (BuckUnit unit : prev.units()) {
                    unit.set(ClassifyBUT.RankingResults.class, result);
                }
                oneSuccess = true;
            } catch (Exception e) {
                logger.severe("Unexpected error while ranking instances");
                e.printStackTrace();
                for (BuckUnit unit : prev.units()) {
                    unit.setOkay(false);
                }
            }
        }
        return oneSuccess;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        return true;
    }
}
