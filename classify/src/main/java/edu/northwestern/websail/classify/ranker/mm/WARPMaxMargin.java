package edu.northwestern.websail.classify.ranker.mm;

import edu.northwestern.websail.classify.data.*;
import edu.northwestern.websail.classify.ranker.RankerUtils;
import edu.northwestern.websail.classify.result.RankingResult;
import edu.northwestern.websail.ds.indexer.HashIndexer;
import edu.northwestern.websail.ds.tuple.IntegerPair;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.CommonOps;

import java.io.Serializable;
import java.util.*;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;

/**
 * @author NorThanapon
 * @since 4/21/15
 * <p/>
 * Weighted Approximately Ranked Pairwise loss (WARP)
 */
public class WARPMaxMargin extends MaxMargin implements Serializable {

    private static final long serialVersionUID = -8028885798750542841L;
    int WARPK = -1;
    // training cache
    HashMap<Integer, ArrayList<Integer>> negativeIndexes = null;

    public WARPMaxMargin() {
    }

    /**
     * Test method
     *
     * @param args is not used
     */
    public static void main(String[] args) {
        ConsoleHandler handler = new ConsoleHandler();
        MaxMargin.logger.setLevel(Level.FINE);
        handler.setLevel(Level.FINE);
        MaxMargin.logger.setUseParentHandlers(false);
        MaxMargin.logger.addHandler(handler);
        FeatureConfig featureConfig = new FeatureConfig();
        HashIndexer<Feature> features = new HashIndexer<Feature>();
        Feature fTestNumber = new Feature("test", "number", Feature.Type.NUMBER);
        Feature fTestNumber2 = new Feature("test", "number2", Feature.Type.NUMBER);
        Feature fTestNominal = new Feature("test", "nominal", Feature.Type.NOMINAL, CommonNominal.boolMap);
        Feature fTestClass = new Feature("test", "class", Feature.Type.NOMINAL, CommonNominal.boolMap);
        features.add(fTestNumber);
        features.add(fTestNumber2);
        features.add(fTestNominal);
        features.add(fTestClass);
        Random rand = new Random();
        int nInstancesList = 67;
        ArrayList<Instances> instancesList = new ArrayList<Instances>();
        for (int i = 0; i < nInstancesList; i++) {
            int nInstances = rand.nextInt(20);
            Instances instances = new Instances();
            instances.setFeatures(features);
            instances.setClassIndex(features.indexOf(fTestClass));
            for (int j = 0; j < nInstances; j++) {
                Instance<String> instance = new Instance<String>(features);
                boolean label = rand.nextBoolean();
                if (label) {
                    instance.setValue(fTestNominal, rand.nextBoolean() ? 1d : 0d);
                    instance.setValue(fTestNumber, rand.nextDouble());
                    instance.setValue(fTestNumber2, rand.nextDouble());
                    instance.setValue(fTestClass, 1d);
                } else {
                    instance.setValue(fTestNominal, rand.nextBoolean() ? 1d : 0d);
                    instance.setValue(fTestNumber, rand.nextDouble());
                    instance.setValue(fTestNumber2, -rand.nextDouble());
                    instance.setValue(fTestClass, 0d);
                }

                instances.getInstances().add(instance);
            }
            instancesList.add(instances);
        }
        WARPMaxMargin mm = new WARPMaxMargin();
        mm.init("-learningRate,0.13,-l2sqr,0.00001,-margin,0.5,-batchSize,1".split(","));
        mm.train(instancesList, featureConfig);
        int badRankInstances = 0;
        for (Instances insts : instancesList) {
            RankingResult rr = mm.rank(insts);
            //System.out.println("Sample Output: ");
            double minGoodScore = Double.MAX_VALUE;
            double maxBadScore = Double.MIN_VALUE;
            if (insts.getInstances().size() < 2) continue;
            for (Integer order : rr.getOrders()) {
                double label = rr.getInstances().getInstances().get(order).getValue(fTestClass);
                double score = rr.getScores()[order];
                if (label == CommonNominal.boolMapTrueValue) {
                    if (score < minGoodScore) {
                        minGoodScore = score;
                    }
                } else {
                    if (score > maxBadScore) {
                        maxBadScore = score;
                    }
                }
            }
            if (mm.margin - minGoodScore + maxBadScore > 0) {
                badRankInstances++;
//                for (Integer order : rr.getOrders()) {
//                    System.out.println(rr.getScores()[order] + ": " + rr.getInstances().getInstances().get(order).getValue(fTestClass));
//                }
//                System.out.println();
            }
        }
        System.out.println("Number of instanes with error: " + badRankInstances);

    }

    @Override
    public void train(List<Instances> instancesList, FeatureConfig config) {
        if (instancesList.size() == 0) {
            logger.warning("No training instance provided. Skip training.");
            return;
        }
        // create feature mapping
        this.config = config;
        featureMap = DataPointUtils.denseFeatureMap(instancesList.get(0).getFeatures(), config, instancesList.get(0).getClassIndex());
        f.init(featureMap.size() + 1, 1); //plus bias
        // sample (all) positive instances
        // pair: (instances index, instance index) -> matrix index
        initWARP(instancesList, CommonNominal.boolMapTrueValue);
        List<IntegerPair> positiveInstances = new ArrayList<IntegerPair>();
        HashMap<IntegerPair, Integer> instMap = new HashMap<IntegerPair, Integer>();
        DenseMatrix64F allX = DataPointUtils.matrix(instancesList, featureMap, true, instMap, positiveInstances);
        // split data set
        List<IntegerPair> trainingInstances = new ArrayList<IntegerPair>();
        List<IntegerPair> validatingInstances = new ArrayList<IntegerPair>();
        createSplit(positiveInstances, trainingInstances, validatingInstances, validatePercent);
        // set up training
        int epoch = 0;
        int nBatches = trainingInstances.size() / miniBatchSize;
        //patience = trainingInstances.size();
        int validationFrequency = Math.min(nBatches, patience / 2);
        double bestValidationError = Double.POSITIVE_INFINITY;
        boolean doneLooping = false;
        MaxMarginFunction bestF = f.copy();
        updateLearningRate(0);
        logger.info("Training: " + trainingInstances.size() +
                ", Validating: " + validatingInstances.size() +
                ", Start training...");
        while (epoch < nEpochs && !doneLooping) {
            epoch++;
            updateLearningRate(epoch);
            Collections.shuffle(trainingInstances);
            double validationError;
            for (int iBatch = 0; iBatch < nBatches; iBatch++) {
                double trainingError = trainModel(trainingInstances, allX, iBatch, instMap, instancesList);
                int iter = (epoch - 1) * nBatches + iBatch;

                // occasionally validate
                if ((iter + 1) % validationFrequency == 0 && validatingInstances.size() > 0) {
                    validationError = validateModel(validatingInstances, allX, instMap, instancesList);
                    logger.fine("Epoch: " + epoch + ", minibatch " + (iBatch + 1) + "/" + nBatches +
                            ", training approx loss: " + trainingError +
                            ", validation loss: " + validationError);
                    if (validationError < bestValidationError) {
                        // good update patience if significant
                        if (validationError < bestValidationError * improvementThreshold) {
                            patience = Math.max(patience, iter * patienceIncrease);
                        }
                        bestValidationError = validationError;
                        bestF = f.copy();
                    }
                }
                if (patience <= iter && epoch > minEpochs) {
                    // done
                    doneLooping = true;
                    break;
                }
            }
        }
        f = bestF;
        if (epoch == nEpochs) {
            logger.info("... reached max epoch.");
        }
        logger.info("Done training at epoch:" + epoch + "" +
                ", training average rank: " + validateModel(trainingInstances, allX, instMap, instancesList) +
                ", best validation average rank: " + bestValidationError +
                "\n" + this.toString());
        clearWARP();
    }

    private double trainModel(List<IntegerPair> trainingInstances, DenseMatrix64F allX,
                              int miniBatchIndex, HashMap<IntegerPair, Integer> instMap,
                              List<Instances> instancesList) {
        int n = trainingInstances.size();
        int from = miniBatchIndex * miniBatchSize;
        int to = (miniBatchIndex + 1) * miniBatchSize;

        if (to > n) to = n;
        // subselect data from the matrix (all point in the pairs)
        List<IntegerPair> miniBPairs = trainingInstances.subList(from, to);
        HashMap<IntegerPair, Integer> miniBMap = new HashMap<IntegerPair, Integer>();
        DenseMatrix64F miniBX = RankerUtils.subMatrixPair(allX, miniBPairs, instMap, miniBMap);
        maskDownInput(miniBX);
        // compute activation
        DenseMatrix64F miniBY = f.compute(miniBX);
        DenseMatrix64F miniBG = new DenseMatrix64F(featureMap.size() + 1, 1);

        int lossErrorCount = 0;
        double loss = 0.0;
        // for each positive instance
        for (IntegerPair pair : miniBPairs) {
            int highIndex = miniBMap.get(pair);
            double highY = miniBY.get(highIndex, 0);
            DenseMatrix64F subMiniBXHigh = RankerUtils.subSelectRow(miniBX, highIndex);
            DenseMatrix64F subMiniBYHigh = RankerUtils.subSelectRow(miniBY, highIndex);
            DenseMatrix64F subMiniBXLow = new DenseMatrix64F(1, miniBX.getNumCols());
            DenseMatrix64F subMiniBYLow = new DenseMatrix64F(1, miniBY.getNumCols());
            // WARP loss
            int approxRank = computeApproxRankError(pair.getFirst(), highY, allX, instMap, subMiniBXLow, subMiniBYLow);
            double WARPLoss = loss(approxRank);
            if (WARPLoss == 0.0) continue;
            loss += Math.max(0.0, (approxRank - 1) / (double) instancesList.get(pair.getFirst()).getInstances().size());
            lossErrorCount++;
            // compute gradient if error
            // f'(x-,W)
            DenseMatrix64F grad = f.grad(subMiniBXLow, subMiniBYLow);
            // f'(x-,W) - f'(x+,W)
            CommonOps.subEquals(grad, f.grad(subMiniBXHigh, subMiniBYHigh));
            // loss(rank(f(x+,W)) * f'(x-,W) - f'(x+,W)
            CommonOps.scale(WARPLoss, grad);
            // sum gradients
            CommonOps.addEquals(miniBG, grad);
        }
        if (lossErrorCount == 0) return 0;
        // update
        DenseMatrix64F miniBL2SqrG = f.gradL2Sqr();
        DenseMatrix64F miniBL1G = f.gradL1();
        // compute regularization
        CommonOps.scale(l2Coef * miniBPairs.size(), miniBL2SqrG);
        CommonOps.scale(l1Coef * miniBPairs.size(), miniBL1G);
        CommonOps.addEquals(miniBG, miniBL2SqrG);
        CommonOps.addEquals(miniBG, miniBL1G);
        // learning rate * average gradient
        CommonOps.scale(-learningRate / miniBPairs.size(), miniBG);
        f.update(miniBG);
        return loss / miniBPairs.size();
    }

    private double validateModel(List<IntegerPair> validatingInstances, DenseMatrix64F allX,
                                 HashMap<IntegerPair, Integer> instMap, List<Instances> instancesList) {
        HashMap<IntegerPair, Integer> vMap = new HashMap<IntegerPair, Integer>();
        DenseMatrix64F miniBX = RankerUtils.subMatrixPair(allX, validatingInstances, instMap, vMap);
        maskDownInput(miniBX);
        // compute activation
        DenseMatrix64F miniBY = f.compute(miniBX);

        double loss = 0.0;
        // for each positive instance
        for (IntegerPair pair : validatingInstances) {
            int highIndex = vMap.get(pair);
            double highY = miniBY.get(highIndex, 0);
            // rank error
            int rankError = computeRankError(pair.getFirst(), highY, allX, instMap);
            loss += Math.max(0.0, (rankError) / (double) instancesList.get(pair.getFirst()).getInstances().size());

        }
        return loss / validatingInstances.size();
    }

    @Override
    protected Options getOptions() {
        Options options = super.getOptions();
        options.addOption("warpK", true, "Optimize precision at <arg>. Default: " + WARPK + " (smoothing)");
        return options;
    }

    @Override
    protected void setup(CommandLine cmd) {
        super.setup(cmd);
        WARPK = Integer.parseInt(cmd.getOptionValue("warpK", WARPK + ""));
    }

    protected void initWARP(List<Instances> instancesList,
                            double goodLabel) {
        negativeIndexes = new HashMap<Integer, ArrayList<Integer>>();
        for (int iInstances = 0; iInstances < instancesList.size(); iInstances++) {
            Instances insts = instancesList.get(iInstances);
            negativeIndexes.put(iInstances, new ArrayList<Integer>());
            for (int iInstance = 0; iInstance < insts.getInstances().size(); iInstance++) {
                Instance inst = insts.getInstances().get(iInstance);
                if (inst.getValue(insts.getClassIndex()) < goodLabel) {
                    negativeIndexes.get(iInstances).add(iInstance);
                }
            }
        }
    }

    protected void clearWARP() {
        negativeIndexes = null;
    }

    protected int computeApproxRankError(int instsIndex, double instScore, DenseMatrix64F allX,
                                         HashMap<IntegerPair, Integer> instMap,
                                         DenseMatrix64F violatingX, DenseMatrix64F violatingY) {
        Random rand = new Random();
        ArrayList<Integer> negativeInstances = negativeIndexes.get(instsIndex);
        int numNegatives = negativeInstances.size();
        int numSamples = 0;
        while (numSamples <= numNegatives - 1) {
            numSamples++;
            int samplingIndex = negativeInstances.get(rand.nextInt(negativeInstances.size()));
            // compute score
            DenseMatrix64F X = RankerUtils.subSelectRow(allX,
                    instMap.get(new IntegerPair(instsIndex, samplingIndex)));
            DenseMatrix64F Y = f.compute(X);
            double samplingScore = Y.get(0, 0);
            if (margin - instScore + samplingScore > 0) {
                RankerUtils.copy(Y, violatingY);
                RankerUtils.copy(X, violatingX);
                break;
            }
        }
        if (numSamples == 0) return 0;
        return (int) Math.floor((numNegatives - 1) / numSamples);
    }

    protected int computeRankError(int instsIndex, double instScore, DenseMatrix64F allX,
                                   HashMap<IntegerPair, Integer> instMap) {
        ArrayList<Integer> negativeInstances = negativeIndexes.get(instsIndex);
        ArrayList<Integer> negativeIndexes = new ArrayList<Integer>();
        for (Integer instIndex : negativeInstances) {
            negativeIndexes.add(
                    instMap.get(new IntegerPair(instsIndex, instIndex)));
        }
        DenseMatrix64F X = RankerUtils.subSelectRows(allX, negativeIndexes);
        DenseMatrix64F Y = f.compute(X);
        int numViolating = 0;
        for (int i = 0; i < Y.getNumRows(); i++) {
            double score = Y.get(i, 0);
            if (margin - instScore + score > 0) {
                numViolating++;
            }
        }
        return numViolating;
    }

    protected double loss(int r) {
        double l = 0.0;
        for (int i = 1; i <= r; i++) {
            if (WARPK == -1) l += 1 / (double) i;
            else if (i <= WARPK) l++;
        }
        return l;
    }

    private void createSplit(List<IntegerPair> allPositiveInstances, List<IntegerPair> trainingPairs,
                             List<IntegerPair> validatingPairs, double validationPercent) {
        Random random = new Random();
        for (IntegerPair pair : allPositiveInstances) {
            if (random.nextDouble() < validationPercent) {
                validatingPairs.add(pair);
            } else {
                trainingPairs.add(pair);
            }
        }
    }
}
