package edu.northwestern.websail.classify.arch;


import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;

import java.util.Arrays;
import java.util.Properties;

public abstract class ClassifierUnit extends EnginePipelineUnit {

    protected Classifier classifier;

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        this.engineSuffix = "classifier";
        this.config = config;
        if (!this.setFeatureConfig()) {
            logger.severe("Fail to initialize features.");
            return false;
        }
        if (!this.load(env)) {
            logger.severe("Fail to initialize classifier.");
            return false;
        }
        classifier = (Classifier) super.engine;
        return true;
    }

    @Override
    protected boolean initEmptyEngine(String[] options) {
        classifier = (Classifier) super.engine;
        if (options.length > 1) {
            return classifier.init(Arrays.copyOfRange(options, 1, options.length));
        } else {
            return classifier.init(new String[1]);
        }
    }
}
