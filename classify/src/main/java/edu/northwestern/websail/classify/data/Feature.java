package edu.northwestern.websail.classify.data;

import edu.northwestern.websail.ds.map.BiMap;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

/**
 * @author NorThanapon
 * @since 11/13/14
 */
public class Feature implements Comparable<Feature>, Serializable {
    private static final long serialVersionUID = -3019412059427027896L;
    private Type type;
    private String group;
    private String name;
    private BiMap<String, Double> nominalMap;

    /**
     * This method is for serialization only
     */
    @Deprecated
    public Feature() {
    }

    /**
     * This method should be called for only indexing purpose.
     *
     * @param fullName is group.name
     */
    public Feature(String fullName) {
        String[] parts = fullName.split("\\.");
        this.group = parts[0];
        this.name = parts[1];
    }

    public Feature(String group, String name, Type type) {
        if (type == Type.NOMINAL) {
            throw new UnsupportedOperationException("NOMINAL type needs a nominal map. Use other constructor.");
        }
        this.group = group;
        this.name = name;
        this.type = type;
    }

    public Feature(String group, String name, Type type, BiMap<String, Double> nominalMap) {
        this.type = type;
        this.group = group;
        this.name = name;
        this.nominalMap = nominalMap;
    }

    public Type getType() {
        return type;
    }

    public String getGroup() {
        return group;
    }

    public String getName() {
        return name;
    }

    public BiMap<String, Double> getNominalMap() {
        return nominalMap;
    }

    @Override
    public int compareTo(@NotNull Feature o) {
        int groupCompare = group.compareTo(o.getGroup());
        if (groupCompare == 0) {
            return name.compareTo(o.getName());
        } else {
            return groupCompare;
        }
    }

    @Override
    public String toString() {
        return group + "." + name;
    }

    @Override
    public boolean equals(@NotNull Object other) {
        if (other instanceof Feature) {
            Feature otherF = (Feature) other;
            return group.equals(otherF.getGroup()) && name.equals(otherF.getName());
        }
        return other instanceof String && fullName().equals(other);
    }

    @Override
    public int hashCode() {
        return fullName().hashCode();
    }

    public String fullName() {
        return group + "." + name;
    }

    public enum Type {
        NUMBER,
        NOMINAL
    }
}
