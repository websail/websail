package edu.northwestern.websail.classify.result;

import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;

/**
 * @author NorThanapon
 * @since 11/14/14
 */
public class ClassificationResult {
    private final Instance instance;
    private final Feature classFeature;
    private final double classValue;
    private final double score;

    public ClassificationResult(Instance instance, Feature classFeature, double classValue, double score) {
        this.instance = instance;
        this.classFeature = classFeature;
        this.classValue = classValue;
        this.score = score;
    }

    public Instance getInstance() {
        return instance;
    }

    public Feature getClassFeature() {
        return classFeature;
    }

    public double getClassValue() {
        return classValue;
    }

    public double getScore() {
        return score;
    }

    public String getClassLabel() {
        return classFeature.getNominalMap().getKey(classValue);
    }
}
