package edu.northwestern.websail.classify.pipeline.engine;

import edu.northwestern.websail.classify.arch.ClassifierUnit;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.classify.result.ClassificationResult;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;

import java.util.ArrayList;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 11/18/14
 */
@PipelineConfigInfo(
        requiredSettings = {"classifierRunner.model=env:*|(classifierRunner.classifier=*, classifierRunner.model=*)"}
)
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class},
        produces = {ClassifyBUT.ClassifyResults.class}
)
public class ClassifierRunner extends ClassifierUnit {

    public ClassifierRunner() {
    }

    @Override
    protected String configPrefix() {
        return "classifierRunner";
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        boolean oneSuccess = false;
        for (BuckUnit unit : prev.units()) {
            if (unit.isNotOkay())
                continue;
            Instances instances = unit.get(ClassifyBUT.BuckInstances.class);
            ArrayList<ClassificationResult> results = new ArrayList<ClassificationResult>();
            for (Instance<Object> instance : instances.getInstances()) {
                try {
                    ClassificationResult result = this.classifier.classify(instance);
                    results.add(result);
                } catch (Exception e) {
                    logger.severe("Unexpected error while classifying instance of " + instance.sourceStr());
                    e.printStackTrace();
                    unit.setOkay(false);
                    continue;
                }
                oneSuccess = true;
            }
            unit.set(ClassifyBUT.ClassifyResults.class, results);
        }
        return oneSuccess;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        return true;
    }
}
