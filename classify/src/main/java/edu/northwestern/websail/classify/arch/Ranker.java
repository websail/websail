package edu.northwestern.websail.classify.arch;

import edu.northwestern.websail.classify.data.FeatureConfig;
import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.classify.result.RankingResult;

import java.io.Serializable;
import java.util.List;

/**
 * @author NorThanapon
 * @since 11/14/14
 */
public interface Ranker extends Serializable {
    boolean init(String[] args);

    void train(List<Instances> instancesList, FeatureConfig config);

    RankingResult rank(Instances instances);

}
