package edu.northwestern.websail.classify.ranker.mm;

import org.apache.commons.math3.analysis.function.Sigmoid;
import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.CommonOps;
import org.ejml.ops.RandomMatrices;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Random;

/**
 * @author NorThanapon
 * @since 3/5/15
 */
public class SigmoidUnit extends MaxMarginFunction implements Serializable {

    private static final long serialVersionUID = 1468855784391288640L;
    DenseMatrix64F W;

    public SigmoidUnit() {

    }

    /**
     * Create a logistic unit with sigmoid activation function
     *
     * @param nIn:  number of inputs (usually features + bias)
     * @param nOut: number of outputs (usually classes)
     */
    public SigmoidUnit(int nIn, int nOut) {
        this.init(nIn, nOut);
    }

    @Override
    public void init(int nIn, int nOut) {
        W = RandomMatrices.createRandom(nIn, nOut, -0.001, 0.001, new Random());
    }

    @Override
    public double L1() {
        double total = 0.0D;
        int rows = W.getNumRows() - 1; //don't count bias
        int cols = W.getNumCols();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                total += Math.abs(W.get(i, j));
            }
        }
        return total;
    }

    @Override
    public double L2Sqr() {
        double total = 0.0D;
        int rows = W.getNumRows() - 1; //don't count bias
        int cols = W.getNumCols();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                double d = W.get(i, j);
                total += d * d;
            }
        }
        return total;
    }

    /**
     * Compute logistic regression score
     *
     * @param X: row vector is a data point (feature + bias)
     * @return sigmoid(WX)
     */
    @Override
    public DenseMatrix64F compute(DenseMatrix64F X) {
        Sigmoid sigmoid = new Sigmoid();
        if (X.getNumCols() != W.getNumRows()) {
            throw new IllegalArgumentException("Number of columns are not matched. (You might forget to add bias column)");
        }
        // Set bias column to 1
//        for(int i =0 ; i<X.getNumRows(); i++){
//            X.set(i, X.getNumCols()-1, 1);
//        }
        // Output
        DenseMatrix64F Y = new DenseMatrix64F(X.getNumRows(), W.getNumCols());
        CommonOps.mult(X, W, Y);
        for (int i = 0; i < Y.getNumRows(); i++) {
            for (int j = 0; j < Y.getNumCols(); j++) {
                Y.set(i, j, sigmoid.value(Y.get(i, j)));
            }
        }
        return Y;
    }

    /**
     * Compute gradient of sigmoid function
     *
     * @param X: row vector is a data point (feature + bias)
     * @return d/dW sigmoid(WX)
     */
    @Override
    public DenseMatrix64F grad(DenseMatrix64F X, DenseMatrix64F Y) {
        DenseMatrix64F g = new DenseMatrix64F(W.getNumRows(), W.getNumCols());
        //TODO: vectorize this algorithm
        for (int i = 0; i < X.getNumRows(); i++) { // instance
            for (int c = 0; c < Y.getNumCols(); c++) { // class
                double tmp = Y.get(i, c) * (1 - Y.get(i, c));
                for (int d = 0; d < X.getNumCols(); d++) { // feature
                    double dy = X.get(i, d) * tmp;
                    g.set(d, c, g.get(d, c) + dy);
                }
            }
        }
        CommonOps.divide(X.getNumRows(), g);
        return g;
    }

    @Override
    public DenseMatrix64F gradL1() {
        // TODO: return a real grad
        return new DenseMatrix64F(this.W.getNumRows(), this.W.getNumCols());
    }

    @Override
    public DenseMatrix64F gradL2Sqr() {
        return new DenseMatrix64F(this.W);
    }

    /**
     * Update model parameters
     *
     * @param step: update step for all parameters
     */
    @Override
    public void update(DenseMatrix64F step) {
        CommonOps.addEquals(W, step);
    }

    public DenseMatrix64F getWeights() {
        return W;
    }

    public void setWeights(DenseMatrix64F w) {
        W = w;
    }

    @Override
    public String toString() {
        NumberFormat format = new DecimalFormat("+#.#####;-#");
        StringBuilder sb = new StringBuilder();
        sb.append("Sigmoid function with weights: \n\t");
        for (int j = 0; j < this.W.getNumCols() - 1; j++) {
            sb.append("  (").append(j).append(")  \t");
        }
        sb.append("  (").append(this.W.getNumCols() - 1).append(")  \n");
        for (int i = 0; i < this.W.getNumRows(); i++) {
            for (int j = 0; j < this.W.getNumCols() - 1; j++) {
                sb.append('(').append(i).append(")\t").append(format.format(this.W.get(i, j))).append('\t');
            }
            sb.append('(').append(i).append(")\t").append(format.format(this.W.get(i, this.W.getNumCols() - 1))).append('\n');
        }
        return sb.toString();
    }

    @Override
    public MaxMarginFunction copy() {
        SigmoidUnit f = new SigmoidUnit();
        f.setWeights(W);
        return f;
    }
}
