package edu.northwestern.websail.classify.ranker;

import edu.northwestern.websail.classify.data.CommonNominal;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.ds.tuple.IntegerPair;
import edu.northwestern.websail.ds.tuple.IntegerTriplet;
import org.ejml.data.DenseMatrix64F;

import java.util.*;

/**
 * @author NorThanapon
 * @since 3/6/15
 */
public class RankerUtils {
    public static List<IntegerTriplet> createAllPairs(Instances instances, int k) {
        ArrayList<IntegerTriplet> pairs = new ArrayList<IntegerTriplet>();
        int classIndex = instances.getClassIndex();
        for (int i = 0; i < instances.getInstances().size(); i++) {
            Instance main = instances.getInstances().get(i);
            for (int j = 0; j < instances.getInstances().size(); j++) {
                Instance second = instances.getInstances().get(j);
                if (main.getValue(classIndex) <= second.getValue(classIndex)) continue; //not better
                pairs.add(new IntegerTriplet(k, i, j));
            }
        }
        return pairs;
    }

    public static List<IntegerTriplet> createAllPairs(Instances instances, int k, int mainI) {
        ArrayList<IntegerTriplet> pairs = new ArrayList<IntegerTriplet>();
        int classIndex = instances.getClassIndex();
        for (int i = 0; i < instances.getInstances().size(); i++) {
            if (i != mainI) continue;
            Instance main = instances.getInstances().get(i);
            for (int j = 0; j < instances.getInstances().size(); j++) {
                Instance second = instances.getInstances().get(j);
                if (main.getValue(classIndex) <= second.getValue(classIndex)) continue; //not better
                pairs.add(new IntegerTriplet(k, i, j));
            }
        }
        return pairs;
    }

    public static List<IntegerTriplet> sampleAndRemove(List<IntegerTriplet> pairs, int size) {
        Random rand = new Random();
        ArrayList<IntegerTriplet> samplePairs = new ArrayList<IntegerTriplet>();
        for (int i = 0; i < size; i++) {
            int index = rand.nextInt(pairs.size());
            samplePairs.add(pairs.get(index));
            pairs.remove(index);
        }
        return samplePairs;
    }

    public static List<IntegerPair> createPositiveSample(List<Instances> instancesList) {
        ArrayList<IntegerPair> pairs = new ArrayList<IntegerPair>();
        for (int i = 0; i < instancesList.size(); i++) {
            Instances insts = instancesList.get(i);
            int classIndex = insts.getClassIndex();
            for (int j = 0; j < insts.getInstances().size(); j++) {
                //XXX: assuming that CommonNominal.boolMapTrueValue is the correct value
                if (insts.getInstances().get(j).getValue(classIndex) >= CommonNominal.boolMapTrueValue)
                    pairs.add(new IntegerPair(i, j));
            }
        }
        return pairs;
    }

    public static DenseMatrix64F subMatrix(DenseMatrix64F allX, List<IntegerTriplet> pairs, HashMap<IntegerPair, Integer> instMap, HashMap<IntegerPair, Integer> newInstMap) {
        HashSet<Integer> usedMatrixIndexes = new HashSet<Integer>();
        for (IntegerTriplet t : pairs) {
            usedMatrixIndexes.add(instMap.get(new IntegerPair(t.getFirst(), t.getSecond())));
            usedMatrixIndexes.add(instMap.get(new IntegerPair(t.getFirst(), t.getThird())));
        }
        DenseMatrix64F subX = new DenseMatrix64F(usedMatrixIndexes.size(), allX.getNumCols());
        usedMatrixIndexes.clear();
        int k = 0;
        for (IntegerTriplet t : pairs) {
            IntegerPair highPair = new IntegerPair(t.getFirst(), t.getSecond());
            int bigHighIndex = instMap.get(highPair);
            IntegerPair lowPair = new IntegerPair(t.getFirst(), t.getThird());
            int bigLowIndex = instMap.get(lowPair);
            if (!usedMatrixIndexes.contains(bigHighIndex)) {
                copyRow(allX, bigHighIndex, subX, k);
                newInstMap.put(highPair, k);
                k++;
                usedMatrixIndexes.add(bigHighIndex);
            }
            if (!usedMatrixIndexes.contains(bigLowIndex)) {
                copyRow(allX, bigLowIndex, subX, k);
                newInstMap.put(lowPair, k);
                k++;
                usedMatrixIndexes.add(bigLowIndex);
            }

        }
        return subX;
    }

    public static DenseMatrix64F subMatrixPair(DenseMatrix64F allX, List<IntegerPair> pairs, HashMap<IntegerPair, Integer> instMap, HashMap<IntegerPair, Integer> newInstMap) {
        DenseMatrix64F subX = new DenseMatrix64F(pairs.size(), allX.getNumCols());
        int k = 0;
        for (IntegerPair t : pairs) {
            copyRow(allX, instMap.get(t), subX, k);
            newInstMap.put(t, k);
            k++;
        }
        return subX;
    }

    public static void copy(DenseMatrix64F origin, DenseMatrix64F dest) {
        for (int i = 0; i < origin.getNumRows(); i++) {
            copyRow(origin, i, dest, i);
        }
    }
    public static void copyRow(DenseMatrix64F allX, int bigR, DenseMatrix64F subX, int subR) {
        for (int j = 0; j < allX.getNumCols(); j++) {
            subX.set(subR, j, allX.get(bigR, j));
        }
    }

    public static DenseMatrix64F selectPositiveRows(DenseMatrix64F data, DenseMatrix64F cond, int condIndex) {
        int rows = 0;
        for (int r = 0; r < cond.getNumRows(); r++) {
            double y = cond.get(r, condIndex);
            if (y <= 0) continue;
            rows++;
        }
        if (rows == 0) return null;
        rows = 0;
        DenseMatrix64F subMatrix = new DenseMatrix64F(rows, data.getNumCols());
        for (int r = 0; r < cond.getNumRows(); r++) {
            double y = cond.get(r, condIndex);
            if (y <= 0) continue;
            copyRow(data, r, subMatrix, rows);
            rows++;
        }
        return subMatrix;
    }

    public static DenseMatrix64F subSelectRows(DenseMatrix64F data, Collection<Integer> rows) {
        DenseMatrix64F sub = new DenseMatrix64F(rows.size(), data.getNumCols());
        int subI = 0;
        for (Integer row : rows) {
            copyRow(data, row, sub, subI++);
        }
        return sub;
    }

    public static DenseMatrix64F subSelectRow(DenseMatrix64F data, int row) {
        DenseMatrix64F sub = new DenseMatrix64F(1, data.getNumCols());
        copyRow(data, row, sub, 0);
        return sub;
    }


}
