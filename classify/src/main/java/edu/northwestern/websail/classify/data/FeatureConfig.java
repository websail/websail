package edu.northwestern.websail.classify.data;

import java.io.Serializable;
import java.util.HashSet;

/**
 * @author NorThanapon
 * @since 11/18/14
 */
public class FeatureConfig implements Serializable {
    private static final long serialVersionUID = -6642023727136622134L;
    private HashSet<String> groups;
    private HashSet<String> fullNames;

    public FeatureConfig() {
    }

    public FeatureConfig(String config) {
        config(config);
    }

    public void config(String config) {
        groups = new HashSet<String>();
        fullNames = new HashSet<String>();
        config = config.trim();
        String[] features = config.split(",");
        for (String feature : features) {
            String[] parts = feature.split("\\.");
            String group = parts[0];
            String name = parts[1];
            if (name.equals("*")) {
                groups.add(group);
            } else {
                fullNames.add(feature);
            }
        }
    }

    public boolean notAllow(Feature feature) {
        return groups != null &&
                fullNames != null &&
                !groups.contains(feature.getGroup()) &&
                !fullNames.contains(feature.fullName());
    }
}
