package edu.northwestern.websail.classify.arch;

import edu.northwestern.websail.classify.data.FeatureConfig;
import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.classify.result.RankingResult;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

/**
 * @author NorThanapon
 * @since 11/14/14
 */
// TODO: Implement this class -- converting Instances to pair-wise Instances
public abstract class PairwiseRanker implements Ranker {

    private static final long serialVersionUID = 4152481897869628502L;
    private final Classifier classifier;

    public PairwiseRanker(Classifier classifier) {
        this.classifier = classifier;
    }

    @Override
    public void train(List<Instances> instancesList, FeatureConfig config) {
        throw new NotImplementedException();
    }

    @Override
    public RankingResult rank(Instances instances) {
        throw new NotImplementedException();
    }

    public Classifier getClassifier() {
        return classifier;
    }
}
