package edu.northwestern.websail.classify.data;

import edu.northwestern.websail.ds.indexer.Indexer;

import java.util.ArrayList;

/**
 * @author NorThanapon
 * @since 11/13/14
 */
public class Instance<T> {
    public static final double MISSING = Double.NaN;
    private final Indexer<Feature> features;
    private final ArrayList<Double> values;
    private final T source;

    public Instance(Indexer<Feature> features) {
        this(features, null);
    }

    public Instance(Indexer<Feature> features, T source) {
        this.features = features;
        this.values = new ArrayList<Double>();
        this.source = source;
    }

    public double getValue(int index) {
        if (features.size() < index)
            throw new IndexOutOfBoundsException("There is no feature defined for index: " + index);
        return this.values.get(index);
    }

    public double getValue(Feature feature) {
        int index = features.indexOf(feature);
        if (index == -1) {
            throw new IndexOutOfBoundsException("The input feature, " + feature + ", is not defined.");
        }
        return getValue(index);
    }

    public String getNominalValue(int index) {
        if (features.size() < index)
            throw new IndexOutOfBoundsException("There is no feature defined for index: " + index);
        Feature feature = features.get(index);
        if (feature.getType() != Feature.Type.NOMINAL) {
            throw new UnsupportedOperationException(feature + "'s type is not nominal.");
        }
        double value = this.getValue(index);
        return feature.getNominalMap().getKey(value);
    }

    public String getNominalValue(Feature feature) {
        int index = features.indexOf(feature);
        if (index == -1) {
            throw new IndexOutOfBoundsException("The input feature, " + feature + ", is not defined.");
        }
        return getNominalValue(index);
    }

    public void setValue(int index, double value) {
        if (features.size() < index)
            throw new IndexOutOfBoundsException("There is no feature defined for index: " + index);
        for (int i = values.size(); i <= index; i++) {
            values.add(MISSING);
        }
        values.set(index, value);
    }

    public void setValue(int index, String nominal) {
        if (features.size() < index)
            throw new IndexOutOfBoundsException("There is no feature defined for index: " + index);
        Feature feature = features.get(index);
        if (feature.getType() != Feature.Type.NOMINAL)
            throw new UnsupportedOperationException(feature + "'s type is not nominal.");
        Double value = feature.getNominalMap().getValue(nominal);
        if (value == null) {
            throw new NullPointerException("Nominal object," + nominal + " , is not in the "
                    + feature + "'s nominal map.");
        }
        setValue(index, value);
    }

    public void setValue(Feature feature, double value) {
        int index = features.indexOf(feature);
        if (index == -1) {
            throw new IndexOutOfBoundsException("The input feature, " + feature + ", is not defined.");
        }
        setValue(index, value);
    }

    public void setValue(Feature feature, String nominal) {
        int index = features.indexOf(feature);
        if (index == -1) {
            throw new IndexOutOfBoundsException("The input feature is not defined");
        }
        setValue(index, nominal);
    }

    public Indexer<Feature> getFeatures() {
        return features;
    }

    public ArrayList<Double> getValues() {
        return values;
    }

    public T getSource() {
        return source;
    }

    public String sourceStr() {
        if (this.source == null) return "anonymous";
        else return source.toString();
    }
}
