package edu.northwestern.websail.classify.arch;

import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.core.pipeline.unit.par.ParallelPLManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 * @since 3/20/15
 */
public abstract class FeatureFilter implements PipelineExecutable {
    public static final Logger logger = Logger.getLogger(FeatureFilter.class.getName());
    protected HashSet<Feature> targets;
    protected int threadId = 0;
    protected List<Instances> instancesList;

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        String featureStr = config.getProperty(this.prefixConfig() + ".targets");
        if (featureStr == null) {
            logger.severe("Missing configuration: " + this.prefixConfig() + ".targets");
            return false;
        }
        targets = new HashSet<Feature>();
        String[] features = featureStr.trim().split(",");
        for (String feature : features) {
            targets.add(new Feature(feature));
        }
        instancesList = new ArrayList<Instances>();
        return true;
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        for (BuckUnit unit : prev.units()) {
            if (unit.isNotOkay()) {
                continue;
            }
            Instances unitInstances = unit.get(ClassifyBUT.BuckInstances.class);
            instancesList.add(unitInstances);
        }
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        collect();
        ParallelPLManager.countDownLatch(env, ParallelPLManager.ALL_LATCH_1);
        ParallelPLManager.awaitLatch(env, ParallelPLManager.ALL_LATCH_1);
        exchange();
        ParallelPLManager.countDownLatch(env, ParallelPLManager.ALL_LATCH_2);
        ParallelPLManager.awaitLatch(env, ParallelPLManager.ALL_LATCH_2);
        return update();
    }

    protected abstract String prefixConfig();

    protected abstract void collect();

    protected abstract void exchange();

    protected abstract boolean update();

    @Override
    public int getThreadId() {
        return threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }
}
