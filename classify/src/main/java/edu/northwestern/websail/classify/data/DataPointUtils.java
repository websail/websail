package edu.northwestern.websail.classify.data;

import edu.northwestern.websail.ds.indexer.HashIndexer;
import edu.northwestern.websail.ds.indexer.Indexer;
import edu.northwestern.websail.ds.tuple.IntegerPair;
import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.RandomMatrices;

import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 * @since 3/5/15
 */
public final class DataPointUtils {
    public final static Logger logger = Logger.getLogger(DataPointUtils.class.getName());
    private final static Random rand = new Random();

    public static Indexer<Feature> denseFeatureMap(Indexer<Feature> features, FeatureConfig config, int classIndex) {
        Indexer<Feature> denseMap = new HashIndexer<Feature>(Feature.class);
        for (int i = 0; i < features.size(); i++) {
            Feature feature = features.get(i);
            if (config.notAllow(feature)) continue;
            if (i == classIndex) continue;
            denseMap.add(features.get(i));
        }
        return denseMap;
    }

    public static DenseMatrix64F matrix(Instances instances, Indexer<Feature> denseFeatureMap, boolean addBias) {
        int rows = instances.getInstances().size();
        int cols = denseFeatureMap.size();
        if (addBias) cols += 1;
        DenseMatrix64F X = RandomMatrices.createRandom(rows, cols, 1, 1, rand);
        if (addBias) cols -= 1;
        for (int i = 0; i < rows; i++) {
            Instance inst = instances.getInstances().get(i);
            for (int j = 0; j < cols; j++) {
                X.set(i, j, inst.getValue(denseFeatureMap.get(j)));
            }
        }
        return X;
    }

    public static DenseMatrix64F matrix(List<Instances> instances, Indexer<Feature> denseFeatureMap, boolean addBias, HashMap<IntegerPair, Integer> outInstanceMap) {
        return matrix(instances, denseFeatureMap, addBias, outInstanceMap, null);
    }

    public static DenseMatrix64F matrix(List<Instances> instances, Indexer<Feature> denseFeatureMap, boolean addBias,
                                        HashMap<IntegerPair, Integer> outInstanceMap, List<IntegerPair> positiveInstances) {
        int rows = 0;
        for (Instances insts : instances) {
            rows += insts.getInstances().size();
        }
        int cols = denseFeatureMap.size();
        if (addBias) cols += 1;
        DenseMatrix64F X = RandomMatrices.createRandom(rows, cols, 1, 1, rand);
        if (addBias) cols -= 1;
        int r = 0;
        for (int k = 0; k < instances.size(); k++) {
            Instances insts = instances.get(k);
            for (int i = 0; i < instances.get(k).getInstances().size(); i++) {
                Instance inst = instances.get(k).getInstances().get(i);
                IntegerPair pair = new IntegerPair(k, i);
                //XXX: assuming that CommonNominal.boolMapTrueValue is the correct value
                if (inst.getValue(insts.getClassIndex()) >= CommonNominal.boolMapTrueValue && positiveInstances != null) {
                    positiveInstances.add(pair);
                }
                outInstanceMap.put(pair, r);
                for (int j = 0; j < cols; j++) {
                    double v = inst.getValue(denseFeatureMap.get(j));
                    if (Double.isNaN(v) || Double.isInfinite(v)) {
                        logger.warning("'" + inst.getSource() + "' " + denseFeatureMap.get(j).fullName() + " has NaN or INF value. Substitute with 0.");
                        v = 0.0;
                    }
                    X.set(r, j, v);
                }
                r++;
            }
        }
        return X;
    }
}
