package edu.northwestern.websail.classify.result;

import edu.northwestern.websail.classify.data.Instances;

/**
 * @author NorThanapon
 * @since 11/14/14
 */
public class RankingResult {
    private Instances instances;
    private Integer[] orders;
    private Double[] scores;

    public RankingResult() {
    }

    public RankingResult(Instances instances, Integer[] orders, Double[] scores) {
        this.instances = instances;
        this.orders = orders;
        this.scores = scores;
    }

    public RankingResult(Instances instances, Integer[] orders) {
        this.instances = instances;
        this.orders = orders;
        this.scores = new Double[orders.length];
        for (int i = 0; i < orders.length; i++) {
            this.scores[i] = (orders.length - orders[i] - 1) * 1d;
        }
    }

    public Instances getInstances() {
        return instances;
    }

    public Integer[] getOrders() {
        return orders;
    }

    public Double[] getScores() {
        return scores;
    }
}
