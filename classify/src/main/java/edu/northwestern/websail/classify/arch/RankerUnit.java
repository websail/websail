package edu.northwestern.websail.classify.arch;


import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;

import java.util.Arrays;
import java.util.Properties;

public abstract class RankerUnit extends EnginePipelineUnit {
    protected Ranker ranker;

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        this.engineSuffix = "ranker";
        this.config = config;
        if (!this.setFeatureConfig()) {
            logger.severe("Fail to initialize features.");
            return false;
        }
        if (!this.load(env)) {
            logger.severe("Fail to initialize classifier.");
            return false;
        }
        ranker = (Ranker) super.engine;
        return true;
    }

    @Override
    protected boolean initEmptyEngine(String[] options) {
        ranker = (Ranker) super.engine;
        if (options.length > 1) {
            return ranker.init(Arrays.copyOfRange(options, 1, options.length));
        } else {
            return ranker.init(new String[1]);
        }
    }


}
