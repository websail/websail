package edu.northwestern.websail.classify.pipeline;

import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.classify.result.ClassificationResult;
import edu.northwestern.websail.classify.result.RankingResult;
import edu.northwestern.websail.core.pipeline.buck.BuckUnitTypes;

import java.util.ArrayList;


/**
 * @author NorThanapon
 * @since 11/18/14
 */
@SuppressWarnings("unchecked")
public class ClassifyBUT {
    public static class BuckInstances implements BuckUnitTypes.UnitTypeKey<edu.northwestern.websail.classify.data.Instances> {
        public Class<Instances> getType() {
            return Instances.class;
        }
    }

    public static class ClassifyResults implements BuckUnitTypes.UnitTypeKey<ArrayList<ClassificationResult>> {
        @Override
        public Class<ArrayList<ClassificationResult>> getType() {
            return (Class<ArrayList<ClassificationResult>>) (new ArrayList<ClassificationResult>())
                    .getClass();
        }
    }

    public static class RankingResults implements BuckUnitTypes.UnitTypeKey<RankingResult> {
        @Override
        public Class<RankingResult> getType() {
            return (Class<RankingResult>) (new RankingResult())
                    .getClass();
        }
    }
}
