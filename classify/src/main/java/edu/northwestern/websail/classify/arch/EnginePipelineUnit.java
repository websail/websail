package edu.northwestern.websail.classify.arch;

import edu.northwestern.websail.classify.data.FeatureConfig;
import edu.northwestern.websail.core.io.OutputFileManager;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.config.PipelineConfig;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;

import java.io.*;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 * @since 3/10/15
 */
@SuppressWarnings("BooleanMethodIsAlwaysInverted")
public abstract class EnginePipelineUnit implements PipelineExecutable {
    public static final Logger logger = Logger.getLogger(EnginePipelineUnit.class.getName());
    protected final String modelSuffix = "model";
    protected final String featureSuffix = "features";
    protected final String outputSuffix = "output";
    protected Properties config;
    protected FeatureConfig fc;
    protected int threadId = 0;
    protected String engineSuffix = "engine";
    protected Object engine;

    protected boolean setFeatureConfig() {
        String featureConfigKey = configPrefix() + "." + featureSuffix;
        logger.info("Feature Configuration Key: " + featureConfigKey);
        String featureConfigStr = PipelineConfig.switchKey(config, featureConfigKey);
        if (featureConfigStr.equals("")) {
            fc = new FeatureConfig();
            logger.info("Empty Feature Configuration. Allow All Features.");
            return true;
        }
        try {
            fc = new FeatureConfig(featureConfigStr);
            logger.info("Feature Configuration Loaded.");
            return true;
        } catch (Exception e) {
            logger.severe("Unexpected error while initializing feature configuration.");
            e.printStackTrace();
            return false;
        }
    }

    protected boolean load(PipelineEnvironment env) {
        String engineKey = configPrefix() + "." + engineSuffix;
        String modelKey = configPrefix() + "." + modelSuffix;

        String modelStr = PipelineConfig.switchKey(config, modelKey);
        // (Option 1) Load from environment
        if (modelStr.startsWith("env:")) {
            modelStr = modelStr.substring(4) + getThreadIdStr();
            logger.info("Load engine from environment " + modelStr + ". Ignore " + engineKey);
            engine = env.getPipelineGlobalOuputs().get(modelStr);
            if (engine == null) {
                logger.severe(modelStr + " does not exist in the environment.");
                return false;
            }
            return true;
        }
        // Create a new instance (either from file or empty engine)
        String engineStr = PipelineConfig.switchKey(config, engineKey);
        if (engineStr.equals("")) {
            logger.severe(engineKey + " does not exist. Please add engine configuration.");
            return false;
        }

        //Check engine class
        String[] engineOptions = engineStr.split(",");
        try {
            engine = Class.forName(engineOptions[0]).newInstance();
        } catch (InstantiationException e) {
            logger.severe(engineOptions[0] + " is not constructable (abstract, or no empty constructor).");
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            logger.severe(engineOptions[0] + " is null or does not have an empty constructor.");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            logger.severe(engineOptions[0] + " cannot be found.");
            e.printStackTrace();
        }
        if (engine == null) return false;

        // (Option 2) Load from file
        if (modelStr.startsWith("file:")) {
            modelStr = modelStr.substring(5);
            logger.info("Load " + engineOptions[0] + " from file: " + modelStr + ". Igore the rest of " + engineKey + ".");
            try {
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(modelStr));
                engine = ois.readObject();
                logger.fine(engine.toString());
                ois.close();
                return true;
            } catch (FileNotFoundException e) {
                logger.severe(modelStr + " does not exist.");
                e.printStackTrace();
            } catch (Exception e) {
                logger.severe("Unexpected error while loading model from " + modelStr);
                e.printStackTrace();
            }
            return false;
        }

        // (Option 3) Empty engine
        logger.info("Initialize empty classifier.");
        return initEmptyEngine(engineOptions);
    }

    protected boolean output(PipelineEnvironment env, Properties config) {
        String outputKey = configPrefix() + "." + outputSuffix;
        String outputStr = PipelineConfig.switchKey(config, outputKey);
        if (outputStr.equals("")) {
            logger.info("There is no engine output setting. Skip output.");
            return true;
        }
        String parts[] = outputStr.split(",");
        boolean allSuccess = true;
        for (String part : parts) {
            boolean result = output(env, part);
            allSuccess = allSuccess && result;
        }
        return allSuccess;
    }

    private boolean output(PipelineEnvironment env, String outputStr) {
        outputStr = outputStr.trim();
        if (outputStr.startsWith("file:")) {
            outputStr = outputStr.substring(5) + getThreadIdStr();
            try {
                boolean result = OutputFileManager.createFile(outputStr);
                if (!result) {
                    logger.warning(outputStr + " file exists. Overwriting.");
                }
                ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(outputStr));
                oos.writeObject(this.engine);
                oos.close();
                logger.info("Save engine to file " + outputStr);
            } catch (IOException e) {
                logger.severe("Unexpected error while saving a engine to " + outputStr);
                e.printStackTrace();
            }
            return true;
        } else if (outputStr.startsWith("env:")) {
            outputStr = outputStr.substring(4);
            env.getPipelineGlobalOuputs().put(outputStr + getThreadIdStr(), this.engine);
            logger.info("Save engine to environment output key " + outputStr + getThreadIdStr());
            return true;
        }
        return false;
    }

    protected abstract String configPrefix();

    protected abstract boolean initEmptyEngine(String[] options);

    @Override
    public int getThreadId() {
        return this.threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }

    public String getThreadIdStr() {
        return "@tId:" + this.threadId;
    }
}
