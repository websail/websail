package edu.northwestern.websail.classify.arch;

import edu.northwestern.websail.classify.data.FeatureConfig;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.classify.result.ClassificationResult;

import java.io.Serializable;

/**
 * @author NorThanapon
 * @since 11/14/14
 */
public interface Classifier extends Serializable {
    boolean init(String[] args);

    void train(Instances instances, FeatureConfig config) throws Exception;

    <T> ClassificationResult classify(Instance<T> instance) throws Exception;
}
