package edu.northwestern.websail.classify.data;

import edu.northwestern.websail.ds.indexer.HashIndexer;
import edu.northwestern.websail.ds.indexer.Indexer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author NorThanapon
 * @since 11/14/14
 */
public class Instances {
    //XXX: HACK
    public boolean _merged = false;
    Indexer<Feature> features;
    List<Instance> instances;
    int classIndex;

    public Instances() {
        features = new HashIndexer<Feature>();
        instances = new ArrayList<Instance>();
    }

    public Instances(Indexer<Feature> features) {
        this.features = features;
        instances = new ArrayList<Instance>();
    }

    public Indexer<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(Indexer<Feature> features) {
        this.features = features;
    }

    public List<Instance> getInstances() {
        return instances;
    }

    public void setInstances(List<Instance> instances) {
        this.instances = instances;
    }

    public int getClassIndex() {
        return classIndex;
    }

    public void setClassIndex(int classIndex) {
        this.classIndex = classIndex;
    }

    /**
     * Merge Instances.
     *
     * @param other is <code>Instances</code> object that has the same feature index
     */
    public void merge(Instances other) {
        if (!other._merged) {
            this.instances.addAll(other.getInstances());
        }
        other._merged = true;
    }
}
