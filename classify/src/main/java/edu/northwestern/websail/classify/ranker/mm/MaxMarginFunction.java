package edu.northwestern.websail.classify.ranker.mm;

import org.ejml.data.DenseMatrix64F;

import java.io.Serializable;

/**
 * @author NorThanapon
 * @since 3/5/15
 */
public abstract class MaxMarginFunction implements Serializable {
    private static final long serialVersionUID = -5840002910423758387L;

    /**
     * Create a computational unit
     *
     * @param nIn:  number of inputs (usually features + bias)
     * @param nOut: number of outputs (usually classes)
     */
    public abstract void init(int nIn, int nOut);

    /**
     * Compute L1 norm for regularization
     *
     * @return L1 norm
     */
    public abstract double L1();

    /**
     * Compute sqaure of L2 norm for regularization
     *
     * @return sqaure of L2 norm
     */
    public abstract double L2Sqr();

    /**
     * Compute score
     *
     * @param X: row vector is a data point (feature + bias)
     * @return a matrix of scores of X (row:instance, column:classes)
     */
    public abstract DenseMatrix64F compute(DenseMatrix64F X);

    /**
     * Compute gradient of the function
     *
     * @param X: row vector is a data point (feature + bias)
     * @return d/dW f(W, X), dimension of (row:nIn, column:nOut) (or step in {@link #update(org.ejml.data.DenseMatrix64F)})
     */
    public abstract DenseMatrix64F grad(DenseMatrix64F X, DenseMatrix64F Y);

    /**
     * Compute gradient of L1 norm
     *
     * @return gradient of L1 norm ()
     */
    public abstract DenseMatrix64F gradL1();

    /**
     * Compute gradient of square of L2 norm
     *
     * @return gradient of square of L2 norm ()
     */
    public abstract DenseMatrix64F gradL2Sqr();

    /**
     * Update parameters
     *
     * @param step is a matrix of parameters to "add" to the current parameters. Dimension of (row:nIn, column:nOut)
     */
    public abstract void update(DenseMatrix64F step);

    /**
     * Copy itself
     */
    public abstract MaxMarginFunction copy();
}
