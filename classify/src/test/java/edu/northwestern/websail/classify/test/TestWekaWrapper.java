package edu.northwestern.websail.classify.test;

import edu.northwestern.websail.classify.arch.Classifier;
import edu.northwestern.websail.classify.classifier.weka.WekaClassifierWrapper;
import edu.northwestern.websail.classify.classifier.weka.WekaUtil;
import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.FeatureConfig;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.classify.result.ClassificationResult;
import edu.northwestern.websail.ds.indexer.HashIndexer;
import edu.northwestern.websail.ds.map.BiMap;
import edu.northwestern.websail.ds.map.HashBiMap;
import org.junit.Assert;
import org.junit.Test;
import weka.core.Attribute;
import weka.core.FastVector;

import java.io.*;

/**
 * @author NorThanapon
 * @since 11/17/14
 */
public class TestWekaWrapper {
    final HashIndexer<Feature> features = new HashIndexer<Feature>();
    final BiMap<String, Double> nominalMap = new HashBiMap<String, Double>();
    final Instances instances = new Instances(features);
    final Instance<String> instance = new Instance<String>(features);
    final Feature fTestNumber = new Feature("test", "number", Feature.Type.NUMBER);
    final Feature fTestNominal = new Feature("test", "nominal", Feature.Type.NOMINAL, nominalMap);
    final double testNumber = 1.0;
    final String testNominal = "false";
    final FeatureConfig fc = new FeatureConfig("test.*");
    public TestWekaWrapper() {
        nominalMap.put("true", 1d);
        nominalMap.put("false", 0d);
        features.add(fTestNumber);
        features.add(fTestNominal);
        instance.setValue(fTestNominal, testNominal);
        instance.setValue(fTestNumber, testNumber);
        instances.getInstances().add(instance);
        instances.setClassIndex(features.indexOf(fTestNominal));

    }

    @Test
    public void testWekaUtil() {

        FastVector attrs = WekaUtil.attrs(features, fc);
        for (int i = 0; i < attrs.size(); i++) {
            Attribute attr = (Attribute) attrs.elementAt(i);
            Assert.assertEquals("Attribute index is consistent.", attr.name(), features.get(i).fullName());
        }

        weka.core.Instances dataset = WekaUtil.dataset(instances, attrs);
        Assert.assertEquals("Class attribute is consistent.", dataset.classAttribute().name(), features.get(instances.getClassIndex()).fullName());
        Assert.assertEquals("Class index is consistent.", dataset.classIndex(), instances.getClassIndex());

        weka.core.Instance datapoint = WekaUtil.datapoint(dataset, instance);
        Assert.assertEquals("Class attribute is consistent.", datapoint.classAttribute().name(), features.get(instances.getClassIndex()).fullName());
        Assert.assertEquals("Number value is correct.", (Double) datapoint.value(0), (Double) instance.getValue(0));
        Assert.assertEquals("Nominal value is correct.", datapoint.attribute(1).value((int) datapoint.value(1)), instance.getNominalValue(1));

    }

    @Test
    public void testWekaWrapper() throws Exception {

        String[] args = {"weka.classifiers.rules.ZeroR", "-D"};
        WekaClassifierWrapper classifier = new WekaClassifierWrapper(args);
        classifier.train(instances, fc);
        ClassificationResult result = classifier.classify(instance);
        Assert.assertEquals("Class feature is consistent.", result.getClassFeature().fullName(), fTestNominal.fullName());
        Assert.assertEquals("Output label is correct.", result.getClassLabel(), testNominal);
        Assert.assertEquals("Output value is correct.", (Double) result.getClassValue(), fTestNominal.getNominalMap().getValue(testNominal));
        Assert.assertEquals("Output score is correct (assuming +1 smoothing).", (Double) result.getScore(), (Double) (2 / (double) 3));
    }

    @Test
    public void testWekaWrapperSerialization() throws Exception {

        String[] args = {"weka.classifiers.rules.ZeroR", "-D"};
        WekaClassifierWrapper classifier = new WekaClassifierWrapper(args);
        classifier.train(instances, fc);
        ClassificationResult result = classifier.classify(instance);

        String filename = "testfile";
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename));
            oos.writeObject(classifier);
            oos.close();

            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename));
            Classifier classifier2 = (Classifier) ois.readObject();
            ClassificationResult result2 = classifier2.classify(instance);
            Assert.assertEquals("Class feature is consistent.", result.getClassFeature().fullName(), result2.getClassFeature().fullName());
            Assert.assertEquals("Output label is correct.", result.getClassLabel(), result2.getClassLabel());
            Assert.assertEquals("Output value is correct.", (Double) result.getClassValue(), (Double) result2.getClassValue());
            Assert.assertEquals("Output score is correct.", (Double) result.getScore(), (Double) result.getScore());
            ois.close();
        } finally {
            File file = new File(filename);
            file.deleteOnExit();
        }
    }
}
