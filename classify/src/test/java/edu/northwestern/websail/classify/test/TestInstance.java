package edu.northwestern.websail.classify.test;

import edu.northwestern.websail.classify.data.*;
import edu.northwestern.websail.ds.indexer.HashIndexer;
import edu.northwestern.websail.ds.map.BiMap;
import edu.northwestern.websail.ds.map.HashBiMap;
import org.ejml.data.DenseMatrix64F;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
/**
 * @author NorThanapon
 * @since 11/13/14
 */
public class TestInstance {

    @Test
    public void testFeatures() {
        BiMap<String, Double> nominalMap = new HashBiMap<String, Double>();
        nominalMap.put("true", 1d);
        nominalMap.put("false", 0d);
        HashIndexer<Feature> features = new HashIndexer<Feature>();
        Feature fTestNumber = new Feature("test", "number", Feature.Type.NUMBER);
        Feature fTestNominal = new Feature("test", "nominal", Feature.Type.NOMINAL, nominalMap);
        Feature fTestNominal2 = new Feature("test", "nominal", Feature.Type.NOMINAL, nominalMap);
        features.add(fTestNumber);
        features.add(fTestNominal);
        features.add(fTestNominal2);
        assertEquals("Feature name cannot be duplicate", (Integer) features.size(), (Integer) 2);
        assertSame("Any feature object with the same name can get feature object.", features.get(new Feature("test.nominal")), fTestNominal);
    }

    @Test
    public void testValues() {
        HashIndexer<Feature> features = new HashIndexer<Feature>();
        BiMap<String, Double> nominalMap = new HashBiMap<String, Double>();
        nominalMap.put("true", 1d);
        nominalMap.put("false", 0d);
        Instance<String> instance = new Instance<String>(features);
        Feature fTestNumber = new Feature("test", "number", Feature.Type.NUMBER);
        features.add(fTestNumber);
        Feature fTestNominal = new Feature("test", "nominal", Feature.Type.NOMINAL, nominalMap);
        features.add(fTestNominal);
        double testNumber = 1.0;
        String testNominal = "false";
        instance.setValue(fTestNominal, testNominal);
        instance.setValue(fTestNumber, testNumber);
        assertEquals("Value is consistent.", (Double) instance.getValue(fTestNumber), (Double) testNumber);
        assertEquals("Value is consistent.", instance.getNominalValue(fTestNominal), testNominal);
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    @Test
    public void testSource() {
        HashIndexer<Feature> features = new HashIndexer<Feature>();
        BiMap<String, Double> nominalMap = new HashBiMap<String, Double>();
        nominalMap.put("true", 1d);
        nominalMap.put("false", 0d);
        String source = "hello";
        Instance<String> instance = new Instance<String>(features, source);
        Instance genInstance = instance;
        String source2 = (String) genInstance.getSource();
        assertSame("Source is the same object.", source, source2);
        assertEquals("Souce's classes are the same.", source.getClass(), source2.getClass());
        assertEquals("Sources are equal", source, source2);
    }

    @Test
    public void testToMatrix() {
        FeatureConfig featureConfig = new FeatureConfig();
        HashIndexer<Feature> features = new HashIndexer<Feature>();
        Instance<String> instance = new Instance<String>(features);
        Feature fTestNumber = new Feature("test", "number", Feature.Type.NUMBER);
        features.add(fTestNumber);
        Feature fTestNominal = new Feature("test", "nominal", Feature.Type.NOMINAL, CommonNominal.boolMap);
        features.add(fTestNominal);
        double testNumber = 100.0;
        String testNominal = "false";
        instance.setValue(fTestNominal, testNominal);
        instance.setValue(fTestNumber, testNumber);
        Instances instances = new Instances(features);
        instances.getInstances().add(instance);
        instances.getInstances().add(instance);
        instances.getInstances().add(instance);
        DenseMatrix64F XFull = DataPointUtils.matrix(instances, features, false);
        DenseMatrix64F XFullBias = DataPointUtils.matrix(instances, features, true);
        featureConfig.config("test.number,test.nominal");
        DenseMatrix64F XClassIndex = DataPointUtils.matrix(instances, DataPointUtils.denseFeatureMap(features, featureConfig, 1), false);
        featureConfig.config("test.nominal");
        DenseMatrix64F X1Feature = DataPointUtils.matrix(instances, DataPointUtils.denseFeatureMap(features, featureConfig, 3), false);
        assertEquals("Data matrix should have the same number of rows as the number of instances", instances.getInstances().size(), XFull.getNumRows());
        assertEquals("Data matrix should have the same number of columns as the number of features", features.size(), XFull.getNumCols());
        assertEquals("Adding bias should append one more column", features.size(), XFullBias.getNumCols() - 1);
        for (int i = 0; i < XFullBias.getNumRows(); i++) {
            assertEquals("Bias column is in the last column and has value of 1.", (Double) 1.0, (Double) XFullBias.get(i, XFullBias.getNumCols() - 1));
        }
        assertEquals("Class index feature is not in", features.size() - 1, XClassIndex.numCols);
        for (int i = 0; i < XClassIndex.getNumRows(); i++) {
            assertEquals("Every value should be " + testNumber, (Double) testNumber, (Double) XClassIndex.get(i, XClassIndex.getNumCols() - 1));
        }
        assertEquals("Unused feature is not in", features.size() - 1, X1Feature.numCols);
        for (int i = 0; i < X1Feature.getNumRows(); i++) {
            assertEquals("Every value should be '" + testNominal + "'", testNominal, CommonNominal.boolMap.getKey(X1Feature.get(i, XClassIndex.getNumCols() - 1)));
        }
    }
}
