package edu.northwestern.websail.classify.test;

import edu.northwestern.websail.classify.ranker.mm.SigmoidUnit;
import org.ejml.data.DenseMatrix64F;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author NorThanapon
 * @since 3/8/15
 */
public class TestMaxMarginFunctions {
    @Test
    public void testLogisticUnit() {
        SigmoidUnit unit = new SigmoidUnit(10, 10);
        DenseMatrix64F W;
        double[][] w = new double[][]{
                {-1, 2, 0.1},
                {2, 4, 0.001},
                {-3, 9, 10},
                {4, 1, -0.11},
                {5, -1, -10}
        };
        W = new DenseMatrix64F(w);
        W.set(0, 0, -1);
        unit.setWeights(W);
        assertEquals("L1 Norm is correct.", (Double) 36.211, (Double) unit.L1());
        assertEquals("L2 Norm is correct.", (Double) 232.022101, (Double) unit.L2Sqr());
        DenseMatrix64F X;
        double[][] x = new double[][]{
                {9, 3, 0, 3, 1},
                {0, 1, 0, 0, 1}
        };
        X = new DenseMatrix64F(x);
        DenseMatrix64F Y = unit.compute(X);
        double[][] y = new double[][]{
                {9.99999168e-01, 1.00000000e+00, 8.0514e-05},
                {9.99088949e-01, 9.52574127e-01, 4.5443e-05}
        };
        for (int i = 0; i < Y.getNumRows(); i++) {
            for (int j = 0; j < Y.getNumCols(); j++) {
                assertTrue("Activation is correct (to some decimal point).", Math.abs(Y.get(i, j) - y[i][j]) < 10e-10);
            }
        }
        DenseMatrix64F G = unit.grad(X, Y);
        double[][] g = new double[][]{
                {3.742e-06, 5.69544412e-14, 3.62283451e-04},
                {4.56357881e-04, 2.25883299e-02, 1.43481762e-04},
                {0.00000000e+00, 0.00000000e+00, 0.00000000e+00},
                {1.24729100e-06, 1.89848137e-14, 1.20761150e-04},
                {4.55526354e-04, 2.25883299e-02, 6.29743279e-05}
        };
        for (int i = 0; i < G.getNumRows(); i++) {
            for (int j = 0; j < G.getNumCols(); j++) {
                assertTrue("Gradient is correct (to some decimal point).", Math.abs(G.get(i, j) - g[i][j]) < 10e-10);
            }
        }
    }
}
