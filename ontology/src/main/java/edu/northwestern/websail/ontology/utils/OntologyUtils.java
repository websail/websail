package edu.northwestern.websail.ontology.utils;


import edu.northwestern.websail.ontology.model.Ontology;

public class OntologyUtils {

	private static final String DBPEDIA_DOMAIN = "dbpedia.org";

	public static String getClassNameFromURI(String URI) {
		return getLastSplitForDBPDomain(URI, "/");
	}

	public static String getInstanceNameFromURI(String URI) {
		return getLastSplitForDBPDomain(URI, "/");
	}

	private static String getLastSplitForDBPDomain(String str, String delimiter) {

		String[] parts = str.split(delimiter);
		if (parts[2].equalsIgnoreCase(DBPEDIA_DOMAIN)
				|| parts[parts.length - 1].equals(Ontology.ROOT_CLASSNAME))
			return parts[parts.length - 1];
		else
			return "";
	}
}
