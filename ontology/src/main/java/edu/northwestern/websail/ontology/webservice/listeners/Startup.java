package edu.northwestern.websail.ontology.webservice.listeners;

import edu.northwestern.websail.ontology.model.OntologyClass;
import edu.northwestern.websail.ontology.model.WebSailDbpStore;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.XMLPropertiesConfiguration;
import org.xml.sax.SAXException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class Startup implements ServletContextListener {

    public void contextDestroyed(ServletContextEvent arg0) {

    }

    public void contextInitialized(ServletContextEvent arg0) {

        try {
            Configuration config = new PropertiesConfiguration(
                    "config/sys.properties");
            String type = config.getString("sys.type");

            System.out.println("Found system type = " + type);
            XMLPropertiesConfiguration xmlConfig = new XMLPropertiesConfiguration(
                    "config/" + type + "/dbpConfigurations.xml");

            String owlFileName = xmlConfig.getString("dbpediaFiles.owlFile");
            String instanceFileName = xmlConfig
                    .getString("dbpediaFiles.instanceFile");

            WebSailDbpStore<OntologyClass> dbp = new WebSailDbpStore<OntologyClass>();
            try {
                System.out.println(owlFileName + "\t" + instanceFileName);
                dbp.loadDbpStore(owlFileName, instanceFileName, OntologyClass.class);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            arg0.getServletContext().setAttribute("ontology", dbp);

        } catch (ConfigurationException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
