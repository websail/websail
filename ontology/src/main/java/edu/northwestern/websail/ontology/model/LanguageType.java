package edu.northwestern.websail.ontology.model;

public enum LanguageType {

    ENGLISH, FRENCH, GREEK, GERMAN, POLISH, SLOVENIAN, ITALIAN, TURKISH, BENGALI,
    RUSSIAN, IRISH, KOREAN, PORTUGUESE, SPANISH, CATALAN, JAPANESE, DUTCH, Basque,
    Indonesian, Arabic;

    public static LanguageType findByAbbr(String abbr) {

        if (abbr.equalsIgnoreCase("en")) {
            return LanguageType.ENGLISH;
        } else if (abbr.equalsIgnoreCase("fr")) {
            return LanguageType.FRENCH;
        } else if (abbr.equalsIgnoreCase("el")) {
            return LanguageType.GREEK;
        } else if (abbr.equalsIgnoreCase("de")) {
            return LanguageType.GERMAN;
        } else if (abbr.equalsIgnoreCase("ko")) {
            return LanguageType.KOREAN;
        } else if (abbr.equalsIgnoreCase("pt")) {
            return LanguageType.PORTUGUESE;
        } else if (abbr.equalsIgnoreCase("es")) {
            return LanguageType.SPANISH;
        } else if (abbr.equalsIgnoreCase("pl")) {
            return LanguageType.POLISH;
        } else if (abbr.equalsIgnoreCase("sl")) {
            return LanguageType.SLOVENIAN;
        } else if (abbr.equalsIgnoreCase("it")) {
            return LanguageType.ITALIAN;
        } else if (abbr.equalsIgnoreCase("tr")) {
            return LanguageType.TURKISH;
        } else if (abbr.equalsIgnoreCase("bn")) {
            return LanguageType.BENGALI;
        } else if (abbr.equalsIgnoreCase("ru")) {
            return LanguageType.RUSSIAN;
        } else if (abbr.equalsIgnoreCase("ga")) {
            return LanguageType.IRISH;
        } else if (abbr.equalsIgnoreCase("ca")) {
            return LanguageType.CATALAN;
        } else if (abbr.equalsIgnoreCase("ja")) {
            return LanguageType.JAPANESE;
        } else if (abbr.equalsIgnoreCase("nl")) {
            return LanguageType.DUTCH;
        } else if (abbr.equalsIgnoreCase("eu")) {
            return LanguageType.Basque;
        } else if (abbr.equalsIgnoreCase("id")) {
            return LanguageType.Indonesian;
        } else if (abbr.equalsIgnoreCase("ar")) {
            return LanguageType.Arabic;
        }
        return null;

    }

}
