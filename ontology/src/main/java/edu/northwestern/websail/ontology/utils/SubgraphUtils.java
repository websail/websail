package edu.northwestern.websail.ontology.utils;


import edu.northwestern.websail.ontology.model.Ontology;
import edu.northwestern.websail.ontology.model.OntologyClass;
import edu.northwestern.websail.ontology.model.OntologyInstance;
import edu.northwestern.websail.ontology.model.WebSailDbpStore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class SubgraphUtils {

    @SuppressWarnings("unchecked")
    public static InstanceSubgraph createSubgraphForInstance(
            WebSailDbpStore dbpoi, String instanceName) {

        InstanceSubgraph subGraph = new InstanceSubgraph();

        Ontology ontology = dbpoi.getOntology();
        HashMap<String, OntologyInstance> instances = dbpoi.getInstances();

        OntologyInstance inst = instances.get(instanceName);

        HashSet<String> typesList = inst.getClassTypes();

//        expandTypesList(dbpoi, typesList);
        addAllNodes(subGraph, typesList);
        addAllEdges(ontology, subGraph,
                typesList);

        return subGraph;
    }

    private static void addAllNodes(InstanceSubgraph subGraph,
                                    HashSet<String> typesList) {
        for (String typeName : typesList) {
            subGraph.addNode(typeName);
        }

    }

    private static <T extends OntologyClass> void addAllEdges(Ontology<T> ontology,
                                                              InstanceSubgraph subGraph,
                                                              HashSet<String> typesList) {

        for (String typeName : typesList) {

            HashSet<String> parents = ontology.getOntologyClasses()
                    .get(typeName).getParentClasses();

            for (String parentName : parents) {
                subGraph.addEdge(parentName, typeName);
            }
        }
    }

    public static <T extends OntologyClass> ArrayList<ArrayList<String>> getPathsFromRoot(WebSailDbpStore<T> dbpoi,
                                                                                          String instanceName) {

        InstanceSubgraph subGraph = createSubgraphForInstance(dbpoi,
                instanceName);

        ArrayList<ArrayList<String>> paths = new ArrayList<ArrayList<String>>();
        ArrayList<String> path = new ArrayList<String>();

        path.add(Ontology.ROOT_CLASSNAME);
        getAllPaths(subGraph, instanceName, Ontology.ROOT_CLASSNAME, paths,
                path);

        return paths;
    }

    @SuppressWarnings("unchecked")
    private static void getAllPaths(InstanceSubgraph subGraph,
                                    String targetInstanceName, String currentNodeName,
                                    ArrayList<ArrayList<String>> paths, ArrayList<String> path) {

        HashSet<String> children = subGraph.getChildren().get(currentNodeName);
        if (children == null) {
            path.add(targetInstanceName);
            paths.add((ArrayList<String>) path.clone());
            path.remove(path.size() - 1);
            return;
        }

        for (String child : children) {
            path.add(child);
            getAllPaths(subGraph, targetInstanceName, child, paths, path);
            path.remove(path.size() - 1);
        }

    }

    public static <T extends OntologyClass> void expandTypesList(WebSailDbpStore<T> dbpoi, HashSet<String> typesList) {

        ArrayList<String> allTypes = new ArrayList<String>();
        for (String typeName : typesList) {
            allTypes.addAll(getAllAncestors(dbpoi, typeName));
        }
        typesList.addAll(allTypes);
    }

    private static <T extends OntologyClass> HashSet<String> getAllAncestors(WebSailDbpStore<T> dbpoi,
                                                                             String ontoClassName) {
        HashSet<String> allAncestors = new HashSet<String>();
        HashSet<String> parents = dbpoi.getOntology().getOntologyClasses().get(ontoClassName).getParentClasses();
        for (String parent : parents) {
            allAncestors.addAll(getAllAncestors(dbpoi, parent));
        }
        return allAncestors;
    }

}
