package edu.northwestern.websail.ontology.model;

import java.util.HashMap;

public class Ontology<T extends OntologyClass> {

    public static final String ROOT_CLASSNAME = "owl#Thing";
    HashMap<String, T> ontologyClasses;
    final HashMap<String, HashMap<ResourceSourceType, String>> classResourceIDMap = new HashMap<String, HashMap<ResourceSourceType, String>>();
    //    InstanceSubgraph ontologyGraph = new InstanceSubgraph();
    private String version;
    private LanguageType language;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public LanguageType getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        if (language.equalsIgnoreCase("en"))
            this.language = LanguageType.ENGLISH;
    }

    public void setLanguage(LanguageType language) {
        this.language = language;
    }

    public HashMap<String, T> getOntologyClasses() {
        return ontologyClasses;
    }

    public void setOntologyClasses(HashMap<String, T> owlClasses) {
        this.ontologyClasses = owlClasses;
    }

    public OntologyClass getOntologyClassForType(String name) {
        return ontologyClasses.get(name);
    }

    public void addClassResourceSource(String resourceID) {
        String[] parts = resourceID.split("/");
        String className = parts[parts.length - 1];
        if (!classResourceIDMap.containsKey(className))
            classResourceIDMap.put(className,
                    new HashMap<ResourceSourceType, String>());

        classResourceIDMap.get(className).put(
                ResourceSourceType.findByURI(resourceID), resourceID);

    }

    public String getClassNameResourceBestMatch(String className) {
        if (classResourceIDMap.containsKey(className)) {

            if (classResourceIDMap.get(className).containsKey(
                    ResourceSourceType.DBPEDIA))
                return classResourceIDMap.get(className).get(
                        ResourceSourceType.DBPEDIA);
            else if (classResourceIDMap.get(className).containsKey(
                    ResourceSourceType.SCHEMA))
                return classResourceIDMap.get(className).get(
                        ResourceSourceType.SCHEMA);
            else if (classResourceIDMap.get(className).containsKey(
                    ResourceSourceType.W3))
                return classResourceIDMap.get(className).get(
                        ResourceSourceType.W3);
            else if (classResourceIDMap.get(className).containsKey(
                    ResourceSourceType.PURL))
                return classResourceIDMap.get(className).get(
                        ResourceSourceType.PURL);
            else if (classResourceIDMap.get(className).containsKey(
                    ResourceSourceType.XMLNS))
                return classResourceIDMap.get(className).get(
                        ResourceSourceType.XMLNS);
            else
                return "";
        } else
            return "";
    }

    public String getClassNameResourceBySource(String className,
                                               ResourceSourceType code) {
        if (classResourceIDMap.containsKey(className))
            return classResourceIDMap.get(className).get(code);
        else
            return "";
    }

    public <T2> Class buildGenericInstance(Class<T2> tClass) throws IllegalAccessException, InstantiationException {
        return tClass.getClass().newInstance();
    }

//    public void initSubGraph() {
//        if (ontologyGraph.getNodes().size() != 0)
//            return;
//
//        for (String nodeName : ontologyClasses.keySet())
//            ontologyGraph.addNode(nodeName);
//
//        for (String nodeName : ontologyClasses.keySet()) {
//            HashSet<String> parents = ontologyClasses
//                    .get(nodeName).getParentClasses();
//
//            for (String parentName : parents) {
//                ontologyGraph.addEdge(parentName, nodeName);
//            }
//        }
//    }
//
//    public InstanceSubgraph getOntologyGraph() {
//        return ontologyGraph;
//    }
//
//    public void setOntologyGraph(InstanceSubgraph ontologyGraph) {
//        this.ontologyGraph = ontologyGraph;
//    }
}
