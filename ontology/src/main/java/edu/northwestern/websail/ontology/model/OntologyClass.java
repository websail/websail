package edu.northwestern.websail.ontology.model;

import java.util.HashMap;
import java.util.HashSet;

public class OntologyClass {

    Boolean isLeaf = false;
    private String resourceID;
    private HashMap<LanguageType, String> labels = new HashMap<LanguageType, String>();
    private String description;
    private HashSet<String> subClasses = new HashSet<String>();
    private HashSet<String> parentClasses = new HashSet<String>();
    private HashSet<String> disjointClasses = new HashSet<String>();
    private HashSet<String> equivalentClasses = new HashSet<String>();
    private final HashSet<String> leafInstances = new HashSet<String>();

    public OntologyClass() {
    }

    public OntologyClass(String resourceID) {
        this.resourceID = resourceID;
    }

    public HashSet<String> getParentClasses() {
        return parentClasses;
    }

    public void setParentClasses(HashSet<String> parentClasses) {
        this.parentClasses = parentClasses;
    }

    public String getResourceID() {
        return resourceID;
    }

    public void setResourceID(String resourceID) {
        this.resourceID = resourceID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void addLabel(String lang, String label) {
        if (LanguageType.findByAbbr(lang) != null) {
            labels.put(LanguageType.findByAbbr(lang), label);
        } else {
            System.out.println("MISSING LANG CODE:" + lang);
        }
    }

    public void addParentClass(String resourceID) {
        parentClasses.add(resourceID);
    }

    public void addSubClass(String resourceID) {
        subClasses.add(resourceID);
    }

    public HashMap<LanguageType, String> getLabels() {
        return labels;
    }

    public void setLabels(HashMap<LanguageType, String> labels) {
        this.labels = labels;
    }

    public HashSet<String> getSubClasses() {
        return subClasses;
    }

    public void setSubClasses(HashSet<String> subClasses) {
        this.subClasses = subClasses;
    }

    public HashSet<String> getDisjointClasses() {
        return disjointClasses;
    }

    public void setDisjointClasses(HashSet<String> disjointClasses) {
        this.disjointClasses = disjointClasses;
    }

    public HashSet<String> getEquivalentClasses() {
        return equivalentClasses;
    }

    public void setEquivalentClasses(HashSet<String> equivalentClasses) {
        this.equivalentClasses = equivalentClasses;
    }

    public Boolean isLeaf() {
        return isLeaf;
    }

    public void setIsLeaf(Boolean leaf) {
        isLeaf = leaf;
    }

    public void addLeafInstance(String name) {
        leafInstances.add(name);
    }

    public HashSet<String> getLeafInstances() {
        return leafInstances;
    }
}
