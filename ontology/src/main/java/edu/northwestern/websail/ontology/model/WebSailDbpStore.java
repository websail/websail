package edu.northwestern.websail.ontology.model;

import edu.northwestern.websail.ontology.utils.OntologyGraphUtils;
import edu.northwestern.websail.ontology.utils.OntologyUtils;
import edu.northwestern.websail.ontology.utils.SubgraphUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WebSailDbpStore<T extends OntologyClass> {

    final Ontology<T> ontology = new Ontology<T>();
    HashMap<String, OntologyInstance> instances;

    public Ontology<T> getOntology() {
        return ontology;
    }

    public HashMap<String, OntologyInstance> getInstances() {
        return instances;
    }

    public <T2 extends OntologyClass> void loadDbpStore(String owlFile, String instancesFile, Class<T2> tClass) throws IOException, SAXException, ParserConfigurationException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        System.out.println("DEBUG: Creating Ontology Graph from file "
                + owlFile + " ...");
        loadOntologyFromOwlFile(owlFile, tClass);
        loadInstances(instancesFile);
        setLeafClasses();
//        this.ontology.initSubGraph();
        System.out.println("DEBUG: Done loading data into memory .. ");
    }

    private <T2 extends OntologyClass> void loadOntologyFromOwlFile(String owlFile, Class<T2> z)
            throws ParserConfigurationException,
            SAXException, IOException, IllegalAccessException, ClassNotFoundException, InstantiationException {

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(new FileInputStream(owlFile));

        OntologyGraphUtils.setOntologyProperties(doc, ontology);
        OntologyGraphUtils.setOntologyClasses(doc, ontology, z);
    }

    private void loadInstances(
            String instanceTypesFile) throws IOException {

        this.instances = new HashMap<String, OntologyInstance>(13225167);
        BufferedReader in = new BufferedReader(new FileReader(new File(
                instanceTypesFile)));
        String line;
        String instanceName;
        int lineNum = 1;
        while ((line = in.readLine()) != null) {

            if (line.startsWith("#"))
                continue;

            String[] ntTriplet = OntologyGraphUtils
                    .getNtTripletFromNtLine(line);
            instanceName = OntologyUtils.getInstanceNameFromURI(ntTriplet[0]);


            OntologyInstance inst = instances.containsKey(instanceName) ? instances
                    .get(instanceName) : new OntologyInstance(instanceName,
                    ntTriplet[0]);
            inst.addType(OntologyUtils.getClassNameFromURI(ntTriplet[2]));
            instances.put(instanceName, inst);
            if (lineNum % 100000 == 0) {
                System.out.print(lineNum + "...");
                if (lineNum % 1000000 == 0) {
                    System.out.print("\n");
                }
            }
            lineNum++;

        }
        in.close();

        System.out.println(lineNum);

    }

    private void setLeafClasses() {

        for (Map.Entry<String, OntologyInstance> instance : instances.entrySet()) {
            ArrayList<ArrayList<String>> paths = SubgraphUtils.getPathsFromRoot(this, instance.getValue().getInstanceName());
            for (ArrayList<String> path : paths) {
                Integer pathLen = path.size();
                T ontClass = ontology.getOntologyClasses().get(path.get(pathLen - 2));
                ontClass.setIsLeaf(true);
                ontClass.addLeafInstance(instance.getValue().getInstanceName());
            }
        }
    }


}
