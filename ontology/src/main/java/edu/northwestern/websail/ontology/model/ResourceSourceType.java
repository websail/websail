package edu.northwestern.websail.ontology.model;

public enum ResourceSourceType {

	DBPEDIA, SCHEMA, PURL, W3, XMLNS;

	public static ResourceSourceType findByURI(String URI) {

		String abbr = URI.split("/")[2];

		if (abbr.equalsIgnoreCase("dbpedia.org")) {
			return DBPEDIA;
		} else if (abbr.equalsIgnoreCase("schema.org")) {
			return SCHEMA;
		} else if (abbr.equalsIgnoreCase("purl.org")) {
			return PURL;
		} else if (abbr.equalsIgnoreCase("www.w3.org")) {
			return W3;
		} else if (abbr.equalsIgnoreCase("xmlns.com")) {
			return XMLNS;
		}

		return null;
	}

}
