package edu.northwestern.websail.ontology.webservice.endpoints;


import edu.northwestern.websail.ontology.model.Ontology;
import edu.northwestern.websail.ontology.model.OntologyClass;
import edu.northwestern.websail.ontology.model.WebSailDbpStore;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

@Path("/ontology/")
public class OntologyService {

    @Context
    ServletContext context;

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/info")
    public String getOntologyInfo() {

        WebSailDbpStore dbpoi = (WebSailDbpStore) context
                .getAttribute("ontology");
        Ontology ontology = dbpoi.getOntology();
        return ontology.getVersion() + " Language=" + ontology.getLanguage()
                + "\n" + Ontology.ROOT_CLASSNAME;
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/class/{class}")
    public OntologyClass getClassDetails(@PathParam("class") String className) {

        WebSailDbpStore dbpoi = (WebSailDbpStore) context
                .getAttribute("ontology");
        Ontology ontology = dbpoi.getOntology();
        return (OntologyClass) ontology.getOntologyClasses().get(ontology.getClassNameResourceBestMatch(className));
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/listLeaves")
    @SuppressWarnings("unchecked")
    public ArrayList<String> getLeafClasses() {

        ArrayList<String> leafClasses = new ArrayList<String>();

        WebSailDbpStore<OntologyClass> dbpoi = (WebSailDbpStore<OntologyClass>) context
                .getAttribute("ontology");
        HashMap<String, OntologyClass> ontologyClasses = dbpoi.getOntology().getOntologyClasses();
        for (Map.Entry<String, OntologyClass> e : ontologyClasses.entrySet()) {
            if (e.getValue().isLeaf())
                leafClasses.add(e.getKey());
        }
        return leafClasses;
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/listClasses")
    @SuppressWarnings("unchecked")
    public HashMap<String, OntologyClass> getOntologyClasses() {

        WebSailDbpStore<OntologyClass> dbpoi = (WebSailDbpStore<OntologyClass>) context
                .getAttribute("ontology");
        return dbpoi.getOntology().getOntologyClasses();
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/listClassNames")
    @SuppressWarnings("unchecked")
    public java.util.Set<String> getOntologyClassNames() {

        WebSailDbpStore<OntologyClass> dbpoi = (WebSailDbpStore<OntologyClass>) context
                .getAttribute("ontology");
        HashMap<String, OntologyClass> ontologyClasses = dbpoi.getOntology().getOntologyClasses();
        return ontologyClasses.keySet();
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/subclasses/class/{class}")
    @SuppressWarnings("unchecked")
    public HashSet<String> getSubClasses(@PathParam("class") String className) {

        try {
            className = URLDecoder.decode(className, "UTF-8");
            WebSailDbpStore<OntologyClass> dbpoi = (WebSailDbpStore<OntologyClass>) context
                    .getAttribute("ontology");
            Ontology ontology = dbpoi.getOntology();
            return ontology.getOntologyClassForType(className).getSubClasses();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return new HashSet<String>();
    }


}
