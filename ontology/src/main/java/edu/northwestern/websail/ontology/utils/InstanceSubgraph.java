package edu.northwestern.websail.ontology.utils;

import java.util.HashMap;
import java.util.HashSet;

public class InstanceSubgraph {

	HashSet<String> nodes = new HashSet<String>();
	HashMap<String, HashSet<String>> children = new HashMap<String, HashSet<String>>();
	

	public void addNode(String nodeURI) {
		nodes.add(nodeURI);
	}

	public void addEdge(String fromRsrcURI, String toRsrcURI) {

		if (!nodes.contains(fromRsrcURI) || !nodes.contains(toRsrcURI))
			return;

		if (!children.containsKey(fromRsrcURI))
			children.put(fromRsrcURI, new HashSet<String>());

		children.get(fromRsrcURI).add(toRsrcURI);
	}

	public HashSet<String> getNodes() {
		return nodes;
	}

	public void setNodes(HashSet<String> nodes) {
		this.nodes = nodes;
	}

	public HashMap<String, HashSet<String>> getChildren() {
		return children;
	}

	public void setChildren(HashMap<String, HashSet<String>> children) {
		this.children = children;
	}

}
