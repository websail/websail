package edu.northwestern.websail.ontology.webservice.endpoints;


import edu.northwestern.websail.ontology.model.OntologyClass;
import edu.northwestern.websail.ontology.model.OntologyInstance;
import edu.northwestern.websail.ontology.model.WebSailDbpStore;
import edu.northwestern.websail.ontology.utils.SubgraphUtils;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

@Path("/instances")
public class InstanceService {

    @Context
    ServletContext context;

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/info/{title}")
    @SuppressWarnings("unchecked")
    public OntologyInstance getInstanceDetails(@PathParam("title") String title) {

        WebSailDbpStore<OntologyClass> dbpoi = (WebSailDbpStore<OntologyClass>) context
                .getAttribute("ontology");
        HashMap<String, OntologyInstance> instances = dbpoi.getInstances();
        return instances.get(title);
    }

//    @GET
//    @Produces("application/json; charset=UTF-8;")
//    @Path("/subGraph/{title}")
//    public InstanceSubgraph getInstanceSubGraph(@PathParam("title") String title) {
//
//        WebSailDbpStore dbpoi = (WebSailDbpStore) context
//                .getAttribute("ontology");
//
//        return SubgraphUtils.createSubgraphForInstance(dbpoi, dbpoi
//                .getInstances().get(title).getInstanceResourceID());
//    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/instancePath/{title}")
    @SuppressWarnings("unchecked")
    public ArrayList<ArrayList<String>> getInstanceOntologyPath(
            @PathParam("title") String title) {

        WebSailDbpStore dbpoi = (WebSailDbpStore) context
                .getAttribute("ontology");
        title = title.replaceAll(" ", "_");

        if (!dbpoi.getInstances().containsKey(title)) {
            return new ArrayList<ArrayList<String>>();
        } else
            return SubgraphUtils.getPathsFromRoot(dbpoi, title);
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/class/{leafClass}")
    public HashSet<String> getInstancesUnderLeafNode(@PathParam("leafClass") String leafClass) {
        WebSailDbpStore dbpoi = (WebSailDbpStore) context
                .getAttribute("ontology");
        OntologyClass c = (OntologyClass) dbpoi.getOntology().getOntologyClasses().get(leafClass);
        if (c == null)
            return new HashSet<String>();
        return c.getLeafInstances();
    }
}
