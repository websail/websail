package edu.northwestern.websail.ontology.utils;

import edu.northwestern.websail.ontology.model.Ontology;
import edu.northwestern.websail.ontology.model.OntologyClass;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.HashMap;
import java.util.Map;

public class OntologyGraphUtils {

    private static final String ONTOLOGY_PROPERTIES_TAG_NAME = "owl:Ontology";
    private static final String CLASS_TAG_NAME = "owl:Class";

    private static final String EQ_CLASS_TAG_NAME = "owl:equivalentClass";
    private static final String DISJOINT_CLASS_TAG_NAME = "owl:disjointWith";
    // private static final String DATATYPE_PROPERTY_TAG_NAME =
    // "owl:DatatypeProperty";
    // private static final String OBJECT_PROPERTY_TAG_NAME =
    // "owl:ObjectProperty";

    private static final String LABEL_TAG_NAME = "rdfs:label";
    private static final String COMMENT_TAG_NAME = "rdfs:comment";
    private static final String PARENT_CLASS_TAG_NAME = "rdfs:subClassOf";

    private static final String LANG_ATTR_NAME = "xml:lang";
    private static final String ABOUT_ATTR_NAME = "rdf:about";
    private static final String RESOURCE_ATTR_NAME = "rdf:resource";

    public static <T extends OntologyClass> void setOntologyProperties(Document doc, Ontology<T> ontology) {
        NodeList ontologyPropertyNodes = doc
                .getElementsByTagName(ONTOLOGY_PROPERTIES_TAG_NAME);
        Node ontologyPropertyNode = ontologyPropertyNodes.item(0);
        NodeList versionInfoNodes = ontologyPropertyNode.getChildNodes();
        for (int i = 0; i < versionInfoNodes.getLength(); i++) {
            if (versionInfoNodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
                ontology.setVersion(versionInfoNodes.item(i).getTextContent());
                String langCode = versionInfoNodes.item(i).getAttributes()
                        .getNamedItem(LANG_ATTR_NAME).getTextContent();
                ontology.setLanguage(langCode);
                break;
            }
        }

    }

    @SuppressWarnings("unchecked")
    public static <T extends OntologyClass> void setOntologyClasses(Document doc, Ontology<T> ontology, Class tClass) throws IllegalAccessException, InstantiationException, ClassNotFoundException {

        HashMap<String, T> owlClasses = new HashMap<String, T>();

        // "owl:Class" nodes
        NodeList owlClassNodes = doc.getElementsByTagName(CLASS_TAG_NAME);
        for (int i = 0; i < owlClassNodes.getLength(); i++) {
            Node classNode = owlClassNodes.item(i);

            String resourceURI = nodeAttribValue(classNode, ABOUT_ATTR_NAME);
            if (resourceURI == null) {
                continue;
            }

            String resourceName = OntologyUtils
                    .getClassNameFromURI(resourceURI);


            T c = owlClasses.get(resourceName) == null ? (T) getInstance(resourceName, tClass)
                    : owlClasses.get(resourceName);
            c.setResourceID(resourceName);

            // Details of each class.
            NodeList classChildNodes = classNode.getChildNodes();
            for (int j = 0; j < classChildNodes.getLength(); j++) {
                if (classChildNodes.item(j).getNodeType() == Node.ELEMENT_NODE) {
                    Node child = classChildNodes.item(j);

                    if (child.getNodeName().equalsIgnoreCase(LABEL_TAG_NAME)) {
                        String lang = nodeAttribValue(child, LANG_ATTR_NAME);
                        if (lang != null)
                            c.addLabel(lang, child.getTextContent());
                    } else if (child.getNodeName().equalsIgnoreCase(
                            COMMENT_TAG_NAME)) {
                        c.setDescription(child.getTextContent());
                    } else if (child.getNodeName().equalsIgnoreCase(
                            PARENT_CLASS_TAG_NAME)) {
                        String parentClassName = OntologyUtils
                                .getClassNameFromURI(nodeAttribValue(child,
                                        RESOURCE_ATTR_NAME));
                        c.addParentClass(parentClassName);
                        if (!owlClasses.containsKey(parentClassName)) {
                            owlClasses.put(parentClassName, (T) getInstance(parentClassName, tClass));
                        }
                        owlClasses.get(parentClassName).addSubClass(
                                resourceName);
                    } else if (child.getNodeName().equalsIgnoreCase(
                            EQ_CLASS_TAG_NAME)) {
                        c.getEquivalentClasses().add(nodeAttribValue(child,
                                RESOURCE_ATTR_NAME));
                    } else if (child.getNodeName().equalsIgnoreCase(
                            DISJOINT_CLASS_TAG_NAME)) {
                        c.getDisjointClasses().add(nodeAttribValue(child,
                                RESOURCE_ATTR_NAME));
                    }
                }
            }
            owlClasses.put(resourceName, c);
        }

        fixDanglingClasses(owlClasses);
        ontology.setOntologyClasses(owlClasses);

    }

    private static <T extends OntologyClass> void fixDanglingClasses(HashMap<String, T> owlClasses) {
        for (Map.Entry<String, T> owlClassEntry : owlClasses.entrySet()) {
            if (owlClassEntry.getValue().getParentClasses().size() == 0) {
                if (!owlClassEntry.getKey().equalsIgnoreCase(Ontology.ROOT_CLASSNAME)) {
                    System.out.println(owlClassEntry.getKey());
                    owlClassEntry.getValue().addParentClass(Ontology.ROOT_CLASSNAME);
                    owlClasses.get(Ontology.ROOT_CLASSNAME).addSubClass(owlClassEntry.getKey());
                }
            }
        }
    }


    private static String nodeAttribValue(Node n, String attr) {
        return n.getAttributes().getNamedItem(attr) == null ? null : n
                .getAttributes().getNamedItem(attr).getTextContent();

    }

    public static String[] getNtTripletFromNtLine(String line) {
        String[] parts = line.split(" ");
        String[] ret = new String[3];
        for (int i = 0; i < 3; i++) {
            ret[i] = parts[i].substring(1, parts[i].length() - 1);

        }

        return ret;
    }

    @SuppressWarnings("unchecked")
    private static <T extends OntologyClass> T getInstance(String resourceName, Class<T> tClass) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        T obj = (T) Class.forName(tClass.getName()).newInstance();
        obj.setResourceID(resourceName);
        return obj;
    }

}
