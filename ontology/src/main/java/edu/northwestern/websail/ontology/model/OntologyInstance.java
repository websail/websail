package edu.northwestern.websail.ontology.model;

import java.util.HashSet;

public class OntologyInstance {

	private String instanceName;
	private String instanceResourceID;
	private HashSet<String> classTypes = new HashSet<String>();

	public OntologyInstance(String className, String URI) {

		this.instanceName = className;
		this.instanceResourceID = URI;
	}

	public void addType(String typeURI) {
		classTypes.add(typeURI);
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public String getInstanceResourceID() {
		return instanceResourceID;
	}

	public void setInstanceResourceID(String instanceResourceID) {
		this.instanceResourceID = instanceResourceID;
	}

	public HashSet<String> getClassTypes() {
		return classTypes;
	}

	public void setClassTypes(HashSet<String> classTypes) {
		this.classTypes = classTypes;
	}

}
