package edu.northwestern.websail.lOntology.ds;

/**
 * @author csbhagav on 8/22/14.
 */
public class PairIntDouble {
    Integer intElement;
    Double doubleElement;

    public PairIntDouble(Integer intElement, Double doubleElement) {
        this.intElement = intElement;
        this.doubleElement = doubleElement;
    }

    public Integer getIntElement() {
        return intElement;
    }

    public void setIntElement(Integer intElement) {
        this.intElement = intElement;
    }

    public Double getDoubleElement() {
        return doubleElement;
    }

    public void setDoubleElement(Double doubleElement) {
        this.doubleElement = doubleElement;
    }

    public String toString() {
        return intElement + "\t" + doubleElement;
    }


}
