package edu.northwestern.websail.lOntology.main;


import edu.northwestern.websail.core.io.FileReadUtils;
import edu.northwestern.websail.lOntology.adapter.EntityVectorsAdapter;
import edu.northwestern.websail.lOntology.ds.PairIntDouble;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author csbhagav on 9/11/14.
 */
public class TableWord2Vec {

    final EntityVectorsAdapter adapter;

    public TableWord2Vec(EntityVectorsAdapter adapter) {
        this.adapter = adapter;
    }

    public static void main(String[] args) throws IOException {

        String idTitleFile = "/websail/common/wikification/data/en/en_id_title.map";
        String titleIdFile = "/websail/common/wikification/data/en/redirect.map";

        String vectorsFile = args[0];
        EntityVectorsAdapter adapter = new EntityVectorsAdapter();
        adapter.readVectorsFile(vectorsFile);
        System.out.println("Number of vectors = " + adapter.getEntityVectors().size());

        HashMap<Integer, String> idTitleMap = FileReadUtils.getMapIntToString(idTitleFile);
        HashMap<String, Integer> titleIdMap = FileReadUtils.getMapStringToInt(titleIdFile);

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String entityStr;
        ArrayList<PairIntDouble> entitySimilarities;
        do {
            System.out.print("\nEnter entity name:\t");
            entityStr = br.readLine();
            String[] parts = entityStr.split(",");
            Integer[] entityIds = new Integer[parts.length];
            for (int i = 0; i < parts.length; i++) {
                if (titleIdMap.containsKey(parts[i].trim())) {
                    entityIds[i] = titleIdMap.get(parts[i].trim());
                } else {
                    System.out.print("Entity Not found ... " + parts[i]);
                }
            }

            entitySimilarities = adapter.setExpansion(entityIds);
            System.out.println("\n*********\n");
            System.out.println("Title\tId\tSimilarity");
            for (int i = 0; i < 20 && i < entitySimilarities.size(); i++) {
                System.out.println(idTitleMap.get(entitySimilarities.get(i).getIntElement()) + "\t" +
                        entitySimilarities.get(i).toString());
            }
            System.out.println("\n*********\n");

        } while (!entityStr.equals("-1"));
    }
}
