package edu.northwestern.websail.lOntology.adapter;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import edu.northwestern.websail.core.io.FileReadUtils;
import edu.northwestern.websail.lOntology.ds.PairIntDouble;
import edu.northwestern.websail.lOntology.model.EntityLatentRepresentation;
import org.la4j.vector.Vector;
import org.la4j.vector.Vectors;
import org.la4j.vector.dense.BasicVector;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author csbhagav on 8/22/14.
 */
public class EntityVectorsAdapter {

    final Logger logger = Logger.getLogger(EntityVectorsAdapter.class.getName());
    TreeMap<Integer, EntityLatentRepresentation> entityVectors = new TreeMap<Integer, EntityLatentRepresentation>();

    public static double cosineSimilarity(Vector v1, double v1Norm, Vector v2, Double v2Norm) {
        return v1.innerProduct(v2) / (v1Norm * v2Norm);
    }

    public static double cosineSimilarity(Vector v1, Vector v2) {
        double norm1 = Math.sqrt(v1.innerProduct(v1));
        double norm2 = Math.sqrt(v2.innerProduct(v2));
        return cosineSimilarity(v1, norm1, v2, norm2);
    }

    public static void main(String[] args) throws IOException {
        String vectorsFile = args.length > 0 ? args[0] : "/Users/csbhagav/Projects/word2vec/ev-small.dat";
        String idTitleFile = "/websail/common/wikification/data/en/en_id_title.map";
        String titleIdFile = "/websail/common/wikification/data/en/redirect.map";
        EntityVectorsAdapter adapter = new EntityVectorsAdapter();
        adapter.readVectorsFile(vectorsFile);
        System.out.println("Number of vectors = " + adapter.entityVectors.size());
        HashMap<Integer, String> idTitleMap = FileReadUtils.getMapIntToString(idTitleFile);
        HashMap<String, Integer> titleIdMap = FileReadUtils.getMapStringToInt(titleIdFile);

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String analogyStr;
        do {
            System.out.print("\nEnter 4 entity IDs:\t");
            analogyStr = br.readLine();
            String[] parts = analogyStr.split(" ");
            Integer arg1 = Integer.valueOf(parts[0]);
            Integer arg2 = Integer.valueOf(parts[1]);
            Integer arg3 = Integer.valueOf(parts[2]);
            Integer arg4 = Integer.valueOf(parts[3]);

            Vector v1 = adapter.getEntityVectors().get(arg1).getRepresentation();
            Vector v2 = adapter.getEntityVectors().get(arg2).getRepresentation();
            Vector v3 = adapter.getEntityVectors().get(arg3).getRepresentation();
            Vector v4 = adapter.getEntityVectors().get(arg4).getRepresentation();

            Vector rel1 = v2.subtract(v1).add(v3);
            double norm = Math.sqrt(rel1.innerProduct(rel1));
            EntityLatentRepresentation relationRepresentation = new EntityLatentRepresentation(rel1, norm);


            ArrayList<PairIntDouble> entitySimilarities = adapter.mostSimilaryEntity(relationRepresentation);
            System.out.println("\n*********\n");
            System.out.println("Title\tId\tSimilarity");
            for (int i = 0; i < 20 && i < entitySimilarities.size(); i++) {
                System.out.println(idTitleMap.get(entitySimilarities.get(i).getIntElement()) + "\t" +
                        entitySimilarities.get(i).toString());
            }
            System.out.println("\n*********\n");

            System.out.println("Relation Similarity = " + EntityVectorsAdapter.cosineSimilarity(rel1, v4));
        } while (!analogyStr.equalsIgnoreCase("-1"));


        br = new BufferedReader(new InputStreamReader(System.in));
        String entityStr;
        ArrayList<PairIntDouble> entitySimilarities;
        do {
            System.out.print("\nEnter entity name:\t");
            entityStr = br.readLine();
            Integer entityId = titleIdMap.get(entityStr);
            if (entityId == null) {
                System.out.print("Entity Not found ... ");
                continue;
            }

            entitySimilarities = adapter.mostSimilaryEntity(entityId);
            System.out.println("\n*********\n");
            System.out.println("Title\tId\tSimilarity");
            for (int i = 0; i < 20 && i < entitySimilarities.size(); i++) {
                System.out.println(idTitleMap.get(entitySimilarities.get(i).getIntElement()) + "\t" +
                        entitySimilarities.get(i).toString());
            }
            System.out.println("\n*********\n");

        } while (!entityStr.equals("-1"));

    }

    public void readVectorsFile(String vectorsFile) {
        int fieldOffset = 0;
        try {
            String binFile = vectorsFile + ".kryo.bin";
            File binaryFile = new File(binFile);
            if (binaryFile.exists()) {
                deserialize(binaryFile);
            } else {
                logger.info("Binary file not found. Reading from file:" + vectorsFile);
                BufferedReader in = new BufferedReader(new FileReader(new File(vectorsFile)));
                String line;
                int lineNum = 0;
                while ((line = in.readLine()) != null) {
                    String[] parts = line.split(" ");
                    if (parts[0].equals(".") || parts.length == 2) {
                        continue;
                    }
                    Integer pgId = Integer.valueOf(parts[fieldOffset]);
                    double[] elements = new double[parts.length - (1 + fieldOffset)];
                    for (int i = 1 + fieldOffset; i < parts.length; i++) {
                        elements[i - (1 + fieldOffset)] = Double.valueOf(parts[i]);
                    }
                    Vector v = new BasicVector(elements);
                    entityVectors.put(pgId, new EntityLatentRepresentation(v));
                    ++lineNum;
                    if (lineNum % 10000 == 0) {
                        System.out.print(lineNum + "...");
                    }
                }
                logger.info("Creating binary file for later ... ");
                this.serializeRepresentations(binFile);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public double findSimilarity(Integer pgId1, Integer pgId2) {
        EntityLatentRepresentation v1 = entityVectors.get(pgId1);
        EntityLatentRepresentation v2 = entityVectors.get(pgId2);
        return v1.cosineSimilarity(v2);
    }

    public double findSimilarity(EntityLatentRepresentation representation, Integer pgId2) {
        EntityLatentRepresentation v2 = entityVectors.get(pgId2);
        return representation.cosineSimilarity(v2);
    }

    public ArrayList<PairIntDouble> mostSimilaryEntity(Integer entityId) {

        ArrayList<PairIntDouble> pageSimilarityScores = new ArrayList<PairIntDouble>();
        for (Map.Entry<Integer, EntityLatentRepresentation> entry : entityVectors.entrySet()) {
            if (entry.getKey().intValue() == entityId.intValue())
                continue;

            Double sim = findSimilarity(entityId, entry.getKey());
            pageSimilarityScores.add(new PairIntDouble(entry.getKey(), sim));
        }

        Collections.sort(pageSimilarityScores, new Comparator<PairIntDouble>() {
            @Override
            public int compare(PairIntDouble pair1, PairIntDouble pair2) {
                int sigNum = (int) Math.signum(pair2.getDoubleElement() - pair1.getDoubleElement());
                if (sigNum == 0)
                    return pair1.getIntElement() - pair2.getIntElement();
                return sigNum;
            }
        });

        return pageSimilarityScores;
    }

    public ArrayList<PairIntDouble> mostSimilaryEntity(EntityLatentRepresentation representation) {

        ArrayList<PairIntDouble> pageSimilarityScores = new ArrayList<PairIntDouble>();
        for (Map.Entry<Integer, EntityLatentRepresentation> entry : entityVectors.entrySet()) {

            Double sim = findSimilarity(representation, entry.getKey());
            pageSimilarityScores.add(new PairIntDouble(entry.getKey(), sim));
        }

        Collections.sort(pageSimilarityScores, new Comparator<PairIntDouble>() {
            @Override
            public int compare(PairIntDouble pair1, PairIntDouble pair2) {
                int sigNum = (int) Math.signum(pair2.getDoubleElement() - pair1.getDoubleElement());
                if (sigNum == 0)
                    return pair1.getIntElement() - pair2.getIntElement();
                return sigNum;
            }
        });

        return pageSimilarityScores;
    }

    public ArrayList<PairIntDouble> mostSimilaryEntity(EntityLatentRepresentation representation,
                                                       ArrayList<PairIntDouble> subset) {

        ArrayList<PairIntDouble> pageSimilarityScores = new ArrayList<PairIntDouble>();

        for (PairIntDouble entry : subset) {
            Double sim = findSimilarity(representation, entry.getIntElement());
            pageSimilarityScores.add(new PairIntDouble(entry.getIntElement(), sim));
        }

        Collections.sort(pageSimilarityScores, new Comparator<PairIntDouble>() {
            @Override
            public int compare(PairIntDouble pair1, PairIntDouble pair2) {
                int sigNum = (int) Math.signum(pair2.getDoubleElement() - pair1.getDoubleElement());
                if (sigNum == 0)
                    return pair1.getIntElement() - pair2.getIntElement();
                return sigNum;
            }
        });

        return pageSimilarityScores;
    }

    public ArrayList<PairIntDouble> setExpansion(Integer[] entityIds) {

        HashSet<Integer> entityIdSet = new HashSet<Integer>(Arrays.asList(entityIds));

        Vector meanVector = new BasicVector(this.entityVectors.firstEntry().getValue().getRepresentation().length());
        int count = 0;
        for (Integer entityId : entityIds) {
            if (entityVectors.containsKey(entityId)) {
                meanVector = meanVector.add(entityVectors.get(entityId).getRepresentation());
                count++;
            }
        }
        meanVector = meanVector.divide(count);

        double meanVectorNorm = meanVector.fold(Vectors.mkEuclideanNormAccumulator());

        ArrayList<PairIntDouble> pageSimilarityScores = new ArrayList<PairIntDouble>();
        for (Map.Entry<Integer, EntityLatentRepresentation> entry : entityVectors.entrySet()) {
            if (entityIdSet.contains(entry.getKey()))
                continue;

            Vector entityVector = entry.getValue().getRepresentation();
            Double sim = cosineSimilarity(meanVector, meanVectorNorm, entityVector, entry.getValue().getVectorL2Norm());
            pageSimilarityScores.add(new PairIntDouble(entry.getKey(), sim));
        }

        Collections.sort(pageSimilarityScores, new Comparator<PairIntDouble>() {
            @Override
            public int compare(PairIntDouble pair1, PairIntDouble pair2) {
                int sigNum = (int) Math.signum(pair2.getDoubleElement() - pair1.getDoubleElement());
                if (sigNum == 0)
                    return pair1.getIntElement() - pair2.getIntElement();
                return sigNum;
            }
        });
        return pageSimilarityScores;
    }

    public void serializeRepresentations(String objectFile) throws FileNotFoundException {
        Kryo kryo = new Kryo();
        Output output = new Output(new BufferedOutputStream(new FileOutputStream(new File(objectFile))));
        kryo.writeObject(output, this.entityVectors);
        output.close();
    }

    @SuppressWarnings("unchecked")
    private void deserialize(File binFile) {
        try {
            Kryo kryo = new Kryo();
            Input input = new Input(new BufferedInputStream(new FileInputStream(binFile)));
            this.entityVectors = kryo.readObject(input, this.entityVectors.getClass());
            input.close();
        } catch (Exception e) {
            logger.severe("Failed to deserialize file");
            e.printStackTrace();
        }
    }

    public TreeMap<Integer, EntityLatentRepresentation> getEntityVectors() {
        return entityVectors;
    }

    public void setEntityVectors(TreeMap<Integer, EntityLatentRepresentation> entityVectors) {
        this.entityVectors = entityVectors;
    }
}
