package edu.northwestern.websail.lOntology.main;


import edu.northwestern.websail.core.io.FileReadUtils;
import edu.northwestern.websail.lOntology.adapter.EntityVectorsAdapter;
import edu.northwestern.websail.lOntology.ml.KMeans;
import edu.northwestern.websail.lOntology.model.Entity;
import edu.northwestern.websail.lOntology.model.EntityLatentRepresentation;

import java.io.*;
import java.util.*;

/**
 * @author csbhagav on 8/25/14.
 */
public class RecurseKMeans {

    static BufferedWriter out;

    public static void main(String[] args) throws IOException {
        String outputFile = args[0];
        out = new BufferedWriter(new FileWriter(new File(outputFile)));

        Properties config = new Properties();
        config.load(new FileInputStream("./config/global.config"));
        String vectorsFile = config.getProperty("entables.wvec");

        EntityVectorsAdapter vectorsAdapter = new EntityVectorsAdapter();
        vectorsAdapter.readVectorsFile(vectorsFile);

        HashMap<Integer, String> idTitleMap = FileReadUtils.getMapIntToString(config.getProperty("idTitleMap"));

        TreeMap<Integer, EntityLatentRepresentation> entityRepresentationMap = vectorsAdapter.getEntityVectors();
        int numEntities = entityRepresentationMap.size();

        Entity[] entities = new Entity[numEntities];
        EntityLatentRepresentation[] entityVectors = new EntityLatentRepresentation[numEntities];

        int idx = 0;
        for (Map.Entry<Integer, EntityLatentRepresentation> entry : entityRepresentationMap.entrySet()) {
            entities[idx] = new Entity(entry.getKey(), idTitleMap.get(entry.getKey()) != null ? idTitleMap.get(entry
                    .getKey()) : "");
            entityVectors[idx] = entry.getValue();
            idx++;
        }
        entityRepresentationMap.clear();

        recurseKMeans("root", entityVectors, entities, 30);
        out.close();
    }


    private static void recurseKMeans(String name, EntityLatentRepresentation[] entityVectors,
                                      Entity[] entities, int maxIterations) throws IOException {
        KMeans kmeans = new KMeans(name, entityVectors, entities, maxIterations);
        kmeans.run();
        ArrayList<Integer>[] clusters = kmeans.getClusters();
        EntityLatentRepresentation[][] representationtParitions = new EntityLatentRepresentation[kmeans.getNumClusters()
                ][];
        Entity[][] entityPartitions = new Entity[kmeans.getNumClusters()][];

        for (int i = 0; i < kmeans.getNumClusters(); i++) {
            ArrayList<Integer> clusterIndices = clusters[i];
            int clusterSize = clusterIndices.size();
            EntityLatentRepresentation[] partitionEntityVectors = new EntityLatentRepresentation[clusterSize];
            Entity[] partitionEntities = new Entity[clusterSize];

            for (int idx = 0; idx < clusterSize; idx++) {
                partitionEntityVectors[idx] = entityVectors[clusterIndices.get(idx)];
                partitionEntities[idx] = entities[clusterIndices.get(idx)];
            }
            representationtParitions[i] = partitionEntityVectors;
            entityPartitions[i] = partitionEntities;
        }

        for (int i = 0; i < kmeans.getNumClusters(); i++) {
            if (representationtParitions[i].length == 1 || entityPartitions[i].length == 1) {
                out.write(name + "/" + (i + 1) + ":" + entityPartitions[i][0].getPgTitle() + "\n");
                continue;
            }
            recurseKMeans(name + "/" + (i + 1), representationtParitions[i], entityPartitions[i],
                    maxIterations);
        }
    }
}