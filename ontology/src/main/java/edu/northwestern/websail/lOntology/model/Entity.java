package edu.northwestern.websail.lOntology.model;

/**
 * @author csbhagav on 8/22/14.
 */
public class Entity {

    Integer pgId;
    String pgTitle;

    public Entity(Integer id, String title) {
        this.pgId = id;
        this.pgTitle = title;
    }

    public Integer getPgId() {
        return pgId;
    }

    public void setPgId(Integer pgId) {
        this.pgId = pgId;
    }

    public String getPgTitle() {
        return pgTitle;
    }

    public void setPgTitle(String pgTitle) {
        this.pgTitle = pgTitle;
    }
}
