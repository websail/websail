package edu.northwestern.websail.lOntology.data;

import java.io.*;

/**
 * @author csbhagav on 8/20/14.
 */
public class SBTVectorRewrite {
    public static void main(String[] args) throws IOException {
        String sbtVectorsFile = "/Users/csbhagav/Projects/word2vec/entityVectors_small_2x2_3x7_2_2_3.dat";
        String sbtVectorsFileRewrite = "/Users/csbhagav/Projects/word2vec/entityVectors_rewrite.dat";

        BufferedWriter out = new BufferedWriter(new FileWriter(new File(sbtVectorsFileRewrite)));
        BufferedReader in = new BufferedReader(new FileReader(new File(sbtVectorsFile)));
        String line;
        while ((line = in.readLine()) != null) {
            String[] parts = line.split("\t");
            String entityName = parts[0];
            String[] indexValuePairs = parts[1].split(" ");
            String indexStr = "";
            String valueStr = "";
            for (String indexValuePair : indexValuePairs) {
                String[] pair = indexValuePair.split(":");
                String index = pair[0];
                String value = pair[1];
                indexStr += index + ",";
                valueStr += value + ",";
            }

            indexStr = indexStr.substring(0, indexStr.length() - 1);
            valueStr = valueStr.substring(0, valueStr.length() - 1);

            out.write(entityName + "\t" + valueStr + "\t" + indexStr + "\n");
        }
        in.close();
        out.close();
    }
}
