package edu.northwestern.websail.lOntology.main;


import edu.northwestern.websail.core.io.FileReadUtils;
import edu.northwestern.websail.lOntology.adapter.EntityVectorsAdapter;
import edu.northwestern.websail.lOntology.model.EntityLatentRepresentation;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.Random;

/**
 * @author csbhagav on 8/26/14.
 */
public class EvaluateKMeansOutput {
    public static void main(String[] args) throws IOException {

        Properties config = new Properties();
        config.load(new FileInputStream("./config/global.config"));

        ArrayList<String> countries = (ArrayList<String>) FileReadUtils.getFileLinesAsList("" +
                "./data/entitySets/countries.txt");
        ArrayList<String> usStates = (ArrayList<String>) FileReadUtils.getFileLinesAsList("" +
                "./data/entitySets/usStates.txt");
        ArrayList<String> indianStates = (ArrayList<String>) FileReadUtils.getFileLinesAsList("" +
                "./data/entitySets/indianStates.txt");

        HashMap<Integer, String> idTitleMap = FileReadUtils.getMapIntToString(config.getProperty("idTitleMap"));

        HashMap<String, String> titlePathInKmeansTree = new HashMap<String, String>();
        BufferedReader in = new BufferedReader(new FileReader(new File("./data/validation-multiCluster.txt")));
        String line;
        while ((line = in.readLine()) != null) {
            String[] parts = line.split(":");
            titlePathInKmeansTree.put(parts[1], parts[0]);
        }

        // Evaluate with vectors

        String vectorsFile = args.length > 0 ? args[0] : "/Users/csbhagav/Projects/LatentOntologies/data/ev-validation.dat";
        EntityVectorsAdapter adapter = new EntityVectorsAdapter();
        adapter.readVectorsFile(vectorsFile);

        System.out.print("Eval for Countries : ");
        evaluation(countries, usStates, indianStates, adapter);
        System.out.print("Eval for US States : ");
        evaluation(usStates, countries, indianStates, adapter);
        System.out.print("Eval for Indian States : ");
        evaluation(indianStates, usStates, countries, adapter);


        // Evaluate with k-means cluster tree distance
        System.exit(0);

        countries = getTitles(countries, idTitleMap);
        usStates = getTitles(usStates, idTitleMap);
        indianStates = getTitles(indianStates, idTitleMap);

        System.out.print("Eval for Countries : ");
        evaluation(countries, usStates, indianStates, titlePathInKmeansTree);
        System.out.print("Eval for US States : ");
        evaluation(usStates, countries, indianStates, titlePathInKmeansTree);
        System.out.print("Eval for Indian States : ");
        evaluation(indianStates, usStates, countries, titlePathInKmeansTree);


    }

    private static void evaluation(ArrayList<String> set1, ArrayList<String> set2, ArrayList<String> set3,
                                   EntityVectorsAdapter adapter) {
        ArrayList<String> incorrectSet = new ArrayList<String>(set2.size() + set3.size());
        incorrectSet.addAll(set2);
        incorrectSet.addAll(set3);
        evaluateSet(set1, incorrectSet, adapter);

    }

    private static void evaluateSet(ArrayList<String> correctSet, ArrayList<String> incorrectSet, EntityVectorsAdapter adapter) {
        Random r = new Random(110886L);
        int correct = 0;
        int total = 0;
        for (int i = 0; i < 10000; i++) {

            int idx1 = r.nextInt(correctSet.size());
            int idx2 = r.nextInt(correctSet.size());
            while (idx1 == idx2) {
                idx2 = r.nextInt(correctSet.size());
            }

            int idx3 = r.nextInt(incorrectSet.size());
            total++;
            EntityLatentRepresentation e1 = adapter.getEntityVectors().get(Integer.valueOf(correctSet.get(idx1)));
            EntityLatentRepresentation e2 = adapter.getEntityVectors().get(Integer.valueOf(correctSet.get(idx2)));
            EntityLatentRepresentation e3 = adapter.getEntityVectors().get(Integer.valueOf(incorrectSet.get(idx3)));
            double sim1 = e1.cosineSimilarity(e2);
            double sim2 = e1.cosineSimilarity(e3);
            if (sim1 > sim2) {
                correct++;
            }
        }
        double perc = (double) correct / (double) total;
        System.out.println(perc + " ( out of " + total +
                ")");


    }

    private static void evaluation(ArrayList<String> set1, ArrayList<String> set2, ArrayList<String> set3, HashMap<String,
            String> titlePathInKmeansTree) {
        ArrayList<String> incorrectSet = new ArrayList<String>(set2.size() + set3.size());
        incorrectSet.addAll(set2);
        incorrectSet.addAll(set3);
        evaluateSet(set1, incorrectSet, titlePathInKmeansTree);
    }

    private static void evaluateSet(ArrayList<String> correctSet, ArrayList<String> incorrectSet, HashMap<String,
            String> titlePathInKmeansTree) {

        Random r = new Random(110886L);
        int correct = 0;
        int total = 0;
        for (int i = 0; i < 10000; i++) {

            int idx1 = r.nextInt(correctSet.size());
            int idx2 = r.nextInt(correctSet.size());
            while (idx1 == idx2) {
                idx2 = r.nextInt(correctSet.size());
            }

            int idx3 = r.nextInt(incorrectSet.size());
            total++;
            if (isCloser(titlePathInKmeansTree.get(correctSet.get(idx1)), titlePathInKmeansTree.get(correctSet.get
                    (idx2)), titlePathInKmeansTree.get(incorrectSet.get(idx3)))) {
                correct++;
            }
        }
        double perc = (double) correct / (double) total;
        System.out.println(perc + " ( out of " + total +
                ")");
    }

    private static boolean isCloser(String fromPath, String toPath, String thanPath) {
        int lcaCorrect = lowestCommonAncestor(fromPath, toPath);
        int lcaIncorrect = lowestCommonAncestor(fromPath, thanPath);
        if (lcaCorrect == lcaIncorrect) {
            return treeDistance(fromPath, toPath) < treeDistance(fromPath, thanPath);
        } else {
            return lcaCorrect > lcaIncorrect;
        }
    }

    private static int lowestCommonAncestor(String pathStr1, String pathStr2) {
        String[] path1Nodes = pathStr1.split("/");
        String[] path2Nodes = pathStr2.split("/");
        int i = 0;
        while (i < path1Nodes.length && i < path2Nodes.length && path1Nodes[i].equals(path2Nodes[i])) {
            i++;
        }
        return i;

    }

    private static int treeDistance(String pathStr1, String pathStr2) {
        String[] path1Nodes = pathStr1.split("/");
        String[] path2Nodes = pathStr2.split("/");

        int i = 0, j = 0;
        while (i < path1Nodes.length && j < path2Nodes.length && path1Nodes[i].equals(path2Nodes[j])) {
            i++;
            j++;
        }
        return path1Nodes.length - i + path2Nodes.length - j;
    }

    private static ArrayList<String> getTitles(ArrayList<String> idList, HashMap<Integer, String> idTitleMap) {

        ArrayList<String> titleList = new ArrayList<String>();
        for (String id : idList) {
            titleList.add(idTitleMap.get(Integer.valueOf(id)));
        }
        System.out.println(titleList);
        return titleList;
    }
}
