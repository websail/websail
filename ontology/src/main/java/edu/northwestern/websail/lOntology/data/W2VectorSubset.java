package edu.northwestern.websail.lOntology.data;


import edu.northwestern.websail.core.io.FileReadUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

/**
 * @author csbhagav on 8/22/14.
 */
public class W2VectorSubset {
    public static void main(String[] args) throws IOException {
        String vectorsFileName = args[0];
        String subsetListFileName = args[1];
        String separator = args[2];

        HashMap<Integer, String> idtitlemap = FileReadUtils.getMapIntToString("/websail/common/wikification/data/en/en_id_title.map");

        BufferedReader in = new BufferedReader(new FileReader(new File(subsetListFileName)));
        String line;
        HashSet<String> ids = new HashSet<String>();
        while ((line = in.readLine()) != null) {
            ids.add(line);
        }

        in.close();

        in = new BufferedReader(new FileReader(new File(vectorsFileName)));
        while ((line = in.readLine()) != null) {
            String[] parts = line.split(separator);
            if (ids.contains(parts[0])) {
                System.out.println(idtitlemap.get(Integer.valueOf(parts[0])) + " " + line);
            }
        }
        in.close();
    }
}
