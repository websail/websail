package edu.northwestern.websail.lOntology.ml;

import edu.northwestern.websail.lOntology.model.Entity;
import edu.northwestern.websail.lOntology.model.EntityLatentRepresentation;
import org.la4j.vector.Vector;
import org.la4j.vector.dense.BasicVector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

/**
 * @author csbhagav on 8/25/14.
 */
public class KMeans {
    final String name;
    int numClusters;
    int numObjects;
    int numIter = 20;
    final EntityLatentRepresentation[] data;
    final Entity[] entities;
    ArrayList<Integer>[] clusters;
    static final Random r = new Random(110886L);
    boolean isConverged = false;

    @SuppressWarnings("unchecked")
    public KMeans(String name, EntityLatentRepresentation[] data, Entity[] entities, int numIter) {
        this.name = name;
        this.numObjects = data.length;

        int numClusters = (int) Math.sqrt(this.numObjects / 2) + 1;
        this.numClusters = (int) Math.sqrt(this.numObjects / 2) + 1;
        this.data = data;
        this.entities = entities;
        this.clusters = new ArrayList[numClusters];
        for (int i = 0; i < numClusters; i++) {
            this.clusters[i] = new ArrayList<Integer>();
        }
        this.numIter = numIter;
    }

    @SuppressWarnings("unchecked")
    public void run() {

        ArrayList<Integer>[] tempClusters = getEmptyClusters();

        HashSet<Integer> centerIndices = new HashSet<Integer>();
        for (int i = 0; i < numClusters; ) {
            int randIdx = r.nextInt(this.numObjects);
            if (!centerIndices.contains(randIdx)) {
                tempClusters[i].add(randIdx);
                centerIndices.add(randIdx);
                i++;
            }
        }
        centerIndices.clear();

        this.clusters = runIter(tempClusters);
    }

    private ArrayList<Integer>[] runIter(ArrayList<Integer>[] oldClusters) {

        ArrayList<Integer>[] newClusters;
        int switchAssignments;
        int iterNum = 0;
        do {
            iterNum++;
            newClusters = getEmptyClusters();
            switchAssignments = 0;
            HashMap<Integer, Integer> oldAssignments = new HashMap<Integer, Integer>(this.numObjects);
            EntityLatentRepresentation[] centers = new EntityLatentRepresentation[numClusters];
            for (int i = 0; i < this.numClusters; i++) {
                Vector meanVector = new BasicVector(data[0].getRepresentation().length());
                for (int j = 0; j < oldClusters[i].size(); j++) {
                    meanVector = meanVector.add(data[oldClusters[i].get(j)].getRepresentation());
                    oldAssignments.put(oldClusters[i].get(j), i);
                }
                meanVector = meanVector.divide(oldClusters[i].size());
                centers[i] = new EntityLatentRepresentation(meanVector);
            }


            for (int i = 0; i < numObjects; i++) {
                EntityLatentRepresentation v1 = data[i];
                int bestIdx = -1;
                double minDistance = Double.MAX_VALUE;
                for (int k = 0; k < numClusters; k++) {
                    double distance = v1.getEuclideanDistance(centers[k]);
                    if (distance < minDistance) {
                        minDistance = distance;
                        bestIdx = k;
                    }
                }
                if (!oldAssignments.containsKey(i) || oldAssignments.get(i) != bestIdx) {
                    switchAssignments++;
                }
                newClusters[bestIdx].add(i);
            }
            oldClusters = newClusters;
            System.out.println("Switched Assignments = " + switchAssignments);
        } while (switchAssignments > 0 && iterNum < numIter);

        if (switchAssignments > 0) {
            isConverged = false;
        }
        return newClusters;
    }

    @SuppressWarnings("unchecked")
    private ArrayList<Integer>[] getEmptyClusters() {
        ArrayList<Integer>[] tempClusters = new ArrayList[numClusters];
        for (int i = 0; i < numClusters; i++) {
            tempClusters[i] = new ArrayList<Integer>();
        }
        return tempClusters;
    }

    public int getNumObjects() {
        return numObjects;
    }

    public void setNumObjects(int numObjects) {
        this.numObjects = numObjects;
    }

    public ArrayList<Integer>[] getClusters() {
        return clusters;
    }

    public void setClusters(ArrayList<Integer>[] clusters) {
        this.clusters = clusters;
    }

    public int getNumClusters() {
        return numClusters;
    }

    public void setNumClusters(int numClusters) {
        this.numClusters = numClusters;
    }
}
