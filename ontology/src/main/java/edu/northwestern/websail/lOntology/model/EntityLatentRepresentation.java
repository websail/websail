package edu.northwestern.websail.lOntology.model;

import org.la4j.vector.Vector;
import org.la4j.vector.dense.BasicVector;

/**
 * @author csbhagav on 8/25/14.
 */
public class EntityLatentRepresentation implements Cloneable {
    Vector representation;
    Double vectorL2Norm;

    public EntityLatentRepresentation() {
    }

    public EntityLatentRepresentation(Vector representation) {
        this.representation = representation;
        this.vectorL2Norm = Math.sqrt(this.representation.innerProduct(this.representation));
    }

    public EntityLatentRepresentation(Vector representation, Double vectorL2Norm) {
        this.representation = representation;
        this.vectorL2Norm = vectorL2Norm;
    }

    public Vector getRepresentation() {
        return representation;
    }

    public void setRepresentation(Vector representation) {
        this.representation = representation;
    }

    public Double getVectorL2Norm() {
        return vectorL2Norm;
    }

    public void setVectorL2Norm(Double vectorL2Norm) {
        this.vectorL2Norm = vectorL2Norm;
    }

    public double cosineSimilarity(EntityLatentRepresentation v2) {
        return this.getRepresentation().innerProduct(v2.getRepresentation()) / (this.getVectorL2Norm() * v2
                .getVectorL2Norm());
    }

    public double getEuclideanDistance(EntityLatentRepresentation v2) {
        Vector temp = this.getRepresentation().subtract(v2.getRepresentation());
        return Math.sqrt(temp.innerProduct(temp));
    }

    public Object clone() throws CloneNotSupportedException {
        super.clone();
        return new EntityLatentRepresentation(new BasicVector(representation), this.vectorL2Norm);
    }
}
