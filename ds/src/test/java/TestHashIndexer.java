import edu.northwestern.websail.ds.indexer.HashIndexer;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.*;


public class TestHashIndexer {

    @Test
    public void testBasicFunctions() {
        HashIndexer<String> indexer = new HashIndexer<String>(String.class);
        assertTrue("Adding a new object should return true.",
                indexer.add("Text"));
        assertFalse("Adding an existing object should return false.",
                indexer.add("Text"));
        assertEquals("Size is correct.", (Integer) 1, (Integer) indexer.size());
        assertEquals("Index is correct.", (Integer) 0,
                (Integer) indexer.indexOf("Text"));
        assertEquals("Object is correct.", "Text", indexer.get(0));
        assertEquals(
                "Get, add if not exist is working for an existing object.",
                (Integer) 0, (Integer) indexer.gadIndexOf("Text"));
        assertEquals("Get, add if not exist is working for a new object.",
                (Integer) 1, (Integer) indexer.gadIndexOf("Text2"));

    }

    @Test
    public void testSerialization() throws IOException, ClassNotFoundException {
        HashIndexer<String> indexer = new HashIndexer<String>(String.class);
        indexer.add("Text");
        indexer.add("Text2");
        indexer.add("Text3");
        String fileName = "tmp.indexer";
        File file;
        ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(fileName)));
        oos.writeObject(indexer);
        oos.flush();
        oos.close();
        file = new File(fileName);
        assertTrue("Serialized file exists.", file.exists());
        ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(
                new FileInputStream(fileName)));
        @SuppressWarnings("unchecked")
        HashIndexer<String> indexer2 = (HashIndexer<String>) ois.readObject();
        for (String str : indexer.getObjects()) {
            assertEquals("Index equals", (Integer) indexer.indexOf(str), (Integer) indexer2.indexOf(str));
        }
        ois.close();
        file.deleteOnExit();

        String fileName2 = "tmp.txt";
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
                fileName2), "UTF-8"));
        indexer2.writeReadable(out);
        out.flush();
        out.close();
        file = new File(fileName2);
        assertTrue("Readable file exists.", file.exists());
        file.deleteOnExit();
    }
}
