package edu.northwestern.websail.ds.map;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author NorThanapon
 * @since 11/14/14
 */
public class HashBiMap<K, V> implements BiMap<K, V> {
    private final HashMap<K, V> keyToVal;
    private final HashMap<V, K> valToKey;


    public HashBiMap() {
        keyToVal = new HashMap<K, V>();
        valToKey = new HashMap<V, K>();
    }

    public HashBiMap(int initSize) {
        keyToVal = new HashMap<K, V>(initSize);
        valToKey = new HashMap<V, K>(initSize);
    }

    @Override
    public boolean put(K key, V value) {
        if (!keyToVal.containsKey(key)
                && !valToKey.containsKey(value)) {
            keyToVal.put(key, value);
            valToKey.put(value, key);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void forcePut(K key, V value) {
        if (keyToVal.containsKey(key)) {
            keyToVal.remove(key);
        }
        if (valToKey.containsKey(value)) {
            valToKey.remove(value);
        }
        keyToVal.put(key, value);
        valToKey.put(value, key);
    }

    @Override
    public boolean containKey(K key) {
        return keyToVal.containsKey(key);
    }

    @Override
    public boolean containValue(V value) {
        return valToKey.containsKey(value);
    }

    @Override
    public V getValue(K key) {
        return keyToVal.get(key);
    }

    @Override
    public K getKey(V value) {
        return valToKey.get(value);
    }

    @Override
    public Set<K> keySet() {
        return keyToVal.keySet();
    }

    @Override
    public Set<V> valueSet() {
        return valToKey.keySet();
    }

    @Override
    public int size() {
        return keyToVal.size();
    }

    @Override
    public V removeByKey(K key) {
        V value = keyToVal.remove(key);
        if (value == null) return null;
        valToKey.remove(value);
        return value;
    }

    @Override
    public K removeByValue(V value) {
        K key = valToKey.remove(value);
        if (key == null) return null;
        keyToVal.remove(key);
        return key;
    }

    @Override
    public Set<Map.Entry<K, V>> entrySetKV() {
        return keyToVal.entrySet();
    }

    @Override
    public Set<Map.Entry<V, K>> entrySetVK() {
        return valToKey.entrySet();
    }

}
