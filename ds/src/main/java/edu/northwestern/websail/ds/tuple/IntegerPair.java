package edu.northwestern.websail.ds.tuple;

import org.jetbrains.annotations.NotNull;

/**
 * @author NorThanapon
 */

public class IntegerPair implements Comparable<IntegerPair> {
    int first;
    int second;

    public IntegerPair() {
    }

    public IntegerPair(int first, int second) {
        this.first = first;
        this.second = second;
    }

    public int getFirst() {
        return first;
    }

    public int getSecond() {
        return second;
    }

    public int compareTo(@NotNull IntegerPair pair) {
        if (this.first < pair.first && this.second <= pair.first)
            return -1;
        else if (this.first >= pair.second && this.second > pair.second)
            return 1;
        return 0;
    }

    public String toString() {
        return this.first + "," + this.second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntegerPair that = (IntegerPair) o;
        return first == that.first && second == that.second;
    }

    @Override
    public int hashCode() {
        int result = first;
        result = 31 * result + second;
        return result;
    }
}
