package edu.northwestern.websail.ds.tuple;

import java.util.Comparator;

/**
 * @author NorThanapon
 */
public class IntegerDoublePair {
    int intElement;
    double doubleElement;

    public IntegerDoublePair(int intElement, double doubleElement) {
        this.intElement = intElement;
        this.doubleElement = doubleElement;
    }

    public static Comparator<IntegerDoublePair> comparator(boolean descending) {
        final int modifier = descending ? 1 : -1;
        return new Comparator<IntegerDoublePair>() {
            @Override
            public int compare(IntegerDoublePair o1, IntegerDoublePair o2) {
                if (o1.getDoubleElement() < o2.getDoubleElement()) {
                    return modifier;
                } else if (o2.getDoubleElement() < o1.getDoubleElement()) {
                    return -1 * modifier;
                } else return 0;
            }
        };
    }

    public int getIntElement() {
        return intElement;
    }

    public void setIntElement(Integer intElement) {
        this.intElement = intElement;
    }

    public double getDoubleElement() {
        return doubleElement;
    }

    public void setDoubleElement(Double doubleElement) {
        this.doubleElement = doubleElement;
    }

    public String toString() {
        return intElement + "\t" + doubleElement;
    }
}
