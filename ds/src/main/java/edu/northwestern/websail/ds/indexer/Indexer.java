package edu.northwestern.websail.ds.indexer;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Serializable;

/**
 * @author NorThanapon
 */
public abstract class Indexer<E extends Comparable<E>> implements Serializable {

    private static final long serialVersionUID = -6817852226827112575L;

    // object
    public abstract E get(final int index);

    public E get(final E obj) {
        // to save space
        int index = this.indexOf(obj);
        if (index == -1) return null;
        else return this.get(index);
    }

    public abstract boolean add(final E obj);

    // index
    public abstract int indexOf(final E obj);

    public abstract int gadIndexOf(final E obj); //get if not exist add

    // etc
    public abstract int size();

    // iteration
    public abstract Iterable<E> getObjects();

    public abstract Iterable<Integer> getKeys();

    public abstract void writeReadable(BufferedWriter out) throws IOException;
}
