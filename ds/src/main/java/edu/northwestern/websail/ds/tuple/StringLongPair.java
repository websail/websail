package edu.northwestern.websail.ds.tuple;

import java.util.Comparator;

/**
 * @author csbhagav on 10/6/14.
 */
public class StringLongPair {
    String stringElement;
    long longElement;

    public StringLongPair(String s, long d) {
        this.stringElement = s;
        this.longElement = d;
    }

    public static Comparator<StringLongPair> comparator(boolean descending) {
        final int modifier = descending ? 1 : -1;
        return new Comparator<StringLongPair>() {
            @Override
            public int compare(StringLongPair o1, StringLongPair o2) {
                if (o1.getLongElement() < o2.getLongElement()) {
                    return modifier;
                } else if (o2.getLongElement() < o1.getLongElement()) {
                    return -1 * modifier;
                } else return 0;
            }
        };
    }

    public String getStringElement() {
        return stringElement;
    }

    public void setStringElement(String stringElement) {
        this.stringElement = stringElement;
    }

    public long getLongElement() {
        return longElement;
    }

    public void setLongElement(long longElement) {
        this.longElement = longElement;
    }
}
