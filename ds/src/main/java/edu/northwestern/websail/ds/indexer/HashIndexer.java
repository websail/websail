package edu.northwestern.websail.ds.indexer;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author NorThanapon
 */

public class HashIndexer<E extends Comparable<E>> extends Indexer<E> {

    private static final long serialVersionUID = 8976217473669318099L;

    ArrayList<E> objects;
    HashMap<E, Integer> map;
    Kryo kryo;
    Class<E> classOfE;
    Serializer<E> kryoObjectSerializer;

    public HashIndexer() {
        objects = new ArrayList<E>();
        map = new HashMap<E, Integer>();
    }

    public HashIndexer(Class<E> classOfE) {
        this(classOfE, null);
    }

    public HashIndexer(Class<E> classOfE, Serializer<E> kryoObjectSerializer) {
        super();
        this.classOfE = classOfE;
        objects = new ArrayList<E>();
        map = new HashMap<E, Integer>();
        kryo = createKryo();
        if (kryoObjectSerializer == null) {
            kryo.register(classOfE);
        } else {
            kryo.register(classOfE, kryoObjectSerializer);
            this.kryoObjectSerializer = kryoObjectSerializer;
        }
    }

    public HashIndexer(Class<E> classOfE, Serializer<E> kryoObjectSerializer, int initSize) {
        super();
        this.classOfE = classOfE;
        objects = new ArrayList<E>();
        map = new HashMap<E, Integer>(initSize);
        kryo = createKryo();
        if (kryoObjectSerializer == null) {
            kryo.register(classOfE);
        } else {
            kryo.register(classOfE, kryoObjectSerializer);
            this.kryoObjectSerializer = kryoObjectSerializer;
        }
    }

    public static Kryo createKryo() {
        Kryo kryo = new Kryo();
        kryo.register(HashMap.class);
        kryo.register(ArrayList.class);
        return kryo;
    }

    @Override
    public E get(int index) {
        if (index >= this.size()) {
            return null;
        }
        return objects.get(index);
    }

    @Override
    public boolean add(E obj) {
        if (map.containsKey(obj)) {
            return false;
        }
        map.put(obj, this.size());
        objects.add(obj);
        return true;
    }

    @Override
    public int indexOf(E obj) {
        Integer index = map.get(obj);
        if (index == null)
            return -1;
        else
            return index;
    }

    @Override
    public int gadIndexOf(E obj) {
        int index = this.indexOf(obj);
        if (index == -1) {
            index = this.size();
            this.add(obj);
        }
        return index;
    }

    @Override
    public int size() {
        return objects.size();
    }

    @Override
    public Iterable<E> getObjects() {
        return objects;
    }

    @Override
    public Iterable<Integer> getKeys() {
        return map.values();
    }

    private void writeObject(ObjectOutputStream oss) throws IOException {
        Output output = new Output(oss);
        this.kryo.writeObject(output, this.classOfE.getName());
        output.flush();
        if (this.kryoObjectSerializer == null) {
            this.kryo.writeObject(output, "");
        } else {
            this.kryo.writeObject(output, this.kryoObjectSerializer.getClass()
                    .getName());
        }
        output.flush();
        this.kryo.writeObject(output, this.map);
        output.flush();
        this.kryo.writeObject(output, this.objects);
        output.flush();
    }

    @SuppressWarnings("unchecked")
    private void readObject(ObjectInputStream ois)
            throws ClassNotFoundException, IOException {
        this.kryo = createKryo();
        Input input = new Input(ois);
        String classOfEName = this.kryo.readObject(input, String.class);
        this.classOfE = (Class<E>) Class.forName(classOfEName);
        String serializerName = this.kryo.readObject(input, String.class);
        if (serializerName.equals("")) {
            this.kryo.register(classOfE);
        } else {
            try {
                this.kryoObjectSerializer = (Serializer<E>) Class.forName(
                        serializerName).newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            this.kryo.register(classOfE, this.kryoObjectSerializer);
        }
        this.map = this.kryo.readObject(input, HashMap.class);
        this.objects = this.kryo.readObject(input, ArrayList.class);
    }

    @Override
    public void writeReadable(BufferedWriter out) throws IOException {
        int i = 0;
        for (E obj : this.getObjects()) {
            out.write(i++ + "\t" + obj.toString() + "\n");
        }
    }
}
