package edu.northwestern.websail.ds.tuple;

import java.util.Comparator;

/**
 * @author csbhagav on 10/6/14.
 */
public class StringDoublePair {
    String stringElement;
    double doubleElement;

    public StringDoublePair(String s, double d) {
        this.stringElement = s;
        this.doubleElement = d;
    }

    public static Comparator<StringDoublePair> comparator(boolean descending) {
        final int modifier = descending ? 1 : -1;
        return new Comparator<StringDoublePair>() {
            @Override
            public int compare(StringDoublePair o1, StringDoublePair o2) {
                if (o1.getDoubleElement() < o2.getDoubleElement()) {
                    return modifier;
                } else if (o2.getDoubleElement() < o1.getDoubleElement()) {
                    return -1 * modifier;
                } else return 0;
            }
        };
    }

    public String getStringElement() {
        return stringElement;
    }

    public void setStringElement(String stringElement) {
        this.stringElement = stringElement;
    }

    public double getDoubleElement() {
        return doubleElement;
    }

    public void setDoubleElement(Double doubleElement) {
        this.doubleElement = doubleElement;
    }
}
