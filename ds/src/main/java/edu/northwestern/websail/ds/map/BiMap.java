package edu.northwestern.websail.ds.map;

import java.util.Map;
import java.util.Set;

/**
 * @author NorThanapon
 * @since 11/14/14
 */
public interface BiMap<K, V> {
    boolean put(K key, V value);

    void forcePut(K key, V value);

    boolean containKey(K key);

    boolean containValue(V value);

    V getValue(K key);

    K getKey(V value);

    Set<K> keySet();

    Set<V> valueSet();

    int size();

    V removeByKey(K key);

    K removeByValue(V value);

    Set<Map.Entry<K, V>> entrySetKV();

    Set<Map.Entry<V, K>> entrySetVK();
}
