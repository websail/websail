package edu.northwestern.websail.ds.tuple;

import org.jetbrains.annotations.NotNull;

/**
 * @author NorThanapon
 */

public class IntegerTriplet implements Comparable<IntegerTriplet> {
    int first;
    int second;
    int third;

    public IntegerTriplet() {
    }

    public IntegerTriplet(int first, int second, int third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public int getFirst() {
        return first;
    }

    public int getSecond() {
        return second;
    }

    public int getThird() {
        return third;
    }

    public int compareTo(@NotNull IntegerTriplet pair) {
        if (this.first < pair.first && this.second <= pair.first)
            return -1;
        else if (this.first >= pair.second && this.second > pair.second)
            return 1;
        else if (this.third < pair.third)
            return -1;
        else if (this.third > pair.third)
            return 1;
        return 0;
    }

    public String toString() {
        return this.first + "," + this.second + "," + this.third;
    }

}
