package edu.northwestern.websail.kpe.pe.helper;

import edu.northwestern.websail.text.data.TaggedToken;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.tokenizer.StanfordNLPSSplit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

/**
 * @author NorThanapon
 * @since 8/18/15
 */
public class ValidPOSTags {
    public static final HashSet<String> validStartPOS = new HashSet<String>();

    static {
        validStartPOS.add("SYM");
        validStartPOS.add("NN");
        validStartPOS.add("NNS");
        validStartPOS.add("NNP");
        validStartPOS.add("NNPS");
        validStartPOS.add("JJ");
        validStartPOS.add("FW");
        validStartPOS.add("VBG");
        validStartPOS.add("VB");
        validStartPOS.add("VBD");
        validStartPOS.add("VBN");
    }

    public static final HashSet<String> validEndPOS = new HashSet<String>();

    static {
        validEndPOS.add("SYM");
        validEndPOS.add("NN");
        validEndPOS.add("NNS");
        validEndPOS.add("NNP");
        validEndPOS.add("NNPS");
        validEndPOS.add("CD");
        validEndPOS.add("FW");
//        validEndPOS.add("VBG");
//        validEndPOS.add("JJ");
    }

    public static final HashSet<String> ALLOW_POS_SET = new HashSet<String>();

    static {
        ALLOW_POS_SET.add("CD");
        ALLOW_POS_SET.add("FW");
        ALLOW_POS_SET.add("SYM");
        ALLOW_POS_SET.add("NN");
        ALLOW_POS_SET.add("NNS");
        ALLOW_POS_SET.add("NNP");
        ALLOW_POS_SET.add("NNPS");
        ALLOW_POS_SET.add("RB");
        ALLOW_POS_SET.add("IN");
        ALLOW_POS_SET.add("JJ");
        ALLOW_POS_SET.add("JJR");
        ALLOW_POS_SET.add("JJS");
        ALLOW_POS_SET.add("VBG");
        ALLOW_POS_SET.add("VBD");
        ALLOW_POS_SET.add("VBN");
    }


    public static void main(String[] args) {
        StanfordNLPSSplit split = new StanfordNLPSSplit(true);
        String text = "Apocrita: A Distributed Peer-to-Peer File Sharing System for Intranets";
        for (TokenSpan span : split.sentenceSplit(text)) {
            System.out.println(span.getTokens());
            for (int i = 0; i < span.getTokens().size(); i++) {
                TaggedToken tt = (TaggedToken) span.getTokens().get(i);
                ArrayList<TaggedToken> phrase = new ArrayList<TaggedToken>();
                if (validEndPOS.contains(tt.getPOS())) {
                    phrase.add(tt);
                    printPhrase(phrase);
                    for (int j = i - 1; j >= 0; j--) {
                        if (i - j >= 5) break;
                        TaggedToken backTT = (TaggedToken) span.getTokens().get(j);
                        if (!ALLOW_POS_SET.contains(backTT.getPOS())) break;
                        if (backTT.getPOS().equals("IN") && !backTT.getText().equalsIgnoreCase("of")) break;
                        phrase.add(backTT);
                        if (validStartPOS.contains(backTT.getPOS())) {
                            printPhrase(phrase);
                        }
                    }
                }
            }
        }
    }

    private static void printPhrase(ArrayList<TaggedToken> phrase) {
        ArrayList<TaggedToken> p = new ArrayList<TaggedToken>(phrase);
        Collections.reverse(p);
        System.out.println(p);
    }
}
