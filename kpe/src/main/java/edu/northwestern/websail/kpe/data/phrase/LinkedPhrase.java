package edu.northwestern.websail.kpe.data.phrase;

import edu.northwestern.websail.text.data.TokenSpan;

/**
 * @author NorThanapon
 */
public class LinkedPhrase extends ProbScorePhrase {
    LinkedPhrase left;
    LinkedPhrase right;
    Status status = Status.INIT;

    public LinkedPhrase() {
        super();
    }

    public LinkedPhrase(String surfaceForm,
                        String source, TokenSpan span) {
        super(surfaceForm, source, span);
    }

    public LinkedPhrase left() {
        return left;
    }

    public LinkedPhrase left(LinkedPhrase left) {
        this.left = left;
        return this;
    }

    public LinkedPhrase right() {
        return right;
    }

    public LinkedPhrase right(LinkedPhrase right) {
        this.right = right;
        return this;
    }

    public Status status() {
        return this.status;
    }

    public LinkedPhrase status(Status status) {
        this.status = status;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("<");
        if (left != null) {
            sb.append(left.text());
        } else {
            sb.append("null");
        }
        sb.append("\n>");
        if (right != null) {
            sb.append(right.text());
        } else {
            sb.append("null");
        }
        sb.append("\n");
        return sb.toString();
    }

    public enum Status {
        INIT,
        EXPANDED,
        MERGED
    }

}
