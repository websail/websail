package edu.northwestern.websail.kpe.pe.filter;


import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.kpe.kpfe.FrequencyKPFE;
import edu.northwestern.websail.text.corpus.data.lucene.IndexAggregator;
import edu.northwestern.websail.text.corpus.data.lucene.collector.DocCountCollector;
import edu.northwestern.websail.text.util.TokenUtil;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 * @since 11/19/14
 */
@PipelineConfigInfo(
        requiredSettings = {"kpe.index.directory=*"}
)
@PipelineBUTInfo(
        consumes = {KPEBUT.Phrases.class},
        produces = {KPEBUT.Phrases.class}
)
public class DFFilter implements PipelineExecutable {
    public static final Logger logger = Logger
            .getLogger(DFFilter.class.getName());
    public static final int MIN_DF = 2;
    int threadId = 0;
    private IndexSearcher searcher;
    private QueryParser qp;

    public DFFilter() {
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        String indexDirectory = config.getProperty("kpe.index.directory");
        if (indexDirectory == null) {
            logger.severe("'kpe.index.directory' is not set.");
            return false;
        }
        DirectoryReader reader;
        try {
            File indexFile = new File(indexDirectory);
            Directory directory = FSDirectory.open(indexFile);
            reader = DirectoryReader.open(directory);
        } catch (IOException e) {
            logger.severe("Cannot open index directory: " + indexDirectory);
            e.printStackTrace();
            return false;
        }
        searcher = new IndexSearcher(reader);
        WhitespaceAnalyzer analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);
        qp = new QueryParser(Version.LUCENE_40, IndexAggregator.DOC_CONTENT_FIELD, analyzer);
        qp.setDefaultOperator(QueryParser.Operator.AND);
        qp.setPhraseSlop(0);
        return true;
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        for (BuckUnit unit : prev.units()) {
            if (unit.isNotOkay())
                continue;
            try {
                FastList<Phrase> phrases = unit.get(KPEBUT.Phrases.class);
                FastList<Phrase> filteredPhrases = new FastList<Phrase>();
                for (Phrase phrase : phrases) {
                    String q = QueryParser.escape(
                            TokenUtil.tokens2Str(phrase.getSpan().getTokens()));
                    int df = search(q);
                    if (df >= MIN_DF)
                        filteredPhrases.add(phrase);
                }
                unit.set(KPEBUT.Phrases.class, filteredPhrases);
            } catch (Exception e) {
                logger.severe("Unexpected error while filtering phrases.");
                e.printStackTrace();
            }
        }

        return true;
    }

    private int search(String phrase)
            throws ParseException, IOException {
        Integer searchResult = FrequencyKPFE.dfCache.get(phrase);
        if (searchResult == null) {
            DocCountCollector collector = new DocCountCollector();
            Query q = qp.parse("\"" + phrase + "\"");
            searcher.search(q, collector);
            searchResult = collector.getTotalDoc();

            synchronized (FrequencyKPFE.dfCache) {
                FrequencyKPFE.dfCache.put(phrase, searchResult);
            }
        }
        return searchResult;
    }


    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public int getThreadId() {
        return threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }

}
