package edu.northwestern.websail.kpe.data.kp;

import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.kpe.kpfe.FrequencyKPFE;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;

/**
 * @author NorThanapon
 * @since 11/20/14
 */
public class KPUtil {

    public static String instance2NormalizedSurface(Instance inst) {
        Phrase phr = (Phrase) inst.getSource();
        return TokenUtil.tokens2Str(phr.getSpan().getTokens());
    }

    public static ScoreKeyPhrase instance2ScoreKeyPhrase(Instance instance) {
        Phrase phr = (Phrase) instance.getSource();
        ScoreKeyPhrase scoreKeyPhrase = new ScoreKeyPhrase(phr.getSource(), phr.getSurfaceForm(), phr.getSpan().getTokens());
        scoreKeyPhrase.setType("unk");
        try {
            scoreKeyPhrase.setIdf(instance.getValue(FrequencyKPFE.idfF));
        } catch (IndexOutOfBoundsException e) {
            scoreKeyPhrase.setIdf(Instance.MISSING);
        }
        try {
            scoreKeyPhrase.setLogTF(instance.getValue(FrequencyKPFE.logtfF));
        } catch (IndexOutOfBoundsException e) {
            scoreKeyPhrase.setLogTF(Instance.MISSING);
        }
        scoreKeyPhrase.setSearchScore(Instance.MISSING);
        return scoreKeyPhrase;
    }

    public static ScoreKeyPhrase averageScores(ArrayList<ScoreKeyPhrase> skps) {
        if (skps.size() == 0) return null;
        ScoreKeyPhrase skp = new ScoreKeyPhrase(skps.get(0).getSource(), skps.get(0).getPhrase(), skps.get(0).getTokens());
        double idf = 0;
        double logtf = 0;
        int idfCount = 0;
        int logtfCount = 0;
        for (ScoreKeyPhrase s : skps) {
            try {
                idf += s.getIdf();
                idfCount++;
            } catch (IndexOutOfBoundsException ignored) {
            }
            try {
                logtf += s.getLogTF();
                logtfCount++;
            } catch (IndexOutOfBoundsException ignored) {
            }
        }
        skp.setIdf(idf / idfCount);
        skp.setLogTF(logtf / logtfCount);
        return skp;
    }

    public static <T extends KeyPhrase> ArrayList<T> removeDuplicateSurfaceKP(ArrayList<T> keyPhrases) {
        ArrayList<T> out = new ArrayList<T>();
        HashSet<String> normalizedSurfaceSet = new HashSet<String>();
        for (T kp : keyPhrases) {
            if (!normalizedSurfaceSet.contains(kp.getNormalizedPhrase())) {
                out.add(kp);
                normalizedSurfaceSet.add(kp.getNormalizedPhrase());
            }
        }
        return out;
    }

    public static class StringLengthComparator implements Comparator<KeyPhrase> {
        @Override
        public int compare(KeyPhrase o1, KeyPhrase o2) {
            if (o1.getNormalizedPhrase().length() < o2.getNormalizedPhrase()
                    .length()) {
                return 1;
            } else if (o1.getNormalizedPhrase().length() > o2
                    .getNormalizedPhrase().length()) {
                return -1;
            }
            return o1.getNormalizedPhrase().compareTo(o2.getNormalizedPhrase());
        }
    }

    public static class GroupLengthComparator implements
            Comparator<KPGroup> {
        @Override
        public int compare(KPGroup o1, KPGroup o2) {
            if (o1.getKps().size() < o2.getKps().size()) {
                return 1;
            } else if (o1.getKps().size() > o2.getKps().size()) {
                return -1;
            }
            return 0;
        }
    }

    public static class GroupRepLengthComparator implements
            Comparator<KPGroup> {
        @Override
        public int compare(KPGroup o1, KPGroup o2) {
            if (o1.getLongestKP().getNormalizedPhrase().length() < o2
                    .getLongestKP().getNormalizedPhrase().length()) {
                return 1;
            } else if (o1.getLongestKP().getNormalizedPhrase().length() > o2
                    .getLongestKP().getNormalizedPhrase().length()) {
                return -1;
            }
            return 0;
        }
    }

    public static class GroupSearchScoreComparator implements
            Comparator<KPGroup> {
        @Override
        public int compare(KPGroup o1, KPGroup o2) {
            double scoreO1 = o1.getRepresentative().getSearchScore();
            double scoreO2 = o2.getRepresentative().getSearchScore();
            if (scoreO1 < scoreO2) {
                return 1;
            } else if (scoreO1 > scoreO2) {
                return -1;
            }
            return 0;
        }
    }

    public static class GroupTFIDFComparator implements
            Comparator<KPGroup> {
        @Override
        public int compare(KPGroup o1, KPGroup o2) {
            double scoreO1 = o1.getRepresentative().getTFIDF();
            double scoreO2 = o2.getRepresentative().getTFIDF();
            if (scoreO1 < scoreO2) {
                return 1;
            } else if (scoreO1 > scoreO2) {
                return -1;
            }
            return 0;
        }
    }
}
