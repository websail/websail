package edu.northwestern.websail.kpe.pe;

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnitTypes;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.kpe.pe.helper.TechnicalTerms;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.tokenizer.SentenceSpliter;
import edu.northwestern.websail.text.tokenizer.StanfordNLPSSplit;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Chandra
 */

@PipelineConfigInfo(
        requiredResources = {PEVResourceType.S_SENTENCE_SPLITER_POOL},
        requiredSettings = {"text.tokenizer.SentenceSpliter.pos=true"}
)
@PipelineBUTInfo(
        consumes = {BuckUnitTypes.TextBeginOffsetBUT.class, BuckUnitTypes.TextBUT.class},
        produces = {KPEBUT.Sentences.class, KPEBUT.Phrases.class}
)
public class TechnicalTermPE extends PhraseExtractor {

    protected SentenceSpliter ssplit;

    public TechnicalTermPE() {
        super();
    }

    public static int numChars(String str, char ch, int start, int end) {
        int count = 0;
        for (int i = start; i < end; i++) {
            if (ch == str.charAt(i)) count++;
        }
        return count;
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config))
            return false;
        this.initialized = false;
        this.ssplit = (SentenceSpliter) env.getPoolResource(
                PEVResourceType.S_SENTENCE_SPLITER_POOL,
                this.threadId, PEVResourceType.S_SENTENCE_SPLITER);
        if (ssplit == null) {
            ssplit = new StanfordNLPSSplit(true);
        }
        this.initialized = true;
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        normalizedToken = true;
        return super.close(env, config);
    }

    @Override
    public FastList<Phrase> extract(String text) throws Exception {

        try {
            if (this.curSentences == null) this.curSentences = ssplit.sentenceSplit(text);
        } catch (Exception e) {
            System.out.println(this.curSource);
            System.out.println(text);
        }

        FastList<Phrase> phrases = new FastList<Phrase>();
        for (TokenSpan s : this.curSentences) {
            List<? extends Token> tokens = s.getTokens();
            if (!normalizedToken) {
                s.setTokens(TokenUtil.cleanTokens(TokenUtil.normalize(tokens, true, true), false, null));
            }
            phrases.addAll(this.chunkSentence(tokens, text));
        }
        return phrases;
    }

    private FastList<Phrase> chunkSentence(List<? extends Token> tokens, String text) throws Exception {
        FastList<Phrase> phrases = new FastList<Phrase>();
        phrases.addAll(getPhrasesMatchingRegex(TechnicalTerms.simple, tokens, text, false));
        phrases.addAll(getPhrasesMatchingRegex(TechnicalTerms.complex, tokens, text, true));
        return phrases;
    }

    private FastList<Phrase> getPhrasesMatchingRegex(Pattern p, List<? extends Token> tokens, String text,
                                                      boolean isComplexPattern) throws Exception {
        String tokenPOSString = TokenUtil.token2POSStr(tokens);
        FastList<Phrase> phrases = new FastList<Phrase>();

        Matcher simpleMatcher = p.matcher(tokenPOSString);
        int i = 0;
        while (simpleMatcher.find()) {
            int spacesBeforeStart = numChars(tokenPOSString, ' ', 0, simpleMatcher.start());
            int spacesBeforeEnd = numChars(tokenPOSString, ' ', 0, simpleMatcher.end());

            int startOffset = tokens.get(spacesBeforeStart).getStartOffset();
            int endOffset = tokens.get(spacesBeforeEnd).getEndOffset();
            String surfaceForm = text.substring(startOffset, endOffset);
            // finding "of" as IN in the matching text
            if (isComplexPattern && !surfaceForm.toLowerCase().contains(" of "))
                continue;
            TokenSpan span = new TokenSpan(startOffset, endOffset, tokens.subList(
                    spacesBeforeStart, spacesBeforeEnd + 1), i++);
            Phrase phrase = new Phrase(surfaceForm, super.curSource, span);
            if (spacesBeforeStart == 0) {
                phrase.setBefore(Token.START);
            } else {
                phrase.setBefore(tokens.get(spacesBeforeStart - 1));
            }
            if (spacesBeforeEnd == tokens.size() - 1) {
                phrase.setAfter(Token.END);
            } else {
                phrase.setAfter(tokens.get(spacesBeforeEnd + 1));
            }
            phrases.add(phrase);
        }
        return phrases;
    }


}
