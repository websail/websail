package edu.northwestern.websail.kpe.pe;

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnitTypes;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.phrase.LinkedPhrase;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.kpe.pe.helper.BottomUpMergingChunker;
import edu.northwestern.websail.kpe.pe.helper.ValidPOSTags;
import edu.northwestern.websail.text.data.TaggedToken;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.ngram.NgramMapCountWrapper;
import edu.northwestern.websail.text.tokenizer.SentenceSpliter;
import edu.northwestern.websail.text.tokenizer.StanfordNLPSSplit;

import java.util.List;
import java.util.Properties;

/**
 * @author NorThanapon
 */
@PipelineConfigInfo(
        requiredResources = {PEVResourceType.S_SENTENCE_SPLITER_POOL, PEVResourceType.NGRAM_COUNT},
        requiredSettings = {"text.tokenizer.SentenceSpliter.pos=true", "ngram.countWordIndexer=*"}
)
@PipelineBUTInfo(
        consumes = {BuckUnitTypes.TextBeginOffsetBUT.class, BuckUnitTypes.TextBUT.class},
        produces = {KPEBUT.Sentences.class, KPEBUT.Phrases.class}
)
public class BottomUpMergingPE extends PhraseExtractor {



    protected BottomUpMergingChunker chunker;
    SentenceSpliter ssplit;

    public BottomUpMergingPE() {
        super();
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config))
            return false;
        this.initialized = false;
        NgramMapCountWrapper countMap = (NgramMapCountWrapper) env
                .getResource(PEVResourceType.CORPUS_NGRAM_COUNT);
        if (countMap == null) {
            logger.severe(PEVResourceType.CORPUS_NGRAM_COUNT
                    + " is not in the environment.");
            return false;
        }
        ssplit = (StanfordNLPSSplit) env.getPoolResource(
                PEVResourceType.S_SENTENCE_SPLITER_POOL,
                this.threadId, PEVResourceType.S_SENTENCE_SPLITER);
        if (ssplit == null) {
            ssplit = new StanfordNLPSSplit(true);
        }
        this.chunker = new BottomUpMergingChunker(countMap, ssplit);
        this.initialized = true;
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        normalizedToken = true;
        return super.close(env, config);
    }

    @Override
    public FastList<Phrase> extract(String text) throws Exception {
        List<TokenSpan> sentences = this.curSentences;
        if (sentences == null) sentences = ssplit.sentenceSplit(text);
        this.curSentences = sentences;

        FastList<LinkedPhrase> phrases = this.chunker.chunk(text, curSentences, !normalizedToken);
        FastList<LinkedPhrase> filteredPhrases = this.filter(phrases);
        return this.preparePhrases(filteredPhrases);
    }

    private FastList<LinkedPhrase> filter(FastList<LinkedPhrase> phrases) {
        FastList<LinkedPhrase> newPhrases = new FastList<LinkedPhrase>();
        for (LinkedPhrase phr : phrases) {
            //System.out.println(phr.span().getTokens());
            LinkedPhrase cleanPhr = cleanPhrase(phr);
            if (cleanPhr == null)
                continue;
            if (cleanPhr.text().length() == 1)
                continue;
            //System.out.println(cleanPhr.span().getTokens());
            newPhrases.add(cleanPhr);
        }
        return newPhrases;
    }

    private LinkedPhrase cleanPhrase(LinkedPhrase phr) {

        List<? extends Token> tokens = phr.span().getTokens();
        List<TaggedToken> tTokens = new FastList<TaggedToken>();
        for (Token t : tokens) {
            tTokens.add((TaggedToken) t);
        }
        if (tokens.size() == 1 && (phr.text().length() <= 1))
            return null;
        int startValid = 0;
        int endValid = tTokens.size();
        while (startValid < tTokens.size()) {
            TaggedToken tt = tTokens.get(startValid);
            if (ValidPOSTags.validStartPOS.contains(tt.getPOS())
                    && !tt.getText().equals("%")) {
                break;
            }
            startValid++;
        }
        while (endValid > 0) {
            TaggedToken tt = tTokens.get(endValid - 1);
            if (tt.getText().equals("+")
                    || (ValidPOSTags.validEndPOS.contains(tt.getPOS()) && !tt.getText()
                    .equals("%"))) {
                break;
            }
            endValid--;
        }
        if (startValid >= endValid || startValid >= tTokens.size()
                || endValid <= 0)
            return null;
        return splitPhrase(phr, startValid, endValid);
    }

    private LinkedPhrase splitPhrase(LinkedPhrase phr, int start, int end) {
        if (start == 0 && end == phr.span().getTokens().size())
            return phr;
        LinkedPhrase sub = subPhrase(phr, start, end);
        if (sub == null)
            return null;
        LinkedPhrase before = subPhrase(phr, 0, start);
        LinkedPhrase after = subPhrase(phr, end, phr.span().getTokens().size());
        if (before != null) {
            if (sub.left() != null) {
                sub.left().right(before);
                before.left(sub.left());
            }
            sub.left(before);
            before.right(sub);
        }
        if (after != null) {
            if (sub.right() != null) {
                sub.right().left(after);
                after.right(sub.right());
            }
            sub.right(after);
            after.left(sub);
        }
        return sub;
    }

    private LinkedPhrase subPhrase(LinkedPhrase phr, int start, int end) {
        if (start >= end)
            return null;
        LinkedPhrase sub = new LinkedPhrase();
        List<? extends Token> tokens = phr.span().getTokens().subList(start, end);
        TokenSpan span = new TokenSpan(tokens.get(0).getStartOffset(), tokens
                .get(end - start - 1).getEndOffset(), tokens, 0);
        sub.span(span);
        sub.text(phr.text().substring(
                span.getStartOffset() - phr.span().getStartOffset(),
                span.getEndOffset() - phr.span().getStartOffset()));
        return sub;
    }

    private FastList<Phrase> preparePhrases(FastList<LinkedPhrase> phrases) {
        FastList<Phrase> newPhrases = new FastList<Phrase>(phrases.size());
        for (LinkedPhrase phr : phrases) {
            phr.setSource(curSource);
            if (phr.left() == null) {
                phr.setBefore(Token.START);
            } else {
                phr.setBefore(phr.left().span().getTokens()
                        .get(phr.left().span().getTokens().size() - 1));
            }
            if (phr.right() == null) {
                phr.setAfter(Token.END);
            } else {
                phr.setAfter(phr.right().span().getTokens().get(0));
            }
            newPhrases.add(phr);
        }
        return newPhrases;
    }

}
