package edu.northwestern.websail.kpe.data;

import edu.northwestern.websail.ds.tuple.IntegerPair;
import edu.northwestern.websail.kpe.data.paper.S2CitationContext;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.tokenizer.StanfordNLPSSplit;
import edu.northwestern.websail.text.util.TokenUtil;
import edu.stanford.nlp.util.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author NorThanapon
 * @since 8/13/15
 */
public class S2PaperUtil {
    // Paper Constants
    public static final int PREF_CITATIONS_TO_KEEP = 20;
    public static final String LOC_TITLE = "TITLE";
    public static final String LOC_ABSTRACT = "ABSTRACT";
    public static final String LOC_INTRO = "INTRODUCTION";
    public static final String LOC_BODY = "BODY";
    public static final String LOC_CITANCE = "CITANCE";
    public static final String LOC_CONCLUSION = "CONCLUSION";
    public static final String OLD_INTRO_END_PATTERN = "\\n2 (?:(?:\\b\\w*\\b) ?){1,5}\\n[A-Z]";
    public static final String INTRO_BEGIN_PATTERN = "Some\\(([A-za-z ]{0,20}?[1I](\\.)?[ ])?(?i)Introduction([ ][1I]?(\\.)?[ ]?[A-za-z ]{0,20}?)?\\)";
    public static final String CONCLUSION_BEGIN_PATTERN = "Some\\((?i)(Conclusion[s]?|Summary)(?i)( and Future Work[s]?)?\\)";
    public static final String DISCUSSION_BEGIN_PATTERN = "Some\\((?i)Discussion[s]?(?i)( and Future Work[s]?)?\\)";
    public static final String SECTION_PATTERN = "Some\\(([^\\)]+)\\)";
    public static final Integer AVG_INTRO_LENGTH = 3500;
    public static final Pattern introBeginPattern = Pattern.compile(INTRO_BEGIN_PATTERN);
    public static final Pattern conclusionBeginPattern = Pattern.compile(S2PaperUtil.CONCLUSION_BEGIN_PATTERN + "|" + S2PaperUtil.DISCUSSION_BEGIN_PATTERN);
    public static final Pattern sectionPattern = Pattern.compile(S2PaperUtil.SECTION_PATTERN);
    public static final IntegerPair NOT_FOUND_OFFSET = new IntegerPair(Integer.MAX_VALUE, Integer.MAX_VALUE);
    // Citation constants
    public static final String nameYearPatternStr = "[\\(\\[][^\\\\)^\\\\][A-Za-z]*,?[ ]?[0-9]{2,4}?[\\)\\]]";
    public static final String etAlPatternStr = "[\\(\\[]([^\\)^\\]]*?(et\\.? al\\.?).*?)[\\)\\]]";
    public static final String yearPattern = "[\\(\\[][0-9]{1,3}[\\)\\]]";
    public static final String multiAuthorPatternStr = "[\\(\\[]" + //open paren
            "((" +
            "([A-Z]+[a-z]+( |,| and )?)+ " + // name(s)
            "([0-9]{2}|[0-9]{4})[a-z]?" + // year + some char i.e. 2001b
            ";?) ?" + // citation separator
            ")+" + // citation(s)
            "[\\)\\]]"; // close paren

    public static final Pattern citationPattern = Pattern.compile(nameYearPatternStr
            + "|" + etAlPatternStr
            + "|" + yearPattern
            + "|" + multiAuthorPatternStr);
    public static final int PENALTY_FOR_NO_CITATION = 100;
    public static final CitationCountComparator citationCountComparator = new CitationCountComparator();
    // Equation and bad parsing
    public static final Pattern NOISE_EQ_PATTERN = Pattern.compile("\\s+([\\(\\[\\{]?(.{1,2}|mod|over|max|min)[\\|\\)\\]\\}\\(\\[\\{=\\+\\*-/~]?[\\|\\s\\)\\]\\}\\(\\[\\{=\\+\\*-/~]+){4,200}");
    // Location Defaults
    private static final String[] locs = {S2PaperUtil.LOC_TITLE, S2PaperUtil.LOC_ABSTRACT, S2PaperUtil.LOC_BODY, S2PaperUtil.LOC_CITANCE};
    public static final HashSet<String> WHOLE_PAPER_LOCATIONS = new HashSet<String>(Arrays.asList(locs));
    private static final String[] taicLocs = {S2PaperUtil.LOC_TITLE, S2PaperUtil.LOC_ABSTRACT, S2PaperUtil.LOC_INTRO, S2PaperUtil.LOC_CITANCE};
    public static final HashSet<String> TAIC_LOCATIONS = new HashSet<String>(Arrays.asList(taicLocs));


    public static void main(String[] args) {
        String text = "<Menace ID= \" Menace . 1 0 5 0 3 8 2 0 5 2 5 3 5 \" l a b e l = \" 01−FIRE \" s e l e c t e d = \" \" d e s c r i p t i o n = \" Type : N a t u r a l /Human/ Env i ronm ental A c c i d e n t a l c a u s e : C o n c e n t r a t i o n o f fl am m abl e o r e x p l o s i v e . . . \" j u s t i f i c a t i o n = \" \" d e s c r i p t i o n M e n a c e E l e m e n t = \" \" p o t e n t i e l = \" \" > Table 1. EBIOS entities and attributes and their corresponding security ontology concepts and relations\nSome(EBIOS XML-elements and at- tributes Security ontology concepts and relations)\n<EntityType> ent: Asset type subclasses of ent:Asset description ent:description of abstract instances of subclasses of ent:Asset <Vulnerability> sec:Vulnerability label subclasses of sec:Vulnerability menace sec:exploitedBy of sec:Vulnerability <EntityTypeList> sec:threatens of sec:Threat <Menace> sec:Threat label subclasses of sec:Threat description sec:description of abstract instances of subclasses of sec:Threat <SeverityScale> sec:affects of sec:Threat <Criteria> sec:SecurityAttribute label instances of sec:SecurityAttribute description sec:description of abstract instances of subclasses of sec:SecurityAttribute <MenaceCauseList> sec:hasSource of sec:Threat <MenaceCause> sec:ThreatSource label subclasses of sec:ThreatSource description sec:description of abstract instances of subclasses of sec:ThreatSource <MenaceOrigineList> sec:hasOrigin of sec:Threat <MenaceOrigine> sec:ThreatOrigin label subclasses of sec:ThreatOrigin description sec:description of abstract instances of subclasses of sec:ThreatOrigin <SecurityObjective> sec:Control label subclasses of sec:Control content sec:description of abstract instances of subclasses of sec:Control <SecurityObjectiveCovers> sec:mitigatedBy of sec:Vulnerability <FunctionnalRequirement> iso:Control abbreviation iso:controlTitle of abstract instances of iso: Control description iso:controlDescription of abstract instances of iso:Control <Objective> sec:correspondsTo of iso:Control <S e v e r i t y S c a l e ID= \" S e v e r i t y S c a l e . 1050973114465 \" > <M e n a c e S e v e r i t y ID= \" M e n a c e S e v e r i t y . 1 1 0 9 4 3 6 1 7 4 0 4 4 \" c r i t e r i a = \" C r i t e r i a . 1 0 1 3 3 0 7 7 4 1 6 4 1 \" s e v e r i t y = \" \" v i o l a t i o n = \" t r u e \" /> <M e n a c e S e v e r i t y ID= \" M e n a c e S e v e r i t y . 1 1 0 9 1 0 8 5 9 7 3 2 0 \" c r i t e r i a = \" C r i t e r i a . 1 0 1 1 6 8 0 6 4 8 0 3 7 \" s e v e r i t y = \" \" v i o l a t i o n = \" t r u e \" /> </ S e v e r i t y S c a l e > <MenaceC aus eLi s t ID= \" MenaceC aus eLi s t . <M e n a c e O r i g i n e L i s t ID= \" M e n a c e O r i g i n e L i s t . 1050973114465 \" / > <MenaceO rigine i d= \" The menaces' attribute Label and Description correspond to the threat sub-concepts and their descriptions.";
        text = removeNoisesAndEquations(removeNoisesAndEquations(cleanCitationContext(cleanSectionHeaders(text))));
        StanfordNLPSSplit spliter = new StanfordNLPSSplit(true);
        for (TokenSpan token : spliter.sentenceSplit(text)) {
            System.out.println("Sentence:");
            System.out.println(token.getTokens());
            System.out.println(TokenUtil.tokens2Str(token.getTokens(), text));
        }
    }


    public static String removeNoisesAndEquations(String text) {
        int idx = 0;
        StringBuilder sb = new StringBuilder();
        Matcher m = NOISE_EQ_PATTERN.matcher(text);
        while (m.find()) {
            sb.append(text.substring(idx, m.start()))
                    .append(StringUtils.repeat(' ', m.end() - m.start()));
            String g = m.group().trim();
            if (g.length() > 0 && g.charAt(g.length() - 1) == '.') {
                sb.setCharAt(sb.length() - 2, '.');
            }
            idx = m.end();
        }
        sb.append(text.substring(idx));
        return sb.toString();
    }

    // Section methods

    /**
     * @param bodyText: text of the paper to find conclusion
     * @return null if conclusion is not found, otherwise start and end offset (in the input bodyText)
     */
    public static IntegerPair locateConclusionOffsets(String bodyText) {
        return locateSectionOffsets(bodyText, conclusionBeginPattern);
    }

    /**
     * @param bodyText: text of the paper to find conclusion
     * @return null if introduction is not found, otherwise start and end offset (in the input bodyText).
     */
    public static IntegerPair locateIntroOffsets(String bodyText) {
        return locateSectionOffsets(bodyText, introBeginPattern);
    }

    private static IntegerPair locateSectionOffsets(String bodyText, Pattern startPattern) {
        Matcher m = startPattern.matcher(bodyText);
        if (m.find()) {
            int conclusionStart = m.start();
            int conclusionStrLen = m.group(0).length();
            m = sectionPattern.matcher(bodyText.substring(conclusionStart + conclusionStrLen));
            int conclusionEndIndex;
            if (m.find()) {
                conclusionEndIndex = m.start() + conclusionStart + conclusionStrLen;
            } else {
                conclusionEndIndex = bodyText.length();
            }
            return new IntegerPair(conclusionStart + conclusionStrLen, conclusionEndIndex);
        }
        return null;
    }

    public static IntegerPair approximateIntroLenght(String bodyText) {
        if (bodyText.trim().length() == 0) return NOT_FOUND_OFFSET;
        Matcher m = introBeginPattern.matcher(bodyText);
        int startIndex = 0;
        int introStartStrLen = 0;
        boolean startFound = false;
        if (m.find()) {
            startIndex = m.start();
            introStartStrLen = m.group(0).length();
            startFound = true;
        }
        m = sectionPattern.matcher(bodyText.substring(startIndex + introStartStrLen));
        if (m.find() && startFound) {
            int endIndex = m.start();
            return new IntegerPair(startIndex + introStartStrLen, endIndex + introStartStrLen + startIndex);
        } else {
            int offset = startIndex + introStartStrLen + S2PaperUtil.AVG_INTRO_LENGTH < bodyText.length() ?
                    startIndex + introStartStrLen + S2PaperUtil.AVG_INTRO_LENGTH : bodyText.length() - 1;
            char curChar = bodyText.charAt(offset);
            while (curChar != ' ' && curChar != '\n' && curChar != '\t' && offset < bodyText.length()) {
                offset++;
                curChar = bodyText.charAt(offset);
            }
            return new IntegerPair(startIndex + introStartStrLen, offset);
        }
    }

    public static String cleanSectionHeaders(String bodyText) {
        StringBuilder sb = new StringBuilder();
        Matcher m = S2PaperUtil.sectionPattern.matcher(bodyText);
        int idx = 0;
        while (m.find()) {
            sb.append(bodyText.substring(idx, m.start())).append("     ");
            sb.append(m.group().substring(5, m.group().length() - 1))
                    .append(' ');
            idx = m.end();
        }
        if (idx <= bodyText.length()) {
            sb.append(bodyText.substring(idx));
        }
        return sb.toString();
    }

    // Citation methods
    public static int countCitations(String text) {
        int citationCount = 0;
        Matcher m = citationPattern.matcher(text);
        while (m.find()) {
            citationCount++;
        }
        if (citationCount == 0) {
            citationCount = PENALTY_FOR_NO_CITATION;
        }
        return citationCount;
    }

    public static ArrayList<S2CitationContext> getPreferredCitationContexts(ArrayList<S2CitationContext> contexts) {
        ArrayList<S2CitationContext> sorted = new ArrayList<S2CitationContext>(contexts);
        if (sorted.size() == 0) return sorted;
        Collections.sort(sorted, citationCountComparator);
        if (sorted.size() < PREF_CITATIONS_TO_KEEP) return sorted;
        return new ArrayList<S2CitationContext>(sorted.subList(0, PREF_CITATIONS_TO_KEEP));
    }

    public static String cleanCitationContext(String context) {
        int start = 0;
        StringBuilder newCitanceSB = new StringBuilder();
        Matcher matcher = citationPattern.matcher(context);
        while (matcher.find()) {
            //System.out.println(context.substring(matcher.start(), matcher.end()));
            newCitanceSB.append(context.substring(start, matcher.start()));
            newCitanceSB.append('(');
            newCitanceSB.append(StringUtils.repeat(' ', matcher.end() - matcher.start() - 2));
            newCitanceSB.append(')');
            start = matcher.end();
        }
        newCitanceSB.append(context.substring(start, context.length()));
        return newCitanceSB.toString();
    }

    public static String cleanAbstract(String text) {
        String lowAbstr = text.toLowerCase().trim();
        if (lowAbstr.startsWith("abstract")) {
            text = text.replaceFirst("(?i)Abstract", "        ");
        } else if (lowAbstr.startsWith("0 abstract")) {
            text = text.replaceFirst("(?i)0 Abstract", "          ");
        } else if (lowAbstr.startsWith("1 abstract")) {
            text = text.replaceFirst("(?i)1 Abstract", "          ");
        }
        return text;
    }

    // Cleaning

    public static class CitationCountComparator implements Comparator<S2CitationContext> {
        @Override
        public int compare(S2CitationContext c1, S2CitationContext c2) {
            return c1.getCitationCount() - c2.getCitationCount();
        }
    }
}
