package edu.northwestern.websail.kpe.main.postprocessing;

import edu.northwestern.websail.core.io.InputFileManager;
import edu.northwestern.websail.core.util.TimerUtil;
import edu.northwestern.websail.kpe.data.LabelAdapter;
import edu.northwestern.websail.kpe.data.kp.KeyPhrase;
import edu.northwestern.websail.text.tokenizer.StanfordNLPTokenizer;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author NorThanapon
 * @since 1/30/15
 */
public class FormatSemEval {
    public static void main(String[] args) {
        TimerUtil timer = new TimerUtil();
        timer.start();
        String dataFile = "/Users/NorThanapon/Desktop/semeval/semeval_dedup_key_phrases.txt";
        //String outputFile = null;
        String answerFile = "/Users/NorThanapon/Desktop/semeval/test.combined.stem.final";
//        Options options = new Options();
//        options.addOption("in", true, "Input key file in AI2 format (TSV)");
//        options.addOption("out", true, "Output key file in SemEval format (CSV)");
//        options.addOption("ids", true, "[Optional] SemEval answer file used to remove keys from other papers");
//
//        CommandLineParser parser = new GnuParser();
//        boolean argsNotOk = false;
//        try {
//            CommandLine cmd = parser.parse(options, args);
//            dataFile = cmd.getOptionValue("in");
//            answerFile = cmd.getOptionValue("ids");
//            outputFile = cmd.getOptionValue("out");
//            if (dataFile == null || outputFile == null) {
//                argsNotOk = true;
//            }
//        } catch (ParseException e) {
//            argsNotOk = true;
//        }
//        if (argsNotOk) {
//            HelpFormatter formatter = new HelpFormatter();
//            formatter.printHelp("WritePaperTokens [options]", options);
//            System.exit(0);
//        }
        StanfordNLPTokenizer tokenizer = new StanfordNLPTokenizer();
        HashMap<String, ArrayList<KeyPhrase>> keys = LabelAdapter.readTsvLabelFile(dataFile, tokenizer, true, true);
        InputFileManager inMgr = new InputFileManager(answerFile);
        String line;
        while ((line = inMgr.readLine()) != null) {
            if (line.trim().equals("")) continue;
            String id = line.split(" : ")[0];
            ArrayList<KeyPhrase> keyphrases = keys.get(id);
            StringBuilder sb = new StringBuilder();
            sb.append(id).append(" : ");
            if (keyphrases != null) {
                for (KeyPhrase kp : keyphrases) {
                    sb.append(kp.getNormalizedPhrase()).append(',');
                }
            }
            System.out.println(sb.substring(0, sb.length() - 1));
        }
        inMgr.close();
    }
}
