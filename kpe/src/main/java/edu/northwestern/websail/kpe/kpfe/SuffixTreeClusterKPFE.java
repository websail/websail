package edu.northwestern.websail.kpe.kpfe;

import edu.northwestern.websail.classify.data.CommonNominal;
import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.clustering.stc.STC;

import java.util.Arrays;
import java.util.Properties;

import static edu.northwestern.websail.classify.data.Feature.Type.NOMINAL;
import static edu.northwestern.websail.classify.data.Feature.Type.NUMBER;

/**
 * @author csbhagav on 1/8/15.
 *         modified: NorThanapon
 */
@PipelineConfigInfo(
        requiredResources = {PEVResourceType.STC, PEVResourceType.STC_CASE}
)
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class}
)
public class SuffixTreeClusterKPFE extends KPFeatureExtractor {

    public static final Feature stcCaseFoldedFound = new Feature("stc", "found", NOMINAL, CommonNominal.boolMap);
    public static final Feature stcCaseFoldedDocFreq = new Feature("stc", "freq", NUMBER);
    public static final Feature stcCaseSensitiveFound = new Feature("stc", "cfound", NOMINAL, CommonNominal.boolMap);
    public static final Feature stcCaseSensitiveDocFreq = new Feature("stc", "cfreq", NUMBER);
    public static final Feature capitalizedFraction = new Feature("stc", "capitalizedFraction", NUMBER);
    private static final Feature[] features = {stcCaseFoldedFound, stcCaseFoldedDocFreq, stcCaseSensitiveFound,
            stcCaseSensitiveDocFreq, capitalizedFraction};
    private static boolean registered = false;
    STC stcCaseFolded;
    STC stcCaseSensitive;

    public SuffixTreeClusterKPFE() {
        curValues = new double[features.length];
        Arrays.fill(curValues, Instance.MISSING);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config)) return false;

        stcCaseFolded = (STC) env.getResource(PEVResourceType.STC);
        if (stcCaseFolded == null) {
            PipelineEnvironment.logResourceNotFound(PEVResourceType.STC, logger);
            return false;
        }

        stcCaseSensitive = (STC) env.getResource(PEVResourceType.STC_CASE);
        if (stcCaseSensitive == null) {
            PipelineEnvironment.logResourceNotFound(PEVResourceType.STC_CASE, logger);
            return false;
        }

        return true;
    }

    @Override
    protected void extract(Instance<Phrase> phraseInstance) throws Exception {
        String surface = phraseInstance.getSource().getSurfaceForm();
        int freq = stcCaseFolded.phraseDocFrequency(surface.toLowerCase());
        curValues[0] = freq > 0 ? CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
        curValues[1] = freq;
        int freqCS = stcCaseSensitive.phraseDocFrequency(surface);
        curValues[2] = freqCS > 0 ? CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
        curValues[3] = freqCS;
        curValues[4] = freq > 0 ? (double) freqCS / ((double) freq + 1) : 0.0;
    }

    @Override
    protected Feature[] features() {
        return features;
    }

    @Override
    protected boolean initForBuck(PipelineEnvironment env, S2PaperBuck prev) {
        return true;
    }

    @Override
    protected boolean initForUnit(BuckUnit unit) {
        return true;
    }

    @Override
    protected boolean register() {
        return registered;
    }

    @Override
    protected void registered() {
        registered = true;
    }
}
