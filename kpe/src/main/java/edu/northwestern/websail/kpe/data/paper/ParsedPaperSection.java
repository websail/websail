package edu.northwestern.websail.kpe.data.paper;

/**
 * @author Northanapon
 */
@Deprecated
public class ParsedPaperSection {
    //a list of section -> [u'body', u'header', u'genericHeader'] all text...
    private String header;
    private String genericHeader;
    private String body;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getGenericHeader() {
        return genericHeader;
    }

    public void setGenericHeader(String genericHeader) {
        this.genericHeader = genericHeader;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


}
