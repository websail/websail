package edu.northwestern.websail.kpe.pe.helper;

import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.util.TokenUtil;
import org.tartarus.snowball.ext.EnglishStemmer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @author NorThanapon
 * @since 11/20/14
 */
public class FilterTerms {

    public static final ArrayList<String> badPhrases = new ArrayList<String>();
    public static final ArrayList<String> badKeys;

    static {
        badPhrases.add("section");
        badPhrases.add("chapter");
        badPhrases.add("number of");
        badPhrases.add("figure");
        badPhrases.add("january");
        badPhrases.add("february");
        badPhrases.add("march");
        badPhrases.add("april");
        badPhrases.add("may");
        badPhrases.add("june");
        badPhrases.add("july");
        badPhrases.add("august");
        badPhrases.add("september");
        badPhrases.add("october");
        badPhrases.add("november");
        badPhrases.add("december");
        badPhrases.add("et");
        badPhrases.add("@");
        badPhrases.add("=");
        badPhrases.add("%");
        badPhrases.add("#");
        badPhrases.add("~");
        badPhrases.add("(");
        badPhrases.add(")");
        badPhrases.add("[");
        badPhrases.add("]");
        badPhrases.add("{");
        badPhrases.add("}");
        badPhrases.add(">");
        badPhrases.add("<");
        badPhrases.add(";");
        badPhrases.add(":");

        //badPhrases.add("alternative");
        //badPhrases.add("candidate");
        badPhrases.add("careful");
        badPhrases.add("different");
        badPhrases.add("experimental");
        badPhrases.add("important");
        badPhrases.add("individual");
        badPhrases.add("initial");
        badPhrases.add("input");
        badPhrases.add("interesting");
        badPhrases.add("introduction");
        badPhrases.add("larger");
        badPhrases.add("paper");
        badPhrases.add("possible");
        badPhrases.add("previous");
        badPhrases.add("recent");
        badPhrases.add("conclusion");
        badPhrases.add("research");
        badPhrases.add("several");
        badPhrases.add("state-of-the-art");
        badKeys = new ArrayList<String>();
        for (String bphr : badPhrases) {
            String[] parts = bphr.split(" ");
            List<Token> tokens = new ArrayList<Token>();
            for (String p : parts) {
                Token t = new Token(p, 0, 0, 0);
                tokens.add(t);
            }
            TokenUtil.normalize(tokens, true, true);
            badKeys.add(tokensToKey(tokens));
        }
        badPhrases.clear();
        badPhrases.add("january");
        badPhrases.add("february");
        badPhrases.add("march");
        badPhrases.add("april");
        badPhrases.add("may");
        badPhrases.add("june");
        badPhrases.add("july");
        badPhrases.add("august");
        badPhrases.add("september");
        badPhrases.add("october");
        badPhrases.add("november");
        badPhrases.add("december");
        badPhrases.add("@");
        badPhrases.add("=");
        badPhrases.add("%");
        badPhrases.add("#");
        badPhrases.add("~");
        badPhrases.add("(");
        badPhrases.add(")");
        badPhrases.add("[");
        badPhrases.add("]");
        badPhrases.add("{");
        badPhrases.add("}");
        badPhrases.add(">");
        badPhrases.add("<");
        badPhrases.add(";");
        badPhrases.add(":");
        badPhrases.add("e.g.");
        badPhrases.add("etc.");
        badPhrases.add("al.");
        badPhrases.add("w.r.t.");
        badPhrases.add("-lrb-");
        badPhrases.add("-rrb-");
        badPhrases.add("-lcb-");
        badPhrases.add("-rcb-");
        badPhrases.add("-lsb-");
        badPhrases.add("-rsb-");
    }

    public static final ArrayList<String> badStartList = new ArrayList<String>();
    public static final HashSet<String> badStartSet;
    public static final ArrayList<String> badEndList = new ArrayList<String>();
    public static final HashSet<String> badEndSet;

    static {
        badStartList.add("a");
        badStartList.add("the");
        badStartList.add("an");
        badStartList.add("new");
        badStartList.add("baseline");
        badStartList.add("basic");
        badStartList.add("better");
        badStartList.add("certain");
        badStartList.add("many");
        badStartList.add("much");
        badStartList.add("such");
        badStartList.add("other");
        badStartList.add("accurate");
        badStartList.add("additional");
        badStartList.add("available");
        badStartList.add("current");
        badStartList.add("effective");
        badStartList.add("most");
        badStartList.add("improving");
        badStartList.add("particular");
        badStartList.add("novel");
        badStartList.add("good");
        badStartList.add("first");
        badStartList.add("in");
        //badStartList.add("standard");
        badStartList.add("kind");
        badStartList.add("of");
        badStartList.add("use");
        badStartList.add("type");
        badStartList.add("idea");
        badStartList.add("final");
        badStartList.add("novel");
        badStartList.add("set");
        badStartList.add("application");
        badStartList.add("similar");
        badStartList.add("\"");
        badStartList.add("'");
        badStartList.add("''");
        badStartList.add("(");
        badStartList.add(")");
        badStartList.add("[");
        badStartList.add("]");
        badStartList.add("{");
        badStartList.add("}");
        badStartList.add("-lrb-");
        badStartList.add("-rrb-");
        badStartList.add("-lcb-");
        badStartList.add("-rcb-");
        badStartList.add("-lsb-");
        badStartList.add("-rsb-");
        badStartSet = new HashSet<String>();
        EnglishStemmer stemmer = new EnglishStemmer();
        for (String bphr : badStartList) {
            stemmer.setCurrent(bphr.toLowerCase());
            stemmer.stem();
            badStartSet.add(stemmer.getCurrent());
        }

        badEndList.add("result");
        badEndList.add("performance");
        badEndList.add("problem");
        badEndList.add("task");
        badEndList.add("system");
        badEndList.add("\"");
        badEndList.add("'");
        badEndList.add("''");
        badEndList.add("-lrb-");
        badEndList.add("-rrb-");
        badEndList.add("-lcb-");
        badEndList.add("-rcb-");
        badEndList.add("-lsb-");
        badEndList.add("-rsb-");
        badEndList.add("(");
        badEndList.add(")");
        badEndList.add("[");
        badEndList.add("]");
        badEndList.add("{");
        badEndList.add("}");

        badEndSet = new HashSet<String>();

        for (String bphr : badEndList) {
            stemmer.setCurrent(bphr.toLowerCase());
            stemmer.stem();
            badEndSet.add(stemmer.getCurrent());
        }
    }




    public static String tokensToKey(List<? extends Token> tokens) {
        StringBuilder sb = new StringBuilder();
        sb.append("_");
        for (Token t : tokens) {
            sb.append(t.getText());
            sb.append("_");
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(badEndSet);
    }

}
