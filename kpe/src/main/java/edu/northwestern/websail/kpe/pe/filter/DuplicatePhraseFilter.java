package edu.northwestern.websail.kpe.pe.filter;


import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.ds.tuple.IntegerPair;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.phrase.Phrase;

import java.util.HashSet;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 * @since 11/19/14
 */
@PipelineConfigInfo()
@PipelineBUTInfo(
        consumes = {KPEBUT.Sentences.class, KPEBUT.Phrases.class},
        produces = {KPEBUT.Phrases.class}
)
public class DuplicatePhraseFilter implements PipelineExecutable {
    public static final Logger logger = Logger
            .getLogger(DuplicatePhraseFilter.class.getName());
    int threadId = 0;

    public DuplicatePhraseFilter() {
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        for (BuckUnit unit : prev.units()) {
            if (unit.isNotOkay())
                continue;
            try {
                FastList<Phrase> phrases = unit.get(KPEBUT.Phrases.class);
                FastList<Phrase> filteredPhrases = new FastList<Phrase>();
                HashSet<IntegerPair> set = new HashSet<IntegerPair>();
                for (Phrase phrase : phrases) {
                    IntegerPair offset = new IntegerPair(phrase.getStartOffset(), phrase.getEndOffset());
                    if (!set.contains(offset)) {
                        set.add(offset);
                        filteredPhrases.add(phrase);
                    }
                }
                unit.set(KPEBUT.Phrases.class, filteredPhrases);
            } catch (Exception e) {
                logger.severe("Unexpected error while filtering phrases.");
                e.printStackTrace();
            }
        }

        return true;
    }


    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public int getThreadId() {
        return threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }

}
