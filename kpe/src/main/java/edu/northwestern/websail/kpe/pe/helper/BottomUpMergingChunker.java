package edu.northwestern.websail.kpe.pe.helper;

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.kpe.data.phrase.LinkedPhrase;
import edu.northwestern.websail.kpe.data.phrase.ProbScorePhrase;
import edu.northwestern.websail.text.data.TaggedToken;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.ngram.NgramMapCountWrapper;
import edu.northwestern.websail.text.tokenizer.SentenceSpliter;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.*;
import java.util.logging.Logger;


/**
 * @author NorThanapon
 */
public class BottomUpMergingChunker {

    public static final Logger logger = Logger.getLogger(BottomUpMergingChunker.class.getName());
    public static final double breakPOSPenalty = 50;
    public static final double smoothing = 0.1;
    public static final double mergeThreshold = -2.5;
    public static final double breakThreshold = -3.5;
    private static final HashSet<String> breakPOS = new HashSet<String>();
    static {
        breakPOS.add(":");
        breakPOS.add("-LRB-");
        breakPOS.add("-LSB-");
        breakPOS.add("-LCB-");
        breakPOS.add("-RRB-");
        breakPOS.add("-RSB-");
        breakPOS.add("-RCB-");
        breakPOS.add("-N-");
        breakPOS.add(".");
        breakPOS.add(",");
        breakPOS.add("PRP");
        breakPOS.add("PRP$");

    }
    private static final ThresholdScoreComparator<LinkedPhrase> thresholdComparator = new ThresholdScoreComparator<LinkedPhrase>(
            breakPOS);
    // ===============================================================
    // Phrase Merging and Selecting
    // ===============================================================
    private static final OffsetComparator<LinkedPhrase> offsetComparator = new OffsetComparator<LinkedPhrase>();
    private static final HashSet<String> finalBreakPOS = new HashSet<String>();
    static {
        finalBreakPOS.addAll(breakPOS);
        finalBreakPOS.add("DT");
        finalBreakPOS.add("RB");
        finalBreakPOS.add("RBR");
        finalBreakPOS.add("RBS");
    }
    private static final ThresholdScoreComparator<LinkedPhrase> finalThresholdComparator = new ThresholdScoreComparator<LinkedPhrase>(
            finalBreakPOS);
    // ===============================================================
    // Phrase creation
    // ===============================================================
    private final NgramMapCountWrapper countMap;
    private final SentenceSpliter ssplit;

    public BottomUpMergingChunker(NgramMapCountWrapper countMap,
                                  SentenceSpliter ssplit) {
        this.countMap = countMap;
        this.ssplit = ssplit;
    }

    public static String displayChunk(FastList<LinkedPhrase> phrases) {
        StringBuilder sb = new StringBuilder();
        for (LinkedPhrase phr : phrases) {
            sb.append("[").append(phr.text()).append("] ");
        }
        return sb.toString();
    }

    private static <T extends ProbScorePhrase> double thresholdScore(T phrase) {
        return phrase.corpusSCP() - penaltyRule(phrase, breakPOS);
    }

    private static <T extends ProbScorePhrase> double thresholdScore(T phrase,
                                                                     HashSet<String> breakPOS) {
        return phrase.corpusSCP() - penaltyRule(phrase, breakPOS);
    }

    private static <T extends ProbScorePhrase> double penaltyRule(T phr,
                                                                  HashSet<String> breakPOS) {
        if (phr.span().getTokens().size() == 1)
            return 0;
        double penalty = 0.0;
        for (Token t : phr.span().getTokens()) {
            TaggedToken tt = (TaggedToken) t;
            if (breakPOS.contains(tt.getPOS())) {
                penalty += breakPOSPenalty;
            }
        }
        return penalty;
    }

    // ===============================================================
    // Scorer
    // ===============================================================

    public FastList<LinkedPhrase> chunk(String text) {
        return this.chunk(text, ssplit.sentenceSplit(text), true);
    }

    public FastList<LinkedPhrase> chunk(String text, List<TokenSpan> sentences, boolean normalize) {
        FastList<LinkedPhrase> out = new FastList<LinkedPhrase>();
        for (TokenSpan span : sentences) {
            List<? extends Token> tokens = span.getTokens();
            if (normalize) {
                tokens = TokenUtil.cleanTokens(TokenUtil.normalize(tokens, true, true), false, null);
            }
            //System.out.println(tokens);
            PriorityQueue<LinkedPhrase> heap = initAllPairs(text, tokens);
            if (heap == null) continue;
            merge(text, heap);
            FastList<LinkedPhrase> outList = new FastList<LinkedPhrase>();
            recSelectPhrases(text, outList, heap, tokens);
            Collections.sort(outList, offsetComparator);
            fillMissingTokens(text, outList, tokens);
            connectSortedPhrases(outList);
            //FastList<LinkedPhrase> outList = selectPhrases(text, heap, tokens);
            out.addAll(outList);
        }
        return out;
    }

    private void merge(String fullText, PriorityQueue<LinkedPhrase> heap) {
        double incr = -0.01;
        double order = incr;
        //System.out.println(heap);
        while (heap.size() > 1) {
            LinkedPhrase bestPhr = heap.peek();
            if (thresholdScore(bestPhr) < mergeThreshold) {
                break;
            }
            bestPhr = heap.poll();
            LinkedPhrase right = bestPhr.right();
            LinkedPhrase left = bestPhr.left();
            LinkedPhrase newPhrRight = mergeRight(fullText, bestPhr);
            LinkedPhrase newPhrLeft = mergeLeft(fullText, bestPhr);
            if (newPhrRight == null && newPhrLeft == null) {
                heap.add(bestPhr);
                break;
            }
            if (newPhrRight != null) {
                newPhrRight.corpusSCP(computeSCP(bestPhr, bestPhr.right(),
                        newPhrRight));
                if (thresholdScore(newPhrRight) < breakThreshold) {
                    newPhrRight = right;
                }
            }
            if (newPhrLeft != null) {
                newPhrLeft.corpusSCP(computeSCP(bestPhr.left(), bestPhr,
                        newPhrLeft));
                if (thresholdScore(newPhrLeft) < breakThreshold) {
                    newPhrLeft = left;
                }
            }

            if (newPhrLeft == bestPhr.left() && newPhrRight == bestPhr.right()) {
                bestPhr.corpusSCP(mergeThreshold + order);
                order += incr;
                heap.add(bestPhr);
                continue;
            }

            if (newPhrRight != null
                    && newPhrRight != bestPhr.right()) {
                newPhrRight.left(newPhrLeft);
                if (right != null) {
                    newPhrRight.right(right.right());
                    if (right.right() != null) {
                        right.right().left(newPhrRight);
                    }
                }
                heap.remove(bestPhr.right());
                heap.add(newPhrRight);
            } else if (right != null) {
                right.left(newPhrLeft);
            }

            if (newPhrLeft != null &&
                    newPhrLeft != bestPhr.left()) {
                newPhrLeft.right(newPhrRight);
                if (left != null) {
                    newPhrLeft.left(left.left());
                    if (left.left() != null) {
                        left.left().right(newPhrLeft);
                    }
                }
                heap.remove(bestPhr.left());
                heap.add(newPhrLeft);
            } else if (left != null) {
                left.right(newPhrRight);
            }
        }
    }

    // ===============================================================
    // Hyper-parameters
    // ===============================================================

    private PriorityQueue<LinkedPhrase> initAllPairs(String fullText,
                                                     List<? extends Token> tokens) {
        if (tokens.size() == 0) {
            return null;
        }
        PriorityQueue<LinkedPhrase> heap = new PriorityQueue<LinkedPhrase>(
                tokens.size(), thresholdComparator);
        LinkedPhrase prev = null;
        for (int i = 0; i < tokens.size() - 1; i++) {
            Token leftToken = tokens.get(i);
            Token rightToken = tokens.get(i + 1);
            LinkedPhrase phr = phrase(fullText, leftToken, rightToken);
            phr.left(prev).corpusSCP(computeSCP(leftToken, rightToken));
            if (prev != null)
                prev.right(phr);
            prev = phr;

            heap.add(phr);
        }
        return heap;
    }

    private void recSelectPhrases(String fullText, FastList<LinkedPhrase> phrases,
                                  PriorityQueue<LinkedPhrase> queue, List<? extends Token> tokens) {
        PriorityQueue<LinkedPhrase> heap = new PriorityQueue<LinkedPhrase>(
                queue.size() <= 0 ? 1 : queue.size(), finalThresholdComparator);
        heap.addAll(queue);
        if (heap.size() == 0) return;
        LinkedPhrase phr = heap.poll();
        if (phr.status() == LinkedPhrase.Status.INIT
                && thresholdScore(phr) < breakThreshold) {
            phrases.addAll(breakPair(fullText, phr));
        } else {
            phrases.add(phr);
        }

        List<Token> leftTokens = new FastList<Token>();
        for (Token token : tokens) {
            if (token.getEndOffset() <= phr.getStartOffset()) {
                leftTokens.add(token);
            }
        }
        List<Token> rightTokens = new FastList<Token>();
        for (Token token : tokens) {
            if (token.getStartOffset() >= phr.getEndOffset()) {
                rightTokens.add(token);
            }
        }
        if (leftTokens.size() > 0) {
            PriorityQueue<LinkedPhrase> leftQueue = initAllPairs(fullText, leftTokens);
            merge(fullText, leftQueue);
            recSelectPhrases(fullText, phrases, leftQueue, leftTokens);
        }
        if (rightTokens.size() > 0) {
            PriorityQueue<LinkedPhrase> rightQueue = initAllPairs(fullText, rightTokens);
            merge(fullText, rightQueue);
            recSelectPhrases(fullText, phrases, rightQueue, rightTokens);
        }
    }

//    private FastList<LinkedPhrase> selectPhrases(String fullText,
//                                                  PriorityQueue<LinkedPhrase> queue, List<? extends Token> tokens) {
//        FastList<LinkedPhrase> phrases = new FastList<LinkedPhrase>();
//        PriorityQueue<LinkedPhrase> heap = new PriorityQueue<LinkedPhrase>(
//                queue.size() <= 0 ? 1 : queue.size(), finalThresholdComparator);
//        heap.addAll(queue);
//
//        while (heap.size() != 0) {
//            //System.out.println(heap);
//            LinkedPhrase phr = heap.poll();
//
//            if (phr.left() != null) {
//                heap.remove(phr.left());
////                if(phr.left().status() != LinkedPhrase.Status.INIT)
////                    heap.addAll(removeOverlap(phrases, substractRight(fullText, phr.left(), phr)));
//            }
//            if (phr.right() != null) {
//                heap.remove(phr.right());
////                if(phr.right().status() != LinkedPhrase.Status.INIT)
////                    heap.addAll(removeOverlap(phrases, substractLeft(fullText, phr, phr.right())));
//            }
//            if (phr.status() == LinkedPhrase.Status.INIT
//                    && thresholdScore(phr) < breakThreshold) {
//                phrases.addAll(breakPair(fullText, phr));
//            } else {
//                phrases.add(phr);
//            }
//        }
//        Collections.sort(phrases, offsetComparator);
//
//        fillMissingTokens(fullText, phrases, tokens);
//        connectSortedPhrases(phrases);
//        return phrases;
//    }

    private void fillMissingTokens(String fullText,
                                   FastList<LinkedPhrase> phrases, List<? extends Token> tokens) {
        int iPhr = 0;
        int iToken = 0;
        while (iToken < tokens.size()) {
            Token t = tokens.get(iToken);
            LinkedPhrase phr;
            if (iPhr >= phrases.size()) {
                phrases.add(phrase(fullText, t));
            }
            phr = phrases.get(iPhr);
            if (phr.span().getStartOffset() == t.getStartOffset()) {
                iToken += phr.span().getTokens().size();
            } else if (phr.span().getStartOffset() > t.getStartOffset()) {
                phrases.add(iPhr, phrase(fullText, t));
                iToken++;
            }
            iPhr++;
        }
    }

    private void connectSortedPhrases(FastList<LinkedPhrase> phrases) {
        LinkedPhrase prev = null;
        for (LinkedPhrase phr : phrases) {
            phr.left(prev);
            if (prev != null)
                prev.right(phr);
            prev = phr;
        }
    }

    private LinkedPhrase mergeRight(String fullText, LinkedPhrase phr) {
        LinkedPhrase rightPhr = phr.right();
        phr.status(LinkedPhrase.Status.MERGED);
        if (rightPhr == null)
            return null;
        rightPhr.status(LinkedPhrase.Status.MERGED);
        return merge(fullText, phr, rightPhr);
    }

    private LinkedPhrase mergeLeft(String fullText, LinkedPhrase phr) {
        LinkedPhrase leftPhr = phr.left();
        phr.status(LinkedPhrase.Status.MERGED);
        if (leftPhr == null) {
            return null;
        }
        leftPhr.status(LinkedPhrase.Status.MERGED);
        return merge(fullText, leftPhr, phr);
    }

    private LinkedPhrase merge(String fullText, LinkedPhrase leftPhr,
                               LinkedPhrase rightPhr) {
        if (leftPhr == null || rightPhr == null)
            return null;
        LinkedPhrase mergedPhr = new LinkedPhrase();
        FastList<Token> tokens = new FastList<Token>();
        int startIndex = nonOverlapIndex(leftPhr.span().getTokens(), rightPhr.span().getTokens());
        tokens.addAll(leftPhr.span().getTokens());
        tokens.addAll(rightPhr.span().getTokens()
                .subList(startIndex, rightPhr.span().getTokens().size()));
        TokenSpan span = new TokenSpan(leftPhr.span().getStartOffset(),
                rightPhr.span().getEndOffset(), tokens, 0);
        mergedPhr.span(span).text(
                fullText.substring(span.getStartOffset(), span.getEndOffset()));
        mergedPhr.status(LinkedPhrase.Status.EXPANDED);
        return mergedPhr;
    }

//    private FastList<LinkedPhrase> substractRight(String fullText, LinkedPhrase left, LinkedPhrase right) {
//        int index = nonOverlapIndex(left.getSpan().getTokens(), right.getSpan().getTokens());
//        List<? extends Token> nonOverlapLeft = left.getSpan().getTokens().subList(0, index);
//        FastList<LinkedPhrase> phrases = new FastList<LinkedPhrase>(initAllPairs(fullText, nonOverlapLeft));
//        if (phrases.size() != 0) {
//            phrases.get(0).left(left.left());
//            phrases.get(phrases.size() - 1).right(null);
//        }
//        return phrases;
//    }
//
//    private FastList<LinkedPhrase> substractLeft(String fullText, LinkedPhrase left, LinkedPhrase right) {
//        int index = nonOverlapIndex(left.getSpan().getTokens(), right.getSpan().getTokens());
//        List<? extends Token> nonOverlapRight = right.getSpan().getTokens().subList(index, right.getSpan().getTokens().size());
//        FastList<LinkedPhrase> phrases = new FastList<LinkedPhrase>(initAllPairs(fullText, nonOverlapRight));
//        if (phrases.size() != 0) {
//            phrases.get(0).left(null);
//            phrases.get(phrases.size() - 1).right(right.right());
//        }
//        return phrases;
//    }

    private int nonOverlapIndex(List<? extends Token> leftTokens, List<? extends Token> rightTokens) {
        Token lastTokenOfLeft = leftTokens.get(leftTokens.size() - 1);
        int index;
        for (index = 0; index < rightTokens.size() - 1; index++) {
            Token right = rightTokens.get(index);
            if (lastTokenOfLeft.getStartOffset() == right.getStartOffset()
                    && lastTokenOfLeft.getEndOffset() == right.getEndOffset())
                return index + 1;
        }
        logger.severe("Left tokens: " + leftTokens + " contain right token: " + rightTokens);
        return rightTokens.size() - 1;
    }

//    private FastList<LinkedPhrase> removeOverlap(FastList<LinkedPhrase> ref, FastList<LinkedPhrase> tocheck) {
//        FastList<LinkedPhrase> filtered = new FastList<LinkedPhrase>();
//        for (LinkedPhrase ch : tocheck) {
//            boolean overlap = false;
//            for (LinkedPhrase r : ref) {
//                if (ch.getSpan().getEndOffset() >= r.getSpan().getStartOffset() && ch.getSpan().getEndOffset() <= r.getSpan().getEndOffset() ||
//                        ch.getSpan().getStartOffset() >= r.getSpan().getStartOffset() && ch.getSpan().getStartOffset() <= r.getSpan().getEndOffset()) {
//                    overlap = true;
//                }
//            }
//            if(!overlap) {
//                filtered.add(ch);
//            }
//        }
//        return filtered;
//    }


    private FastList<LinkedPhrase> breakPair(String fullText, LinkedPhrase phr) {
        FastList<LinkedPhrase> phrases = new FastList<LinkedPhrase>();
        List<? extends Token> tokens1 = phr.span().getTokens().subList(0, 1);
        List<? extends Token> tokens2 = phr.span().getTokens().subList(1, 2);
        TokenSpan span1 = new TokenSpan(tokens1.get(0).getStartOffset(),
                tokens1.get(0).getEndOffset(), tokens1, 0);
        TokenSpan span2 = new TokenSpan(tokens2.get(0).getStartOffset(),
                tokens2.get(0).getEndOffset(), tokens2, 0);
        LinkedPhrase phr1 = new LinkedPhrase();
        LinkedPhrase phr2 = new LinkedPhrase();

        phr1.left(phr.left()).right(phr2);
        phr2.left(phr1);
        if (phr.left() != null)
            phr.left().right(phr1);
        phr1.span(span1)
                .text(fullText.substring(span1.getStartOffset(),
                        span1.getEndOffset()));
        phr2.span(span2)
                .text(fullText.substring(span2.getStartOffset(),
                        span2.getEndOffset()));
        phrases.add(phr1);
        phrases.add(phr2);
        return phrases;
    }

    private LinkedPhrase phrase(String fullText, Token token) {
        LinkedPhrase phr = new LinkedPhrase();
        FastList<Token> tokenList = new FastList<Token>();
        tokenList.add(token);
        TokenSpan span = new TokenSpan(token.getStartOffset(),
                token.getEndOffset(), tokenList, 0);
        phr.span(span).text(
                fullText.substring(span.getStartOffset(), span.getEndOffset()));
        return phr;
    }

    private LinkedPhrase phrase(String fullText, Token leftToken,
                                Token rightToken) {
        LinkedPhrase phr = new LinkedPhrase();
        FastList<Token> tokenList = new FastList<Token>();
        tokenList.add(leftToken);
        tokenList.add(rightToken);
        TokenSpan span = new TokenSpan(leftToken.getStartOffset(),
                rightToken.getEndOffset(), tokenList, 0);
        phr.span(span).text(
                fullText.substring(span.getStartOffset(), span.getEndOffset()));
        return phr;
    }

    private double computeSCP(Token leftToken, Token rightToken) {
        String leftText = leftToken.getText();
        String rightText = rightToken.getText();
        String comText = leftText + " " + rightText;
        return getLogCount(comText, 2) * 2 - getLogCount(leftText, 1)
                - getLogCount(rightText, 1);
    }

    private double computeSCP(ProbScorePhrase leftPhr, ProbScorePhrase rightPhr, ProbScorePhrase mergedPhr) {
        String leftNgram = TokenUtil.tokens2Str(leftPhr.span()
                .getTokens());
        String rightNgram = TokenUtil.tokens2Str(rightPhr.span()
                .getTokens());
        String mergedNgram = TokenUtil.tokens2Str(mergedPhr.span()
                .getTokens());
        double leftPr = getLogCount(leftNgram, leftPhr.span().getTokens()
                .size());
        double rightPr = getLogCount(rightNgram, rightPhr.span().getTokens()
                .size());
        double mergedPr = getLogCount(mergedNgram, mergedPhr.span().getTokens()
                .size());

        return mergedPr * 2 - leftPr - rightPr;
    }

    private double getLogCount(String tokenString, int size) {
        if (size > countMap.getMaxOrder())
            return -50;
        Long count = countMap.getCount(tokenString.split(" "));
        return Math.log10(count - 1 + smoothing);
    }

    public static class ThresholdScoreComparator<T extends ProbScorePhrase> implements
            Comparator<T> {
        final HashSet<String> breakPOS;

        public ThresholdScoreComparator(HashSet<String> breakPOS) {
            this.breakPOS = breakPOS;
        }

        @Override
        public int compare(T o1, T o2) {
            Double score1 = thresholdScore(o1, breakPOS);
            Double score2 = thresholdScore(o2, breakPOS);
            if (!score1.equals(score2)) {
                return score2.compareTo(score1);
            } else {
                Integer o1L = o1.getSpan().getTokens().size();
                Integer o2L = o2.getSpan().getTokens().size();
                return o1L.compareTo(o2L);
            }
        }

    }

    public static class OffsetComparator<T extends ProbScorePhrase> implements
            Comparator<T> {
        @Override
        public int compare(T o1, T o2) {
            Integer score1 = o1.span().getStartOffset();
            Integer score2 = o2.span().getStartOffset();
            return score1.compareTo(score2);
        }

    }

}
