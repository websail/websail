package edu.northwestern.websail.kpe.kpfe;

import edu.northwestern.websail.classify.data.CommonNominal;
import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 11/19/14
 */
@PipelineConfigInfo(
        requiredResources = {PEVResourceType.KPE_DICT}
)
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class}
)
public class DictionaryKPFE extends KPFeatureExtractor {

    public static final Feature patternDictF = new Feature("dict", "patterns",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature prefixPatternDictF = new Feature("dict", "prefixPatterns",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature mentionDictF = new Feature("dict", "mentions",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature anyDictF = new Feature("dict", "any",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    private static final Feature[] features = {patternDictF, prefixPatternDictF, mentionDictF, anyDictF};
    private static boolean registered = false;
    HashMap<String, Integer> dict;

    public DictionaryKPFE() {
        curValues = new double[features.length];
        Arrays.fill(curValues, Instance.MISSING);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config)) return false;
        this.dict = (HashMap<String, Integer>) env.getResource(PEVResourceType.KPE_DICT);
        if (this.dict == null) {
            PipelineEnvironment.logResourceNotFound(PEVResourceType.KPE_DICT, logger);
        }
        return true;
    }

    @Override
    protected void extract(Instance<Phrase> phraseInstance) throws Exception {
        String searchKey = TokenUtil.tokens2Str(phraseInstance.getSource().getSpan().getTokens());
        Integer v = dict.get(searchKey);
        if (v == null) v = 0;
        boolean existsInPatternDict = (v & 1) == 1;
        boolean patternDictPrefixMatch = false;
        for (Map.Entry<String, Integer> dictPhrase : dict.entrySet()) {
            if ((dictPhrase.getValue() & 1) != 1) continue;
            String phrase = dictPhrase.getKey();
            if (phrase.startsWith(searchKey)) {
                patternDictPrefixMatch = true;
                break;
            }
        }
        boolean existsInMentionDict = (v & 2) == 2;
        curValues[0] = existsInPatternDict ? CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
        curValues[1] = patternDictPrefixMatch ? CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
        curValues[2] = existsInMentionDict ? CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
        curValues[3] = existsInPatternDict || patternDictPrefixMatch || existsInMentionDict
                ? CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
    }

    @Override
    protected Feature[] features() {
        return features;
    }

    @Override
    protected boolean initForBuck(PipelineEnvironment env, S2PaperBuck prev) {
        return true;
    }

    @Override
    protected boolean initForUnit(BuckUnit unit) {
        return true;
    }

    @Override
    protected boolean register() {
        return registered;
    }

    @Override
    protected void registered() {
        registered = true;
    }
}
