package edu.northwestern.websail.kpe.out.dedup;

import edu.northwestern.websail.kpe.data.kp.KPGroup;
import edu.northwestern.websail.kpe.data.kp.KPUtil;
import edu.northwestern.websail.kpe.data.kp.KeyPhrase;
import edu.northwestern.websail.kpe.data.kp.ScoreKeyPhrase;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

/**
 * @author NorThanapon
 * @since 11/20/14
 */
public class KPDedupper {
    private static final KPUtil.StringLengthComparator lenComparator = new KPUtil.StringLengthComparator();
    private static final KPUtil.GroupLengthComparator gLenComparator = new KPUtil.GroupLengthComparator();
    private static final KPUtil.GroupRepLengthComparator gRepLenComparator = new KPUtil.GroupRepLengthComparator();
    private static final KPUtil.GroupSearchScoreComparator gSSComparator = new KPUtil.GroupSearchScoreComparator();

    private final KPDedupProcessor processor;
    private final KPDedupMetric metric;

    public KPDedupper(DirectoryReader reader, HashMap<Integer, String> indexDocIDMap) {
        this.metric = new KPDedupMetric(reader, indexDocIDMap);
        this.processor = new KPDedupProcessor(metric);
    }

    public ArrayList<KPGroup> group(String source, ArrayList<ScoreKeyPhrase> kps) throws IOException, ParseException {
        ArrayList<ScoreKeyPhrase> data = new ArrayList<ScoreKeyPhrase>(kps);
        data = KPUtil.removeDuplicateSurfaceKP(data);
        Collections.sort(data, lenComparator);
        HashMap<ScoreKeyPhrase, KPGroup> group = initGroup(source, data);
        populateOutput(group, data);
        ArrayList<KPGroup> outSets = mergeDuplicateKPGroups(group);
        outSets = mergeRepresentativeKPGroup(outSets);
        rankKPGroup(outSets);
        return outSets;
    }

    private HashMap<ScoreKeyPhrase, KPGroup> initGroup(String source, ArrayList<ScoreKeyPhrase> data) throws IOException, ParseException {
        HashMap<ScoreKeyPhrase, KPGroup> group = new HashMap<ScoreKeyPhrase, KPGroup>();
        for (ScoreKeyPhrase key : data) {
            KPGroup set = new KPGroup(source);
            set.add(key);
            group.put(key, set);
            key.setSearchScore(Math.log10(key.getTokens().size() + 1)
                    * metric.searchScore(key.bowExact(), key.getSource()));
        }
        return group;
    }

    private void populateOutput(HashMap<ScoreKeyPhrase, KPGroup> group, ArrayList<ScoreKeyPhrase> data) throws IOException, ParseException {
        // check group for all pairs
        for (int i = 0; i < data.size(); i++) {
            ScoreKeyPhrase mainKP = data.get(i);
            for (int j = 0; j < data.size(); j++) {
                if (i == j)
                    continue;
                ScoreKeyPhrase newKP = data.get(j);
                if (!processor.sameGroup(mainKP, newKP)) {
                    continue;
                }
                group.get(mainKP).add(newKP);
            }
        }
    }

    private ArrayList<KPGroup> mergeDuplicateKPGroups(HashMap<ScoreKeyPhrase, KPGroup> group) {
        // eliminate overlapping groups
        ArrayList<KPGroup> outSets = new ArrayList<KPGroup>();
        ArrayList<KPGroup> groups = new ArrayList<KPGroup>(
                group.values());
        Collections.sort(groups, gLenComparator);
        for (KPGroup kg : groups) {
            boolean c = false;
            for (KPGroup sg : outSets) {
                if (sg.getKps().containsAll(kg.getKps())) {
                    c = true;
                    break;
                }
            }
            if (!c) {
                outSets.add(kg);
            }
        }
        return outSets;
    }

    private ArrayList<KPGroup> mergeRepresentativeKPGroup(ArrayList<KPGroup> outSets) throws IOException, ParseException {
        // merge groups using its representative
        Collections.sort(outSets, gRepLenComparator);
        HashSet<Integer> mergedIndexes = new HashSet<Integer>();
        ArrayList<KPGroup> mergedGroups = new ArrayList<KPGroup>();
        for (int i = 0; i < outSets.size(); i++) {
            if (mergedIndexes.contains(i))
                continue;
            mergedIndexes.add(i);
            ScoreKeyPhrase curRep = outSets.get(i).getRepresentative();
            KPGroup curGroup = new KPGroup(outSets.get(i)
                    .getSource());
            mergedGroups.add(curGroup);
            curGroup.addAll(outSets.get(i));
            for (int j = 0; j < outSets.size(); j++) {
                if (mergedIndexes.contains(j))
                    continue;
                KeyPhrase nRep = outSets.get(j).getLongestKP();
                if (processor.sameGroup(curRep, nRep)) {
                    curGroup.addAll(outSets.get(j));
                    mergedIndexes.add(j);
                }
            }
        }
        return mergedGroups;
    }

    private void rankKPGroup(ArrayList<KPGroup> outSets) throws IOException, ParseException {
        for (KPGroup kg : outSets) {
            kg.setSearchScore(Math.log(kg.bowExact().size() + 1)
                    * metric.searchScore(kg.bowExact(), kg.getSource()));
        }
        Collections.sort(outSets, gSSComparator);

    }
}
