package edu.northwestern.websail.kpe.kpfe.postprocessing;

import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.classify.result.RankingResult;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;

import java.util.ArrayList;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 4/7/15
 */
@PipelineConfigInfo()
@PipelineBUTInfo(
        consumes = {ClassifyBUT.RankingResults.class},
        produces = {ClassifyBUT.RankingResults.class}
)
public class RemoveLowRankInstances implements PipelineExecutable {
    public final int MAX_ORDER = 50;
    private int threadId = 0;

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        for (BuckUnit unit : prev.units()) {
            RankingResult result = unit.get(ClassifyBUT.RankingResults.class);
            if (result == null) {
                continue;
            }
            int newSize = MAX_ORDER;
            if (result.getOrders().length < MAX_ORDER) {
                newSize = result.getOrders().length;
            }
            RankingResult filteredResult = new RankingResult(result.getInstances(), new Integer[newSize], new Double[newSize]);
            ArrayList<Instance> filteredInstance = new ArrayList<Instance>();
            for (int order = 0; order < result.getOrders().length; order++) {
                if (order >= MAX_ORDER) {
                    break;
                }
                int index = result.getOrders()[order];
                filteredInstance.add(result.getInstances().getInstances().get(index));
                filteredResult.getOrders()[order] = order;
                filteredResult.getScores()[order] = result.getScores()[index];
            }
            filteredResult.getInstances().setInstances(filteredInstance);
            unit.set(ClassifyBUT.RankingResults.class, filteredResult);
        }
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public int getThreadId() {
        return threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }
}
