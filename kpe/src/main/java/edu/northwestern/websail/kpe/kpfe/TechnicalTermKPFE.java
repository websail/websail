package edu.northwestern.websail.kpe.kpfe;

import edu.northwestern.websail.classify.data.CommonNominal;
import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.kpe.pe.helper.TechnicalTerms;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author NorThanapon
 * @since 11/19/14
 */
@PipelineConfigInfo()
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class}
)
public class TechnicalTermKPFE extends KPFeatureExtractor {

    public static final Feature simpleTTF = new Feature("syntactic", "isSimpleTechnicalTerm",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature complexTTF = new Feature("syntactic", "isComplexTechnicalTerm",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    private static final Feature[] features = {simpleTTF, complexTTF};
    private static boolean registered = false;

    public TechnicalTermKPFE() {
        curValues = new double[features.length];
        Arrays.fill(curValues, Instance.MISSING);
    }

    @Override
    protected void extract(Instance<Phrase> phraseInstance) throws Exception {
        String posStr = TokenUtil.token2POSStr(phraseInstance.getSource().getSpan().getTokens());
        curValues[0] = matchTTPattern(TechnicalTerms.simple, posStr) ?
                CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
        curValues[1] = matchTTPattern(TechnicalTerms.complex, posStr) ?
                CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
    }

    @Override
    protected Feature[] features() {
        return features;
    }

    @Override
    protected boolean initForBuck(PipelineEnvironment env, S2PaperBuck prev) {
        return true;
    }

    @Override
    protected boolean initForUnit(BuckUnit unit) {
        return true;
    }

    private boolean matchTTPattern(Pattern p, String tokenPOSString) {
        Matcher simpleMatcher = p.matcher(tokenPOSString);
        return simpleMatcher.find()
                && simpleMatcher.start() == 0
                && simpleMatcher.end() == tokenPOSString.length();
    }

    @Override
    protected boolean register() {
        return registered;
    }

    @Override
    protected void registered() {
        registered = true;
    }
}
