package edu.northwestern.websail.kpe.kpfe.postprocessing;

import edu.northwestern.websail.classify.data.CommonNominal;
import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.ds.indexer.Indexer;
import edu.northwestern.websail.kpe.data.kp.KPUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 3/27/15
 * <p/>
 * Only use after feature extraction units and before classification/ranking units
 * <p/>
 * Be advised the side effects of this pipeline unit is huge.
 * It will discard every buck unit, and make a new Instances for a new unit.
 */
@PipelineConfigInfo()
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class},
        produces = {ClassifyBUT.BuckInstances.class}
)
public class MergeDuplicateInstances implements PipelineExecutable {
    private int threadId = 0;

    public MergeDuplicateInstances() {
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        Instances buckInstances = mergeUnitInstances(prev);
        //System.out.println("Merged Unit Instances: "+buckInstances.getInstances().size());
        HashMap<String, ArrayList<Instance>> instMap = collectInstances(buckInstances);
        //System.out.println("Group Instances: " + instMap.size());
        ArrayList<Instance> mergedInstances = mergeInstances(instMap, buckInstances.getFeatures());
        //System.out.println("Merge Duplicate Instances: " + mergedInstances.size());
        buckInstances.setInstances(mergedInstances);
        ArrayList<BuckUnit> units = new ArrayList<BuckUnit>();
        for (BuckUnit unit : prev.units()) {
            units.add(unit);
        }
        for (BuckUnit unit : units) {
            prev.removeUnit(unit);
        }
        BuckUnit newUnit = new BuckUnit();
        newUnit.set(ClassifyBUT.BuckInstances.class, buckInstances);
        prev.addUnit(newUnit);
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public int getThreadId() {
        return threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }

    private Instances mergeUnitInstances(Buck buck) {
        Instances instances = null;
        for (BuckUnit unit : buck.units()) {
            Instances unitInstances = unit.get(ClassifyBUT.BuckInstances.class);
            if (instances == null) instances = unitInstances;
            else {
                instances.merge(unitInstances);
            }
        }
        return instances;
    }

    private HashMap<String, ArrayList<Instance>> collectInstances(Instances iSet) {
        HashMap<String, ArrayList<Instance>> instMap = new HashMap<String, ArrayList<Instance>>();
        if (iSet == null) return instMap;
        for (Instance instance : iSet.getInstances()) {
            String norm = KPUtil.instance2NormalizedSurface(instance);
            if (!instMap.containsKey(norm)) {
                instMap.put(norm, new ArrayList<Instance>());
            }
            instMap.get(norm).add(instance);
        }
        return instMap;
    }

    private ArrayList<Instance> mergeInstances(HashMap<String, ArrayList<Instance>> instMap, Indexer<Feature> featureIndexer) {
        ArrayList<Instance> newInstances = new ArrayList<Instance>();
        for (ArrayList<Instance> instances : instMap.values()) {
            newInstances.add(mergeInstances(instances, featureIndexer));
        }
        return newInstances;
    }

    private Instance mergeInstances(ArrayList<Instance> instances, Indexer<Feature> featureIndexer) {
        Instance main = null;
        HashMap<Feature, Integer> featureCountMap = new HashMap<Feature, Integer>();
        for (Instance inst : instances) {
            if (main == null) main = inst;
            else {
                collectInstanceFeatures(main, inst, featureIndexer, featureCountMap);
            }
        }
        finalizeInstanceFeatures(main, featureCountMap);
        return main;
    }

    private void collectInstanceFeatures(Instance main, Instance inst, Indexer<Feature> featureIndexer, HashMap<Feature, Integer> featureCountMap) {
        for (Feature f : featureIndexer.getObjects()) {
            double vInst = inst.getValue(f);
            double vMain = main.getValue(f);
            main.setValue(f, vMain + vInst);
            if (featureCountMap.containsKey(f)) {
                featureCountMap.put(f, featureCountMap.get(f) + 1);
            } else {
                featureCountMap.put(f, 2);
            }
        }
    }

    private void finalizeInstanceFeatures(Instance main, HashMap<Feature, Integer> featureCountMap) {
        for (Map.Entry<Feature, Integer> p : featureCountMap.entrySet()) {
            Feature f = p.getKey();
            Integer count = p.getValue();
            double v = main.getValue(f);
            if (f.getType() == Feature.Type.NOMINAL && f.getNominalMap() == CommonNominal.boolMap) {
                // OR
                main.setValue(f, v > CommonNominal.boolMapFalseValue ? CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue);
            } else if (f.getType() == Feature.Type.NUMBER) {
                // avrage
                main.setValue(f, v / count);
            }
        }
    }

}
