package edu.northwestern.websail.kpe.main.preprocessing;

import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.InstanceList;
import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.io.InputFileManager;
import edu.northwestern.websail.core.util.TimerUtil;
import edu.northwestern.websail.kpe.data.ACLPaperAdapter;
import edu.northwestern.websail.kpe.data.paper.Paper;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.kpe.pe.NounPhrasePE;
import edu.northwestern.websail.text.chunker.StanfordChunker;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.tokenizer.SUPTBTokenizer;
import edu.northwestern.websail.text.tokenizer.StanfordNLPSSplit;
import edu.northwestern.websail.text.topics.MalletHelper;
import org.apache.commons.cli.*;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 * @author NorThanapon
 * @since 7/20/15
 */
public class TrainTopicModel {
    static final int PHRASE_MAX_NUM_TOKENS = 5;
    static final FastList<String> paperIdList = new FastList<String>();
    static String docDataDir;
    static String dataFile;
    static String citationFile;
    static String ldaFile;
    static String topicFile;
    static boolean addPhrases;
    static Integer nThreads = 2;
    static Integer numIters = 1500;
    static FastList<Pipe> pipes;
    public static void main(String[] args) throws Exception {

        TimerUtil timer = new TimerUtil();
        timer.start();
        if (!processOptions(args)) {
            return;
        }
        InstanceList instances;
        if (docDataDir != null) {
            instances = instancesFromDocData();
        } else {
            instances = instancesFromPapers();
        }
        if (instances != null) {
            System.out.println("Papers: " + instances.size());
            ParallelTopicModel.logger.setLevel(Level.INFO);
            ParallelTopicModel model = new ParallelTopicModel(100, 1.0, 0.01);
            model.setNumThreads(nThreads);
            model.setNumIterations(numIters);
            model.addInstances(instances);
            System.out.println("Training model...");
            model.estimate();
            System.out.println("Saving model...");
            model.write(new File(ldaFile));
            System.out.println("Saving inferencer...");
            MalletHelper.saveTopicInferencer(model.getInferencer(), pipes, ldaFile + ".inferencer");
            System.out.println("Saving state...");
            model.printState(new File(ldaFile + ".state.gz"));
            System.out.println("Saving topic distribution of the training documents...");
            MalletHelper.saveTopicDistributions(model, topicFile, paperIdList);
        }
        timer.end();
        System.out.println("Done " + timer.totalTime());
    }

    public static InstanceList instancesFromDocData() {
        pipes = MalletHelper.makeDataReadyPipeList();
        InstanceList instances = new InstanceList(new SerialPipes(pipes));
        File dir = new File(docDataDir + "/tokens");
        File[] parts = dir.listFiles();
        if (parts == null) {
            System.out.println(docDataDir + "/tokens is empty.");
            return null;
        }
        for (File part : parts) {
            if (!part.getName().startsWith("part")) {
                continue;
            }
            System.out.println(part.getName());
            InputFileManager posInMgr = new InputFileManager(docDataDir + "/pos/" + part.getName());
            InputFileManager tokInMgr = new InputFileManager(docDataDir + "/tokens/" + part.getName());
            InputFileManager spnInMgr = new InputFileManager(docDataDir + "/spans/" + part.getName());
            int currentLine = 0;
            String posLine = posInMgr.readLine();
            String[] posParts = posLine.split("\t");
            String curPaperId = posParts[0];
            String nextPaperId;
            int nextIdLine;
            while ((posLine = posInMgr.readLine()) != null) {
                posParts = posLine.split("\t");
                nextPaperId = posParts[0];
                nextIdLine = Integer.parseInt(posParts[1]);
                StringBuilder paper = new StringBuilder();
                do {
                    String tokenLine = tokInMgr.readLine();
                    String spanLine = spnInMgr.readLine();
                    appendTokenLine(paper, tokenLine, spanLine);
                    currentLine++;
                } while (currentLine < nextIdLine);
                instances.addThruPipe(MalletHelper.makeInstance(paper.toString(), curPaperId));
                paperIdList.add(curPaperId);
                curPaperId = nextPaperId;
            }
            //last doc
            StringBuilder paper = new StringBuilder();
            String tokenLine;
            while ((tokenLine = tokInMgr.readLine()) != null) {
                String spanLine = spnInMgr.readLine();
                appendTokenLine(paper, tokenLine, spanLine);
            }
            instances.addThruPipe(MalletHelper.makeInstance(paper.toString(), curPaperId));
            paperIdList.add(curPaperId);
        }
        return instances;
    }

    private static void appendTokenLine(StringBuilder sb, String tokenLine, String spanLine) {
        sb.append(tokenLine).append('\n');
        if (spanLine.trim().length() == 0) return;
        String[] tokens = tokenLine.split(" ");
        String[] spans = spanLine.split("\t");
        for (String span : spans) {
            String[] parts = span.split(",");
            int start = Integer.parseInt(parts[0]);
            int end = Integer.parseInt(parts[1]);
            for (int i = start; i < end; i++) {
                sb.append(tokens[i]).append('_');
            }
            sb.setLength(sb.length() - 1);
            sb.append(' ');
        }
        sb.append('\n');
    }

    private static InstanceList instancesFromPapers() throws Exception {
        //XXX: Hack tokenizer and sentence spliter type
        SUPTBTokenizer tokenizer = new SUPTBTokenizer();
        NounPhrasePE pe = null;
        if (addPhrases) {
            StanfordChunker ssa = new StanfordChunker();
            StanfordNLPSSplit nlpsSplit = new StanfordNLPSSplit(true);
            pe = new NounPhrasePE(ssa, nlpsSplit);
        }

        System.out.println("Loading papers...");
        ACLPaperAdapter adapter;
        if (citationFile != null) {
            adapter = new ACLPaperAdapter(dataFile, tokenizer, true,
                    citationFile, true);
        } else {
            adapter = new ACLPaperAdapter(dataFile, tokenizer, true, true);
        }
        System.out.println("Building instances...");
        pipes = MalletHelper.makeBasicPipeList();
        InstanceList instances = new InstanceList(new SerialPipes(pipes));
        //TODO: Parallelize this loop
        for (Map.Entry<String, Paper> p : adapter.getPapers().entrySet()) {
            Paper paper = p.getValue();
            StringBuilder sb = new StringBuilder();
            sb.append(paper.getTitle()).append("\n")
                    .append(paper.getPaperAbstract()).append("\n")
                    .append(paper.getBodyText()).append("\n")
                    .append(paper.getCitancesAsParagraph()).append("\n");
            if (addPhrases && pe != null) {
                FastList<Phrase> phrases = pe.extract(sb.toString(), paper.getId());
                for (Phrase phrase : phrases) {
                    List<? extends Token> tokens = phrase.getSpan().getTokens();
                    if (tokens.size() == 1 || tokens.size() > PHRASE_MAX_NUM_TOKENS) {
                        continue;
                    }
                    sb.append(' ');
                    for (Token token : tokens) {
                        sb.append(token.getText()).append('_');
                    }
                    sb.setLength(sb.length() - 1);
                }
            }
            instances.addThruPipe(MalletHelper.makeInstance(sb.toString(), paper.getId()));
            paperIdList.add(paper.getId());
        }
        adapter.close();
        return instances;
    }

    public static boolean processOptions(String[] args) {
        Options options = new Options();
        options.addOption("paper", true, "Paper file in json format");
        options.addOption("docData", true, "A directory containing doc data (See WriteDocumentData). This is an alternative option of paper");
        options.addOption("citation", true, "[Optional] Citation context file");
        options.addOption("numThreads", true, "[Optional] Number of threads. Default value: " + nThreads);
        options.addOption("numIters", true, "[Optional] Number of training iterations. Default value: " + numIters);
        options.addOption("ldaModel", true, "Output LDA model file");
        options.addOption("topics", true, "Output toptic distribution of the training documents.");
        options.addOption("addPhrases", false, "[Optional] Add phrases to the training documents.");
        CommandLineParser parser = new GnuParser();
        boolean argsNotOk = false;

        try {
            CommandLine cmd = parser.parse(options, args);
            docDataDir = cmd.getOptionValue("docData");
            dataFile = cmd.getOptionValue("paper");
            citationFile = cmd.getOptionValue("citation");
            ldaFile = cmd.getOptionValue("ldaModel");
            topicFile = cmd.getOptionValue("topics");
            addPhrases = cmd.hasOption("addPhrases");
            if (cmd.getOptionValue("numThreads") != null) {
                nThreads = Integer.parseInt(cmd.getOptionValue("numThreads"));
            }
            if (cmd.getOptionValue("numIters") != null) {
                numIters = Integer.parseInt(cmd.getOptionValue("numIters"));
            }
            if ((dataFile == null && docDataDir == null) || ldaFile == null) {
                argsNotOk = true;
            }
        } catch (ParseException e) {
            argsNotOk = true;
        }
        if (argsNotOk) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("BuildResources [options]", options);
        }
        return !argsNotOk;
    }
}
