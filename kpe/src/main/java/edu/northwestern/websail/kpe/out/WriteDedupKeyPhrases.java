package edu.northwestern.websail.kpe.out;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import edu.northwestern.websail.classify.data.CommonNominal;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.classify.result.ClassificationResult;
import edu.northwestern.websail.core.io.OutputFileManager;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.core.pipeline.unit.common.WriteAndConcatFileUnit;
import edu.northwestern.websail.kpe.data.kp.KPGroup;
import edu.northwestern.websail.kpe.data.kp.KPUtil;
import edu.northwestern.websail.kpe.data.kp.ScoreKeyPhrase;
import edu.northwestern.websail.kpe.out.dedup.KPDedupper;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 11/22/14
 */
@PipelineConfigInfo(
        requiredSettings = {"kpe.out.dedupKeyphraseFile=*, kpe.index.directory=*, kpe.index.idMapFile=*"}
)
public class WriteDedupKeyPhrases extends WriteAndConcatFileUnit {
    private DirectoryReader reader;
    private KPDedupper dedupper;

    public WriteDedupKeyPhrases() {
    }


    @SuppressWarnings("unchecked")
    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config)) return false;
        String indexDirectory = config.getProperty("kpe.index.directory");
        if (indexDirectory == null) {
            logger.severe("'kpe.index.directory' is not set.");
            return false;
        }
        try {
            File indexFile = new File(indexDirectory);
            Directory directory = FSDirectory.open(indexFile);
            reader = DirectoryReader.open(directory);
        } catch (IOException e) {
            logger.severe("Cannot open index directory: " + indexDirectory);
            e.printStackTrace();
            return false;
        }
        String indexMapFile = config.getProperty("kpe.index.idMapFile");
        if (indexMapFile == null) {
            logger.severe("'kpe.index.idMapFile' is not set.");
            return false;
        }
        HashMap<Integer, String> map;
        try {
            ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(
                    new FileInputStream(indexMapFile)));
            Kryo kryo = new Kryo();
            Input input = new Input(ois);
            map = kryo.readObject(input, HashMap.class);
            input.close();
            ois.close();
        } catch (FileNotFoundException e) {
            logger.severe(indexMapFile + " does not exist.");
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            logger.severe("Unexpected error while loading " + indexMapFile);
            e.printStackTrace();
            return false;
        }
        if (map == null) {
            logger.severe("Index map is null.");
            return false;
        }
        dedupper = new KPDedupper(reader, map);
        return true;
    }

    public boolean close(PipelineEnvironment env, Properties config) {
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return super.close(env, config);
    }

    @Override
    protected String outFileConfigKey() {
        return "kpe.out.dedupKeyphraseFile";
    }


    @Override
    protected boolean write(PipelineEnvironment env, Buck prev, OutputFileManager out) {
        String source = prev.getSource();
        ArrayList<ScoreKeyPhrase> keyPhrases = new ArrayList<ScoreKeyPhrase>();
        for (BuckUnit unit : prev.units()) {
            ArrayList<ClassificationResult> results = unit.get(ClassifyBUT.ClassifyResults.class);
            for (ClassificationResult result : results) {
                if (result.getClassValue() == CommonNominal.boolMapTrueValue) {
                    keyPhrases.add(KPUtil.instance2ScoreKeyPhrase(result.getInstance()));
                }
            }
        }
        try {
            ArrayList<KPGroup> groups = dedupper.group(source, keyPhrases);
            if (groups.size() > 0) {
                out.print(source + "\t");
                for (int i = 0; i < groups.size() - 1; i++) {
                    KPGroup group = groups.get(i);
                    if (group.getKps().size() == 0) continue;
                    out.print(group.getRepresentative().getPhrase().replaceAll("  ", " ") + "\t");
                }
                out.println(groups.get(groups.size() - 1).getRepresentative().getPhrase().replaceAll("  ", " "));
            }
        } catch (Exception e) {
            logger.severe("Unexpected error occur during grouping key phrases");
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
