package edu.northwestern.websail.kpe.main.postprocessing;

import edu.northwestern.websail.core.io.InputFileManager;
import edu.northwestern.websail.core.util.TimerUtil;
import edu.northwestern.websail.kpe.data.kp.KeyPhrase;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.tokenizer.StanfordNLPTokenizer;
import edu.northwestern.websail.text.tokenizer.Tokenizer;
import edu.northwestern.websail.text.util.TokenUtil;
import org.tartarus.snowball.ext.PorterStemmer;

import java.io.IOException;
import java.util.*;

/**
 * @author NorThanapon
 * @since 1/30/15
 */

@SuppressWarnings("ALL")
public class AnalyzeRankSemEval {
    public static void main(String[] args) throws IOException {
        TimerUtil timer = new TimerUtil();
        timer.start();
        String dataFile = "/Users/NorThanapon/Documents/current/semeval/semeval_ranked_key_phrases.txt";
        //String outputFile = null;
        String answerFile = "/Users/NorThanapon/Documents/current/semeval/test.combined.stem.final";
//        Options options = new Options();
//        options.addOption("in", true, "Input key file in AI2 format (TSV)");
//        options.addOption("out", true, "Output key file in SemEval format (CSV)");
//        options.addOption("ids", true, "[Optional] SemEval answer file used to remove keys from other papers");
//
//        CommandLineParser parser = new GnuParser();
//        boolean argsNotOk = false;
//        try {
//            CommandLine cmd = parser.parse(options, args);
//            dataFile = cmd.getOptionValue("in");
//            answerFile = cmd.getOptionValue("ids");
//            outputFile = cmd.getOptionValue("out");
//            if (dataFile == null || outputFile == null) {
//                argsNotOk = true;
//            }
//        } catch (ParseException e) {
//            argsNotOk = true;
//        }
//        if (argsNotOk) {
//            HelpFormatter formatter = new HelpFormatter();
//            formatter.printHelp("WritePaperTokens [options]", options);
//            System.exit(0);
//        }
        String line;
        StanfordNLPTokenizer tokenizer = new StanfordNLPTokenizer();
        HashMap<String, ArrayList<KeyPhrase>> keys = readRankResult(dataFile, tokenizer, Integer.MAX_VALUE);
        InputFileManager inMgr = new InputFileManager(answerFile);

        int existBelow = 0;
        int totalLabels = 0;
        int totalOutput = 0;
        int outputMatch = 0;
        while ((line = inMgr.readLine()) != null) {
            if (line.trim().equals("")) continue;
            String id = line.split(" : ")[0];
            ArrayList<KeyPhrase> keyphrases = keys.get(id);
            HashSet<String> labelSet = new HashSet<String>(Arrays.asList(line.split(" : ")[1].split(",|\\+")));
            totalLabels += labelSet.size();
            HashSet<String> notMatchTopLabels = new HashSet<String>(labelSet);
            HashSet<String> notMatchLabels = new HashSet<String>(labelSet);
            ArrayList<String> matchBelow = new ArrayList<String>();
            StringBuilder sb = new StringBuilder();
            sb.append(id).append("\t");
            StringBuilder iSB = new StringBuilder();
            iSB.append(id).append("\t");
            int count = 0;
            if (keyphrases != null) {
                for (KeyPhrase kp : keyphrases) {
                    sb.append(kp.getNormalizedPhrase()).append('\t');
                    if (!labelSet.contains(kp.getNormalizedPhrase())) {
                        iSB.append(kp.getNormalizedPhrase()).append('\t');
                    } else {
                        outputMatch++;
                    }
                    totalOutput++;
                    notMatchTopLabels.remove(kp.getNormalizedPhrase());
                    count++;
                    //if (count > pos * keyphrases.size()) break;
                    if (count >= 15) break;
                }
            }
            count = 0;
            if (keyphrases != null) {
                for (KeyPhrase kp : keyphrases) {
                    if (count >= 15 && notMatchLabels.contains(kp.getNormalizedPhrase())) {
                        existBelow++;
                        matchBelow.add(kp.getNormalizedPhrase());
                    }
                    notMatchLabels.remove(kp.getNormalizedPhrase());
                    count++;

                }
            }

            StringBuilder lSB = new StringBuilder();
            lSB.append(id).append("\t");
            for (String label : labelSet) {
                lSB.append(label).append('\t');
            }

            StringBuilder nTLSB = new StringBuilder();
            nTLSB.append(id).append("\t");
            for (String label : notMatchTopLabels) {
                nTLSB.append(label).append('\t');
            }


            StringBuilder nLSB = new StringBuilder();
            nLSB.append(id).append("\t");
            for (String label : notMatchLabels) {
                nLSB.append(label).append('\t');
            }

            StringBuilder mbSB = new StringBuilder();
            mbSB.append(id).append("\t");
            for (String label : matchBelow) {
                mbSB.append(label).append('\t');
            }

//            System.out.println(lSB.substring(0, lSB.length() - 1));
//            System.out.println(sb.substring(0, sb.length() - 1));
//            System.out.println();
//            System.out.println(nTLSB.substring(0, nTLSB.length() - 1));
//            System.out.println(nLSB.substring(0, nLSB.length() - 1));
//            System.out.println(iSB.substring(0, iSB.length() - 1));
            System.out.println(mbSB.substring(0, mbSB.length() - 1));

            System.out.println();
//            System.out.println();
        }
        inMgr.close();
        System.out.println("Total labeled KPs: " + totalLabels);
        System.out.println("Total output KPs: " + totalOutput);
        System.out.println("KPs exist above top 15: " + outputMatch);
        System.out.println("KPs exist below top 15: " + existBelow);

    }

    private static HashMap<String, ArrayList<KeyPhrase>> readRankResult(String file, Tokenizer tokenizer, int top) throws IOException {
        HashMap<String, ArrayList<KeyPhrase>> results = new HashMap<String, ArrayList<KeyPhrase>>();
        InputFileManager inMgr = new InputFileManager(file);
        String line;
        while ((line = inMgr.readLine()) != null) {
            String[] parts = line.split("\t");
            String paperId = parts[0];

            if (!results.containsKey(paperId)) {
                results.put(paperId, new ArrayList<KeyPhrase>());
            } else if (results.get(paperId).size() > top) {
                continue;
            }
            String kpString = parts[2];//.replaceAll("-" , " ");
            tokenizer.initialize(kpString);
            List<Token> tokens = tokenizer.getAllTokens();
            tokens = TokenUtil.normalize(tokens, true, true, new PorterStemmer());
            KeyPhrase kp = new KeyPhrase(paperId, kpString, tokens);
            results.get(paperId).add(kp);
        }
        inMgr.close();
        return results;
    }
}
