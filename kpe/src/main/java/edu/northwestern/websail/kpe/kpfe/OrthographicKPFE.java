package edu.northwestern.websail.kpe.kpfe;

import edu.northwestern.websail.classify.data.CommonNominal;
import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.buck.BuckUnitTypes;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.kpe.pe.helper.AcronymHelper;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.tokenizer.Tokenizer;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author NorThanapon
 * @since 11/19/14
 */
@PipelineConfigInfo(
        requiredResources = {PEVResourceType.TOKENIZER_POOL}
)
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class}
)
public class OrthographicKPFE extends KPFeatureExtractor {

    public static final Feature numToken1F = new Feature("ortho", "numToken1",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature numToken2F = new Feature("ortho", "numToken2",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature numToken3F = new Feature("ortho", "numToken3",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature numToken4F = new Feature("ortho", "numToken4",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature numToken5F = new Feature("ortho", "numToken5",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature diffCaseF = new Feature("ortho", "diffCases",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature abbrF = new Feature("ortho", "abbr",
            Feature.Type.NUMBER, CommonNominal.boolMap);
    public static final Feature allCapWords = new Feature("ortho", "allCapWords",
            Feature.Type.NUMBER, CommonNominal.boolMap);
    private static final Feature[] features = {numToken1F, numToken2F, numToken3F,
            numToken4F, numToken5F, diffCaseF, abbrF, allCapWords};
    private static boolean registered = false;
    private int currentIndex;
    private Tokenizer tokenizer;
    private ArrayList<String> fullText;
    private HashMap<String, Double> otherCaseCache;

    public OrthographicKPFE() {
        curValues = new double[features.length];
        Arrays.fill(curValues, Instance.MISSING);
        currentIndex = -1;
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config)) return false;
        this.tokenizer = (Tokenizer) env.getPoolResource(
                PEVResourceType.TOKENIZER_POOL, this.threadId,
                PEVResourceType.TOKENIZER);
        if (tokenizer == null) {
            PipelineEnvironment.logResourceNotFound(PEVResourceType.TOKENIZER_POOL, logger);
            return false;
        }
        return true;
    }

    @Override
    protected void extract(Instance<Phrase> phraseInstance) throws Exception {
        String surface = phraseInstance.getSource().getSurfaceForm();
        tokenizer.initialize(surface);
        List<Token> tokens = tokenizer.getAllTokens();
        int size = tokens.size();
        for (int i = 0; i < 5; i++) {
            curValues[i] = size == (i + 1) ? CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
        }
        curValues[5] = findOtherCaseFolding(tokens, surface);
        curValues[6] = AcronymHelper.isAbbr(phraseInstance.getSource().getSurfaceForm()) &&
                phraseInstance.getSource().getSurfaceForm().length() <= 5 ?
                CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
        curValues[7] = AcronymHelper.isAllCap(phraseInstance.getSource().getSurfaceForm()) ?
                CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
    }

    private double findOtherCaseFolding(List<Token> tokens, String surface) {
        String pattern = TokenUtil.tokens2Regex(tokens,
                surface, true);
        String key = pattern.toLowerCase();
        if (this.otherCaseCache.containsKey(key)) return this.otherCaseCache.get(key);
        Pattern p = Pattern.compile("(?i)" + pattern);
        Pattern pCase = Pattern.compile(pattern);
        for (int i = 0; i < fullText.size(); i++) {
            String text = fullText.get(i);
            Matcher matcher = p.matcher(text);
            while (matcher.find()) {
                if (matcher.start() != tokens.get(0).getStartOffset()
                        || i != currentIndex) {
                    String mPhrase = text.substring(matcher.start(),
                            matcher.end());
                    Matcher mCase = pCase.matcher(mPhrase);
                    if (!mCase.find()) {
                        this.otherCaseCache.put(key, CommonNominal.boolMapTrueValue);
                        return CommonNominal.boolMapTrueValue;
                    }
                }
            }
        }
        this.otherCaseCache.put(key, CommonNominal.boolMapFalseValue);
        return CommonNominal.boolMapFalseValue;
    }

    @Override
    protected Feature[] features() {
        return features;
    }

    @Override
    protected boolean initForBuck(PipelineEnvironment env, S2PaperBuck prev) {
        fullText = new ArrayList<String>();
        otherCaseCache = new HashMap<String, Double>();
        for (BuckUnit unit : prev.units()) {
            fullText.add(unit.get(BuckUnitTypes.TextBUT.class));
        }
        return true;
    }

    @Override
    protected boolean initForUnit(BuckUnit unit) {
        currentIndex++;
        return true;
    }

    @Override
    protected boolean register() {
        return registered;
    }

    @Override
    protected void registered() {
        registered = true;
    }
}
