package edu.northwestern.websail.kpe.pe.helper;

import edu.northwestern.websail.core.io.InputFileManager;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.tokenizer.StanfordNLPTokenizer;
import edu.northwestern.websail.text.tokenizer.Tokenizer;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.*;
import java.util.regex.Pattern;

/**
 * @author NorThanapon
 * @since 1/11/15
 */
public class AcronymHelper {
    public static final Comparator<StringIntegerPair> DESC_STRINGINTEGER =
            new Comparator<StringIntegerPair>() {
                @Override
                public int compare(StringIntegerPair o1, StringIntegerPair o2) {
                    return o1.getCount() - o2.getCount();
                }
            };
    public static final Pattern abbrP = Pattern.compile("^([A-Z])([A-Z\\.]+)");
    public static final Pattern allCapP = Pattern.compile("^([A-Z][a-z]*((\\s+of)?((\\s+|-?)[A-Z][a-z]*))*)");

    public static void main(String[] args) {
        String text = "Semantic Role Labeling ( SRL )";
        StanfordNLPTokenizer tokenizer = new StanfordNLPTokenizer();
        tokenizer.initialize(text);
        List<Token> tokens = tokenizer.getAllTokens();
        tokens = TokenUtil.normalize(tokens, true, true);
        System.out.println(tokens);
        String phrase = " Open SRL";
        tokenizer.initialize(phrase);
        List<Token> phrTokens = tokenizer.getAllTokens();
        phrTokens = TokenUtil.normalize(phrTokens, true, true);
        HashMap<String, String> map = buildAbbrMap(tokens, text, false);
        System.out.println(map);
        System.out.println(expand(map, phrTokens, "Open SRL", -phrTokens.get(0).getStartOffset()));
        System.out.println(isAllCap("Test A-Z"));

    }

    public static HashMap<String, ArrayList<StringIntegerPair>> loadAcronymMap(String filename, Tokenizer tokenizer) {
        HashMap<String, ArrayList<StringIntegerPair>> map = new HashMap<String, ArrayList<StringIntegerPair>>();
        InputFileManager inMgr = new InputFileManager(filename);
        String line;
        while ((line = inMgr.readLine()) != null) {
            String[] parts = line.split("\t");
            int count = Integer.parseInt(parts[0]);
            String full = parts[1].trim();
            String abbr = parts[2].trim();
            abbr = normalizeAbbr(abbr);
            tokenizer.initialize(full);
            List<Token> tokens = tokenizer.getAllTokens();
            tokens = TokenUtil.normalize(tokens, true, true);
            if (!map.containsKey(abbr)) {
                map.put(abbr, new ArrayList<StringIntegerPair>());
            }
            map.get(abbr).add(new StringIntegerPair(count, full, tokens));
        }
        inMgr.close();
        for (Map.Entry<String, ArrayList<StringIntegerPair>> p : map.entrySet()) {
            Collections.sort(p.getValue(), DESC_STRINGINTEGER);
        }
        return map;
    }

    public static String expand(HashMap<String, String> map, List<? extends Token> tokens, String text, int offset) {
        StringBuilder sb = new StringBuilder();
        for (Token t : tokens) {
            String str = text.substring(t.getStartOffset() + offset, t.getEndOffset() + offset);
            if (isAbbr(str)) {
                String abbr = normalizeAbbr(str);
                if (map.containsKey(abbr)) {
                    str = map.get(abbr);
                }
            }
            sb.append(str).append(' ');
        }
        return sb.substring(0, sb.length() - 1);
    }

    public static String expand(HashMap<String, ArrayList<StringIntegerPair>> map,
                                List<? extends Token> phrTokens, String surface, int offset,
                                List<? extends Token> allTokens) {
        StringBuilder sb = new StringBuilder();
        for (Token t : phrTokens) {
            String str = surface.substring(t.getStartOffset() + offset, t.getEndOffset() + offset);
            if (isAbbr(str)) {
                String abbr = normalizeAbbr(str);
                if (map.containsKey(abbr)) {
                    ArrayList<StringIntegerPair> candidates = map.get(abbr);
                    for (StringIntegerPair p : candidates) {
                        if (TokenUtil.indexOfTokensKMP(allTokens, p.getTokens(), 0) != -1) {
                            str = p.getFull();
                        }
                    }
                }
            }
            sb.append(str).append(' ');
        }
        return sb.substring(0, sb.length() - 1);
    }

    public static HashMap<String, String> buildAbbrMap(List<? extends Token> tokens, String text, boolean removeAmbi) {
        HashMap<String, String> abbrMap = new HashMap<String, String>();
        int l = -1;
        int r = -1;
        for (int i = 0; i < tokens.size(); i++) {
            Token t = tokens.get(i);

            if (t.getText().equals("(") || t.getText().equals("-lrb-")) {
                l = i;
            }
            if ((t.getText().equals(")") || t.getText().equals("-rrb-")) && l != -1) {
                r = i;
            }
            if (l == 0)
                continue;

            if (l != -1 && r != -1 && r - l > 1) {
                List<? extends Token> abbrTokens = tokens.subList(l + 1, r);
                String abbr = text.substring(abbrTokens.get(0).getStartOffset(), abbrTokens.get(abbrTokens.size() - 1).getEndOffset());
                if (!isAbbr(abbr))
                    continue;

                abbr = normalizeAbbr(abbr);
                if (abbrMap.containsKey(abbr) && removeAmbi) {
                    abbrMap.remove(abbr);
                    continue;
                }

                String fullForm = traceFullForm(tokens, text, abbr, l - 1);
                if (fullForm != null) {
                    abbrMap.put(abbr, fullForm);
                }
            }
        }
        return abbrMap;
    }

    public static String traceFullForm(List<? extends Token> tokens, String text, String abbr, int start) {
        StringBuilder sb = new StringBuilder();
        String curText = text.substring(tokens.get(start).getStartOffset(), tokens.get(start).getEndOffset());
        int prevIndex = curText.length();
        for (int i = abbr.length() - 1; i >= 0; i--) {
            if (start < 0) {
                return null;
            }
            char c = abbr.charAt(i);
            int index = curText.indexOf(c);
            if (index == -1 || index > prevIndex) {
                return null;
            }
            if (index == 0) {
                sb.insert(0, curText + " ");
                start--;
                if (start >= 0) {
                    curText = text.substring(tokens.get(start).getStartOffset(), tokens.get(start).getEndOffset());
                    prevIndex = curText.length();
                }
            } else {
                prevIndex = index;
            }
        }
        if (sb.length() == 0)
            return null;
        return sb.substring(0, sb.length() - 1);
    }

    private static String normalizeAbbr(String surface) {
        return surface.replaceAll("\\.", "");
    }

    public static boolean isAbbr(String s) {
        return abbrP.matcher(s).find();
    }

    public static boolean isAllCap(String s) {
        return allCapP.matcher(s).matches();
    }
    public static class StringIntegerPair {
        private final int count;
        private final String full;
        private final List<Token> tokens;

        public StringIntegerPair(int count, String full, List<Token> tokens) {
            this.count = count;
            this.full = full;
            this.tokens = tokens;
        }

        public int getCount() {
            return count;
        }

        public String getFull() {
            return full;
        }

        public List<Token> getTokens() {
            return tokens;
        }
    }

}
