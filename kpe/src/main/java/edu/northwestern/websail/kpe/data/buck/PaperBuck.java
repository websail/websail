package edu.northwestern.websail.kpe.data.buck;

import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.kpe.data.paper.Paper;

import java.util.ArrayList;

/**
 * @author csbhagav on 7/31/14.
 */
@Deprecated
public class PaperBuck extends Buck {

    ArrayList<BuckUnit> units = new ArrayList<BuckUnit>();
    Paper paper;

    public PaperBuck(String source, Paper paper) {
        super(source);
        this.paper = paper;
    }

    @Override
    public Iterable<BuckUnit> units() {
        return units;
    }

    @Override
    public BuckUnit unit() {
        return units.get(0);
    }

    @Override
    public void addUnit(BuckUnit unit) {
        units.add(unit);
    }

    @Override
    public void removeUnit(BuckUnit unit) {
        units.remove(unit);
    }

    public ArrayList<BuckUnit> getUnits() {
        return units;
    }

    public void setUnits(ArrayList<BuckUnit> units) {
        this.units = units;
    }

    public Paper getPaper() {
        return paper;
    }

    public void setPaper(Paper paper) {
        this.paper = paper;
    }

    public String toString() {
        return this.paper.getId();
    }
}
