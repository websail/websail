package edu.northwestern.websail.kpe.data;

import com.google.gson.Gson;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.buck.BuckUnitTypes;
import edu.northwestern.websail.ds.tuple.IntegerPair;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.buck.PaperBuck;
import edu.northwestern.websail.kpe.data.paper.Citance;
import edu.northwestern.websail.kpe.data.paper.Paper;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.tokenizer.Tokenizer;
import edu.northwestern.websail.text.util.TokenUtil;
import edu.stanford.nlp.util.StringUtils;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author csbhagav on 6/16/14.
 */
@Deprecated
public class ACLPaperAdapter implements Closeable {

    public static final Logger logger = Logger.getLogger(ACLPaperAdapter.class.getName());

    private static final String INTRO_END_PATTERN = "\\n2 (?:(?:\\b\\w*\\b) ?){1,5}\\n[A-Z]";
    private static final Integer AVG_INTRO_LENGTH = 3500;
    final String dataFile;
    //static Pattern citationPattern = Pattern.compile("[\\(\\[][^\\(^\\)]*[0-9]{4}(, etc\\.)?[\\)\\]]");
    final HashMap<String, HashMap<String, ArrayList<IntegerPair>>> citationsFromToOffsets = new HashMap<String, HashMap<String, ArrayList<IntegerPair>>>();
    final HashMap<String, ArrayList<String>> citationsToFrom = new HashMap<String, ArrayList<String>>();
    final HashMap<String, HashMap<String, ArrayList<Citance>>> citancesToFrom = new HashMap<String, HashMap<String, ArrayList<Citance>>>();
    final Tokenizer tokenizer;
    boolean replaceNewLine = false;

    HashMap<String, Paper> paperDataset;
    Boolean includeCitations = false;
    Boolean inMemory = false;

    // ---------------------------------------------------------------------
    // Constructors
    // ---------------------------------------------------------------------
    public ACLPaperAdapter(String dataFile, Tokenizer tokenizer) {
        this.dataFile = dataFile;
        this.tokenizer = tokenizer;
    }

    public ACLPaperAdapter(String dataFile, Tokenizer tokenizer, String citationsFile) {
        this(dataFile, tokenizer);
        includeCitations = true;
        populateCitationGraph(citationsFile);
    }

    public ACLPaperAdapter(String dataFile, Tokenizer tokenizer, boolean replaceNewLine,
                           String citationsFile) {
        this(dataFile, tokenizer, citationsFile);
        this.replaceNewLine = replaceNewLine;
    }

    public ACLPaperAdapter(String dataFile, Tokenizer tokenizer, boolean replaceNewLine,
                           String citationsFile, boolean loadDatasetInMemory) {
        this(dataFile, tokenizer, replaceNewLine, citationsFile);
        this.inMemory = loadDatasetInMemory;
        if (this.inMemory) {
            paperDataset = getPapers();
        }
    }

    public ACLPaperAdapter(String dataFile, Tokenizer tokenizer, boolean replaceNewLine,
                           String citationsFile, Set<String> paperIds) {
        this(dataFile, tokenizer, replaceNewLine, citationsFile);
        paperDataset = getPapers(paperIds);
    }

    public ACLPaperAdapter(String dataFile, Tokenizer tokenizer, boolean replaceNewLine) {
        this(dataFile, tokenizer);
        this.replaceNewLine = replaceNewLine;
    }

    public ACLPaperAdapter(String dataFile, Tokenizer tokenizer, boolean replaceNewLine, boolean inMemory) {
        this(dataFile, tokenizer, replaceNewLine);
        this.inMemory = inMemory;
        if (this.inMemory) {
            paperDataset = getPapers();
        }
    }

    // ---------------------------------------------------------------------
    // Paper Construction
    // ---------------------------------------------------------------------

    public static String cleanCitance(String citance) {
        int start = 0;
        StringBuilder newCitanceSB = new StringBuilder();
        Matcher matcher = Citance.citationPattern.matcher(citance);
        while (matcher.find()) {
            newCitanceSB.append(citance.substring(start, matcher.start()));
            newCitanceSB.append('(');
            newCitanceSB.append(StringUtils.repeat(' ', matcher.end() - matcher.start() - 2));
            newCitanceSB.append(')');
            start = matcher.end();
        }
        newCitanceSB.append(citance.substring(start, citance.length()));
        return newCitanceSB.toString();
    }

    public static String cleanAbstract(String text, boolean replaceNewLine) {
        String lowAbstr = text.toLowerCase().trim();
        if (lowAbstr.startsWith("abstract")) {
            text = text.replaceFirst("(?i)Abstract", "        ");
        } else if (lowAbstr.startsWith("0 abstract")) {
            text = text.replaceFirst("(?i)0 Abstract", "          ");
        } else if (lowAbstr.startsWith("1 abstract")) {
            text = text.replaceFirst("(?i)1 Abstract", "          ");
        }
        if (replaceNewLine)
            return text.replaceAll("\n", " ");
        return text;
    }

    public static String cleanIntro(String text, boolean replaceNewLine) {
        String lowBody = text.toLowerCase().trim();
        String intro = text;
        if (lowBody.startsWith("introduction")) {
            intro = text.replaceFirst("(?i)Introduction", "            ");
        } else if (lowBody.startsWith("1 introduction")) {
            intro = text.replaceFirst("(?i)1 Introduction", "              ");
        } else if (lowBody.startsWith("1. introduction")) {
            intro = text.replaceFirst("(?i)1. Introduction", "               ");
        } else if (lowBody.startsWith("i. introduction")) {
            intro = text.replaceFirst("(?i)I. INTRODUCTION", "               ");
        } else if (lowBody.startsWith("i introduction")) {
            intro = text.replaceFirst("(?i)I INTRODUCTION", "              ");
        }
        if (replaceNewLine)
            return cleanCitance(intro).replaceAll("\n", " ");
        return cleanCitance(intro);
    }

    private Paper makeAclPaper(Gson gson, String aclJson) {
        Paper paper = gson.fromJson(aclJson, Paper.class);
        try {
            this.tokenizer.initialize(paper.getBodyText());
            this.populateSectionOffsets(paper, tokenizer.getAllTokens());
            tokenizer.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (includeCitations) {
            paper.setCitedBy(citationsToFrom.get(paper.getId()));
            paper.setCitancesFrom(citancesToFrom.get(paper.getId()));
        }
        return paper;
    }

    private void populateSectionOffsets(Paper paper, List<Token> bodyTokens) {
        if (bodyTokens.size() == 0) return;
        Pattern p = Pattern.compile(INTRO_END_PATTERN);
        Matcher m = p.matcher(paper.getBodyText());
        if (m.find()) {
            int start = m.start();
            paper.getSectionOffsets().put(Paper.LOC_INTRO, new IntegerPair(0, start));
        } else {
            int offset = AVG_INTRO_LENGTH < paper.getBodyText().length() ? AVG_INTRO_LENGTH
                    : paper.getBodyText().length() - 1;
            int tokenIdx = TokenUtil.indexOfCharOffset(bodyTokens, offset);
            if (tokenIdx == -1) tokenIdx = TokenUtil.nearestIndexOfCharOffset(
                    bodyTokens, offset);
            paper.getSectionOffsets().put(Paper.LOC_INTRO, new IntegerPair(0,
                    bodyTokens.get(tokenIdx).getEndOffset()));
        }
    }

    // ---------------------------------------------------------------------
    // Buck Construction
    // ---------------------------------------------------------------------

    private PaperBuck makeBuck(Paper paper, boolean fullBody, boolean noBody) {
        PaperBuck buck = new PaperBuck(paper.getId(), paper);
        BuckUnit titleUnit = new BuckUnit();
        titleUnit.set(BuckUnitTypes.TextBUT.class, paper.getTitle() == null ? "" : paper.getTitle());
        titleUnit.set(KPEBUT.LocationType.class, Paper.LOC_TITLE);
        buck.getUnits().add(titleUnit);

        BuckUnit abstractUnit = new BuckUnit();
        abstractUnit.set(BuckUnitTypes.TextBUT.class, this.getAbstract(paper));
        abstractUnit.set(KPEBUT.LocationType.class,
                Paper.LOC_ABSTRACT);
        buck.getUnits().add(abstractUnit);

        if (!noBody) {
            String intro = this.getIntro(paper);
            BuckUnit introTextUnit = new BuckUnit();
            introTextUnit.set(BuckUnitTypes.TextBUT.class, intro);
            introTextUnit.set(KPEBUT.LocationType.class,
                    Paper.LOC_INTRO);
            buck.getUnits().add(introTextUnit);

            if (fullBody) {
                BuckUnit bodyTextUnit = new BuckUnit();
                bodyTextUnit.set(BuckUnitTypes.TextBUT.class, paper.getBodyText().substring(intro.length(), paper.getBodyText().length()));
                bodyTextUnit.set(KPEBUT.LocationType.class,
                        Paper.LOC_BODY);
                buck.getUnits().add(bodyTextUnit);
            }
        }
        BuckUnit citanceParagraphUnit = new BuckUnit();
        citanceParagraphUnit.set(BuckUnitTypes.TextBUT.class,
                paper.getCitancesAsParagraph());
        citanceParagraphUnit.set(KPEBUT.LocationType.class,
                Paper.LOC_CITANCE);

        buck.getUnits().add(citanceParagraphUnit);
        return buck;
    }

    private String getIntro(Paper paper) {
        // ParsedPaper pp = paper.getParcitDocument();
        String intro = "";
        if (paper.getSectionOffsets().get(Paper.LOC_INTRO) != null) {
            intro = paper.getSectionString(Paper.LOC_INTRO);
        }
        if (this.replaceNewLine)
            return cleanIntro(intro, true).replaceAll("\n", " ");
        else
            return cleanIntro(intro, false);
    }

    private String getAbstract(Paper paper) {
        String abstr = paper.getPaperAbstract();
        if (this.replaceNewLine)
            return cleanAbstract(abstr, true).replaceAll("\n", " ");
        else {
            return cleanAbstract(abstr, false);
        }
    }

    // ---------------------------------------------------------------------
    // Citation
    // ---------------------------------------------------------------------

    private void populateCitationGraph(String citationsFile) {
        if (citationsFile == null) {
            logger.warning("Citation file is null, Citation is not loaded");
            return;
        }
        try {
            BufferedReader citationsIn = new BufferedReader(new FileReader(
                    citationsFile));
            String line;
            boolean first = true;
            while ((line = citationsIn.readLine()) != null) {
                if (first) {
                    first = false;
                    continue;
                }

                String[] parts = line.split("\\|\\|\\|");
                String toId = parts[0];
                String fromId = parts[1];
                String citationSection = parts[2];
                Integer startOffset = Integer.valueOf(parts[3]);
                Integer endOffset = Integer.valueOf(parts[4]);
                String citanceString = parts[5];
                Integer listStartOffset = Integer.valueOf(parts[6]);
                Integer listEndOffset = Integer.valueOf(parts[7]);
                String precedingNounPhrase = parts[8];

                if (!citationsFromToOffsets.containsKey(fromId)) {
                    citationsFromToOffsets.put(fromId,
                            new HashMap<String, ArrayList<IntegerPair>>());
                }
                if (!citationsFromToOffsets.get(fromId).containsKey(toId)) {
                    citationsFromToOffsets.get(fromId).put(toId,
                            new ArrayList<IntegerPair>());
                }
                if (!citancesToFrom.containsKey(toId)) {
                    citancesToFrom.put(toId,
                            new HashMap<String, ArrayList<Citance>>());
                }
                if (!citancesToFrom.get(toId).containsKey(fromId)) {
                    citancesToFrom.get(toId).put(fromId,
                            new ArrayList<Citance>());
                }
                if (!citationsToFrom.containsKey(toId)) {
                    citationsToFrom.put(toId, new ArrayList<String>());
                }

                IntegerPair offsetPair = new IntegerPair(startOffset, endOffset);
                Citance citance = new Citance(offsetPair, citanceString);
                citance.setListOffsets(new IntegerPair(listStartOffset,
                        listEndOffset));
                citance.setPrecedinNounPhrase(precedingNounPhrase);
                citance.setSection(citationSection);

                citationsFromToOffsets.get(fromId).get(toId).add(offsetPair);
                citationsToFrom.get(toId).add(fromId);
                citancesToFrom.get(toId).get(fromId).add(citance);
            }
            citationsIn.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // ---------------------------------------------------------------------
    // Read data
    // ---------------------------------------------------------------------

    public HashMap<String, Paper> getPapers() {
        return getPapers(null);
    }

    public HashMap<String, Paper> getPapers(Set<String> paperIds) {
        HashMap<String, Paper> papers = new HashMap<String, Paper>();
        try {
            Gson gson = new Gson();
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    new FileInputStream(this.dataFile), "UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                String id = line.substring(7, line.indexOf("\","));
                if (paperIds != null && !paperIds.contains(id)) {
                    continue;
                }
                Paper paper = makeAclPaper(gson, line);
                if (replaceNewLine)
                    paper.setBodyText(paper.getBodyText().replaceAll("\n", " "));
                papers.put(paper.getId(), paper);
            }
            in.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return papers;
    }
    public List<Buck> getBucks() {
        return this.getBucks(false, false);
    }

    public List<Buck> getBucks(boolean fullbody) {
        return this.getBucks(fullbody, false);
    }

    public List<Buck> getBucks(boolean fullbody, boolean noBody) {
        List<Buck> paperBucks = new ArrayList<Buck>();

        try {
            if (this.inMemory) {
                for (Map.Entry<String, Paper> entry : paperDataset
                        .entrySet()) {
                    Paper paper = entry.getValue();
                    PaperBuck aclBuck = this.makeBuck(paper, fullbody, noBody);
                    paperBucks.add(aclBuck);
                }
            } else {
                Gson gson = new Gson();
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        new FileInputStream(this.dataFile), "UTF-8"));
                String line;
                while ((line = in.readLine()) != null) {
                    Paper paper = makeAclPaper(gson, line);
                    if (replaceNewLine)
                        paper.setBodyText(paper.getBodyText().replaceAll("\n",
                                " "));
                    PaperBuck aclBuck = this.makeBuck(paper, fullbody, noBody);
                    paperBucks.add(aclBuck);
                }
                in.close();
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // System.out.println(paperBucks.size());
        return paperBucks;
    }

    public Paper getACLPaper(String id) {
        if (this.inMemory) {
            return paperDataset.get(id);
        } else {
            logger.warning("Paper dataset is not in memory.");
            return null;
        }
    }

    @Override
    public void close() throws IOException {
    }
}
