package edu.northwestern.websail.kpe.pe.filter;

import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.core.pipeline.unit.par.ParallelPLManager;
import edu.northwestern.websail.kpe.data.LabelAdapter;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.kp.KeyPhrase;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.tokenizer.Tokenizer;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.*;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 * @since 11/21/14
 */
@PipelineConfigInfo(
        requiredResources = {PEVResourceType.TOKENIZER_POOL},
        requiredSettings = {"kpe.labelFile=*"}
)
@PipelineBUTInfo(
        consumes = {KPEBUT.Phrases.class}
)
public class MatchSemEvalKey implements PipelineExecutable {
    public static final String kpeLabelKey = "kpeLabelForSetting";
    public static final Logger logger = Logger.getLogger(MatchSemEvalKey.class.getName());
    public static final Object lock = new Object();
    public static int globalMatchCount = 0;
    public static int globalTotalCount = 0;
    public static int globalPositive = 0;
    public static int globalNegative = 0;
    HashMap<String, ArrayList<KeyPhrase>> labels;
    private int threadId = 0;

    public MatchSemEvalKey() {
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (env.getPipelineGlobalOuputs().containsKey(kpeLabelKey)) {
            logger.info("Get labels from environment.");
            labels = (HashMap<String, ArrayList<KeyPhrase>>) env.getPipelineGlobalOuputs().get(kpeLabelKey);
        } else {
            logger.info("Get labels from file.");
            Tokenizer tokenizer = (Tokenizer) env.getPoolResource(
                    PEVResourceType.TOKENIZER_POOL, 0,
                    PEVResourceType.TOKENIZER);
            String trainingFile = config.getProperty("kpe.labelFile");

            labels = LabelAdapter.readSemEvalLabel(trainingFile, true, tokenizer);

        }
        return labels != null;

    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        setLabels(prev);
        return true;
    }

    private void setLabels(Buck buck) {
        ArrayList<KeyPhrase> paperLabels = labels.get(buck.getSource());
        int match = 0;
        int unmatch = 0;
        HashSet<String> matched = new HashSet<String>();
        HashSet<String> all = new HashSet<String>();
        for (BuckUnit unit : buck.units()) {
            for (Phrase phrase : unit.get(KPEBUT.Phrases.class)) {
                boolean isKey = false;
                if (paperLabels != null) {
                    List<? extends Token> tokens = phrase.getSpan().getTokens();
                    String normalizedPhrase = TokenUtil.tokens2Str(tokens);
                    for (KeyPhrase kp : paperLabels) {
                        all.add(kp.getNormalizedPhrase());
                        if (kp.getNormalizedPhrase().equals(normalizedPhrase)) {
                            isKey = true;
                            matched.add(kp.getNormalizedPhrase());
                            break;
                        }
                    }
                }
                phrase.setKey(isKey);
                if (isKey) match++;
                else unmatch++;
            }
        }
        if (paperLabels == null) {
            logger.fine(buck.getSource() + ": not a training paper.");
        } else {
            HashSet<String> missing = new HashSet<String>(all);
            missing.removeAll(matched);
            logger.fine(buck.getSource() + ": " + match + " positive, " + unmatch + " negative. " +
                    "Labels: " + matched.size() + "/" + all.size() + " matched.\n" +
                    "Missing key: " + missing.toString());
            synchronized (lock) {
                globalMatchCount += matched.size();
                globalTotalCount += all.size();
                globalPositive += match;
                globalNegative += unmatch;
            }
        }
    }


    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        ParallelPLManager.countDownLatch(env, ParallelPLManager.ALL_LATCH_1);
        ParallelPLManager.awaitLatch(env, ParallelPLManager.ALL_LATCH_1);
        if (this.threadId == 0) {
            logger.info("Phrase Match " + globalPositive + "/" + (globalPositive + globalNegative) + " = " + ((double) globalPositive / (globalPositive + globalNegative)) +
                    ", Label Match: " + globalMatchCount + "/" + globalTotalCount + " = " + ((double) globalMatchCount / globalTotalCount));
        }
        return true;
    }

    @Override
    public int getThreadId() {
        return threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;

    }
}
