package edu.northwestern.websail.kpe.data;

import com.google.gson.Gson;
import edu.northwestern.websail.core.io.InputFileManager;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.buck.BuckUnitTypes;
import edu.northwestern.websail.ds.tuple.IntegerPair;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import edu.northwestern.websail.kpe.data.paper.S2CitationContext;
import edu.northwestern.websail.kpe.data.paper.S2Paper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 * @since 8/13/15
 * <p/>
 * Read papers from S2 format as of Jul 15
 */
public class S2PaperAdapter {
    public static final Logger logger = Logger.getLogger(S2PaperAdapter.class.getName());
    // Processor
    Gson gson;
    // S2 Paper does not need this, but data from Parcit (SemEval, CeKE, or old S2 format) will need to set to true
    boolean replaceNewLine = false;
    // Bookkeeping data
    int numPaperNoConclusion = 0;
    int numPaperNoIntro = 0;
    int numPaperParsed = 0;

    public S2PaperAdapter() {
        super();
        gson = new Gson();
    }

    public S2PaperAdapter(boolean replaceNewLine) {
        super();
        gson = new Gson();
        this.replaceNewLine = replaceNewLine;
    }

    public static void main(String[] args) {
        InputFileManager inMgr = new InputFileManager("/Users/NorThanapon/Desktop/part-13376");
        String line;

        while ((line = inMgr.readLine()) != null) {
            S2PaperAdapter adapter = new S2PaperAdapter(true);
            S2Paper paper = adapter.makePaper(line);
            if (paper == null) {
                System.out.println("Not processing");
                return;
            }
            if (!paper.getId().equals("03b1607cb2dcb79297e3a5b8b475f17d92ff9243")) continue;
            System.out.println("================Adapter Info=====================");
            System.out.println("Papers Parsed: " + adapter.numPaperParsed);
            System.out.println("Introduction not found: " + adapter.numPaperNoIntro);
            System.out.println("Conclusion not found: " + adapter.numPaperNoConclusion);
            System.out.println("================ID=====================");
            System.out.println(paper.getId());
            System.out.println("================Title=====================");
            System.out.println(paper.getTitle());
            System.out.println("================Abstract=====================");
            System.out.println(paper.getPaperAbstract());
            System.out.println("================Body=====================");
            System.out.println(paper.getBodyText());
            System.out.println("================Citation=====================");
            for (S2CitationContext context : paper.getCitationContexts()) {
                System.out.println(context.getCitationString());
                System.out.println(context.getCitationCount());
            }
            System.out.println("================Pref Citation Paragraphs=====================");
            System.out.println(paper.getCitationParagraphs());
            System.out.println("================Intro=====================");
            if (paper.getSectionOffsets().get(S2PaperUtil.LOC_INTRO) != S2PaperUtil.NOT_FOUND_OFFSET) {
                System.out.println(paper.getBodyText().substring(paper.getSectionOffsets().get(S2PaperUtil.LOC_INTRO).getFirst(), paper.getSectionOffsets().get(S2PaperUtil.LOC_INTRO).getSecond()));
            } else {
                System.out.println("INTRODUCTION NOT FOUND");
            }
            System.out.println("================Conclusion=====================");
            if (paper.getSectionOffsets().get(S2PaperUtil.LOC_CONCLUSION) != S2PaperUtil.NOT_FOUND_OFFSET) {
                System.out.println(paper.getBodyText().substring(paper.getSectionOffsets().get(S2PaperUtil.LOC_CONCLUSION).getFirst(), paper.getSectionOffsets().get(S2PaperUtil.LOC_CONCLUSION).getSecond()));
            } else {
                System.out.println("CONCLUSION NOT FOUND");
            }
            System.out.println("================Buck=====================");
            S2PaperBuck buck = adapter.makeBuck(paper, S2PaperUtil.WHOLE_PAPER_LOCATIONS);
            for (BuckUnit unit : buck.units()) {
                System.out.println("--------------------Units----------------------------");
                System.out.println(unit.get(BuckUnitTypes.TextBUT.class));
            }

        }
        inMgr.close();
    }

    // ---------------------------------------------------------------------
    // Paper Construction
    // ---------------------------------------------------------------------

    public S2Paper makePaper(String jsonString) {
        if (jsonString.contains(",\"pdfProcessed\":false}")) return null;
        numPaperParsed++;
        S2Paper paper = gson.fromJson(jsonString, S2Paper.class);
        if (replaceNewLine) {
            if (paper.getBodyText() != null)
                paper.setBodyText(paper.getBodyText().replaceAll("\n", " "));
            if (paper.getPaperAbstract() != null)
                paper.setPaperAbstract(paper.getPaperAbstract().replaceAll("\n", " "));
            if (paper.getTitle() != null)
                paper.setTitle(paper.getTitle().replaceAll("\n", " "));
        }
        this.populateSectionOffsets(paper);

        return paper;
    }

    private void populateSectionOffsets(S2Paper paper) {
        if (paper.getBodyText() == null || paper.getBodyText().trim().length() == 0) {
            paper.getSectionOffsets().put(S2PaperUtil.LOC_INTRO, S2PaperUtil.NOT_FOUND_OFFSET);
            this.numPaperNoIntro++;
            this.numPaperNoConclusion++;
            paper.getSectionOffsets().put(S2PaperUtil.LOC_CONCLUSION, S2PaperUtil.NOT_FOUND_OFFSET);
            return;
        }
        IntegerPair introOffset = S2PaperUtil.locateIntroOffsets(paper.getBodyText());
        IntegerPair conclusionOffset = S2PaperUtil.locateConclusionOffsets(paper.getBodyText());
        if (introOffset == null) {
            this.numPaperNoIntro++;
            paper.getSectionOffsets().put(S2PaperUtil.LOC_INTRO, S2PaperUtil.NOT_FOUND_OFFSET);
        } else {
            paper.getSectionOffsets().put(S2PaperUtil.LOC_INTRO, introOffset);
        }
        if (conclusionOffset == null) {
            this.numPaperNoConclusion++;
            paper.getSectionOffsets().put(S2PaperUtil.LOC_CONCLUSION, S2PaperUtil.NOT_FOUND_OFFSET);
        } else {
            paper.getSectionOffsets().put(S2PaperUtil.LOC_CONCLUSION, conclusionOffset);
        }
    }

    // ---------------------------------------------------------------------
    // Buck Construction
    // ---------------------------------------------------------------------

    public S2PaperBuck makeBuck(S2Paper paper, HashSet<String> paperParts) {
        S2PaperBuck buck = new S2PaperBuck(paper.getId(), paper);
        if (paperParts.contains(S2PaperUtil.LOC_TITLE)) {
            BuckUnit titleUnit = new BuckUnit();
            titleUnit.set(BuckUnitTypes.TextBUT.class, paper.getTitle() == null ? "" : paper.getTitle());
            titleUnit.set(KPEBUT.LocationType.class, S2PaperUtil.LOC_TITLE);
            buck.getUnits().add(titleUnit);
        }

        if (paperParts.contains(S2PaperUtil.LOC_ABSTRACT)) {
            BuckUnit abstractUnit = new BuckUnit();
            abstractUnit.set(BuckUnitTypes.TextBUT.class, S2PaperUtil.cleanAbstract(this.cleanText(paper.getPaperAbstract())));
            abstractUnit.set(KPEBUT.LocationType.class, S2PaperUtil.LOC_ABSTRACT);
            buck.getUnits().add(abstractUnit);
        }

        if (paperParts.contains(S2PaperUtil.LOC_BODY)) { // with body included, we have no need to add intro or conclusion
            BuckUnit bodyTextUnit = new BuckUnit();
            String bodyText = paper.getBodyText();
            bodyTextUnit.set(BuckUnitTypes.TextBUT.class, this.cleanText(bodyText));
            bodyTextUnit.set(KPEBUT.LocationType.class, S2PaperUtil.LOC_BODY);
            buck.getUnits().add(bodyTextUnit);
        } else { // now look for intro and conclusion
            if (paperParts.contains(S2PaperUtil.LOC_INTRO)) {
                IntegerPair offset = paper.getSectionOffsets().get(S2PaperUtil.LOC_INTRO);
                if (offset == S2PaperUtil.NOT_FOUND_OFFSET) { // now we have to approximate
                    offset = S2PaperUtil.approximateIntroLenght(paper.getBodyText());
                }
                String intro = "";
                if (offset != S2PaperUtil.NOT_FOUND_OFFSET) { // else nothing for us to do
                    intro = paper.getBodyText().substring(offset.getFirst(), offset.getSecond());
                }
                BuckUnit introTextUnit = new BuckUnit();
                introTextUnit.set(BuckUnitTypes.TextBUT.class, this.cleanText(intro));
                introTextUnit.set(KPEBUT.LocationType.class, S2PaperUtil.LOC_INTRO);
                buck.getUnits().add(introTextUnit);
            }
            if (paperParts.contains(S2PaperUtil.LOC_CONCLUSION)) {
                IntegerPair offset = paper.getSectionOffsets().get(S2PaperUtil.LOC_CONCLUSION);
                String conclusion = "";
                if (offset != S2PaperUtil.NOT_FOUND_OFFSET) {
                    conclusion = this.cleanText(paper.getBodyText().substring(offset.getFirst(), offset.getSecond()));
                }
                BuckUnit conclusionUnit = new BuckUnit();
                conclusionUnit.set(BuckUnitTypes.TextBUT.class, conclusion);
                conclusionUnit.set(KPEBUT.LocationType.class, S2PaperUtil.LOC_CONCLUSION);
                buck.getUnits().add(conclusionUnit);
            }
        }

        if (paperParts.contains(S2PaperUtil.LOC_CITANCE)) {
            BuckUnit citanceParagraphUnit = new BuckUnit();
            citanceParagraphUnit.set(BuckUnitTypes.TextBUT.class, this.cleanText(paper.getCitationParagraphs()));
            citanceParagraphUnit.set(KPEBUT.LocationType.class, S2PaperUtil.LOC_CITANCE);
            buck.getUnits().add(citanceParagraphUnit);
        }

        return buck;
    }

    private String cleanText(String text) {
        return S2PaperUtil.removeNoisesAndEquations( // some noises are extremely long
                        S2PaperUtil.removeNoisesAndEquations(
                                S2PaperUtil.cleanCitationContext(
                                        S2PaperUtil.cleanSectionHeaders(text))));
    }

    public ArrayList<S2PaperBuck> makeAllBuck(String dataFile, HashSet<String> paperParts) {
        ArrayList<S2PaperBuck> bucks = new ArrayList<S2PaperBuck>();
        String line;
        InputFileManager inMgr = new InputFileManager(dataFile);
        while ((line = inMgr.readLine()) != null) {
            S2Paper paper = this.makePaper(line);
            if (paper == null) continue;
            bucks.add(this.makeBuck(paper, paperParts));
        }
        return bucks;
    }
}
