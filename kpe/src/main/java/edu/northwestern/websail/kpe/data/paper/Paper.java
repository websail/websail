package edu.northwestern.websail.kpe.data.paper;

import edu.northwestern.websail.ds.tuple.IntegerPair;
import edu.northwestern.websail.kpe.data.ACLPaperAdapter;

import java.util.*;

/**
 * @author csbhagav on 6/16/14.
 */
@Deprecated
public class Paper {

    /*
     * paper keys [u'bodyText', u'totalCitedByCount', u'paperAbstract',
     * u'classification', u'title', u'authors', u'venue', u'citationRank',
     * u'followOn', u'citedBy', u'references', u'year', u'parcitDocument',
     * u'keyPhrases', u'id', u'citationRate']
     */
    public static final int CITATION_THRESHOLD = 20;
    public static final String LOC_TITLE = "TITLE";
    public static final String LOC_ABSTRACT = "ABSTRACT";
    public static final String LOC_INTRO = "INTRODUCTION";
    public static final String LOC_BODY = "BODY";
    public static final String LOC_CITANCE = "CITANCE";
    String id;
    String title;
    String venue;
    ArrayList<String> authors;
    String year;
    String paperAbstract;
    String bodyText;
    Integer completeRefCount;
    // citation count < 2
    ArrayList<String> keyPhrases;
    ArrayList<String> references;
    HashMap<String, ArrayList<Citance>> citancesFrom;
    HashMap<String, ArrayList<Citance>> citancesTo;
    ArrayList<String> citedBy;
    ArrayList<Citance> customCitanceCollection; // Stores top 10 based on ascending citation count and then only
    HashMap<String, IntegerPair> sectionOffsets;

    public Paper() {
        this.sectionOffsets = new HashMap<String, IntegerPair>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public ArrayList<String> getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList<String> authors) {
        this.authors = authors;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPaperAbstract() {
        return paperAbstract;
    }

    public void setPaperAbstract(String paperAbstract) {
        this.paperAbstract = paperAbstract;
    }

    public String getBodyText() {
        return bodyText;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }

    public Integer getCompleteRefCount() {
        return completeRefCount;
    }

    public void setCompleteRefCount(Integer completeRefCount) {
        this.completeRefCount = completeRefCount;
    }

    public ArrayList<String> getKeyPhrases() {
        return keyPhrases;
    }

    public void setKeyPhrases(ArrayList<String> keyPhrases) {
        this.keyPhrases = keyPhrases;
    }

    public HashMap<String, ArrayList<Citance>> getCitancesFrom() {
        return citancesFrom;
    }

    public void setCitancesFrom(HashMap<String, ArrayList<Citance>> citancesFrom) {
        this.citancesFrom = citancesFrom;
        if (citancesFrom == null) return;
        ArrayList<Citance> allCitances = new ArrayList<Citance>();
        for (Map.Entry<String, ArrayList<Citance>> entry : this.citancesFrom.entrySet()) {
            allCitances.addAll(entry.getValue());
        }

        Collections.sort(allCitances, new Comparator<Citance>() {
            @Override
            public int compare(Citance o1, Citance o2) {
                return o1.getCitationCount() - o2.getCitationCount();
            }
        });
        this.customCitanceCollection = new ArrayList<Citance>();
        //if(CITATION_THRESHOLD==0) return;
        for (int i = 0; i < allCitances.size(); i++) {
            Citance citance = allCitances.get(i);

            if (i >= CITATION_THRESHOLD && citance.getCitationCount() > 1) {
                break;
            }
            this.customCitanceCollection.add(citance);
        }
    }

    public ArrayList<Citance> getCustomCitanceCollection() {
        return customCitanceCollection;
    }

    public void setCustomCitanceCollection(ArrayList<Citance> customCitanceCollection) {
        this.customCitanceCollection = customCitanceCollection;
    }

    public ArrayList<String> getCitedBy() {
        return citedBy;
    }

    public void setCitedBy(ArrayList<String> citedBy) {
        this.citedBy = citedBy;
    }

    public ArrayList<String> getReferences() {
        return references;
    }

    public void setReferences(ArrayList<String> references) {
        this.references = references;
    }

    public String getSectionString(String locationString) {
        IntegerPair pair = this.sectionOffsets.get(locationString);
        if (pair == null) {
            return "";
        }
        return this.bodyText.substring(pair.getFirst(), pair.getSecond());
    }

    public HashMap<String, IntegerPair> getSectionOffsets() {
        return sectionOffsets;
    }

    public void setSectionOffsets(HashMap<String, IntegerPair> sectionOffsets) {
        this.sectionOffsets = sectionOffsets;
    }

    public String getCitancesAsParagraph() {
        if (this.citancesFrom == null || this.customCitanceCollection == null)
            return "";

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < this.customCitanceCollection.size() && i < 20; i++) {
            sb.append(ACLPaperAdapter.cleanCitance(this.customCitanceCollection.get(i).getSentence())).append("\n");
        }

        return sb.toString();
    }
}
