package edu.northwestern.websail.kpe.pe.filter;

import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.LabelAdapter;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.kp.KeyPhrase;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.tokenizer.Tokenizer;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 11/21/14
 */
@PipelineConfigInfo(
        requiredResources = {PEVResourceType.TOKENIZER_POOL},
        requiredSettings = {"kpe.labelFile=*"}
)
@PipelineBUTInfo(
        consumes = {KPEBUT.Phrases.class}
)
public class MatchACLKey implements PipelineExecutable {
    public static final String kpeLabelKey = "kpeLabelForSetting";
    HashMap<String, ArrayList<KeyPhrase>> labels;
    private int threadId = 0;

    public MatchACLKey() {
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (env.getPipelineGlobalOuputs().containsKey(kpeLabelKey)) {
            labels = (HashMap<String, ArrayList<KeyPhrase>>) env.getPipelineGlobalOuputs().get(kpeLabelKey);

        } else {
            Tokenizer tokenizer = (Tokenizer) env.getPoolResource(
                    PEVResourceType.TOKENIZER_POOL, 0,
                    PEVResourceType.TOKENIZER);
            String trainingFile = config.getProperty("kpe.labelFile");
            labels = LabelAdapter.readLabelFile(trainingFile, tokenizer, null);
        }
        return labels != null;

    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        setLabels(prev);
        return true;
    }

    private void setLabels(Buck buck) {
        ArrayList<KeyPhrase> paperLabels = labels.get(buck.getSource());
        for (BuckUnit unit : buck.units()) {
            for (Phrase phrase : unit.get(KPEBUT.Phrases.class)) {
                boolean isKey = false;
                if (paperLabels != null) {
                    List<? extends Token> tokens = phrase.getSpan().getTokens();
                    String normalizedPhrase = TokenUtil.tokens2Str(tokens);
                    for (KeyPhrase kp : paperLabels) {
                        if (kp.getNormalizedPhrase().equals(normalizedPhrase)) {
                            isKey = true;
                            break;
                        }
                    }
                }
                phrase.setKey(isKey);
            }
        }
    }


    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public int getThreadId() {
        return threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;

    }
}
