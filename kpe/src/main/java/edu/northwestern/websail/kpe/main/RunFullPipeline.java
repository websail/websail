package edu.northwestern.websail.kpe.main;

import edu.northwestern.websail.classify.classifier.weka.WekaClassifierWrapper;
import edu.northwestern.websail.core.pipeline.PipelineExecutor;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.kpe.data.ACLPaperAdapter;
import edu.northwestern.websail.kpe.data.LabelAdapter;
import edu.northwestern.websail.kpe.data.kp.KeyPhrase;
import edu.northwestern.websail.kpe.pe.filter.MatchACLKey;
import edu.northwestern.websail.text.tokenizer.Tokenizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;

/**
 * @author NorThanapon
 * @since 1/8/15
 */
public class RunFullPipeline {
    public static void main(String[] args) throws IOException {
        String mainPipelineFile = "pconfig/full/jan9/init.xml";
        String trainPipelineFile = "pconfig/full/jan9/train.xml";
        String outPipelineFile = "pconfig/full/jan9/out.xml";
        String trainingFile = "/websail/common/keyphrase_extraction/kpe/kpe.labels/kpe.training_label.v3";
        String dataFile = "/websail/common/keyphrase_extraction/data/papers/acl.json";
        String citationFile = "/websail/common/keyphrase_extraction/data/citations/citation_context.txt";
        WekaClassifierWrapper.logger.setLevel(Level.FINE);
        PipelineExecutor mainPipeline = null;
        PipelineExecutor trainPipeline = null;
        PipelineExecutor outPipeline = null;
        try {
            mainPipeline = new PipelineExecutor(mainPipelineFile);
            Tokenizer tokenizer = (Tokenizer) mainPipeline.getEnv().getPoolResource(
                    PEVResourceType.TOKENIZER_POOL, 0,
                    PEVResourceType.TOKENIZER);
            System.out.print("Loading label data... ");
            HashMap<String, ArrayList<KeyPhrase>> trainingLabels =
                    LabelAdapter.readLabelFile(trainingFile, tokenizer, null);
            System.out.println(trainingLabels.keySet().size() + " papers");
            mainPipeline.getEnv().getPipelineGlobalOuputs().put(MatchACLKey.kpeLabelKey, trainingLabels);
            System.out.print("Loading papers... ");
            ACLPaperAdapter adapter = new ACLPaperAdapter(dataFile, tokenizer, true, citationFile, trainingLabels.keySet());
            ArrayList<Buck> trainBucks = new ArrayList<Buck>();
            ArrayList<Buck> allBucks = new ArrayList<Buck>();
            for (Buck b : adapter.getBucks()) {
                if (trainingLabels.containsKey(b.getSource())) {
                    trainBucks.add(b);
                }
                allBucks.add(b);
            }
            adapter.close();
            System.out.println(allBucks.size() + " papers.");
            System.out.println("Start main pipeline...");
            mainPipeline.parRun(allBucks);
            System.out.println("Start training pipeline...");
            trainPipeline = new PipelineExecutor(trainPipelineFile, mainPipeline);
            trainPipeline.run(trainBucks);
            System.out.println("Start output pipeline...");
            outPipeline = new PipelineExecutor(outPipelineFile, mainPipeline);
            outPipeline.parRun(allBucks);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (trainPipeline != null) {
                trainPipeline.close();
            }
            if (mainPipeline != null) {
                mainPipeline.close();
            }
            if (outPipeline != null) {
                outPipeline.close();
            }
        }
    }
}
