package edu.northwestern.websail.kpe.pe;


import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnitTypes;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.chunker.Chunker;
import edu.northwestern.websail.text.chunker.NounPhraseFoundDelegate;
import edu.northwestern.websail.text.chunker.StanfordChunker;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.tokenizer.SentenceSpliter;
import edu.northwestern.websail.text.tokenizer.StanfordNLPSSplit;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.List;
import java.util.Properties;

/**
 * @author NorThanapon
 */
@PipelineConfigInfo(
        requiredResources = {PEVResourceType.S_SENTENCE_SPLITER_POOL, PEVResourceType.S_CHUNKER_POOL},
        requiredSettings = {"text.tokenizer.SentenceSpliter.pos=true"}
)
@PipelineBUTInfo(
        consumes = {BuckUnitTypes.TextBeginOffsetBUT.class, BuckUnitTypes.TextBUT.class},
        produces = {KPEBUT.Sentences.class, KPEBUT.Phrases.class}
)
public class NounPhrasePE extends PhraseExtractor implements NounPhraseFoundDelegate {

    protected Chunker chunker;
    protected SentenceSpliter ssplit;
    protected FastList<Phrase> curPhrases;
    protected TokenSpan curSentence;
    protected String curText;

    public NounPhrasePE() {
        super();
    }

    public NounPhrasePE(Chunker chunker, SentenceSpliter ssplit) {
        super();
        this.chunker = chunker;
        this.ssplit = ssplit;
    }

    public static void main(String[] args) throws Exception {
        String text = "Erik Ehn is an American playwright and director known for proposing the Regional Alternative Theatre movement. "
                + "The former dean of theater at CalArts , "
                + "the California Institute of Arts, he is head of playwriting and professor of theatre. "
                + "and performance studies at Myrtle Beach Film festival . "
                + "His published works include The Saint Plays, Beginner \n and 13 Christs."
                + " Erik's mother was a dancer. combsum and combmnz strategy";
        StanfordChunker ssa = new StanfordChunker();
        StanfordNLPSSplit nlpsSplit = new StanfordNLPSSplit();
        NounPhrasePE pe = new NounPhrasePE(ssa, nlpsSplit);
        System.out.println(pe.extract(text, "test").get(0).getSpan());
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config))
            return false;
        this.initialized = false;
        this.chunker = (Chunker) env.getPoolResource(PEVResourceType.S_CHUNKER_POOL, this.getThreadId(), PEVResourceType.S_CHUNKER);
        if (chunker == null) {
            logger.severe(PEVResourceType.S_CHUNKER_POOL
                    + " is not in the environment.");
            return false;
        }
        this.ssplit = (SentenceSpliter) env.getPoolResource(
                PEVResourceType.S_SENTENCE_SPLITER_POOL,
                this.threadId, PEVResourceType.S_SENTENCE_SPLITER);
        if (ssplit == null) {
            ssplit = new StanfordNLPSSplit(true);
        }
        this.initialized = true;
        return true;
    }

    @Override
    protected FastList<Phrase> extract(String text) throws Exception {
        return extract(text, null);
    }

    public FastList<Phrase> extract(String text, String source) throws Exception {
        if (source != null) this.curSource = source;
        curText = text;
        curPhrases = new FastList<Phrase>();
        if (normalizedToken) {
            logger.warning("Text has been normalized. This PE will not work as expected.");
        }
        if (this.curSentences == null) this.curSentences = ssplit.sentenceSplit(text);
        if (this.chunker.getClass() == StanfordChunker.class) {
            try {
                this.chunker.initialize(text, curSource);
                this.chunker.extractNounPhrase(this);
            } catch (Exception e) {
                logger.severe("StanfordChunker cannot parse the input text. " + e.getMessage());
                return curPhrases;
            }
        } else {
            for (TokenSpan sentence : curSentences) {
                FastList<TokenSpan> s = new FastList<TokenSpan>();
                s.add(sentence);
                this.curSentence = sentence;
                this.chunker.initialize(s, curSource);
                this.chunker.extractNounPhrase(this);
            }
        }
        if (!normalizedToken) {
            for (TokenSpan s : this.curSentences) {
                List<? extends Token> tokens = s.getTokens();
                s.setTokens(TokenUtil.cleanTokens(TokenUtil.normalize(tokens, true, true), false, null));
            }
        }
        return curPhrases;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        normalizedToken = true;
        return super.close(env, config);
    }

    @Override
    public void found(TokenSpan nounPhrase) throws Exception {
        Phrase phrase = new Phrase(curText.substring(nounPhrase.getStartOffset(), nounPhrase.getEndOffset()), this.curSource, nounPhrase);
        Token startToken = nounPhrase.getTokens().get(0);
        Token endToken = nounPhrase.getTokens().get(nounPhrase.getTokens().size() - 1);
        int startTokenIndex = TokenUtil.indexOfCharOffset(curSentence.getTokens(), startToken.getStartOffset());
        int endTokenIndex = TokenUtil.indexOfCharOffset(curSentence.getTokens(), endToken.getStartOffset());
        if (startTokenIndex == -1 || endTokenIndex == -1) {
            logger.warning("Cannot find the input noun phrase in the current senetence. Ignore this noun phrase.");
        }
        if (startTokenIndex == 0) {
            phrase.setBefore(Token.START);
        } else {
            phrase.setBefore(curSentence.getTokens().get(startTokenIndex - 1));
        }
        if (endTokenIndex == curSentence.getTokens().size() - 1) {
            phrase.setAfter(Token.END);
        } else {
            phrase.setAfter(curSentence.getTokens().get(endTokenIndex + 1));
        }
        this.curPhrases.add(phrase);
    }

    @Override
    public void foundSub(TokenSpan cover, TokenSpan nounPhrase) {
        Phrase phrase = new Phrase(curText.substring(nounPhrase.getStartOffset(), nounPhrase.getEndOffset()), this.curSource, nounPhrase);
        Token startToken = nounPhrase.getTokens().get(0);
        Token endToken = nounPhrase.getTokens().get(nounPhrase.getTokens().size() - 1);
        int startTokenIndex = TokenUtil.indexOfCharOffset(curSentence.getTokens(), startToken.getStartOffset());
        int endTokenIndex = TokenUtil.indexOfCharOffset(curSentence.getTokens(), endToken.getStartOffset());
        if (startTokenIndex == -1 || endTokenIndex == -1) {
            logger.warning("Cannot find the input noun phrase in the current senetence. Ignore this noun phrase.");
        }
        if (startTokenIndex == 0) {
            phrase.setBefore(Token.START);
        } else {
            phrase.setBefore(curSentence.getTokens().get(startTokenIndex - 1));
        }
        if (endTokenIndex == curSentence.getTokens().size() - 1) {
            phrase.setAfter(Token.END);
        } else {
            phrase.setAfter(curSentence.getTokens().get(endTokenIndex + 1));
        }
        this.curPhrases.add(phrase);
    }

    @Override
    public void setSentence(TokenSpan sentence) {
        this.curSentence = sentence;
        curSentence.setTokens(TokenUtil.cleanTokens(TokenUtil.normalize(curSentence.getTokens(), true, true), false, null));
    }
}
