package edu.northwestern.websail.kpe.main.preprocessing;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;
import edu.berkeley.nlp.lm.StupidBackoffLm;
import edu.berkeley.nlp.lm.io.IOUtils;
import edu.berkeley.nlp.lm.io.LmReaders;
import edu.berkeley.nlp.lm.map.NgramMapWrapper;
import edu.berkeley.nlp.lm.util.LongRef;
import edu.northwestern.websail.core.io.InputFileManager;
import edu.northwestern.websail.core.util.TimerUtil;
import edu.northwestern.websail.ds.tuple.StringLongPair;
import edu.northwestern.websail.kpe.data.ACLPaperAdapter;
import edu.northwestern.websail.kpe.data.paper.Paper;
import edu.northwestern.websail.text.corpus.data.lucene.IndexAggregator;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.ngram.ARPALMWrapper;
import edu.northwestern.websail.text.ngram.NGramUtils;
import edu.northwestern.websail.text.ngram.NgramMapCountWrapper;
import edu.northwestern.websail.text.ngram.SRILMAdapter;
import edu.northwestern.websail.text.tokenizer.StanfordNLPSSplit;
import edu.northwestern.websail.text.tokenizer.StanfordNLPTokenizer;
import edu.northwestern.websail.text.util.TokenUtil;
import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

/**
 * @author NorThanapon
 * @since 1/27/15
 */
public class BuildResources {
    static final String buckDir = null;
    final static int countOrder = 5;
    final static int lmOrder = 3;
    static String dataFile = null;
    static String citationFile = null;
    static String indexDir = null;
    static String ngramDir = null;
    static String paperNgramDir = null;
    static String web1TBLM = null;
    static String web1TVocab = null;
    static boolean useStopword = false;
    static boolean clean = false;
    static IndexAggregator agg = null;
    static SRILMAdapter corpusSRILM = null;
    static SRILMAdapter web1TSRILM = null;
    static StanfordNLPSSplit sSpliter;
    static StanfordNLPTokenizer tokenizer;
    static String[] eachNgramCountFiles = null;
    static String web1tDir = null;
    static String[] web1tEachCountFiles = null;

    public static void main(String[] args) throws IOException, InterruptedException {
        TimerUtil timer = new TimerUtil();
        timer.start();
        if (!processOptions(args)) {
            return;
        }
        //XXX: Hack tokenizer and sentence spliter type

        sSpliter = new StanfordNLPSSplit(true);
        tokenizer = new StanfordNLPTokenizer();


        initIndex();
        initNgram();
        initWeb1T();
        initPaperNgram();

        System.out.println("Loading papers...");
        ACLPaperAdapter adapter;
        if (citationFile != null) {
            adapter = new ACLPaperAdapter(dataFile, tokenizer, true,
                    citationFile, true);
        } else {
            adapter = new ACLPaperAdapter(dataFile, tokenizer, true, true);
        }
        System.out.println("Building resources...");

        int i = 0;

        for (Map.Entry<String, Paper> p : adapter.getPapers().entrySet()) {
            Paper paper = p.getValue();

            ArrayList<TokenSpan> sentences = new ArrayList<TokenSpan>();
            sentences.addAll(sSpliter.sentenceSplit(paper.getTitle()));
            sentences.addAll(sSpliter.sentenceSplit(paper.getPaperAbstract()));
            sentences.addAll(sSpliter.sentenceSplit(paper.getBodyText()));
            sentences.addAll(sSpliter.sentenceSplit(paper.getCitancesAsParagraph()));
            String content = join(sentences);
            String normContent = normalize(sentences);
            index(i++, p.getKey(), normContent);
            ngram(normContent);
            web1TNgram(content);
            paperNgram(p.getKey(), normContent, sSpliter);
        }
        adapter.close();
        finalizeIndex();
        finalizeNgram();
        finalizeWeb1TNgram();
        deleteNgramCountTempFiles();
        timer.end();
        System.out.println("Done " + timer.totalTime());
    }

    private static void initIndex() throws IOException {
        if (indexDir == null) {
            return;
        }
        if (checkDirectory(indexDir)) {
            System.out.println("Initializing index builder");
            agg = new IndexAggregator();
            agg.setOnlyWhiteSpace(true);
            agg.initEmptyAggregator(indexDir);
        } else {
            System.out.println("Skip building index");
            indexDir = null;
        }
    }

    private static void initPaperNgram() throws IOException {
        if (paperNgramDir == null) {
            return;
        }
        if (checkDirectory(paperNgramDir)) {
            System.out.println("Initializing paper ngram language model");
        } else {
            System.out.println("Skip building paper ngram language model");
            paperNgramDir = null;
        }
    }

    private static void initNgram() throws IOException {

        eachNgramCountFiles = new String[countOrder];
        for (int i = 0; i < countOrder; i++) {
            eachNgramCountFiles[i] = ngramDir + "/" + (i + 1) + "gms";
        }
        if (checkDirectory(ngramDir)) {
            System.out.println("Initializing ngram language model");
            for (String n : eachNgramCountFiles) {
                checkDirectory(n);
            }
            SRILMAdapter sriLM = new SRILMAdapter(sSpliter);
            SRILMAdapter.VERBOSE = false;
            sriLM.INTEMP = "acl_ncase_stem.txt";
            sriLM.setDeleteTemp(false);
            corpusSRILM = sriLM;

        } else {
            System.out.println("Skip building ngram language model");
        }
        for (int i = 0; i < countOrder; i++) {
            if (i == 0) {
                eachNgramCountFiles[i] = eachNgramCountFiles[i] + "/vocab_cs";
            } else {
                eachNgramCountFiles[i] = eachNgramCountFiles[i] + "/" + (i + 1) + "gm-0000";
            }

        }
    }

    private static void initWeb1T() throws IOException {
        if (web1TVocab == null || web1TBLM == null) return;
        web1tDir = ngramDir + "/web1t";
        web1tEachCountFiles = new String[countOrder];
        checkDirectory(web1tDir);
        for (int i = 1; i <= countOrder; i++) {
            checkDirectory(web1tDir + "/" + i + "gms");
            web1tEachCountFiles[i - 1] = web1tDir + "/" + i + "gms/" + i + "gm-0000";
            if (i == 1) {
                web1tEachCountFiles[i - 1] = web1tDir + "/" + i + "gms/vocab_cs";
            }
        }

        SRILMAdapter sriLM = new SRILMAdapter(sSpliter);
        SRILMAdapter.VERBOSE = false;
        sriLM.INTEMP = "acl.txt";
        sriLM.setDeleteTemp(false);
        web1TSRILM = sriLM;
    }

    private static boolean checkDirectory(String dir) throws IOException {
        System.out.println("Checking " + dir + " directory...");
        File file = new File(dir);
        if (file.exists() && clean) {
            System.out.println("Deleting directory...");
            FileUtils.deleteDirectory(file);
        } else if (!clean && file.exists()) {
            System.out.println("Direcotry exists");
            return false;
        }
        if (!file.mkdirs()) {
            System.out.println("Cannot create directory " + dir + ", Program will not exit.");
            System.exit(0);
        }
        return true;
    }

    private static void index(int i, String paperId, String content) throws InterruptedException, IOException {
        if (agg != null) agg.addPlaintextDocument(i, paperId, content);
    }

    private static void paperNgram(String paperId, String content, StanfordNLPSSplit sSpliter) throws IOException {
        if (paperNgramDir == null) return;
        SRILMAdapter sriLM = new SRILMAdapter(sSpliter);
        SRILMAdapter.VERBOSE = false;
        sriLM.setDeleteTemp(true);
        sriLM.INTEMP = "paper.temp.in";
        sriLM.OUTTEMP = "paper.temp.out";
        sriLM.setDiscount("wbdiscount");
        sriLM.addFormatedTextToInputFile(content);
        String outLMFile = paperNgramDir + "/" + paperId + ".arpa.lm";
        String countLMFile = paperNgramDir + "/" + paperId + ".count";
        sriLM.ngramCount(null, 3, null, null, null, null, countLMFile, outLMFile, null);
        ARPALMWrapper model = new ARPALMWrapper(outLMFile, false, false);
        LmReaders.writeLmBinary(model.getLm(),
                outLMFile.replaceFirst("arpa.lm", "barpa.lm"));
        File f = new File(outLMFile);
        f.deleteOnExit();
        f = new File(countLMFile);
        f.deleteOnExit();
    }

    private static void ngram(String content) throws IOException {
        if (corpusSRILM != null) corpusSRILM.addFormatedTextToInputFile(content);
    }


    private static void web1TNgram(String content) throws IOException {
        if (web1TSRILM != null) web1TSRILM.addFormatedTextToInputFile(content);
    }

    private static void finalizeIndex() throws IOException {
        if (agg == null) return;
        agg.close();
        File file = new File(indexDir);
        System.out.println("Creating index ID map...");
        HashMap<Integer, String> map = new HashMap<Integer, String>();
        Directory directory = FSDirectory.open(file);
        DirectoryReader currentReader = DirectoryReader.open(directory);
        for (int i = 0; i < currentReader.maxDoc(); i++) {
            Document doc = currentReader.document(i);
            String aclId = doc.getField("title").stringValue();
            map.put(i, aclId);
        }
        currentReader.close();
        ObjectOutputStream oss = new ObjectOutputStream(
                new BufferedOutputStream(new FileOutputStream(indexDir
                        + ".map.bin")));
        Output output = new Output(oss);
        Kryo kryo = new Kryo();
        kryo.writeObject(output, map);
        output.flush();
        output.close();
        oss.close();
    }

    private static void finalizeNgram() throws IOException {
        if (corpusSRILM == null) return;

        String outLMFile = ngramDir + "/papers.arpa.lm";
        System.out.println("Building corpus LM...");
        corpusSRILM.ngramCount(null, lmOrder, null, null, null, ngramDir + "/papers.vocab", ngramDir
                + "/papers.count", outLMFile, null);
        ARPALMWrapper model = new ARPALMWrapper(outLMFile, false, false);
        System.out.println("Writing binary file...");
        LmReaders.writeLmBinary(model.getLm(),
                outLMFile.replaceFirst("arpa.lm", "barpa.lm"));

        System.out.println("Counting ngram...");
        corpusSRILM.setDiscount(null);
        corpusSRILM.setDeleteTemp(true);
        corpusSRILM.ngramCount(null, countOrder, null, null, null, null, ngramDir
                + "/papers.true.count", null, eachNgramCountFiles);
        formatCountFiles();
    }

    private static void formatCountFiles() throws IOException {
        System.out.println("Making Google ngram dir...");
        long[] maxCounts = new long[countOrder];
        String[] maxNgrams = new String[countOrder];
        NGramUtils.makeGoogleDirectory(eachNgramCountFiles, maxCounts, maxNgrams);
        System.out.println("Reading Corpus Data...");
        // We cannot use the same WordIndexer with Google LM because of the start and end sentence symbols...
        NgramMapWrapper<String, LongRef> ngramMap = LmReaders.readNgramMapFromGoogleNgramDir(ngramDir, true);
        System.out.println("Writing Ngram map and word indexer...");
        IOUtils.writeObjFileHard(ngramDir + "/papers.widx", ngramMap.getWordIndexer());
        NgramMapCountWrapper wrapper = new NgramMapCountWrapper(ngramMap.getNgramMap(), ngramMap.getWordIndexer(), maxCounts, maxNgrams);
        IOUtils.writeObjFileHard(ngramDir + "/papers.bmap", wrapper);
    }

    private static void finalizeWeb1TNgram() throws IOException {
        if (web1TSRILM == null) return;
        System.out.println("Counting ngram for Web1T...");
        web1TSRILM.setDiscount(null);
        web1TSRILM.setDeleteTemp(true);
        web1TSRILM.ngramCount(null, countOrder, null, null, null, null, web1tDir
                + "/papers.true.count", null, web1tEachCountFiles);

        System.out.println("Reading Google Web1T...");
        StupidBackoffLm<String> googleLM = LmReaders.readGoogleLmBinary(web1TBLM, web1TVocab);
        long[] web1TMaxCounts = new long[countOrder];
        String[] web1TMaxNgrams = new String[countOrder];
        makeWeb1TCountFiles(web1tEachCountFiles, googleLM, web1TMaxCounts, web1TMaxNgrams);
        NgramMapWrapper<String, LongRef> web1TNgramMap = LmReaders.readNgramMapFromGoogleNgramDir(web1tDir, true);
        System.out.println("Writing Web1T Ngram map and word indexer...");
        IOUtils.writeObjFileHard(ngramDir + "/web1t_papers.widx", web1TNgramMap.getWordIndexer());
        NgramMapCountWrapper web1TWrapper = new NgramMapCountWrapper(web1TNgramMap.getNgramMap(), web1TNgramMap.getWordIndexer(), web1TMaxCounts, web1TMaxNgrams);
        IOUtils.writeObjFileHard(ngramDir + "/web1t_papers.bmap", web1TWrapper);

    }

    private static void makeWeb1TCountFiles(String[] ngramCountFiles, StupidBackoffLm<String> googleLM, long[] outMaxCounts, String[] outMaxNgrams) throws IOException {

        ArrayList<StringLongPair> pairs = new ArrayList<StringLongPair>();
        for (int i = 0; i < ngramCountFiles.length; i++) {
            InputFileManager inputFileManager = new InputFileManager(web1tEachCountFiles[i]);
            BufferedWriter bufferedWriter = new BufferedWriter(
                    new OutputStreamWriter(
                            new GZIPOutputStream(new FileOutputStream(ngramCountFiles[i] + ".gz"))
                    ));
            String line;
            while ((line = inputFileManager.readLine()) != null) {
                if (line.equals("")) continue;
                String[] parts = line.split("\t");
                String word = parts[0];
                long count = getLMCount(googleLM, word);
                if (count > outMaxCounts[i] && !word.contains("<s>") && !word.contains("</s>")) {
                    outMaxCounts[i] = count;
                    outMaxNgrams[i] = word;
                }
                if (i == 0) {
                    pairs.add(new StringLongPair(word, count));
                } else {
                    bufferedWriter.write(word + "\t" + count + "\n");
                }
            }
            if (i == 0) {
                Collections.sort(pairs, StringLongPair.comparator(true));
            }
            for (StringLongPair slp : pairs) {
                bufferedWriter.write(slp.getStringElement() + "\t" + slp.getLongElement() + "\n");
            }
            inputFileManager.close();
            bufferedWriter.close();
        }

    }

    private static long getLMCount(StupidBackoffLm<String> googleLM, String ngram) {
        String[] grams = ngram.split(" ");
        int unk = googleLM.getWordIndexer().getIndexPossiblyUnk(googleLM.getWordIndexer().getUnkSymbol());
        int[] index = new int[grams.length];
        for (int i = 0; i < grams.length; i++) {
            String gram = grams[i];
            index[i] = googleLM.getWordIndexer().getIndexPossiblyUnk(gram);
            if (index[i] == unk) return 1l;
        }
        long count = googleLM.getRawCount(index, 0, index.length);
        if (count <= 0) return 1;
        return count;
    }

    private static void deleteNgramCountTempFiles() throws IOException {
        if (web1TBLM != null && web1TVocab != null) {
            File file = new File(ngramDir + "/web1t");
            FileUtils.deleteDirectory(file);
        }
        if (corpusSRILM == null) return;
        for (int i = 1; i <= countOrder; i++) {
            File file = new File(ngramDir + "/" + i + "gms");
            FileUtils.deleteDirectory(file);
        }
        File file = new File(ngramDir + "/papers.count");
        FileUtils.deleteQuietly(file);
    }

    public static boolean processOptions(String[] args) {
        Options options = new Options();
        options.addOption("paper", true, "Paper file in json format");
        options.addOption("indexDir", true, "[Either] Build Lucene index (paper is a document) into <arg> directory");
        options.addOption("ngramDir", true, "[Either] Build ngram language model of all paper into <arg> directory");
        options.addOption("paperNgramDir", true, "[Either] Build ngram language model for each paper into <arg> directory");
        //options.addOption("buckDir", true, "[Either] Write a text file for each paper (as used in the system) into <arg> directory");
        options.addOption("citation", true, "[Optional] Citation context file");
        options.addOption("clean", false, "[Optional] Delete previous data before building");
        options.addOption("web1TBLM", true, "[Either] Web1T Binary LM. This will make Web1T count file.");
        options.addOption("web1TVocab", true, "[Either] Web1T Vocab File. This will make Web1T count file.");

        CommandLineParser parser = new GnuParser();
        boolean argsNotOk = false;

        try {
            CommandLine cmd = parser.parse(options, args);
            dataFile = cmd.getOptionValue("paper");
            citationFile = cmd.getOptionValue("citation");
            indexDir = cmd.getOptionValue("indexDir");
            ngramDir = cmd.getOptionValue("ngramDir");
            paperNgramDir = cmd.getOptionValue("paperNgramDir");
            web1TBLM = cmd.getOptionValue("web1TBLM");
            web1TVocab = cmd.getOptionValue("web1TVocab");
            //buckDir = cmd.getOptionValue("buckDir");
            clean = cmd.hasOption("clean");

            if (dataFile == null ||
                    (indexDir == null && ngramDir == null && paperNgramDir == null)) {
                argsNotOk = true;
            }
        } catch (ParseException e) {
            argsNotOk = true;
        }
        if (argsNotOk) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("BuildResources [options]", options);
        }
        return !argsNotOk;
    }

    private static String normalize(ArrayList<TokenSpan> sentences) {
        StringBuilder sb = new StringBuilder();
        for (TokenSpan span : sentences) {
            sb.append(
                    TokenUtil.tokens2Str(
                            TokenUtil.cleanTokens(
                                    TokenUtil.normalize(
                                            span.getTokens(), true, true),
                                    false, null))).append('\n');
        }
        return sb.toString();
    }

    private static String join(ArrayList<TokenSpan> sentences) {
        StringBuilder sb = new StringBuilder();
        for (TokenSpan span : sentences) {
            sb.append(
                    TokenUtil.tokens2Str(TokenUtil.cleanTokens(span.getTokens(),
                            false, null))).append('\n');
        }
        return sb.toString();
    }
}
