package edu.northwestern.websail.kpe.data.kp;

import edu.northwestern.websail.text.data.Token;

import java.util.List;

/**
 * @author NorThanapon
 * @since 11/20/14
 */
public class ScoreKeyPhrase extends KeyPhrase {

    private double logTF;
    private double idf;
    private double searchScore;

    public ScoreKeyPhrase(String source, String phrase, List<? extends Token> normalizedTokens) {
        super(source, phrase, normalizedTokens);
    }

    public double getLogTF() {
        return logTF;
    }

    public void setLogTF(double logTF) {
        this.logTF = logTF;
    }

    public double getIdf() {
        return idf;
    }

    public void setIdf(double idf) {
        this.idf = idf;
    }

    public double getSearchScore() {
        return searchScore;
    }

    public void setSearchScore(double searchScore) {
        this.searchScore = searchScore;
    }

    public void mergeScores(ScoreKeyPhrase kp) {
        this.logTF = (this.logTF + kp.logTF) / 2;
        this.idf = (this.idf + kp.idf) / 2;
        this.searchScore = (this.searchScore + kp.searchScore) / 2;
    }

    public double getTFIDF() {
        return (this.logTF + 1) * this.idf;
    }

    @Override
    public String toString() {
        return super.toString() + "\t" + getLogTF() + "\t" + getIdf();
    }
}
