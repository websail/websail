package edu.northwestern.websail.kpe.main.sandbox;

import cc.mallet.pipe.SerialPipes;
import cc.mallet.types.InstanceList;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.gs.collections.impl.map.mutable.UnifiedMap;
import edu.northwestern.websail.core.io.InputFileManager;
import edu.northwestern.websail.text.corpus.data.lucene.IndexAggregator;
import edu.northwestern.websail.text.corpus.data.lucene.collector.DocIdCollector;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.tokenizer.SUPTBTokenizer;
import edu.northwestern.websail.text.topics.MalletHelper;
import edu.northwestern.websail.text.topics.MalletInferencerWrapper;
import edu.northwestern.websail.text.util.TokenUtil;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author NorThanapon
 * @since 7/28/15
 */
public class TestTopicDist {
    public static void main(String[] args) throws IOException, ClassNotFoundException, ParseException {
        SUPTBTokenizer tokenizer = new SUPTBTokenizer();
        UnifiedMap<String, double[]> topicMap = MalletHelper.readTopicDistributionMap(args[0]);
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(args[1]));
        Input input = new Input(ois);
        Kryo kryo = new Kryo();
        @SuppressWarnings("unchecked") HashMap<Integer, String> luceneDocIdMap = kryo.readObject(input, HashMap.class);
        File indexFile = new File(args[2]);
        Directory directory = FSDirectory.open(indexFile);
        DirectoryReader reader = DirectoryReader.open(directory);
        IndexSearcher searcher = new IndexSearcher(reader);
        WhitespaceAnalyzer analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);
        QueryParser qp = new QueryParser(Version.LUCENE_40, IndexAggregator.DOC_CONTENT_FIELD, analyzer);
        qp.setDefaultOperator(QueryParser.Operator.AND);
        qp.setPhraseSlop(0);

        MalletInferencerWrapper wrapper = MalletHelper.loadTopicInferencer(args[3]);
        SerialPipes pipes = new SerialPipes(wrapper.getPipes());
        InstanceList instances = new InstanceList(pipes);


        InputFileManager inMgr = new InputFileManager(args[4]);
        String line;
        while ((line = inMgr.readLine()) != null) {
            String[] parts = line.split("\t");
            String phrase = parts[2];
            tokenizer.initialize(phrase);
            List<Token> tokens = tokenizer.getAllTokens();
            tokens = TokenUtil.normalize(tokens, true, true);
            String queryTerm = QueryParser.escape(
                    TokenUtil.tokens2Str(tokens));
            Query q = qp.parse("\"" + queryTerm + "\"");
            DocIdCollector collector = new DocIdCollector();
            searcher.search(q, collector);
            ArrayList<Integer> docIds = collector.getDocIds();
            double[] avgTopics = averageDists(topicMap, luceneDocIdMap, docIds);
            double[] comTopics = combineDists(topicMap, luceneDocIdMap, docIds);
            instances.addThruPipe(MalletHelper.makeInstance(phrase, null));
            double[] probs = wrapper.getInferencer().getSampledDistribution(instances.get(0), 1000, 5, 100);
            instances.remove(0);
            System.out.println(entropy(probs) + "\t" + entropy(avgTopics) + "\t" + entropy(comTopics));
        }
    }

    public static double[] averageDists(UnifiedMap<String, double[]> topicMap, HashMap<Integer, String> luceneDocIdMap, ArrayList<Integer> docIds) {
        double[] result = null;
        for (Integer docId : docIds) {
            String paperId = luceneDocIdMap.get(docId);
            double[] topics = topicMap.get(paperId);
            if (result == null) {
                result = new double[topics.length];
            }
            add(result, topics);
        }
        if (result == null) return null;
        scale(result, 1 / (double) docIds.size());
        return result;
    }

    public static void add(double[] toAdded, double[] adding) {
        for (int i = 0; i < toAdded.length; i++) {
            toAdded[i] = toAdded[i] + adding[i];
        }
    }

    public static void scale(double[] toScaled, double scaling) {
        for (int i = 0; i < toScaled.length; i++) {
            toScaled[i] = toScaled[i] * scaling;
        }
    }

    public static double[] combineDists(UnifiedMap<String, double[]> topicMap, HashMap<Integer, String> luceneDocIdMap, ArrayList<Integer> docIds) {
        double[] result = null;
        for (Integer docId : docIds) {
            String paperId = luceneDocIdMap.get(docId);
            double[] topics = topicMap.get(paperId);
            if (result == null) {
                result = new double[topics.length];
            }
            mult(result, topics);
        }
        if (result == null) return null;
        normalize(result);
        return result;
    }

    public static void mult(double[] toMulted, double[] multing) {
        for (int i = 0; i < toMulted.length; i++) {
            toMulted[i] = toMulted[i] + Math.log10(multing[i]);
        }
    }

    public static void normalize(double[] toNormalized) {
        double sum = 0.0;
        for (double d : toNormalized) {
            sum += Math.pow(10, d);
        }
        for (int i = 0; i < toNormalized.length; i++) {
            toNormalized[i] = Math.pow(10, toNormalized[i]) / sum;
        }
        //System.out.println(Arrays.toString(toNormalized));
    }

    public static double entropy(double[] probs) {
        if (probs == null) return -1;
        double result = 0;
        for (double prob : probs) {
            if (prob == 0)
                continue;
            result -= prob * (Math.log(prob) / Math.log(2));
        }
        return result;
    }
}
