package edu.northwestern.websail.kpe.data.kp;

import edu.northwestern.websail.text.data.Token;

import java.util.Collections;
import java.util.HashSet;

/**
 * @author NorThanapon
 * @since 11/20/14
 */
public class KPGroup {
    private HashSet<ScoreKeyPhrase> kps;
    private double sumLogTF;
    private double sumIDF;
    private String source;
    private int size;
    private double sortScore;
    private double searchScore;

    public KPGroup(String source) {
        this.source = source;
        this.sumIDF = 0.0;
        this.sumLogTF = 0.0;
        size = 0;
        this.kps = new HashSet<ScoreKeyPhrase>();
        this.sortScore = 0.0;
        this.searchScore = 0.0;
    }

    public HashSet<ScoreKeyPhrase> getKps() {
        return kps;
    }

    public void setKps(HashSet<ScoreKeyPhrase> kps) {
        this.kps = kps;
    }

    public double getAvgLogTF() {
        if (kps.size() == 0)
            return 0.0;
        return this.sumLogTF / size;
    }

    public double getAvgIDF() {
        if (kps.size() == 0)
            return 0.0;
        return this.sumIDF / size;
    }

    public double getMaxLogTF() {
        if (kps.size() == 0)
            return 0.0;
        double max = 0.0;
        for (ScoreKeyPhrase kp : this.kps) {
            max = Math.max(kp.getLogTF(), max);
        }
        return max;
    }

    public double getMaxIDF() {
        if (kps.size() == 0)
            return 0.0;
        double max = 0.0;
        for (ScoreKeyPhrase kp : this.kps) {
            max = Math.max(kp.getIdf(), max);
        }
        return max;
    }

    public double getAvgTFIDF() {
        if (kps.size() == 0)
            return 0.0;
        double tfidf = 0.0;
        for (ScoreKeyPhrase kp : this.kps) {
            tfidf += kp.getTFIDF();
        }
        return tfidf / kps.size();
    }

    public double getMaxTFIDF() {
        if (kps.size() == 0)
            return 0.0;
        double max = 0.0;
        for (ScoreKeyPhrase kp : this.kps) {
            max = Math.max(kp.getTFIDF(), max);
        }
        return max;
    }

    public double getSumTFIDF() {
        return (this.getSumLogTF() + 1) * this.getSumIDF();
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return source + "\t" + kps + "\t" + this.getAvgLogTF() + "\t"
                + this.getAvgIDF();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof KPGroup) {
            KPGroup kpg = (KPGroup) o;
            return this.kps.equals(kpg.kps);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.kps.hashCode();
    }

    public void add(ScoreKeyPhrase key) {
        kps.add(key);
        size++;
        this.sumLogTF += key.getLogTF();
        this.sumIDF += key.getIdf();
    }

    public void addAll(KPGroup newKPs) {
        for (ScoreKeyPhrase newKP : newKPs.getKps()) {
            if (!this.getKps().contains(newKP)) {
                this.add(newKP);
            }
        }

    }

    public double getSumLogTF() {
        return sumLogTF;
    }

    public void setSumLogTF(double sumLogTF) {
        this.sumLogTF = sumLogTF;
    }

    public double getSumIDF() {
        return sumIDF;
    }

    public void setSumIDF(double sumIDF) {
        this.sumIDF = sumIDF;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public double getSortScore() {
        return sortScore;
    }

    public void setSortScore(double sortScore) {
        this.sortScore = sortScore;
    }

    public Double getSearchScore() {
        return searchScore;
    }

    public void setSearchScore(Double searchScore) {
        this.searchScore = searchScore;
    }

    public ScoreKeyPhrase getLongestKP() {
        ScoreKeyPhrase maxLenKP = null;
        int maxLen = 0;
        for (ScoreKeyPhrase kp : this.getKps()) {
            if (kp.getNormalizedPhrase().length() > maxLen) {
                maxLen = kp.getNormalizedPhrase().length();
                maxLenKP = kp;
            }
        }
        return maxLenKP;
    }

    public ScoreKeyPhrase getMaxSearchScoreKP() {
        ScoreKeyPhrase maxKP = null;
        double maxScore = 0;
        for (ScoreKeyPhrase kp : this.getKps()) {
            if (kp.getSearchScore() > maxScore) {
                maxScore = kp.getSearchScore();
                maxKP = kp;
            }
        }
        return maxKP;
    }


    public ScoreKeyPhrase getMaxTFIDFKP() {
        ScoreKeyPhrase maxKP = null;
        double maxTFIDF = 0;
        for (ScoreKeyPhrase kp : this.getKps()) {
            if (kp.getTFIDF() > maxTFIDF) {
                maxTFIDF = kp.getTFIDF();
                maxKP = kp;
            }
        }
        return maxKP;
    }

    public HashSet<String> bow() {
        HashSet<String> bow = new HashSet<String>();
        for (KeyPhrase kp : this.kps) {
            for (Token t : kp.getTokens()) {
                String parts[] = t.getText().split("-");
                Collections.addAll(bow, parts);
            }
        }
        return bow;
    }

    public HashSet<String> bowExact() {
        HashSet<String> bow = new HashSet<String>();
        for (KeyPhrase kp : this.kps) {
            for (Token t : kp.getTokens()) {
                bow.add(t.getText());
            }
        }
        return bow;
    }

    public ScoreKeyPhrase getRepresentative() {
        return this.getLongestKP();
    }

}
