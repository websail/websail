package edu.northwestern.websail.kpe.out.dedup;

import edu.northwestern.websail.kpe.data.kp.KeyPhrase;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.util.LevenshteinDistance;
import edu.northwestern.websail.text.util.TokenUtil;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class KPDedupMetric {

    final HashMap<String, HashSet<Integer>> searchCache;
    final DirectoryReader reader;
    private final IndexSearcher searcher;
    private final QueryParser qp;
    HashMap<Integer, String> indexDocIDMap;

    public KPDedupMetric(DirectoryReader reader) {
        this.reader = reader;
        searchCache = new HashMap<String, HashSet<Integer>>();
        searcher = new IndexSearcher(reader);
        WhitespaceAnalyzer analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);
        qp = new QueryParser(Version.LUCENE_40, "article", analyzer);
        qp.setDefaultOperator(QueryParser.Operator.AND);
        qp.setPhraseSlop(0);

    }

    public KPDedupMetric(DirectoryReader reader, HashMap<Integer, String> indexDocIDMap) {
        this(reader);
        this.indexDocIDMap = indexDocIDMap;
    }

    public double bowCosine(HashSet<String> bow1, HashSet<String> bow2)
            throws IOException {
        double norm1 = 0.0;
        double norm2 = 0.0;
        double dot = 0.0;
        for (String b1 : bow1) {
            double idf = this.idf(b1);
            norm1 += (idf * idf);
            if (bow2.contains(b1)) {
                dot += idf * idf;
            }
        }
        for (String b2 : bow2) {
            double idf = this.idf(b2);
            norm2 += (idf * idf);
        }
        return dot / (Math.sqrt(norm2) * Math.sqrt(norm1));
    }

    public double[] editDistance(KeyPhrase k1, KeyPhrase k2) {
        int distance = LevenshteinDistance.computeLevenshteinDistance(
                k1.getNormalizedPhrase(), k2.getNormalizedPhrase());
        int max = Math.max(k1.getNormalizedPhrase().length(), k2
                .getNormalizedPhrase().length());
        return new double[]{distance, distance / (double) max};
    }

    public double[] hitsRatio(KeyPhrase k1, KeyPhrase k2)
            throws ParseException, IOException {
        HashSet<Integer> q1Docs = this.searchDoc(k1);
        HashSet<Integer> q2Docs = this.searchDoc(k2);
        int size1 = q1Docs.size();
        q1Docs.retainAll(q2Docs);
        q2Docs.removeAll(q1Docs);
        int union = size1 + q2Docs.size();
        int intersect = q1Docs.size();
        return new double[]{intersect, union, (double) intersect / union};
    }

    public double[] jaccardSimilarity(KeyPhrase k1, KeyPhrase k2) {
        HashSet<String> h1 = new HashSet<String>();
        HashSet<String> h2 = new HashSet<String>();
        for (Token t : k1.getTokens()) {
            h1.add(t.getText().toLowerCase());
        }
        for (Token t : k2.getTokens()) {
            h2.add(t.getText().toLowerCase());
        }
        int maxSize = Math.max(h1.size(), h2.size());
        int sizeh1 = h1.size();
        h1.retainAll(h2);
        h2.removeAll(h1);
        int union = sizeh1 + h2.size();
        int intersection = h1.size();
        return new double[]{intersection, union, (double) intersection / union, (double) intersection / maxSize};
    }

    public boolean match(KeyPhrase k1, KeyPhrase k2) {
        return k1.getNormalizedPhrase().equals(k2.getNormalizedPhrase());
    }

    public boolean match(KeyPhrase k1, KeyPhrase k2, int firstNChars) {
        int firstNCharK1 = firstNChars;
        if (firstNCharK1 > k1.getNormalizedPhrase().length()) {
            firstNCharK1 = k1.getNormalizedPhrase().length();
        }
        int firstNCharK2 = firstNChars;
        if (firstNCharK2 > k2.getNormalizedPhrase().length()) {
            firstNCharK2 = k2.getNormalizedPhrase().length();
        }
        String firstNK1 = k1.getNormalizedPhrase().substring(0, firstNCharK1);
        String firstNK2 = k2.getNormalizedPhrase().substring(0, firstNCharK2);
        if (firstNCharK1 == firstNCharK2) {
            return firstNK1.equals(firstNK2);
        } else if (firstNCharK1 > firstNCharK2) {
            return firstNK1.contains(firstNK2);
        } else {
            return firstNK2.contains(firstNK1);
        }
    }

    public boolean isContaining(KeyPhrase k1, KeyPhrase k2) {
        return k1.getSigPhrase().contains(k2.getSigPhrase());
    }

    public boolean isContained(KeyPhrase k1, KeyPhrase k2) {
        return k2.getSigPhrase().contains(k1.getSigPhrase());
    }

    public boolean overlap(KeyPhrase k1, KeyPhrase k2) {
        ArrayList<String> h1 = new ArrayList<String>();
        ArrayList<String> h2 = new ArrayList<String>();
        for (Token s : k1.getTokens()) {
            h1.add(s.getText().toLowerCase());
        }
        for (Token s : k2.getTokens()) {
            h2.add(s.getText().toLowerCase());
        }
        int i = 0, j = 0, k = -1;
        while (i < h1.size() && j < h2.size()) {
            String a = h1.get(i);
            String b = h2.get(j);
            if (a.equalsIgnoreCase(b)) {
                j++;
                if (k == -1)
                    k = i;
            }
            i++;
        }
        if (j != 0 && j + k == i) {
            return true;
        }
        i = 0;
        j = 0;
        k = -1;
        while (i < h2.size() && j < h1.size()) {
            String a = h2.get(i);
            String b = h1.get(j);
            if (a.equalsIgnoreCase(b)) {
                j++;
                if (k == -1)
                    k = i;
            }
            i++;
        }
        return j != 0 && j + k == i;
    }

    private HashSet<Integer> searchDoc(KeyPhrase k) throws ParseException,
            IOException {
        String searchTerms = TokenUtil.tokens2Str(k.getTokens());
        String phrase = QueryParser.escape(searchTerms);
        if (searchCache.containsKey(phrase)) {
            return new HashSet<Integer>(searchCache.get(phrase));
        }
        Query q = qp.parse("\"" + phrase + "\"");
        ScoreDoc[] qHits = searcher.search(q, null, 30000).scoreDocs;
        HashSet<Integer> qDocs = new HashSet<Integer>();
        for (ScoreDoc d : qHits) {
            qDocs.add(d.doc);
        }
        searchCache.put(phrase, qDocs);
        return new HashSet<Integer>(qDocs);

    }

    private double idf(String term) throws IOException {
        int df = reader.docFreq(new Term("article", term));
        return Math.log10((double) reader.numDocs() / (df + 1));
    }

    public double searchScore(HashSet<String> bow, String paperId) throws IOException, ParseException {
        StringBuilder sb = new StringBuilder();
        for (String a : bow) {
            sb.append(a);
            sb.append(' ');
        }
        String phrase = sb.toString();
        Query q = qp.parse(QueryParser.escape(phrase));
        ScoreDoc[] qHits = searcher.search(q, null, 30000).scoreDocs;
        for (ScoreDoc d : qHits) {
            String pId = this.indexDocIDMap.get(d.doc);
            if (pId.equals(paperId)) return d.score;
        }
        return 0.0;
    }

}
