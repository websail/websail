package edu.northwestern.websail.kpe.data.kp;

import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.util.TokenUtil;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * @author NorThanapon
 * @since 11/20/14
 */
public class KeyPhrase implements Comparable<KeyPhrase> {
    private final String source;
    private final String phrase;
    private final List<? extends Token> tokens;
    private final String normalizedPhrase;
    private String type;

    public KeyPhrase(String source, String phrase, List<? extends Token> normalizedTokens) {
        this.source = source;
        this.phrase = phrase;
        this.tokens = normalizedTokens;
        this.normalizedPhrase = TokenUtil.tokens2Str(normalizedTokens);
    }

    public String getSource() {
        return source;
    }

    public String getPhrase() {
        return phrase;
    }

    public List<? extends Token> getTokens() {
        return tokens;
    }

    public String getNormalizedPhrase() {
        return normalizedPhrase;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSigPhrase() {
        return "_" + normalizedPhrase.replaceAll(" ", "_") + "_";
    }

    @Override
    public int compareTo(@NotNull KeyPhrase o) {
        return this.normalizedPhrase.compareTo(o.getNormalizedPhrase());
    }

    @Override
    public String toString() {
        // return this.normalizedPhrase;
        return this.phrase.replaceAll("  ", " ");
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof KeyPhrase) {
            KeyPhrase kp = (KeyPhrase) o;
            return kp.normalizedPhrase.equals(this.normalizedPhrase);
        } else if (o instanceof Phrase) {
            Phrase phrase = (Phrase) o;
            return this.normalizedPhrase.equals(TokenUtil.tokens2Str(phrase.getSpan().getTokens()));
        } else {
            return this.normalizedPhrase.equals(o.toString());
        }

    }

    @Override
    public int hashCode() {
        return this.normalizedPhrase.hashCode();
    }

    public HashSet<String> bow() {
        HashSet<String> bow = new HashSet<String>();
        for (Token t : this.getTokens()) {
            String parts[] = t.getText().split("-");
            for (int i = 0; i < parts.length; i++) {
                if (parts[i].length() >= 5 && (parts[i].endsWith("or") || parts[i].endsWith("er"))) {
                    parts[i] = parts[i].substring(0, parts[i].length() - 2);
                }
            }
            Collections.addAll(bow, parts);
        }

        return bow;
    }

    public HashSet<String> bowExact() {
        HashSet<String> bow = new HashSet<String>();
        for (Token t : this.getTokens()) {
            bow.add(t.getText());
        }

        return bow;
    }
}
