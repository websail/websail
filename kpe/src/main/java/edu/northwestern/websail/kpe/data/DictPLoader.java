package edu.northwestern.websail.kpe.data;

import edu.northwestern.websail.core.io.InputFileManager;
import edu.northwestern.websail.core.pipeline.config.PEVResourceLoader;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.tokenizer.Tokenizer;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 1/8/15
 */
public class DictPLoader {
    public static HashMap<String, Integer> dictionary(PipelineEnvironment env, Properties config, Boolean global, Boolean put) throws Exception {
        String patternFile = config.getProperty("kpe.dict.patternFile");
        String tonyFile = config.getProperty("kpe.dict.mentionFile");
        Tokenizer tokenizer = (Tokenizer) env.getPoolResource(PEVResourceType.TOKENIZER_POOL, 0, PEVResourceType.TOKENIZER);
        if (tokenizer == null) {
            PEVResourceLoader.loadResource(env, PEVResourceType.TOKENIZER_POOL, config, global);
            tokenizer = (Tokenizer) env.getPoolResource(PEVResourceType.TOKENIZER_POOL, 0, PEVResourceType.TOKENIZER);
        }

        HashMap<String, Integer> map = new HashMap<String, Integer>();

        InputFileManager ptF = new InputFileManager(patternFile);
        String line;
        while ((line = ptF.readLine()) != null) {
            tokenizer.initialize(line);
            List<Token> tokens = tokenizer.getAllTokens();
            tokens = TokenUtil.normalize(tokens, true, true);
            String key = TokenUtil.tokens2Str(tokens);
            Integer v = map.get(key);
            if (v == null) v = 0;
            v = v | 1;
            map.put(key, v);
        }
        ptF.close();

        InputFileManager mntF = new InputFileManager(tonyFile);
        while ((line = mntF.readLine()) != null) {
            String[] parts = line.split("\t");
            line = parts[0];
            tokenizer.initialize(line);
            List<Token> tokens = tokenizer.getAllTokens();
            tokens = TokenUtil.normalize(tokens, true, true);
            String key = TokenUtil.tokens2Str(tokens);
            Integer v = map.get(key);
            if (v == null) v = 0;
            v = v | 2;
            map.put(key, v);
        }
        mntF.close();

        if (put) {
            PEVResourceLoader.setResource(env, PEVResourceType.KPE_DICT, global, map);
        }
        return map;
    }
}
