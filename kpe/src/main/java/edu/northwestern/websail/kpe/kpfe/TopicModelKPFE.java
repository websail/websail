package edu.northwestern.websail.kpe.kpfe;

import com.gs.collections.impl.map.mutable.UnifiedMap;
import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.core.pipeline.unit.par.ParallelPLManager;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.corpus.data.lucene.IndexAggregator;
import edu.northwestern.websail.text.corpus.data.lucene.collector.DocIdCollector;
import edu.northwestern.websail.text.util.TokenUtil;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 7/31/15
 */
@PipelineConfigInfo(
        requiredResources = {PEVResourceType.INDEX_READER,
                PEVResourceType.INDEX_ID_MAP,
                PEVResourceType.TOPIC_DIST_MAP}
)
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class}
)
public class TopicModelKPFE extends KPFeatureExtractor {

    // XXX: This KPFE searches index for documents matching phrases. It is similar to FrequencyKPFE, so some refactor needed.

    public static final Feature topicEntropyF = new Feature("topic", "entropy", Feature.Type.NUMBER);
    public static final UnifiedMap<String, Double> entropyCache = new UnifiedMap<String, Double>();
    private static final Feature[] features = {topicEntropyF};
    private static final Object lock = new Object();
    private static boolean registered = false;
    private IndexSearcher searcher;
    private QueryParser qp;
    private UnifiedMap<String, double[]> topicMap;
    private HashMap<Integer, String> indexIdMap;

    public TopicModelKPFE() {
        curValues = new double[features.length];
        Arrays.fill(curValues, Instance.MISSING);
    }

    public static double entropy(double[] probs) {
        if (probs == null) return 0;
        double result = 0;
        for (double prob : probs) {
            if (prob == 0)
                continue;
            result -= prob * (Math.log(prob) / Math.log(2));
        }
        return result;
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config)) return false;
        DirectoryReader reader = (DirectoryReader) env.getResource(PEVResourceType.INDEX_READER);
        if (reader == null) {
            PipelineEnvironment.logResourceNotFound(PEVResourceType.INDEX_READER, logger);
            return false;
        }
        searcher = new IndexSearcher(reader);
        WhitespaceAnalyzer analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);
        qp = new QueryParser(Version.LUCENE_40, IndexAggregator.DOC_CONTENT_FIELD, analyzer);
        qp.setDefaultOperator(QueryParser.Operator.AND);
        qp.setPhraseSlop(0);
        //noinspection unchecked
        indexIdMap = (HashMap) env.getResource(PEVResourceType.INDEX_ID_MAP);
        if (indexIdMap == null) {
            PipelineEnvironment.logResourceNotFound(PEVResourceType.INDEX_ID_MAP, logger);
            return false;
        }
        //noinspection unchecked
        topicMap = (UnifiedMap) env.getResource(PEVResourceType.TOPIC_DIST_MAP);
        if (topicMap == null) {
            PipelineEnvironment.logResourceNotFound(PEVResourceType.TOPIC_DIST_MAP, logger);
            return false;
        }
        ParallelPLManager.countDownLatch(env, ParallelPLManager.ALL_LATCH_1);
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        ParallelPLManager.awaitLatch(env, ParallelPLManager.ALL_LATCH_1);
        if (threadId == 0) {
            entropyCache.clear();
        }
        return super.close(env, config);
    }

    @Override
    protected void extract(Instance<Phrase> phraseInstance) throws Exception {
        String phrase = QueryParser.escape(
                TokenUtil.tokens2Str(phraseInstance.getSource().getSpan().getTokens()));
        curValues[0] = entropy(phrase);
    }

    private double entropy(String phrase) throws ParseException, IOException {
        if (entropyCache.containsKey(phrase)) return entropyCache.get(phrase);
        Query q = qp.parse("\"" + phrase + "\"");
        DocIdCollector collector = new DocIdCollector();
        searcher.search(q, collector);
        ArrayList<Integer> docIds = collector.getDocIds();
        double[] dist = averageDists(docIds);
        double entropy = entropy(dist);
        synchronized (lock) {
            entropyCache.put(phrase, entropy);
        }
        return entropy;
    }

    private double[] averageDists(ArrayList<Integer> docIds) {
        double[] result = null;
        for (Integer docId : docIds) {
            String paperId = indexIdMap.get(docId);
            double[] topics = topicMap.get(paperId);
            if (result == null) {
                result = new double[topics.length];
            }
            add(result, topics);
        }
        if (result == null) return null;
        scale(result, 1 / (double) docIds.size());
        return result;
    }

    private void add(double[] toAdded, double[] adding) {
        for (int i = 0; i < toAdded.length; i++) {
            toAdded[i] = toAdded[i] + adding[i];
        }
    }

    private void scale(double[] toScaled, double scaling) {
        for (int i = 0; i < toScaled.length; i++) {
            toScaled[i] = toScaled[i] * scaling;
        }
    }

    @Override
    protected Feature[] features() {
        return features;
    }

    @Override
    protected boolean initForBuck(PipelineEnvironment env, S2PaperBuck prev) {
        return true;
    }

    @Override
    protected boolean initForUnit(BuckUnit unit) {
        return true;
    }

    @Override
    protected boolean register() {
        return registered;
    }

    @Override
    protected void registered() {
        registered = true;
    }
}
