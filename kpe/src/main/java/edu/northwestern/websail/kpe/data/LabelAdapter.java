package edu.northwestern.websail.kpe.data;

import edu.northwestern.websail.core.io.InputFileManager;
import edu.northwestern.websail.kpe.data.kp.KeyPhrase;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.tokenizer.Tokenizer;
import edu.northwestern.websail.text.util.TokenUtil;
import org.tartarus.snowball.ext.PorterStemmer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author NorThanapon
 * @since 11/21/14
 */
public class LabelAdapter {
    public static HashMap<String, ArrayList<KeyPhrase>> readLabelFile(
            String file, Tokenizer tokenizer, String fixedPaperId) {
        HashMap<String, ArrayList<KeyPhrase>> labels = new HashMap<String, ArrayList<KeyPhrase>>();
        InputFileManager inMgr = new InputFileManager(file);
        String line;
        while ((line = inMgr.readLine()) != null) {

            String[] parts = line.split("\t");
            String paperId = fixedPaperId;
            if (paperId == null) {
                paperId = parts[0];
            }
            if (!labels.containsKey(paperId)) {
                labels.put(paperId, new ArrayList<KeyPhrase>());
            }
            if (parts.length > 1) {
                String taskKeys = parts[1];
                labels.get(paperId).addAll(
                        readKeys(paperId, taskKeys, "problem", tokenizer));
            }
            if (parts.length > 2) {
                String methodKeys = parts[2];
                labels.get(paperId).addAll(
                        readKeys(paperId, methodKeys, "method", tokenizer));
            }
            if (parts.length > 3) {
                String resourceKeys = parts[3];

                labels.get(paperId).addAll(
                        readKeys(paperId, resourceKeys, "resource", tokenizer));
            }
        }
        inMgr.close();
        return labels;
    }

    private static ArrayList<KeyPhrase> readKeys(String source, String keys,
                                                 String type, Tokenizer tokenizer) {
        ArrayList<KeyPhrase> kps = new ArrayList<KeyPhrase>();
        String[] parts = keys.split(", ");
        for (String k : parts) {
            tokenizer.initialize(k);
            List<Token> tokens = tokenizer.getAllTokens();
            tokens = TokenUtil.normalize(tokens, true, true);
            KeyPhrase kp = new KeyPhrase(source, k, tokens);
            kp.setType(type);
            kps.add(kp);
        }

        return kps;
    }

    public static HashMap<String, ArrayList<KeyPhrase>> readSemEvalLabel(String file, boolean stem, Tokenizer tokenizer) {
        HashMap<String, ArrayList<KeyPhrase>> labels = new HashMap<String, ArrayList<KeyPhrase>>();
        InputFileManager inMgr = new InputFileManager(file);
        String line;
        while ((line = inMgr.readLine()) != null) {
            String parts[] = line.split(" : ");
            String id = parts[0];
            labels.put(id, readSemEvalKeys(id, parts[1], stem, tokenizer));
        }
        return labels;
    }

    private static ArrayList<KeyPhrase> readSemEvalKeys(String source, String keys, boolean stem, Tokenizer tokenizer) {
        ArrayList<KeyPhrase> kps = new ArrayList<KeyPhrase>();
        String[] parts = keys.split(",|\\+");
        for (String k : parts) {
            tokenizer.initialize(k);
            List<Token> tokens = tokenizer.getAllTokens();
            if (stem) tokens = TokenUtil.normalize(tokens, true, true);
            KeyPhrase kp = new KeyPhrase(source, k, tokens);
            kps.add(kp);
        }

        return kps;
    }

    public static HashMap<String, ArrayList<KeyPhrase>> readTsvLabelFile(
            String file, Tokenizer tokenizer, boolean lowercase, boolean stem) {
        HashMap<String, ArrayList<KeyPhrase>> labels = new HashMap<String, ArrayList<KeyPhrase>>();
        InputFileManager inMgr = new InputFileManager(file);
        String line;
        while ((line = inMgr.readLine()) != null) {
            String[] parts = line.split("\t");
            String paperId = parts[0];

            if (!labels.containsKey(paperId)) {
                labels.put(paperId, new ArrayList<KeyPhrase>());
            }

            for (int idx = 1; idx < parts.length; idx++) {
                if (parts[idx].isEmpty())
                    continue;
                String kpString = parts[idx];//.replaceAll("-" , " ");
                tokenizer.initialize(kpString);
                List<Token> tokens = tokenizer.getAllTokens();
                tokens = TokenUtil.normalize(tokens, lowercase, stem, new PorterStemmer());
                KeyPhrase kp = new KeyPhrase(paperId, kpString, tokens);
                labels.get(paperId).add(kp);
            }
        }
        inMgr.close();
        return labels;
    }
}
