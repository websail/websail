package edu.northwestern.websail.kpe.data.paper;

import java.util.ArrayList;

/**
 * @author NorThanapon
 */
@Deprecated
public class ParsedPaper {
    /*
	 * 
	 * parcitDocument keys [u'figures', u'footnotes', u'sections', u'captions']
	 * figures -> a list of string footnotes -> a list of string captions -> a
	 * list of string sections -> a list of section -> [u'body', u'header',
	 * u'genericHeader'] all text...
	 */

    private ArrayList<String> figures;
    private ArrayList<String> footnotes;
    private ArrayList<String> captions;
    private ArrayList<ParsedPaperSection> sections;

    public ParsedPaper() {
    }

    public ArrayList<String> getFigures() {
        return figures;
    }

    public void setFigures(ArrayList<String> figures) {
        this.figures = figures;
    }

    public ArrayList<String> getFootnotes() {
        return footnotes;
    }

    public void setFootnotes(ArrayList<String> footnotes) {
        this.footnotes = footnotes;
    }

    public ArrayList<String> getCaptions() {
        return captions;
    }

    public void setCaptions(ArrayList<String> captions) {
        this.captions = captions;
    }

    public ArrayList<ParsedPaperSection> getSections() {
        return sections;
    }

    public void setSections(ArrayList<ParsedPaperSection> sections) {
        this.sections = sections;
    }

}
