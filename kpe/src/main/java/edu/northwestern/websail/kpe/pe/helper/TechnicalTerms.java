package edu.northwestern.websail.kpe.pe.helper;

import java.util.regex.Pattern;

/**
 * @author NorThanapon
 * @since 11/19/14
 */
public class TechnicalTerms {
    private static final String NOUN_PATTERN = "(N[NPS]{1,5})|VBG";
    private static final String ADJECTIVE_PATTERN = "J[JRS]{1,4}";
    private static final String NUMERIC_PATTERN = "CD";

    private static final String TECH_TERM_SIMPLE_PATTERN = "(?:(?:" + ADJECTIVE_PATTERN +
            "|" + NOUN_PATTERN + ")\\s*)+(?:" + NOUN_PATTERN + "|" + NUMERIC_PATTERN + ")";
    public static final Pattern simple = Pattern.compile(TECH_TERM_SIMPLE_PATTERN);
    private static final String TECH_TERM_COMPLEX_PATTERN = "(?:" + ADJECTIVE_PATTERN + "|" + NOUN_PATTERN +
            "\\s*)*" + NOUN_PATTERN + "\\s+" +
            "IN (?:" + TECH_TERM_SIMPLE_PATTERN + "|" + NUMERIC_PATTERN + ")*";
    public static final Pattern complex = Pattern.compile(TECH_TERM_COMPLEX_PATTERN);
}

