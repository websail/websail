package edu.northwestern.websail.kpe.main.sandbox;

import edu.berkeley.nlp.lm.WordIndexer;
import edu.berkeley.nlp.lm.io.IOUtils;
import edu.northwestern.websail.text.ngram.NgramMapCountWrapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author NorThanapon
 * @since 2/27/15
 */
public class NgramMap {
    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws IOException {
        String dir = "/websail/common/keyphrase_extraction/data/semeval10/ngrams/corpus_ncase_stem";
        String widxFile = dir + "/papers.widx";
        String countFile = dir + "/papers.bmap";
        String w1tCountFile = dir + "/web1t_papers.bmap";
        String w1tWidxFile = dir + "/web1t_papers.widx";

        WordIndexer<String> wIdx = (WordIndexer<String>) IOUtils.readObjFileHard(widxFile);
        NgramMapCountWrapper countMap = (NgramMapCountWrapper) IOUtils.readObjFileHard(countFile);
        WordIndexer<String> web1tWIdx = (WordIndexer<String>) IOUtils.readObjFileHard(w1tWidxFile);
        NgramMapCountWrapper w1tCountMap = (NgramMapCountWrapper) IOUtils.readObjFileHard(w1tCountFile);

        countMap.setWordIndexer(wIdx);
        w1tCountMap.setWordIndexer(web1tWIdx);

        System.out.println("Count Map Top");
        for (int i = 0; i < countMap.getMaxOrder(); i++) {
            System.out.println(countMap.getTopNgram(i + 1) + ": " + countMap.getTopCountNgram(i + 1));
        }

        System.out.println("W1T Count Map Top");
        for (int i = 0; i < w1tCountMap.getMaxOrder(); i++) {
            System.out.println(w1tCountMap.getTopNgram(i + 1) + ": " + w1tCountMap.getTopCountNgram(i + 1));
        }

        System.out.println("Count Map Unigram count: " + countMap.getUnigramCount());
        System.out.println("W1T Count Map Unigram count: " + w1tCountMap.getUnigramCount());

        while (true) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter ngram...\n");
            String testText = br.readLine();
            String[] ngrams = testText.trim().split(" ");
            System.out.println("Corpus Count: " + countMap.getCount(ngrams));
            System.out.println("Corpus Count: " + w1tCountMap.getCount(ngrams));
            if (testText.equals("<exit>")) break;

        }
    }
}
