package edu.northwestern.websail.kpe.main.postprocessing;

import edu.northwestern.websail.core.io.InputFileManager;
import edu.northwestern.websail.core.io.OutputFileManager;
import edu.northwestern.websail.core.util.TimerUtil;
import edu.northwestern.websail.kpe.data.kp.KeyPhrase;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.tokenizer.StanfordNLPTokenizer;
import edu.northwestern.websail.text.tokenizer.Tokenizer;
import edu.northwestern.websail.text.util.TokenUtil;
import org.tartarus.snowball.ext.PorterStemmer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author NorThanapon
 * @since 1/30/15
 */
public class FormatRankSemEval {
    public static void main(String[] args) {
        TimerUtil timer = new TimerUtil();
        timer.start();
        String dataFile = args[0];//"/Users/NorThanapon/Documents/current/semeval/semeval_ranked_key_phrases.txt";
        //String outputFile = null;
        String answerFile = args[1];//"/Users/NorThanapon/Documents/current/semeval/test.combined.stem.final";
        String outputFile = args[2];
//        Options options = new Options();
//        options.addOption("in", true, "Input key file in AI2 format (TSV)");
//        options.addOption("out", true, "Output key file in SemEval format (CSV)");
//        options.addOption("ids", true, "[Optional] SemEval answer file used to remove keys from other papers");
//
//        CommandLineParser parser = new GnuParser();
//        boolean argsNotOk = false;
//        try {
//            CommandLine cmd = parser.parse(options, args);
//            dataFile = cmd.getOptionValue("in");
//            answerFile = cmd.getOptionValue("ids");
//            outputFile = cmd.getOptionValue("out");
//            if (dataFile == null || outputFile == null) {
//                argsNotOk = true;
//            }
//        } catch (ParseException e) {
//            argsNotOk = true;
//        }
//        if (argsNotOk) {
//            HelpFormatter formatter = new HelpFormatter();
//            formatter.printHelp("WritePaperTokens [options]", options);
//            System.exit(0);
//        }
        String line;
        StanfordNLPTokenizer tokenizer = new StanfordNLPTokenizer();
        HashMap<String, ArrayList<KeyPhrase>> keys = readRankResult(dataFile, tokenizer, Integer.MAX_VALUE);
        InputFileManager inMgr = new InputFileManager(answerFile);
        OutputFileManager outMgr = new OutputFileManager(outputFile);
        while ((line = inMgr.readLine()) != null) {
            if (line.trim().equals("")) continue;
            String id = line.split(" : ")[0];
            ArrayList<KeyPhrase> keyphrases = keys.get(id);
            StringBuilder sb = new StringBuilder();
            sb.append(id).append(" : ");
            int count = 0;
            if (keyphrases != null) {
                for (KeyPhrase kp : keyphrases) {
                    sb.append(kp.getNormalizedPhrase()).append(',');
                    count++;
                    //if (count > pos * keyphrases.size()) break;
                    if (count >= 15) break;
                }
            }
            outMgr.println(sb.substring(0, sb.length() - 1));
        }
        inMgr.close();
        outMgr.close();
    }

    private static HashMap<String, ArrayList<KeyPhrase>> readRankResult(String file, Tokenizer tokenizer, int top) {
        HashMap<String, ArrayList<KeyPhrase>> results = new HashMap<String, ArrayList<KeyPhrase>>();
        InputFileManager inMgr = new InputFileManager(file);
        String line;
        while ((line = inMgr.readLine()) != null) {
            String[] parts = line.split("\t");
            String paperId = parts[0];

            if (!results.containsKey(paperId)) {
                results.put(paperId, new ArrayList<KeyPhrase>());
            } else if (results.get(paperId).size() > top) {
                continue;
            }
            String kpString = parts[2];//.replaceAll("-" , " ");
            tokenizer.initialize(kpString);
            List<Token> tokens = tokenizer.getAllTokens();
            tokens = TokenUtil.normalize(tokens, true, false, new PorterStemmer());
            KeyPhrase kp = new KeyPhrase(paperId, kpString, tokens);
            results.get(paperId).add(kp);
        }
        inMgr.close();
        return results;
    }
}
