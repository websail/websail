package edu.northwestern.websail.kpe.main.preprocessing;

import edu.northwestern.websail.core.io.OutputFileManager;
import edu.northwestern.websail.core.util.TimerUtil;
import edu.northwestern.websail.kpe.data.ACLPaperAdapter;
import edu.northwestern.websail.kpe.data.paper.Paper;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.tokenizer.StanfordNLPSSplit;
import edu.northwestern.websail.text.tokenizer.StanfordNLPTokenizer;
import edu.northwestern.websail.text.util.StopWords;
import edu.northwestern.websail.text.util.TokenUtil;
import org.apache.commons.cli.*;

import java.util.Map;
import java.util.Set;

/**
 * @author NorThanapon
 * @since 1/16/15
 */
public class WritePaperTokens {
    public static void main(String[] args) {
        TimerUtil timer = new TimerUtil();
        timer.start();
        String dataFile = null;//"/websail/common/keyphrase_extraction/data/papers/acl.json";
        String citationFile = null;//"/websail/common/keyphrase_extraction/data/citations/citation_context.txt";
        String outputDir = null;//"/websail/common/keyphrase_extraction/data/papers/acl_paper_txt";
        String outputFile = null;//"/websail/common/keyphrase_extraction/data/papers/acl.txt";
        boolean useStopword = false;
        Options options = new Options();
        options.addOption("paper", true, "Paper file in json format");
        options.addOption("writeDir", true, "[Either] Write a text file for each paper into <arg> directory");
        options.addOption("writeFile", true, "[Either] Write a text file of all paper into <arg> file");
        options.addOption("citation", true, "[Optional] Citation context file, default no citation context");
        options.addOption("useStopWords", false, "[Optional] Set to use stop words, default does not use stop words");
        CommandLineParser parser = new GnuParser();
        boolean argsNotOk = false;
        try {
            CommandLine cmd = parser.parse(options, args);
            dataFile = cmd.getOptionValue("paper");
            citationFile = cmd.getOptionValue("citation");
            outputDir = cmd.getOptionValue("writeDir");
            outputFile = cmd.getOptionValue("writeFile");
            if (cmd.hasOption("useStopWords")) {
                useStopword = true;
            }
            if (dataFile == null ||
                    (outputDir == null && outputFile == null)) {
                argsNotOk = true;
            }
        } catch (ParseException e) {
            argsNotOk = true;
        }
        if (argsNotOk) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("WritePaperTokens [options]", options);
            System.exit(0);
        }

        //XXX: Hack tokenizer and sentence spliter type
        StanfordNLPTokenizer tokenizer = new StanfordNLPTokenizer();
        StanfordNLPSSplit sSpliter = new StanfordNLPSSplit(true);

        System.out.println("Loading paper...");
        ACLPaperAdapter adapter;
        if (citationFile != null) {
            adapter = new ACLPaperAdapter(dataFile, tokenizer, true,
                    citationFile, true);
        } else {
            adapter = new ACLPaperAdapter(dataFile, tokenizer, true, true);
        }
        System.out.println("Processing and writing file(s)...");
        OutputFileManager outFile = null;
        if (outputFile != null)
            outFile = new OutputFileManager(outputFile);
        for (Map.Entry<String, Paper> p : adapter.getPapers().entrySet()) {
            Paper paper = p.getValue();
            StringBuilder sb = new StringBuilder();
            sb.append(tokenize(paper.getTitle(), sSpliter, useStopword))
                    .append(tokenize(paper.getPaperAbstract(), sSpliter, useStopword))
                    .append(tokenize(paper.getBodyText(), sSpliter, useStopword))
                    .append(tokenize(paper.getCitancesAsParagraph(), sSpliter, useStopword));
            if (outputDir != null) {
                OutputFileManager output = new OutputFileManager(outputDir + "/" + p.getKey() + ".txt");
                output.print(sb.toString());
                output.close();
            }
            if (outFile != null) outFile.print(sb.toString());
        }
        if (outFile != null) outFile.close();
        timer.end();
        System.out.println("Done " + timer.totalTime());
    }

    private static String tokenize(String str, StanfordNLPSSplit sSplit, boolean useStopwords) {
        Set<String> stopwords = null;
        if (useStopwords) stopwords = StopWords._words;
        if (str == null) return "\n";
        StringBuilder sb = new StringBuilder();

        for (TokenSpan span : sSplit.sentenceSplit(str)) {
            sb.append(
                    TokenUtil.tokens2Str(
                            TokenUtil.cleanTokens(
                                    TokenUtil.normalize(
                                            span.getTokens(), true, true),
                                    false, stopwords))).append('\n');
        }
        return sb.toString();
    }
}
