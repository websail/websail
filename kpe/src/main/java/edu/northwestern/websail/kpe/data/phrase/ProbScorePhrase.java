package edu.northwestern.websail.kpe.data.phrase;

import edu.northwestern.websail.text.data.TokenSpan;

/**
 * @author NorThanapon
 */
public class ProbScorePhrase extends Phrase {
    double corpusNgram;
    double paperNgram;
    double corpusSCP;
    double paperSCP;
    double significantScore;
    String location;

    public ProbScorePhrase() {
        super();
    }

    public ProbScorePhrase(String surfaceForm,
                           String source, TokenSpan span) {
        super(surfaceForm, source, span);
    }

    @Override
    public String toString() {
        return String.valueOf(span.getTokens()) + "(" + corpusSCP() + ")" + "\n";
    }

    public ProbScorePhrase span(TokenSpan span) {
        this.span = span;
        return this;
    }

    public TokenSpan span() {
        return span;
    }

    public ProbScorePhrase text(String text) {
        this.surfaceForm = text;
        return this;
    }

    public String text() {
        return this.surfaceForm;
    }

    public ProbScorePhrase location(String location) {
        this.location = location;
        return this;
    }

    public String location() {
        return this.location;
    }

    public ProbScorePhrase corpusNgram(double score) {
        this.corpusNgram = score;
        return this;
    }

    public double corpusNgram() {
        return this.corpusNgram;
    }

    public double nCorpusNgram() {
        return this.corpusNgram / this.span.getTokens().size();
    }

    public ProbScorePhrase paperNgram(double score) {
        this.paperNgram = score;
        return this;
    }

    public double paperNgram() {
        return this.paperNgram;
    }

    public double nPaperNgram() {
        return this.paperNgram / this.span.getTokens().size();
    }

    public ProbScorePhrase corpusSCP(double score) {
        this.corpusSCP = score;
        return this;
    }

    public double corpusSCP() {
        return this.corpusSCP;
    }

    public ProbScorePhrase paperSCP(double score) {
        this.paperSCP = score;
        return this;
    }

    public double paperSCP() {
        return this.paperSCP;
    }

    public ProbScorePhrase sigScore(double score) {
        this.significantScore = score;
        return this;
    }

    public double sigScore() {
        return this.significantScore;
    }
}