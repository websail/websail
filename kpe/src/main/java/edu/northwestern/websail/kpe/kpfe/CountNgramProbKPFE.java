package edu.northwestern.websail.kpe.kpfe;

import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.ngram.NgramMapCountWrapper;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.Arrays;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 1/7/15
 * <p/>
 * Similar to NgramProbKPFE
 * This replace ngram langauge model with count to compute probabilities.
 * There is NO support for paper level probablity
 */
@PipelineConfigInfo(
        requiredSettings = {"ngram.countWordIndexer=*"},
        requiredResources = {PEVResourceType.CORPUS_NGRAM_COUNT}
)
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class}
)
public class CountNgramProbKPFE extends KPFeatureExtractor {
    private static final double MIN_PROB = -13.;
    private static final Feature[] features = {NgramProbKPFE.corpusPrF,
            NgramProbKPFE.normCorpusPrF, NgramProbKPFE.gapScoreF};
    private static boolean registered = false;
    NgramMapCountWrapper nGramCountMap;
    private double[] topNgramProbs;

    public CountNgramProbKPFE() {
        curValues = new double[features.length];
        Arrays.fill(curValues, Instance.MISSING);
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config)) return false;

        nGramCountMap = (NgramMapCountWrapper) env.getResource(PEVResourceType.CORPUS_NGRAM_COUNT);
        if (nGramCountMap == null) {
            PipelineEnvironment.logResourceNotFound(PEVResourceType.NGRAM_COUNT, logger);
            return false;
        }
        topNgramProbs = new double[nGramCountMap.getMaxOrder()];
        for (int i = 0; i < nGramCountMap.getMaxOrder(); i++) {
            topNgramProbs[i] = prob(nGramCountMap.getTopNgram(i + 1));
        }
        return true;
    }

    @Override
    protected void extract(Instance<Phrase> phraseInstance) throws Exception {
        String[] ngram = TokenUtil.tokens2StrArr(phraseInstance.getSource().getSpan().getTokens());
        double corpusNgram = this.prob(TokenUtil.tokens2Str(phraseInstance.getSource().getSpan().getTokens()));
        if (corpusNgram < MIN_PROB) {
            corpusNgram = MIN_PROB;
        }
        int n = ngram.length;
        if (n > topNgramProbs.length) {
            n = topNgramProbs.length;
        }
        double normCorpusNgram = corpusNgram - topNgramProbs[n - 1];
        double gapScore = this.computeGapSCP(ngram, phraseInstance.getSource().getBefore().getText(),
                phraseInstance.getSource().getAfter().getText());

        curValues[0] = corpusNgram;
        curValues[1] = normCorpusNgram;
        curValues[2] = gapScore;
    }

    @Override
    protected Feature[] features() {
        return features;
    }

    @Override
    protected boolean initForBuck(PipelineEnvironment env, S2PaperBuck prev) {
        return true;
    }

    @Override
    protected boolean initForUnit(BuckUnit unit) {
        return true;
    }

    private double computeGapSCP(String[] ngram, String before, String after) {
        if (before == null || after == null)
            return Instance.MISSING;
        double[] unigramProbs = new double[ngram.length];
        for (int i = 0; i < ngram.length; i++) {
            unigramProbs[i] = prob(ngram[i]);
        }
        double internalSCP = this.computeInternalSCP(ngram, unigramProbs);
        double externalSCP = this.computeExternalSCP(ngram, unigramProbs,
                before, after);
        return internalSCP - externalSCP;
    }

    private double computeInternalSCP(String[] ngram, double[] unigramProbs) {
        double internalSCP = 0.0;
        int count = 0;
        if (ngram.length == 1) {
            internalSCP = unigramProbs[0];
            count++;
        } else {
            for (int i = 0; i < ngram.length; i++) {
                if (i == 0)
                    continue;
                String[] subPhrase = Arrays.copyOfRange(ngram, i - 1, i + 1);
                internalSCP += (prob(join(subPhrase)) * 2
                        - unigramProbs[i - 1] - unigramProbs[i]);
                count++;
            }
        }
        internalSCP = internalSCP / count;

        return internalSCP;
    }

    private double computeExternalSCP(String[] ngram, double[] unigramProbs,
                                      String before, String after) {
        double unigramProbBefore = prob(before);
        double unigramProbAfter = prob(after);
        String[] head = new String[2];
        String[] tail = new String[2];
        head[0] = before;
        head[1] = ngram[0];
        tail[0] = ngram[ngram.length - 1];
        tail[1] = after;
        int count = 0;
        double externalSCP = 0.0;
        if (!before.equals("<s>")) {
            externalSCP += (prob(join(head)) * 2
                    - unigramProbBefore - unigramProbs[0]);
            count++;
        }
        if (!after.equals("</s>")) {
            externalSCP += (prob(join(tail)) * 2
                    - unigramProbAfter - unigramProbs[ngram.length - 1]);
            count++;
        }
        if (count == 0)
            return 0.0;
        return externalSCP / count;
    }

    private double prob(String ngram) {
        long count = nGramCountMap.getCount(ngram.split(" "));
        double p = Math.log10((double) count / (double) nGramCountMap.getUnigramCount());
        if (p < MIN_PROB) return MIN_PROB;
        return p;
    }

    private String join(String[] ngram) {
        StringBuilder builder = new StringBuilder();
        for (String s : ngram) {
            builder.append(s).append(' ');
        }
        return builder.substring(0, builder.length() - 1);
    }

    @Override
    protected boolean register() {
        return registered;
    }

    @Override
    protected void registered() {
        registered = true;
    }
}
