package edu.northwestern.websail.kpe.data.paper;

import edu.northwestern.websail.ds.tuple.IntegerPair;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author csbhagav on 8/1/14.
 */
@Deprecated
public class Citance {
    public static final String nameYearPatternStr = "[\\(\\[][^\\\\)^\\\\][A-Za-z]*,?[ ]?[0-9]{2,4}?[\\)\\]]";
    public static final String etAlPatternStr = "[\\(\\[]([^\\)^\\]]*?(et\\.? al\\.?).*?)[\\)\\]]";
    public static final String yearPattern = "[\\(\\[][0-9]{1,3}[\\)\\]]";
    public static final String multiAuthorPatternStr = "[\\(\\[]((\\b[A-z ]+\\b[,][ ][0-9]{2,4}([a-z])?[;]?)[ ]?)+[\\)\\]]";

    public static final Pattern citationPattern = Pattern.compile(nameYearPatternStr + "|" + etAlPatternStr + "|" +
            yearPattern + "|" + multiAuthorPatternStr);
    IntegerPair offsets;
    IntegerPair listOffsets;
    String sentence;
    String section;
    String precedinNounPhrase;
    Integer citationCount;

    public Citance() {
    }

    public Citance(IntegerPair offsets, String sentence) {
        this.offsets = offsets;
        this.sentence = sentence;

        citationCount = 0;
        Matcher m = citationPattern.matcher(this.sentence);
        while (m.find()) {
            citationCount++;
        }
        if (citationCount == 0) {
            citationCount = 100;
        }
    }

    public IntegerPair getOffsets() {
        return offsets;
    }

    public void setOffsets(IntegerPair offsets) {
        this.offsets = offsets;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public IntegerPair getListOffsets() {
        return listOffsets;
    }

    public void setListOffsets(IntegerPair listOffsets) {
        this.listOffsets = listOffsets;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getPrecedinNounPhrase() {
        return precedinNounPhrase;
    }

    public void setPrecedinNounPhrase(String precedinNounPhrase) {
        this.precedinNounPhrase = precedinNounPhrase;
    }

    public Integer getCitationCount() {
        return citationCount;
    }

    public void setCitationCount(Integer citationCount) {
        this.citationCount = citationCount;
    }
}
