package edu.northwestern.websail.kpe.out.dedup;


import edu.northwestern.websail.kpe.data.kp.KPGroup;
import edu.northwestern.websail.kpe.data.kp.KeyPhrase;
import org.apache.lucene.queryparser.classic.ParseException;

import java.io.IOException;

public class KPDedupProcessor {
    final KPDedupMetric metric;
    private boolean debug = false;

    public KPDedupProcessor(KPDedupMetric metric) {
        this.metric = metric;
    }

    public boolean sameGroup(KeyPhrase kp1, KeyPhrase kp2)
            throws ParseException, IOException {
        if (metric.match(kp1, kp2)) {
            if (debug)
                System.out.println("String Match!");
            return true;
        }
        int maxLen = Math.max(kp1.getNormalizedPhrase().length(), kp2
                .getNormalizedPhrase().length());
        if (maxLen >= 6) {
            double[] ed = metric.editDistance(kp1, kp2);
            if (ed[0] <= 3 && metric.match(kp1, kp2, 4)) {
                if (debug)
                    System.out.println("Edit Distance Match!");
                return true;
            }
        }


//        double[] hitRatio = metric.hitsRatio(kp1, kp2);
//        if (hitRatio[2] > .6 && hitRatio[1] > 5
//                && (kp1.getTokens().size() > 1 || kp2.getTokens().size() > 1)) {
//            if (debug) System.out.println("Hit Ratio Match!");
//            return true;
//        }
//
//        if (hitRatio[2] > .001 && metric.isContaining(kp1, kp2)) {
//            if (debug) System.out.println("Containing and Hit Ratio Match!");
//            return true;
//        }
//
//
//        double bowCosine = metric.bowCosine(
//                kp1.bow(),
//                kp2.bow());
//
//        if (bowCosine > 0.95) {
//            if (debug) System.out.println("BOW Match!");
//            return true;
//        }
//
//        if (hitRatio[2] > .2 && bowCosine > 0.9) {
//            if (debug) System.out.println("BOW Cosine, and Hit Ratio Match!");
//            return true;
//        }
//        double[] jacc = metric.jaccardSimilarity(kp1, kp2);
//        if (bowCosine > 0.9 && jacc[3] > 0.6) {
//            if (debug) System.out.println("Jaccard, and BOW Cosine Match!");
//            return true;
//        }
//
//        if (hitRatio[2] > .048 && bowCosine > 0.7 && jacc[3] > 0.6) {
//            if (debug) System.out.println("Jaccard, BOW Cosine, and Hit Ratio Match!");
//            return true;
//        }
//        if (debug) System.out.println("Not Match :(");
        return false;
    }

    public boolean sameGroup(KeyPhrase newKP, KPGroup group)
            throws ParseException, IOException {
        for (KeyPhrase kp : group.getKps()) {
            if (!sameGroup(kp, newKP))
                return false;
        }
        return true;
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }
}
