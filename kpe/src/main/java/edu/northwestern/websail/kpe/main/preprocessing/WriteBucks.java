package edu.northwestern.websail.kpe.main.preprocessing;

import com.google.gson.Gson;
import edu.northwestern.websail.core.io.OutputFileManager;
import edu.northwestern.websail.core.util.TimerUtil;
import edu.northwestern.websail.kpe.data.ACLPaperAdapter;
import edu.northwestern.websail.kpe.data.paper.Paper;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.tokenizer.StanfordNLPSSplit;
import edu.northwestern.websail.text.tokenizer.StanfordNLPTokenizer;
import edu.northwestern.websail.text.util.TokenUtil;
import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * @author NorThanapon
 * @since 1/16/15
 *
 * Write content of papers in JSON format. The content is the input to KPE
 *
 */
public class WriteBucks {
    public static void main(String[] args) throws IOException {
        TimerUtil timer = new TimerUtil();
        timer.start();
        String dataFile = null;//"/websail/common/keyphrase_extraction/data/papers/acl.json";
        String citationFile = null;//"/websail/common/keyphrase_extraction/data/citations/citation_context.txt";
        String outputDir = null;//"/websail/common/keyphrase_extraction/data/papers/acl_paper_txt";

        Options options = new Options();
        options.addOption("paper", true, "Paper file in json format");
        options.addOption("writeDir", true, "[Either] Write a text file for each paper into <arg> directory");
        options.addOption("citation", true, "[Optional] Citation context file, default no citation context");

        CommandLineParser parser = new GnuParser();
        boolean argsNotOk = false;
        try {
            CommandLine cmd = parser.parse(options, args);
            dataFile = cmd.getOptionValue("paper");
            citationFile = cmd.getOptionValue("citation");
            outputDir = cmd.getOptionValue("writeDir");

            if (dataFile == null || outputDir == null) {
                argsNotOk = true;
            }
        } catch (ParseException e) {
            argsNotOk = true;
        }
        if (argsNotOk) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("WritePaperTokens [options]", options);
            System.exit(0);
        }

        //XXX: Hack tokenizer and sentence spliter type
        StanfordNLPTokenizer tokenizer = new StanfordNLPTokenizer();
        StanfordNLPSSplit sSpliter = new StanfordNLPSSplit(true);

        System.out.println("Initializing output directory...");
        checkDirectory(outputDir);
        System.out.println("Loading paper...");
        ACLPaperAdapter adapter;
        if (citationFile != null) {
            adapter = new ACLPaperAdapter(dataFile, tokenizer, true,
                    citationFile, true);
        } else {
            adapter = new ACLPaperAdapter(dataFile, tokenizer, true, true);
        }
        System.out.println("Processing and writing file(s)...");
        Gson gson = new Gson();
        for (Map.Entry<String, Paper> p : adapter.getPapers().entrySet()) {
            Paper paper = p.getValue();
            OutputFileManager output = new OutputFileManager(outputDir + "/" + p.getKey() + ".json");
            OutPaper op = new OutPaper(tokenize(paper.getTitle(), sSpliter),
                    p.getKey(),
                    tokenize(paper.getCitancesAsParagraph(), sSpliter),
                    tokenize(ACLPaperAdapter.cleanCitance(paper.getBodyText()), sSpliter),
                    tokenize(ACLPaperAdapter.cleanAbstract(paper.getPaperAbstract(), false), sSpliter));
            String json = gson.toJson(op);
            output.println(json);
            output.close();
        }
        timer.end();
        System.out.println("Done " + timer.totalTime());
    }

    private static String tokenize(String str, StanfordNLPSSplit sSplit) {
        if (str == null) return "\n";
        StringBuilder sb = new StringBuilder();

        for (TokenSpan span : sSplit.sentenceSplit(str)) {
            sb.append(TokenUtil.tokens2Str(span.getTokens())).append('\n');
        }
        return sb.toString();
    }

    public static void checkDirectory(String dir) throws IOException {
        System.out.println("Checking " + dir + " directory...");
        File file = new File(dir);
        if (file.exists()) {
            System.out.println("Deleting directory...");
            FileUtils.deleteDirectory(file);
        }
        if (!file.mkdirs()) {
            System.out.println("Cannot create directory " + dir + ", Program will not exit.");
            System.exit(0);
        }
    }

    private static class OutPaper {
        private final String title;
        private final String id;
        private final String citations;
        private final String body;
        private final String abstr;

        public OutPaper(String title, String id, String citations, String body, String abstr) {
            this.title = title;
            this.id = id;
            this.citations = citations;
            this.body = body;
            this.abstr = abstr;
        }

        public String getTitle() {
            return title;
        }

        public String getId() {
            return id;
        }

        public String getCitations() {
            return citations;
        }

        public String getBody() {
            return body;
        }

        public String getAbstr() {
            return abstr;
        }
    }
}
