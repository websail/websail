package edu.northwestern.websail.kpe.out;

import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.classify.result.ClassificationResult;
import edu.northwestern.websail.core.io.OutputFileManager;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.core.pipeline.unit.common.WriteAndConcatFileUnit;

import java.util.ArrayList;

/**
 * @author NorThanapon
 * @since 11/20/14
 */
@PipelineConfigInfo(
        requiredSettings = {"kpe.out.cutPosFile=*"}
)
public class WriteCutPosition extends WriteAndConcatFileUnit {

    public WriteCutPosition() {
    }

    @Override
    protected String outFileConfigKey() {
        return "kpe.out.cutPosFile";
    }

    @SuppressWarnings("unchecked")
    @Override
    protected boolean write(PipelineEnvironment env, Buck prev, OutputFileManager out) {
        String source = prev.getSource();
        BuckUnit unit = prev.unit();
        ArrayList<ClassificationResult> results = unit.get(ClassifyBUT.ClassifyResults.class);
        ClassificationResult result = results.get(0);
        out.println(source + "\t" + result.getClassValue());
        return true;
    }
}
