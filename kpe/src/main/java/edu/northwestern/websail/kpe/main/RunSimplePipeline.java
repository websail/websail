package edu.northwestern.websail.kpe.main;

import edu.northwestern.websail.core.pipeline.PipelineExecutor;
import edu.northwestern.websail.core.pipeline.config.PipelineUnit;
import edu.northwestern.websail.core.util.TimerUtil;
import edu.northwestern.websail.kpe.data.S2PaperAdapter;
import edu.northwestern.websail.kpe.data.S2PaperUtil;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import org.apache.commons.cli.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 8/13/15
 */
public class RunSimplePipeline {

    static final S2PaperAdapter adapter = new S2PaperAdapter();
    static String data;
    static String pipe;
    static String outPFile;
    static String outKPFile;
    static String outDedupKPFile;
    static String outDocDataPrefix;
    public static void main(String[] args) throws Exception {
        TimerUtil timerUtil = new TimerUtil();
        timerUtil.start();
        if (!processOptions(args)) {
            return;
        }
        PipelineExecutor executor = new PipelineExecutor(pipe);
        File dataFile = new File(data);
        if (dataFile.isDirectory()) {
            File[] files = dataFile.listFiles();
            outPFile = executor.getPipelineConfig().getGlobalConfig().getProperty("kpe.out.phraseFile", "ps.txt");
            outKPFile = executor.getPipelineConfig().getGlobalConfig().getProperty("kpe.out.keyphraseFile", "kps.txt");
            outDedupKPFile = executor.getPipelineConfig().getGlobalConfig().getProperty("kpe.out.dedupKeyphraseFile", "dedup.kps.txt");
            outDocDataPrefix = executor.getPipelineConfig().getGlobalConfig().getProperty("kpe.out.docDataFilePrefix", "");
            if (files == null) return;
            int i = 1;
            for (File f : files) {
                if (f.getName().startsWith("_") || f.getName().startsWith(".")) continue;
                updateOutputConfig(executor, f);
                System.out.println("Processing file: " + f.getAbsolutePath());
                System.out.println(i + "/" + files.length);
                runForFile(f.getAbsolutePath(), executor);
                i++;
            }
        } else {
            System.out.println("Processing file: " + data);
            runForFile(data, executor);
        }
    }

    private static void runForFile(String file, PipelineExecutor executor) throws Exception {
        ArrayList<S2PaperBuck> bucks = adapter.makeAllBuck(file, S2PaperUtil.WHOLE_PAPER_LOCATIONS);
        System.out.println("Papers: " + bucks.size());
        if (bucks.size() == 0) {
            System.out.println("No available paper.");
            return;
        }
        executor.parRun(bucks);
        executor.getEnv().getPipelineGlobalOuputs().clear();
    }

    private static void updateOutputConfig(PipelineExecutor executor, File f) {
        updateOutputConfig(executor.getPipelineConfig().getGlobalConfig(), f);
        for (PipelineUnit plUnits : executor.getPlUnits()) {
            updateOutputConfig(plUnits.getUnitConfig(), f);
        }
    }

    private static void updateOutputConfig(Properties config, File f) {
        config.setProperty("kpe.out.phraseFile", f.getName() + "." + outPFile);
        config.setProperty("kpe.out.keyphraseFile", f.getName() + "." + outKPFile);
        config.setProperty("kpe.out.dedupKeyphraseFile", f.getName() + "." + outDedupKPFile);
        config.setProperty("kpe.out.docDataFilePrefix", f.getName() + outDocDataPrefix);

    }

    private static boolean processOptions(String[] args) {
        Options options = new Options();
        options.addOption("paper", true, "Paper file or directory containing files in json format");
        options.addOption("pipe", true, "XML file defining pipeline to run this");
        CommandLineParser parser = new GnuParser();
        boolean argsNotOk = false;

        try {
            CommandLine cmd = parser.parse(options, args);
            data = cmd.getOptionValue("paper");
            pipe = cmd.getOptionValue("pipe");
            if (data == null || pipe == null) {
                argsNotOk = true;
            }
        } catch (ParseException e) {
            argsNotOk = true;
        }
        if (argsNotOk) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("RunSimplePipeline [options]", options);
        }
        return !argsNotOk;
    }
}
