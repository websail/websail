package edu.northwestern.websail.kpe.kpfe;

import edu.northwestern.websail.classify.data.CommonNominal;
import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.S2PaperUtil;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import edu.northwestern.websail.kpe.data.paper.Paper;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @author NorThanapon
 * @since 11/19/14
 */
@PipelineConfigInfo()
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class}
)
public class CitationContextKPFE extends KPFeatureExtractor {

    public static final Feature inCCF = new Feature("citation", "foundInCitationContext",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature matchCCF = new Feature("citation", "matchedInCitationContext",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    private static final Feature[] features = {inCCF, matchCCF};
    private static boolean registered = false;
    private HashMap<String, Double> matchCCFCache;
    private ArrayList<TokenSpan> ccSentences;

    public CitationContextKPFE() {
        curValues = new double[features.length];
        Arrays.fill(curValues, Instance.MISSING);
    }

    @Override
    protected void extract(Instance<Phrase> phraseInstance) throws Exception {
        String surfaceKey = TokenUtil.tokens2Str(phraseInstance.getSource().getSpan().getTokens());
        if (matchCCFCache.containsKey(surfaceKey)) {
            curValues[1] = matchCCFCache.get(surfaceKey);
            return;
        }
        double value = CommonNominal.boolMapFalseValue;
        for (TokenSpan sentence : ccSentences) {
            if (TokenUtil.indexOfTokensKMP(sentence.getTokens(), phraseInstance.getSource().getSpan().getTokens(), 0) != -1) {
                value = CommonNominal.boolMapTrueValue;
            }
        }
        curValues[1] = value;
        matchCCFCache.put(surfaceKey, value);
    }

    @Override
    protected Feature[] features() {
        return features;
    }

    @Override
    protected boolean initForBuck(PipelineEnvironment env, S2PaperBuck prev) {
        ccSentences = new ArrayList<TokenSpan>();
        matchCCFCache = new HashMap<String, Double>();
        for (BuckUnit unit : prev.units()) {
            String location = unit.get(KPEBUT.LocationType.class);
            if (location.equals(S2PaperUtil.LOC_CITANCE)) {
                List<TokenSpan> sentences = unit.get(KPEBUT.Sentences.class);
                for (TokenSpan sentence : sentences) {
                    ccSentences.add(sentence);
                }
            }
        }
        return true;
    }

    @Override
    protected boolean initForUnit(BuckUnit unit) {
        String location = unit.get(KPEBUT.LocationType.class);
        if (location == null) {
            logger.warning("Location of a unit cannot be determined. Some feature values will be MISSING.");
            curValues[0] = Instance.MISSING;
        } else {
            curValues[0] = location.equals(Paper.LOC_CITANCE) ?
                    CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
        }
        return true;
    }

    @Override
    protected boolean register() {
        return registered;
    }

    @Override
    protected void registered() {
        registered = true;
    }
}
