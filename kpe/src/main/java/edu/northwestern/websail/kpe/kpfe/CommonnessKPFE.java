package edu.northwestern.websail.kpe.kpfe;

import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.ngram.NgramMapCountWrapper;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 11/19/14
 */
@PipelineConfigInfo(
        requiredSettings = {"ngram.countWordIndexer=*", "ngram.web1TCountWordIndexer=*"},
        requiredResources = {PEVResourceType.CORPUS_NGRAM_COUNT, PEVResourceType.WEB1T_NGRAM_COUNT}
)
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class}
)
public class CommonnessKPFE extends KPFeatureExtractor {

    public static final Feature corpusCF = new Feature("commonness", "corpus", Feature.Type.NUMBER);
    public static final Feature logCorpusCF = new Feature("commonness", "logCorpus", Feature.Type.NUMBER);
    public static final Feature web1TCF = new Feature("commonness", "web1t", Feature.Type.NUMBER);
    public static final Feature logWeb1TCF = new Feature("commonness", "logweb1t", Feature.Type.NUMBER);
    private static final Feature[] features = {corpusCF, logCorpusCF, web1TCF, logWeb1TCF};
    private static boolean registered = false;
    private NgramMapCountWrapper countMap;
    private NgramMapCountWrapper web1TCountMap;

    public CommonnessKPFE() {
        curValues = new double[features.length];
        Arrays.fill(curValues, Instance.MISSING);
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config)) return false;
        countMap = (NgramMapCountWrapper) env
                .getResource(PEVResourceType.CORPUS_NGRAM_COUNT);
        if (countMap == null) {
            PipelineEnvironment.logResourceNotFound(PEVResourceType.CORPUS_NGRAM_COUNT, logger);
            return false;
        }
        web1TCountMap = (NgramMapCountWrapper) env
                .getResource(PEVResourceType.WEB1T_NGRAM_COUNT);
        if (web1TCountMap == null) {
            PipelineEnvironment.logResourceNotFound(PEVResourceType.WEB1T_NGRAM_COUNT, logger);
            return false;
        }
        return true;
    }

    @Override
    protected void extract(Instance<Phrase> phraseInstance) throws Exception {
        List<? extends Token> tokens = phraseInstance.getSource().getSpan().getTokens();
        int n = tokens.size();
//        if (n > this.countMap.getMaxOrder()) {
//            Arrays.fill(curValues, Instance.MISSING);
//            return;
//        }
        String[] key = TokenUtil.tokens2StrArr(tokens);
        String[] surfaceKey = TokenUtil.tokens2StrArr(tokens, phraseInstance.getSource().getSurfaceForm());
        long count = this.countMap.getCount(key);
        long web1tCount = this.web1TCountMap.getCount(surfaceKey);
        Long topCount = countMap.getTopCountNgram(n);
        if (topCount == null) topCount = countMap.getTopCountNgram(countMap.getMaxOrder());
        Long web1tTopCount = this.web1TCountMap.getTopCountNgram(n);
        if (web1tTopCount == null) web1tTopCount = web1TCountMap.getTopCountNgram(web1TCountMap.getMaxOrder());
        curValues[0] = (double) count / (double) topCount;
        curValues[1] = Math.log10(curValues[0]);
        curValues[2] = (double) web1tCount / (double) web1tTopCount;
        curValues[3] = Math.log10((double) web1tCount) - Math.log10((double) web1tTopCount);
    }

    @Override
    protected Feature[] features() {
        return features;
    }

    @Override
    protected boolean initForBuck(PipelineEnvironment env, S2PaperBuck prev) {
        return true;
    }

    @Override
    protected boolean initForUnit(BuckUnit unit) {
        return true;
    }

    @Override
    protected boolean register() {
        return registered;
    }

    @Override
    protected void registered() {
        registered = true;
    }
}
