package edu.northwestern.websail.kpe.data.phrase;


import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.data.TokenSpan;

/**
 * @author NorThanapon
 */
public class Phrase {

    protected String source;
    protected String surfaceForm;
    protected TokenSpan span;
    protected Token before;
    protected Token after;
    protected boolean isKey;

    public Phrase() {
    }

    public Phrase(String surfaceForm,
                  String source, TokenSpan span) {
        this.surfaceForm = surfaceForm;
        this.source = source;
        this.span = span;
    }

    public String getSignature() {
        return this.surfaceForm + "@" + this.getSource();
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public TokenSpan getSpan() {
        return span;
    }

    public void setSpan(TokenSpan span) {
        this.span = span;
    }

    public Token getBefore() {
        return before;
    }

    public void setBefore(Token before) {
        this.before = before;
    }

    public Token getAfter() {
        return after;
    }

    public void setAfter(Token after) {
        this.after = after;
    }

    public String getSurfaceForm() {
        return surfaceForm;
    }

    public void setSurfaceForm(String surfaceForm) {
        this.surfaceForm = surfaceForm;
    }

    public int getStartOffset() {
        return this.span.getStartOffset();
    }

    public int getEndOffset() {
        return this.span.getEndOffset();
    }

    public boolean isKey() {
        return isKey;
    }

    public void setKey(boolean isKey) {
        this.isKey = isKey;
    }

    public String toString() {
        return this.surfaceForm;
    }
}
