package edu.northwestern.websail.kpe.main.sandbox;

import edu.northwestern.websail.core.io.OutputFileManager;

/**
 * @author NorThanapon
 * @since 8/11/15
 */
public class TestOutputMgr {
    public static void main(String[] args) {
        OutputFileManager outMgr = new OutputFileManager("/Users/NorThanapon/Desktop/test/a.txt");
        outMgr.close();
    }
}
