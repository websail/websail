package edu.northwestern.websail.kpe.data.paper;

import edu.northwestern.websail.kpe.data.S2PaperUtil;

/**
 * @author csbhagav, NorThanapon
 * @since 6/28/15
 * <p/>
 * Citation contexts provided in S2 paper format
 */
public class S2CitationContext {

    // fields in json
    int citeStart;
    int citeEnd;
    String sectionName;
    String string;
    double relevance;

    // cache
    int citationCount = -1;

    @Deprecated // for json deserialization
    public S2CitationContext() {
    }

    public S2CitationContext(int citeStart, int citeEnd, String sectionName, String citationString, double relevance) {
        this.citeStart = citeStart;
        this.citeEnd = citeEnd;
        this.sectionName = sectionName;
        this.string = citationString;
        this.relevance = relevance;
    }

    public int getCiteStart() {
        return citeStart;
    }

    public int getCiteEnd() {
        return citeEnd;
    }

    public String getSectionName() {
        return sectionName;
    }

    public String getCitationString() {
        return string;
    }

    public double getRelevance() {
        return relevance;
    }

    public int getCitationCount() {
        if (this.citationCount == -1) {
            this.citationCount = S2PaperUtil.countCitations(this.getCitationString());
        }
        return citationCount;
    }
}
