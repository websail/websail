package edu.northwestern.websail.kpe.pe.filter;


import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.buck.BuckUnitTypes;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.kpe.pe.helper.AcronymHelper;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.tokenizer.SentenceSpliter;
import edu.northwestern.websail.text.tokenizer.StanfordNLPSSplit;
import edu.northwestern.websail.text.tokenizer.StanfordNLPTokenizer;
import edu.northwestern.websail.text.tokenizer.Tokenizer;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 * @since 11/19/14
 */
@PipelineConfigInfo(
        requiredResources = {PEVResourceType.TOKENIZER_POOL, PEVResourceType.S_SENTENCE_SPLITER_POOL},
        requiredSettings = {"kpe.acronymFile=*"}
)
@PipelineBUTInfo(
        consumes = {KPEBUT.Sentences.class, KPEBUT.Phrases.class}
)
public class AcronymExpanser implements PipelineExecutable {
    public static final Logger logger = Logger
            .getLogger(AcronymExpanser.class.getName());

    int threadId = 0;
    SentenceSpliter ssplit;
    Tokenizer tokenizer;
    HashMap<String, ArrayList<AcronymHelper.StringIntegerPair>> abbrMap;

    public AcronymExpanser() {
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        ssplit = (StanfordNLPSSplit) env.getPoolResource(
                PEVResourceType.S_SENTENCE_SPLITER_POOL,
                this.threadId, PEVResourceType.S_SENTENCE_SPLITER);
        if (ssplit == null) {
            ssplit = new StanfordNLPSSplit(true);
        }
        tokenizer = (Tokenizer) env.getPoolResource(PEVResourceType.TOKENIZER_POOL,
                this.threadId, PEVResourceType.TOKENIZER);
        if (tokenizer == null) {
            tokenizer = new StanfordNLPTokenizer();
        }
        String abbrFile = config.getProperty("kpe.acronymFile", null);
        if (abbrFile == null) {
            logger.warning("kpe.acronymFile is not defined. Global acronym list is ignored!");
        } else {
            abbrMap = AcronymHelper.loadAcronymMap(abbrFile, tokenizer);
        }
        return true;
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        HashMap<String, String> localAbbrMap = new HashMap<String, String>();
        List<Token> allTokens = new ArrayList<Token>();
        for (BuckUnit unit : prev.units()) {
            if (unit.isNotOkay())
                continue;
            List<Token> unitTokens = new ArrayList<Token>();
            for (TokenSpan sentence : unit.get(KPEBUT.Sentences.class)) {
                unitTokens.addAll(sentence.getTokens());
            }
            allTokens.addAll(unitTokens);
            localAbbrMap.putAll(AcronymHelper.buildAbbrMap(unitTokens,
                    unit.get(BuckUnitTypes.TextBUT.class),
                    false));
        }
        for (BuckUnit unit : prev.units()) {
            if (unit.isNotOkay())
                continue;
            try {
                FastList<Phrase> phrases = unit.get(KPEBUT.Phrases.class);
                for (Phrase phrase : phrases) {
                    if (expanLocal(phrase, localAbbrMap)) {
                        continue;
                    }
                    expanGlobal(phrase, allTokens);
                }
            } catch (Exception e) {
                logger.severe("Unexpected error while expanding phrases.");
                e.printStackTrace();
            }
        }
        return true;
    }

    private boolean expanLocal(Phrase phrase, HashMap<String, String> localAbbrMap) {
        String surface = phrase.getSurfaceForm();
        String expansion = AcronymHelper.expand(localAbbrMap, phrase.getSpan().getTokens(),
                surface, -phrase.getSpan().getTokens().get(0).getStartOffset());
        return !expansion.equalsIgnoreCase(surface) && setNewSurface(phrase, expansion);
    }

    private void expanGlobal(Phrase phrase, List<Token> allTokens) {
        if (abbrMap == null) return;
        String surface = phrase.getSurfaceForm();
        String expansion = AcronymHelper.expand(abbrMap, phrase.getSpan().getTokens(),
                surface, -phrase.getSpan().getTokens().get(0).getStartOffset(), allTokens);
        if (!expansion.equalsIgnoreCase(surface)) {
            setNewSurface(phrase, expansion);
        }
    }

    private boolean setNewSurface(Phrase phrase, String expansion) {
        List<TokenSpan> spans = ssplit.sentenceSplit(expansion);
        if (spans.size() > 1) return false;
        TokenSpan span = spans.get(0);
        span.setStartOffset(phrase.getSpan().getStartOffset());
        span.setEndOffset(phrase.getSpan().getEndOffset());
        span.setTokens(TokenUtil.normalize(span.getTokens(), true, true));
        phrase.setSurfaceForm(expansion);
        phrase.setSpan(span);
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public int getThreadId() {
        return threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }

}
