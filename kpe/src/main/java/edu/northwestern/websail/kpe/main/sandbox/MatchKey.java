package edu.northwestern.websail.kpe.main.sandbox;

import edu.northwestern.websail.core.io.InputFileManager;
import edu.northwestern.websail.core.io.OutputFileManager;
import edu.northwestern.websail.kpe.data.LabelAdapter;
import edu.northwestern.websail.kpe.data.kp.KeyPhrase;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.tokenizer.StanfordNLPTokenizer;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.*;

/**
 * @author NorThanapon
 * @since 1/29/15
 */
public class MatchKey {
    public static void main(String[] args) {
        StanfordNLPTokenizer tokenizer = new StanfordNLPTokenizer();
        String labelFile = "/Users/NorThanapon/Desktop/semeval/train.combined.final";
        HashMap<String, ArrayList<KeyPhrase>> labels = LabelAdapter.readSemEvalLabel(labelFile, true, tokenizer);
        //String labelFile = "/Users/NorThanapon/Desktop/test.combined.stem.final";
        //HashMap<String, ArrayList<KeyPhrase>> labels = LabelAdapter.readSemEvalLabel(labelFile, false, tokenizer);
        String phrasesFile = "/Users/NorThanapon/Desktop/semeval/semeval_phrases.txt";
        OutputFileManager outMgr = new OutputFileManager(phrasesFile + ".label");

        HashMap<String, HashSet<String>> matchKeys = new HashMap<String, HashSet<String>>();
        int total = 0;
        for (ArrayList<KeyPhrase> l : labels.values()) {
            total += l.size();
        }
        InputFileManager inputMgr = new InputFileManager(phrasesFile);
        String line;
        String id = "";
        HashSet<String> keyMatch = new HashSet<String>();
        int count = 0;
        int prevSize = 0;
        while ((line = inputMgr.readLine()) != null) {
            String[] parts = line.split("\t");
            ArrayList<KeyPhrase> paperLabel = labels.get(parts[0]);
            if (paperLabel == null) continue;
            if (!id.equals(parts[0])) {
                System.out.println(id + ": " + keyMatch.size() + "/" + prevSize);
                count += keyMatch.size();
                matchKeys.put(id, keyMatch);
                keyMatch = new HashSet<String>();
                id = parts[0];
                prevSize = paperLabel.size();
            }
            tokenizer.initialize(parts[1]);
            List<Token> tokens = tokenizer.getAllTokens();
            tokens = TokenUtil.normalize(tokens, true, true);
            KeyPhrase kp = new KeyPhrase(parts[0], parts[1], tokens);
            boolean match = false;
            for (KeyPhrase key : paperLabel) {
                if (kp.getNormalizedPhrase().equals(key.getNormalizedPhrase())) {
                    match = true;
                    keyMatch.add(key.getNormalizedPhrase());
                }
            }
            outMgr.println(line + "\t" + match);

        }
        outMgr.close();
        count += keyMatch.size();
        System.out.println(id + ": " + keyMatch.size() + "/" + labels.get(id).size());
        matchKeys.put(id, keyMatch);
        System.out.println(count + "/" + total + " = " + count / (double) total);
        int[] tokenCount = {0, 0, 0, 0, 0};
        int[] tokenNotMatchCount = {0, 0, 0, 0, 0};
        int notMatch = 0;
        for (Map.Entry<String, ArrayList<KeyPhrase>> p : labels.entrySet()) {
            //System.out.println(p.getKey());
            for (KeyPhrase k : p.getValue()) {
                int len = k.getTokens().size();
                if (len > 5) len = 5;
                tokenCount[len - 1]++;
                if (!matchKeys.get(p.getKey()).contains(k.getNormalizedPhrase())) {
                    notMatch++;
                    if (len == 2) System.out.println(p.getKey() + "\t" + k);
                    tokenNotMatchCount[len - 1]++;
                }
            }
        }
        System.out.println(notMatch);
        System.out.println(Arrays.toString(tokenCount));
        System.out.println(Arrays.toString(tokenNotMatchCount));
    }
}
