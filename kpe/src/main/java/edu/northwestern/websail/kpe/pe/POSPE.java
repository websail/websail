package edu.northwestern.websail.kpe.pe;

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnitTypes;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.kpe.pe.helper.ValidPOSTags;
import edu.northwestern.websail.text.data.TaggedToken;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.tokenizer.SentenceSpliter;
import edu.northwestern.websail.text.tokenizer.StanfordNLPSSplit;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.List;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 8/18/15
 */
@PipelineConfigInfo(
        requiredResources = {PEVResourceType.S_SENTENCE_SPLITER_POOL},
        requiredSettings = {"text.tokenizer.SentenceSpliter.pos=true"}
)
@PipelineBUTInfo(
        consumes = {BuckUnitTypes.TextBeginOffsetBUT.class, BuckUnitTypes.TextBUT.class},
        produces = {KPEBUT.Sentences.class, KPEBUT.Phrases.class}
)
public class POSPE extends PhraseExtractor {
    private static final int MAX_LEN = 5;

    protected SentenceSpliter ssplit;

    public POSPE() {
    }

    public POSPE(SentenceSpliter ssplit) {
        this.ssplit = ssplit;
    }

    public static void main(String[] args) throws Exception {
        POSPE pospe = new POSPE(new StanfordNLPSSplit(true));
        System.out.println(pospe.extract("Apocrita: A Distributed Peer-to-Peer File Sharing System for Intranets"));
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config))
            return false;
        this.initialized = false;
        this.ssplit = (SentenceSpliter) env.getPoolResource(
                PEVResourceType.S_SENTENCE_SPLITER_POOL,
                this.threadId, PEVResourceType.S_SENTENCE_SPLITER);
        if (ssplit == null) {
            ssplit = new StanfordNLPSSplit(true);
        }
        this.initialized = true;
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        normalizedToken = true;
        return super.close(env, config);
    }

    @Override
    protected FastList<Phrase> extract(String text) throws Exception {

        try {
            if (this.curSentences == null) this.curSentences = ssplit.sentenceSplit(text);
        } catch (Exception e) {
            logger.severe("Unexpected error while trying to split sentences. Source: " + this.curSource);
            e.printStackTrace();
        }
        FastList<Phrase> phrases = new FastList<Phrase>();
        for (TokenSpan span : this.curSentences) {
            List<? extends Token> tokens = span.getTokens();
            if (!normalizedToken) {
                span.setTokens(TokenUtil.cleanTokens(TokenUtil.normalize(tokens, true, true), false, null));
            }
            for (int i = 0; i < span.getTokens().size(); i++) {
                TaggedToken tt = (TaggedToken) span.getTokens().get(i);
                if (ValidPOSTags.validEndPOS.contains(tt.getPOS())) {
                    addPhrase(i, i, span.getTokens(), text, phrases);
                    for (int j = i - 1; j >= 0; j--) {
                        if (i - j >= MAX_LEN) break;
                        TaggedToken backTT = (TaggedToken) span.getTokens().get(j);
                        if (!ValidPOSTags.ALLOW_POS_SET.contains(backTT.getPOS())) break;
                        if (backTT.getPOS().equals("IN") && !backTT.getText().equalsIgnoreCase("of")) break;
                        if (ValidPOSTags.validStartPOS.contains(backTT.getPOS())) {
                            addPhrase(j, i, span.getTokens(), text, phrases);
                        }
                    }
                }
            }
        }
        return phrases;
    }

    private void addPhrase(int start, int end, List<? extends Token> sentence, String fullText, FastList<Phrase> phrases) {
        Token a = sentence.get(start);
        Token b = sentence.get(end);
        TokenSpan span = new TokenSpan(a.getStartOffset(), b.getEndOffset(), sentence.subList(
                start, end + 1), 0);
        Phrase phrase = new Phrase(fullText.substring(a.getStartOffset(), b.getEndOffset()), super.curSource, span);
        if (start == 0) {
            phrase.setBefore(Token.START);
        } else {
            phrase.setBefore(sentence.get(start));
        }
        if (end == sentence.size() - 1) {
            phrase.setAfter(Token.END);
        } else {
            phrase.setAfter(sentence.get(end));
        }
        phrases.add(phrase);
    }

}
