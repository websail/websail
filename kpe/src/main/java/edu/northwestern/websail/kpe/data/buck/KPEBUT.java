package edu.northwestern.websail.kpe.data.buck;

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.pipeline.buck.BuckUnitTypes;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.data.TokenSpan;

/**
 * @author NorThanapon
 * @since 11/7/14
 */
@SuppressWarnings("unchecked")
public class KPEBUT {
    public static class LocationType implements BuckUnitTypes.UnitTypeKey<String> {
        public Class<String> getType() {
            return String.class;
        }
    }

    public static class Sentences implements BuckUnitTypes.UnitTypeKey<FastList<TokenSpan>> {
        @Override
        public Class<FastList<TokenSpan>> getType() {
            return (Class<FastList<TokenSpan>>) (new FastList<TokenSpan>())
                    .getClass();
        }
    }

    public static class Phrases implements BuckUnitTypes.UnitTypeKey<FastList<Phrase>> {
        @Override
        public Class<FastList<Phrase>> getType() {
            return (Class<FastList<Phrase>>) (new FastList<Phrase>())
                    .getClass();
        }
    }
}
