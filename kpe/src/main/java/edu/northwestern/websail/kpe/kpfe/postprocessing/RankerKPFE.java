package edu.northwestern.websail.kpe.kpfe.postprocessing;

import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.classify.result.RankingResult;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.ds.tuple.IntegerDoublePair;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.kpe.kpfe.KPFeatureExtractor;

import java.util.Arrays;
import java.util.HashMap;

/**
 * @author NorThanapon
 * @since 11/19/14
 */
@PipelineConfigInfo()
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class, ClassifyBUT.RankingResults.class}
)
public class RankerKPFE extends KPFeatureExtractor {

    public static final Feature rankerOrderF = new Feature("ranker", "order",
            Feature.Type.NUMBER);
    public static final Feature rankerScoreF = new Feature("ranker", "score",
            Feature.Type.NUMBER);

    private static final Feature[] features = {rankerOrderF, rankerScoreF};
    private static boolean registered = false;
    private HashMap<Instance, IntegerDoublePair> rankMap;

    public RankerKPFE() {
        curValues = new double[features.length];
        Arrays.fill(curValues, Instance.MISSING);
    }

    @Override
    protected void extract(Instance<Phrase> phraseInstance) throws Exception {
        IntegerDoublePair pair = rankMap.get(phraseInstance);
        if (pair == null) {
            logger.warning("Cannot find instance " + phraseInstance + " in the rank map");
            return;
        }
        curValues[0] = pair.getIntElement();
        curValues[1] = pair.getDoubleElement();
    }

    @Override
    protected Feature[] features() {
        return features;
    }

    @Override
    protected boolean initForBuck(PipelineEnvironment env, S2PaperBuck prev) {
        return true;
    }

    @Override
    protected boolean initForUnit(BuckUnit unit) {
        rankMap = new HashMap<Instance, IntegerDoublePair>();
        RankingResult result = unit.get(ClassifyBUT.RankingResults.class);
        for (int order = 0; order < result.getOrders().length; order++) {
            int index = result.getOrders()[order];
            double score = result.getScores()[index];
            Instance inst = result.getInstances().getInstances().get(index);
            rankMap.put(inst, new IntegerDoublePair(order, score));
        }
        return true;
    }

    @Override
    protected boolean register() {
        return registered;
    }

    @Override
    protected void registered() {
        registered = true;
    }
}
