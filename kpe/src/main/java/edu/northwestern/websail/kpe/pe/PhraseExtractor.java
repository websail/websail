package edu.northwestern.websail.kpe.pe;

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.buck.BuckUnitTypes;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.data.TokenSpan;

import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 * @since 11/7/14
 */
@PipelineConfigInfo()
public abstract class PhraseExtractor implements PipelineExecutable {

    public static final Logger logger = Logger.getLogger(PhraseExtractor.class.getName());
    protected static final String PE_NORMALIZED_KEY = "peNormalizedKey";
    protected boolean normalizedToken = false;
    protected int threadId;
    protected boolean initialized;
    protected String curSource;
    protected List<TokenSpan> curSentences;
    protected int curOffset;

    public PhraseExtractor() {
        this.initialized = false;
        this.threadId = 0;
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (env.getPipelineGlobalOuputs().containsKey(PE_NORMALIZED_KEY)) {
            normalizedToken = (Boolean) env.getPipelineGlobalOuputs().get(PE_NORMALIZED_KEY);
        }
        return true;
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        if (!this.initialized) {
            logger.severe("Phrase Extractor has not yet been initialized.");
            return false;
        }
        logger.fine("@" + getThreadId() + ": " + prev.getSource());
        boolean oneSuccessful = false;
        this.curSource = prev.getSource();
        for (BuckUnit unit : prev.units()) {
            this.curSentences = unit.get(KPEBUT.Sentences.class);
            if (unit.isNotOkay())
                continue;
            FastList<Phrase> unitPhrases = this.extract(unit);

            if (this.curSentences != null) {
                if (this.curSentences instanceof FastList)
                    unit.set(KPEBUT.Sentences.class, (FastList<TokenSpan>) this.curSentences);
                else
                    unit.set(KPEBUT.Sentences.class, new FastList<TokenSpan>(this.curSentences));
            }
            this.curSentences = null;
            oneSuccessful = oneSuccessful || unitPhrases != null;
            unit.setOkay(unitPhrases != null);
            if (unitPhrases == null) continue;
            if (unit.get(KPEBUT.Phrases.class) != null) {
                unit.get(KPEBUT.Phrases.class).addAll(unitPhrases);
            } else {
                unit.set(KPEBUT.Phrases.class, unitPhrases);
            }
        }
        return oneSuccessful;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        env.getPipelineGlobalOuputs().put(PE_NORMALIZED_KEY, normalizedToken);
        return true;
    }

    @Override
    public int getThreadId() {
        return this.threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }

    private FastList<Phrase> extract(BuckUnit unit) {

        FastList<Phrase> phrases = null;
        Integer offset = unit.get(BuckUnitTypes.TextBeginOffsetBUT.class);
        if (offset == null)
            offset = 0;
        String text = unit.get(BuckUnitTypes.TextBUT.class);
        curOffset = offset;
        try {
            phrases = this.extract(text);
        } catch (Exception e) {
            logger.severe("Unexecpted Error while extracting phrases from \n'"
                    + text + "'");
            e.printStackTrace();
        } finally {
            curOffset = 0;
        }
        return phrases;
    }

    abstract protected FastList<Phrase> extract(String text) throws Exception;

    public List<TokenSpan> getCurSentences() {
        return curSentences;
    }

    protected void setCurSentences(FastList<TokenSpan> sentences) {
        this.curSentences = sentences;
    }
}
