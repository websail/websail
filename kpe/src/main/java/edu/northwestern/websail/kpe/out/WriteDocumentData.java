package edu.northwestern.websail.kpe.out;

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.io.OutputFileManager;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.buck.BuckUnitTypes;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.core.pipeline.unit.par.ParallelPLManager;
import edu.northwestern.websail.core.util.TimerUtil;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.kpe.pe.NounPhrasePE;
import edu.northwestern.websail.text.chunker.StanfordChunker;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.tokenizer.StanfordNLPSSplit;
import edu.northwestern.websail.text.util.TokenUtil;

import java.io.*;
import java.util.List;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 8/11/15
 * <p/>
 * Write sentences, spans of phrases, and paper positions.
 */
@PipelineConfigInfo(
        optionalSettings = {"kpe.out.docDataDir=*", "kpe.out.docDataFilePrefix=*"}
)
@PipelineBUTInfo(
        consumes = {KPEBUT.Sentences.class, KPEBUT.Phrases.class},
        produces = {}
)
public class WriteDocumentData implements PipelineExecutable {
    private static final Object lock = new Object();
    int threadId = 0;
    String commonTokenFilename;
    String commonSpanFilename;
    String commonPosFilename;
    OutputFileManager tokenOutMgr;
    OutputFileManager spanOutMgr;
    OutputFileManager posOutMgr;
    int currentLine = 0;
    String filePrefix;

    public WriteDocumentData() {
    }

    public static void main(String[] args) throws Exception {
        TimerUtil timerUtil = new TimerUtil();
        timerUtil.start();

        String text = "This is a test sentence. There is another sentence somewhere.";
        StanfordChunker ssa = new StanfordChunker();
        StanfordNLPSSplit nlpsSplit = new StanfordNLPSSplit(true);
        NounPhrasePE pe = new NounPhrasePE(ssa, nlpsSplit);
        FastList<Phrase> phrases = pe.extract(text, null);
        List<TokenSpan> sentences = pe.getCurSentences();
        int phrIndex = 0;
        for (TokenSpan sentence : sentences) {
            System.out.println(TokenUtil.tokens2Str(sentence.getTokens()));
            while (phrIndex < phrases.size()) {
                Phrase phr = phrases.get(phrIndex);
                int numTokens = phr.getSpan().getTokens().size();
                if (numTokens > 1) {
                    System.out.println(sentence.getTokens().indexOf(phr.getSpan().getTokens().get(0)));
                    System.out.println(sentence.getTokens().indexOf(phr.getSpan().getTokens().get(numTokens - 1)));
                }
                phrIndex++;
            }
        }
        timerUtil.end();
        System.out.println("Done [" + timerUtil.totalTime() + "]");
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        filePrefix = config.getProperty("kpe.out.docDataFilePrefix", "part");
        commonTokenFilename = config.getProperty("kpe.out.docDataDir", ".") + "/tokens/" + filePrefix;
        commonSpanFilename = config.getProperty("kpe.out.docDataDir", ".") + "/spans/" + filePrefix;
        commonPosFilename = config.getProperty("kpe.out.docDataDir", ".") + "/pos/" + filePrefix;
        tokenOutMgr = new OutputFileManager(config.getProperty("kpe.out.docDataDir", ".") + "/tokens/" + filePrefix + "." + this.getThreadId());
        spanOutMgr = new OutputFileManager(config.getProperty("kpe.out.docDataDir", ".") + "/spans/" + filePrefix + "." + this.getThreadId());
        posOutMgr = new OutputFileManager(config.getProperty("kpe.out.docDataDir", ".") + "/pos/" + filePrefix + "." + this.getThreadId());
        if (threadId == 0) {
            try {
                OutputFileManager.createFile(commonTokenFilename);
                OutputFileManager.createFile(commonSpanFilename);
                OutputFileManager.createFile(commonPosFilename);
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            ParallelPLManager.countDownLatch(env, ParallelPLManager.LEADER_LATCH);
        }
        return true;
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        posOutMgr.print(prev.getSource() + "\t" + currentLine);
        for (BuckUnit unit : prev.units()) {
            if (unit.isNotOkay())
                continue;
            posOutMgr.print("\t" + unit.get(KPEBUT.LocationType.class) + "\t" + currentLine);
            List<TokenSpan> unitSentences = unit.get(KPEBUT.Sentences.class);
            FastList<Phrase> unitPhrases = unit.get(KPEBUT.Phrases.class);
            int phrIndex = 0;
            for (TokenSpan sentence : unitSentences) {
                tokenOutMgr.println(TokenUtil.tokens2Str(sentence.getTokens(), unit.get(BuckUnitTypes.TextBUT.class)));
                while (phrIndex < unitPhrases.size()) {
                    Phrase phr = unitPhrases.get(phrIndex);
                    int numTokens = phr.getSpan().getTokens().size();
                    if (numTokens > 1) {
                        int start = sentence.getTokens().indexOf(phr.getSpan().getTokens().get(0));
                        int end = sentence.getTokens().indexOf(phr.getSpan().getTokens().get(numTokens - 1)) + 1;
                        if (start == -1 || end == -1) break;
                        spanOutMgr.print(start + "," + end + "\t");
                    }
                    phrIndex++;
                }
                spanOutMgr.println("");
                currentLine++;
            }
        }
        posOutMgr.println("\t" + currentLine);
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        tokenOutMgr.close();
        spanOutMgr.close();
        posOutMgr.close();
        ParallelPLManager.awaitLatch(env, ParallelPLManager.LEADER_LATCH);
        synchronized (lock) {
            copyFile(tokenOutMgr.getFilename(), commonTokenFilename);
            copyFile(spanOutMgr.getFilename(), commonSpanFilename);
            copyFile(posOutMgr.getFilename(), commonPosFilename);
        }
        deleteFile(tokenOutMgr.getFilename());
        deleteFile(spanOutMgr.getFilename());
        deleteFile(posOutMgr.getFilename());
        return true;
    }

    private void copyFile(String src, String dest) {
        try {
            OutputStream out = new BufferedOutputStream(new FileOutputStream(dest, true));
            InputStream in = new BufferedInputStream(new FileInputStream(src));
            int b;
            byte[] buf = new byte[1 << 20];
            while ((b = in.read(buf)) >= 0) {
                out.write(buf, 0, b);
            }
            in.close();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteFile(String filename) {
        File file = new File(filename);
        file.deleteOnExit();

    }

    @Override
    public int getThreadId() {
        return threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }
}
