package edu.northwestern.websail.kpe.main;

import edu.northwestern.websail.classify.classifier.weka.WekaClassifierWrapper;
import edu.northwestern.websail.classify.ranker.mm.MaxMargin;
import edu.northwestern.websail.core.pipeline.PipelineExecutor;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.kpe.data.ACLPaperAdapter;
import edu.northwestern.websail.kpe.data.LabelAdapter;
import edu.northwestern.websail.kpe.data.kp.KeyPhrase;
import edu.northwestern.websail.kpe.pe.filter.MatchACLKey;
import edu.northwestern.websail.text.tokenizer.Tokenizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;

/**
 * @author NorThanapon
 * @since 1/28/15
 */
public class RunACL20kPipeline {
    public static final boolean fullBody = false;
    public static final boolean noBody = false;

    public static void main(String[] args) throws IOException {

        String mainPipelineFile = "pconfig/full/acl20k/init.xml";
        String trainRankerPipelineFile = "pconfig/full/acl20k/ranker_train.xml";
        String outRankerPipelineFile = "pconfig/full/acl20k/ranker_run.xml";
        String trainingFile = "/websail/common/keyphrase_extraction/data/acl20k/labales/kpe.training_label.v3";
        String testLabelFile = "/websail/common/keyphrase_extraction/data/acl20k/labales/kpe.label.v3.tsv";
        String dataFile = "/websail/common/keyphrase_extraction/data/acl20k/papers/acl.json";
        String citationFile = "/websail/common/keyphrase_extraction/data/acl20k/citations/citation_context.txt";
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(Level.FINE);
        MaxMargin.logger.setLevel(Level.FINE);
        MaxMargin.logger.setUseParentHandlers(false);
        MaxMargin.logger.addHandler(handler);
        WekaClassifierWrapper.logger.setLevel(Level.FINE);
        WekaClassifierWrapper.logger.setUseParentHandlers(false);
        WekaClassifierWrapper.logger.addHandler(handler);
        PipelineExecutor mainPipeline = null;
        PipelineExecutor trainRankerPipeline = null;
        PipelineExecutor outRankerPipeline = null;
        try {
            mainPipeline = new PipelineExecutor(mainPipelineFile);
            Tokenizer tokenizer = (Tokenizer) mainPipeline.getEnv().getPoolResource(
                    PEVResourceType.TOKENIZER_POOL, 0,
                    PEVResourceType.TOKENIZER);
            System.out.print("Loading label data... ");
            HashMap<String, ArrayList<KeyPhrase>> trainingLabels =
                    LabelAdapter.readLabelFile(trainingFile, tokenizer, null);
            HashMap<String, ArrayList<KeyPhrase>> testLabels =
                    LabelAdapter.readLabelFile(testLabelFile, tokenizer, null);
            //set keys to env
            mainPipeline.getEnv().getPipelineGlobalOuputs().put(MatchACLKey.kpeLabelKey, trainingLabels);
            System.out.println(trainingLabels.keySet().size() + " papers");
            System.out.print("Loading papers... ");
            ACLPaperAdapter adapter = new ACLPaperAdapter(dataFile, tokenizer, true, citationFile, true);
            ArrayList<Buck> trainRankBucks = new ArrayList<Buck>();
            ArrayList<Buck> allBucks = new ArrayList<Buck>();

            for (Buck b : adapter.getBucks(fullBody, noBody)) {
                if (!(trainingLabels.containsKey(b.getSource()) || testLabels.containsKey(b.getSource())))
                    continue;
                if (trainingLabels.containsKey(b.getSource())) {
                    trainRankBucks.add(b);
                }
                allBucks.add(b);
            }
            adapter.close();
            System.out.println("all: " + allBucks.size() + " papers.");
            System.out.println("training ranker: " + trainRankBucks.size() + " papers.");
            System.out.println("Start main pipeline...");
            mainPipeline.parRun(allBucks);
            System.out.println("Start training ranker pipeline...");
            trainRankerPipeline = new PipelineExecutor(trainRankerPipelineFile, mainPipeline);
            trainRankerPipeline.run(trainRankBucks);
            System.out.println("Start output ranker pipeline...");
            outRankerPipeline = new PipelineExecutor(outRankerPipelineFile, mainPipeline);
            outRankerPipeline.parRun(allBucks);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (trainRankerPipeline != null) {
                trainRankerPipeline.close();
            }
            if (mainPipeline != null) {
                mainPipeline.close();
            }
            if (outRankerPipeline != null) {
                outRankerPipeline.close();
            }
        }
    }
}

