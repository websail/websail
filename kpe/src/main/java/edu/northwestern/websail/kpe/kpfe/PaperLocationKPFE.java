package edu.northwestern.websail.kpe.kpfe;

import edu.northwestern.websail.classify.data.CommonNominal;
import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.ds.tuple.IntegerPair;
import edu.northwestern.websail.kpe.data.S2PaperUtil;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import edu.northwestern.websail.kpe.data.phrase.Phrase;

import java.util.Arrays;

/**
 * @author NorThanapon
 * @since 11/19/14
 */
@PipelineConfigInfo()
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class}
)
public class PaperLocationKPFE extends KPFeatureExtractor {

    public static final Feature inTitleF = new Feature("location", "foundInTitle",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature inAbstractF = new Feature("location", "foundInAbstr",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature inIntroF = new Feature("location", "foundInIntro",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature inBodyF = new Feature("location", "foundInBody",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature inConcF = new Feature("location", "foundInConclusion",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    private static final Feature[] features = {inTitleF, inAbstractF, inIntroF, inBodyF, inConcF};
    private static boolean registered = false;
    String currentLoc;
    public PaperLocationKPFE() {
        curValues = new double[features.length];
        Arrays.fill(curValues, Instance.MISSING);
    }

    @Override
    protected void extract(Instance<Phrase> phraseInstance) throws Exception {
        if (currentLoc != null &&
                (currentLoc.equals(S2PaperUtil.LOC_BODY) ||
                        currentLoc.equals(S2PaperUtil.LOC_INTRO) ||
                        currentLoc.equals(S2PaperUtil.LOC_CONCLUSION))) {
            int start = phraseInstance.getSource().getStartOffset();
            int end = phraseInstance.getSource().getEndOffset();
            IntegerPair introOffset = curPaper.getSectionOffsets().get(S2PaperUtil.LOC_INTRO);
            if (introOffset == S2PaperUtil.NOT_FOUND_OFFSET) {
                curValues[2] = CommonNominal.boolMapFalseValue;
            } else {
                curValues[2] = start >= introOffset.getFirst() && end <= introOffset.getSecond() ?
                        CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
            }
            IntegerPair concOffset = curPaper.getSectionOffsets().get(S2PaperUtil.LOC_CONCLUSION);
            if (concOffset == S2PaperUtil.NOT_FOUND_OFFSET) {
                curValues[4] = CommonNominal.boolMapFalseValue;
            } else {
                curValues[4] = start >= concOffset.getFirst() && end <= concOffset.getSecond() ?
                        CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
            }
        }
    }

    @Override
    protected Feature[] features() {
        return features;
    }

    @Override
    protected boolean initForBuck(PipelineEnvironment env, S2PaperBuck prev) {
        return true;
    }

    @Override
    protected boolean initForUnit(BuckUnit unit) {
        String location = unit.get(KPEBUT.LocationType.class);
        this.currentLoc = location;
        if (location == null) {
            logger.warning("Location of a unit cannot be determined. Feature values will be MISSING.");
            Arrays.fill(curValues, Instance.MISSING);
        } else {
            curValues[0] = location.equals(S2PaperUtil.LOC_TITLE) ?
                    CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
            curValues[1] = location.equals(S2PaperUtil.LOC_ABSTRACT) ?
                    CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
            curValues[2] = location.equals(S2PaperUtil.LOC_INTRO) ?
                    CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
            curValues[3] = location.equals(S2PaperUtil.LOC_BODY) || location.equals(S2PaperUtil.LOC_INTRO) || location.equals(S2PaperUtil.LOC_CONCLUSION) ?
                    CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
            curValues[4] = location.equals(S2PaperUtil.LOC_CONCLUSION) ?
                    CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
        }
        return true;
    }

    @Override
    protected boolean register() {
        return registered;
    }

    @Override
    protected void registered() {
        registered = true;
    }
}
