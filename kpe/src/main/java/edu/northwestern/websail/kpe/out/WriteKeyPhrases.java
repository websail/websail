package edu.northwestern.websail.kpe.out;

import edu.northwestern.websail.classify.data.CommonNominal;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.classify.result.ClassificationResult;
import edu.northwestern.websail.classify.result.RankingResult;
import edu.northwestern.websail.core.io.OutputFileManager;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.core.pipeline.unit.common.WriteAndConcatFileUnit;
import edu.northwestern.websail.kpe.data.kp.KPUtil;
import edu.northwestern.websail.kpe.data.kp.ScoreKeyPhrase;
import edu.northwestern.websail.kpe.kpfe.InstanceInitalizer;

import java.util.*;

/**
 * @author NorThanapon
 * @since 11/20/14
 */
@PipelineConfigInfo(
        requiredSettings = {"kpe.out.keyphraseFile=*"},
        optionalSettings = {"kpe.out.keyphraseSimpleDedup={true|false}"}
)
public class WriteKeyPhrases extends WriteAndConcatFileUnit {

    boolean simpleDedup = false;

    public WriteKeyPhrases() {
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config)) {
            return false;
        }
        simpleDedup = Boolean.parseBoolean(config.getProperty("kpe.out.keyphraseSimpleDedup", "false"));
        return true;
    }

    @Override
    protected String outFileConfigKey() {
        return "kpe.out.keyphraseFile";
    }

    @SuppressWarnings("unchecked")
    @Override
    protected boolean write(PipelineEnvironment env, Buck prev, OutputFileManager out) {
        if (simpleDedup) {
            simpleDedupOutput(prev, out);
        } else {
            basicOutput(prev, out);
        }
        return true;
    }

    private void simpleDedupOutput(Buck prev, OutputFileManager out) {
        String source = prev.getSource();
        RankingResult rankingResult = null;
        HashMap<String, ArrayList<Instance>> classifiedDedupMap = new HashMap<String, ArrayList<Instance>>();
        for (BuckUnit unit : prev.units()) {
            ArrayList<ClassificationResult> results = unit.get(ClassifyBUT.ClassifyResults.class);
            RankingResult unitRankingResult = unit.get(ClassifyBUT.RankingResults.class);
            if (results != null) {
                collectClassificationResults(results, classifiedDedupMap);
            } else if (unitRankingResult != null) {
                rankingResult = unitRankingResult;
            }
        }
        if (rankingResult == null) {
            simpleDedupOutputClassification(classifiedDedupMap, out, source);
        } else {
            simpleDedupOutputRanking(rankingResult, out, source);
        }
    }

    private void collectClassificationResults(ArrayList<ClassificationResult> results, HashMap<String, ArrayList<Instance>> classifiedDedupMap) {
        for (ClassificationResult result : results) {
            if (result.getClassValue() == CommonNominal.boolMapTrueValue) {
                String norm = KPUtil.instance2NormalizedSurface(result.getInstance());
                if (!classifiedDedupMap.containsKey(norm)) {
                    classifiedDedupMap.put(norm, new ArrayList<Instance>());
                }
                classifiedDedupMap.get(norm).add(result.getInstance());
            }
        }
    }

    private void simpleDedupOutputClassification(HashMap<String, ArrayList<Instance>> classifiedDedupMap, OutputFileManager out, String source) {
        for (ArrayList<Instance> instances : classifiedDedupMap.values()) {
            ArrayList<ScoreKeyPhrase> skps = new ArrayList<ScoreKeyPhrase>();
            for (Instance instance : instances) {
                skps.add(KPUtil.instance2ScoreKeyPhrase(instance));
            }
            ScoreKeyPhrase skp = KPUtil.averageScores(skps);
            out.println(source +
                    "\t" + skp +
                    "\ttrue");
        }
    }

    private void simpleDedupOutputRanking(RankingResult rankingResult, OutputFileManager out, String source) {
        // collect
        HashMap<String, ArrayList<ScoreKeyPhrase>> dedupOrderMap = new HashMap<String, ArrayList<ScoreKeyPhrase>>();
        HashMap<String, Double> dedupScoreMap = new HashMap<String, Double>();
        HashMap<String, Double> classMap = new HashMap<String, Double>();
        for (int i = 0; i < rankingResult.getInstances().getInstances().size(); i++) {
            String norm = KPUtil.instance2NormalizedSurface(rankingResult.getInstances().getInstances().get(i));
            if (!dedupOrderMap.containsKey(norm)) {
                dedupOrderMap.put(norm, new ArrayList<ScoreKeyPhrase>());
                dedupScoreMap.put(norm, 0.0);
                classMap.put(norm, 0.0);
            }
            dedupOrderMap.get(norm).add(KPUtil.instance2ScoreKeyPhrase(rankingResult.getInstances().getInstances().get(i)));
            dedupScoreMap.put(norm, dedupScoreMap.get(norm) + rankingResult.getScores()[i]);
            classMap.put(norm, classMap.get(norm) + rankingResult.getInstances().getInstances().get(i).getValue(InstanceInitalizer.classFeature));
        }
        // average
        Double[] avgScores = new Double[dedupOrderMap.size()];
        Integer[] orders = new Integer[dedupOrderMap.size()];
        Double[] classValues = new Double[dedupOrderMap.size()];
        ArrayList<ScoreKeyPhrase> avgKP = new ArrayList<ScoreKeyPhrase>();
        int i = 0;
        for (String norm : dedupOrderMap.keySet()) {
            avgKP.add(KPUtil.averageScores(dedupOrderMap.get(norm)));
            avgScores[i] = dedupScoreMap.get(norm) / dedupOrderMap.get(norm).size();
            orders[i] = i;
            classValues[i] = classMap.get(norm) >= 1 ? 1.0 : 0.0;
            i++;
        }
        // sort
        SKPIndexComparator comparator = new SKPIndexComparator(avgScores);
        Arrays.sort(orders, comparator);
        // output
        for (Integer index : orders) {
            out.println(source +
                    "\t" + avgScores[index] +
                    "\t" + avgKP.get(index) +
                    "\t" + classValues[index]);
        }
    }

    private void basicOutput(Buck prev, OutputFileManager out) {
        String source = prev.getSource();
        RankingResult rankingResult = null;
        for (BuckUnit unit : prev.units()) {
            ArrayList<ClassificationResult> results = unit.get(ClassifyBUT.ClassifyResults.class);
            RankingResult unitRankingResult = unit.get(ClassifyBUT.RankingResults.class);
            if (results != null) {
                basicOutputClassification(results, out, source);
            } else if (unitRankingResult != null) {
                rankingResult = unitRankingResult;
            }
        }
        if (rankingResult != null) {
            basicOutputRanking(rankingResult, out, source);
        }
    }

    private void basicOutputClassification(ArrayList<ClassificationResult> results, OutputFileManager out, String source) {
        for (ClassificationResult result : results) {
            if (result.getClassValue() == CommonNominal.boolMapTrueValue) {
                out.println(source +
                        "\t" + KPUtil.instance2ScoreKeyPhrase(result.getInstance()) +
                        "\t" + result.getInstance().getValue(InstanceInitalizer.classFeature));
            }
        }
    }

    private void basicOutputRanking(RankingResult rankingResult, OutputFileManager out, String source) {
        for (Integer index : rankingResult.getOrders()) {
            out.println(source +
                    "\t" + rankingResult.getScores()[index] +
                    "\t" + KPUtil.instance2ScoreKeyPhrase(rankingResult.getInstances().getInstances().get(index)) +
                    "\t" + rankingResult.getInstances().getInstances().get(index).getValue(InstanceInitalizer.classFeature));
        }
    }

    private static class SKPIndexComparator implements Comparator<Integer> {
        private final Double[] array;

        public SKPIndexComparator(Double[] array) {
            this.array = array;
        }

        @Override
        public int compare(Integer index1, Integer index2) {
            return array[index2].compareTo(array[index1]);
        }
    }
}
