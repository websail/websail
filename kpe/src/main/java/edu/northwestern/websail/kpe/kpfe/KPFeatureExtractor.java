package edu.northwestern.websail.kpe.kpfe;

import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.ds.indexer.Indexer;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import edu.northwestern.websail.kpe.data.paper.S2Paper;
import edu.northwestern.websail.kpe.data.phrase.Phrase;

import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 * @since 11/18/14
 */
public abstract class KPFeatureExtractor implements PipelineExecutable {
    public static final Logger logger = Logger.getLogger(KPFeatureExtractor.class.getName());
    private static final Object lock = new Object();
    protected int threadId = 0;
    protected S2Paper curPaper;
    protected double[] curValues;
    public KPFeatureExtractor() {
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        synchronized (lock) {
            if (!register()) {
                Indexer<Feature> features = InstanceInitalizer.envFeatures(env);
                if (features == null) {
                    logger.severe("Instance Features has not been initialized. Please execute "
                            + InstanceInitalizer.class.getName() + " prior to this pipeline.");
                    return false;
                }
                for (Feature feature : features()) {
                    boolean result = features.add(feature);
                    if (!result) {
                        logger.warning("Duplicate feature found. Ignore " + feature.fullName());
                    }
                }
                registered();
            }
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        boolean oneSuccess = false;
        S2PaperBuck paperBuck = (S2PaperBuck) prev;
        curPaper = paperBuck.getPaper();
        if (!initForBuck(env, paperBuck)) {
            logger.severe("Fail to initialize feature extractor for buck: " + prev);
            return false;
        }
        for (BuckUnit unit : prev.units()) {
            if (unit.isNotOkay())
                continue;
            if (!initForUnit(unit)) {
                logger.warning("Fail to initialize feature extractor for a unit of " + prev);
                continue;
            }
            Instances instances = unit.get(ClassifyBUT.BuckInstances.class);
            for (Instance instance : instances.getInstances()) {
                try {
                    extract(instance);
                    setFeatures(instance);
                    oneSuccess = true;
                } catch (Exception e) {
                    logger.warning("Unexpected error while extracting feature for instance " + instance.getSource());
                    e.printStackTrace();
                }
            }
        }
        return oneSuccess;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        return true;
    }

    protected void setFeatures(Instance instance) {
        Feature[] features = features();
        if (curValues.length != features.length) {
            throw new UnsupportedOperationException("Defined features and extracted values do not have the same lenght. Expect "
                    + features.length + ", have " + curValues.length + ". Please check init() and features() method.");
        }
        for (int i = 0; i < features.length; i++) {
            instance.setValue(features[i], curValues[i]);
        }
    }

    @Override
    public int getThreadId() {
        return threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }

    protected abstract void extract(Instance<Phrase> phraseInstance) throws Exception;

    protected abstract Feature[] features();

    protected abstract boolean initForBuck(PipelineEnvironment env, S2PaperBuck prev);

    protected abstract boolean initForUnit(BuckUnit unit);

    protected abstract boolean register();

    protected abstract void registered();
}
