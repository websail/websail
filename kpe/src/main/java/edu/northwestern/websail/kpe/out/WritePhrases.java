package edu.northwestern.websail.kpe.out;

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.io.OutputFileManager;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.core.pipeline.unit.common.WriteAndConcatFileUnit;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.phrase.Phrase;

/**
 * @author NorThanapon
 * @since 11/20/14
 */
@PipelineConfigInfo(
        requiredSettings = {"kpe.out.phraseFile=*"}
)
public class WritePhrases extends WriteAndConcatFileUnit {

    public WritePhrases() {
    }

    @Override
    protected String outFileConfigKey() {
        return "kpe.out.phraseFile";
    }

    @Override
    protected boolean write(PipelineEnvironment env, Buck prev, OutputFileManager out) {
        String source = prev.getSource();
        for (BuckUnit unit : prev.units()) {
            FastList<Phrase> phrases = unit.get(KPEBUT.Phrases.class);
            if (phrases == null) return true;
            for (Phrase phrase : phrases) {
                out.println(source + "\t" + phrase.getSurfaceForm());
            }
        }
        return true;
    }
}
