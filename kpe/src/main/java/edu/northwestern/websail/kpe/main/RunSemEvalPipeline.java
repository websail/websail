package edu.northwestern.websail.kpe.main;

import edu.northwestern.websail.classify.classifier.weka.WekaClassifierWrapper;
import edu.northwestern.websail.classify.ranker.mm.MaxMargin;
import edu.northwestern.websail.core.pipeline.PipelineExecutor;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.util.TimerUtil;
import edu.northwestern.websail.kpe.data.LabelAdapter;
import edu.northwestern.websail.kpe.data.S2PaperAdapter;
import edu.northwestern.websail.kpe.data.S2PaperUtil;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import edu.northwestern.websail.kpe.data.kp.KeyPhrase;
import edu.northwestern.websail.kpe.main.postprocessing.FormatRankSemEval;
import edu.northwestern.websail.kpe.pe.filter.MatchSemEvalKey;
import edu.northwestern.websail.text.tokenizer.Tokenizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;

/**
 * @author NorThanapon
 * @since 1/28/15
 */
public class RunSemEvalPipeline {
    public static final String mainPipelineFile = "pconfig/full/semeval/init.xml";
    public static final String trainRankerPipelineFile = "pconfig/full/semeval/ranker_train.xml";
    public static final String outRankerPipelineFile = "pconfig/full/semeval/ranker_run.xml";
    public static final String trainingFile = "/websail/common/keyphrase_extraction/data/semeval10/official/train/train.combined.final";
    public static final String testFile = "/websail/common/keyphrase_extraction/data/semeval10/official/answer/test.combined.stem.final";
    public static final String dataFile = "/websail/common/keyphrase_extraction/data/semeval10/papers/semeval.json";
    public static final HashSet<String> targetLocation = S2PaperUtil.WHOLE_PAPER_LOCATIONS;

    public static void main(String[] args) throws IOException {
        boolean run = Boolean.parseBoolean(args[0]);
        TimerUtil timerUtil = new TimerUtil();
        timerUtil.start();
        configLoggers();
        PipelineExecutor mainPipeline = null;
        PipelineExecutor trainRankerPipeline = null;
        PipelineExecutor outRankerPipeline = null;
        try {
            mainPipeline = new PipelineExecutor(mainPipelineFile);
            Tokenizer tokenizer = (Tokenizer) mainPipeline.getEnv().getPoolResource(
                    PEVResourceType.TOKENIZER_POOL, 0,
                    PEVResourceType.TOKENIZER);
            System.out.print("Loading label data... ");
            HashMap<String, ArrayList<KeyPhrase>> trainingLabels =
                    LabelAdapter.readSemEvalLabel(trainingFile, true, tokenizer);
            //set keys to env
            mainPipeline.getEnv().getPipelineGlobalOuputs().put(MatchSemEvalKey.kpeLabelKey, trainingLabels);
            System.out.println(trainingLabels.keySet().size() + " papers");
            System.out.print("Loading papers... ");
            S2PaperAdapter adapter = new S2PaperAdapter(true);
            ArrayList<S2PaperBuck> allBucks = adapter.makeAllBuck(dataFile, S2PaperUtil.WHOLE_PAPER_LOCATIONS);
            ArrayList<S2PaperBuck> trainRankBucks = new ArrayList<S2PaperBuck>();
            for (S2PaperBuck b : allBucks) {
                if (trainingLabels.containsKey(b.getSource())) {
                    trainRankBucks.add(b);
                }
            }
            System.out.println("all: " + allBucks.size() + " papers.");
            System.out.println("training ranker: " + trainRankBucks.size() + " papers.");
            System.out.println("Start main pipeline...");
            mainPipeline.parRun(allBucks);
            if (run) {
                System.out.println("Start training ranker pipeline...");
                trainRankerPipeline = new PipelineExecutor(trainRankerPipelineFile, mainPipeline);
                trainRankerPipeline.run(trainRankBucks);
                System.out.println("Start output ranker pipeline...");
                outRankerPipeline = new PipelineExecutor(outRankerPipelineFile, mainPipeline);
                outRankerPipeline.parRun(allBucks);
                String rankKPFile = mainPipeline.getPipelineConfig().getGlobalConfig().getProperty("kpe.out.keyphraseFile");
                System.out.println("Formating output ...");
                FormatRankSemEval.main(new String[]{rankKPFile, testFile, rankKPFile + ".top15"});
            } else {
                System.out.println("Skip training and testing!");
            }
            timerUtil.end();
            System.out.println("Done [" + timerUtil.totalTime() + "]");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (trainRankerPipeline != null) {
                trainRankerPipeline.close();
            }
            if (mainPipeline != null) {
                mainPipeline.close();
            }
            if (outRankerPipeline != null) {
                outRankerPipeline.close();
            }
        }
    }

    private static void configLoggers() {
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(Level.FINE);
        MaxMargin.logger.setLevel(Level.FINE);
        MaxMargin.logger.setUseParentHandlers(false);
        MaxMargin.logger.addHandler(handler);
        WekaClassifierWrapper.logger.setLevel(Level.FINE);
        WekaClassifierWrapper.logger.setUseParentHandlers(false);
        WekaClassifierWrapper.logger.addHandler(handler);
    }
}

