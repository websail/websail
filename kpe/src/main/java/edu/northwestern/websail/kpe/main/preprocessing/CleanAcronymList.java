package edu.northwestern.websail.kpe.main.preprocessing;

import edu.northwestern.websail.core.io.InputFileManager;
import edu.northwestern.websail.core.io.OutputFileManager;
import edu.northwestern.websail.kpe.pe.helper.AcronymHelper;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.tokenizer.StanfordNLPTokenizer;
import edu.northwestern.websail.text.tokenizer.Tokenizer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author NorThanapon
 * @since 1/11/15
 */
public class CleanAcronymList {
    public static void main(String[] args) {
        String acrFile = args[0];
        String outFile = args[1];
        Tokenizer tokenizer = new StanfordNLPTokenizer();

        InputFileManager inMgr = new InputFileManager(acrFile);
        ArrayList<Integer> counts = new ArrayList<Integer>();
        ArrayList<String> fullForms = new ArrayList<String>();
        ArrayList<String> shortForms = new ArrayList<String>();
        String line;
        while ((line = inMgr.readLine()) != null) {
            String[] parts = line.split("\t");
            int count = Integer.parseInt(parts[0]);
            String full = parts[1].trim();
            // XXX: Hack to clean a number after phrases
            char c = full.charAt(full.length() - 1);
            if (c >= '0' && c <= '9') {
                full = full.substring(0, full.length() - 1);
            }
            String abbr = parts[2].trim();
            tokenizer.initialize(full);
            List<? extends Token> tokens = tokenizer.getAllTokens();
            full = AcronymHelper.traceFullForm(tokens, full, abbr, tokens.size() - 1);
            if (full == null) continue;
            counts.add(count);
            fullForms.add(full);
            shortForms.add(abbr);
        }
        inMgr.close();
        HashMap<String, Integer> fullCount = new HashMap<String, Integer>();
        HashMap<String, String> fullAbbr = new HashMap<String, String>();
        for (int i = 0; i < fullForms.size(); i++) {
            String fullForm = fullForms.get(i);
            String abbr = shortForms.get(i);
            int count = counts.get(i);
            if (fullCount.containsKey(fullForm)) {
                count += fullCount.get(fullForm);
            }
            fullCount.put(fullForm, count);
            fullAbbr.put(fullForm, abbr);
        }
        OutputFileManager outMgr = new OutputFileManager(outFile);
        for (Map.Entry<String, String> p : fullAbbr.entrySet()) {
            String fullForm = p.getKey();
            String abbr = p.getValue();
            Integer count = fullCount.get(fullForm);
            outMgr.println(count + "\t" + fullForm + "\t" + abbr);
        }
        outMgr.close();
    }
}
