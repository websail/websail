package edu.northwestern.websail.kpe.kpfe;

import edu.northwestern.websail.classify.data.CommonNominal;
import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.data.Token;

import java.util.Arrays;

/**
 * @author NorThanapon
 * @since 11/19/14
 */
@PipelineConfigInfo(
)
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class}
)
public class MathExprKPFE extends KPFeatureExtractor {

    public static final Feature shortAvgNumCharF = new Feature("math", "shortAvgNumChar",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature shortLastNumCharF = new Feature("math", "shortLastNumChar",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature charAtLastToken = new Feature("math", "charAtLastToken",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature shortCommonVariableF = new Feature("math", "shortCommonVariable",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature simpleMathExprF = new Feature("math", "simpleMathExpr",
            Feature.Type.NOMINAL, CommonNominal.boolMap);
    public static final Feature lastCharIsANumF = new Feature("math", "lastCharIsANum",
            Feature.Type.NOMINAL, CommonNominal.boolMap);

    private static final Feature[] features = {shortAvgNumCharF, shortLastNumCharF, charAtLastToken,
            shortCommonVariableF, simpleMathExprF, lastCharIsANumF};
    private static final String scvRegex = "[\\+\\-abctrijkxyvzmn]{1,2}";
    private static final String smeRegex = "\\s*-?.+(?:\\s*[-+*/]\\s*\\d+)+";
    private static final String endNumRegex = ".*\\d$";
    private static final int shortThreshold = 3;
    private static boolean registered = false;

    public MathExprKPFE() {
        curValues = new double[features.length];
        Arrays.fill(curValues, Instance.MISSING);
    }

    @Override
    protected void extract(Instance<Phrase> phraseInstance) throws Exception {
        Phrase phr = phraseInstance.getSource();
        double avgLen = 0.0;
        int curLen = 0;
        boolean scv = false;
        boolean endWithNumber = false;
        boolean sme = phr.getSurfaceForm().matches(smeRegex);
        for (Token token : phr.getSpan().getTokens()) {
            curLen = token.getText().length();
            if (curLen < shortThreshold)
                scv = scv || token.getText().matches(scvRegex);
            endWithNumber = token.getText().matches(endNumRegex);
            avgLen += curLen;

        }
        avgLen = avgLen / phr.getSpan().getTokens().size();
        curValues[0] = avgLen < shortThreshold ? CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
        curValues[1] = curLen < shortThreshold ? CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
        curValues[2] = curLen == 1 ? CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
        curValues[3] = scv ? CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
        curValues[4] = sme ? CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
        curValues[5] = endWithNumber ? CommonNominal.boolMapTrueValue : CommonNominal.boolMapFalseValue;
    }

    @Override
    protected Feature[] features() {
        return features;
    }

    @Override
    protected boolean initForBuck(PipelineEnvironment env, S2PaperBuck prev) {
        return true;
    }

    @Override
    protected boolean initForUnit(BuckUnit unit) {
        return true;
    }

    @Override
    protected boolean register() {
        return registered;
    }

    @Override
    protected void registered() {
        registered = true;
    }
}
