package edu.northwestern.websail.kpe.kpfe;

import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.ngram.ARPALMWrapper;
import edu.northwestern.websail.text.ngram.NgramMapCountWrapper;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.Arrays;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 11/19/14
 */
@PipelineConfigInfo(
        requiredSettings = {"kpe.lm.paper.arpaBinaryDirectory=*", "ngram.countWordIndexer=*"},
        requiredResources = {PEVResourceType.CORPUS_NGRAM_MODEL, PEVResourceType.CORPUS_NGRAM_COUNT}
)
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class}
)
public class NgramProbKPFE extends KPFeatureExtractor {
    public static final Feature corpusPrF = new Feature("ngramProb", "corpus", Feature.Type.NUMBER);
    public static final Feature normCorpusPrF = new Feature("ngramProb", "normCorpus", Feature.Type.NUMBER);
    public static final Feature paperPrF = new Feature("ngramProb", "paper", Feature.Type.NUMBER);
    public static final Feature normPaperPrF = new Feature("ngramProb", "normPaper", Feature.Type.NUMBER);
    public static final Feature paperCorpusRatioF = new Feature("ngramProb", "paperCorpusRatio", Feature.Type.NUMBER);
    public static final Feature gapScoreF = new Feature("ngramProb", "gapSCore", Feature.Type.NUMBER);
    private static final double MIN_PROB = -13.;
    private static final Feature[] features = {corpusPrF, normCorpusPrF, paperPrF, normPaperPrF, paperCorpusRatioF, gapScoreF};
    private static boolean registered = false;
    private double[] topNgramProbs;
    private ARPALMWrapper corpusLM;
    private ARPALMWrapper paperLM;
    private String paperNgramDir;

    public NgramProbKPFE() {
        curValues = new double[features.length];
        Arrays.fill(curValues, Instance.MISSING);
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config)) return false;
        boolean success = true;
        this.corpusLM = (ARPALMWrapper) env
                .getResource(PEVResourceType.CORPUS_NGRAM_MODEL);
        if (this.corpusLM == null) {
            PipelineEnvironment.logResourceNotFound(PEVResourceType.CORPUS_NGRAM_MODEL, logger);
            success = false;
        }
        if (config.containsKey("kpe.lm.arpaBinaryDirectory")) {
            this.paperNgramDir = config
                    .getProperty("kpe.lm.arpaBinaryDirectory");
        } else {
            logger.severe("Missing configuration key 'acl.lm.arpaBinaryDirectory'");
            success = false;
        }
        if (!success) {
            return false;
        }
        NgramMapCountWrapper nGramCountMap = (NgramMapCountWrapper) env.getResource(PEVResourceType.CORPUS_NGRAM_COUNT);
        if (nGramCountMap == null) {
            PipelineEnvironment.logResourceNotFound(PEVResourceType.CORPUS_NGRAM_COUNT, logger);
            return false;
        }
        topNgramProbs = new double[nGramCountMap.getMaxOrder()];
        for (int i = 0; i < nGramCountMap.getMaxOrder(); i++) {
            topNgramProbs[i] = this.corpusLM.getScore(nGramCountMap.getTopNgram(i).split(" "));
        }
        return true;
    }

    @Override
    protected void extract(Instance<Phrase> phraseInstance) throws Exception {
        String[] ngram = TokenUtil.tokens2StrArr(phraseInstance.getSource().getSpan().getTokens());
        double corpusNgram = this.corpusLM.getScore(ngram);
        if (corpusNgram < MIN_PROB) {
            corpusNgram = MIN_PROB;
        }
        double normCorpusNgram = corpusNgram - topNgramProbs[ngram.length - 1];
        double paperNgram = this.paperLM.getScore(ngram);
        if (paperNgram < MIN_PROB) {
            paperNgram = MIN_PROB;
        }
        double normPaperNgram = paperNgram - topNgramProbs[ngram.length - 1];
        double gapScore = this.computeGapSCP(ngram, phraseInstance.getSource().getBefore().getText(),
                phraseInstance.getSource().getAfter().getText());
        curValues[0] = corpusNgram;
        curValues[1] = normCorpusNgram;
        curValues[2] = paperNgram;
        curValues[3] = normPaperNgram;
        curValues[4] = paperNgram - corpusNgram;
        curValues[5] = gapScore;
    }

    @Override
    protected Feature[] features() {
        return features;
    }

    @Override
    protected boolean initForBuck(PipelineEnvironment env, S2PaperBuck prev) {
        String paperId = prev.getSource();
        this.paperLM = new ARPALMWrapper(this.paperNgramDir + paperId
                + ".barpa.lm", false, true);
        return true;
    }

    @Override
    protected boolean initForUnit(BuckUnit unit) {
        return true;
    }

    private double computeGapSCP(String[] ngram, String before, String after) {
        if (before == null || after == null)
            return Instance.MISSING;
        double[] unigramProbs = new double[ngram.length];
        for (int i = 0; i < ngram.length; i++) {
            unigramProbs[i] = this.corpusLM.getScore(ngram[i]);
        }
        double internalSCP = this.computeInternalSCP(ngram, unigramProbs);
        double externalSCP = this.computeExternalSCP(ngram, unigramProbs,
                before, after);
        return internalSCP - externalSCP;
    }

    private double computeInternalSCP(String[] ngram, double[] unigramProbs) {
        double internalSCP = 0.0;
        int count = 0;
        if (ngram.length == 1) {
            internalSCP = unigramProbs[0];
            count++;
        } else {
            for (int i = 0; i < ngram.length; i++) {
                if (i == 0)
                    continue;
                String[] subPhrase = Arrays.copyOfRange(ngram, i - 1, i + 1);
                internalSCP += (this.corpusLM.getScore(subPhrase) * 2
                        - unigramProbs[i - 1] - unigramProbs[i]);
                count++;
            }
        }
        internalSCP = internalSCP / count;

        return internalSCP;
    }

    private double computeExternalSCP(String[] ngram, double[] unigramProbs,
                                      String before, String after) {
        double unigramProbBefore = this.corpusLM.getScore(before);
        double unigramProbAfter = this.corpusLM.getScore(after);
        String[] head = new String[2];
        String[] tail = new String[2];
        head[0] = before;
        head[1] = ngram[0];
        tail[0] = ngram[ngram.length - 1];
        tail[1] = after;
        int count = 0;
        double externalSCP = 0.0;
        if (!before.equals("<s>")) {
            externalSCP += (this.corpusLM.getScore(head) * 2
                    - unigramProbBefore - unigramProbs[0]);
            count++;
        }
        if (!after.equals("</s>")) {
            externalSCP += (this.corpusLM.getScore(tail) * 2 - unigramProbAfter - unigramProbs[ngram.length - 1]);
            count++;
        }
        if (count == 0)
            return 0.0;
        return externalSCP / count;
    }

    @Override
    protected boolean register() {
        return registered;
    }

    @Override
    protected void registered() {
        registered = true;
    }
}
