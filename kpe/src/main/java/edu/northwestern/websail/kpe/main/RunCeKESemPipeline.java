package edu.northwestern.websail.kpe.main;

import edu.northwestern.websail.core.io.InputFileManager;
import edu.northwestern.websail.core.io.OutputFileManager;
import edu.northwestern.websail.core.pipeline.PipelineExecutor;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.config.PipelineUnit;
import edu.northwestern.websail.core.util.TimerUtil;
import edu.northwestern.websail.kpe.data.ACLPaperAdapter;
import edu.northwestern.websail.kpe.data.LabelAdapter;
import edu.northwestern.websail.kpe.data.kp.KeyPhrase;
import edu.northwestern.websail.kpe.main.postprocessing.FormatRankSemEval;
import edu.northwestern.websail.kpe.pe.filter.MatchSemEvalKey;
import edu.northwestern.websail.text.tokenizer.SUPTBTokenizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 * @since 8/3/15
 */
public class RunCeKESemPipeline {

    // Pipeline
    static final String mainPipelineFile = "pconfig/full/cekesem/init.xml";
    static final String trainRankerPipelineFile = "pconfig/full/cekesem/ranker_train.xml";
    static final String outRankerPipelineFile = "pconfig/full/cekesem/ranker_run.xml";
    // Papers and Labels
    static final String paperDir = "/websail/common/keyphrase_extraction/data/ceke_semeval_lite/papers";
    static final String labelFile = paperDir + "/papers.labels";
    static final String paperFile = paperDir + "/papers.json";
    // Data in buck
    static final boolean fullBody = true;
    static final boolean noBody = false;
    //XXX: Hack tokenizer
    static final SUPTBTokenizer tokenizer = new SUPTBTokenizer();
    static String targetIdFile = null;
    static String testFile;

    public static void main(String[] args) throws IOException {
        TimerUtil timerUtil = new TimerUtil();
        timerUtil.start();
        System.out.println("Target dataset: " + args[0]);
        targetIdFile = paperDir + "/" + args[0] + ".ids";
        testFile = args[1];
        setUpLogger();
        ArrayList<Buck> paperBucks;
        HashMap<String, ArrayList<KeyPhrase>> labels;
        try {
            paperBucks = loadData();
            System.out.print("Loading label data... ");
            labels = LabelAdapter.readSemEvalLabel(labelFile, true, tokenizer);
            int match = checkLabels(labels, paperBucks);
            System.out.println(match + " papers with labels");
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        PipelineExecutor mainPipeline = null;
        try {
            mainPipeline = new PipelineExecutor(mainPipelineFile);
            mainPipeline.getEnv().getPipelineGlobalOuputs().put(MatchSemEvalKey.kpeLabelKey, labels);
            System.out.println("Start main pipeline...");
            mainPipeline.parRun(paperBucks);
            CVRunner.runCV(paperBucks, trainRankerPipelineFile, outRankerPipelineFile, mainPipeline, 10);
            System.out.println();
            String rankKPFile = mainPipeline.getPipelineConfig().getGlobalConfig().getProperty("kpe.out.keyphraseFile");
            mergeOutputFiles(rankKPFile, 10);
            System.out.println("Formating output ...");
            FormatRankSemEval.main(new String[]{rankKPFile, testFile, rankKPFile + ".top15"});
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mainPipeline != null) {
                mainPipeline.close();
            }
        }

        timerUtil.end();
        System.out.println("Done [" + timerUtil.totalTime() + "]");
    }

    private static void setUpLogger() {
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(Level.FINE);
//        MatchSemEvalKey.logger.setLevel(Level.FINE);
//        MatchSemEvalKey.logger.setUseParentHandlers(false);
//        MatchSemEvalKey.logger.addHandler(handler);
//        MaxMargin.logger.setLevel(Level.FINE);
//        MaxMargin.logger.setUseParentHandlers(false);
//        MaxMargin.logger.addHandler(handler);
//        WekaClassifierWrapper.logger.setLevel(Level.FINE);
//        WekaClassifierWrapper.logger.setUseParentHandlers(false);
//        WekaClassifierWrapper.logger.addHandler(handler);
//        PhraseExtractor.logger.setLevel(Level.FINE);
//        PhraseExtractor.logger.setUseParentHandlers(false);
//        PhraseExtractor.logger.addHandler(handler);

    }

    private static ArrayList<Buck> loadData() throws IOException {
        System.out.print("Loading papers... ");
        HashSet<String> targetIds = new HashSet<String>();
        InputFileManager inMgr = new InputFileManager(targetIdFile);
        String line;
        while ((line = inMgr.readLine()) != null) {
            targetIds.add(line.trim());
        }
        inMgr.close();
        ACLPaperAdapter adapter = new ACLPaperAdapter(paperFile, tokenizer, true, true);
        ArrayList<Buck> allBucks = new ArrayList<Buck>();
        for (Buck b : adapter.getBucks(fullBody, noBody)) {
            if (targetIds.contains(b.getSource()))
                allBucks.add(b);
        }
        adapter.close();
        System.out.println(allBucks.size() + " papers");
        return allBucks;
    }

    private static int checkLabels(HashMap<String, ArrayList<KeyPhrase>> labels, ArrayList<Buck> paperBucks) {
        int match = 0;
        for (Buck b : paperBucks) {
            if (labels.containsKey(b.getSource())) {
                match++;
            }
        }
        return match;
    }

    private static void mergeOutputFiles(String filename, int folds) {
        OutputFileManager outMgr = new OutputFileManager(filename);
        for (int i = 0; i < folds; i++) {
            String fn = "cv" + i + "_" + filename;
            InputFileManager inMgr = new InputFileManager(fn);
            String line;
            while ((line = inMgr.readLine()) != null) {
                outMgr.println(line);
            }
            inMgr.close();
        }
        outMgr.close();
    }

    private static class CVRunner {
        public static final Logger logger = Logger.getLogger(CVRunner.class.getName());
        public static final boolean singleThread = true;

        public static void runCV(ArrayList<Buck> bucks, String trainingPipelineXML,
                                 String testingPipelineXML, PipelineExecutor commonPipeline, int folds)
                throws Exception {
            ArrayList<ArrayList<Buck>> parts = partition(bucks, folds);
            if (singleThread) {
                for (int i = 0; i < folds; i++) {
                    ArrayList<Buck> trainingBucks = getTrainingBucks(parts, i);
                    CVFold fold = new CVFold(i, trainingBucks, parts.get(i),
                            trainingPipelineXML, testingPipelineXML, commonPipeline);
                    fold.run();
                }
            } else {
                ArrayBlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(
                        folds);
                ThreadPoolExecutor executor = new ThreadPoolExecutor(folds,
                        folds, 5L, TimeUnit.MINUTES, queue,
                        new ThreadPoolExecutor.CallerRunsPolicy());
                for (int i = 0; i < folds; i++) {
                    ArrayList<Buck> trainingBucks = getTrainingBucks(parts, i);
                    CVFold fold = new CVFold(i, trainingBucks, parts.get(i),
                            trainingPipelineXML, testingPipelineXML, commonPipeline);
                    executor.execute(fold);
                }
                executor.shutdown();
                executor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
            }
        }

        public static ArrayList<ArrayList<Buck>> partition(ArrayList<Buck> bucks, int num) {
            ArrayList<ArrayList<Buck>> parts = new ArrayList<ArrayList<Buck>>();
            int each = bucks.size() / num;
            int extra = bucks.size() % num;
            int current = 0;
            for (int i = 0; i < num; i++) {
                int len = each;
                if (extra > 0)
                    len++;
                ArrayList<Buck> subBucks = new ArrayList<Buck>();
                subBucks.addAll(bucks.subList(current, current + len));
                current += len;
                extra--;
                parts.add(subBucks);
            }
            return parts;
        }

        public static ArrayList<Buck> getTrainingBucks(ArrayList<ArrayList<Buck>> parts, int except) {
            ArrayList<Buck> bucks = new ArrayList<Buck>();
            for (int i = 0; i < parts.size(); i++) {
                if (i == except) continue;
                bucks.addAll(parts.get(i));
            }
            return bucks;
        }

        private static class CVFold implements Runnable {
            final ArrayList<Buck> trainingBucks;
            final ArrayList<Buck> testingBucks;
            final String trainingPipelineXML;
            final String testingPipelineXML;
            final PipelineExecutor commonPipeline;
            int foldNum = 0;

            public CVFold(int foldNum, ArrayList<Buck> trainingBucks,
                          ArrayList<Buck> testingBucks, String trainingPipelineXML,
                          String testingPipelineXML, PipelineExecutor commonPipeline) {
                this.foldNum = foldNum;
                this.trainingBucks = trainingBucks;
                this.testingBucks = testingBucks;
                this.trainingPipelineXML = trainingPipelineXML;
                this.testingPipelineXML = testingPipelineXML;
                this.commonPipeline = commonPipeline;
            }

            private void prependConfigValue(PipelineExecutor pipeline, String key,
                                            String prefix) {
                for (PipelineUnit unit : pipeline.getPlUnits()) {
                    if (unit.getUnitConfig().containsKey(key)) {
                        String value = unit.getUnitConfig().getProperty(
                                key);
                        if (value.indexOf(':') != -1) {
                            String directive = value.substring(0, value.indexOf(':'));
                            value = value.substring(value.indexOf(':') + 1);
                            prefix = directive + ":" + prefix;
                        }
                        unit.getUnitConfig().setProperty(key,
                                prefix + "_" + value);
                    }
                }
            }

            @Override
            public void run() {
                logger.info("CVFold " + this.foldNum + ": Training...");
                boolean trainingSuccess = true;
                PipelineExecutor trainingPipeline = null;
                try {
                    if (commonPipeline != null) {
                        logger.info("CVFold " + this.foldNum + ": Inherit resources from a common pipline.");
                        trainingPipeline = new PipelineExecutor(trainingPipelineXML, commonPipeline);
                    } else {
                        logger.info("CVFold " + this.foldNum + ": No common pipeline, it's okay.");
                        trainingPipeline = new PipelineExecutor(trainingPipelineXML);
                    }
                    this.prependConfigValue(trainingPipeline,
                            "rankerBuilder.output", "cv" + this.foldNum);
                    trainingPipeline.run(this.trainingBucks);

                } catch (Exception e) {
                    logger.severe("CVFold " + this.foldNum
                            + ": Unexpected error during training");
                    e.printStackTrace();
                    trainingSuccess = false;
                }
                if (!trainingSuccess) {
                    logger.severe("CVFold " + this.foldNum
                            + ": Fail on training, skip testing");
                    return;
                }
                logger.info("CVFold " + this.foldNum + ": Testing...");
                PipelineExecutor testingPipeline;
                try {
                    testingPipeline = new PipelineExecutor(testingPipelineXML,
                            trainingPipeline);
                    this.prependConfigValue(testingPipeline,
                            "rankerLoader.model", "cv" + this.foldNum);
                    this.prependConfigValue(testingPipeline,
                            "rankerLoader.output", "cv" + this.foldNum);
                    this.prependConfigValue(testingPipeline,
                            "rankerRunner.model", "cv" + this.foldNum);
                    this.prependConfigValue(testingPipeline,
                            "kpe.out.keyphraseFile", "cv" + this.foldNum);
                    testingPipeline.run(this.testingBucks);
                    testingPipeline.close();
                    trainingPipeline.close();
                } catch (Exception e) {
                    logger.severe("CVFold " + this.foldNum
                            + ": Unexpected error during testing");
                    e.printStackTrace();
                }
            }
        }

    }
}
