package edu.northwestern.websail.kpe.main.sandbox;

import edu.berkeley.nlp.lm.WordIndexer;
import edu.berkeley.nlp.lm.io.IOUtils;
import edu.northwestern.websail.kpe.pe.helper.BottomUpMergingChunker;
import edu.northwestern.websail.text.ngram.NgramMapCountWrapper;
import edu.northwestern.websail.text.tokenizer.StanfordNLPSSplit;

/**
 * @author NorThanapon
 * @since 1/29/15
 */
public class BMCTest {
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        String dir = "/websail/common/keyphrase_extraction/data/semeval10/ngrams/corpus_ncase_stem";
        String widxFile = dir + "/papers.widx";
        String countFile = dir + "/papers.bmap";
        String w1tCountFile = dir + "/web1t_papers.bmap";

        WordIndexer<String> wIdx = (WordIndexer<String>) IOUtils.readObjFileHard(widxFile);
        NgramMapCountWrapper countMap = (NgramMapCountWrapper) IOUtils.readObjFileHard(countFile);
        NgramMapCountWrapper w1tCountMap = (NgramMapCountWrapper) IOUtils.readObjFileHard(w1tCountFile);

        countMap.setWordIndexer(wIdx);
        w1tCountMap.setWordIndexer(wIdx);


        StanfordNLPSSplit stanfordNLPSSplit = new StanfordNLPSSplit(true);

//        BottomUpMergingChunker.breakThreshold = -4;
//        BottomUpMergingChunker.mergeThreshold = -2.9;

        BottomUpMergingChunker chunker = new BottomUpMergingChunker(countMap, stanfordNLPSSplit);

        String text = "Scalable Grid Service Discovery Based on UDDI";

        System.out.println(BottomUpMergingChunker.displayChunk(chunker.chunk(text)));

        String text2 = "The standardization of grids based on web " +
                "services has resulted in the need for scalable web service " +
                "discovery mechanisms to be deployed in grids.";

        System.out.println(BottomUpMergingChunker.displayChunk(chunker.chunk(text2)));
    }
}
