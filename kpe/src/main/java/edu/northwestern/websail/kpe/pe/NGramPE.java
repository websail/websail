package edu.northwestern.websail.kpe.pe;


import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnitTypes;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.tokenizer.SentenceSpliter;
import edu.northwestern.websail.text.tokenizer.StanfordNLPSSplit;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.List;
import java.util.ListIterator;
import java.util.Properties;

/**
 * @author Chandra
 */

@PipelineConfigInfo(
        requiredResources = {PEVResourceType.S_SENTENCE_SPLITER_POOL},
        requiredSettings = {"text.tokenizer.SentenceSpliter.pos=true"}
)
@PipelineBUTInfo(
        consumes = {BuckUnitTypes.TextBeginOffsetBUT.class, BuckUnitTypes.TextBUT.class},
        produces = {KPEBUT.Sentences.class, KPEBUT.Phrases.class}
)
public class NGramPE extends PhraseExtractor {

    protected SentenceSpliter ssplit;

    public NGramPE() {
        super();
    }

    public static void main(String[] args) throws Exception {
        NGramPE pe = new NGramPE();
        pe.ssplit = new StanfordNLPSSplit(true);
        System.out.println(pe.extract("This is a very long sentence. Hello World."));
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config))
            return false;
        this.initialized = false;
        this.ssplit = (SentenceSpliter) env.getPoolResource(
                PEVResourceType.S_SENTENCE_SPLITER_POOL,
                this.threadId, PEVResourceType.S_SENTENCE_SPLITER);
        if (ssplit == null) {
            ssplit = new StanfordNLPSSplit(true);
        }
        this.initialized = true;
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        normalizedToken = true;
        return super.close(env, config);
    }

//    private void addUniGramPhrase(List<? extends Token> tokens, int i, String text, FastList<Phrase> phrases){
//        Token t = tokens.get(i);
//        TokenSpan span = new TokenSpan(t.getStartOffset(), t.getEndOffset(), tokens.subList(
//                i, i + 1), i);
//        Phrase phrase = new Phrase(text.substring(t.getStartOffset(), t.getEndOffset()), super.curSource, span);
//        if (i == 0) {
//            phrase.setBefore(Token.START);
//        } else {
//            phrase.setBefore(tokens.get(i - 1));
//        }
//        if (i == tokens.size()-1) {
//            phrase.setAfter(Token.END);
//        } else {
//            phrase.setAfter(tokens.get(i+1));
//        }
//        phrases.add(phrase);
//    }

//    private void addBiGramPhrase(List<? extends Token> tokens, int i, String text, FastList<Phrase> phrases) {
//        if (i == 0) return;
//        Token a = tokens.get(i - 1);
//        Token b = tokens.get(i);
//        TokenSpan span = new TokenSpan(a.getStartOffset(), b.getEndOffset(), tokens.subList(
//                i - 1, i + 1), i);
//        Phrase phrase = new Phrase(text.substring(a.getStartOffset(), b.getEndOffset()), super.curSource, span);
//        if (i == 1) {
//            phrase.setBefore(Token.START);
//        } else {
//            phrase.setBefore(tokens.get(i - 2));
//        }
//        if (i == tokens.size() - 1) {
//            phrase.setAfter(Token.END);
//        } else {
//            phrase.setAfter(tokens.get(i + 1));
//        }
//        phrases.add(phrase);
//    }

    @Override
    public FastList<Phrase> extract(String text) throws Exception {

        try {
            if (this.curSentences == null) this.curSentences = ssplit.sentenceSplit(text);
        } catch (Exception e) {
            System.out.println(this.curSource);
            System.out.println(text);
        }

        FastList<Phrase> phrases = new FastList<Phrase>();
        for (TokenSpan s : this.curSentences) {
            List<? extends Token> tokens = s.getTokens();
            if (!normalizedToken) {
                s.setTokens(TokenUtil.cleanTokens(TokenUtil.normalize(tokens, true, true), false, null));
            }
            addNgramsUpto(tokens, 5, text, phrases);
        }
        return phrases;
    }

    private void addNgramsUpto(List<? extends Token> sentence, int maxGramSize, String fullText, FastList<Phrase> phrases) {

        int ngramSize;
        int curPointer = 0;
        int backPointer;

        //sentence becomes ngrams
        for (ListIterator<? extends Token> it = sentence.listIterator(); it.hasNext(); ) {

            //1- add the word itself
            backPointer = curPointer;
            addPhrase(backPointer, curPointer, sentence, fullText, phrases);
            ngramSize = 1;


            //2- insert prevs of the word and add those too
            while (it.hasPrevious() && ngramSize < maxGramSize) {
                it.previous();
                backPointer--;
                addPhrase(backPointer, curPointer, sentence, fullText, phrases);
                ngramSize++;
            }

            //go back to initial position
            while (ngramSize > 0) {
                ngramSize--;
                it.next();
            }
            curPointer++;
        }
    }

    private void addPhrase(int start, int end, List<? extends Token> sentence, String fullText, FastList<Phrase> phrases) {
        Token a = sentence.get(start);
        Token b = sentence.get(end);
        TokenSpan span = new TokenSpan(a.getStartOffset(), b.getEndOffset(), sentence.subList(
                start, end + 1), 0);
        Phrase phrase = new Phrase(fullText.substring(a.getStartOffset(), b.getEndOffset()), super.curSource, span);
        if (start == 0) {
            phrase.setBefore(Token.START);
        } else {
            phrase.setBefore(sentence.get(start));
        }
        if (end == sentence.size() - 1) {
            phrase.setAfter(Token.END);
        } else {
            phrase.setAfter(sentence.get(end));
        }
        phrases.add(phrase);
    }
}
