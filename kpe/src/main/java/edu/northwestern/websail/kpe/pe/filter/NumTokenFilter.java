package edu.northwestern.websail.kpe.pe.filter;


import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.data.Token;

import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 * @since 11/19/14
 */
@PipelineConfigInfo()
@PipelineBUTInfo(
        consumes = {KPEBUT.Phrases.class},
        produces = {KPEBUT.Phrases.class}
)
public class NumTokenFilter implements PipelineExecutable {
    public static final Logger logger = Logger
            .getLogger(NumTokenFilter.class.getName());
    public static final int MIN_TOKENS = 1;
    public static final int MAX_TOKENS = 5;
    public static final int MIN_KEY_PHRASE_LENGTH = 3;


    int threadId = 0;

    public NumTokenFilter() {
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        for (BuckUnit unit : prev.units()) {
            if (unit.isNotOkay())
                continue;
            try {
                FastList<Phrase> phrases = unit.get(KPEBUT.Phrases.class);
                FastList<Phrase> filteredPhrases = new FastList<Phrase>();
                for (Phrase phrase : phrases) {
                    List<? extends Token> tokens = phrase.getSpan().getTokens();
                    if (tokens.size() > MAX_TOKENS) continue;
                    if (tokens.size() < MIN_TOKENS) continue;
                    if (phrase.getSurfaceForm().length() < MIN_KEY_PHRASE_LENGTH) continue;
                    int firstTokenLength = tokens.get(0).getEndOffset() - tokens.get(0).getStartOffset();
                    if (firstTokenLength <= 1) continue;
                    int length = 0;
                    for (Token t : tokens) {
                        length += (t.getEndOffset() - t.getStartOffset());
                    }
                    if (length == tokens.size()) continue;
                    filteredPhrases.add(phrase);
                }
                unit.set(KPEBUT.Phrases.class, filteredPhrases);
            } catch (Exception e) {
                logger.severe("Unexpected error while filtering phrases.");
                e.printStackTrace();
            }
        }

        return true;
    }


    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public int getThreadId() {
        return threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }

}
