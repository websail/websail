package edu.northwestern.websail.kpe.data.paper;

import edu.northwestern.websail.ds.tuple.IntegerPair;
import edu.northwestern.websail.kpe.data.S2PaperUtil;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author NorThanapon, csbhagav
 * @since 8/13/15
 * <p/>
 * Represent a paper with data needed for this system.
 * S2 format as of Jul 15
 * <p/>
 * [u'bodyText',
 * u'citationRate',
 * u'classification',
 * u'numCitedBy',
 * u'followOn',
 * u'references',
 * u'year',
 * u'id',
 * u'numFollowOn',
 * u'paperAbstract',
 * u'title',
 * u'citedBy',
 * u'keyPhrases',
 * u'totalCitedByCount',
 * u'shortVenue',
 * u'citationContexts',
 * u'authors',
 * u'citationRank',
 * u'venue',
 * u'sourceInfo',
 * u'mainConcepts',
 * u'citationRankPercentile']
 */
public class S2Paper {

    // postprocessing data
    final HashMap<String, IntegerPair> sectionOffsets;
    // fields from json file
    String id;
    String title;
    Integer year;
    String paperAbstract;
    String bodyText;
    String[] authors;
    ArrayList<S2CitationContext> citationContexts;
    ArrayList<S2CitationContext> preferredCitationContexts;

    @Deprecated //for Json Deserialization
    public S2Paper() {
        sectionOffsets = new HashMap<String, IntegerPair>();
    }

    public S2Paper(String id, String title, Integer year, String paperAbstract, String bodyText,
                   String[] authors, ArrayList<S2CitationContext> citationContexts) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.paperAbstract = paperAbstract;
        this.bodyText = bodyText;
        this.authors = authors;
        this.citationContexts = citationContexts;
        sectionOffsets = new HashMap<String, IntegerPair>();
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getYear() {
        return year;
    }

    public String getPaperAbstract() {
        return paperAbstract;
    }

    public void setPaperAbstract(String paperAbstract) {
        this.paperAbstract = paperAbstract;
    }

    public String getBodyText() {
        if(bodyText==null) return ""; return bodyText;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }

    public String[] getAuthors() {
        return authors;
    }

    public ArrayList<S2CitationContext> getCitationContexts() {
        if (citationContexts == null) {
            citationContexts = new ArrayList<S2CitationContext>();
        }
        return citationContexts;
    }

    public void setCitationContexts(ArrayList<S2CitationContext> citationContexts) {
        this.citationContexts = citationContexts;
    }

    public HashMap<String, IntegerPair> getSectionOffsets() {
        return sectionOffsets;
    }

    public ArrayList<S2CitationContext> getPreferredCitationContexts() {
        if (preferredCitationContexts == null) {
            this.preferredCitationContexts = S2PaperUtil.getPreferredCitationContexts(this.getCitationContexts());
        }
        return preferredCitationContexts;
    }

    public String getCitationParagraphs() {
        ArrayList<S2CitationContext> contexts = this.getPreferredCitationContexts();
        if (contexts.size() == 0) return "";
        StringBuilder sb = new StringBuilder();
        for (S2CitationContext context : contexts) {
            sb.append(context.getCitationString()).append('\n');
        }
        return sb.toString();
    }

}
