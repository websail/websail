package edu.northwestern.websail.kpe.kpfe;

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.classify.data.CommonNominal;
import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.data.Instances;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.ds.indexer.HashIndexer;
import edu.northwestern.websail.ds.indexer.Indexer;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.phrase.Phrase;

import java.util.Properties;

/**
 * @author NorThanapon
 * @since 11/18/14
 */
@PipelineConfigInfo()
@PipelineBUTInfo(
        consumes = {KPEBUT.Phrases.class},
        produces = {ClassifyBUT.BuckInstances.class}
)
public class InstanceInitalizer implements PipelineExecutable {
    public static final Feature classFeature = new Feature("label", "isKey", Feature.Type.NOMINAL, CommonNominal.boolMap);
    private final static Object lock = new Object();
    private final static String featureOutputKey = "KPE.InstanceFeatureKey";
    protected int threadId = 0;
    private Indexer<Feature> features;
    private int classIndex;

    public InstanceInitalizer() {
    }

    @SuppressWarnings("unchecked")
    public static Indexer<Feature> envFeatures(PipelineEnvironment env) {
        return (Indexer<Feature>) env.getPipelineGlobalOuputs().get(featureOutputKey);
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        synchronized (lock) {
            if (!env.getPipelineGlobalOuputs().containsKey(featureOutputKey)) {
                features = new HashIndexer<Feature>(Feature.class);
                features.add(classFeature);
                classIndex = features.gadIndexOf(classFeature);
                env.getPipelineGlobalOuputs().put(featureOutputKey, features);
            } else {
                features = envFeatures(env);
                classIndex = features.indexOf(classFeature);
            }
        }
        return true;
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        for (BuckUnit unit : prev.units()) {
            Instances instances = new Instances(features);
            instances.setClassIndex(classIndex);
            if (unit.isNotOkay())
                continue;
            FastList<Phrase> phrases = unit.get(KPEBUT.Phrases.class);
            for (Phrase phr : phrases) {
                Instance<Phrase> instance = new Instance<Phrase>(features, phr);
                instances.getInstances().add(instance);
                instance.setValue(classIndex, phr.isKey() + "");
            }
            unit.set(ClassifyBUT.BuckInstances.class, instances);
        }
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public int getThreadId() {
        return threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }
}
