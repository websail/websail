package edu.northwestern.websail.kpe.pe.filter;


import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.kpe.pe.helper.FilterTerms;
import edu.northwestern.websail.kpe.pe.helper.ValidPOSTags;
import edu.northwestern.websail.text.data.TaggedToken;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.data.TokenSpan;

import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 * @since 11/19/14
 */
@PipelineConfigInfo()
@PipelineBUTInfo(
        consumes = {KPEBUT.Phrases.class},
        produces = {KPEBUT.Phrases.class}
)
public class PhraseStripper implements PipelineExecutable {
    public static final Logger logger = Logger.getLogger(PhraseStripper.class
            .getName());

    private int threadId = 0;

    public PhraseStripper() {
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        for (BuckUnit unit : prev.units()) {
            if (unit.isNotOkay())
                continue;
            try {
                FastList<Phrase> phrases = unit.get(KPEBUT.Phrases.class);
                FastList<Phrase> filteredPhrases = new FastList<Phrase>();
                for (Phrase phrase : phrases) {
                    if (strip(phrase)) {
                        filteredPhrases.add(phrase);
                    }
                }
                unit.set(KPEBUT.Phrases.class, filteredPhrases);
            } catch (Exception e) {
                logger.severe("Unexpected error while filtering mention.");
                e.printStackTrace();
            }
        }
        return true;
    }

    private boolean strip(Phrase phrase) {
        TokenSpan span = phrase.getSpan();
        List<? extends Token> tokens = span.getTokens();

        if (tokens.size() == 1 && (tokens.get(0).getText().length() <= 1))
            return false;
        int startValid = 0;
        int endValid = tokens.size();
        while (startValid < tokens.size()) {
            Token t = tokens.get(startValid);
            TaggedToken tt = (TaggedToken) t;
            boolean news = t.getText().equals("new") && t.getEndOffset() - t.getStartOffset() > t.getText().length();
            boolean badStart = FilterTerms.badStartSet.contains(t.getText());
            boolean atLeastOneChar = t.getText().matches(".*[A-Za-z]+.*");
            boolean validPOS = ValidPOSTags.validStartPOS.contains(tt.getPOS());
            if ((!badStart || news) && atLeastOneChar && validPOS) {
                break;
            }
            startValid++;
        }
        while (endValid > 0) {
            Token t = tokens.get(endValid - 1);
            TaggedToken tt = (TaggedToken) t;
            boolean lenGTOne = t.getText().length() > 1;
            boolean alphanumeric = t.getText().matches("^[\\p{L}\\p{N}\\+]+$");
            boolean numberWithDot = t.getText().matches("^[0-9]+\\.[0-9]+$");
            boolean badEnd = FilterTerms.badEndSet.contains(t.getText());
            boolean validPOS = ValidPOSTags.validEndPOS.contains(tt.getPOS());
            if (validPOS && !badEnd && !numberWithDot && (alphanumeric || lenGTOne)) {
                break;
            }
            endValid--;
        }
        return !(startValid >= endValid || startValid >= tokens.size() || endValid <= 0)
                && subMention(phrase, startValid, endValid);
    }

    private boolean subMention(Phrase phrase, int start, int end) {
        TokenSpan span = phrase.getSpan();
        if (start == 0 && end == span.getTokens().size())
            return true;
        TokenSpan sub = span.subSpan(start, end);
        if (sub == null)
            return false;
        TokenSpan before = span.subSpan(0, start);
        TokenSpan after = span.subSpan(end, span.getTokens().size());
        Token leftToken = phrase.getBefore();
        Token rightToken = phrase.getAfter();
        if (before != null) {
            leftToken = before.getTokens().get(before.getTokens().size() - 1);
        }
        if (after != null) {
            rightToken = after.getTokens().get(0);
        }
        phrase.setSurfaceForm(phrase.getSurfaceForm().substring(
                sub.getStartOffset() - span.getStartOffset(),
                sub.getEndOffset() - span.getStartOffset()));
        phrase.setBefore(leftToken);
        phrase.setAfter(rightToken);
        phrase.setSpan(sub);
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public int getThreadId() {
        return threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }
}
