package edu.northwestern.websail.kpe.kpfe;

import edu.northwestern.websail.classify.data.Feature;
import edu.northwestern.websail.classify.data.Instance;
import edu.northwestern.websail.classify.pipeline.ClassifyBUT;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.buck.S2PaperBuck;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.text.corpus.data.lucene.IndexAggregator;
import edu.northwestern.websail.text.corpus.data.lucene.collector.ByteTermFreqCollector;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.util.TokenUtil;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.util.*;


/**
 * @author NorThanapon
 * @since 11/19/14
 */
@PipelineConfigInfo(
        requiredResources = {PEVResourceType.INDEX_READER}
)
@PipelineBUTInfo(
        consumes = {ClassifyBUT.BuckInstances.class}
)
public class FrequencyKPFE extends KPFeatureExtractor {

    public static final Feature idfF = new Feature("freq", "idf", Feature.Type.NUMBER);
    public static final Feature logtfF = new Feature("freq", "logtf", Feature.Type.NUMBER);
    public static final Feature tfidfF = new Feature("freq", "tfidf", Feature.Type.NUMBER);
    public static final Feature entropyF = new Feature("freq", "entropy", Feature.Type.NUMBER);
    public static final Feature relFOCCF = new Feature("location", "rel1stOcc", Feature.Type.NUMBER);
    public static final HashMap<String, Integer> dfCache = new HashMap<String, Integer>();
    public static final HashMap<String, Double> entropyCache = new HashMap<String, Double>();
    private static final Object lock = new Object();
    private static final Feature[] features = {idfF, logtfF, tfidfF, entropyF, relFOCCF};
    private static Integer threadCount = 0;
    private static boolean registered = false;
    private DirectoryReader reader;
    private IndexSearcher searcher;
    private QueryParser qp;
    private HashMap<String, Integer[]> tfCache;
    private ArrayList<List<? extends Token>> fullTextTokens;
    private int totalDocs;
    private String currentSource;

    public FrequencyKPFE() {
        curValues = new double[features.length];
        Arrays.fill(curValues, Instance.MISSING);
        synchronized (lock) {
            threadCount++;
        }
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        if (!super.init(env, config)) return false;
        reader = (DirectoryReader) env.getResource(PEVResourceType.INDEX_READER);
        if (reader == null) {
            PipelineEnvironment.logResourceNotFound(PEVResourceType.INDEX_READER, logger);
        }
        totalDocs = reader.numDocs();
        searcher = new IndexSearcher(reader);
        WhitespaceAnalyzer analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);
        qp = new QueryParser(Version.LUCENE_40, IndexAggregator.DOC_CONTENT_FIELD, analyzer);
        qp.setDefaultOperator(QueryParser.Operator.AND);
        qp.setPhraseSlop(0);
        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        synchronized (lock) {
            threadCount--;
            if (threadCount == 0) {
                dfCache.clear();
                entropyCache.clear();
            }
        }
        return super.close(env, config);
    }

    @Override
    protected void extract(Instance<Phrase> phraseInstance) throws Exception {
        String phrase = QueryParser.escape(
                TokenUtil.tokens2Str(phraseInstance.getSource().getSpan().getTokens()));
        double[] result = search(phrase);
        double df = result[0];
        double entropy = result[1];
        double idf = df != 0 ? Math.log10(this.totalDocs / df) : 0;
        int totalTF = 0;
        double maxRelativeFirstOcc = 0;
        for (int iHayStack = 0; iHayStack < this.fullTextTokens.size(); iHayStack++) {
            List<? extends Token> hayStack = this.fullTextTokens.get(iHayStack);
            if (hayStack.size() == 0) continue;
            Integer[] info = this.findPhrases(phraseInstance.getSource(), iHayStack);
            int tf = info[0];
            int index = info[1];
            totalTF += tf;
            if (tf == 0 || index == -1)
                continue;
            double rfocc = Math.pow((1 - index / (double) hayStack.size()), tf);
            if (rfocc > 1) {
                logger.warning(currentSource + ": " + phraseInstance.getSource().getSurfaceForm() +
                        " has RFOCC greater than one for tokens: " + hayStack);
            }
            if (Double.isNaN(rfocc) || Double.isInfinite(rfocc)) {
                logger.warning(currentSource + ": " + phraseInstance.getSource().getSurfaceForm() +
                        " has infinity RFOCC for tokens: " + hayStack);
            }
            if (rfocc > maxRelativeFirstOcc) {
                maxRelativeFirstOcc = rfocc;
            }
        }
        if (totalTF == 0) {
            logger.warning(currentSource + ": " + phraseInstance.getSource().getSurfaceForm() +
                    " has 0 count.");
        }
        curValues[0] = idf;
        curValues[1] = totalTF == 0 ? 0 : Math.log10(totalTF + 1);
        curValues[2] = totalTF == 0 ? 0 : curValues[1] * curValues[0];
        curValues[3] = entropy;
        curValues[4] = maxRelativeFirstOcc;
    }

    private double[] search(String phrase)
            throws ParseException, IOException {
        Integer searchResult = dfCache.get(phrase);
        Double entropy = entropyCache.get(phrase);
        if (searchResult == null || entropy == null) {
            ByteTermFreqCollector collector = new ByteTermFreqCollector(this.reader);
            Query q = qp.parse("\"" + phrase + "\"");
            searcher.search(q, collector);
            searchResult = collector.getTotalDocs();
            entropy = computeEntropy(collector.getTotalCount(), collector.getTf());
            synchronized (dfCache) {
                dfCache.put(phrase, searchResult);
                entropyCache.put(phrase, entropy);
            }
        }
        double[] result = new double[2];
        result[0] = searchResult;
        result[1] = entropy;
        return result;
    }

    private double computeEntropy(long totalCount, HashMap<?, Long> tfs) {
        double h = 0;
        for (Map.Entry<?, Long> entry : tfs.entrySet()) {
            double p = entry.getValue() / (double) totalCount;
            h += p * (Math.log(1 / p) / Math.log(2));
        }
        return h;
    }

    private Integer[] findPhrases(Phrase phrase, int iHayStack) {
        List<? extends Token> hayStack = this.fullTextTokens.get(iHayStack);
        Integer[] info = new Integer[2];
        List<? extends Token> tokens = phrase.getSpan().getTokens();
        String key = TokenUtil.tokens2Str(tokens) + "@stack_" + iHayStack;
        if (tfCache.containsKey(key)) {
            return tfCache.get(key);
        }
        int index = TokenUtil.indexOfTokensKMP(hayStack, tokens, 0);
        if (index == -1) {
            info[0] = 0;
            info[1] = -1;
        } else {
            info[0] = 1;
            info[1] = index;
            while ((index = TokenUtil.indexOfTokensKMP(hayStack, tokens, index + 1)) != -1) {
                info[0]++;
            }
        }
        tfCache.put(key, info);
        return info;
    }

    @Override
    protected Feature[] features() {
        return features;
    }

    @Override
    protected boolean initForBuck(PipelineEnvironment env, S2PaperBuck prev) {
        tfCache = new HashMap<String, Integer[]>();
        setFullText(prev);
        return true;
    }

    private void setFullText(S2PaperBuck buck) {
        this.currentSource = buck.getSource();
        fullTextTokens = new ArrayList<List<? extends Token>>();
        for (BuckUnit unit : buck.units()) {
            List<TokenSpan> sentences = unit.get(KPEBUT.Sentences.class);
            List<Token> paperTokens = new ArrayList<Token>();
            for (TokenSpan span : sentences) {
                paperTokens.addAll(span.getTokens());
            }
            fullTextTokens.add(paperTokens);
        }
    }

    @Override
    protected boolean initForUnit(BuckUnit unit) {
        return true;
    }

    @Override
    protected boolean register() {
        return registered;
    }

    @Override
    protected void registered() {
        registered = true;
    }
}
