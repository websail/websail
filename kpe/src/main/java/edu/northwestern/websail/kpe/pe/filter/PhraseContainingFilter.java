package edu.northwestern.websail.kpe.pe.filter;

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.pipeline.annotation.PipelineBUTInfo;
import edu.northwestern.websail.core.pipeline.annotation.PipelineConfigInfo;
import edu.northwestern.websail.core.pipeline.arch.PipelineExecutable;
import edu.northwestern.websail.core.pipeline.buck.Buck;
import edu.northwestern.websail.core.pipeline.buck.BuckUnit;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.kpe.data.buck.KPEBUT;
import edu.northwestern.websail.kpe.data.phrase.Phrase;
import edu.northwestern.websail.kpe.pe.helper.FilterTerms;
import edu.northwestern.websail.text.util.StopWords;

import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 * @since 11/19/14
 */
@PipelineConfigInfo()
@PipelineBUTInfo(
        consumes = {KPEBUT.Phrases.class},
        produces = {KPEBUT.Phrases.class}
)
public class PhraseContainingFilter implements PipelineExecutable {
    public static final Logger logger = Logger
            .getLogger(PhraseContainingFilter.class.getName());


    private int threadId = 0;

    public PhraseContainingFilter() {
    }

    @Override
    public boolean init(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public boolean execute(PipelineEnvironment env, Buck prev) {
        for (BuckUnit unit : prev.units()) {
            if (unit.isNotOkay())
                continue;
            try {
                FastList<Phrase> phrases = unit
                        .get(KPEBUT.Phrases.class);
                FastList<Phrase> filteredPhrases = new FastList<Phrase>();
                for (Phrase phrase : phrases) {
                    if (!phrase.getSurfaceForm().matches(".*[A-Za-z]+.*"))
                        continue;
                    if (phrase.getSurfaceForm().matches("[0-9]+.?"))
                        continue;
                    if (phrase.getSurfaceForm().matches(".*[0-9]{5}.*"))
                        continue;
                    if (phrase.getSurfaceForm().matches(".*&.*;.*"))
                        continue;
                    if (phrase.getSurfaceForm().contains("---"))
                        continue;
                    if (phrase.getSurfaceForm().contains("\""))
                        continue;
                    if (phrase.getSurfaceForm().contains("'"))
                        continue;
                    if (StopWords._words.contains(phrase.getSurfaceForm().toLowerCase()))
                        continue;
                    String key = FilterTerms.tokensToKey(phrase.getSpan().getTokens());
                    boolean containBad = false;
                    for (String bp : FilterTerms.badPhrases) {
                        if (phrase.getSurfaceForm().contains(bp)) {
                            containBad = true;
                            break;
                        }
                    }
                    if (containBad)
                        continue;
                    for (String bk : FilterTerms.badKeys) {
                        if (key.contains(bk)) {
                            containBad = true;
                            break;
                        }
                    }
                    if (containBad)
                        continue;
                    if (phrase.getAfter().getText().equalsIgnoreCase("et"))
                        continue;
                    if (phrase.getSurfaceForm().matches(
                            "[A-Z].+( and [A-Z].+)?(,)? (et al\\.)?(,)?[0-9]{4}")) {
                        if (phrase.getBefore().getText().equals("(")
                                || phrase.getAfter().getText().equals(";")
                                || phrase.getAfter().getText().equals(")"))
                            continue;
                    }
                    filteredPhrases.add(phrase);
                }
                unit.set(KPEBUT.Phrases.class, filteredPhrases);
            } catch (Exception e) {
                logger.severe("Unexpected error while filtering mention.");
                e.printStackTrace();
            }
        }

        return true;
    }

    @Override
    public boolean close(PipelineEnvironment env, Properties config) {
        return true;
    }

    @Override
    public int getThreadId() {
        return threadId;
    }

    @Override
    public void setThreadId(int id) {
        this.threadId = id;
    }

}
