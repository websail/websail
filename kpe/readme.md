# Readme

## Requirements
1. Java 7
2. [maven](http://maven.apache.org)
3. [SRILM](http://www.speech.sri.com/projects/srilm/) (`ngram-count` should be in a command path)
4. Stanford Core NLP model jar file (available [here](http://nlp.stanford.edu/software/corenlp.shtml) in the main package)

## Compile
1. Go to `websail` directory (parent of this directory)
2. Run `mvn clean package -pl kpe -am`
3. Copy `kpe/target/websail-kpe-<version>-jar-with-dependencies.jar` to desired directory  

## Run
The jar file includes all of the Java dependencies (except Core NLP's models). We can use it to run any `main` class in this sub-module. 
To run, ```java -XX:+UseG1GC -Xmx<memory> -cp <path-to-websail-kpe-jar>:<path-to-core-nlp-model-jar> <main-class>```. For example, ```java -XX:+UseG1GC -Xmx100g -cp websail-kpe-0.0.1-jar-with-dependencies.jar:corenlp-models.jar edu.northwestern.websail.kpe.sandbox.Sandbox``` runs ```Sanbox``` class given 100 GB of memory, and both `websail-kpe-0.0.1-jar-with-dependencies.jar` and `corenlp-models.jar` are in current directory.

## Preprocessing
`websail-kpe` required 4 main resources: (1) Lucene Index of the corpus, (2) LM model of the corpus, (3) LM models of the papers, and (4) NGram count file. Most of these can be built using class in this sub-module. The main classes that build these resources (described below) have  hard-code directories (mostly output, paper json, and citation context). You will need to *change* each of them to the right path, and *recompile*.

Since this paths are rarely changed in our environment, we fix them into the code. You can set path variables to take command line arguments (`args[index]`).

1. Lucene Index of the corpus: `edu.northwestern.websail.kpe.main.preprocessing.BuildACLLuceneIndex`
2. LM model of the corpus: `edu.northwestern.websail.kpe.main.preprocessing.BuildACLCorpusNGramModel`
3. LM models of the papers:
`edu.northwestern.websail.kpe.main.preprocessing.BuildACLPaperNGramModels`
4. NGram count file: This is a bit different.
    1. Building LM model of the corpus will also output a big text file that contains all of the  titles, abstracts, body texts, and citation contexts. We will need this file, named `acl_ncase_stem.txt`.
    2. Run `ngram-count -order 5 -text <the big text file> -write <output count file>`
    3. In the code, change `countFile` to `<output count file>`
    4. Run as the class
    
## Running Sandbox
The sandbox class `edu.northwestern.websail.kpe.sandbox.Sandbox` is a generic KPE main class that loads papers, citations, and training data. Then it runs *pipeline* with a subset of papers. 

The example of *pipeline* configuration is located at `pconfig/sandbox/sandbox.xml` and `pconfig/sandbox/sandbox.config`. The example includes almost the whole pipeline of current KPE code, starting from phrase extraction to dedup key phrases.

## TO-DO
* Add dictionary building code
* Add fuzzy matching evaluation code