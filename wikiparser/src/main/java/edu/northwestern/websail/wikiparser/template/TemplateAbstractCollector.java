package edu.northwestern.websail.wikiparser.template;


import edu.northwestern.websail.wikiparser.model.WikiTemplate;
import org.sweble.wikitext.engine.PageTitle;

public abstract class TemplateAbstractCollector {

    public abstract boolean exist(String templateFullTitle);

    public abstract void set(String templateFullTitle, WikiTemplate template);

    public abstract WikiTemplate get(String templateFullTitle);

    public abstract WikiTemplate get(String templateFullTitle, PageTitle wp);

    public abstract void serialize(String config) throws Exception;

    public abstract TemplateAbstractCollector deserialize(String config) throws Exception;

    public abstract int numTemplate();

    public WikiTemplate getFinalTemplate(String templateFullTitle) {
        if (!this.exist(templateFullTitle)) return null;
        WikiTemplate template = this.get(templateFullTitle);
        if (template == null) return null;
        if (template.isRedirecting()) return this.getFinalTemplate(template.getRedirectedTemplateName());
        else return template;
    }

    public WikiTemplate getFinalTemplate(String templateFullTitle, PageTitle p) {
        WikiTemplate template = this.get(templateFullTitle, p);
        if (template == null) return null;
        if (template.isRedirecting()) return this.getFinalTemplate(template.getRedirectedTemplateName(), p);
        else return template;
    }

}
