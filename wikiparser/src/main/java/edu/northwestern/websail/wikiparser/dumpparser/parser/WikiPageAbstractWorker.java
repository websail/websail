package edu.northwestern.websail.wikiparser.dumpparser.parser;

/**
 * @author NorThanapon
 */

import edu.northwestern.websail.wikiparser.dumpparser.dao.WikiPage;

public abstract class WikiPageAbstractWorker implements Runnable {
    protected WikiPage page;

    public WikiPage getPage() {
        return page;
    }

    public void setPage(WikiPage page) {
        this.page = page;
    }

    /*
     * This can carry information from this object to the next page
     */
    public abstract WikiPageAbstractWorker newInstance();
}
