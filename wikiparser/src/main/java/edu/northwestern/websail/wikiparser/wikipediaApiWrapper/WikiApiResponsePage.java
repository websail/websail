package edu.northwestern.websail.wikiparser.wikipediaApiWrapper;

/**
 * @author csbhagav
 */
public class WikiApiResponsePage {
    Integer pgId;
    String title;
    String wikiText;

    public String getWikiText() {
        return wikiText;
    }

    public void setWikiText(String wikiText) {
        this.wikiText = wikiText;
    }

    public Integer getPgId() {
        return pgId;
    }

    public void setPgId(Integer pgId) {
        this.pgId = pgId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
