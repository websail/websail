package edu.northwestern.websail.wikiparser.model;


import com.google.gson.Gson;
import com.mongodb.*;
import org.jongo.*;
import org.jongo.bson.BsonDocument;
import org.jongo.marshall.jackson.JacksonMapper;


public class WikiExtractedPageAdapter {

    private DB db;
    private Jongo jongo;
    private MongoCollection pages;
    private String collectionName = "page";
    private ResultHandler<WikiExtractedPage> handler;

    public WikiExtractedPageAdapter(DB db) {
        this.db = db;
        final Mapper mapper = new JacksonMapper.Builder().build();
        this.jongo = new Jongo(db, mapper);
        this.pages = jongo.getCollection(collectionName);
        final Gson gson = new Gson();
        handler = new ResultHandler<WikiExtractedPage>() {
            public WikiExtractedPage map(DBObject dbo) {
                try {
                    return mapper.getUnmarshaller().unmarshall((BsonDocument) dbo, WikiExtractedPage.class);
                } catch (org.jongo.marshall.MarshallingException e) {
                    return gson.fromJson(dbo.toString(), WikiExtractedPage.class);
                }
            }
        };
    }

    public WikiExtractedPageAdapter(DB db, String collectionName) {
        this.db = db;
        this.jongo = new Jongo(db);
        this.collectionName = collectionName;
        this.pages = jongo.getCollection(collectionName);
    }

    public WikiExtractedPage getPageWithOId(String oId) {
        return pages.findOne(Oid.withOid(oId)).map(this.handler);
    }

    public WikiExtractedPage getPage(int pageId) {
        return pages.findOne("{'title.id':" + pageId + "}").map(this.handler);
    }

    public WikiExtractedPage getPage(String title) {
        return pages.findOne("{'title.title':'" + title + "'}").map(this.handler);
    }

    public Iterable<WikiExtractedPage> getAllNonRedirectPages() {
        return pages.find("{'title.redirecting':false}").map(this.handler);
    }

    public Iterable<WikiExtractedPage> getAllNonRedirectPages(int skip) {
        return pages.find("{'title.redirecting':false}").skip(skip).map(this.handler);
    }

    public Iterable<WikiExtractedPage> getAllNonRedirectPages(String projectedFields) {
        return pages.find("{'title.redirecting':false}").projection(projectedFields).map(this.handler);
    }

    public Iterable<WikiExtractedPage> getAllNonRedirectPages(String projectedFields, int skip) {
        return pages.find("{'title.redirecting':false}").projection(projectedFields).skip(skip).map(this.handler);
    }

    public DBCursor getAllNonRedirectedPageCursor() {
        DBCollection collection = db.getCollection(this.collectionName);
        return collection.find(new BasicDBObject("title.redirecting", false));
    }

    public DBCursor getAllNonRedirectedPageCursor(Integer limit) {
        DBCollection collection = db.getCollection(this.collectionName);
        return collection.find(new BasicDBObject("title.redirecting", false)).limit(limit);
    }

    public DBCursor getSamplePageCursor(Integer pgId) {
        DBCollection collection = db.getCollection(this.collectionName);
        return collection.find(new BasicDBObject("title.id", pgId));
    }

    public DB getDb() {
        return db;
    }

    public void setDb(DB db) {
        this.db = db;
    }

    public Jongo getJongo() {
        return jongo;
    }

    public void setJongo(Jongo jongo) {
        this.jongo = jongo;
    }

    public MongoCollection getPages() {
        return pages;
    }

    public void setPages(MongoCollection pages) {
        this.pages = pages;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }


}
