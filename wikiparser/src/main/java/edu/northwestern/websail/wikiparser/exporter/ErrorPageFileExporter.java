package edu.northwestern.websail.wikiparser.exporter;

import edu.northwestern.websail.wikiparser.model.WikiExtractedPage;
import edu.northwestern.websail.wikiparser.wikitextparser.utils.TextUtils;

import java.util.logging.Logger;

/**
 * @author NorThanapon
 */
public class ErrorPageFileExporter implements PageExtractionResultDelegate {
    public static Logger logger = Logger.getLogger(SimpleJsonPageExporter.class.getName());
    private String errorPath;

    public ErrorPageFileExporter(String errorPath) {
        this.errorPath = errorPath;
    }

    @Override
    public boolean doneExtractingPage(WikiExtractedPage page) {
        return true;
    }

    @Override
    public boolean errorExtractingPage(int titleId, String titleName,
                                       String wikiText) {
        TextUtils.writeText(
                errorPath + "/" + titleId + "-" + titleName.replaceAll(" ", "_") + ".wikitext",
                wikiText);
        return true;
    }
}
