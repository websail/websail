package edu.northwestern.websail.wikiparser.model;

/**
 * @author NorThanapon
 */

public class WikiTitle {
    private int id;
    private String language;
    private String title;
    private boolean redirecting;
    private WikiTitle redirectedTitle;
    private int namesapce;

    public WikiTitle() {
    }

    public WikiTitle(String language, int id, String title) {
        this(language, id, title, false, null);
    }

    public WikiTitle(String language, int id, String title, boolean redirecting, WikiTitle redirectedTitle) {
        this.id = id;
        this.language = language;
        this.title = title;
        this.redirecting = redirecting;
        this.redirectedTitle = redirectedTitle;
        this.setNamesapce(0);
    }

    public WikiTitle(String language, int id, String title, boolean redirecting, WikiTitle redirectedTitle, int namespace) {
        this.id = id;
        this.language = language;
        this.title = title;
        this.redirecting = redirecting;
        this.redirectedTitle = redirectedTitle;
        this.setNamesapce(namespace);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isRedirecting() {
        return redirecting;
    }

    public void setRedirecting(boolean redirecting) {
        this.redirecting = redirecting;
    }

    public WikiTitle getRedirectedTitle() {
        return redirectedTitle;
    }

    public void setRedirectedTitle(WikiTitle redirectedTitle) {
        this.redirectedTitle = redirectedTitle;
    }

    public int getNamesapce() {
        return namesapce;
    }

    public void setNamesapce(int namesapce) {
        this.namesapce = namesapce;
    }

    public String toString() {
        return this.language + ":" + this.title + " (ns:" + this.namesapce + ")";
    }
}
