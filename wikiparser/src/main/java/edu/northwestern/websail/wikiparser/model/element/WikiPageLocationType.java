package edu.northwestern.websail.wikiparser.model.element;


/**
 * @author NorThanapon
 */
public enum WikiPageLocationType {

    BEFORE_OVERVIEW,
    OVERVIEW,
    MAIN,
    MAIN_TABLE,
    MAIN_LIST,
    TEMPLATE,
    OTHER_TABLE
}
