package edu.northwestern.websail.wikiparser.wikitextparser.sweblewrapper;

/**
 * @author NorThanapon
 * Wrapper Class of Sweble Engine http://sweble.org
 * Paper: http://dl.acm.org/citation.cfm?id=2491057 
 */

import de.fau.cs.osr.ptk.common.AstVisitor;
import de.fau.cs.osr.utils.StringUtils;
import edu.northwestern.websail.wikiparser.model.WikiExtractedPage;
import edu.northwestern.websail.wikiparser.model.WikiTitle;
import edu.northwestern.websail.wikiparser.model.WikiTitleFactory;
import edu.northwestern.websail.wikiparser.model.element.*;
import edu.northwestern.websail.wikiparser.wikitextparser.utils.ExtractedPageUtils;
import edu.northwestern.websail.wikiparser.wikitextparser.utils.TextUtils;
import org.sweble.wikitext.engine.PageTitle;
import org.sweble.wikitext.engine.config.WikiConfig;
import org.sweble.wikitext.engine.nodes.*;
import org.sweble.wikitext.engine.utils.EngineAstTextUtils;
import org.sweble.wikitext.parser.nodes.*;
import org.sweble.wikitext.parser.parser.LinkTargetException;
import org.sweble.wikitext.parser.utils.StringConversionException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

public class SweblePlainTextProcessor extends AstVisitor<WtNode> implements CompleteEngineVisitorNoReturn {
    protected static final Set<String> blockElements = new HashSet<String>();

    static {
        // left out del and ins, added table elements
        blockElements.add("div");
        blockElements.add("address");
        blockElements.add("blockquote");
        blockElements.add("center");
        blockElements.add("dir");
        blockElements.add("div");
        blockElements.add("dl");
        blockElements.add("fieldset");
        blockElements.add("form");
        blockElements.add("h1");
        blockElements.add("h2");
        blockElements.add("h3");
        blockElements.add("h4");
        blockElements.add("h5");
        blockElements.add("h6");
        blockElements.add("hr");
        blockElements.add("isindex");
        blockElements.add("menu");
        blockElements.add("noframes");
        blockElements.add("noscript");
        blockElements.add("ol");
        blockElements.add("p");
        blockElements.add("pre");
        blockElements.add("table");
        blockElements.add("ul");
        blockElements.add("center");
        blockElements.add("caption");
        blockElements.add("tr");
        blockElements.add("td");
        blockElements.add("th");
        blockElements.add("colgroup");
        blockElements.add("thead");
        blockElements.add("tbody");
        blockElements.add("tfoot");
        blockElements.add("span");
    }

    protected static final Set<String> blockedElements = new HashSet<String>();

    static {
        // tag to be ignored
        blockElements.add("noinclude"); //automatically excluded by sweble, added here just in case
    }

    protected StringBuilder sbText;
    protected StringBuilder sbTable;
    protected ArrayList<WikiSection> sections;
    protected ArrayList<WikiParagraph> paragraphs;
    protected ArrayList<WikiLink> internalLinks;
    protected ArrayList<WikiLink> interWikiLinks;
    protected ArrayList<WikiLink> categoryLinks;
    protected ArrayList<WikiHTMLElement> tableHTMLs;
    protected WikiConfig config;
    protected WikiTitleFactory titleFactory;
    protected WikiTitle redirectTo = null;
    protected WikiPageLocationType locType;
    protected EngineNodeFactory nf;
    protected EngineAstTextUtils tu;
    protected WikiExtractedPage page;
    private int indentLevel = 0;
    private int currentSectionNumber = -1;
    private Logger logger = Logger.getLogger(SweblePlainTextProcessor.class.getName());

    // IMPLEMENTED ELEMENTS
    private HashSet<String> interWikiPrefixSet;

    public SweblePlainTextProcessor(WikiConfig config, WikiExtractedPage page) {
        sbText = new StringBuilder();
        sbTable = new StringBuilder();
        internalLinks = new ArrayList<WikiLink>();
        interWikiLinks = new ArrayList<WikiLink>();
        categoryLinks = new ArrayList<WikiLink>();
        sections = new ArrayList<WikiSection>();
        paragraphs = new ArrayList<WikiParagraph>();
        tableHTMLs = new ArrayList<WikiHTMLElement>();
        this.config = config;
        this.titleFactory = new WikiTitleFactory();
        locType = WikiPageLocationType.BEFORE_OVERVIEW;
        nf = config.getNodeFactory();
        tu = config.getAstTextUtils();
        this.page = page;
        this.interWikiPrefixSet = SwebleUtils.extractInterWikiPrefixSet(config);
    }

    /**
     * If the cell content is only one paragraph, the content of the paragraph
     * is returned. Otherwise the whole cell content is returned. This is done
     * to render cells with a single paragraph without the paragraph tags.
     */
    protected static WtNode getCellContent(WtNodeList body) {
        if (body.size() >= 1 && body.get(0) instanceof WtParagraph) {
            boolean ok = true;
            for (int i = 1; i < body.size(); ++i) {
                if (!(body.get(i) instanceof WtNewline)) {
                    ok = false;
                    break;
                }
            }
            if (ok)
                body = (WtParagraph) body.get(0);
        }
        return body;
    }

    @Override
    protected boolean before(WtNode node) {
        // This method is called by go() before visitation starts
        return super.before(node);
    }

    @Override
    protected Object after(WtNode node, Object result) {
        // This method is called by go() after visitation has finished
        // The return value will be passed to go() which passes it to the caller


        String text = sbText.toString();
        page.setCategoryLinks(categoryLinks);
        page.setInternalLinks(internalLinks);
        page.setInterWikiLinks(interWikiLinks);
        page.setParagraphs(paragraphs);
        page.setSections(sections);
        page.setPlainText(text);
        page.setTableHTMLs(new ArrayList<WikiHTMLElement>());
        if (redirectTo != null) {
            page.getTitle().setRedirecting(true);
            page.getTitle().setRedirectedTitle(redirectTo);
        }
        ExtractedPageUtils.recoverOverviewParagraph(page);
        // ExtractedPageUtils.printPage(page);

        return page;
    }

    @Override
    public void visit(EngPage page) {
        iterate(page);
    }

    @Override
    public void visit(WtSection s) {
        int sectionNum = this.sections.size();
        if (sectionNum == 0) locType = WikiPageLocationType.MAIN;
        int start = sbText.length();
        iterate(s.getHeading());
        String title = getNodeTextContent(s.getHeading());
        int endHeading = sbText.length();
        WikiSection section = new WikiSection(start, 0, locType, s.getLevel(), endHeading, sectionNum);
        currentSectionNumber = sectionNum;
        this.sections.add(section);
        iterate(s.getBody());
        int end = sbText.length();
        section.setEndOffset(end);
        section.setSectionTitle(title);
    }

    @Override
    public void visit(WtHeading heading) {
        //section heading
        iterate(heading);
    }

    @Override
    public void visit(WtBody body) {
        //section body
        iterate(body);
    }

    @Override
    public void visit(WtParagraph p) {

        int start = sbText.length();
        int pNum = this.paragraphs.size();
        if (this.sections.size() == 0 &&
                (locType == WikiPageLocationType.MAIN ||
                        locType == WikiPageLocationType.BEFORE_OVERVIEW))
            locType = WikiPageLocationType.OVERVIEW;
        WikiParagraph paragraph = new WikiParagraph(start, 0, pNum, locType);
        paragraphs.add(paragraph);
        iterate(p);
        int end = sbText.length();
        if (sbText.substring(start, end).trim().equals("")) paragraphs.remove(paragraph);
        paragraph.setEndOffset(end);
    }

    @Override
    public void visit(WtNewline p) {
        if (locType == WikiPageLocationType.MAIN_LIST ||
                locType == WikiPageLocationType.MAIN_TABLE ||
                locType == WikiPageLocationType.OTHER_TABLE) {
            print('\n');
        }

        int start = sbText.length();
        int pNum = this.paragraphs.size();
        //if(this.sections.size() == 0) locType = WikiPageLocationType.OVERVIEW;
        WikiParagraph paragraph = new WikiParagraph(start, 0, pNum, locType);
        paragraphs.add(paragraph);
        iterate(p);
        print('\n');
        int end = sbText.length();
        if (sbText.substring(start, end).trim().equals("")) paragraphs.remove(paragraph);
        paragraph.setEndOffset(end);
    }

    @Override
    public void visit(WtHorizontalRule p) {
        if (locType == WikiPageLocationType.MAIN_LIST ||
                locType == WikiPageLocationType.MAIN_TABLE ||
                locType == WikiPageLocationType.OTHER_TABLE) {
            print('\n');
        }
        int start = sbText.length();
        int pNum = this.paragraphs.size();
        //if(this.sections.size() == 0) locType = WikiPageLocationType.OVERVIEW;
        WikiParagraph paragraph = new WikiParagraph(start, 0, pNum, locType);
        paragraphs.add(paragraph);
        iterate(p);
        print('\n');
        int end = sbText.length();
        if (sbText.substring(start, end).trim().equals("")) paragraphs.remove(paragraph);
        paragraph.setEndOffset(end);
    }

    @Override
    public void visit(WtText text) {
        //text unit
        print(text.getContent());
    }

    @Override
    public void visit(WtBold text) {
        //text unit
        iterate(text);
    }

    @Override
    public void visit(WtItalics text) {
        //text unit
        iterate(text);
    }

    @Override
    public void visit(WtWhitespace arg0) {
        print(' ');
    }

    @Override
    public void visit(WtInternalLink link) {

        WikiTitle target = null;
        boolean isCategory = false;
        boolean isInterWiki = false;
        String defaultLanguage = titleFactory.language();
        try {
            if (link.getTarget().isResolved()) {
                PageTitle page = PageTitle.make(config, link.getTarget().getAsString());
                titleFactory.title(page.getNormalizedFullTitle())
                        .namespace(page.getNamespace().getId());
                if (page.getNamespace().getId() == 14) {
                    isCategory = true;
                } else if (page.getInterwikiLink() != null) {

                    String languagePrefix = page.getInterwikiLink().getPrefix();
                    if (!defaultLanguage.equals(languagePrefix)) {
                        titleFactory.language(languagePrefix);
                        isInterWiki = true;
                    }
                }
                target = titleFactory.produce();
                titleFactory = titleFactory.language(defaultLanguage);
            }
        } catch (LinkTargetException ignored) {
        }
        print(link.getPrefix());
        print(' ');
        int start = sbText.length();
        if (!(locType == WikiPageLocationType.MAIN_TABLE || locType == WikiPageLocationType.OTHER_TABLE ||
                isCategory)) {
            //System.out.println(link.getPrefix());
            if (!link.hasTitle()) {
                iterate(link.getTarget());
            } else {
                iterate(link.getTitle());
            }
            //System.out.println(link.getPostfix());

        }
        int end = sbText.length();
        //print(' ');
        print(link.getPostfix());
        String surface = sbText.substring(start, end);
        if ((locType == WikiPageLocationType.MAIN_TABLE || locType == WikiPageLocationType.OTHER_TABLE || isCategory)
                && target != null) {

            sbTable.append("<a href=\"/wiki/").append(target.getTitle()).append("\">");
            if (!link.hasTitle()) {
                iterate(link.getTarget());
            } else {
                iterate(link.getTitle());
            }
            sbTable.append("</a>");

            if (link.hasTitle()) {
                try {
                    WtText t = (WtText) link.getTitle().get(0);
                    surface = t.getContent().trim();
                } catch (Exception e) {
                    surface = getNodeTextContent(link.getTarget());
                }
            } else {
                surface = link.getTarget().getAsString();
            }
        }
        surface = surface.replaceAll("[\\s]+", " ");
        WikiLink linkObj = new WikiLink(start, end, locType, surface, target);
        if (isCategory) this.categoryLinks.add(linkObj);
        else if (isInterWiki) this.interWikiLinks.add(linkObj);
        else this.internalLinks.add(linkObj);
    }

    @Override
    public void visit(WtExternalLink n) {
        if (n.hasTitle()) {
            iterate(n.getTitle());
        }
    }

    @Override
    public void visit(WtDefinitionList dList) {
        WikiPageLocationType tempType = locType;
        if (locType == WikiPageLocationType.MAIN)
            locType = WikiPageLocationType.MAIN_LIST;
        iterate(dList);
        locType = tempType;
    }

    @Override
    public void visit(WtDefinitionListTerm dListTerm) {
        print('\t');
        iterate(dListTerm);
        print('\n');

    }

    @Override
    public void visit(WtDefinitionListDef dListDef) {

        print("\t\t");
        iterate(dListDef);
        print('\n');
    }

    @Override
    public void visit(WtOrderedList list) {
        WikiPageLocationType tempType = locType;
        if (locType == WikiPageLocationType.MAIN)
            locType = WikiPageLocationType.MAIN_LIST;
        print('\n');
        indentLevel++;
        iterate(list);
        indentLevel--;
        locType = tempType;
    }

    @Override
    public void visit(WtUnorderedList list) {
        WikiPageLocationType tempType = locType;
        if (locType == WikiPageLocationType.MAIN)
            locType = WikiPageLocationType.MAIN_LIST;
        print('\n');
        indentLevel++;
        iterate(list);
        indentLevel--;
        locType = tempType;
    }

    @Override
    public void visit(WtListItem listItem) {
        this.indent();
        iterate(listItem);
        print('\n');
    }

    @Override
    public void visit(WtSemiPre n) {
        iterate(n);
        print('\n');
    }

    @Override
    public void visit(WtSemiPreLine n) {
        iterate(n);
        print('\n');
    }

    @Override
    public void visit(WtTagExtension n) {
        //special case for tags
        //including <pre>
        if (n.getName().trim().equalsIgnoreCase("ref"))
            return;
        if (n.getName().trim().equalsIgnoreCase("references"))
            return;
        print(n.getBody().getContent());
    }

    @Override
    public void visit(EngNowiki n) {
        print(n.getContent());
    }

    @Override
    public void visit(WtXmlEntityRef entity) {
        print("&" + entity.getName() + ";");
    }

    @Override
    public void visit(WtXmlCharRef entity) {
        print("&#" + entity.getCodePoint() + ";");
    }

    @Override
    public void visit(WtIllegalCodePoint n) {
        final String cp = n.getCodePoint();
        for (int i = 0; i < cp.length(); ++i) {
            int code = (int) cp.charAt(i);
            print("&#" + code + ";");
        }
    }

    @Override
    public void visit(WtTable table) {
        WikiPageLocationType tempLocType = locType;
        if (locType == WikiPageLocationType.MAIN)
            locType = WikiPageLocationType.MAIN_TABLE;
        else if (locType != WikiPageLocationType.MAIN_TABLE)
            locType = WikiPageLocationType.OTHER_TABLE;
        print("<table");
        iterate(this.cleanAttribs(table.getXmlAttributes()));
        print(">");
        if (!sbTable.toString().toLowerCase().contains("wikitable")) {
            locType = tempLocType;
            sbTable = new StringBuilder();
            fixTableBody(table.getBody());
        } else {
            fixTableBody(table.getBody());
            print("</table>");
            locType = tempLocType;
            WikiHTMLElement tableHTML = new WikiHTMLElement(sbText.length(), sbText.length(), locType, sbTable.toString());
            tableHTML.setInSectionNumber(currentSectionNumber);
            tableHTMLs.add(tableHTML);
        }
        sbTable = new StringBuilder();
    }

    @Override
    public void visit(WtTableImplicitTableBody n) {
//		WikiPageLocationType tempLocType = locType;
//		if(locType == WikiPageLocationType.MAIN)
//			locType = WikiPageLocationType.MAIN_TABLE;
//		else if(locType != WikiPageLocationType.MAIN_TABLE)
//			locType = WikiPageLocationType.OTHER_TABLE;
//		print("<table>");
        iterate(n.getBody());
//		print("</table>");
//		locType = tempLocType;
//		sbTable = new StringBuilder();
    }

    @Override
    public void visit(WtTableCaption n) {
        if (!(locType == WikiPageLocationType.MAIN_TABLE ||
                locType == WikiPageLocationType.OTHER_TABLE)) {
            return;
        }
        print("\n<caption");
        iterate(this.cleanAttribs(n.getXmlAttributes()));
        print(">\n");
        dispatch(getCellContent(n.getBody()));
        print("</caption>\n");

    }

    @Override
    public void visit(WtTableRow n) {

        boolean cellsDefined = false;
        for (WtNode cell : n.getBody()) {
            switch (cell.getNodeType()) {
                case WtNode.NT_TABLE_CELL:
                case WtNode.NT_TABLE_HEADER:
                    cellsDefined = true;
                    break;
            }
        }

        if (cellsDefined) {
            if (!(locType == WikiPageLocationType.MAIN_TABLE ||
                    locType == WikiPageLocationType.OTHER_TABLE)) {
                dispatch(getCellContent(n.getBody()));
                return;
            }
            print("<tr");
            iterate(cleanAttribs(n.getXmlAttributes()));
            print(">\n");
            dispatch(getCellContent(n.getBody()));
            print("</tr>\n");
        } else {
            iterate(n.getBody());
        }
    }

    @Override
    public void visit(WtTableCell n) {

        if (!(locType == WikiPageLocationType.MAIN_TABLE ||
                locType == WikiPageLocationType.OTHER_TABLE)) {
            dispatch(getCellContent(n.getBody()));
            return;
        }
        print("<td");
        iterate(cleanAttribs(n.getXmlAttributes()));
        print(">\n");
        dispatch(getCellContent(n.getBody()));
        print("</td>\n");
    }

    @Override
    public void visit(WtTableHeader n) {

        if (!(locType == WikiPageLocationType.MAIN_TABLE ||
                locType == WikiPageLocationType.OTHER_TABLE)) {
            dispatch(getCellContent(n.getBody()));
            return;
        }
        print("<th");
        iterate(cleanAttribs(n.getXmlAttributes()));
        print(">\n");
        dispatch(getCellContent(n.getBody()));
        print("</th>\n");
    }

    @Override
    public void visit(WtRedirect link) {
        WikiTitle target = null;
        try {
            if (link.getTarget().isResolved()) {
                PageTitle page = PageTitle.make(config, link.getTarget().getAsString());
                target = titleFactory.title(page.getTitle()).namespace(page.getNamespace().getId()).produce();
                if (page.getNamespace().equals(config.getNamespace("Category"))) {
                    return;
                }
                if (page.getInterwikiLink() != null) {
                    String defaultLanguage = titleFactory.language();
                    target = titleFactory.language(page.getInterwikiLink().getPrefix()).produce();
                    titleFactory.language(defaultLanguage);
                }
            }
        } catch (LinkTargetException ignored) {
        }
        redirectTo = target;
    }

    @Override
    public void visit(WtUrl linkUrl) {
        String url;
        if (linkUrl.getProtocol().equals(""))
            url = linkUrl.getPath();
        else
            url = linkUrl.getProtocol() + ":" + linkUrl.getPath();
        print(url);
    }

    @Override
    public void visit(WtXmlAttribute n) {
        if (!n.getName().isResolved()) {
            logger.warning("Unresolved attribute name: " + n);
        } else {
            String attr;
            if (n.hasValue()) {
                attr = " " + n.getName().getAsString() + "=\"" + cleanAttribValue(n.getValue()) + "\"";
            } else {
                attr = " " + n.getName().getAsString() + "=\"" + n.getName().getAsString() + "\"";
            }
            if (locType == WikiPageLocationType.MAIN_TABLE || locType == WikiPageLocationType.OTHER_TABLE) {
                print(attr);
            }
        }
    }

    @Override
    public void visit(WtXmlAttributes n) {
        for (WtNode n1 : n) {
            switch (n1.getNodeType()) {
                case WtNode.NT_XML_ATTRIBUTE:
                case WtNode.NT_XML_ATTRIBUTE_GARBAGE:
                    dispatch(n1);
                    break;
                default:
                    logger.warning("Non-attribute node in attributes");
                    break;
            }
        }
    }

    // DON'T KNOW, BUT  THIS SEEMS TO WORK
    @Override
    public void visit(WtValue n) {
        iterate(n);
    }

    @Override
    public void visit(WtNodeList list) {
        iterate(list);
    }

    @Override
    public void visit(WtLinkTitle title) {
        iterate(title);
    }

    @Override
    public void visit(WtName name) {
        iterate(name);
    }

    @Override
    public void visit(WtOnlyInclude n) {
        iterate(n);
    }

    @Override
    public void visit(WtParsedWikitextPage n) {
        iterate(n);
    }

    @Override
    public void visit(WtPreproWikitextPage n) {
        iterate(n);
    }

    @Override
    public void visit(EngProcessedPage n) {
        dispatch(n.getPage());
    }

    @Override
    public void visit(EngSoftErrorNode n) {
        visit((WtXmlElement) n);
    }

    // IGNORED ELEMENTS
    @Override
    public void visit(WtImageLink arg0) {
    }

    @Override
    public void visit(WtLinkOptionGarbage arg0) {
    }

    @Override
    public void visit(WtIgnored arg0) {
    }

    @Override
    public void visit(WtPageSwitch arg0) {
    }

    @Override
    public void visit(WtXmlAttributeGarbage arg0) {
    }

    @Override
    public void visit(WtXmlComment arg0) {
    }

    // SHOULD NOT HAPPEN
    @Override
    public void visit(WtPageName arg0) {
        this.shouldNotHappen(arg0);
    }

    @Override
    public void visit(WtLinkOptionLinkTarget arg0) {
        this.shouldNotHappen(arg0);
    }

    @Override
    public void visit(WtLinkOptionAltText arg0) {
        this.shouldNotHappen(arg0);
    }

    @Override
    public void visit(WtLinkOptions arg0) {
        this.shouldNotHappen(arg0);
    }

    @Override
    public void visit(WtTicks arg0) {
        this.shouldNotHappen(arg0);
    }

    // NOT IMPLEMENTED ELEMENTS

    @Override
    public void visit(WtImEndTag arg0) {
        //XXX: Sweble bug
        //this.shouldNotHappen(arg0);
    }

    @Override
    public void visit(WtLinkOptionKeyword arg0) {
        this.shouldNotHappen(arg0);
    }

    @Override
    public void visit(WtLinkOptionResize arg0) {
        this.shouldNotHappen(arg0);
    }

    @Override
    public void visit(WtImStartTag arg0) {
        this.shouldNotHappen(arg0);
    }

    @Override
    public void visit(WtTagExtensionBody arg0) {
        this.shouldNotHappen(arg0);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public void visit(WtXmlElement n) {
        if (n.hasBody()) {
            if (blockElements.contains(n.getName().toLowerCase())) {
                if (locType == WikiPageLocationType.MAIN_TABLE || locType == WikiPageLocationType.OTHER_TABLE) {
                    print("<" + n.getName());
                    iterate(n.getXmlAttributes());
                    print(">");
                }
                print("\n");
                dispatch(n.getBody());
                if (locType == WikiPageLocationType.MAIN_TABLE || locType == WikiPageLocationType.OTHER_TABLE) {
                    print("</" + n.getName() + ">");
                }
                print("\n");
            } else if (blockedElements.contains(n.getName().toLowerCase())) {

            } else {
                dispatch(n.getBody());
            }
        } else {
            if (n.getName().equals("br")) {
                print('\n');
            }
        }
    }

    @Override
    public void visit(WtXmlEmptyTag arg0) {


    }

    @Override
    public void visit(WtXmlStartTag arg0) {


    }

    @Override
    public void visit(WtXmlEndTag arg0) {


    }

    @Override
    public void visit(WtTemplate arg0) {


    }

    @Override
    public void visit(WtTemplateArguments arg0) {


    }

    @Override
    public void visit(WtTemplateArgument arg0) {


    }

    @Override
    public void visit(WtTemplateParameter arg0) {


    }

    @Override
    public void visit(WtSignature arg0) {


    }

    private String getNodeTextContent(WtNode n) {
        String textContent = "";
        for (WtNode currNode : n) {
            if (currNode.getNodeType() == WtNode.NT_TEXT) {
                textContent += ((WtText) currNode).getContent() + " ";
            }
        }
        return textContent.trim();
    }

    //GETTERS AND SETTERS
    public WikiTitleFactory getTitleFactory() {
        return titleFactory;
    }

    public void setTitleFactory(WikiTitleFactory titleFactory) {
        this.titleFactory = titleFactory;
    }

    //PRINTING FUNCTIONS
    private void indent() {
        for (int i = 0; i < indentLevel; i++) {
            print('\t');
        }
    }

    private void print(String text) {
        if (locType == WikiPageLocationType.MAIN_TABLE ||
                locType == WikiPageLocationType.OTHER_TABLE) {
            sbTable.append(text);
            return;
        }

        text = TextUtils.replaceTags(
                TextUtils.replaceHTMLEntities(text, null)
                , null);
        text = TextUtils.removeInterWikiPrefix(text, interWikiPrefixSet);

        sbText.append(text);
    }

    private void print(char c) {
        if (locType == WikiPageLocationType.MAIN_TABLE ||
                locType == WikiPageLocationType.OTHER_TABLE) {
            sbTable.append(c);
            return;
        }
        sbText.append(c);
    }

    private void shouldNotHappen(WtNode node) {
        logger.warning("Unexpected behavior: Visiting " + node + "(" + this.page.getTitle() + ")");
    }

    /**
     * CODE BELOW:
     * Copied from Sweble HtmlRenderer
     */
    private void fixTableBody(WtNodeList body) {
        boolean hadRow = false;
        WtTableRow implicitRow = null;
        for (WtNode c : body) {
            switch (c.getNodeType()) {
                case WtNode.NT_TABLE_HEADER: //SAME THING
                case WtNode.NT_TABLE_CELL:
                    if (hadRow) {
                        dispatch(c);
                    } else {
                        if (implicitRow == null)
                            implicitRow = nf
                                    .tr(nf.emptyAttrs(), nf.body(nf.list()));
                        implicitRow.getBody().add(c);
                    }
                    break;


                case WtNode.NT_TABLE_CAPTION:
                    if (!hadRow && implicitRow != null)
                        dispatch(implicitRow);
                    implicitRow = null;
                    dispatch(c);
                    break;


                case WtNode.NT_TABLE_ROW:
                    if (!hadRow && implicitRow != null)
                        dispatch(implicitRow);
                    hadRow = true;
                    dispatch(c);
                    break;

                default:
                    if (!hadRow && implicitRow != null)
                        implicitRow.getBody().add(c);
                    else
                        dispatch(c);
                    break;
            }
        }
    }

    @SuppressWarnings({"StatementWithEmptyBody", "SuspiciousMethodCalls"})
    protected WtNodeList cleanAttribs(WtNodeList xmlAttributes) {
        ArrayList<WtXmlAttribute> clean = null;

        WtXmlAttribute style = null;
        for (WtNode a : xmlAttributes) {
            if (a instanceof WtXmlAttribute) {
                WtXmlAttribute attr = (WtXmlAttribute) a;
                if (!attr.getName().isResolved())
                    continue;

                String name = attr.getName().getAsString().toLowerCase();
//                if (locType != WikiPageLocationType.MAIN_TABLE && locType != WikiPageLocationType.OTHER_TABLE)
                if (name.equals("style")) {
                    style = attr;
                } else if (name.equals("width")) {
                    if (clean == null)
                        clean = new ArrayList<WtXmlAttribute>();
                    clean.add(attr);
                } else if (name.equals("align")) {
                    if (clean == null)
                        clean = new ArrayList<WtXmlAttribute>();
                    clean.add(attr);
                }
            }
        }

        if (clean == null || clean.isEmpty())
            return xmlAttributes;

        String newStyle = "";
        if (style != null)
            newStyle = cleanAttribValue(style.getValue());

        for (WtXmlAttribute a : clean) {
            if (!a.getName().isResolved())
                continue;

            String name = a.getName().getAsString().toLowerCase();
            if (name.equals("align")) {
                newStyle = String.format("text-align: %s; ",
                        cleanAttribValue(a.getValue()))
                        + newStyle;
            } else {
                newStyle = String.format("%s: %s; ", name,
                        cleanAttribValue(a.getValue()))
                        + newStyle;
            }
        }

        WtXmlAttribute newStyleAttrib = nf.attr(
                nf.name(nf.list(nf.text("style"))),
                nf.value(nf.list(nf.text(newStyle))));

        WtNodeList newAttribs = nf.attrs(nf.list());
        for (WtNode a : xmlAttributes) {
            if (a == style) {
                newAttribs.add(newStyleAttrib);
            } else if (clean.contains(a)) {
                // Remove
            } else {
                // Copy the rest
                newAttribs.add(a);
            }
        }

        if (style == null)
            newAttribs.add(newStyleAttrib);

        return newAttribs;
    }

    protected String cleanAttribValue(WtNodeList value) {
        try {
            return StringUtils.collapseWhitespace(tu.astToText(value)).trim();
        } catch (StringConversionException e) {
            //logger.warning("Error for " + value.toString() + " " +e.getMessage());
            return "";
        }
    }
}
