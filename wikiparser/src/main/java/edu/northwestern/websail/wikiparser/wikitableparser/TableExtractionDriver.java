package edu.northwestern.websail.wikiparser.wikitableparser;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import edu.northwestern.websail.wikiparser.exporter.MongoDBPageExporter;
import edu.northwestern.websail.wikiparser.model.WikiExtractedPage;
import org.jongo.Jongo;

import java.io.*;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author csbhagav on 1/6/14.
 */
public class TableExtractionDriver {

    public static void main(String[] args) throws IOException, InterruptedException {

        String language = args[0];
        String pgTitleIdMapFile = args[1];
        Integer numThreads = Integer.valueOf(args[2]);

        MongoClient mongoClient;
        DB db;
        Properties mongoConfig = new Properties();
        mongoConfig.load(new FileInputStream("./config/mongo.properties"));

        try {
            mongoClient = new MongoClient(mongoConfig.getProperty("hostName"));
            db = mongoClient.getDB(mongoConfig.getProperty("dbName"));
            if (!db.authenticate(mongoConfig.getProperty("username"), mongoConfig.getProperty("password")
                    .toCharArray())) {
                System.out.println("username or password is not correct.");
                mongoClient.close();
                return;
            }
        } catch (UnknownHostException e1) {
            System.out.println("Host DB error");
            e1.printStackTrace();
            return;
        }
        Jongo tablesDb = new Jongo(db);

        System.out.println("Connecting to mongo success. - " + mongoClient);

        ArrayBlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(numThreads * 2);
        ThreadPoolExecutor exec = new ThreadPoolExecutor(numThreads, numThreads, 5L,
                TimeUnit.MINUTES, queue, new ThreadPoolExecutor.CallerRunsPolicy());

        HashMap<String, Integer> pgTitleIdMap = readPgTitleIdMap(pgTitleIdMapFile);

        System.out.println("Iterating through page collection");
        Iterable<WikiExtractedPage> it = tablesDb.getCollection("page").find("{\"title.id\":{$nin : [22329043,2326869]}}").as(WikiExtractedPage
                .class);


        int numPages = 0;
        MongoDBPageExporter exporter = new MongoDBPageExporter("error_pages/" + language + "Tables", db);

        for (WikiExtractedPage page : it) {
            try {
                numPages++;
                if (numPages % 1000 == 0) {
                    System.out.print(numPages + "...");
                    if (numPages % 10000 == 0) {
                        System.out.println("");
                    }
                }
                if (page.getTableHTMLs().size() == 0) {
                    continue;
                }

                TableExtractionWorker tableWorker = new TableExtractionWorker(pgTitleIdMap, false);
                tableWorker.setExporter(exporter);
                tableWorker.setPage(page);
                exec.submit(tableWorker);

            } catch (Exception e) {
                System.out.println("Error converting to WikiExtractedPage obj:" + numPages);

            }
        }
        exec.shutdown();

        while (!exec.isTerminated())
            exec.awaitTermination(5L, TimeUnit.SECONDS);
        mongoClient.close();
    }


    private static HashMap<String, Integer> readPgTitleIdMap(String pgTitleIdMapFile) throws IOException {

        System.out.print("Reading ... " + pgTitleIdMapFile + " ...");
        HashMap<String, Integer> pgTitleIdMap = new HashMap<String, Integer>();

        BufferedReader in = new BufferedReader(new FileReader(new File(pgTitleIdMapFile)));
        String line;
        while ((line = in.readLine()) != null) {
            String[] parts = line.split("\t");
            pgTitleIdMap.put(parts[0], Integer.valueOf(parts[1]));
        }
        System.out.print("DONE");
        return pgTitleIdMap;
    }

}
