package edu.northwestern.websail.wikiparser.model.element;

/**
 * @author csbhagav
 */
public enum LinkType {
    INTERNAL, MMUL, WEBSAIL, INTERNAL_RED, EXTERNAL, CATEGORY_LINK, UNSET
}
