package edu.northwestern.websail.wikiparser.wikitextparser.adapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Logger;


public class ParsoidAdapter {
    public static Logger logger = Logger.getLogger(ParsoidAdapter.class.getName());
    private String rootAPI = "http://stark.cs.northwestern.edu:8101";

    public ParsoidAdapter() {
    }

    public static void main(String[] args) {
        ParsoidAdapter adapter = new ParsoidAdapter();
        String html = adapter.html("enwiki", "Dungannon_Swifts_F.C.");
        System.out.println(html);

        String wikiText = adapter.wikiText("enwiki", "Dungannon_Swifts_F.C.", html);
        System.out.println(wikiText);
    }

    public String html(String prefix, String pageName) {
        URL url = null;
        try {
            url = this.formURL(prefix, pageName);
        } catch (MalformedURLException e) {
            logger.severe(rootAPI + "/" + prefix + "/" + pageName + " specifies an unknown protocol.");
            e.printStackTrace();
        }
        if (url == null) return null;
        HttpURLConnection con;
        try {
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "UTF-8"));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(new String((inputLine + "\n").getBytes(), "UTF-8"));
            }
            in.close();
            return response.toString();
        } catch (IOException e) {
            logger.severe("Cannot get data from " + rootAPI + "/" + prefix + "/" + pageName);
            e.printStackTrace();
        }

        return null;
    }

    public String wikiText(String prefix, String pageName, String html) {
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
        System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http", "warn");
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httppost = new HttpPost(this.formRequestURL(prefix, pageName));
        ArrayList<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>(2);
        params.add(new BasicNameValuePair("html", html));
        try {
            httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            logger.severe("This should not happen at all, how could UTF-8 is not supported.");
            e.printStackTrace();
        }
        HttpResponse response = null;

        try {
            response = httpclient.execute(httppost);
        } catch (ClientProtocolException e1) {

            e1.printStackTrace();
        } catch (IOException e1) {

            e1.printStackTrace();
        }
        if (response == null) return null;
        HttpEntity entity = response.getEntity();

        if (entity != null) {
            try {

                InputStream instream = entity.getContent();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(instream));
                String inputLine;
                StringBuilder content = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine).append("\n");
                }
                in.close();
                instream.close();
                return content.toString();
            } catch (IOException e) {
                logger.severe("Cannot get data from " + rootAPI + "/" + prefix + "/" + pageName);
                e.printStackTrace();
            }
        }
        try {
            httpclient.close();
        } catch (IOException e) {
            //that's okay
            e.printStackTrace();
        }
        return null;

    }

    private URL formURL(String prefix, String pageName) throws MalformedURLException {
        String url = this.formRequestURL(prefix, pageName);
        return new URL(url);
    }

    private String formRequestURL(String prefix, String pageName) {
        return rootAPI + "/" + prefix + "/" + pageName;
    }
}
