package edu.northwestern.websail.wikiparser.model.table;

import edu.northwestern.websail.wikiparser.model.element.WikiLink;
import edu.northwestern.websail.wikiparser.utils.CellTextTokenizer;
import edu.northwestern.websail.wikiparser.utils.TrieTextNormalizer;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;

/*
 * @author csbhagav
 */

public class WikiCell {

    Integer cellID;
    HashSet<String> textTokens;
    String text = "";
    Integer wikiReferenceID;
    ArrayList<String> path;
    String tdHtmlString = "";
    ArrayList<WikiLink> surfaceLinks = new ArrayList<WikiLink>();
    //    HashMap<String, WikiLink> predictedSurfaceLinks = new HashMap<String, WikiLink>();
    Integer subtableID = -1;
    Boolean isNumeric = false;

    public WikiCell() {
    }

    public WikiCell(String text) {
        this(-1, text);
    }

    public WikiCell(Integer cellID, String text) {
        super();
        this.cellID = cellID;
        this.text = TrieTextNormalizer.getCleanStr(text);
        this.textTokens = CellTextTokenizer.normalizedText(this.text);
    }

    public Integer getCellID() {
        return cellID;
    }

    public void setCellID(Integer cellID) {
        this.cellID = cellID;
    }

    public Integer getWikiReferenceID() {
        return wikiReferenceID;
    }

    public void setWikiReferenceID(Integer wikiReferenceID) {
        this.wikiReferenceID = wikiReferenceID;
    }

    public HashSet<String> getTextTokens() {
        return textTokens;
    }

    public void setTextTokens(HashSet<String> textTokens) {
        this.textTokens = textTokens;
    }

    public ArrayList<String> getPath() {
        return path;
    }

    public void setPath(ArrayList<String> path) {
        this.path = path;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTdHtmlString() {
        return tdHtmlString;
    }

    public void setTdHtmlString(String tdHtmlString) throws UnsupportedEncodingException {
        this.tdHtmlString = tdHtmlString;
    }

    public ArrayList<WikiLink> getSurfaceLinks() {
        return surfaceLinks;
    }

    public void setSurfaceLinks(ArrayList<WikiLink> surfaceLinks) {
        this.surfaceLinks = surfaceLinks;
    }

//    public HashMap<String, WikiLink> getPredictedSurfaceLinks() {
//        return predictedSurfaceLinks;
//    }
//
//    public void setPredictedSurfaceLinks(HashMap<String, WikiLink> predictedSurfaceLinks) {
//        this.predictedSurfaceLinks = predictedSurfaceLinks;
//    }
//
//    public void addPredictedSurfacLink(String surface, WikiLink wl) {
//        this.predictedSurfaceLinks.put(surface, wl);
//    }

    public Integer getSubtableID() {
        return subtableID;
    }

    public void setSubtableID(Integer subtableID) {
        this.subtableID = subtableID;
    }

    public Boolean getIsNumeric() {
        return isNumeric;
    }

    public void setIsNumeric(Boolean isNumeric) {
        this.isNumeric = isNumeric;
    }

    public String toString() {
        String retStr = "";
        retStr += cellID + "- Candidates=" + surfaceLinks;
        return retStr;
    }
}
