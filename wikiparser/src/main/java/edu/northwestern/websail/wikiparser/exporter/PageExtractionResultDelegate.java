package edu.northwestern.websail.wikiparser.exporter;

import edu.northwestern.websail.wikiparser.model.WikiExtractedPage;

/**
 * @author NorThanapon
 */
public interface PageExtractionResultDelegate {
    public boolean doneExtractingPage(WikiExtractedPage page);

    public boolean errorExtractingPage(int titleId, String titleName, String wikiText);
}
