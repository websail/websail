package edu.northwestern.websail.wikiparser.exporter;


import edu.northwestern.websail.wikiparser.model.WikiExtractedPage;

/**
 * @author NorThanapon
 */
public class PageExtractionDelegateSequence implements PageExtractionResultDelegate {

    private Iterable<PageExtractionResultDelegate> sequence;

    public PageExtractionDelegateSequence(Iterable<PageExtractionResultDelegate> sequence) {
        this.sequence = sequence;
    }

    @Override
    public boolean doneExtractingPage(WikiExtractedPage page) {
        boolean success = true;
        for (PageExtractionResultDelegate delegate : sequence) {
            success = success && delegate.doneExtractingPage(page);
        }
        return success;
    }

    @Override
    public boolean errorExtractingPage(int titleId, String titleName,
                                       String wikiText) {
        boolean success = true;
        for (PageExtractionResultDelegate delegate : sequence) {
            success = success && delegate.errorExtractingPage(titleId, titleName, wikiText);
        }
        return success;
    }

}
