package edu.northwestern.websail.wikiparser.model.element;

/**
 * @author NorThanapon
 */

public abstract class WikiElement {
    protected int offset;
    protected int endOffset;
    protected boolean isInTemplate;
    protected WikiPageLocationType locType;

    public WikiElement() {
    }

    public WikiElement(int start, int end, WikiPageLocationType locType, boolean isInTemplate) {
        this.offset = start;
        this.endOffset = end;
        this.isInTemplate = isInTemplate;
        this.locType = locType;
    }

    public WikiElement(int start, int end, WikiPageLocationType locType) {
        this.offset = start;
        this.endOffset = end;
        this.locType = locType;
        this.isInTemplate = false;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getEndOffset() {
        return endOffset;
    }

    public void setEndOffset(int endOffset) {
        this.endOffset = endOffset;
    }

    public WikiPageLocationType getLocType() {
        return locType;
    }

    public void setLocType(WikiPageLocationType locType) {
        this.locType = locType;
    }

    public boolean isInTemplate() {
        return isInTemplate;
    }

    public void setInTemplate(boolean isInTemplate) {
        this.isInTemplate = isInTemplate;
    }
}
