package edu.northwestern.websail.wikiparser.wikitableparser;

import edu.northwestern.websail.wikiparser.exporter.MongoDBPageExporter;
import edu.northwestern.websail.wikiparser.model.WikiExtractedPage;
import edu.northwestern.websail.wikiparser.model.WikiTitle;
import edu.northwestern.websail.wikiparser.model.element.LinkType;
import edu.northwestern.websail.wikiparser.model.element.WikiHTMLElement;
import edu.northwestern.websail.wikiparser.model.element.WikiLink;
import edu.northwestern.websail.wikiparser.model.element.WikiPageLocationType;
import edu.northwestern.websail.wikiparser.model.table.WikiCell;
import edu.northwestern.websail.wikiparser.model.table.WikiTable;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * @author csbhagav on 1/6/14.
 */
public class TableExtractionWorker implements Runnable {

    WikiExtractedPage page;
    Boolean debug;
    MongoDBPageExporter extractionDelegate;
    Whitelist wl;
    private HashMap<String, Integer> pgTitleIdMap;
    private int tableNumber = 1;

    public TableExtractionWorker(HashMap<String, Integer> pgTitleIdMap, Boolean debug) {
        this.pgTitleIdMap = pgTitleIdMap;
        this.debug = debug;
        this.wl = Whitelist.relaxed();
        wl.addAttributes("span", "style");
    }

    public void parsePageTables() throws IOException {
        ArrayList<WikiHTMLElement> tables = page.getTableHTMLs();

        String localPath = getClass().getResource("/xhtml1-strict.dtd").toString();
        String docTypeDeclaration = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<!DOCTYPE xml SYSTEM \"" +
                localPath +
                "\">";
        tableNumber = 1;
        String currSectionTitle;
        for (WikiHTMLElement table : tables) {
            if (table.getInSectionNumber() >= 0)
                currSectionTitle = page.getSections().get(table.getInSectionNumber()).getSectionTitle();
            else currSectionTitle = "";
            String tableHTML = table.getHtml();
            tableHTML = Jsoup.clean(tableHTML, "http://www.wikipedia.org/", wl);
            org.jsoup.nodes.Document doc = Jsoup.parse(tableHTML, "http://www.wikipedia.org/");
            doc.select("*[style*=display:none]").remove();
            tableHTML = doc.html();
            parseAndStoreTable(docTypeDeclaration + cleanCellText(tableHTML), currSectionTitle);
        }
    }

    private WikiTable parseAndStoreTable(String tableHTML, String currSectionTitle) {

        WikiTable wt = null;

        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory
                    .newInstance();
            docFactory.setIgnoringElementContentWhitespace(true);
            docFactory.setIgnoringComments(true);
            DocumentBuilder builder = docFactory.newDocumentBuilder();
            Document doc = builder.parse(new ByteArrayInputStream(tableHTML
                    .getBytes("UTF-8")));

            String tableCaption = setTableCaption(doc, currSectionTitle);
            NodeList headerRowNodeList = getHeaderRows(doc);

            int numHeaderRows = headerRowNodeList.getLength();

            int numColumns = getNumColumns(headerRowNodeList, doc);

            NodeList dataRowNodeList = getDataRows(doc, numHeaderRows);

            int numDataRows = dataRowNodeList.getLength();

            if (numHeaderRows == 0)
                numHeaderRows = 1;
            WikiCell[][] dataMatrix = new WikiCell[numDataRows][numColumns];

            WikiCell[][] headMatrix = new WikiCell[numHeaderRows][numColumns];

            if (debug)
                System.out.println("TableID = " + tableNumber + "\n" + "Caption = "
                        + tableCaption + "\nNumRows = " + numDataRows
                        + "\nNumColumns = " + numColumns);

            convertRowDataTo2DArray(headMatrix, headerRowNodeList,
                    numHeaderRows, numColumns, currSectionTitle);
            sanitize(headMatrix);
            if (debug)
                show(headMatrix);

            convertRowDataTo2DArray(dataMatrix, dataRowNodeList, numDataRows,
                    numColumns, currSectionTitle);
            sanitize(dataMatrix);

            if (debug)
                show(dataMatrix);

            wt = new WikiTable(page.getTitle().getId(), tableNumber, numHeaderRows, numDataRows, numColumns);
            wt.setTableData(dataMatrix);
            wt.setTableHeaders(headMatrix);
            if (tableCaption.equalsIgnoreCase("") && page.getTableHTMLs().size() > 0 && tableNumber <= page
                    .getTableHTMLs().size()
                    ) {
                int idx = page.getTableHTMLs().get(tableNumber - 1).getInSectionNumber();
                if (idx < 0)
                    idx = 0;
                if (page.getSections().size() > 0)
                    wt.setTableCaption(page.getSections().get(idx)
                            .getSectionTitle());
            } else wt.setTableCaption(tableCaption);
            wt.setPgTitle(page.getTitle().getTitle());
            wt.setSectionTitle(currSectionTitle);
            cleanDuplicateColumns(wt);
            wt.setNumericColumns(getNumericColumnsSet(wt));
            extractionDelegate.doneExtractingTable(wt);
            tableNumber++;

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            extractionDelegate.errorExtractingPage(page.getTitle().getId(), page.getTitle().getTitle(), tableHTML);
        } catch (UnsupportedEncodingException e) {
            extractionDelegate.errorExtractingPage(page.getTitle().getId(), page.getTitle().getTitle(), tableHTML);
        } catch (SAXParseException e) {
            e.printStackTrace();
            System.out.println(tableHTML);
            extractionDelegate.errorExtractingPage(page.getTitle().getId(), page.getTitle().getTitle(), tableHTML);
        } catch (SAXException e) {
            extractionDelegate.errorExtractingPage(page.getTitle().getId(), page.getTitle().getTitle(), tableHTML);
        } catch (IOException e) {
            e.printStackTrace();
            extractionDelegate.errorExtractingPage(page.getTitle().getId(), page.getTitle().getTitle(), tableHTML);
        } catch (XPathExpressionException e) {
            extractionDelegate.errorExtractingPage(page.getTitle().getId(), page.getTitle().getTitle(), tableHTML);
        }


        return wt;
    }

    private void cleanDuplicateColumns(WikiTable wt) {

        WikiCell[][] tableData = wt.getTableData();
        WikiCell[][] tableHeader = wt.getTableHeaders();
        int numRows = wt.getNumDataRows();
        int numCols = wt.getNumCols();
        int numHeadRows = wt.getNumHeaderRows();

        HashSet<Integer> duplicateColumns = new HashSet<Integer>();
        for (int j = 0; j < numCols - 1; j++) {
            int checkCol = j + 1;
            boolean checkColDuplicate = true;
            for (int i = 0; i < numRows; i++) {
                if (!tableData[i][j].getText().equalsIgnoreCase(tableData[i][checkCol].getText())) {
                    checkColDuplicate = false;
                    break;
                }
            }
            if (checkColDuplicate) {
                duplicateColumns.add(checkCol);
            }
        }

        if (duplicateColumns.size() > 0) {
            WikiCell[][] newTableData = new WikiCell[numRows][numCols - duplicateColumns.size()];
            WikiCell[][] newTableHeader = new WikiCell[numHeadRows][numCols - duplicateColumns.size()];
            int currJ = -1;
            for (int j = 0; j < numCols; j++) {

                if (duplicateColumns.contains(j)) {
                    continue;
                } else currJ++;


                for (int i = 0; i < numRows; i++) {
                    newTableData[i][currJ] = tableData[i][j];
                }

                for (int i = 0; i < numHeadRows; i++) {
                    newTableHeader[i][currJ] = tableHeader[i][j];
                }

            }
            wt.setTableData(newTableData);
            wt.setTableHeaders(newTableHeader);
            wt.setNumDataRows(newTableData.length);
            wt.setNumHeaderRows(newTableHeader.length);
            wt.setNumCols(numCols - duplicateColumns.size());
        }


    }

    private HashSet<Integer> getNumericColumnsSet(WikiTable wt) {

        HashSet<Integer> numericCols = new HashSet<Integer>();
        WikiCell[][] tableData = wt.getTableData();
        int numRows = wt.getNumDataRows();
        for (int j = 0; j < wt.getNumCols(); j++) {
            int numNumeric = 0;
            for (int i = 0; i < wt.getNumDataRows(); i++) {
                if (tableData[i][j].getIsNumeric()) {
                    numNumeric++;
                }
            }
            double numPerc = (double) numNumeric / (double) numRows;
            if (numPerc > 0.5) {
                numericCols.add(j);
            }
        }
        return numericCols;

    }

    private HashMap<String, LinkedList<Integer>> getHashMap(int numRows,
                                                            int numCols) {

        HashMap<String, LinkedList<Integer>> map = new HashMap<String, LinkedList<Integer>>();

        for (int i = 0; i < numRows; i++) {
            String key = i + "";
            if (!map.containsKey(key)) {
                map.put(key, new LinkedList<Integer>());
            }

            for (int j = 0; j < numCols; j++)
                map.get(key).addLast(j);
        }
        return map;
    }

    @SuppressWarnings("MismatchedReadAndWriteOfArray")
    private void convertRowDataTo2DArray(WikiCell[][] matrix,
                                         NodeList rowNodes, int numRows, int numCols, String currSectionTitle)
            throws XPathExpressionException, ParserConfigurationException,
            SAXException, IOException {

        HashMap<String, LinkedList<Integer>> firstUnsetCellMap = getHashMap(
                numRows, numCols);

        Integer[] colIdx = new Integer[rowNodes.getLength()];
        for (int i = 0; i < rowNodes.getLength(); i++)
            colIdx[i] = 0;
        String key;
        for (int i = 0; i < rowNodes.getLength(); i++) {

            NodeList cellNodesList = rowNodes.item(i).getChildNodes();

            for (int j = 0; j < cellNodesList.getLength(); j++) {
                if (cellNodesList.item(j).getNodeType() == Node.TEXT_NODE)
                    continue;
                WikiCell cellData = parseItemContent(cellNodesList.item(j), currSectionTitle);
                cellData.setSurfaceLinks(extractSurfaceLinkMap(nodeToString(cellNodesList.item(j)), pgTitleIdMap));
                cellData.setIsNumeric(isTextNumeric(cellData.getText()));
                cellData.setTdHtmlString(nodeToString(cellNodesList.item(j)));

                NamedNodeMap attribMap = cellNodesList.item(j).getAttributes();
                int colSpan = getSpanValue(attribMap, "colspan", numCols);
                int rowSpan = getSpanValue(attribMap, "rowspan", numRows);

                key = i + "";
                if (firstUnsetCellMap.get(key).size() > 0)
                    assignValues(matrix, cellData, i, firstUnsetCellMap
                                    .get(key).getFirst(), rowSpan, colSpan,
                            firstUnsetCellMap);
            }
        }
    }

    private Boolean isTextNumeric(String text) {

        boolean isNumber;
        try {
            @SuppressWarnings("UnusedDeclaration") double d = Double.parseDouble(text);
            isNumber = true;
        } catch (NumberFormatException e) {
            if (text.contains(",")) {
                text = text.replaceAll(",", "");
                isNumber = isTextNumeric(text);
            } else isNumber = false;
        }
        return isNumber;
    }

    private void assignValues(WikiCell[][] matrix, WikiCell cellData, int x,
                              int y, int rowSpan, int colSpan,
                              HashMap<String, LinkedList<Integer>> firstUnsetCellMap) {

        for (int i = 0; i < rowSpan; i++) {
            int changingRowId = i + x;
            String key = changingRowId + "";
            for (int j = 0; j < colSpan; j++) {

                if (x + i >= matrix.length || y + j >= matrix[0].length)
                    continue;

                matrix[x + i][y + j] = cellData;
                Integer removeId = y + j;
                firstUnsetCellMap.get(key).remove(removeId);
            }
        }
    }

    private NodeList getDataRows(Document doc, int numHeaderRows)
            throws XPathExpressionException {
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        XPathExpression xpathExpr = xpath
                .compile("//table/tbody/tr[position() > " + numHeaderRows
                        + "]");
        Object rowList = xpathExpr.evaluate(doc, XPathConstants.NODESET);
        return (NodeList) rowList;
    }

    private String setTableCaption(Document doc, String currSectionTitle)
            throws XPathExpressionException, ParserConfigurationException,
            SAXException, IOException {
        String tableCaption;
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();

        XPathExpression xpathExpr = xpath.compile("//table/caption");
        Object captionObj = xpathExpr.evaluate(doc, XPathConstants.NODE);
        if (captionObj == null) {
            tableCaption = "";
        } else {
            Node captionNode = (Node) captionObj;

            WikiCell captionItem = parseItemContent(captionNode, currSectionTitle);

            tableCaption = captionItem.getText();
        }
        return tableCaption;
    }

    private String cleanCellText(String cellText) {
        cellText = cellText.replaceAll("\\[{2}File:.*?\\]{2}|\\[{2}Image:.*?\\]{2}", "");
        return cellText;
    }


    @SuppressWarnings("UnusedAssignment")
    private WikiCell parseItemContent(Node node, String currSectionTitle)
            throws XPathExpressionException, ParserConfigurationException,
            SAXException, IOException {

        WikiCell cell = new WikiCell("");

        if (node == null)
            return cell;
        else if (node.getNodeType() == Node.TEXT_NODE) {
            cell.setText(node.getNodeValue().trim());
            return cell;
        } else if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            if (element.getAttribute("style").toLowerCase()
                    .contains("display:none")) {
                return new WikiCell("");
            } else {

                String text = "";
                String display = "";

                NodeList children = element.getChildNodes();
                for (int i = 0; i < children.getLength(); i++) {
                    WikiCell subCell = parseItemContent(children.item(i), currSectionTitle);
                    if (subCell == null)
                        continue;
                    text += " \n " + subCell.getText().trim();
                    display += " \n " + subCell.getTdHtmlString();
                    if (subCell.getSubtableID() != -1)
                        cell.setSubtableID(subCell.getSubtableID());
                }
                text = text.trim();
                display = display.trim();
                String tagName = element.getTagName();
                if (tagName.equalsIgnoreCase("table")) {
                    String tableHTML = "<div>" + nodeToString(node) + "</div>";
                    WikiTable wt = parseAndStoreTable(tableHTML, currSectionTitle);
                    cell.setSubtableID(wt.getTableId());
                    return cell;
                } else if (tagName.equalsIgnoreCase("a")) {
                    String href = element.getAttribute("href");
                    cell.setText(text);
                    String prefix = "<a href=\"" + href + "\">";
                    display = prefix + display + "</a>";
                    return cell;
                } else {
                    cell.setText(text);
                    return cell;
                }
            }
        } else return cell;
    }

    private String nodeToString(Node node) {
        StringWriter sw = new StringWriter();
        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            t.transform(new DOMSource(node), new StreamResult(sw));
        } catch (TransformerException te) {
            System.out.println("nodeToString Transformer Exception");
        }
        return sw.toString();
    }

    private NodeList getHeaderRows(Document doc)
            throws XPathExpressionException {

        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();

        XPathExpression xpathExpr = xpath
                .compile("/html/body/table/tbody[count(tr) > 1]/tr[count(th) =  count(*)  or count(*) = 0]");
        Object headerRows = xpathExpr.evaluate(doc, XPathConstants.NODESET);
        NodeList headerRowNodes = (NodeList) headerRows;

        if (headerRowNodes == null || headerRowNodes.getLength() == 0) {
            xpathExpr = xpath.compile("/html/body/table/tbody/tr[position() = 1]");
            headerRows = xpathExpr.evaluate(doc, XPathConstants.NODESET);
            headerRowNodes = (NodeList) headerRows;
        } else {
            int numHeaderRows = getHeaderLimit(headerRowNodes);
            xpathExpr = xpath
                    .compile("/html/body/table/tbody/tr[count(th) =  count(*)][position() <= "
                            + numHeaderRows + "]");
            headerRows = xpathExpr.evaluate(doc, XPathConstants.NODESET);
            headerRowNodes = (NodeList) headerRows;
        }

        return headerRowNodes;

    }

    private Integer getHeaderLimit(NodeList headerRows) {
        int count = 1;
        for (int i = 0; i < headerRows.getLength() - 1; i++) {

            Node curr = headerRows.item(i);
            Node next = headerRows.item(i + 1);

            if (curr.getNextSibling() != next) {

                break;
            }

            count++;
        }

        return count;
    }

    private int getNumColumns(NodeList headerRowNodes, Document doc)
            throws XPathExpressionException {

        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        NodeList nl;
        if (headerRowNodes == null || headerRowNodes.getLength() == 0) {
            XPathExpression xpathExpr = xpath
                    .compile("//table/tbody/tr[position() = 1]");
            Object headerRows = xpathExpr.evaluate(doc, XPathConstants.NODESET);
            nl = (NodeList) headerRows;
        } else {
            nl = headerRowNodes;
        }
        Node rowNode = nl.item(0);
        if (rowNode == null)
            return 0;
        NodeList rowCellsList = rowNode.getChildNodes();
        int numColumns = 0;
        for (int i = 0; i < rowCellsList.getLength(); i++) {
            Node cellNode = rowCellsList.item(i);
            String nodeName = cellNode.getNodeName();
            if (nodeName.equalsIgnoreCase("th")
                    || nodeName.equalsIgnoreCase("td")) {
                NamedNodeMap attribMap = cellNode.getAttributes();

                int colSpan = getSpanValue(attribMap, "colspan", 1);
                numColumns += colSpan;
            }
        }

        return numColumns;

    }

    private int getSpanValue(NamedNodeMap attribMap, String key, int total) {
        int val = 1;
        String str = getAttributeValue(attribMap, key);

        str = str.replaceAll("\\[|\\]|\\(|\\)| |;|,|!|\"|`|���|]|@|���", "");

        if (str.equalsIgnoreCase(""))
            return val;

        boolean percent = false;

        if (str.endsWith("%")) {
            percent = true;
            str = str.substring(0, str.lastIndexOf("%"));
        }
        val = getIntValue(str);
        if (percent)
            val = (val * total) / 100;

        return val;

    }

    private int getIntValue(String str) {
        int val;
        try {
            val = (int) Math.floor(Double.parseDouble(str));
        } catch (Exception e) {
            String str2 = str.replaceAll("[^\\d.]", "");
            val = (int) Math.floor(Double.parseDouble(str2));
            System.out.println("Error: TableID = "
                    + " Invalid integer parsing : " + str + " - set to " + val);
        }
        return val;
    }

    private String getAttributeValue(NamedNodeMap attribMap, String key) {

        if (attribMap == null || attribMap.getLength() == 0
                || attribMap.getNamedItem(key) == null)
            return "";

        return attribMap.getNamedItem(key).toString().split("=")[1].replaceAll(
                "\"|\'", "");

    }


    public ArrayList<WikiLink> extractSurfaceLinkMap(String htmlTableElement, HashMap<String,
            Integer> pgTitleId) throws UnsupportedEncodingException {
        org.jsoup.nodes.Document tableElementTree = Jsoup.parseBodyFragment(htmlTableElement);
        Elements links = tableElementTree.select("a");
        ArrayList<WikiLink> linksList = new ArrayList<WikiLink>();
        //looping through the whole
        for (Object link1 : links) {
            org.jsoup.nodes.Element link = (org.jsoup.nodes.Element) link1;
            String linkHref = link.attr("href");
            String[] parts = linkHref.split("/");
            if (parts.length != 5)
                continue;
            String pageTitle = parts[4];
            String pageTitleOrig = URLDecoder.decode(pageTitle, "UTF-8");
            String linkText = link.text();
            if (linkText.length() > 0 && pgTitleId.containsKey(pageTitle))
                linksList.add(new WikiLink(0, 1, WikiPageLocationType.MAIN_TABLE, linkText, new WikiTitle("en", pgTitleId.get(pageTitleOrig), pageTitleOrig), LinkType.INTERNAL));
            else
                linksList.add(new WikiLink(0, 1, WikiPageLocationType.MAIN_TABLE, linkText, new WikiTitle("en", -1, pageTitleOrig), LinkType.INTERNAL_RED));
        }
        return linksList;
    }

    private void sanitize(WikiCell[][] arr) {

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] == null)
                    arr[i][j] = new WikiCell("");
                else {

                    arr[i][j].setText(arr[i][j].getText()
                            .replaceAll("\u00A0", "").replaceAll("\\s+", " ")
                            .trim());
                    arr[i][j].setText(arr[i][j].getText()
                            .replaceAll("\u00A0", "").replaceAll("\\s+", " ")
                            .trim());
                }

            }
        }
    }

    private void show(WikiCell[][] arr) {

        System.out.println("**************");
        for (WikiCell[] anArr : arr) {
            for (WikiCell anAnArr : anArr) {
                String t = anAnArr.getText();
                System.out.print(t + "\t");
            }
            System.out.println("");
        }
        System.out.println("**************");
    }

    public WikiExtractedPage getPage() {
        return page;
    }

    public void setPage(WikiExtractedPage page) {
        this.page = page;
    }

    public void setExporter(MongoDBPageExporter exporter) {
        this.extractionDelegate = exporter;
    }

    @Override
    public void run() {
        try {
            parsePageTables();
        } catch (IOException e) {
            extractionDelegate.errorExtractingPage(page.getTitle().getId(), page.getTitle().getTitle(),
                    "");
        }
    }
}
