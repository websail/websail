package edu.northwestern.websail.wikiparser.exporter;


import edu.northwestern.websail.wikiparser.model.WikiExtractedPage;
import edu.northwestern.websail.wikiparser.wikitextparser.utils.ExtractedPageUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 */
public class SimpleJsonPageExporter extends ErrorPageFileExporter {
    public static Logger logger = Logger.getLogger(SimpleJsonPageExporter.class.getName());
    private String objectPath;

    public SimpleJsonPageExporter(String objectPath, String errorPath) {
        super(errorPath);
        this.objectPath = objectPath;
    }

    @Override
    public boolean doneExtractingPage(WikiExtractedPage page) {
        try {
            ExtractedPageUtils.serialize(page, objectPath);
        } catch (FileNotFoundException e) {
            logger.severe("The directory is not found.");
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            logger.severe("Unexpected error in I/O.");
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
