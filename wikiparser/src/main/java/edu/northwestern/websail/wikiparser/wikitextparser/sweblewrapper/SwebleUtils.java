package edu.northwestern.websail.wikiparser.wikitextparser.sweblewrapper;

import org.sweble.wikitext.engine.config.Interwiki;
import org.sweble.wikitext.engine.config.WikiConfig;

import java.util.HashSet;

public class SwebleUtils {
    public static HashSet<String> extractInterWikiPrefixSet(WikiConfig config) {
        HashSet<String> prefixSet = new HashSet<String>();
        for (Interwiki interWiki : config.getInterwikis()) {
            prefixSet.add(interWiki.getPrefix());
        }
        return prefixSet;
    }

    public static String cleanWikiText(String wikiText) {
        return wikiText.replaceAll("(?i)[<](/)?img[^>]*[>]", " ")
                .replaceAll("(?i)[<]caption[^/>]*[>]", "|+")
                .replaceAll("(?i)</caption>", " ")
                .replaceAll("(?i)[<]table[^/>]*class=\"wikitable\"[^/>]*[>]", "{| class=\"wikitable\"")
                .replaceAll("(?i)[<]table[^/>]*[>]", "{|")
                .replaceAll("(?i)[<]tr[^/>]*[>]", "\n|-")
                .replaceAll("(?i)[<]td[^/>]*[>]", "\n|")
                .replaceAll("(?i)[<]th[^/>]*[>]", "\n!")
                .replaceAll("(?i)</table>", "\n|}")
                .replaceAll("(?i)</tr>", "\n")
                .replaceAll("(?i)</td>", "\n")
                .replaceAll("(?i)</th>", "\n")
                .replaceAll("(?i)(<gallery>).*?(</gallery>)", " ")
                .replaceAll("(?i)(<ref>).*?(</ref>)", " ")
                .replaceAll("(?i)<ref name=.*?>.*?</ref>", "")
                .replaceAll("(?i)[<](/)?[\\w]+[^>\n<]*[>]", " ")
                ;
    }


    public static String cleanHTML(String html) {
        return html.replaceAll("(?i)[<]table[^/>]*[>][^\\w^[</table>]]*[</table>]", "");
    }
}
