package edu.northwestern.websail.wikiparser.exporter;

import com.mongodb.DB;
import com.mongodb.WriteResult;
import edu.northwestern.websail.wikiparser.model.WikiExtractedPage;
import edu.northwestern.websail.wikiparser.model.table.WikiTable;
import org.jongo.Jongo;
import org.jongo.MongoCollection;

/**
 * @author NorThanapon
 */
public class MongoDBPageExporter extends ErrorPageFileExporter {
    private MongoCollection pages;
    private MongoCollection tables;

    public MongoDBPageExporter(String errorPath, DB db) {
        super(errorPath);
        Jongo jongo = new Jongo(db);
        String collectionName = "page";
        this.pages = jongo.getCollection(collectionName);
        String tableCollectionName = "tables";
        this.tables = jongo.getCollection(tableCollectionName);
    }

    @Override
    public boolean doneExtractingPage(WikiExtractedPage page) {
        WriteResult r = pages.save(page);
        return r.getN() > 0;
    }

    public boolean doneExtractingTable(WikiTable table) {
        WriteResult r = tables.save(table);
        return r.getN() > 0;
    }
}
