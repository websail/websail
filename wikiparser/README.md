# Wiki Text Parser
This parser is a wrapper of sweble parser.

## Dependencies
* Java 7
* Maven
* MongoDB

## How to parse Wikipedia to MongoDB
1. Download Wikipedia article dump
    * [English Wikipedia Dump](https://dumps.wikimedia.org/enwiki/)
    * Look for `pages-articles` **without** `multistream`
    * On Unix server, use `wget <URL>` command
2. Package and upload jar file
    1. Go to `websail` directory (parent of this project)
    2. Run `mvn clean package -am -pl wikiparser`
        * This will generate a couple jar files in `wikiparser/target` directory
        * We need to use `websail-wikiparser-0.0.1-jar-with-dependencies.jar`
    3. Use `scp` to upload the jar file
        * On Unix: `scp wikiparser/target/websail-wikiparser-0.0.1-jar-with-dependencies.jar <username>@<host>:`
        * On Windows: use [WinSCP](http://winscp.net/eng/download.php)
3. Collect template data
    * `java -XX:+UseG1GC -Xmx40g -cp <path-to-jar-file> edu.northwestern.websail.wikiparser.template.PageTemplateDriver <path-to-dump-directory> true <output>`
    * This will write template map (for parsing) to `<output>`
4. Crate Mongo database
    1. Log in to mongo:
        * `mongo -u <username> -p <password> admin`
        * root username and password
    2. Create database and account
        * `use <database>` to create `<database>`
        * Refer to [creating account](http://docs.mongodb.org/manual/reference/method/db.createUser/)
5. Parse Wikipedia
    1. Go to where the jar file is
    2. Create `config/mongo.properties` with the content in the below listing (5). On our server, the `<host>` is `stark.cs.northwestern.edu` and other credentials as in step 4
    3. Create `error_pages/en` to store error pages.
    4. Run the parser
        * `java -XX:+UseG1GC -Xmx50g -cp <path-to-jar-file> edu.northwestern.websail.wikiparser.example.PageExtractionDriver <path-to-dump-directory> true en <template map>  true > std.out 2> std.err`
        * This will read dump files from `<path-to-dump-directory>` and save the parsed result to mongodb as configured in the previous step
    5. Listing:

            hostName=<host>
            username=<username>
            password=<password>
            dbName=<database name>

6. Create indexes
    1. Log in to mongo to the database
    2. Run the following commands to create 3 indexes
        * db.page.createIndex({'title.id': 1})
        * db.page.createIndex({'title.title': 1})
        * db.page.createIndex({'title.redirecting': 1})
