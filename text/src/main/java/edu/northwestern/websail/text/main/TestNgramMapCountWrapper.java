package edu.northwestern.websail.text.main;

import edu.berkeley.nlp.lm.WordIndexer;
import edu.berkeley.nlp.lm.io.IOUtils;
import edu.northwestern.websail.text.ngram.NgramMapCountWrapper;
import edu.northwestern.websail.text.ploader.NGramPLoader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author NorThanapon
 * @since 3/17/15
 */
@SuppressWarnings("ALL")
public class TestNgramMapCountWrapper {
    public static void main(String[] args) throws IOException {
        String wFile = "/Users/NorThanapon/Desktop/corpus_ncase_stem/web1t_papers.widx";//args[0];
        String mFile = "/Users/NorThanapon/Desktop/corpus_ncase_stem/web1t_papers.bmap";//args[1];
        WordIndexer<String> wordIndexer = (WordIndexer<String>) IOUtils.readObjFileHard(wFile);
        NgramMapCountWrapper map = NGramPLoader.loadNgramMapCountWrapper(mFile, wordIndexer);

        System.out.println("LM loaded, order " + map.getMaxOrder());
        System.out.println("Unigram Sum: " + map.getUnigramCount());

        while (true) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter ngram...\n");
            String testText = br.readLine();

            long web1tCount = map.getCount(testText.trim().split("\\s+"));
            Long web1tTopCount = map.getTopCountNgram(testText.trim().split("\\s+").length);
            if (web1tTopCount == null) web1tTopCount = map.getTopCountNgram(map.getMaxOrder());
            System.out.println((double) web1tCount / (double) web1tTopCount);
            System.out.println(Math.log10((double) web1tCount) - Math.log10((double) web1tTopCount));
            System.out.println();
            if (testText.equals("<exit>")) break;
        }

    }
}
