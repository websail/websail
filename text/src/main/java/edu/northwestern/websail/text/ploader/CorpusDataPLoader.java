package edu.northwestern.websail.text.ploader;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.gs.collections.impl.map.mutable.UnifiedMap;
import edu.northwestern.websail.core.pipeline.config.PEVResourceLoader;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.text.topics.MalletHelper;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 7/29/15
 */
public class CorpusDataPLoader {
    public static UnifiedMap<String, double[]> loadTopicDistMap(PipelineEnvironment env, Properties config,
                                                             Boolean global, Boolean put)
            throws IOException, ClassNotFoundException {
        String filename = config.getProperty("corpus.topic.distMapFile");
        UnifiedMap<String, double[]> topicMap = MalletHelper.readTopicDistributionMap(filename);
        if (put) {
            PEVResourceLoader.setResource(env, PEVResourceType.TOPIC_DIST_MAP, global, topicMap);
        }
        return topicMap;
    }

    public static DirectoryReader loadIndexDR(PipelineEnvironment env, Properties config,
                                              Boolean global, Boolean put) throws IOException {

        String indexDirectory = config.getProperty("corpus.index.directory");
        File indexFile = new File(indexDirectory);
        Directory directory = FSDirectory.open(indexFile);
        DirectoryReader reader = DirectoryReader.open(directory);
        if (put) {
            PEVResourceLoader.setResource(env, PEVResourceType.INDEX_READER, global, reader);
        }
        return reader;
    }

    public static HashMap<Integer, String> loadIndexDocIdMap(PipelineEnvironment env, Properties config,
                                                             Boolean global, Boolean put) throws IOException {
        String indexMapFile = config.getProperty("corpus.index.docIdMapBinFile");
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(indexMapFile));
        Input input = new Input(ois);
        Kryo kryo = new Kryo();
        @SuppressWarnings("unchecked")
        HashMap<Integer, String> luceneDocIdMap = kryo.readObject(input, HashMap.class);
        if (put) {
            PEVResourceLoader.setResource(env, PEVResourceType.INDEX_ID_MAP, global, luceneDocIdMap);
        }
        return luceneDocIdMap;
    }
}
