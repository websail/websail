package edu.northwestern.websail.text.chunker;

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.tokenizer.StanfordNLPSSplit;
import edu.northwestern.websail.text.util.StanfordNLPProperties;
import edu.northwestern.websail.text.util.TokenSpanUtil;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasOffset;
import edu.stanford.nlp.ling.Label;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.util.CoreMap;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class StanfordChunker extends Chunker {
    private final StanfordCoreNLP pipeline;
    public StanfordChunker() {
        super();
        Properties props = StanfordNLPProperties.srParserConfigure();
        props.put("annotators", "tokenize, ssplit, pos, lemma, ner, parse");
        pipeline = new StanfordCoreNLP(props);
    }

    public static void main(String[] args) throws Exception {
        String text = "Erik Ehn is an American playwright and director known for proposing the Regional Alternative Theatre movement. "
                + "The former dean of theater at CalArts , "
                + "the California Institute of Arts, he is head of playwriting and professor of theatre. "
                + "and performance studies at Myrtle Beach Film festival . "
                + "His published works include The Saint Plays, Beginner \n and 13 Christs."
                + " Erik's mother was a dancer. combsum and combmnz strategy";
        StanfordChunker ssa = new StanfordChunker();
        PrintNounPhraseDelegate delegate = new PrintNounPhraseDelegate();
        ssa.initialize(text, null);
        ssa.extractNounPhrase(delegate);
    }

    @Override
    public void initialize(FastList<TokenSpan> sentences, String source) {
        super.initialize(sentences, source);
        logger.warning("Sentences will be ignored. Use StanfordChunker.initialize(String, String).");
        this.currentText = TokenSpanUtil.spans2String(sentences);
    }

    @Override
    public void extractNounPhrase(NounPhraseFoundDelegate delegate)
            throws Exception {
        String text = this.currentText;
        Annotation document = new Annotation(text);
        pipeline.annotate(document);
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        int iPhrase = 0;
        int iToken = 0;
        int iSentence = 0;
        for (CoreMap sentence : sentences) {
            TokenSpan outSentence = StanfordNLPSSplit.extractSentence(sentence, iToken, iSentence);
            if (outSentence == null) continue;
            delegate.setSentence(outSentence);
            iSentence++;
            iToken += outSentence.getTokens().size();
            Tree tree = sentence.get(TreeAnnotation.class);
            // System.out.println(tree);
            LinkedList<TokenSpan> stack = new LinkedList<TokenSpan>();
            for (Tree subtree : tree) {
                if (!subtree.label().value().equals("NP"))
                    continue;
                // System.out.println(subtree);
                FastList<Token> phrase = new FastList<Token>();
                boolean first = true;

                for (Tree leaf : subtree.getLeaves()) {
                    // this is the text of the token
                    Label token = leaf.label();
//                    String word = ((CoreLabel) token).get(TextAnnotation.class);
                    String pos = ((CoreLabel) token)
                            .get(PartOfSpeechAnnotation.class);
//                    String ne = ((CoreLabel) token)
//                            .get(NamedEntityTagAnnotation.class);
                    HasOffset ofs = (HasOffset) token;
                    int startOffset = ofs.beginPosition();
//                    int endOffset = ofs.endPosition();
                    if (first && Chunker.isIgnoredFirstNPToken(pos)) {
                        first = false;
                        continue;
                    }
                    first = false;
                    Token t = findTokenWithStartOffset(outSentence, startOffset);
                    if (t == null) {
                        phrase.clear();
                        break;
                    }
                    phrase.add(t);
                }
                if (phrase.size() < 1)
                    continue;
                TokenSpan span = new TokenSpan(phrase.get(0).getStartOffset(),
                        phrase.get(phrase.size() - 1).getEndOffset(), phrase,
                        iPhrase++);
                while (stack.size() > 0
                        && !(span.getStartOffset() >= stack.getFirst()
                        .getStartOffset() && span.getEndOffset() <= stack
                        .getFirst().getEndOffset())) {
                    stack.removeFirst();
                }
                if (stack.size() == 0) {
                    delegate.found(span);
                } else
                    delegate.foundSub(stack.getFirst(), span);
                stack.addFirst(span);

            }
        }
    }

    private Token findTokenWithStartOffset(TokenSpan span, int start) {
        for (Token t : span.getTokens()) {
            if (t.getStartOffset() == start) {
                return t;
            }
        }
        return null;
    }

    // ==========================================//
    // Test methods
    // ==========================================//

    public HashSet<String> getNounPharseList(String text) {
        HashSet<String> neSet = new HashSet<String>();
        Annotation document = new Annotation(text);
        pipeline.annotate(document);
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);

        String prevNer = "";
        String namedEntity = "";
        for (CoreMap sentence : sentences) {
            for (CoreLabel token : sentence
                    .get(CoreAnnotations.TokensAnnotation.class)) {
                String word = token.get(TextAnnotation.class);
                if (token.ner().equalsIgnoreCase("O")) {
                    if (!namedEntity.equalsIgnoreCase("")) {
                        neSet.add(namedEntity.substring(0,
                                namedEntity.length() - 1));
                        namedEntity = "";
                    }
                } else {

                    if (!token.ner().equalsIgnoreCase(prevNer)) {
                        if (!namedEntity.equalsIgnoreCase("")) {
                            neSet.add(namedEntity.substring(0,
                                    namedEntity.length() - 1));
                            namedEntity = word + " ";
                        } else {
                            namedEntity += word + " ";
                        }
                    } else {
                        namedEntity += word + " ";
                    }
                }
                prevNer = token.ner();
            }
        }

        if (!namedEntity.equalsIgnoreCase("")) {
            neSet.add(namedEntity.substring(0, namedEntity.length() - 1));
        }
        return neSet;
    }
}
