package edu.northwestern.websail.text.util;

import edu.northwestern.websail.text.data.TokenSpan;

import java.util.List;

/**
 * @author NorThanapon
 * @since 11/10/14
 */
public class TokenSpanUtil {
    public static String spans2String(List<? extends TokenSpan> spans) {
        return spans2String(spans, ' ');
    }

    public static String spans2String(List<? extends TokenSpan> spans, char separator) {
        StringBuilder sb = new StringBuilder();
        for (TokenSpan span : spans) {
            sb.append(TokenUtil.tokens2Str(span.getTokens())).append(separator);
        }
        return sb.substring(0, sb.length() - 1);
    }
}
