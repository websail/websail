package edu.northwestern.websail.text.chunker;

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.text.data.TaggedToken;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.data.TokenSpan;

import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author NorThanapon
 *         <p/>
 *         currently only implemented noun phrase finding
 */
public abstract class Chunker {
    public static final Logger logger = Logger.getLogger(Chunker.class.getName());
    public static final HashSet<String> firstNPIgnoreList = new HashSet<String>();

    static {
        Chunker.firstNPIgnoreList.add("DT");
        Chunker.firstNPIgnoreList.add("PRP$");
        Chunker.firstNPIgnoreList.add("PRP");
        Chunker.firstNPIgnoreList.add("WDT");
        Chunker.firstNPIgnoreList.add("CC");
        Chunker.firstNPIgnoreList.add("WP");
        Chunker.firstNPIgnoreList.add("WRB");
        Chunker.firstNPIgnoreList.add("WP$");
        Chunker.firstNPIgnoreList.add(",");
        Chunker.firstNPIgnoreList.add("VBP");
        Chunker.firstNPIgnoreList.add("VBZ");
        Chunker.firstNPIgnoreList.add("VBP");
        Chunker.firstNPIgnoreList.add("VBD");
        Chunker.firstNPIgnoreList.add("VB");
        Chunker.firstNPIgnoreList.add("POS");
        Chunker.firstNPIgnoreList.add("MD");
        Chunker.firstNPIgnoreList.add("#");
        Chunker.firstNPIgnoreList.add("-LRB-");
        Chunker.firstNPIgnoreList.add("-RRB-");
        Chunker.firstNPIgnoreList.add("-LCB-");
        Chunker.firstNPIgnoreList.add("-RCB-");
        Chunker.firstNPIgnoreList.add("-LSB-");
        Chunker.firstNPIgnoreList.add("-RSB-");
        Chunker.firstNPIgnoreList.add("(");
        Chunker.firstNPIgnoreList.add(")");
        Chunker.firstNPIgnoreList.add("{");
        Chunker.firstNPIgnoreList.add("}");
        Chunker.firstNPIgnoreList.add("[");
        Chunker.firstNPIgnoreList.add("]");
        Chunker.firstNPIgnoreList.add("''");
        Chunker.firstNPIgnoreList.add(":");
        Chunker.firstNPIgnoreList.add("IN");
        Chunker.firstNPIgnoreList.add("TO");
        Chunker.firstNPIgnoreList.add(".");
    }

    protected String currentText;
    protected String currentSource;
    protected List<TokenSpan> currentSentences;

    public Chunker() {
    }

    protected static boolean isIgnoredFirstNPToken(String pos) {
        return Chunker.firstNPIgnoreList.contains(pos);
    }

    public static TokenSpan cleanNounPhrase(TokenSpan span) {
        int start = 0;
        for (Token token : span.getTokens()) {
            if (!(token instanceof TaggedToken)) {
                throw new UnsupportedOperationException("The input TokenSpan needs to contain TaggedToken.");
            }
            TaggedToken tt = (TaggedToken) token;
            if (Chunker.firstNPIgnoreList.contains(tt.getPOS())) {
                start++;
            }
        }
        if (start >= span.getTokens().size()) return null;
        else
            return span.subSpan(start, span.getTokens().size());
    }

    public void initialize(String text, String source) {
        this.currentText = text;
        this.currentSource = source;
        this.currentSentences = null;
    }

    public void initialize(FastList<TokenSpan> sentences, String source) {
        this.currentSource = source;
        this.currentText = null;
        this.currentSentences = sentences;
    }

    public abstract void extractNounPhrase(NounPhraseFoundDelegate delegate) throws Exception;

    // ==========================================//
    // AUX class
    // ==========================================//
    public static class PrintNounPhraseDelegate implements
            NounPhraseFoundDelegate {
        public PrintNounPhraseDelegate() {

        }
        @Override
        public void found(TokenSpan nounPhrase) {
            System.out.println(nounPhrase.getTokens());
        }

        @Override
        public void foundSub(TokenSpan cover, TokenSpan nounPhrase) {

            System.out.println("COVER: "
                    + cover.getTokens());
            System.out.println("SUB: "
                    + nounPhrase.getTokens());
        }

        @Override
        public void setSentence(TokenSpan sentence) {
            if (sentence==null){
                System.out.println("Null sentences");
            }
        }
    }

}
