package edu.northwestern.websail.text.corpus.util;

import edu.northwestern.websail.ds.indexer.HashIndexer;
import edu.northwestern.websail.text.corpus.model.stat.CorpusStat;
import edu.northwestern.websail.text.corpus.model.stat.IndexerCorpusStat;
import edu.northwestern.websail.text.corpus.model.term.TermMeasureStat;

import java.io.*;
import java.util.Set;
import java.util.logging.Logger;


public class CorpusStatUtil {

    public static final Logger logger = Logger.getLogger(CorpusStatUtil.class.getName());

    @SuppressWarnings("unchecked")
    public static void prune2IndexerCorpusStat(int minTF, //5
                                               int minDF, //2
                                               String inStatFile,
                                               String outIndexerFile, // "../data/corpus_stat/vocab.index.bin";
                                               String outPrunedStatFile, // "../data/corpus_stat/pruned_stat.bin";
                                               String outReadableIndexerFile, // "../data/corpus_stat/vocab.index.txt";
                                               String stopwordFile
    ) throws IOException, ClassNotFoundException {
        Set<String> customStopWords = StopWordUtil.readStopWords(stopwordFile, true);
        StopWordUtil.addCustomStopWords(customStopWords);
        HashIndexer<String> indexer;
        File indexerFile = new File(outIndexerFile);
        if (indexerFile.exists()) {
            logger.info("Indexer exists, loading...");
            ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(
                    new FileInputStream(outIndexerFile)));
            indexer = (HashIndexer<String>) ois.readObject();
            ois.close();
            logger.info("Indexer size: " + indexer.size());
        } else {
            logger.info("Init empty indexer");
            indexer = new HashIndexer<String>(String.class);
        }
        logger.info("Deserializing corpus stat...");
        CorpusStat stat = deserializeBinary(inStatFile);
        IndexerCorpusStat pStat = new IndexerCorpusStat(stat.getName(),
                stat.getTotalDocs(), indexer);
        logger.info("Total of " + stat.getTotalTerms() + " terms");
        for (TermMeasureStat str : stat.getTerms().values()) {
            if (str.getTf() < minTF)
                continue;
            if (str.getDf1() < minDF)
                continue;
            if (str.getTerm().length() < 3)
                continue;
            if (customStopWords.contains(str.getTerm().toLowerCase()))
                continue;
            pStat.addTerm(str);
        }
        logger.info("Terms left the indexer: " + indexer.size());
        logger.info("Serializing binary indexer file");
        ObjectOutputStream oos = new ObjectOutputStream(
                new BufferedOutputStream(new FileOutputStream(outIndexerFile)));
        oos.writeObject(indexer);
        oos.flush();
        oos.close();
        logger.info("Serializing readable indexer file");
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(outReadableIndexerFile), "UTF-8"));
        indexer.writeReadable(out);
        logger.info("Serializing binary corpus stat file");
        ObjectOutputStream oos2 = new ObjectOutputStream(
                new BufferedOutputStream(new FileOutputStream(outPrunedStatFile)));
        oos2.writeObject(pStat);
        oos2.flush();
        oos2.close();
    }

    public static CorpusStat deserializeBinary(String filename) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename));
        Object object = ois.readObject();
        ois.close();
        return (CorpusStat) object;
    }

    public static void serializeBinary(CorpusStat stat, String filename) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename));
        oos.writeObject(stat);
        oos.flush();
        oos.close();
    }
}
