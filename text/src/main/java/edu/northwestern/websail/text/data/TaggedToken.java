package edu.northwestern.websail.text.data;

/**
 * @author NorThanapon
 */

public class TaggedToken extends Token {

    private static final long serialVersionUID = -8939090179339475906L;
    private String pos;
    private String ner;
    private Integer edgeId;

    public TaggedToken(String text, int start, int end, int index, String POS, String ner) {
        super(text, start, end, index);
        this.pos = POS;
        this.ner = ner;
    }

    public String getPOS() {
        return pos;
    }

    public void setPOS(String pos) {
        this.pos = pos;
    }


    public String getNER() {
        return ner;
    }

    public void setNER(String NER) {
        ner = NER;
    }

    public Integer getEdgeId() {
        return edgeId;
    }

    public void setEdgeId(Integer edgeId) {
        this.edgeId = edgeId;
    }

    public String toString() {
        return super.toString() + "/" + pos + "/" + ner;
    }
}
