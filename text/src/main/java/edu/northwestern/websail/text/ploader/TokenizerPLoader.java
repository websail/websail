package edu.northwestern.websail.text.ploader;

import edu.northwestern.websail.core.pipeline.config.PEVResourceLoader;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.text.chunker.Chunker;
import edu.northwestern.websail.text.tokenizer.SentenceSpliter;
import edu.northwestern.websail.text.tokenizer.Tokenizer;

import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 11/21/14
 */

public class TokenizerPLoader {

    public static Tokenizer tokenizer(PipelineEnvironment env, Properties config, Boolean global, Boolean put)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        String tokenizerClassName = config.getProperty("text.tokenizer.class");
        if (tokenizerClassName == null) {
            PEVResourceLoader.logger.severe("'text.tokenizer.class' configuration cannot be found.");
            return null;
        }
        Tokenizer tokenizer = (Tokenizer) Class.forName(tokenizerClassName).newInstance();
        if (put) {
            PEVResourceLoader.setResource(env, PEVResourceType.TOKENIZER, global, tokenizer);
        }
        return tokenizer;
    }

    public static Tokenizer[] tokenizers(PipelineEnvironment env, Properties config, Boolean global, Boolean put)
            throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        int poolSize = PEVResourceLoader.getPoolSize(config, "text.tokenizer.poolSize");
        Tokenizer[] tokenizers = new Tokenizer[poolSize];
        for (int i = 0; i < poolSize; i++) {
            tokenizers[i] = tokenizer(env, config, global, false);
        }
        if (put) {
            PEVResourceLoader.setResource(env, PEVResourceType.TOKENIZER_POOL, global, tokenizers);
        }
        return tokenizers;
    }

    public static SentenceSpliter sentenceSpliter(PipelineEnvironment env, Properties config, Boolean global, Boolean put)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        String sentenceSpliterClass = config.getProperty("text.tokenizer.SentenceSpliter.class");
        Boolean pos = Boolean.parseBoolean(config.getProperty("text.tokenizer.SentenceSpliter.pos", "false"));
        if (sentenceSpliterClass == null) {
            PEVResourceLoader.logger.severe("'text.tokenizer.SentenceSpliter.class' configuration cannot be found.");
            return null;
        }
        SentenceSpliter sentenceSpliter = (SentenceSpliter) Class.forName(sentenceSpliterClass).getDeclaredConstructor(Boolean.class).newInstance(pos);
        if (put) {
            PEVResourceLoader.setResource(env, PEVResourceType.S_SENTENCE_SPLITER, global, sentenceSpliter);
        }
        return sentenceSpliter;
    }

    public static SentenceSpliter[] sentenceSpliters(PipelineEnvironment env, Properties config, Boolean global, Boolean put)
            throws IllegalAccessException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
        int poolSize = PEVResourceLoader.getPoolSize(config, "text.tokenizer.poolSize");
        SentenceSpliter[] sentenceSpliters = new SentenceSpliter[poolSize];
        for (int i = 0; i < poolSize; i++) {
            sentenceSpliters[i] = sentenceSpliter(env, config, global, false);
        }
        if (put) {
            PEVResourceLoader.setResource(env, PEVResourceType.S_SENTENCE_SPLITER_POOL, global, sentenceSpliters);
        }
        return sentenceSpliters;
    }

    public static Chunker chunker(PipelineEnvironment env, Properties config, Boolean global, Boolean put)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        String chunkerClassName = config.getProperty("text.tokenizer.Chunker.class");
        if (chunkerClassName == null) {
            PEVResourceLoader.logger.severe("'text.tokenizer.Chunker.class' configuration cannot be found.");
            return null;
        }
        Chunker chunker = createChunker(chunkerClassName, (SentenceSpliter) env.getResource(PEVResourceType.S_SENTENCE_SPLITER));
        if (put) {
            PEVResourceLoader.setResource(env, PEVResourceType.S_CHUNKER, global, chunker);
        }
        return chunker;
    }

    public static Chunker[] chunkers(PipelineEnvironment env, Properties config, Boolean global, Boolean put)
            throws IllegalAccessException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
        String chunkerClassName = config.getProperty("text.tokenizer.Chunker.class");
        if (chunkerClassName == null) {
            PEVResourceLoader.logger.severe("'text.tokenizer.Chunker.class' configuration cannot be found.");
            return null;
        }
        int poolSize = PEVResourceLoader.getPoolSize(config, "text.tokenizer.poolSize");
        Chunker[] chunkers = new Chunker[poolSize];
        for (int i = 0; i < poolSize; i++) {
            chunkers[i] = createChunker(chunkerClassName,
                    (SentenceSpliter) env.getPoolResource(PEVResourceType.S_SENTENCE_SPLITER_POOL, i, PEVResourceType.S_SENTENCE_SPLITER));
        }
        if (put) {
            PEVResourceLoader.setResource(env, PEVResourceType.S_CHUNKER_POOL, global, chunkers);
        }
        return chunkers;
    }

    private static Chunker createChunker(String chunkerClassName, SentenceSpliter sentenceSpliter)
            throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Chunker chunker;
        if (chunkerClassName.contains("OpenNLP") && sentenceSpliter != null) {
            chunker = (Chunker) Class.forName(chunkerClassName).getDeclaredConstructor(SentenceSpliter.class).newInstance(sentenceSpliter);
        } else {
            chunker = (Chunker) Class.forName(chunkerClassName).newInstance();
        }
        return chunker;
    }


}
