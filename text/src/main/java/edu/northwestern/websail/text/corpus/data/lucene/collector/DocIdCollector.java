package edu.northwestern.websail.text.corpus.data.lucene.collector;

import org.apache.lucene.index.AtomicReaderContext;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.Scorer;

import java.io.IOException;
import java.util.ArrayList;

public class DocIdCollector extends Collector {
    protected final ArrayList<Integer> docIds;
    private int totalDocs = 0;
    private int base = 0;

    public DocIdCollector() {
        docIds = new ArrayList<Integer>();
    }

    @Override
    public boolean acceptsDocsOutOfOrder() {
        return true;
    }

    @Override
    public void collect(int docId) throws IOException {
        totalDocs++;
        docIds.add(docId + base);
    }

    @Override
    public void setNextReader(AtomicReaderContext context)
            throws IOException {
        this.base = context.docBase;
    }

    @Override
    public void setScorer(Scorer score) throws IOException {

    }

    public int getTotalDocs() {
        return totalDocs;
    }

    public ArrayList<Integer> getDocIds() {
        return docIds;
    }
}
