package edu.northwestern.websail.text.corpus.model.stat;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import edu.northwestern.websail.ds.indexer.Indexer;
import edu.northwestern.websail.text.corpus.model.term.TermMeasureStat;
import edu.northwestern.websail.text.corpus.model.term.TermStat;
import edu.northwestern.websail.text.corpus.util.Measurement;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class IndexerCorpusStat extends CorpusStat {

    private static final long serialVersionUID = 7086602877642216327L;
    private Indexer<String> wordIndexer;
    private ArrayList<TermMeasureStat> objects;
    private Kryo kryo;

    public IndexerCorpusStat(String name, int totalDocs,
                             Indexer<String> wordIndexer) {
        super(name, totalDocs);
        this.wordIndexer = wordIndexer;
        this.kryo = createKryo();
        objects = new ArrayList<TermMeasureStat>(this.wordIndexer.size());
        for (int i = 0; i < this.wordIndexer.size(); i++) {
            objects.add(null);
        }
    }

    public IndexerCorpusStat() {
        super("", 0);
    }

    public static Kryo createKryo() {
        Kryo kryo = new Kryo();
        kryo.register(HashMap.class);
        kryo.register(ArrayList.class);
        kryo.register(TermMeasureStat.class);
        return kryo;
    }

    public void addTerm(String term, long tf) {
        TermMeasureStat t = new TermMeasureStat(term, tf);
        Measurement.computeMeasures(t, this.totalDocs);
        t.setTerm(null);
        boolean added = this.wordIndexer.add(t.getTerm());
        if (added)
            this.objects.add(t);
        else
            this.objects.set(this.wordIndexer.indexOf(term), t);
        this.totalTerms++;
        this.totalTermFrequency += tf;
    }

    public void addTerm(TermStat t) {
        TermMeasureStat tM = new TermMeasureStat(t);
        Measurement.computeMeasures(tM, this.totalDocs);

        String term = tM.getTerm();
        tM.setTerm(null);
        boolean added = this.wordIndexer.add(term);
        if (added)
            this.objects.add(tM);
        else
            this.objects.set(this.wordIndexer.indexOf(term), tM);
        this.totalTerms++;
        this.totalTermFrequency += t.getTf();
    }

    public void addTerm(TermMeasureStat t) {
        String term = t.getTerm();
        t.setTerm(null);
        boolean added = this.wordIndexer.add(term);
        if (added)
            this.objects.add(t);
        else
            this.objects.set(this.wordIndexer.indexOf(term), t);
        this.totalTerms++;
        this.totalTermFrequency += t.getTf();
    }

    public TermMeasureStat getTermMeasureStat(String key) throws IOException {
        int index = wordIndexer.indexOf(key);
        if (index == -1)
            return null;
        else {
            if (index >= objects.size())
                return null;
            TermMeasureStat tm = objects.get(index);
            if (tm == null) return null;
            tm.setTerm(key);
            return tm;
        }

    }

    public Indexer<String> getWordIndexer() {
        return wordIndexer;
    }

    public void setWordIndexer(Indexer<String> wordIndexer) {
        this.wordIndexer = wordIndexer;
    }

    public ArrayList<TermMeasureStat> getObjects() {
        return objects;
    }

    public void setObjects(ArrayList<TermMeasureStat> objects) {
        this.objects = objects;
    }

    private void writeObject(ObjectOutputStream oss) throws IOException {
        Output output = new Output(oss);
        this.kryo.writeObject(output, this.getName());
        output.flush();
        this.kryo.writeObject(output, this.getTotalTerms());
        output.flush();
        this.kryo.writeObject(output, this.getTotalDocs());
        output.flush();
        this.kryo.writeObject(output, this.getTotalTermFrequency());
        output.flush();
        this.kryo.writeObject(output, this.objects);
        output.flush();
    }

    @SuppressWarnings("unchecked")
    private void readObject(ObjectInputStream ois)
            throws ClassNotFoundException, IOException {
        this.kryo = createKryo();
        Input input = new Input(ois);
        this.name = this.kryo.readObject(input, String.class);
        this.totalTerms = this.kryo.readObject(input, Integer.class);
        this.totalTermFrequency = this.kryo.readObject(input, Long.class);
        this.totalDocs = this.kryo.readObject(input, Integer.class);
        this.objects = this.kryo.readObject(input, ArrayList.class);
    }
}
