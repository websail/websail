package edu.northwestern.websail.text.tokenizer;

/**
 * @author NorThanapon
 */

import com.gs.collections.impl.set.mutable.UnifiedSet;
import edu.northwestern.websail.text.data.Token;

import java.util.List;
import java.util.Set;

public abstract class Tokenizer {
    protected String text;
    protected Set<String> stopwords;
    protected boolean toLower;

    public Tokenizer() {
        stopwords = new UnifiedSet<String>();
        toLower = false;
    }

    public void initialize(String text) {
        this.text = text;
    }

    public boolean isToLower() {
        return toLower;
    }

    public void setToLower(boolean toLower) {
        this.toLower = toLower;
    }
    public void setStopwords(Set<String> stopwords) {
        this.stopwords = stopwords;
    }

    public abstract List<Token> getAllTokens();

    public abstract Token next();

    public abstract Token getCurrentToken();

    public abstract void clear();

}
