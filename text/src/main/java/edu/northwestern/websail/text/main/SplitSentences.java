package edu.northwestern.websail.text.main;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import java.io.*;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 8/18/15
 */
public class SplitSentences {
    public static void main(String args[]) throws IOException {
        String inputFile = args[0];
        String outputFile = args[1];

        Properties props = new Properties();
        props.put("annotators", "tokenize, ssplit");
        props.put("ssplit.newlineIsSentenceBreak", "always");
        props.put("ssplit.htmlBoundariesToDiscard", ".*");
        props.put("tokenize.options", "untokenizable=noneKeep,ptb3Escaping=true");

        StanfordCoreNLP corenlp = new StanfordCoreNLP(props);
        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)));
        PrintStream out = new PrintStream(new FileOutputStream(outputFile));
        String line;
        while ((line = in.readLine()) != null) {
            Annotation doc = new Annotation(line);
            corenlp.annotate(doc);
            for (CoreMap sentence : doc.get(CoreAnnotations.SentencesAnnotation.class)) {
                boolean first = true;
                for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                    String word = token.get(CoreAnnotations.TextAnnotation.class);
                    if (first) {
                        first = false;
                    } else {
                        out.print(' ');
                    }
                    out.print(word);
                }
                out.println();
            }
        }
    }
}
