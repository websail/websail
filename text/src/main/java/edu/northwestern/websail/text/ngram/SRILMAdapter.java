/**
 * @author NorThanapon
 */

package edu.northwestern.websail.text.ngram;

import edu.northwestern.websail.text.tokenizer.SentenceSpliter;

import java.io.*;
import java.util.List;

/*
 * This class is an adapter for SRILM 
 * initially ngram-count (http://www.speech.sri.com/projects/srilm/manpages/ngram-count.1.html)
 * A machine needs to install SRILM and can execute ngram-count without specifying path or
 * modifying NGRAMCOUNT to give a full path of the command
 */
public class SRILMAdapter {
    public static final String NGRAM_COUNT = "ngram-count";
    public static final String NGRAM_MERGE = "ngram-merge";
    public static boolean VERBOSE = false;
    public final boolean lowercase = false;
    public final boolean stem = false;
    private final SentenceSpliter sSpliter;
    public String INTEMP = "temp.in";
    public String OUTTEMP = "temp.out";
    private boolean deleteTemp = true;
    private String discount = "kndiscount";

    public SRILMAdapter(SentenceSpliter sSpliter) {
        this.sSpliter = sSpliter;
    }

    /*
     * merge ngram-count count files
     */
    public boolean ngramMerge(List<String> inCountFiles, String outCountFile) {
        StringBuilder cmdSB = new StringBuilder();
        cmdSB.append(NGRAM_MERGE);
        cmdSB.append(" -write ");
        cmdSB.append(outCountFile);
        for (String file : inCountFiles) {
            cmdSB.append(" ");
            cmdSB.append(file);
        }
        cmdSB.append(" > ");
        cmdSB.append(OUTTEMP);
        String command = cmdSB.toString();
        if (VERBOSE)
            System.out.println(command);
        String[] cmd = {"/bin/sh", "-c", command};
        boolean success;
        try {
            Runtime rt = Runtime.getRuntime();
            Process pr = rt.exec(cmd);
            pr.waitFor();
            if (VERBOSE)
                System.out.println(this.readOutput());
            success = true;
        } catch (Exception e) {
            e.printStackTrace();
            deleteTempFile();
            success = false;
        }
        this.deleteTempFile();
        return success;
    }

    /*
     * initial run with out any input other than text
     */
    public boolean ngramCount(String text, int order, String outVocabFile,
                              String outCountFile, String outLMFile) {
        return ngramCount(text, order, null, null, null, outVocabFile,
                outCountFile, outLMFile);
    }

    /*
     * initial run with out any input other than text with text from INTEMP
     * (addTextToInputFile)
     */
    public boolean ngramCount(int order, String outVocabFile,
                              String outCountFile, String outLMFile) {
        return ngramCount(null, order, null, null, null, outVocabFile,
                outCountFile, outLMFile);
    }

    /*
     * initial run with out any input other than text with text from INTEMP
     * (addTextToInputFile) minimum in and out
     */
    public boolean ngramCount(int order, String outCountFile) {
        return ngramCount(null, order, null, null, null, null, outCountFile,
                null);
    }

    /*
     * second run with existing count and lm build a new vocab
     */
    public boolean ngramCount(String text, int order, String initCountFile,
                              String initLMFile, String outVocabFile, String outCountFile,
                              String outLMFile) {
        return ngramCount(text, order, null, initCountFile, initLMFile,
                outVocabFile, outCountFile, outLMFile);
    }

    /*
     * second run with existing count and lm with text from INTEMP
     * (addTextToInputFile) build a new vocab
     */
    public boolean ngramCount(int order, String initCountFile,
                              String initLMFile, String outVocabFile, String outCountFile,
                              String outLMFile) {
        return ngramCount(null, order, null, initCountFile, initLMFile,
                outVocabFile, outCountFile, outLMFile);
    }

    public boolean ngramCount(String text, int order, String initVocabFile,
                              String initCountFile, String initLMFile, String outVocabFile,
                              String outCountFile, String outLMFile) {
        return this.ngramCount(text, order, initVocabFile, initCountFile,
                initLMFile, outVocabFile, outCountFile, outLMFile, null);
    }

    /*
     * ex. ngram-count -text test2.txt -order 2 -write test2.count -write-vocab
     * test.vocab -read test.count -vocab test.vocab -lm test2.lm -init-lm
     * test.lm
     */
    public boolean ngramCount(String text, int order, String initVocabFile,
                              String initCountFile, String initLMFile, String outVocabFile,
                              String outCountFile, String outLMFile,
                              String[] outEachNGramCountFiles) {
        String command = preparedNGramCountCommand(text, order, initVocabFile,
                initCountFile, initLMFile, outVocabFile, outCountFile,
                outLMFile, outEachNGramCountFiles);
        if (VERBOSE)
            System.out.println(command);

        String[] cmd = {"/bin/sh", "-c", command};
        boolean success;
        try {
            Runtime rt = Runtime.getRuntime();
            Process pr = rt.exec(cmd);
            pr.waitFor();
            if (VERBOSE)
                System.out.println(this.readOutput());
            success = true;
        } catch (Exception e) {
            e.printStackTrace();
            deleteTempFile();
            success = false;
        }
        this.deleteTempFile();
        return success;
    }

    private String preparedNGramCountCommand(String text, int order,
                                             String initVocabFile, String initCountFile, String initLMFile,
                                             String outVocabFile, String outCountFile, String outLMFile,
                                             String[] outEachGramCountFiles) {
        String cmd = NGRAM_COUNT;
        StringBuilder cmdBuilder = new StringBuilder();
        try {
            cmdBuilder.append(cmd);
            cmdBuilder.append(" -text ").append(INTEMP);
            cmdBuilder.append(" -order ").append(order);
            cmdBuilder.append(" -write ").append(outCountFile);
            cmdBuilder.append(" -minprune 6 ");
            if (discount != null) cmdBuilder.append(" -").append(discount).append(" ");
            cmdBuilder.append(" -sort ");
            // the rest are optional
            if (text != null)
                writeInput(
                        NGramUtils.formatText(text, sSpliter, lowercase, stem),
                        false);
            if (initVocabFile != null)
                cmdBuilder.append(" -vocab ").append(initVocabFile);
            if (outVocabFile != null)
                cmdBuilder.append(" -write-vocab ").append(outVocabFile);
            if (initCountFile != null)
                cmdBuilder.append(" -read ").append(initCountFile);
            if (initLMFile != null)
                cmdBuilder.append(" -init-lm ").append(initLMFile);
            if (outLMFile != null)
                cmdBuilder.append(" -lm ").append(outLMFile);
            if (outEachGramCountFiles != null) {
                for (int i = 0; i < outEachGramCountFiles.length; i++) {
                    cmdBuilder.append(" -write").append(i + 1).append(" ").append(outEachGramCountFiles[i]);
                }
            }
            cmdBuilder.append(" &> ").append(OUTTEMP);
            cmd = cmdBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            deleteTempFile();
            cmd = null;
        }
        return cmd;
    }

    public long addTextToInputFile(String text) throws
            IOException {
        File f = new File(INTEMP);
        this.writeInput(NGramUtils.formatText(text, sSpliter, lowercase, stem)
                + "\n", f.exists());
        return f.length();
    }

    public long addFormatedTextToInputFile(String text) throws
            IOException {
        File f = new File(INTEMP);
        this.writeInput(text + "\n", f.exists());
        return f.length();
    }

    private void writeInput(String input, boolean append) throws IOException {
        FileOutputStream fs = new FileOutputStream(INTEMP, append);
        OutputStreamWriter writer = new OutputStreamWriter(fs, "UTF-8");
        try {
            writer.write(input);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            writer.close();
            fs.close();
        }
    }

    private String readOutput() throws IOException {
        FileInputStream fs = new FileInputStream(OUTTEMP);
        InputStreamReader reader = new InputStreamReader(fs, "UTF-8");
        BufferedReader in = new BufferedReader(reader);
        String output = "";
        String str;
        while ((str = in.readLine()) != null) {
            output += str + "\n";
        }
        in.close();
        reader.close();
        fs.close();
        return output;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void deleteTempFile() {
        if (deleteTemp) {
            File f1 = new File(INTEMP);
            File f2 = new File(OUTTEMP);
            f1.delete();
            f2.delete();

        }
    }

    public boolean isDeleteTemp() {
        return deleteTemp;
    }

    public void setDeleteTemp(boolean deleteTemp) {
        this.deleteTemp = deleteTemp;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

}
