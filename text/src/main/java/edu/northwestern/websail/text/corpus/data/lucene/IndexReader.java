package edu.northwestern.websail.text.corpus.data.lucene;

import edu.northwestern.websail.text.corpus.data.Reader;
import edu.northwestern.websail.text.corpus.model.term.TermStat;
import org.apache.lucene.index.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

import java.io.File;
import java.io.IOException;


public class IndexReader extends Reader {

    private DirectoryReader currentReader;
    private int currentTotalDocs;
    private TermsEnum currentIterator;
    private BytesRef currentBytesRef;

    public IndexReader() {
    }

    public void initialize(String indexDirectory, String language) throws IOException {
        String currentDirectory = indexDirectory + "/" + language;
        this.initializeReader(currentDirectory);
        Fields fields = MultiFields.getFields(currentReader);
        Terms terms = fields.terms("article");
        this.currentIterator = terms.iterator(null);
        this.currentTotalDocs = currentReader.numDocs();
    }

    public boolean nextTerm() throws IOException {
        currentBytesRef = currentIterator.next();
        return currentBytesRef != null;
    }

    public TermStat getCurrentTermStat() throws IOException {
        String term = new String(currentBytesRef.bytes, currentBytesRef.offset, currentBytesRef.length);
        int termFreq = (int) currentIterator.totalTermFreq();
        TermStat tStat = new TermStat(term, termFreq);
        DocsEnum docs = currentIterator.docs(null, null);
        while (docs.nextDoc() != DocsEnum.NO_MORE_DOCS) {
            tStat.updateDF((long) docs.freq());
        }
        return tStat;
    }

    public void close() throws IOException {
        this.currentReader.close();
    }

    private void initializeReader(String indexDirectory) throws IOException {
        File indexFile = new File(indexDirectory);
        Directory directory = FSDirectory.open(indexFile);
        currentReader = DirectoryReader.open(directory);
    }

    public TermsEnum getCurrentIterator() {
        return currentIterator;
    }

    public int getCurrentTotalDocs() {
        return currentTotalDocs;
    }


}
