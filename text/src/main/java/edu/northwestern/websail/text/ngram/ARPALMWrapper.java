package edu.northwestern.websail.text.ngram;

import edu.berkeley.nlp.lm.ArrayEncodedProbBackoffLm;
import edu.berkeley.nlp.lm.WordIndexer;
import edu.berkeley.nlp.lm.collections.Counter;
import edu.berkeley.nlp.lm.io.IOUtils;
import edu.berkeley.nlp.lm.io.LmReaders;
import edu.berkeley.nlp.lm.map.NgramMap;
import edu.berkeley.nlp.lm.util.StrUtils;
import edu.berkeley.nlp.lm.values.ProbBackoffPair;
import edu.northwestern.websail.core.util.TimerUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * @author NorThanapon
 */

public class ARPALMWrapper {
    private final ArrayEncodedProbBackoffLm<String> lm;
    private final WordIndexer<String> wordIndexer;
    private final int maxOrder;
    private final NgramMap<ProbBackoffPair> map;
    private final int unkIndex;
    private final int startIndex;
    private final int endIndex;
    public boolean isDebug = false;
    private int OOVs = 0;

    @SuppressWarnings("unchecked")
    public ARPALMWrapper(String arpaFile, boolean compressed, boolean binary) {
        if (binary) {
            this.lm = (ArrayEncodedProbBackoffLm<String>) IOUtils.readObjFileHard(arpaFile);
        } else {
            this.lm = LmReaders.readArrayEncodedLmFromArpa(arpaFile, compressed);
        }
        this.wordIndexer = this.lm.getWordIndexer();
        this.maxOrder = this.lm.getLmOrder();
        this.map = this.lm.getNgramMap();
        this.unkIndex = this.wordIndexer.getIndexPossiblyUnk(this.wordIndexer.getUnkSymbol());
        this.startIndex = this.wordIndexer.getIndexPossiblyUnk(this.wordIndexer.getStartSymbol());
        this.endIndex = this.wordIndexer.getIndexPossiblyUnk(this.wordIndexer.getEndSymbol());
        this.lm.setOovWordLogProb(-50.0f);
    }

    public ARPALMWrapper(String arpaFile, boolean compressed, WordIndexer<String> indexer) {
        this.lm = LmReaders.readArrayEncodedLmFromArpa(arpaFile, compressed, indexer);
        this.wordIndexer = this.lm.getWordIndexer();
        this.maxOrder = this.lm.getLmOrder();
        this.map = this.lm.getNgramMap();
        this.unkIndex = this.wordIndexer.getIndexPossiblyUnk(this.wordIndexer.getUnkSymbol());
        this.startIndex = this.wordIndexer.getIndexPossiblyUnk(this.wordIndexer.getStartSymbol());
        this.endIndex = this.wordIndexer.getIndexPossiblyUnk(this.wordIndexer.getEndSymbol());
        this.lm.setOovWordLogProb(-50.0f);
    }

    public static void main(String[] args) throws IOException {
        TimerUtil util = new TimerUtil();
        String file = "../wikifier_data/ngram/en/en.barpa.lm";
        if (args.length >= 1) file = args[0];
        System.out.println("load model...");
        util.start();
        ARPALMWrapper w = new ARPALMWrapper(file, false, true);
        w.isDebug = true;
//
//		String file = "../wikifier_data/ngram/en/en.arpa.lm";
//		if(args.length >= 1) file = args[0];
//		System.out.println("load model from "+ file +" ...");
//		util.start();
//		ARPALMWrapper w = new ARPALMWrapper(file, false, new HashWordIndexer());
//		w.isDebug = true;

        util.end();
        System.out.println(util.totalTime());
        System.out.println("Ready");
//		util.reset();
//		util.start();
//		System.out.println("Write binary file");
//		LmReaders.writeLmBinary(w.getLm(), file.replaceFirst("en.arpa.lm", "en.barpa.lm"));
//		util.end();
//		System.out.println(util.totalTime());
//		util.reset();
//		util.start();
//		LmReaders.readLmBinary(file.replaceFirst("en.arpa.lm", "en.barpa.lm"));
//		util.end();
//		System.out.println(util.totalTime());

        while (true) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter ngram...\n");
            String ipLine = br.readLine();
            System.out.println(w.getScore(ipLine.split(" ")));
            if (ipLine.equals("<exit>")) break;
        }

    }

    public double getScore(String word) {
        String[] ngram = {word};
        return this.getScore(ngram);
    }

    public float getScore(String[] ngram) {
        int[] ngramPos = this.getNgramIntegerRep(ngram);
        return this.getScore(ngramPos, 0, ngram.length);
    }

    public float getScore(String[] ngram, int startPos, int endPos) {
        int[] ngramPos = this.getNgramIntegerRep(ngram);
        return this.getScore(ngramPos, startPos, endPos);
    }

    public float getScore(int[] ngramPos, int startPos, int endPos) {
        float score = 0.0f;
        if (endPos - startPos == 1) {
            if (ngramPos[startPos] == this.startIndex ||
                    //ngramPos[startPos] == this.endIndex||
                    ngramPos[startPos] == this.unkIndex)
                return Float.NEGATIVE_INFINITY;
            else
                return this.lm.getLogProb(ngramPos, startPos, endPos);
        }
        //ending chain
        for (int i = startPos + 1; i < startPos + maxOrder && i <= endPos; i++) {
            if (i == startPos + 1 && (ngramPos[startPos] == this.startIndex || ngramPos[startPos] == this.endIndex))
                continue;
            final float scoreNgram = lm.getLogProb(ngramPos, startPos, i);
            if (isDebug) System.out.println(this.getProbString(ngramPos, startPos, i, scoreNgram));
            score += scoreNgram;
        }
        for (int i = startPos + maxOrder; i < endPos + 1; i++) {
            final float scoreNgram = lm.getLogProb(ngramPos, i - maxOrder, i);
            if (isDebug) System.out.println(this.getProbString(ngramPos, i - maxOrder, i, scoreNgram));
            score += scoreNgram;
        }
        return score;
    }

    private String getProbString(int[] ngramPos, int startPos, int endPos, float logProb) {
        float prob = (float) Math.pow(10, logProb);
        StringBuilder sb = new StringBuilder();
        sb.append("P( ");
        sb.append(this.wordIndexer.getWord(ngramPos[endPos - 1])).append(" ");
        if (endPos - startPos > 1) sb.append(" | ");
        for (int i = startPos; i < endPos - 1; i++) {
            sb.append(this.wordIndexer.getWord(ngramPos[i])).append(" ");
        }
        sb.append(") = ").append(prob).append(" [").append(logProb).append("]");
        return sb.toString();
    }

    public String getNgramString(int[] ngram) {
        List<String> ngramWords = WordIndexer.StaticMethods.toList(this.wordIndexer, ngram);
        return StrUtils.join(ngramWords);
    }

    public int[] getNgramIntegerRep(String[] ngram) {
        OOVs = 0;
        int[] ngramPos = WordIndexer.StaticMethods.toArray(this.wordIndexer, Arrays.asList(ngram));
        //boolean found = false;
        for (int idx : ngramPos) {
            if (idx == this.unkIndex) {
                OOVs++;
            }
        }
        return ngramPos;
    }

    public Collection<Map.Entry<String, Double>> sortedFillInTheBlank(List<String> words, boolean pageIdOnly,
                                                                      Integer[] variablePositions,
                                                                      int variableIndex, HashSet<Integer> relevantIds) {
        Counter<String> c = new Counter<String>();
        if (pageIdOnly) {
            for (Integer id : relevantIds) {
                String word = "page_id_" + id;
                words.set(variablePositions[variableIndex], word);
                c.setCount(word, Math.exp(lm.getLogProb(words) * Math.log(10)));
            }
        } else {
            for (int index = 0; index < wordIndexer.numWords(); ++index) {
                String word = wordIndexer.getWord(index);
                if (word.equals(lm.getWordIndexer().getStartSymbol())) continue;
                words.set(variablePositions[variableIndex], word);
                c.setCount(word, Math.exp(lm.getLogProb(words) * Math.log(10)));
            }
        }
        return c.getEntriesSortedByDecreasingCount();
    }

    public ArrayEncodedProbBackoffLm<String> getLm() {
        return lm;
    }

    public WordIndexer<String> getWordIndexer() {
        return wordIndexer;
    }

    public int getMaxOrder() {
        return maxOrder;
    }

    public NgramMap<ProbBackoffPair> getMap() {
        return map;
    }

    public int getOOVs() {
        return OOVs;
    }

    public void setOOVScore(float oovWordLogProb) {
        this.lm.setOovWordLogProb(oovWordLogProb);
    }

    public int getStartIndex() {
        return this.startIndex;
    }

    public int getEndIndex() {
        return this.endIndex;
    }

    public int getUnknownIndex() {
        return this.unkIndex;
    }


}
