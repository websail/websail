package edu.northwestern.websail.text.corpus.data;
/**
 * @author NorThanapon
 */

import edu.northwestern.websail.text.corpus.util.StopWordUtil;

import java.io.IOException;
import java.util.Set;

public abstract class Aggregator {
    protected int docCount;
    protected int docSkip;
    protected boolean verbose = false;
    protected boolean onlyWhiteSpace;
    protected Set<String> stopWords;

    public Aggregator() {
        this.docCount = 0;
        this.docSkip = 0;
        this.stopWords = StopWordUtil.emptyStopWords();
    }

    public Aggregator(String stopWordsFile) throws IOException {
        this();
        this.stopWords = StopWordUtil.readStopWords(stopWordsFile, true);
    }

    public abstract void initEmptyAggregator(String directory) throws IOException;

    public abstract void addPlaintextDocument(int docId, String docTitle, String content) throws IOException, InterruptedException;

    public abstract void close() throws IOException;

    public void skip() {
        this.docSkip++;
    }

    public boolean isVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public boolean isOnlyWhiteSpace() {
        return onlyWhiteSpace;
    }

    public void setOnlyWhiteSpace(boolean onlyWhiteSpace) {
        this.onlyWhiteSpace = onlyWhiteSpace;
    }

}
