/**
 * @author NorThanapon
 */
package edu.northwestern.websail.text.ngram;


import edu.northwestern.websail.core.io.InputFileManager;
import edu.northwestern.websail.ds.tuple.StringLongPair;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.tokenizer.SentenceSpliter;
import edu.northwestern.websail.text.tokenizer.Tokenizer;
import edu.northwestern.websail.text.util.TokenUtil;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.zip.GZIPOutputStream;

public class NGramUtils {

    public static ArrayList<ArrayList<String>> getNGramNeighbors(String text,
                                                                 ArrayList<TokenSpan> sentences, int startTarget, int endTarget,
                                                                 int left, int right, Tokenizer tokenizer) throws IOException {
        ArrayList<String> leftNeighbors = new ArrayList<String>();
        ArrayList<String> rightNeighbors = new ArrayList<String>();
        ArrayList<ArrayList<String>> neighbors = new ArrayList<ArrayList<String>>();
        ArrayList<TokenSpan> targetSpans = getSentenceSpans(sentences,
                startTarget, endTarget);
        if (targetSpans == null) {
            leftNeighbors.add("<s>");
            rightNeighbors.add("</s>");
            neighbors.add(leftNeighbors);
            neighbors.add(rightNeighbors);
            return neighbors;
        }
        String sentence = spansToString(targetSpans, text);
        // System.out.println(sentence);
        Token targetSentence = getTargetLine(sentence, startTarget, endTarget,
                targetSpans.get(0).getStartOffset());
        sentence = targetSentence.getText();
        // System.out.println(sentence);
        tokenizer.initialize(sentence);
        List<Token> tokens = tokenizer.getAllTokens();
        TokenUtil.adjustOffset(tokens, targetSpans.get(0)
                .getStartOffset() + targetSentence.getStartOffset());
        // System.out.println(tokens);
        int[] startToEnd = findTarget(tokens, startTarget, endTarget);
        // System.out.println(startToEnd[0] + ", " + startToEnd[1]);
        int leftStartIndex = startToEnd[0] - left;
        int leftEndIndex = startToEnd[0];
        int rightStartIndex = startToEnd[1] + 1;
        int rightEndIndex = startToEnd[1] + right + 1;

        List<Token> leftTokens = new ArrayList<Token>();
        List<Token> rightTokens = new ArrayList<Token>();
        if (leftEndIndex >= 0)
            leftTokens = getTokens(tokens, leftStartIndex, leftEndIndex);
        if (rightStartIndex >= 0)
            rightTokens = getTokens(tokens, rightStartIndex, rightEndIndex);

        if (leftTokens.size() < left)
            leftNeighbors.add("<s>");
        leftNeighbors.addAll(Arrays.asList(TokenUtil
                .tokens2StrArr(leftTokens)));
        rightNeighbors.addAll(Arrays.asList(TokenUtil
                .tokens2StrArr(rightTokens)));
        if (rightTokens.size() < right)
            rightNeighbors.add("</s>");
        neighbors.add(leftNeighbors);
        neighbors.add(rightNeighbors);
        // System.out.println(neighbors);
        return neighbors;

    }

    private static Token getTargetLine(String sentence, int startTarget,
                                       int endTarget, int offset) {
        String before = "";
        if (startTarget - offset > 0)
            before = sentence.substring(0, startTarget - offset);
        String after = "";
        if (endTarget - offset < sentence.length())
            after = sentence.substring(endTarget - offset, sentence.length());
        int startLine = before.lastIndexOf('\n') + 1;
        int endLine = after.indexOf('\n');
        if (endLine == -1)
            endLine = sentence.length();
        else
            endLine = endLine + before.length() + endTarget - startTarget;
        // System.out.println(startLine + "," + endLine);
        return new Token(sentence.substring(startLine, endLine), startLine,
                endLine, 0);
    }

    private static int[] findTarget(List<Token> tokens, int startTarget,
                                    int endTarget) {
        int[] startToEnd = new int[2];
        startToEnd[0] = -100;
        startToEnd[1] = -100;
        for (int i = 0; i < tokens.size(); i++) {
            Token t = tokens.get(i);
            if (t.getStartOffset() >= startTarget && startToEnd[0] == -100)
                startToEnd[0] = i;
            if (t.getEndOffset() >= endTarget && startToEnd[1] == -100)
                startToEnd[1] = i;
        }
        return startToEnd;
    }

    private static List<Token> getTokens(List<Token> tokens, int start, int end) {
        if (start < 0)
            start = 0;
        if (end > tokens.size())
            end = tokens.size();
        ArrayList<Token> t = new ArrayList<Token>();
        for (int i = start; i < end; i++) {
            t.add(tokens.get(i));
        }
        return t;
    }

    private static String spansToString(ArrayList<TokenSpan> spans, String text) {
        return text.substring(spans.get(0).getStartOffset(),
                spans.get(spans.size() - 1).getEndOffset());
    }

    private static ArrayList<TokenSpan> getSentenceSpans(
            ArrayList<TokenSpan> sentences, int startTarget, int endTarget) {
        ArrayList<TokenSpan> sentenceSpans = null;
        for (TokenSpan span : sentences) {
            if (span.getStartOffset() <= startTarget
                    && span.getEndOffset() >= endTarget) {
                sentenceSpans = new ArrayList<TokenSpan>();
                sentenceSpans.add(span);
                break;
            } else if (span.getStartOffset() <= startTarget
                    && span.getEndOffset() <= endTarget) {
                // target is longer than a sentence --> start sentence
                sentenceSpans = new ArrayList<TokenSpan>();
                sentenceSpans.add(span);
            } else if (span.getStartOffset() >= startTarget
                    && span.getEndOffset() >= endTarget) {
                // target is longer than a sentence --> end sentence
                if (sentenceSpans == null)
                    return null;
                sentenceSpans.add(span);
                break;
            } else if (span.getStartOffset() >= startTarget
                    && span.getEndOffset() <= endTarget
                    && sentenceSpans != null) {
                // target is longer than a sentence --> in between
                sentenceSpans.add(span);
            }
        }
        return sentenceSpans;
    }

    public static String formatText(String text,
                                    SentenceSpliter sentenceSpliter, boolean lowercase, boolean stem) {
        StringBuilder sb = new StringBuilder();
        String paragraphs[] = text.split("\n");
        for (String paragraph : paragraphs) {
            List<TokenSpan> sentences = sentenceSpliter
                    .sentenceSplit(paragraph);
            for (TokenSpan sentence : sentences) {
                List<? extends Token> tokens = sentence.getTokens();
                tokens = TokenUtil.normalize(tokens, lowercase, stem);
                String cleanText = TokenUtil.tokens2Str(
                        tokens).trim();
                if (cleanText.equals(""))
                    continue;
                sb.append(cleanText);
                sb.append('\n');
            }
        }
        return sb.toString();
    }

    public static void makeGoogleDirectory(String[] ngramCountFiles, long[] outTopCounts, String[] outTopNgrams) throws IOException {

        ArrayList<StringLongPair> pairs = new ArrayList<StringLongPair>();
        for (int i = 0; i < ngramCountFiles.length; i++) {
            InputFileManager inputFileManager = new InputFileManager(ngramCountFiles[i]);
            BufferedWriter bufferedWriter = new BufferedWriter(
                    new OutputStreamWriter(
                            new GZIPOutputStream(new FileOutputStream(ngramCountFiles[i] + ".gz"))
                    ));
            String line;
            while ((line = inputFileManager.readLine()) != null) {
                if (line.equals("")) continue;
                String[] parts = line.split("\t");
                String word = parts[0];
                long count = Long.parseLong(parts[1]);

                if (count > outTopCounts[i] && !word.contains("<s>") && !word.contains("</s>")) {
                    outTopCounts[i] = count;
                    outTopNgrams[i] = word;
                }
                if (i == 0) {
                    pairs.add(new StringLongPair(word, count));
                } else {
                    bufferedWriter.write(word + "\t" + count + "\n");
                }
            }
            if (i == 0) {
                Collections.sort(pairs, StringLongPair.comparator(true));
            }
            for (StringLongPair slp : pairs) {
                bufferedWriter.write(slp.getStringElement() + "\t" + slp.getLongElement() + "\n");
            }
            inputFileManager.close();
            bufferedWriter.close();
        }
    }
}
