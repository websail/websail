package edu.northwestern.websail.text.corpus.data.lucene.collector;

import edu.northwestern.websail.text.corpus.data.lucene.IndexAggregator;
import org.apache.lucene.index.AtomicReaderContext;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.util.HashMap;

/**
 * @author NorThanapon
 * @since 11/7/14
 */
public class ByteTermFreqCollector extends Collector {
    private final DirectoryReader reader;

    private int totalDocs = 0;
    private int base = 0;
    private HashMap<Integer, Long> tf = new HashMap<Integer, Long>();
    private long totalCount = 0;

    public ByteTermFreqCollector(DirectoryReader reader) {
        this.reader = reader;
    }

    @Override
    public boolean acceptsDocsOutOfOrder() {
        return true;
    }

    @Override
    public void collect(int docId) throws IOException {
        totalDocs++;
        Terms vector = this.reader.getTermVector(docId + base, IndexAggregator.DOC_CONTENT_FIELD);
        if (vector == null) return;
        TermsEnum termsEnum = vector.iterator(null);
        BytesRef text;
        while ((text = termsEnum.next()) != null) {
            long freq = termsEnum.totalTermFreq();
            totalCount += freq;
            if (this.tf.containsKey(text.hashCode())) freq += this.tf.get(text.hashCode());
            tf.put(text.hashCode(), freq);
        }
    }

    @Override
    public void setNextReader(AtomicReaderContext context) throws IOException {
        this.base = context.docBase;
    }

    @Override
    public void setScorer(Scorer score) throws IOException {

    }

    public int getTotalDocs() {
        return totalDocs;
    }

    public void setTotalDocs(int totalDocs) {
        this.totalDocs = totalDocs;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public HashMap<Integer, Long> getTf() {
        return tf;
    }

    public void setTf(HashMap<Integer, Long> tf) {
        this.tf = tf;
    }


}
