package edu.northwestern.websail.text.corpus.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author NorThanapon
 * @since 11/7/14
 */
public class StopWordUtil {
    public static Set<String> emptyStopWords() {
        return new HashSet<String>();
    }

    public static Set<String> readStopWords(String stopwordFile, boolean lowerCase) throws IOException {
        HashSet<String> set = new HashSet<String>();
        BufferedReader in = new BufferedReader(new FileReader(stopwordFile));
        String line;
        while ((line = in.readLine()) != null) {
            set.add(lowerCase ? line.toLowerCase() : line);
        }
        in.close();
        return set;
    }

    public static void addCustomStopWords(Set<String> customStopWords) {
        customStopWords.add("-lrb-");
        customStopWords.add("-rrb-");
        customStopWords.add(".");
        customStopWords.add("-lcb-");
        customStopWords.add("-rcb-");
        customStopWords.add("-lsb-");
        customStopWords.add("-rsb-");
        customStopWords.add("-1");
        customStopWords.add("");
        customStopWords.add(" ");
    }
}
