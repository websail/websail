package edu.northwestern.websail.text.main;

import edu.berkeley.nlp.lm.StupidBackoffLm;
import edu.berkeley.nlp.lm.WordIndexer;
import edu.berkeley.nlp.lm.io.LmReaders;
import edu.berkeley.nlp.lm.values.CountValueContainer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/**
 * @author NorThanapon
 * @since 2/24/15
 */
public class TestGoogleLM {
    public static void main(String[] args) throws IOException {
        String vocabFile = args[0];
        String blmFile = args[1];
        System.out.println("blm: " + blmFile);
        System.out.println("vocab: " + vocabFile);
        StupidBackoffLm<String> googleLM = LmReaders.readGoogleLmBinary(blmFile, vocabFile);


        System.out.println("LM loaded, order " + googleLM.getLmOrder());
        System.out.println("Unigram Sum: " + ((CountValueContainer) googleLM.getNgramMap().getValues()).getUnigramSum());

        while (true) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter ngram...\n");
            String testText = br.readLine();
            List<String> words = Arrays.asList(testText.trim().split("\\s+"));
            int[] ngrams = new int[words.size()];
            WordIndexer<String> wIdx = googleLM.getWordIndexer();
            for (int i = 0; i < words.size(); i++) {
                ngrams[i] = wIdx.getIndexPossiblyUnk(words.get(i));
            }
            System.out.println("Sentence score: " + googleLM.scoreSentence(words));
            System.out.println("Log Probability: " + googleLM.getLogProb(words));
            long count = googleLM.getRawCount(ngrams, 0, ngrams.length);
            if (count == -1L)
                System.out.println("Count: " + 0);
            else
                System.out.println("Count: " + count);
            System.out.println();
            if (testText.equals("<exit>")) break;
        }
    }
}

