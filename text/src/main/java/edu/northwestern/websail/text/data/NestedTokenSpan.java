package edu.northwestern.websail.text.data;

/**
 * @author NorThanapon
 */

import java.util.List;

public class NestedTokenSpan extends TokenSpan {

    private String pos;
    private String ne;
    private NestedTokenSpan parent;
    private List<NestedTokenSpan> children;

    public NestedTokenSpan(int startOffset, int endOffset, List<Token> tokens,
                           int index, String pos) {
        super(startOffset, endOffset, tokens, index);
        this.pos = pos;
    }

    public String pos() {
        return pos;
    }

    public NestedTokenSpan pos(String pos) {
        this.pos = pos;
        return this;
    }

    public String ne() {
        return ne;
    }

    public NestedTokenSpan ne(String ne) {
        this.ne = ne;
        return this;
    }


    public NestedTokenSpan parent() {
        return parent;
    }

    public NestedTokenSpan parent(NestedTokenSpan parent) {
        this.parent = parent;
        return this;
    }

    public List<NestedTokenSpan> children() {
        return children;
    }

    public NestedTokenSpan children(List<NestedTokenSpan> children) {
        this.children = children;
        return this;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        return toStringBuilder(sb).toString();
    }

    public StringBuilder toStringBuilder(StringBuilder sb) {

        sb.append('(');
        sb.append(pos);

        if (children != null) {
            for (NestedTokenSpan child : children) {
                sb.append(' ');
                child.toStringBuilder(sb);
            }
        } else {
            sb.append(" [");
            sb.append(this.getStartOffset());
            sb.append(':');
            sb.append(this.getEndOffset());
            sb.append("]/");
            sb.append(ne);
        }
        return sb.append(')');

    }
}
