package edu.northwestern.websail.text.main;

import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.tokenizer.StanfordNLPSSplit;
import edu.northwestern.websail.text.util.TokenUtil;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author NorThanapon
 * @since 1/10/15
 */
public class StanfordExamples {
    private static final String NOUN_PATTERN = "(N[NPS]{1,5})|VBG";
    private static final String ADJECTIVE_PATTERN = "J[JRS]{1,2}";
    private static final String NUMERIC_PATTERN = "CD";

    private static final String TECH_TERM_SIMPLE_PATTERN = "(?:(?:" + ADJECTIVE_PATTERN +
            "|" + NOUN_PATTERN + ")\\s*)+ (?:" + NOUN_PATTERN + "|" + NUMERIC_PATTERN + ")";
    public static final Pattern simple = Pattern.compile(TECH_TERM_SIMPLE_PATTERN);
    private static final String TECH_TERM_COMPLEX_PATTERN = "(?:" + ADJECTIVE_PATTERN + "|" + NOUN_PATTERN +
            "\\s*)*" + NOUN_PATTERN + "\\s+" +
            "IN (?:" + TECH_TERM_SIMPLE_PATTERN + "|" + NUMERIC_PATTERN + ")*";
    public static final Pattern complex = Pattern.compile(TECH_TERM_COMPLEX_PATTERN);

    public static void main(String[] args) throws Exception {
        StanfordNLPSSplit nlpsSplit = new StanfordNLPSSplit(true);
        String text = "Hello-word (AC).";
        List<? extends Token> tokens = nlpsSplit.sentenceSplit(text).get(0).getTokens();
        System.out.println(tokens);
        String tokenPOSString = TokenUtil.token2POSStr(tokens);
        System.out.println(tokenPOSString);
        Matcher simpleMatcher = simple.matcher(tokenPOSString);
        System.out.println(simple.toString());
        while (simpleMatcher.find()) {
            int spacesBeforeStart = numChars(tokenPOSString, ' ', 0, simpleMatcher.start());
            int spacesBeforeEnd = numChars(tokenPOSString, ' ', 0, simpleMatcher.end());
            System.out.println(tokenPOSString.substring(simpleMatcher.start(), simpleMatcher.end()));
            int startOffset = tokens.get(spacesBeforeStart).getStartOffset();
            int endOffset = tokens.get(spacesBeforeEnd).getEndOffset();
            String surfaceForm = text.substring(startOffset, endOffset);
            System.out.println(surfaceForm);
        }
    }

    public static int numChars(String str, char ch, int start, int end) {
        int count = 0;
        for (int i = start; i < end; i++) {
            if (ch == str.charAt(i)) count++;
        }
        return count;
    }

}
