package edu.northwestern.websail.text.tokenizer;

/**
 * @author NorThanapon
 */

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.text.data.TaggedToken;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.util.StanfordNLPProperties;
import edu.northwestern.websail.text.util.TokenUtil;
import edu.stanford.nlp.ling.CoreAnnotations.*;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import java.util.List;
import java.util.Properties;


public class StanfordNLPSSplit extends SentenceSpliter {
    private final StanfordCoreNLP pipeline;

    public StanfordNLPSSplit() {
        this(false);
    }

    public StanfordNLPSSplit(Boolean pos) {
        super(pos);
        Properties props = StanfordNLPProperties.basicConfigure();
        if (pos) {
            props.put("annotators", "tokenize, ssplit, pos");
        } else
            props.put("annotators", "tokenize, ssplit");
        pipeline = new StanfordCoreNLP(props);
    }

    public static void main(String[] args) throws Exception {
        String text = "<Menace ID= \" Menace . 1 0 5 0 3 8 2 0 5 2 5 3 5 \" l a b e l = \" 01−FIRE \" s e l e c t e d = \" \" d e s c r i p t i o n = \" Type : N a t u r a l /Human/ Env i ronm ental A c c i d e n t a l c a u s e : C o n c e n t r a t i o n o f fl am m abl e o r e x p l o s i v e . . . \" j u s t i f i c a t i o n = \" \" d e s c r i p t i o n M e n a c e E l e m e n t = \" \" p o t e n t i e l = \" \" > Table 1. EBIOS entities and attributes and their corresponding security ontology concepts and relations\nSome(EBIOS XML-elements and at- tributes Security ontology concepts and relations)\n<EntityType> ent: Asset type subclasses of ent:Asset description ent:description of abstract instances of subclasses of ent:Asset <Vulnerability> sec:Vulnerability label subclasses of sec:Vulnerability menace sec:exploitedBy of sec:Vulnerability <EntityTypeList> sec:threatens of sec:Threat <Menace> sec:Threat label subclasses of sec:Threat description sec:description of abstract instances of subclasses of sec:Threat <SeverityScale> sec:affects of sec:Threat <Criteria> sec:SecurityAttribute label instances of sec:SecurityAttribute description sec:description of abstract instances of subclasses of sec:SecurityAttribute <MenaceCauseList> sec:hasSource of sec:Threat <MenaceCause> sec:ThreatSource label subclasses of sec:ThreatSource description sec:description of abstract instances of subclasses of sec:ThreatSource <MenaceOrigineList> sec:hasOrigin of sec:Threat <MenaceOrigine> sec:ThreatOrigin label subclasses of sec:ThreatOrigin description sec:description of abstract instances of subclasses of sec:ThreatOrigin <SecurityObjective> sec:Control label subclasses of sec:Control content sec:description of abstract instances of subclasses of sec:Control <SecurityObjectiveCovers> sec:mitigatedBy of sec:Vulnerability <FunctionnalRequirement> iso:Control abbreviation iso:controlTitle of abstract instances of iso: Control description iso:controlDescription of abstract instances of iso:Control <Objective> sec:correspondsTo of iso:Control <S e v e r i t y S c a l e ID= \" S e v e r i t y S c a l e . 1050973114465 \" > <M e n a c e S e v e r i t y ID= \" M e n a c e S e v e r i t y . 1 1 0 9 4 3 6 1 7 4 0 4 4 \" c r i t e r i a = \" C r i t e r i a . 1 0 1 3 3 0 7 7 4 1 6 4 1 \" s e v e r i t y = \" \" v i o l a t i o n = \" t r u e \" /> <M e n a c e S e v e r i t y ID= \" M e n a c e S e v e r i t y . 1 1 0 9 1 0 8 5 9 7 3 2 0 \" c r i t e r i a = \" C r i t e r i a . 1 0 1 1 6 8 0 6 4 8 0 3 7 \" s e v e r i t y = \" \" v i o l a t i o n = \" t r u e \" /> </ S e v e r i t y S c a l e > <MenaceC aus eLi s t ID= \" MenaceC aus eLi s t . <M e n a c e O r i g i n e L i s t ID= \" M e n a c e O r i g i n e L i s t . 1050973114465 \" / > <MenaceO rigine i d= \" The menaces' attribute Label and Description correspond to the threat sub-concepts and their descriptions.";
        StanfordNLPSSplit spliter = new StanfordNLPSSplit(true);
        for (TokenSpan token : spliter.sentenceSplit(text)) {
            System.out.println(TokenUtil.tokens2Str(token.getTokens(), text));
        }
    }

    public static List<TokenSpan> extractSetences(List<CoreMap> sentences) {
        FastList<TokenSpan> outSentences = new FastList<TokenSpan>();
        int iToken = 0;
        int iSentence = 0;
        for (CoreMap sentence : sentences) {
            TokenSpan span = extractSentence(sentence, iToken, iSentence);
            if(span!=null) {
                outSentences.add(span);
                iSentence++;
                iToken += span.getTokens().size();
            }
        }
        return outSentences;
    }

    public static TokenSpan extractSentence(CoreMap sentence, int iToken, int iSentence){
        int localIndex = 0;
        FastList<Token> tokens = new FastList<Token>();
        for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
            String word = token.get(TextAnnotation.class);
            int startOffset = token.get(CharacterOffsetBeginAnnotation.class);
            int endOffset = token.get(CharacterOffsetEndAnnotation.class);
            String pos = token.get(PartOfSpeechAnnotation.class);
            String ne = token.get(NamedEntityTagAnnotation.class);
            TaggedToken t = new TaggedToken(word, startOffset, endOffset, iToken++, pos, ne);
            tokens.add(t);
            t.setLocalIndex(localIndex++);
        }
        if (tokens.size() < 1) return null;
        return new TokenSpan(tokens.get(0).getStartOffset(),
                tokens.get(tokens.size() - 1).getEndOffset(),
                tokens, iSentence);
    }

    public List<TokenSpan> sentenceSplit(String text) {
        if (text == null) return new FastList<TokenSpan>();
        Annotation document = new Annotation(text);
        pipeline.annotate(document);
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        return extractSetences(sentences);
    }

    public boolean posEnabled() {
        return this.posEnabled;
    }
}
