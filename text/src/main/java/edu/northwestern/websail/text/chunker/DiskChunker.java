package edu.northwestern.websail.text.chunker;

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.text.data.NestedTokenSpan;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.data.TokenSpan;

import java.io.IOException;


public class DiskChunker extends Chunker {

    private final DependencyReader reader;
    private FastList<NestedTokenSpan> currentSentences;
    private int targetStartOffset;

    public DiskChunker(DependencyReader reader) {
        super();
        this.reader = reader;
        this.targetStartOffset = 0;
    }

    @Override
    public void initialize(String text, String source) {
        String prevS = this.currentSource;
        super.initialize(text, source);
        if (prevS == null || !prevS.equals(this.currentSource)
                || currentSentences == null) {
            try {
                currentSentences = reader.loadArticle(Integer.parseInt(source));
            } catch (NumberFormatException e) {
                logger.severe(source + " is not an integer.");
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // System.out.println(this.currentSource+"/n"+this.currentSentences);
    }

    @Override
    public void initialize(FastList<TokenSpan> sentences, String source) {
        super.initialize(sentences, source);
        throw new UnsupportedOperationException("Unsupported function. Use DiskChunker.initialize(String, String).");
    }

    @Override
    public void extractNounPhrase(NounPhraseFoundDelegate delegate)
            throws Exception {
        int start = this.getTargetStartOffset();
        int end = this.getTargetStartOffset() + this.currentText.length();
        // System.out.println(start + ", " + end);
        for (NestedTokenSpan sentence : currentSentences) {
            if ((sentence.getStartOffset() < start && sentence.getEndOffset() <= start)
                    || (sentence.getStartOffset() >= end && sentence
                    .getEndOffset() >= end)) { // not overlap
                continue;
            }
            this.addTextToToken(sentence);
            this.extractNounPhrase(sentence, true, delegate);
        }
    }

    private void extractNounPhrase(NestedTokenSpan curNode, boolean topNP,
                                   NounPhraseFoundDelegate delegate) throws Exception {
        if (curNode.pos().equals("NP")) {
            int start = this.getTargetStartOffset();
            int end = this.getTargetStartOffset() + this.currentText.length();
            if (curNode.getStartOffset() >= start
                    && curNode.getEndOffset() <= end) {
                TokenSpan np = Chunker.cleanNounPhrase(curNode);
                if (np != null) {
                    if (topNP)
                        delegate.found(np);
                    else
                        delegate.foundSub(curNode.parent(), np);
                    //END
                }
                topNP = false;
            }
        }
        if (curNode.children() != null) {
            for (NestedTokenSpan child : curNode.children()) {
                this.extractNounPhrase(child, topNP, delegate);
            }
        }
    }

    private void addTextToToken(NestedTokenSpan curNode) {
        int start = this.getTargetStartOffset();
        int end = this.getTargetStartOffset() + this.currentText.length();
        for (Token t : curNode.getTokens()) {
            if (t.getStartOffset() >= start && t.getEndOffset() <= end) {
                t.setText(this.currentText.substring(
                        t.getStartOffset() - start, t.getEndOffset() - start));
            }
        }
    }

    public int getTargetStartOffset() {
        return targetStartOffset;
    }

    public void setTargetStartOffset(int targetStartOffset) {
        this.targetStartOffset = targetStartOffset;
    }
}
