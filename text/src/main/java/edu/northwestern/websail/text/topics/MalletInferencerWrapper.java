package edu.northwestern.websail.text.topics;

import cc.mallet.pipe.Pipe;
import cc.mallet.topics.TopicInferencer;
import com.gs.collections.impl.list.mutable.FastList;

/**
 * @author NorThanapon
 * @since 7/20/15
 */
public class MalletInferencerWrapper {
    private TopicInferencer inferencer;
    private FastList<Pipe> pipes;

    public MalletInferencerWrapper(TopicInferencer inferencer, FastList<Pipe> pipes) {
        this.inferencer = inferencer;
        this.pipes = pipes;
    }

    public TopicInferencer getInferencer() {
        return inferencer;
    }

    public FastList<Pipe> getPipes() {
        return pipes;
    }
}
