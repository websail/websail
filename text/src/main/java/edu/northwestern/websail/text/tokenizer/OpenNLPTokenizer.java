package edu.northwestern.websail.text.tokenizer;

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.util.OpenNLPModels;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.util.Span;

import java.io.IOException;
import java.util.List;

/**
 * @author NorThanapon
 * @since 11/10/14
 */
public class OpenNLPTokenizer extends Tokenizer {

    TokenizerME tokenizer;
    FastList<Token> tokens;
    private int currentIndex;

    public OpenNLPTokenizer() throws IOException {
        super();
        tokenizer = new TokenizerME(OpenNLPModels.getTokenizerModel());
        currentIndex = -1;
    }

    @Override
    public List<Token> getAllTokens() {
        return this.tokens;
    }

    @Override
    public Token next() {
        if ((++currentIndex) < tokens.size()) {
            return this.tokens.get(currentIndex);
        }
        return null;
    }

    @Override
    public Token getCurrentToken() {
        if (currentIndex < tokens.size()) {
            return this.tokens.get(currentIndex);
        }
        return null;
    }

    @Override
    public void clear() {
        this.tokens = null;
        currentIndex = -1;
    }

    public void initialize(String text) {
        super.initialize(text);
        this.clear();
        Span[] spans = tokenizer.tokenizePos(text);
        tokens = new FastList<Token>(spans.length);
        int i = 0;
        for (Span span : spans) {
            String tokenText = span.getCoveredText(text).toString();
            if (toLower) {
                tokenText = tokenText.toLowerCase();
            }
            if (stopwords.contains(tokenText.toLowerCase())) continue;
            Token t = new Token(tokenText, span.getStart(), span.getEnd(), i);
            i++;
            tokens.add(t);
        }
        currentIndex = 0;
    }
}
