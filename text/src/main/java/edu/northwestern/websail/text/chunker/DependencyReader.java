package edu.northwestern.websail.text.chunker;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.core.io.InputFileManager;
import edu.northwestern.websail.ds.tuple.IntegerPair;
import edu.northwestern.websail.text.data.NestedTokenSpan;
import edu.northwestern.websail.text.data.TaggedToken;
import edu.northwestern.websail.text.data.Token;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Logger;


public class DependencyReader implements Closeable {
    public static final Logger logger = Logger.getLogger(DependencyReader.class
            .getName());

    private static final int INIT_SIZE = 4000000;
    private final HashMap<Integer, Long> posMap;
    private final RAFDescriptor[] rafPool;
    private final Random rand = new Random();

    public DependencyReader(int poolSize, String rafFile, String posFile)
            throws IOException {
        this.posMap = loadPosition(posFile);
        this.rafPool = initializeRAFPool(poolSize, rafFile);

    }

    public static FastList<NestedTokenSpan> readLine(String line) {
        FastList<NestedTokenSpan> sentences = new FastList<NestedTokenSpan>();
        FastList<IntegerPair> sentenceOffsets = splitSentences(line,
                new IntegerPair(0, line.length()));
        for (IntegerPair offset : sentenceOffsets) {
            sentences.add(parseSpan(line, offset));
        }
        return sentences;
    }

    private static NestedTokenSpan parseSpan(String line, IntegerPair offset) {
        if (isLeaf(line, offset))
            return null;
        int level = 0;
        int startTag = 0;
        int startNested = 0;
        int endNested = 0;
        String pos = null;
        for (int i = offset.getFirst(); i < offset.getSecond(); i++) {
            if (line.charAt(i) == '(') {
                if (level == 0) {
                    startTag = i + 1;
                } else if (level == 1) {
                    pos = line.substring(startTag, i).trim();
                    startNested = i;
                }
                level++;
            }
        }
        for (int i = offset.getSecond() - 1; i >= offset.getFirst(); i--) {
            if (line.charAt(i) == ')') {
                endNested = i;
                break;
            }
        }
        NestedTokenSpan curNode = new NestedTokenSpan(-1, -1,
                new ArrayList<Token>(), -1, pos);
        FastList<NestedTokenSpan> children = parseLine(line, new IntegerPair(
                startNested, endNested));
        curNode.children(children);
        ArrayList<Token> tokens = new ArrayList<Token>();
        for (NestedTokenSpan child : curNode.children()) {
            child.parent(curNode);
            tokens.addAll(child.getTokens());
        }
        curNode.setTokens(tokens);
        if (tokens.size() > 0) {
            curNode.setStartOffset(tokens.get(0).getStartOffset());
            curNode.setEndOffset(tokens.get(tokens.size() - 1).getEndOffset());
        }

        return curNode;
    }

    private static NestedTokenSpan parseLeaf(String line, IntegerPair offset) {
        if (!isLeaf(line, offset))
            return null;
        int startPOS = -1;
        int endPOS = -1;
        int startOffset = -1;
        int endOffset = -1;
        int startNE = -1;
        int endNE = -1;
        for (int i = offset.getFirst(); i < offset.getSecond(); i++) {
            char curCh = line.charAt(i);
            if (curCh == '(') {
                startPOS = i + 1;
            } else if (curCh == '[') {
                endPOS = i;
                startOffset = i + 1;
            } else if (curCh == ']') {
                endOffset = i;
            } else if (curCh == '/') {
                startNE = i + 1;
            } else if (curCh == ')') {
                endNE = i;
            }
        }
        String pos = cleanPOS(line.substring(startPOS, endPOS).trim());
        String offsetString = line.substring(startOffset, endOffset).trim();
        String[] parts = offsetString.split(":");
        int start = Integer.parseInt(parts[0]);
        int end = Integer.parseInt(parts[1]);
        String ne = line.substring(startNE, endNE);
        TaggedToken token = new TaggedToken(null, start, end, -1, pos, ne);
        ArrayList<Token> tokens = new ArrayList<Token>();
        tokens.add(token);
        NestedTokenSpan span = new NestedTokenSpan(start, end, tokens, -1, pos);
        span.ne(ne);
        return span;
    }

    private static FastList<NestedTokenSpan> parseLine(String line,
                                                        IntegerPair offset) {
        FastList<NestedTokenSpan> sentences = new FastList<NestedTokenSpan>();
        FastList<IntegerPair> sentenceOffsets = splitSentences(line, offset);
        for (IntegerPair nOffset : sentenceOffsets) {
            NestedTokenSpan span = parseSpan(line, nOffset);
            if (span == null) {
                span = parseLeaf(line, nOffset);
            }
            if (span != null)
                sentences.add(span);
        }
        return sentences;
    }

    private static boolean isLeaf(String line, IntegerPair offset) {
        int c = 0;
        for (int i = offset.getFirst(); i < offset.getSecond(); i++) {
            if (line.charAt(i) == '(') {
                c++;
                if (c > 1)
                    return false;
            }
        }
        return true;
    }

    private static FastList<IntegerPair> splitSentences(String line,
                                                         IntegerPair offset) {
        FastList<IntegerPair> offsets = new FastList<IntegerPair>();
        int curStartOffset = offset.getFirst();

        int level = 0;
        for (int curOffset = offset.getFirst(); curOffset < offset.getSecond(); curOffset++) {
            if (line.charAt(curOffset) == '(') {
                if (level == 0) {
                    curStartOffset = curOffset;
                }
                level++;

            } else if (line.charAt(curOffset) == ')') {
                level--;
                if (level == 0) {
                    IntegerPair p = new IntegerPair(curStartOffset,
                            curOffset + 1);
                    offsets.add(p);
                }
            }
        }

        return offsets;
    }

    private static String cleanPOS(String pos) {
        int lastDashIndex = pos.lastIndexOf('-');
        if (lastDashIndex == -1)
            return pos;
        else
            return pos.substring(0, lastDashIndex);
    }

    public static Kryo createKryo() {
        Kryo kryo = new Kryo();
        kryo.register(String.class, new ExactStringSerializer());
        return kryo;
    }

    public static void main(String[] args) {
        String depLine = "(ROOT (S (NP (NNP-1 [0:4]/PERSON)) (VP (MD-2 [5:8]/O) (VP (VB-3 [9:14]/O) (PP (TO-4 [15:17]/O) (:-5 [17:18]/O))))))(ROOT (NP (NP (NP (NNP-1 [23:28]/PERSON) (NNP-2 [29:33]/PERSON)) (PRN (-LRB--3 [35:36]/O) (NP (NP (CD-4 [36:40]/DATE)) (:-5 [40:41]/O) (NP (CD-6 [41:45]/DATE))) (-RRB--7 [45:46]/O))) (,-8 [46:47]/O) (NP (NP (JJ-9 [48:62]/O) (NN-10 [63:71]/O)) (PP (IN-11 [72:74]/O) (NP (NNP-12 [75:80]/LOCATION) (NNP-13 [81:87]/LOCATION))))))(ROOT (NP (NP (NP (NNP-1 [91:95]/PERSON)) (PRN (-LRB--2 [96:97]/O) (NP (NN-3 [97:101]/O)) (-RRB--4 [101:102]/O))) (,-5 [103:104]/O) (NP (NP (CD-6 [105:109]/DATE) (NN-7 [110:119]/O)) (PP (IN-8 [120:122]/O) (NP (NNP-9 [123:128]/PERSON) (NNP-10 [129:133]/PERSON))) (PP (IN-11 [134:136]/O) (NP (NNP-12 [137:143]/PERSON) (NNP-13 [144:149]/PERSON))))))(ROOT (SQ (``-1 [153:154]/O) (FRAG (NP (NNP-2 [154:158]/O)) (''-3 [158:159]/O) (PRN (-LRB--4 [160:161]/O) (NP (NN-5 [161:165]/O)) (-RRB--6 [165:166]/O)) (,-7 [167:168]/O) (NP (NP (CD-8 [169:173]/DATE) (NN-9 [174:178]/O)) (PP (IN-10 [179:184]/O) (NP (NP (NNP-11 [185:190]/PERSON) (NNP-12 [191:195]/PERSON)) (VP (VBN-13 [196:203]/O) (PP (IN-14 [204:206]/O) (NP (NNP-15 [207:212]/PERSON) (NNP-16 [213:220]/PERSON))))))))))(ROOT (NP (NP (NP (NNP-1 [224:228]/PERSON)) (PRN (-LRB--2 [229:230]/O) (NP (NN-3 [230:240]/O)) (-RRB--4 [240:241]/O))) (,-5 [242:243]/O) (NP (NP (DT-6 [244:245]/O) (JJ-7 [246:252]/MISC) (NN-8 [253:263]/O)) (PP (IN-9 [264:266]/O) (NP (NNP-10 [267:273]/LOCATION) (NNP-11 [274:278]/LOCATION))))))(ROOT (NP (NP (NNP-1 [281:285]/PERSON)) (PRN (-LRB--2 [286:287]/O) (NP (NN-3 [287:292]/O)) (-RRB--4 [292:293]/O))))(ROOT (NP (NP (NNP-1 [297:304]/O)) (,-2 [305:306]/O) (NP (NNP-3 [307:315]/O) (NN-4 [316:323]/O))))(ROOT (NP (NP (NNP-1 [326:330]/O)) (,-2 [331:332]/O) (NP (NP (DT-3 [333:335]/O) (JJ-4 [336:341]/O) (NN-5 [342:347]/O) (NN-6 [348:352]/O)) (PP (IN-7 [353:355]/O) (NP (NP (NN-8 [357:365]/O)) (PRN (-LRB--9 [366:367]/O) (NP (NN-10 [367:374]/O)) (-RRB--11 [374:375]/O)))))))(ROOT (S (VP (VB-1 [379:382]/O) (ADVP (RB-2 [383:387]/O)))))(ROOT (NP (NP (NNP-1 [391:395]/O)) (,-2 [396:397]/O) (NP (NN-3 [398:402]/O) (NNS-4 [403:413]/O)) (NN-5 [414:419]/O)))";
        // String depLine =
        // "(ROOT (S (NP (NNP-1 [0:4]/PERSON)) (VP (MD-2 [5:8]/O) (VP (VB-3 [9:14]/O) (PP (TO-4 [15:17]/O) (:-5 [17:18]/O))))))";
        for (NestedTokenSpan span : readLine(depLine)) {
            System.out.println(span);
        }
    }

    public FastList<NestedTokenSpan> loadArticle(int titleId)
            throws IOException {
        RAFDescriptor[] pool = rafPool;
        if (pool == null) {
            throw new NullPointerException(
                    "RAF Pool has not been initialized properly. Please check your rafFile.");
        }
        HashMap<Integer, Long> positionMap = this.posMap;
        if (positionMap == null) {
            throw new NullPointerException(
                    "Position Map has not been initialized properly. Please check your posFile.");
        }
        Long pos = positionMap.get(titleId);
        if (pos == null) {
            logger.warning(titleId + " is not in the posiiton map.");
            return null;
        }
        //System.out.println(pos);
        RAFDescriptor desc = this.getMinUsageDescriptor(pool, pos);
        return desc.read(pos);
    }

    public HashMap<Integer, Long> posMap() {
        return this.posMap;
    }

    private synchronized RAFDescriptor getMinUsageDescriptor(RAFDescriptor[] pool,
                                                             long position) {
        long min = Long.MAX_VALUE;
        int minIndex;
        ArrayList<Integer> availableIndexes = new ArrayList<Integer>();
        for (RAFDescriptor aPool : pool) {
            if (aPool.curPos == position)
                return aPool;
            if (aPool.usage <= min) {
                min = aPool.usage;
            }
        }
        for (int i = 0; i < pool.length; i++) {
            if (pool[i].usage <= min) {
                availableIndexes.add(i);
            }
        }
        minIndex = availableIndexes.get(rand.nextInt(availableIndexes.size()));
        return pool[minIndex];
    }

    private HashMap<Integer, Long> loadPosition(String filename) {
        InputFileManager inMgr = new InputFileManager(filename);
        HashMap<Integer, Long> positionMap = new HashMap<Integer, Long>(
                INIT_SIZE);
        String line;
        while ((line = inMgr.readLine()) != null) {
            if (line.trim().equals(""))
                continue;
            String[] parts = line.split("\t");
            positionMap.put(Integer.parseInt(parts[0]), Long.valueOf(parts[1]));
        }
        inMgr.close();
        return positionMap;
    }

    private RAFDescriptor[] initializeRAFPool(int poolSize, String rafFile)
            throws IOException {
        RAFDescriptor[] pool = new RAFDescriptor[poolSize];
        for (int i = 0; i < poolSize; i++) {
            pool[i] = new RAFDescriptor(rafFile);
        }
        return pool;
    }

    @Override
    public void close() throws IOException {
        if (this.rafPool != null) {
            for (RAFDescriptor raf : rafPool) {
                raf.close();
            }
        }
    }

    private static final class RAFDescriptor implements Closeable {
        final RandomAccessFile raf;
        final Kryo kryo;
        long curPos;
        long usage;
        FastList<NestedTokenSpan> current;

        public RAFDescriptor(String rafSketchFile) throws IOException {
            raf = new RandomAccessFile(rafSketchFile, "r");
            kryo = createKryo();
            usage = 0;
            curPos = -1;
        }

        public FastList<NestedTokenSpan> read(long position)
                throws IOException {
            if (current != null && curPos == position) {
                return current;
            }
            curPos = -1;
            current = null;
            fetchCurrent(position);

            return current;
        }

        private synchronized void fetchCurrent(long position)
                throws IOException {
            BufferedInputStream buff = new BufferedInputStream(
                    new FileInputStream(raf.getFD()));
            usage++;
            Input input = new Input(buff);
            this.raf.seek(position);
            String line = kryo.readObject(input, String.class);
            current = readLine(line);
            curPos = position;
            usage--;
        }

        @Override
        public void close() throws IOException {
            raf.close();
        }

    }

    public final static class ExactStringSerializer extends Serializer<String> {
        @Override
        public void write(Kryo kryo, Output output, String object) {
            output.write((object + "\n").getBytes());

        }

        @Override
        public String read(Kryo kryo, Input input, Class<String> type) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    input));
            try {
                return reader.readLine().trim();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }
    }

}
