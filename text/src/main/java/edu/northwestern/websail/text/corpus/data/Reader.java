package edu.northwestern.websail.text.corpus.data;
/**
 * @author NorThanapon
 */

import edu.northwestern.websail.text.corpus.model.term.TermStat;

import java.io.IOException;

public abstract class Reader {

    public Reader() {
    }

    public abstract void initialize(String directory, String language) throws IOException;

    public abstract boolean nextTerm() throws IOException;

    public abstract TermStat getCurrentTermStat() throws IOException;

    public abstract void close() throws IOException;

    public abstract int getCurrentTotalDocs();
}
