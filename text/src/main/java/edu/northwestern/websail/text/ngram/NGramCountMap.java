package edu.northwestern.websail.text.ngram;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class NGramCountMap implements Serializable {

    private static final long serialVersionUID = 3739534753846927481L;
    ArrayList<HashMap<String, Integer>> countMap;
    ArrayList<Long> totalCounts;
    ArrayList<String> topNgrams;
    ArrayList<Integer> topCountNgrams;
    ArrayList<Double> totalLogCounts;
    int maxOrder = 5;

    public NGramCountMap() {
        this.countMap = new ArrayList<HashMap<String, Integer>>();
        this.totalCounts = new ArrayList<Long>();
        this.topCountNgrams = new ArrayList<Integer>();
        this.topNgrams = new ArrayList<String>();
        this.totalLogCounts = new ArrayList<Double>();
        for (int i = 0; i < maxOrder; i++) {
            countMap.add(new HashMap<String, Integer>());
            totalCounts.add(0l);
            topNgrams.add("");
            topCountNgrams.add(0);
            totalLogCounts.add(0.0);
        }
    }

    public NGramCountMap(int maxOrder) {
        this.maxOrder = maxOrder;
        this.countMap = new ArrayList<HashMap<String, Integer>>();
        this.totalCounts = new ArrayList<Long>();
        this.topCountNgrams = new ArrayList<Integer>();
        this.topNgrams = new ArrayList<String>();
        this.totalLogCounts = new ArrayList<Double>();
        for (int i = 0; i < maxOrder; i++) {
            countMap.add(new HashMap<String, Integer>());
            totalCounts.add(0l);
            topNgrams.add("");
            topCountNgrams.add(0);
            totalLogCounts.add(0.0);
        }
    }

    public static Kryo createKryo() {
        Kryo kryo = new Kryo();
        kryo.register(HashMap.class);
        kryo.register(ArrayList.class);
        kryo.register(Long.class);
        kryo.register(String.class);
        kryo.register(Integer.class);
        kryo.register(Double.class);
        return kryo;
    }

    public String getTopNgram(int n) {
        if (this.maxOrder < n)
            return null;
        return this.topNgrams.get(n - 1);
    }

    public Integer getTopCountNgram(int n) {
        if (this.maxOrder < n)
            return null;
        return this.topCountNgrams.get(n - 1);
    }

    public Long getTotalCountNgram(int n) {
        if (this.maxOrder < n)
            return null;
        return this.totalCounts.get(n - 1);
    }

    public Double getLogTotalCountNgram(int n) {
        if (this.maxOrder < n)
            return null;
        return this.totalLogCounts.get(n - 1);
    }

    public Integer getCount(String a, int n) {
        if (this.maxOrder < n)
            return 1;
        Integer count = this.countMap.get(n - 1).get(a);
        if (count == null)
            return 1;
        else
            return count;
    }

    public int getMaxOrder() {
        return this.maxOrder;
    }

    public void addNgramCountLine(String line) {
        String[] parts = line.split("\t");
        String[] ngram = parts[0].split(" ");
        int n = ngram.length;
        int count = Integer.parseInt(parts[1]);
        totalCounts.set(n - 1, totalCounts.get(n - 1) + count);
        totalLogCounts.set(n - 1, Math.log10(totalCounts.get(n - 1)));
        int max = topCountNgrams.get(n - 1);
        if (max < count && !(line.contains("<s>") || line.contains("</s>"))) {
            topCountNgrams.set(n - 1, count);
            topNgrams.set(n - 1, parts[0]);
        }
        if (count == 1)
            return;
        countMap.get(n - 1).put(parts[0], count);
    }

    private void writeObject(ObjectOutputStream oss) throws IOException {
        Kryo kryo = createKryo();
        Output output = new Output(oss);
        kryo.writeObject(output, this.maxOrder);
        output.flush();
        kryo.writeObject(output, this.countMap);
        output.flush();
        kryo.writeObject(output, this.topCountNgrams);
        output.flush();
        kryo.writeObject(output, this.totalLogCounts);
        output.flush();
        kryo.writeObject(output, this.topNgrams);
        output.flush();
        kryo.writeObject(output, this.totalCounts);
        output.flush();
    }

    @SuppressWarnings("unchecked")
    private void readObject(ObjectInputStream ois)
            throws ClassNotFoundException, IOException {
        Kryo kryo = createKryo();
        Input input = new Input(ois);
        this.maxOrder = kryo.readObject(input, Integer.class);
        this.countMap = kryo.readObject(input, ArrayList.class);
        this.topCountNgrams = kryo.readObject(input, ArrayList.class);
        this.totalLogCounts = kryo.readObject(input, ArrayList.class);
        this.topNgrams = kryo.readObject(input, ArrayList.class);
        this.totalCounts = kryo.readObject(input, ArrayList.class);
    }

}
