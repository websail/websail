package edu.northwestern.websail.text.tokenizer;

/**
 * @author NorThanapon
 */

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.util.StanfordNLPProperties;
import edu.northwestern.websail.text.util.TokenUtil;
import edu.stanford.nlp.ling.CoreAnnotations.CharacterOffsetBeginAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.CharacterOffsetEndAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

import java.util.List;
import java.util.Properties;

public class StanfordNLPTokenizer extends Tokenizer {

    private final StanfordCoreNLP pipeline;
    private Annotation document;
    private FastList<Token> tokens;
    private int currentIndex;


    public StanfordNLPTokenizer() {
        super();
        Properties props = StanfordNLPProperties.basicConfigure();
        props.put("annotators", "tokenize");
        pipeline = new StanfordCoreNLP(props);
    }

    public static void main(String[] args) {

        StanfordNLPTokenizer t = new StanfordNLPTokenizer();
        String text = "";
        t.setToLower(true);
        t.initialize(text);

        for (Token token : t.getAllTokens()) {
            System.out.println(token.getText() + " " + token.getStartOffset() + "," + token.getEndOffset());
        }
        System.out.println(TokenUtil.tokens2Str(t.getAllTokens()));
        String text2 = "o’donovan ~( Thanapon Noraset Category:Hello World spin-offs";
        t.initialize(text2);
        for (Token token : t.getAllTokens()) {
            System.out.println(token.getText() + " " + token.getStartOffset() + "," + token.getEndOffset());
        }
    }

    public void initialize(String text) {
        this.clear();
        document = new Annotation(text);
        pipeline.annotate(document);
        tokens = new FastList<Token>();
        int i = 0;
        if (document == null) {
            System.out.println("BUG:\t" + text);
        }
        for (CoreLabel token : document.get(TokensAnnotation.class)) {
            Token t = new Token(token.get(TextAnnotation.class),
                    token.get(CharacterOffsetBeginAnnotation.class),
                    token.get(CharacterOffsetEndAnnotation.class),
                    i++);
            if (stopwords.contains(t.getText().toLowerCase())) continue;
            if (toLower) t.setText(t.getText().toLowerCase());
            tokens.add(t);
        }
    }

    @Override
    public List<Token> getAllTokens() {
        return tokens;
    }

    @Override
    public Token next() {
        if ((++currentIndex) < tokens.size()) {
            return this.tokens.get(currentIndex);
        }
        return null;
    }

    @Override
    public Token getCurrentToken() {
        if (currentIndex < tokens.size()) {
            return this.tokens.get(currentIndex);
        }
        return null;
    }

    @Override
    public void clear() {
        document = null;

    }
}
