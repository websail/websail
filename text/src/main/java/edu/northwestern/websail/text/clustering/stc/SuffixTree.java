package edu.northwestern.websail.text.clustering.stc;

import gnu.trove.TIntHashSet;

import java.util.HashMap;

public class SuffixTree {

    final HashMap<Integer, SuffixTree> _outLinks = new HashMap<Integer, SuffixTree>();
    final TIntHashSet _docIds = new TIntHashSet();

    public static void main(String[] args) {
        //simple text code:
        SuffixTree st = new SuffixTree();
        st.addSuffixes(new int[]{0, 1, 2}, 1, 2);
        st.addSuffixes(new int[]{1, 2}, 2);
        st.addSuffixes(new int[]{0, 2}, 2);
        st.addSuffixes(new int[]{0, 1, 3}, 3, 2);
        System.out.println(st._outLinks.size());
        System.out.println(st);
    }

    //note: not efficient
    public String toString() {
        String s = _docIds.toString();
        for (Integer wId : _outLinks.keySet()) {
            s += "[" + wId + ":" + _outLinks.get(wId).toString();
            s += "]";
        }
        return s;
    }

//	//returns unique children and sets uniqueChildCount
//	public TIntHashSet countUniqueChildren() {
//		TIntHashSet uc = new TIntHashSet();
//		uc.addAll(_docIds);
//		Iterator<Integer> it = _outLinks.keySet().iterator();
//		while(it.hasNext()) {
//			int wId = it.next();
//			uc.addAll(_outLinks.get(wId).countUniqueChildren());
//		}
//		_uniqueChildCount = uc.size();
//		return uc;
//	}

    //adds a single suffix to this node, starting at idx
    //if depthLimit >= 0, only recurses to depth limit (0 for stopping here)
    public void add(int[] words, int idx, int docId, int depthLimit) {
        _docIds.add(docId);
        if (idx < words.length && (depthLimit != 0)) {
            int wId = words[idx];
            if (!_outLinks.containsKey(wId)) {
                _outLinks.put(wId, new SuffixTree());
            }
            SuffixTree child = _outLinks.get(wId);
            child.add(words, idx + 1, docId, depthLimit - 1);
        }
    }

    public void addSuffixes(int[] words, int docId) {
        addSuffixes(words, docId, -1);
    }

    //adds all suffixes in words to this node
    //if depth limit >= 0, only goes to given depth
    public void addSuffixes(int[] words, int docId, int depthLimit) {
        for (int i = 0; i < words.length; i++) {
            add(words, i, docId, depthLimit);
        }
    }

    public TIntHashSet lookup(int[] words) {
        return lookup(words, 0);
    }

    private TIntHashSet lookup(int [] words, int idx) {
        if(idx == words.length)
            return this._docIds;
        else if(!this._outLinks.containsKey(words[idx]))
            return null;
        else
            return this._outLinks.get(words[idx]).lookup(words, idx+1);
    }

}
