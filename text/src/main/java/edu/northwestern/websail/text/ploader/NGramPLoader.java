package edu.northwestern.websail.text.ploader;

import edu.berkeley.nlp.lm.WordIndexer;
import edu.berkeley.nlp.lm.io.IOUtils;
import edu.northwestern.websail.core.pipeline.config.PEVResourceLoader;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.text.ngram.ARPALMWrapper;
import edu.northwestern.websail.text.ngram.NGramCountMap;
import edu.northwestern.websail.text.ngram.NgramMapCountWrapper;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Properties;

/**
 * @author NorThanapon
 * @since 11/21/14
 */
public class NGramPLoader {

    @Deprecated
    public static NGramCountMap nGramCountMap(PipelineEnvironment env, Properties config, Boolean global, Boolean put) throws IOException, ClassNotFoundException {
        String fileName = config.getProperty("ngram.countBinaryFile");
        ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(
                new FileInputStream(fileName)));
        NGramCountMap map = (NGramCountMap) ois.readObject();
        ois.close();
        if (put) {
            PEVResourceLoader.setResource(env, PEVResourceType.NGRAM_COUNT, global, map);
        }
        return map;
    }

    public static ARPALMWrapper loadNGramModel(PipelineEnvironment env, Properties config, Boolean global, Boolean put) {
        String ngramModelFile = config.getProperty("ngram.lm.arpaBinaryFile");
        if (ngramModelFile == null) {
            PEVResourceLoader.logger.severe("'ngram.lm.arpaBinaryFile' is not available.");
        }
        ARPALMWrapper lm = new ARPALMWrapper(ngramModelFile, false, true);
        if (put) {
            PEVResourceLoader.setResource(env, PEVResourceType.CORPUS_NGRAM_MODEL, global, lm);
        }
        return lm;
    }

    public static NgramMapCountWrapper corpusNGramCountMap(PipelineEnvironment env, Properties config, Boolean global, Boolean put) {
        String fileName = config.getProperty("ngram.corpusCountBinaryFile");
        WordIndexer<String> wordIndexer = getOrLoadWordIndexer(env, config, "ngram.countWordIndexer");
        NgramMapCountWrapper map = loadNgramMapCountWrapper(fileName, wordIndexer);
        if (put) {
            PEVResourceLoader.setResource(env, PEVResourceType.CORPUS_NGRAM_COUNT, global, map);
        }
        return map;
    }

    public static NgramMapCountWrapper web1TNGramCountMap(PipelineEnvironment env, Properties config, Boolean global, Boolean put) {
        String fileName = config.getProperty("ngram.web1TCountBinaryFile");
        WordIndexer<String> wordIndexer = getOrLoadWordIndexer(env, config, "ngram.web1TCountWordIndexer");
        NgramMapCountWrapper map = loadNgramMapCountWrapper(fileName, wordIndexer);
        if (put) {
            PEVResourceLoader.setResource(env, PEVResourceType.WEB1T_NGRAM_COUNT, global, map);
        }
        return map;
    }

    public static NgramMapCountWrapper loadNgramMapCountWrapper(String file, WordIndexer<String> wordIndexer) {
        NgramMapCountWrapper countMap = (NgramMapCountWrapper) IOUtils.readObjFileHard(file);
        countMap.setWordIndexer(wordIndexer);
        return countMap;
    }

    @SuppressWarnings("unchecked")
    private static WordIndexer<String> getOrLoadWordIndexer(PipelineEnvironment env, Properties config, String key) {
        WordIndexer<String> wordIndexer = (WordIndexer<String>) env.getPipelineGlobalOuputs().get(key);
        if (wordIndexer == null) {
            String file = config.getProperty(key);
            wordIndexer = (WordIndexer<String>) IOUtils.readObjFileHard(file);
            env.getPipelineGlobalOuputs().put(key, wordIndexer);
        }
        return wordIndexer;
    }
}
