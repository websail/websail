package edu.northwestern.websail.text.corpus.data.lucene.collector;

import edu.northwestern.websail.text.corpus.data.lucene.IndexAggregator;
import edu.northwestern.websail.text.corpus.model.term.TermMeasureStat;
import edu.northwestern.websail.text.corpus.util.Measurement;
import org.apache.lucene.index.AtomicReaderContext;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author NorThanapon
 * @since 11/7/14
 */
public class TermMeasureStatCollector extends Collector {
    private final DirectoryReader reader;

    private int totalDocs = 0;
    private int base = 0;
    private HashMap<String, TermMeasureStat> stat;

    public TermMeasureStatCollector(DirectoryReader reader) {
        stat = new HashMap<String, TermMeasureStat>();
        this.reader = reader;
    }

    public TermMeasureStatCollector(DirectoryReader reader, HashMap<String, TermMeasureStat> stat) {
        this.reader = reader;
        this.stat = stat;
    }

    @Override
    public boolean acceptsDocsOutOfOrder() {
        return true;
    }

    @Override
    public void collect(int docId) throws IOException {
        totalDocs++;
        Terms vector = this.reader.getTermVector(docId + base, IndexAggregator.DOC_CONTENT_FIELD);
        if (vector == null) return;
        TermsEnum termsEnum = vector.iterator(null);
        BytesRef text;
        while ((text = termsEnum.next()) != null) {
            String term = text.utf8ToString();
            long freq = termsEnum.totalTermFreq();
            if (!this.stat.containsKey(term)) {
                TermMeasureStat tStat = new TermMeasureStat(term);
                stat.put(term, tStat);
            }
            stat.get(term).updateDF(freq);
        }
    }

    @Override
    public void setNextReader(AtomicReaderContext context) throws IOException {
        this.base = context.docBase;
    }

    @Override
    public void setScorer(Scorer score) throws IOException {

    }

    public int getTotalDocs() {
        return totalDocs;
    }

    public void setTotalDocs(int totalDocs) {
        this.totalDocs = totalDocs;
    }

    public HashMap<String, TermMeasureStat> getStat() {
        for (Map.Entry<String, TermMeasureStat> entry : this.stat.entrySet()) {
            Measurement.computeMeasures(entry.getValue(), totalDocs);
        }
        return stat;
    }

    public void setStat(HashMap<String, TermMeasureStat> tf) {
        this.stat = tf;
    }
}
