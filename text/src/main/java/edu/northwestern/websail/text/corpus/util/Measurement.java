package edu.northwestern.websail.text.corpus.util;

/**
 * @author NorThanapon
 *         all the measures defined in Church and Gale 1995
 */

import edu.northwestern.websail.text.corpus.model.term.ExtendedTermMeasureStat;
import edu.northwestern.websail.text.corpus.model.term.TermMeasureStat;


public class Measurement {

    public static void computeExtendedMeasures(ExtendedTermMeasureStat t, int totalDocs) {
        computeMeasures(t, totalDocs);
        t.setBurstiness(burstiness(t.getDf1() + 1, t.getTf() + 1));
        t.setAdapt3(adapt(t.getDf2() + 1, t.getDf3() + 1));
        t.setAdapt4(adapt(t.getDf2() + 1, t.getDf3() + 1));
        t.setIdf2(idf(totalDocs + 1, t.getDf2() + 1));
        t.setrIDF2(rIDF(totalDocs + 1, t.getDf2() + 1, t.getTf() + 1));
    }

    public static void computeMeasures(TermMeasureStat t, int totalDocs) {
        t.setIdf(idf(totalDocs + 1, t.getDf1() + 1));
        t.setrIDF(rIDF(totalDocs + 1, t.getDf1() + 1, t.getTf() + 1));
        t.setAdapt2(adapt(t.getDf1() + 1, t.getDf2() + 1));
    }

    public static double idf(int totalDocs, int df1) {
        //-log2 Pr(x>=1) -> log2 totalDocs/df
        return log2((double) totalDocs / (double) df1);
    }

    public static double rIDF(int totalDocs, int df1, long tf) {
        double idf = idf(totalDocs, df1);
        double lambda = (double) tf / (double) totalDocs;
        return idf + log2(1 - Math.pow(Math.E, -lambda));
    }

    public static double burstiness(int df1, long tf) {
        return (double) tf / (double) df1;
    }

    public static double adapt(int dfi, int dfj) {
        return (double) dfj / (double) dfi;
    }

    public static double log2(double x) {
        return Math.log(x) / Math.log(2);
    }
}
