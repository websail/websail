package edu.northwestern.websail.text.chunker;

import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.tokenizer.OpenNLPSSplit;
import edu.northwestern.websail.text.tokenizer.SentenceSpliter;
import edu.northwestern.websail.text.util.OpenNLPModels;
import edu.northwestern.websail.text.util.TokenUtil;
import opennlp.tools.chunker.ChunkerME;
import opennlp.tools.util.Span;

import java.io.IOException;

/**
 * @author NorThanapon
 * @since 11/10/14
 */
public class OpenNLPShallowChunker extends Chunker {
    final SentenceSpliter sSplit;
    final ChunkerME chunker;

    public OpenNLPShallowChunker() throws IOException {
        this(new OpenNLPSSplit(true));
    }

    public OpenNLPShallowChunker(SentenceSpliter sSplit) throws IOException {
        super();
        this.sSplit = sSplit;
        this.chunker = new ChunkerME(OpenNLPModels.getChunkerModel());
    }

    @Override
    public void initialize(String text, String source) {
        super.initialize(text, source);
        this.currentSentences = sSplit.sentenceSplit(this.currentText);
    }

    @Override
    public void extractNounPhrase(NounPhraseFoundDelegate delegate) throws Exception {
        int i = 0;
        for (TokenSpan sentence : currentSentences) {
            delegate.setSentence(sentence);
            String[] tokens = TokenUtil.tokens2StrArr(sentence.getTokens());
            String[] tags = TokenUtil.token2POSArr(sentence.getTokens());
            Span[] chunks = chunker.chunkAsSpans(tokens, tags);
            for (Span chunk : chunks) {
                if (!chunk.getType().equals("NP")) {
                    continue;
                }
                Token startToken = sentence.getTokens().get(chunk.getStart());
                Token endToken = sentence.getTokens().get(chunk.getEnd() - 1);
                TokenSpan np = Chunker.cleanNounPhrase(
                        new TokenSpan(startToken.getStartOffset(), endToken.getEndOffset(),
                                sentence.getTokens().subList(chunk.getStart(), chunk.getEnd()), i++));
                if (np != null) {
                    delegate.found(np);
                }
            }
        }
    }
}
