package edu.northwestern.websail.text.corpus.util;

import java.io.*;

public class SWATHAdapter {
    private static final String SWATHDIR = "/usr/local/bin/swath";
    private static final String INTEMP = "temp.in";
    private static final String OUTTEMP = "temp.out";

    public static String swath(String input, boolean isMule, String separator) throws IOException, InterruptedException {
        String mule = "";
        if (isMule) mule = "mule";
        String command = SWATHDIR + " " + mule + " " + " -u u,u " +
                " -b \"" + separator + "\"" +
                " < " + INTEMP + " > " + OUTTEMP;
        System.out.println(command);
        writeInput(input);
        Runtime rt = Runtime.getRuntime();
        String[] cmd = {"/bin/sh", "-c", command};
        Process pr = rt.exec(cmd);
        pr.waitFor();
        String output = readOutput();
        deleteTempFiles();
        return output;
    }

    private static void writeInput(String input) throws IOException {
        FileOutputStream fs = new FileOutputStream(INTEMP);
        OutputStreamWriter writer = new OutputStreamWriter(fs, "UTF-8");
        try {
            writer.write(input);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            writer.close();
            fs.close();
        }
    }

    private static String readOutput() throws IOException {
        FileInputStream fs = new FileInputStream(OUTTEMP);
        InputStreamReader reader = new InputStreamReader(fs, "UTF-8");
        BufferedReader in = new BufferedReader(reader);
        String output = "";
        String str;
        while ((str = in.readLine()) != null) {
            output += str + "\n";
        }
        in.close();
        reader.close();
        fs.close();
        return output;
    }

    private static void deleteTempFiles() {
        File f1 = new File(INTEMP);
        File f2 = new File(OUTTEMP);
        f1.deleteOnExit();
        f2.deleteOnExit();

    }
}
