package edu.northwestern.websail.text.util;

import opennlp.tools.chunker.ChunkerModel;
import opennlp.tools.parser.ParserModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.TokenizerModel;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author NorThanapon
 * @since 11/7/14
 *
 * This class helps create OpenNLP's models. We assume that the models are thread-safe, so they can be singleton.
 * It picks up model files from a jar file.
 */
public class OpenNLPModels {
    private static TokenizerModel tokenizerModel;
    private static SentenceModel sentenceModel;
    private static ChunkerModel chunkerModel;
    private static POSModel posModel;
    private static ParserModel parserModel;

    public synchronized static void initTokenizerModel() throws IOException {
        if (tokenizerModel != null) return;
        InputStream is = OpenNLPModels.class.getResourceAsStream("/org/apache/opennlp/models/en/en-token.bin");
        tokenizerModel = new TokenizerModel(is);
        is.close();
    }

    public synchronized static void initSentenceModel() throws IOException {
        if (sentenceModel != null) return;
        InputStream is = OpenNLPModels.class.getResourceAsStream("/org/apache/opennlp/models/en/en-sent.bin");
        sentenceModel = new SentenceModel(is);
        is.close();
    }

    public synchronized static void initPOSModel() throws IOException {
        if (posModel != null) return;
        InputStream is = OpenNLPModels.class.getResourceAsStream("/org/apache/opennlp/models/en/en-pos-maxent.bin");
        posModel = new POSModel(is);
        is.close();
    }

    public synchronized static void initChunkerModel() throws IOException {
        if (chunkerModel != null) return;
        InputStream is = OpenNLPModels.class.getResourceAsStream("/org/apache/opennlp/models/en/en-chunker.bin");
        chunkerModel = new ChunkerModel(is);
        is.close();
    }

    public synchronized static void initParserModel() throws IOException {
        if (parserModel != null) return;
        InputStream is = OpenNLPModels.class.getResourceAsStream("/org/apache/opennlp/models/en/en-parser-chunking.bin");
        parserModel = new ParserModel(is);
        is.close();
    }

    public static TokenizerModel getTokenizerModel() throws IOException {
        if (tokenizerModel == null) initTokenizerModel();
        return tokenizerModel;
    }

    public static SentenceModel getSentenceModel() throws IOException {
        if (sentenceModel == null) initSentenceModel();
        return sentenceModel;
    }

    public static ChunkerModel getChunkerModel() throws IOException {
        if (chunkerModel == null) initChunkerModel();
        return chunkerModel;
    }

    public static POSModel getPOSModel() throws IOException {
        if (posModel == null) initPOSModel();
        return posModel;
    }

    public static ParserModel getParserModel() throws IOException {
        if (parserModel == null) initParserModel();
        return parserModel;
    }
}
