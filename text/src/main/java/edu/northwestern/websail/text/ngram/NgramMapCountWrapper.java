package edu.northwestern.websail.text.ngram;

import edu.berkeley.nlp.lm.WordIndexer;
import edu.berkeley.nlp.lm.map.NgramMap;
import edu.berkeley.nlp.lm.util.LongRef;
import edu.berkeley.nlp.lm.values.CountValueContainer;

import java.io.Serializable;

public class NgramMapCountWrapper implements Serializable {

    public static final long DEFAULT_COUNT = 1;
    private static final long serialVersionUID = 3739534753846927481L;
    long[] topCountNgrams;
    String[] topNgrams;
    NgramMap<LongRef> map;
    WordIndexer<String> wordIndexer;
    int unkIndex = -1;

    public NgramMapCountWrapper() {
    }

    public NgramMapCountWrapper(NgramMap<LongRef> map, WordIndexer<String> wordIndexer, long[] topCountNgrams, String[] topNgrams) {
        this.setMap(map);
        this.topCountNgrams = topCountNgrams;
        this.topNgrams = topNgrams;
        this.setWordIndexer(wordIndexer);
    }

    public Long getTopCountNgram(int order) {
        if (this.getMaxOrder() < order)
            return null;
        return this.topCountNgrams[order - 1];
    }

    public String getTopNgram(int order) {
        if (this.getMaxOrder() < order)
            return null;
        return this.topNgrams[order - 1];
    }

    public long getUnigramCount() {
        return ((CountValueContainer) map.getValues()).getUnigramSum();
    }

    public long getCount(String[] ngrams) {
        if (this.getMaxOrder() < ngrams.length)
            return 1l;
        int[] index = new int[ngrams.length];
        for (int i = 0; i < ngrams.length; i++) {
            String gram = ngrams[i];
            index[i] = wordIndexer.getIndexPossiblyUnk(gram);
            if (index[i] == unkIndex) return 1l;
        }
        long count = this.getCount(index, 0, index.length);
        if (count <= 0) return 1l;
        return count;
    }

    public int getMaxOrder() {
        return this.map.getMaxNgramOrder();
    }

    private long getCount(int[] ngram, int startPos, int endPos) {
        NgramMap<LongRef> localMap = this.map;
        long probContext = 0L;
        LongRef scratch = new LongRef(-1L);

        for (int probContextOrder = -1; probContextOrder < endPos - startPos - 1; ++probContextOrder) {
            assert probContext >= 0L;
            probContext = localMap.getValueAndOffset(probContext, probContextOrder, ngram[endPos - probContextOrder - 2], scratch);
            if (probContext < 0L) {
                return -1L;
            }
        }

        return scratch.value;
    }

    public NgramMap<LongRef> getMap() {
        return map;
    }

    public void setMap(NgramMap<LongRef> map) {
        this.map = map;
    }

    public WordIndexer<String> getWordIndexer() {
        return wordIndexer;
    }

    public void setWordIndexer(WordIndexer<String> wordIndexer) {
        this.wordIndexer = wordIndexer;
        this.unkIndex = wordIndexer.getIndexPossiblyUnk(wordIndexer.getUnkSymbol());
    }

}
