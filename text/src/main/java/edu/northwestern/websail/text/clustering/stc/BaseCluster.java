package edu.northwestern.websail.text.clustering.stc;

import edu.northwestern.websail.text.util.StopWords;
import gnu.trove.TIntHashSet;
import org.jetbrains.annotations.NotNull;

public class BaseCluster implements Comparable {
	public final TIntHashSet _docIds;
	public final String _phrase;
	final int _length; //convenience field -- does not count stop words

	
	public BaseCluster(String phrase, TIntHashSet docs) {
		int len = 0;
		_phrase = phrase;
		String [] words = phrase.split(" ");
		for (String word : words) {
            if (!StopWords.isStop(word))
                len++;
		}
		_length = len;
		_docIds = new TIntHashSet();
        _docIds.addAll(docs.toArray());
    }
	
	public String toString() {
		return _phrase + "\t" + _docIds.toString();
	}
	
	public double score() {
		if(_docIds.size() == 1)
			return Double.NEGATIVE_INFINITY;
		double multiplier = 1.0;
		if(_length > 1)
			multiplier *= 20.0;
		if(_length > 2)
			multiplier *= 3.0;
		if(_length > 3)
			multiplier *= 1.3;
		if(_length > 4)
			multiplier *= 1.1;
		return (_docIds.size() * multiplier);
	}
	
	@Override
	public int compareTo(@NotNull Object o) {
		double os = ((BaseCluster)o).score();
		double ts = this.score();
		if(os > ts)
			return -1;
		else if(os == ts)
			return 0;
		else
			return 1;
	}
	
}