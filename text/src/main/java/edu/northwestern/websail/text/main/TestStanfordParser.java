package edu.northwestern.websail.text.main;

import edu.stanford.nlp.dcoref.CorefChain;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.util.CoreMap;

import java.util.*;

/**
 * @author NorThanapon
 * @since 4/18/15
 */
public class TestStanfordParser {

    private static HashMap<String, String> gPOSMap = new HashMap<String, String>();

    static {
        gPOSMap.put("JJ", "JJ");
        gPOSMap.put("JJR", "JJ");
        gPOSMap.put("JJS", "JJ");
        gPOSMap.put("NN", "NN");
        gPOSMap.put("NNS", "NN");
        gPOSMap.put("NNP", "NN");
        gPOSMap.put("NNPS", "NN");
        gPOSMap.put("PR", "PR");
        gPOSMap.put("PRP", "PR");
        gPOSMap.put("PRP$", "PR");
        gPOSMap.put("WP", "PR");
        gPOSMap.put("WP$", "PR");
        gPOSMap.put("RB", "RB");
        gPOSMap.put("RBR", "RB");
        gPOSMap.put("RBS", "RB");
        gPOSMap.put("WRB", "RB");
        gPOSMap.put("VB", "VB");
        gPOSMap.put("VBD", "VB");
        gPOSMap.put("VBG", "VB");
        gPOSMap.put("VBN", "VB");
        gPOSMap.put("VBP", "VB");
        gPOSMap.put("VBZ", "VB");
    }

    public static void main(String[] args) {
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
        String str = "I like the book about New York because it is interesting.";
        System.out.println(parse(pipeline, str));

    }

    private static String parse(StanfordCoreNLP parser, String text) {
        StringBuilder posSB = new StringBuilder();
        StringBuilder dpSB = new StringBuilder();
        StringBuilder nerSB = new StringBuilder();
        StringBuilder lemmaSB = new StringBuilder();
        StringBuilder corefSB = new StringBuilder();
        Annotation document = new Annotation(text);
        parser.annotate(document);
        List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
        dpSB.append("# dependencies\n");
        posSB.append("# part-of-speech\n");
        nerSB.append("# named entities\n");
        lemmaSB.append("# lemmatization\n");
        corefSB.append("# coref chains\n");
        ArrayList<Integer> offsets = new ArrayList<Integer>();
        int offset = 0;
        for (CoreMap sentence : sentences) {
            offsets.add(offset);
            SemanticGraph dependencies = sentence.get(SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation.class);
            for (IndexedWord iw : dependencies.getRoots()) {
                dpSB.append("(amr_dp_root amr_tk_tok")
                        .append(offset + iw.index() - 1).append(" amr_nil)\n");
            }
            for (SemanticGraphEdge e : dependencies.edgeListSorted()) {
                dpSB.append("(amr_dp_").append(e.getRelation()).append(" ");
                dpSB.append("amr_tk_tok").append(offset + e.getSource().index() - 1);
                dpSB.append(" ");
                dpSB.append("amr_tk_tok").append(offset + e.getTarget().index() - 1);
                dpSB.append(")\n");
            }
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
                String lemma = token.get(CoreAnnotations.LemmaAnnotation.class);
                String ner = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);

                posSB.append("(amr_pos_")
                        .append(escapePOS(pos))
                        .append(" amr_tk_tok")
                        .append(offset + token.index() - 1)
                        .append(" amr_nil)\n");
                lemmaSB.append("(amr_lemm_")
                        .append(escapeLemma(lemma))
                        .append(" amr_tk_tok")
                        .append(offset + token.index() - 1)
                        .append(" amr_nil)\n");
                if (!ner.equals("O")) {
                    nerSB.append("(amr_ner_")
                            .append(ner)
                            .append(" amr_tk_tok")
                            .append(offset + token.index() - 1)
                            .append(" amr_nil)\n");
                }
                String gpos = gPOS(pos);
                if (gpos != null) {
                    posSB.append("(amr_pos_g_")
                            .append(escapePOS(gPOS(pos)))
                            .append(" amr_tk_tok")
                            .append(offset + token.index() - 1)
                            .append(" amr_nil)\n");
                }
            }
            offset += sentence.get(CoreAnnotations.TokensAnnotation.class).size();
        }

        for (Map.Entry<Integer, CorefChain> p : document.get(CorefCoreAnnotations.CorefChainAnnotation.class).entrySet()) {
            CorefChain chain = p.getValue();
            if (chain.getMentionsInTextualOrder().size() < 2) {
                continue;
            }
            int startToken = -1;
            for (CorefChain.CorefMention mention : chain.getMentionsInTextualOrder()) {
                int mentionOffset = offsets.get(mention.sentNum - 1) + mention.endIndex - 2;
                if (startToken == -1) {
                    startToken = mentionOffset;
                } else {
                    corefSB.append("(amr_coref_link ")
                            .append("amr_tk_tok").append(mentionOffset)
                            .append(" amr_tk_tok").append(startToken)
                            .append(")\n");
                }
            }
        }

        return dpSB.toString() + posSB.toString() + nerSB.toString() + lemmaSB.toString() + corefSB.toString();
    }

    private static String gPOS(String pos) {
        if (gPOSMap.containsKey(pos)) {
            return gPOSMap.get(pos);
        } else {
            return null;
        }
    }

    private static String escapePOS(String pos) {
        if (pos.equals("''")) return "_DQOUTE_";
        if (pos.equals("``")) return "_DQOUTE_";
        if (pos.equals(":")) return "_COLON_";
        if (pos.equals(";")) return "_SCOLON_";
        if (pos.equals(",")) return "_COMMA_";
        if (pos.equals("-LRB-")) return "_LRB_";
        if (pos.equals("-RRB-")) return "_RRB_";
        if (pos.equals("-LCB-")) return "_LCB_";
        if (pos.equals("-RCB-")) return "_RCB_";
        if (pos.equals("-LSB-")) return "_LSB_";
        if (pos.equals("-RSB-")) return "_RSB_";
        return pos;
    }

    private static String escapeLemma(String lemma) {
        return lemma.replaceAll("\"", "_DQOUTE_")
                .replaceAll("''", "_DQOUTE_")
                .replaceAll("``", "_DQOUTE_")
                .replaceAll(",", "_COMMA_")
                .replaceAll("'", "_SQOUTE_")
                .replaceAll(":", "_COLON_")
                .replaceAll(";", "_SCOLON_")
                .replaceAll("--", "_EMDASH_")
                .replaceAll("-", "_ENDASH_")
                .replaceAll("\\.\\.\\.", "_ELLIPSIS_")
                .replaceAll("\\.", "_DOT_")
                .replaceAll("-lrb-", "_LRB_")
                .replaceAll("-rrb-", "_RRB_")
                .replaceAll("-lcb-", "_LCB_")
                .replaceAll("-rcb-", "_RCB_")
                .replaceAll("-lsb-", "_LSB_")
                .replaceAll("-rsb-", "_RSB_")
                .replaceAll(" ", "_SPACE_");
    }
}
