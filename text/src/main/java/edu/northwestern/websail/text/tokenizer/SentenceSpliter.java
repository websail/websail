package edu.northwestern.websail.text.tokenizer;

import edu.northwestern.websail.text.data.TokenSpan;

import java.util.List;

/**
 * @author NorThanapon
 * @since 11/5/14
 */
public abstract class SentenceSpliter {
    protected boolean posEnabled = false;

    public SentenceSpliter() {
    }

    public SentenceSpliter(Boolean posEnabled) {
        this.posEnabled = posEnabled;
    }

    public abstract List<TokenSpan> sentenceSplit(String text);
}
