package edu.northwestern.websail.text.clustering.stc;

import edu.northwestern.websail.text.util.StopWords;
import gnu.trove.TIntHashSet;
import gnu.trove.TIntIterator;

import java.io.*;
import java.util.PriorityQueue;
import java.util.concurrent.ConcurrentHashMap;


public class STC {
    public static final int _DEPTHLIMIT = 5; //max size of key phrase, including stop words and punct
    //stores word<->ID mapping:
    final ConcurrentHashMap<String, Integer> _dict = new ConcurrentHashMap<String, Integer>();
    final ConcurrentHashMap<Integer, String> _revDict = new ConcurrentHashMap<Integer, String>();
    //scores filename<->ID mapping:
    final ConcurrentHashMap<String, Integer> _fileDict = new ConcurrentHashMap<String, Integer>();
    final ConcurrentHashMap<Integer, String> _revFileDict = new ConcurrentHashMap<Integer, String>();
    final SuffixTree _st = new SuffixTree();
    public int _numBaseClusters = 1000;
    public double _mergeThreshold = 0.5;
    public boolean _caseFold = true;

    public static void toUseAPI(String docsDirectory) throws Exception {
        STC stc = new STC();
        stc.addDocuments(docsDirectory);
        String s = "natural language processing";
        int df = stc.phraseDocFrequency(s);
        System.out.println("\"" + s + "\" appears in " + df + " docs.");
        s = "processing";
        df = stc.phraseDocFrequency(s);
        System.out.println("\"" + s + "\" appears in " + df + " docs.");
        s = "information for";
        df = stc.phraseDocFrequency(s);
        System.out.println("\"" + s + "\" appears in " + df + " docs.");
        s = "schwizzles perform well";
        df = stc.phraseDocFrequency(s);
        System.out.println("\"" + s + "\" appears in " + df + " docs.");
        s = "algorithms perform well";
        df = stc.phraseDocFrequency(s);
        System.out.println("\"" + s + "\" appears in " + df + " docs.");
        s = "language natural processing language";
        df = stc.phraseDocFrequency(s);
        System.out.println("\"" + s + "\" appears in " + df + " docs.");
    }

    public static void main(String[] args) throws Exception {
//		toUseAPI(args[0]);
//		if(true)
//			return;
        if (args.length < 2) {
            System.out.println("Usage: java STC <in-directory> <outfile> [<num base clusters> <merge threshold> <case folding>]");
            return;
        }
        STC stc = new STC();
        if (args.length == 5) {
            stc._numBaseClusters = Integer.parseInt(args[2]);
            stc._mergeThreshold = Double.parseDouble(args[3]);
            stc._caseFold = Boolean.parseBoolean(args[4]);
        }

        TIntHashSet ts = new TIntHashSet();
        ts.add(20);
        ts.add(30);
        BaseCluster bc = new BaseCluster("the people", ts);
        System.out.println(bc + " " + bc._length + " " + bc.score());

        stc.addDocuments(args[0]);
        //System.out.println(stc._st);
        BaseCluster[] bcs = stc.getBaseClusters();
        int[] cids = stc.clusterBaseClusters(bcs);
//		for(int i=bcs.length-1; i>=0; i--) {
//			System.out.println(i + " " + cids[i] + " " + bcs[i] + " " + bcs[i]._length + " " + bcs[i].score());
//			String [] toks = bcs[i]._phrase.split(" ");
//			System.out.println(Arrays.toString(toks));
//		}
        stc.outputClusters(args[1], bcs, cids);
    }

    public void addDocuments(String inDir) throws Exception {
        File[] fs = (new File(inDir)).listFiles();
        assert fs != null;
        for (File f : fs) {
            addDocument(f);
//            if (i % 500 == 0)
//                System.out.print(i + ".");
//			if(i==2500)
//				break;
        }
        //System.out.println("added documents to trie");
    }

    //gets word's id, adding to hashes if necessary
    public int wordId(String w) {
        int id;
        if (!_dict.containsKey(w)) {
            id = _dict.size() + 1;
            _dict.put(w, id);
            _revDict.put(id, w);
        }
        return _dict.get(w);
    }

    public void addDocument(File inFile) throws Exception {
        int id;
        if (!_fileDict.containsKey(inFile.getName())) {
            id = _fileDict.size() + 1;
            _fileDict.put(inFile.getName(), id);
            _revFileDict.put(id, inFile.getName());
        } else {
            id = _fileDict.get(inFile.getName());
        }

        BufferedReader brIn = new BufferedReader(
                new InputStreamReader(new FileInputStream(inFile), "UTF8"));
        String sLine;

        while ((sLine = brIn.readLine()) != null) {
            addSentence(sLine, id);
        }
        brIn.close();
    }

    public void addSentence(String sLine, int docId) throws Exception {
        String tokenized = StringUtilities.tokenize(sLine);
        if (_caseFold)
            tokenized = tokenized.toLowerCase();
        String[] toks = tokenized.split(" ");
        int[] wids = new int[toks.length];
        //System.out.println(wids.length);
        for (int i = 0; i < wids.length; i++) {
            wids[i] = wordId(toks[i]);
        }
        _st.addSuffixes(wids, docId, _DEPTHLIMIT);
    }

    public void addAsBaseClusterToQueue(String prefix, PriorityQueue<BaseCluster> c, SuffixTree st, int pruneToSize) {
        BaseCluster bc = new BaseCluster(prefix, st._docIds);
        c.add(bc);
        if (c.size() > pruneToSize) {
            c.poll();
        }
        for (Integer wId : st._outLinks.keySet()) {
            String s = _revDict.get(wId);
            addAsBaseClusterToQueue((prefix + " " + s).trim(), c, st._outLinks.get(wId), pruneToSize);
        }
    }

    public BaseCluster[] getBaseClusters() {
        BaseCluster[] out = new BaseCluster[_numBaseClusters];
        PriorityQueue<BaseCluster> pq = new PriorityQueue<BaseCluster>();
        addAsBaseClusterToQueue("", pq, _st, _numBaseClusters);
        for (int i = out.length - 1; i >= 0; i--) {
            if (pq.size() > 0)
                out[i] = pq.poll();
        }
        return out;
    }

    public boolean closeEnough(BaseCluster a, BaseCluster b) {
        TIntIterator it = a._docIds.iterator();
        int intersect = 0;
        while (it.hasNext()) {
            if (b._docIds.contains(it.next()))
                intersect++;
        }
        return ((double) intersect) / ((double) a._docIds.size()) > this._mergeThreshold
                && ((double) intersect) / ((double) b._docIds.size()) > this._mergeThreshold;
    }

    public int[] clusterBaseClusters(BaseCluster[] bcs) {
        int[] out = new int[bcs.length];
        for (int i = 0; i < out.length; i++) {
            out[i] = i;
            for (int j = 1; j < out.length; j++) {
                if (closeEnough(bcs[i], bcs[j])) {
                    out[j] = out[i];
                }
            }
        }
        return out;
    }

    public void outputClusters(String outFile, BaseCluster[] bcs, int[] cids) throws Exception {
        BufferedWriter bwOut = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "UTF8"));

        for (int i = 0; i < cids.length; i++) {
            TIntHashSet docs = new TIntHashSet();
            String rep = "";
            double maxScore = -1.0;
            for (int j = 0; j < cids.length; j++) {
                if (cids[j] == i) {
                    System.out.println(i + ":" + bcs[j]._phrase);
                    double score = bcs[j].score();
                    if (bcs[j].score() > maxScore) {
                        maxScore = score;
                        rep = bcs[j]._phrase;
                    }
                    docs.addAll(bcs[j]._docIds.toArray());
                }
            }
            if (maxScore > 0) {
                System.out.println("CLUSTER " + i + "\t" + rep);
                //check for starts/ends with stop word:
                String[] words = rep.split(" ");
                if (StopWords.isStop(words[0].toLowerCase()) || StopWords.isStop(words[words.length - 1].toLowerCase())) {
                    System.out.println("discarded due to stop words.");
                    continue;
                }
                bwOut.write(rep + "\t");
                TIntIterator it = docs.iterator();
                while (it.hasNext()) {
                    int doc = it.next();
                    bwOut.write(this._revFileDict.get(doc) + " ");
                }
                bwOut.write("\r\n");
            }
        }
        bwOut.close();
    }

    public int[] translate(String[] ws) {
        int[] out = new int[ws.length];
        for (int i = 0; i < out.length; i++) {
            out[i] = wordId(ws[i]);
        }
        return out;
    }

    public int phraseDocFrequency(String phrase) throws Exception {
        String[] ws = phrase.split(" ");
        if (ws.length > _DEPTHLIMIT) {
            return 0;
//            throw new Exception("phrase must be less than depth limit (currently " + _DEPTHLIMIT + ".");
        }
        int[] wids = translate(ws);
        TIntHashSet words = _st.lookup(wids);
        if (words == null)
            return 0;
        else
            return words.size();
    }

}
