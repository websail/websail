package edu.northwestern.websail.text.util;

import java.util.Properties;

/**
 * @author NorThanapon
 * @since 11/5/14
 */
public class StanfordNLPProperties {
    //ptb3Escaping is essential for downstream pipelines' accuracy
    public static final String TOKENIZER_CONFIG = "untokenizable=noneKeep,ptb3Escaping=true";

    public static Properties basicConfigure() {
        Properties props = new Properties();
        props.put("ssplit.newlineIsSentenceBreak", "always");
        props.put("ssplit.htmlBoundariesToDiscard", ".*");
        props.put("tokenize.options", TOKENIZER_CONFIG);
        props.put("parse.model", "edu/stanford/nlp/models/lexparser/englishRNN.ser.gz");
        props.put("ner.applyNumericClassifiers", "false");
        return props;
    }

    public static Properties srParserConfigure() {
        Properties props = new Properties();
        props.put("ssplit.newlineIsSentenceBreak", "always");
        props.put("ssplit.htmlBoundariesToDiscard", ".*");
        props.put("tokenize.options", TOKENIZER_CONFIG);
        props.put("parse.model", "edu/stanford/nlp/models/srparser/englishSR.ser.gz");
        props.put("ner.applyNumericClassifiers", "false");
        return props;
    }
}
