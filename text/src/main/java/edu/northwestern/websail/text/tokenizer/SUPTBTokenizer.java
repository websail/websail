package edu.northwestern.websail.text.tokenizer;

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.util.StanfordNLPProperties;
import edu.stanford.nlp.process.LexedTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;

import java.io.StringReader;
import java.util.List;

/**
 * @author NorThanapon
 * @since 7/17/15
 */
public class SUPTBTokenizer extends Tokenizer {

    private static final SUTokenFactory defaultTokenFactory = new SUTokenFactory();
    private List<Token> tokens;
    private int curPos = 0;

    public SUPTBTokenizer() {
        super();
    }

    public static void main(String args[]) {
        SUPTBTokenizer tokenizer = new SUPTBTokenizer();
        tokenizer.initialize("Hello World! This is a\n sentence.");
        System.out.println(tokenizer.getAllTokens());
    }

    @Override
    public void initialize(String text) {
        this.clear();
        StringReader reader = new StringReader(text);
        PTBTokenizer<Token> tokenizer = new PTBTokenizer<Token>(reader, defaultTokenFactory, StanfordNLPProperties.TOKENIZER_CONFIG);
        this.tokens = new FastList<Token>(tokenizer.tokenize());
    }

    @Override
    public List<Token> getAllTokens() {
        return this.tokens;
    }

    @Override
    public Token next() {
        curPos++;
        if (curPos < tokens.size())
            return tokens.get(curPos);
        return null;
    }

    @Override
    public Token getCurrentToken() {
        return this.tokens.get(this.curPos);
    }

    @Override
    public void clear() {
        this.tokens = null;
        this.curPos = 0;
    }

    private static class SUTokenFactory implements LexedTokenFactory<Token> {

        @Override
        public Token makeToken(String s, int begin, int length) {
            return new Token(s, begin, begin + length, -1);
        }
    }
}
