package edu.northwestern.websail.text.ploader;

import edu.northwestern.websail.core.pipeline.config.PEVResourceLoader;
import edu.northwestern.websail.core.pipeline.config.PEVResourceType;
import edu.northwestern.websail.core.pipeline.config.PipelineEnvironment;
import edu.northwestern.websail.text.clustering.stc.STC;

import java.util.Properties;

/**
 * @author csbhagav on 1/8/15.
 */
public class STCLoader {

    public static STC loadSuffixTreeClusters(PipelineEnvironment env, Properties config, Boolean global, Boolean put) {
        STC stc = loadSTC(config, true);
        stc.clusterBaseClusters(stc.getBaseClusters());
        if (put) {
            PEVResourceLoader.setResource(env, PEVResourceType.STC, global, stc);
        }
        return stc;
    }

    public static STC loadSuffixTreeClustersCaseSensitive(PipelineEnvironment env, Properties config, Boolean global, Boolean put) {
        STC stc = loadSTC(config, false);
        stc.clusterBaseClusters(stc.getBaseClusters());
        if (put) {
            PEVResourceLoader.setResource(env, PEVResourceType.STC_CASE, global, stc);
        }
        return stc;
    }

    public static STC loadSTC(Properties config, Boolean caseFolded) {
//        String stcObjFile = config.getProperty("text.stc.stcObj.caseSensitive");
//        if (caseFolded) {
//            stcObjFile = config.getProperty("text.stc.stcObj.caseFolded");
//        }
        return readSTCObject(caseFolded, config);
    }

    public static STC readSTCObject(Boolean caseFolded, Properties config) {


        try {
            return createSTCObjectFile(caseFolded, config);

            //TODO: Custom serialization for STC needs to be built later.
//            File objFile = new File(objectFile);
//            if (objFile.exists()) {
//                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(objFile));
//                STC stc = (STC) ois.readObject();
//                ois.close();
//                return stc;
//            } else {
//                return createSTCObjectFile(objectFile, caseFolded, config);
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static STC createSTCObjectFile(Boolean caseFolded, Properties config) throws Exception {
        String documentsDir = config.getProperty("text.stc.inputDirectory",
                "/websail/common/keyphrase_extraction/data/papers/papers-ssplit");

        STC stc = new STC();
        stc._caseFold = caseFolded;
        stc.addDocuments(documentsDir);
//        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File(objectFile)));
//        oos.writeObject(stc);
//        oos.close();
        return stc;
    }


}
