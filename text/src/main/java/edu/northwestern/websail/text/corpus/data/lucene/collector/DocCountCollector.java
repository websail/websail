package edu.northwestern.websail.text.corpus.data.lucene.collector;

import org.apache.lucene.index.AtomicReaderContext;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.Scorer;

import java.io.IOException;

/**
 * @author NorThanapon
 * @since 11/7/14
 */
public class DocCountCollector extends Collector {
    //private int base;
    private int totalDoc = 0;

    @Override
    public boolean acceptsDocsOutOfOrder() {
        return true;
    }

    @Override
    public void collect(int docId) throws IOException {

        totalDoc++;
    }

    @Override
    public void setNextReader(AtomicReaderContext context) throws IOException {
        //this.base = context.docBase;
    }

    @Override
    public void setScorer(Scorer score) throws IOException {
        //don't care
    }

    public int getTotalDoc() {
        return totalDoc;
    }

    public void setTotalDoc(int totalDoc) {
        this.totalDoc = totalDoc;
    }

}
