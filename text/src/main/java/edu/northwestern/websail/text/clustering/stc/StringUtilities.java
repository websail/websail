package edu.northwestern.websail.text.clustering.stc;

//All this code taken from David Ko's KnowItAll implementation

public class StringUtilities {

	private static boolean wroteSpace = false;
	
	 /***
	   * Insert space between words and punctuation, with special handling of several cases.
	   * Replaces html &#999; codes with printable characters or with space
	   */
	  public static synchronized String tokenize(String s1)
	  {
	    StringBuffer buf = new StringBuffer();
	    char c = ' '; // current character
	    char p; // previous character
	    char n; // next character

	    String s = s1.trim();
	    if (s.endsWith("."))
	    {
	      s = s.substring(0, s.length() - 1) + " .";
	    }
	    for (int i = 0; i < s.length(); i++)
	    {
	      p = c;
	      c = s.charAt(i);
	      if (i + 1 < s.length())
	      {
	        n = s.charAt(i + 1);
	      }
	      else
	      {
	        n = ' ';
	      }

	      if (Character.isWhitespace(c))
	      {
	        addSpace(buf);    // any whitespace becomes a space
	      }
	      else if (c == ',' && !(Character.isDigit(p) && Character.isDigit(n)))
	      {
	        addSpaceCharSpace(buf, c);     // space around , unless in number
	      }
	      else if (c == '.') {
	        if (!Character.isLetterOrDigit(n)) // number or email dot
	          addSpaceCharSpace(buf, c); // space before and after .
	        // unless embedded in number or word
	        else addChar(buf, c);
	      }
	      else if (c == ':' || c == ';' || c == '(' || c == ')' || c == '[' ||
	               c == ']' || c == '\"' || c == '+' || c == '?' || c == '*' ||
	               c == '!' || c == '%' || c == '#' || c == '/' || c == '}' || c == '{')
	      {
	        addSpaceCharSpace(buf, c);     // space around punctuation listed above
	      }
	      else if (c == '`' && p == '`') //space after intro quotes ``
	      {
	        addCharSpace(buf, c);
	      }
	      else if (c == '\'')    // apostrophe
	      {
	        if(n == 's') {
	          addSpaceChar(buf, c);       // ____'s  ->  ____ 's
	        } else if(Character.isLetter(p) && Character.isLetter(n)) {
	          addChar(buf, c);            // keep ' in O'Brien together
	        } else if (p == '\'') {   //treat '' as a single word
	          addCharSpace(buf, c);
	        } else if (n == '\'') {   //treat '' as a single word
	          addSpaceChar(buf, c);
	        }
	          else {
	          addSpaceCharSpace(buf, c);  // otherwise space before and after '
	        }
	      }
//	      else if (c == '?')    // ? as apostrophe
//	      {
//	        if(n == 's') {
//	          addSpaceChar(buf, '\'');       // ____'s  ->  ____ 's
//	        } else if(Character.isLetter(p) && Character.isLetter(n)) {
//	          addChar(buf, c);            // keep ? in urls together
//	        } else {
//	          addSpaceCharSpace(buf, c);  // otherwise space before and after ?
//	        }
//	      }

	      else if (c == '-')    // hyphen
	      {
	        if(Character.isLetter(p) && Character.isLetter(n)) {
	          addChar(buf, c);            // keep hyphenated word joined
	        } else if(p == '-' || n == '-'){   // two or more hyphens in a row
	          if(p == '-' && n == '-'){
	            addChar(buf, c);          // middle hypen, just add it
	          } else if(p != '-') {
	            addSpaceChar(buf, c);     // first hyphen, space before it
	          } else {
	            addChar(buf, c);          // last hyphen, space after it
	            addSpace(buf);
	          }
	        } else {
	          addSpaceCharSpace(buf, c);  // otherwise space before and after hyphen
	        }
	      }
	      else {
	        addChar(buf, c); // default: just add the character
	      }
	    }
	    addSpace(buf);            // space at end of sentence
	    return buf.toString();

	  }


	  private static void addSpace(StringBuffer buf)
	  {
	    if (!wroteSpace)
	    {
	      if(buf.length() > 0) {    // don't add space to start of buffer
	        buf.append(" ");
	      }
	      wroteSpace = true;
	    }
	  }

	  private static void addSpaceChar(StringBuffer buf, char c)
	  {
	    addSpace(buf);
	    addChar(buf, c);
	  }

	  private static void addCharSpace(StringBuffer buf, char c)
	  {
	    addChar(buf, c);
	    addSpace(buf);
	  }

	  private static void addSpaceCharSpace(StringBuffer buf, char c)
	  {
	    addSpace(buf);
	    addChar(buf, c);
	    addSpace(buf);
	  }

	  private static void addChar(StringBuffer buf, char c)
	  {
	    buf.append(c);
	    wroteSpace = false;
	  }

	
	public static void main(String[] args) {

		String s = "This (the string) is what I'm going to attempt to \'tokenize\'";
		String s2 = tokenize(s);
		System.out.println(s2);

	}

}
