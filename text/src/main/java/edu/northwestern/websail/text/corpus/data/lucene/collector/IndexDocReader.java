package edu.northwestern.websail.text.corpus.data.lucene.collector;

import edu.northwestern.websail.text.corpus.data.lucene.IndexAggregator;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class IndexDocReader {
    private DirectoryReader currentReader;
    private IndexSearcher searcher;

    public IndexDocReader(String indexDirectory) throws IOException {
        this.initializeReader(indexDirectory);
    }

    public static void main(String[] args) throws IOException {
        IndexDocReader iddr = new IndexDocReader(args[0]);
        TermMeasureStatCollector collector = new TermMeasureStatCollector(iddr.getCurrentReader());
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(6886);
        a.add(9959000);
        iddr.getDocuments(a, collector);
        System.out.println(collector.getTotalDocs());
        System.out.println(collector.getStat().size());
        System.out.println(collector.getStat());
    }

    private void initializeReader(String indexDirectory) throws IOException {
        File indexFile = new File(indexDirectory);
        Directory directory = FSDirectory.open(indexFile);
        currentReader = DirectoryReader.open(directory);
        searcher = new IndexSearcher(currentReader);
    }

    public void close() throws IOException {
        this.currentReader.close();
    }

    public void getDocuments(int docId, Collector collector) throws IOException {
        Query q = NumericRangeQuery.newIntRange(IndexAggregator.DOC_ID_FIELD, docId, docId, true, true);
        searcher.search(q, collector);
    }

    public void getDocuments(ArrayList<Integer> docIds, Collector collector) throws IOException {
        BooleanQuery combined = new BooleanQuery();
        for (Integer docId : docIds) {
            Query q = NumericRangeQuery.newIntRange(IndexAggregator.DOC_ID_FIELD, docId, docId, true, true);
            combined.add(q, BooleanClause.Occur.SHOULD);
        }
        searcher.search(combined, collector);
    }

    public DirectoryReader getCurrentReader() {
        return currentReader;
    }

    public void setCurrentReader(DirectoryReader currentReader) {
        this.currentReader = currentReader;
    }
}
