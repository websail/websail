package edu.northwestern.websail.text.corpus.data.lucene;

import edu.northwestern.websail.text.corpus.data.Aggregator;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;


public class IndexAggregator extends Aggregator {
    public static final String DOC_ID_FIELD = "id";
    public static final String DOC_TITLE_FIELD = "title";
    public static final String DOC_CONTENT_FIELD = "article";
    FieldType fieldType;
    IndexWriter iwriter;

    public IndexAggregator() {
        this.fieldType = getIndexfieldType();
    }

    public IndexAggregator(String stopWordsFile) throws IOException {
        super(stopWordsFile);
        this.fieldType = getIndexfieldType();
    }

    @Override
    public void initEmptyAggregator(String indexDirectory) throws IOException {
        Analyzer analyzer = this.initAnalyzer();
        File indexFile = new File(indexDirectory);
        Directory directory = FSDirectory.open(indexFile);
        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40, analyzer);
        this.iwriter = new IndexWriter(directory, config);
    }

    public void initExistingAggregator(String indexDirectory) throws IOException {
        Analyzer analyzer = this.initAnalyzer();
        File indexFile = new File(indexDirectory);
        Directory directory = FSDirectory.open(indexFile);
        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40, analyzer);
        config.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
        this.iwriter = new IndexWriter(directory, config);
    }

    @Override
    public void close() throws IOException {
        iwriter.close();
    }

    @Override
    public void addPlaintextDocument(int docId, String docTitle, String content) throws IOException, InterruptedException {
        if (content.trim().equals("")) return;
        Document doc = new Document();
        doc.add(new IntField(DOC_ID_FIELD, docId, Field.Store.YES));
        doc.add(new StringField(DOC_TITLE_FIELD, docTitle, Field.Store.YES));
        doc.add(new Field(DOC_CONTENT_FIELD, content, this.fieldType));
        this.iwriter.addDocument(doc);
        docCount++;
        if (docCount % 1000 == 0 && verbose) System.out.println("processing: " + docCount + ", skip: " + this.docSkip);
    }

    private Analyzer initAnalyzer() {
        Analyzer analyzer;
        if (this.onlyWhiteSpace) {
            analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);
        } else {
            CharArraySet stWords = new CharArraySet(Version.LUCENE_40, super.stopWords.size(), true);
            stWords.addAll(super.stopWords);
            analyzer = new StandardAnalyzer(Version.LUCENE_40, stWords);
        }
        return analyzer;
    }

    private FieldType getIndexfieldType() {
        FieldType vecType = new FieldType();
        vecType.setIndexed(true);
        vecType.setTokenized(true);
        vecType.setStored(true);
        vecType.setStoreTermVectors(true);
        vecType.setStoreTermVectorPositions(true);
        return vecType;
    }
}
