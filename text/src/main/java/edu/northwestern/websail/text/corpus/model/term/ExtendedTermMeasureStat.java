package edu.northwestern.websail.text.corpus.model.term;

/**
 * @author NorThanapon
 * @since 11/7/14
 */
public class ExtendedTermMeasureStat extends TermMeasureStat {
    private static final long serialVersionUID = -8052573077676194415L;
    private Double idf2;
    private Double rIDF2;
    private Double adapt3;
    private Double adapt4;
    private Double burstiness;
    private Integer df3;
    private Integer df4;
    private Integer df5;

    public ExtendedTermMeasureStat(String term) {
        super(term);
        this.df3 = 0;
        this.df4 = 0;
        this.df5 = 0;
    }

    public ExtendedTermMeasureStat() {
    }

    public ExtendedTermMeasureStat(String term, long tf) {
        super(term, tf);
    }

    public ExtendedTermMeasureStat(TermStat t) {
        super(t);
    }

    public ExtendedTermMeasureStat(ExtendedTermMeasureStat t) {
        super(t);
        this.df3 = t.getDf3();
        this.df4 = t.getDf4();
        this.df5 = t.getDf5m();
    }

    @Override
    public void updateDF(long docTF) {
        if (!isTFGiven) this.tf += docTF;
        if (docTF > 4) this.df5++;
        if (docTF > 3) this.df4++;
        if (docTF > 2) this.df3++;
        if (docTF > 1) this.df2++;
        if (docTF > 0) this.df1++;
    }

    public Integer getDf3() {
        return df3;
    }

    public void setDf3(Integer df3) {
        this.df3 = df3;
    }

    public Integer getDf4() {
        return df4;
    }

    public void setDf4(Integer df4) {
        this.df4 = df4;
    }

    public Integer getDf5m() {
        return df5;
    }

    public Double getAdapt3() {
        return adapt3;
    }

    public void setAdapt3(Double adapt3) {
        this.adapt3 = adapt3;
    }

    public Double getBurstiness() {
        return burstiness;
    }

    public void setBurstiness(Double burstiness) {
        this.burstiness = burstiness;
    }

    public Double getAdapt4() {
        return adapt4;
    }

    public void setAdapt4(Double adapt4) {
        this.adapt4 = adapt4;
    }

    public Double getIdf2() {
        return idf2;
    }

    public void setIdf2(Double idf2) {
        this.idf2 = idf2;
    }

    public Double getrIDF2() {
        return rIDF2;
    }

    public void setrIDF2(Double rIDF2) {
        this.rIDF2 = rIDF2;
    }

    public Integer getDf5() {
        return df5;
    }

    public void setDf5(Integer df5) {
        this.df5 = df5;
    }

}
