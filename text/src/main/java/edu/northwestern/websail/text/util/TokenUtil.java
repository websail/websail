package edu.northwestern.websail.text.util;

import com.gs.collections.impl.list.mutable.FastList;
import com.gs.collections.impl.set.mutable.UnifiedSet;
import edu.northwestern.websail.text.data.TaggedToken;
import edu.northwestern.websail.text.data.Token;
import edu.stanford.nlp.util.StringUtils;
import org.tartarus.snowball.SnowballStemmer;
import org.tartarus.snowball.ext.EnglishStemmer;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * @author NorThanapon
 * @since 11/10/14
 */
public class TokenUtil {
    public static final String NUM_TOKEN = "-n-";
    public static final String UNK_TOKEN = "-unk-";
    public static final Pattern ctrlPattern = Pattern.compile("\\p{Cc}");

    //==============================================================
    // Tokens Conversion Methods
    //==============================================================

    public static String[] tokens2StrArr(List<? extends Token> tokens) {
        return tokens2StrArr(tokens, false);
    }

    public static String[] tokens2StrArr(List<? extends Token> tokens, boolean lowerCase) {
        String[] result = new String[tokens.size()];
        for (int i = 0; i < tokens.size(); i++) {
            if (lowerCase)
                result[i] = tokens.get(i).getText().toLowerCase();
            else
                result[i] = tokens.get(i).getText();
        }
        return result;
    }

    public static String[] tokens2StrArr(List<? extends Token> tokens, String surface) {
        String[] result = new String[tokens.size()];
        int offset = 0;
        for (int i = 0; i < tokens.size(); i++) {
            if (i == 0) offset = tokens.get(i).getStartOffset();
            result[i] = surface.substring(tokens.get(i).getStartOffset() - offset, tokens.get(i).getEndOffset() - offset);
        }
        return result;
    }

    public static Set<String> tokens2StrSet(List<? extends Token> tokens) {
        Set<String> set = new UnifiedSet<String>();
        for (Token t : tokens) {
            set.add(t.getText());
        }
        return set;
    }

    public static String tokens2Str(List<? extends Token> tokens) {
        if (tokens.size() == 0) return "";
        if (tokens.size() == 1) return tokens.get(0).getText();
        StringBuilder result = new StringBuilder();
        for (Token token : tokens) {
            result.append(token.getText().replace(' ', '_').replace('\n', '_'));
            result.append(' ');
        }
        return result.substring(0, result.length() - 1);
    }

    public static String tokens2Str(List<? extends Token> tokens, String text) {
        if (tokens.size() == 0) return "";
        if (tokens.size() == 1) return text.substring(tokens.get(0).getStartOffset(), tokens.get(0).getEndOffset());
        StringBuilder result = new StringBuilder();
        for (Token token : tokens) {
            result.append(text.substring(token.getStartOffset(), token.getEndOffset()).replace(' ', '_').replace('\n', '_'));
            result.append(' ');
        }
        return result.substring(0, result.length() - 1);
    }

    public static String tokens2StrOffset(List<? extends Token> tokens) {
        if (tokens.size() == 0) return "";
        StringBuilder result = new StringBuilder();
        for (Token t : tokens) {
            int start = result.length();
            result.append(t.getText());
            int end = result.length();
            result.append(' ');
            t.setStartOffset(start);
            t.setEndOffset(end);
        }
        return result.toString();
    }

    public static List<String> tokens2StrList(List<? extends Token> tokens) {
        return Arrays.asList(tokens2StrArr(tokens));
    }

    public static String token2POSStr(List<? extends Token> tokens) throws Exception {
        StringBuilder stringBuilder = new StringBuilder();
        for (Token t : tokens) {
            if (t instanceof TaggedToken) {
                stringBuilder.append(((TaggedToken) t).getPOS()).append(' ');
            } else {
                throw new Exception("No POS information found in tag");
            }
        }
        return stringBuilder.substring(0, stringBuilder.length() - 1);
    }

    public static String[] token2POSArr(List<? extends Token> tokens) throws Exception {
        String[] tags = new String[tokens.size()];
        int i = 0;
        for (Token t : tokens) {
            if (t instanceof TaggedToken) {
                tags[i] = ((TaggedToken) t).getPOS();
                i++;
            } else {
                throw new Exception("No POS information found in tag");
            }
        }
        return tags;
    }

    public static String tokens2Regex(List<? extends Token> tokens, String text) {
        return tokens2Regex(tokens, text, false);
    }

    public static String tokens2Regex(List<? extends Token> tokens, String text,
                                      boolean resetTokenOffset) {
        return tokens2Regex(tokens, text, resetTokenOffset, false);
    }

    public static String tokens2Regex(List<? extends Token> tokens, String text,
                                      boolean resetTokenOffset, boolean lowercase) {
        if (lowercase)
            text = text.toLowerCase();
        int tokenStart = 0;
        if (resetTokenOffset)
            tokenStart = tokens.get(0).getStartOffset();
        StringBuilder sb = new StringBuilder();
        for (Token t : tokens) {
            sb.append(Pattern.quote(text.substring(t.getStartOffset()
                    - tokenStart, t.getEndOffset() - tokenStart)));
            sb.append("[\\s\\-]*");
        }
        return sb.toString();
    }

    //==============================================================
    // Token(s) Search
    //==============================================================

    public static int indexOfCharOffset(List<? extends Token> tokens, int offset) {
        int low = 0;
        int high = tokens.size();
        int medium;
        int sol = -1;
        while (low < high) {
            medium = (low + high) / 2;
            if (tokens.get(medium).getStartOffset() <= offset
                    && tokens.get(medium).getEndOffset() > offset) {
                sol = medium;
                break;
            } else {
                if (tokens.get(medium).getStartOffset() > offset) {
                    high = medium;
                } else
                    low = medium + 1;
            }
        }
        return sol;
    }

    public static int nearestIndexOfCharOffset(List<? extends Token> tokens, int offset) {
        int minDist = Integer.MAX_VALUE;
        int i = 0;
        int minIndex = -1;
        for (Token token : tokens) {
            if (token.getStartOffset() >= offset && token.getEndOffset() < offset) {
                return offset;
            }
            int dist = Math.min(Math.abs(token.getStartOffset() - offset), Math.abs(token.getEndOffset() - offset));
            if (dist < minDist) {
                minDist = dist;
                minIndex = i;
            }
            i++;
        }
        return minIndex;
    }

    public static int indexOfTokensKMP(List<? extends Token> hayStack, List<? extends Token> needles, int startOffset) {
        int[] t = kmpTable(needles);
        int m = startOffset;
        int i = 0;
        while (m + i < hayStack.size()) {
            if (needles.get(i).getText().equals(hayStack.get(m + i).getText())) {
                if (i == needles.size() - 1) {
                    return m;
                }
                i++;
            } else {
                if (t != null && t[i] > -1) {
                    m = m + i - t[i];
                    i = t[i];
                } else {
                    i = 0;
                    m++;
                }
            }
        }
        return -1;
    }

    private static int[] kmpTable(List<? extends Token> needles) {
        if (needles.size() == 0)
            return null;
        int[] t = new int[needles.size()];
        t[0] = -1;
        int pos = 2;
        int cnd = 0;
        while (pos < needles.size()) {
            if (needles.get(pos - 1).getText()
                    .equals(needles.get(cnd).getText())) {
                cnd += 1;
                t[pos] = cnd;
                pos++;
            } else if (cnd > 0) {
                cnd = t[cnd];
            } else {
                t[pos] = 0;
                pos++;
            }
        }
        return t;
    }

    //==============================================================
    // Tokens Modification Methods
    //==============================================================

    public static List<Token> normalize(List<? extends Token> tokens, boolean lowercase,
                                        boolean stem, Set<String> stopwords, SnowballStemmer stemmer) {
        FastList<Token> newTokens = new FastList<Token>();
        if(stemmer==null && stem) stemmer = new EnglishStemmer();
        for (Token t : tokens) {
            if (lowercase) {
                t.setText(t.getText().toLowerCase());
            }
            if (stopwords != null && stopwords.contains(t.getText().toLowerCase())) {
                continue;
            }
            if (stem) {
                stemmer.setCurrent(t.getText());
                stemmer.stem();
                t.setText(stemmer.getCurrent());
            }
            if (t.getText().length() > 0) {
                newTokens.add(t);
            }
        }
        return newTokens;
    }

    public static List<Token> normalize(List<? extends Token> tokens, boolean lowercase,
                                        boolean stem, Set<String> stopwords) {
       return normalize(tokens, lowercase, stem, stopwords, null);
    }

    public static List<Token> normalize(List<? extends Token> tokens, boolean lowercase,
                                        boolean stem) {
        return normalize(tokens, lowercase, stem, null, null);
    }


    public static List<Token> normalize(List<? extends Token> tokens, boolean lowercase,
                                        boolean stem, SnowballStemmer stemmer) {
        return normalize(tokens, lowercase, stem, null, stemmer);
    }

    public static List<Token> cleanTokens(List<? extends Token> tokens, boolean numeric,
                                          Set<String> vocab) {
        FastList<Token> newTokens = new FastList<Token>();
        for (Token t : tokens) {
            if (t.getText().length() == 0) continue;
            if (ctrlPattern.matcher(t.getText()).matches()) continue;
            if (numeric && StringUtils.isNumeric(t.getText()) && !t.getText().equals(".")) {
                t.setText(NUM_TOKEN);
            }
            char firstChar = t.getText().charAt(0);
            if (numeric && StringUtils.isNumeric(t.getText().substring(1)) && !t.getText().equals(".")
                    && (firstChar == '-' || firstChar == '+')) {
                t.setText(firstChar + NUM_TOKEN);
            }
            if (vocab != null && !vocab.contains(t.getText())) {
                t.setText(UNK_TOKEN);
            }
            newTokens.add(t);
        }
        return newTokens;
    }

    public static void adjustOffset(List<? extends Token> tokens, int offset) {
        for (Token tk : tokens) {
            tk.setStartOffset(tk.getStartOffset() + offset);
            tk.setEndOffset(tk.getEndOffset() + offset);
        }
    }

}
