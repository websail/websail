package edu.northwestern.websail.text.corpus.model.term;

/**
 *    @author NorThanapon
 */

import java.io.Serializable;

public class TermStat implements Serializable {

    private static final long serialVersionUID = 3417327564929523132L;
    protected String term;
    protected Integer df1;
    protected Integer df2;
    protected boolean isTFGiven;
    protected Long tf;

    public TermStat(String term) {
        this.term = term;
        this.df1 = 0;
        this.df2 = 0;
        this.isTFGiven = false;
        this.tf = 0l;
    }

    public TermStat() {
    }

    public TermStat(String term, long tf) {
        this(term);
        this.isTFGiven = true;
        this.tf = tf;
    }

    public TermStat(TermStat t) {
        this.term = t.getTerm();
        this.df1 = t.getDf1();
        this.df2 = t.getDf2();
        this.tf = t.getTf();
        this.isTFGiven = true;
    }

    public void updateDF(long docTF) {
        if (!isTFGiven) this.tf += docTF;
        if (docTF > 1) this.df2++;
        if (docTF > 0) this.df1++;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public Integer getDf1() {
        return df1;
    }

    public void setDf1(Integer df1) {
        this.df1 = df1;
    }

    public Integer getDf2() {
        return df2;
    }

    public void setDf2(Integer df2) {
        this.df2 = df2;
    }


    public Long getTf() {
        return tf;
    }

    public void setTf(Long tf) {
        this.tf = tf;
    }

    public boolean isTFGiven() {
        return isTFGiven;
    }

    public void setTFGiven(boolean isTFGiven) {
        this.isTFGiven = isTFGiven;
    }

}
