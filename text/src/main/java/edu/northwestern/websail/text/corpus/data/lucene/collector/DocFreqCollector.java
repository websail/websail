package edu.northwestern.websail.text.corpus.data.lucene.collector;

import edu.northwestern.websail.text.corpus.data.lucene.IndexAggregator;
import org.apache.lucene.index.AtomicReaderContext;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.util.HashMap;

/**
 * @author NorThanapon
 * @since 11/7/14
 */
public class DocFreqCollector extends Collector {
    private final DirectoryReader reader;
    private final HashMap<String, Integer> df = new HashMap<String, Integer>();
    private int totalDocs = 0;
    private int base = 0;
    private long totalCount = 0;

    public DocFreqCollector(DirectoryReader reader) {
        this.reader = reader;
    }

    @Override
    public boolean acceptsDocsOutOfOrder() {
        return true;
    }

    @Override
    public void collect(int docId) throws IOException {
        totalDocs++;
        Terms vector = this.reader.getTermVector(docId + base, IndexAggregator.DOC_CONTENT_FIELD);
        if (vector == null) return;
        TermsEnum termsEnum = vector.iterator(null);
        BytesRef text;
        while ((text = termsEnum.next()) != null) {
            String term = text.utf8ToString();
            int docF = 1;
            totalCount += docF;
            if (this.df.containsKey(term)) docF += this.df.get(term);
            df.put(term, docF);
        }
    }

    @Override
    public void setNextReader(AtomicReaderContext context) throws IOException {
        this.base = context.docBase;
    }

    @Override
    public void setScorer(Scorer score) throws IOException {

    }

    public int getTotalDocs() {
        return totalDocs;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public HashMap<String, Integer> getDF() {
        return df;
    }


}
