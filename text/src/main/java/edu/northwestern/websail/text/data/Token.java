package edu.northwestern.websail.text.data;

/**
 * @author NorThanapon
 */

import edu.stanford.nlp.ling.HasWord;

import java.io.Serializable;

public class Token implements HasWord, Serializable {

    public static final Token START = new Token("<s>", -1, -1, -1);
    public static final Token END = new Token("</s>", -1, -1, -1);
    private static final long serialVersionUID = -8977067965522627662L;
    private String text;
    private int startOffset;
    private int endOffset;
    private int index;
    private int localIndex = 0;

    public Token() {
    }

    public Token(String text, int start, int end, int index) {
        this.startOffset = start;
        this.endOffset = end;
        this.text = text;
        this.setIndex(index);
    }

    public int getLocalIndex() {
        return localIndex;
    }

    public void setLocalIndex(int localIndex) {
        this.localIndex = localIndex;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getStartOffset() {
        return startOffset;
    }

    public void setStartOffset(int startOffset) {
        this.startOffset = startOffset;
    }

    public int getEndOffset() {
        return endOffset;
    }

    public void setEndOffset(int endOffset) {
        this.endOffset = endOffset;
    }

    @Override
    public String toString() {
        return this.text;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o instanceof Token) {
            Token o2 = (Token) o;
            return this.getText().equalsIgnoreCase(o2.getText()) && this.getStartOffset() == o2.getStartOffset() && this
                    .getEndOffset() == o2.getEndOffset();
        }
        return false;
    }


    @Override
    public String word() {
        return this.getText();
    }

    @Override
    public void setWord(String s) {
        this.setText(s);
    }
}
