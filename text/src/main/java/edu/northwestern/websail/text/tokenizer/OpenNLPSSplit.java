package edu.northwestern.websail.text.tokenizer;

import com.gs.collections.impl.list.mutable.FastList;
import edu.northwestern.websail.text.data.TaggedToken;
import edu.northwestern.websail.text.data.TokenSpan;
import edu.northwestern.websail.text.util.OpenNLPModels;
import edu.northwestern.websail.text.util.TokenUtil;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.util.Span;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author NorThanapon
 * @since 11/10/14
 */
public class OpenNLPSSplit extends SentenceSpliter {
    TokenizerME tokenizer;
    SentenceDetectorME sSplit;
    POSTaggerME posTagger;

    public OpenNLPSSplit() throws IOException {
        this(false);
    }

    public OpenNLPSSplit(Boolean posEnabled) throws IOException {
        super(posEnabled);
        tokenizer = new TokenizerME(OpenNLPModels.getTokenizerModel());
        sSplit = new SentenceDetectorME(OpenNLPModels.getSentenceModel());
        if (posEnabled) {
            posTagger = new POSTaggerME(OpenNLPModels.getPOSModel());
        }
    }

    @Override
    public List<TokenSpan> sentenceSplit(String text) {
        List<TokenSpan> sentences = new FastList<TokenSpan>();
        if (text == null) return sentences;
        Span[] tSpans = tokenizer.tokenizePos(text);
        Span[] sSpans = sSplit.sentPosDetect(text);
        int curToken = 0;
        int iSen = 0;
        for (Span sSpan : sSpans) {
            int i = 0;
            ArrayList<TaggedToken> senTokens = new ArrayList<TaggedToken>();
            for (int iToken = curToken; iToken < tSpans.length; iToken++) {
                Span tSpan = tSpans[iToken];
                if (tSpan.getEnd() > sSpan.getEnd()) {
                    break;
                }
                TaggedToken t = new TaggedToken(tSpan.getCoveredText(text).toString(), tSpan.getStart(), tSpan.getEnd(), i, null, null);
                i++;
                senTokens.add(t);
                curToken++;
            }
            TokenSpan sentence = new TokenSpan(sSpan.getStart(), sSpan.getEnd(), senTokens, iSen);
            iSen++;
            sentences.add(sentence);
            if (posEnabled) {
                this.addPOS(sentence);
            }
        }
        return sentences;
    }

    private void addPOS(TokenSpan span) {
        String[] sentenceArr = TokenUtil.tokens2StrArr(span.getTokens());
        String[] tags = posTagger.tag(sentenceArr);
        for (int i = 0; i < span.getTokens().size(); i++) {
            TaggedToken t = (TaggedToken) span.getTokens().get(i);
            t.setPOS(tags[i]);
        }
    }
}
