package edu.northwestern.websail.text.topics;

import cc.mallet.extract.StringSpan;
import cc.mallet.extract.StringTokenization;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.TokenSequence2FeatureSequence;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.Alphabet;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import com.gs.collections.impl.list.mutable.FastList;
import com.gs.collections.impl.map.mutable.UnifiedMap;
import com.gs.collections.impl.set.mutable.UnifiedSet;
import edu.northwestern.websail.text.data.Token;
import edu.northwestern.websail.text.tokenizer.SUPTBTokenizer;
import edu.northwestern.websail.text.tokenizer.Tokenizer;
import edu.northwestern.websail.text.util.StopWords;
import edu.northwestern.websail.text.util.TokenUtil;
import org.tartarus.snowball.ext.EnglishStemmer;

import java.io.*;
import java.util.List;
import java.util.Set;

/**
 * @author NorThanapon
 * @since 7/17/15
 */
public class MalletHelper {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
//        ArrayList<Pipe> pipeList = new ArrayList<Pipe>();
//
//        // Pipes: lowercase, tokenize, remove stopwords, map to features
//        pipeList.add( new CharSequenceLowercase() );
//        pipeList.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")) );
//        pipeList.add( new TokenSequenceRemoveStopwords(new File("stoplists/en.txt"), "UTF-8", false, false, false) );
//        pipeList.add( new TokenSequence2FeatureSequence() );
//
//        InstanceList instances = new InstanceList (new SerialPipes(pipeList));
//
//        Reader fileReader = new InputStreamReader(new FileInputStream(new File(args[0])), "UTF-8");
//        instances.addThruPipe(new CsvIterator(fileReader, Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"),
//                3, 2, 1)); // data, label, name fields
//
//        // Create a model with 100 topics, alpha_t = 0.01, beta_w = 0.01
//        //  Note that the first parameter is passed as the sum over topics, while
//        //  the second is the parameter for a single dimension of the Dirichlet prior.
//        int numTopics = 100;
//        ParallelTopicModel model = new ParallelTopicModel(numTopics, 1.0, 0.01);
//
//        model.addInstances(instances);
//
//        // Use two parallel samplers, which each look at one half the corpus and combine
//        //  statistics after every iteration.
//        model.setNumThreads(2);
//
//        // Run the model for 50 iterations and stop (this is for testing only,
//        //  for real applications, use 1000 to 2000 iterations)
//        model.setNumIterations(50);
//        model.estimate();
        SUPTBTokenizer tokenizer = new SUPTBTokenizer();
        FastList<Pipe> pipeList = new FastList<Pipe>();
        PreprocessText pipe = new PreprocessText(tokenizer, true, true, true);
        TokenSequence2FeatureSequence ts2fPipe = new TokenSequence2FeatureSequence();
        pipeList.add(pipe);
        pipeList.add(ts2fPipe);
        ObjectOutputStream oss = new ObjectOutputStream(new FileOutputStream("/Users/NorThanapon/Desktop/mallet.dict"));
        oss.writeObject(ts2fPipe.getDataAlphabet());
        oss.close();
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("/Users/NorThanapon/Desktop/mallet.dict"));
        Alphabet alpabet = (Alphabet) ois.readObject();
        ois.close();
        InstanceList instances = new InstanceList(new SerialPipes(pipeList));
        instances.addThruPipe(makeInstance("Hello World. My name is Nor.", "a"));
        instances.addThruPipe(makeInstance("LDA is the best model.", "b"));
        instances.addThruPipe(makeInstance("I am using mallet to build topic models.", "c"));
        for (Instance inst : instances) {
            System.out.println(inst.getData());
        }
        ParallelTopicModel model = new ParallelTopicModel(2, 1.0, 0.01);
        model.addInstances(instances);
        model.setNumIterations(50);
        model.estimate();
        oss = new ObjectOutputStream(new FileOutputStream("/Users/NorThanapon/Desktop/mallet.model"));
        oss.writeObject(model);
        oss.close();
        ois = new ObjectInputStream(new FileInputStream("/Users/NorThanapon/Desktop/mallet.model"));
        ParallelTopicModel model2 = (ParallelTopicModel) ois.readObject();
        ois.close();
        FastList<Pipe> pipeArrayList = new FastList<Pipe>();
        pipeArrayList.add(new PreprocessText(tokenizer, true, true, true));
        pipeArrayList.add(new TokenSequence2FeatureSequence(alpabet));
        InstanceList test = new InstanceList(new SerialPipes(pipeArrayList));
        test.addThruPipe(makeInstance("This is our model.", "test"));
        for (Instance inst : test) {
            System.out.println(inst.getData());
        }
        TopicInferencer inferencer = model2.getInferencer();
        double[] testProbabilities = inferencer.getSampledDistribution(test.get(0), 10, 1, 5);
        System.out.println("0\t" + testProbabilities[0]);
        System.out.println("1\t" + testProbabilities[1]);

        inferencer = model.getInferencer();
        testProbabilities = inferencer.getSampledDistribution(test.get(0), 10, 1, 5);
        System.out.println("0\t" + testProbabilities[0]);
        System.out.println("1\t" + testProbabilities[1]);

        oss = new ObjectOutputStream(new FileOutputStream("/Users/NorThanapon/Desktop/mallet.infer"));
        oss.writeObject(inferencer);
        oss.close();
        ois = new ObjectInputStream(new FileInputStream("/Users/NorThanapon/Desktop/mallet.infer"));
        inferencer = (TopicInferencer) ois.readObject();
        testProbabilities = inferencer.getSampledDistribution(test.get(0), 10, 1, 5);
        System.out.println("0\t" + testProbabilities[0]);
        System.out.println("1\t" + testProbabilities[1]);


    }

    public static Instance makeInstance(String text, String docId) {
        return new Instance(text, null, docId, null);
    }

    public static FastList<Pipe> makeBasicPipeList() {
        SUPTBTokenizer tokenizer = new SUPTBTokenizer();
        FastList<Pipe> pipeList = new FastList<Pipe>();
        PreprocessText pipe = new PreprocessText(tokenizer, true, true, true);
        TokenSequence2FeatureSequence ts2fPipe = new TokenSequence2FeatureSequence();
        pipeList.add(pipe);
        pipeList.add(ts2fPipe);
        return pipeList;
    }

    public static FastList<Pipe> makeDataReadyPipeList() {
        FastList<Pipe> pipeList = new FastList<Pipe>();
        UnifiedSet<String> stopwords = new UnifiedSet<String>();
        EnglishStemmer stemmer = new EnglishStemmer();
        for (String st : StopWords._words) {
            stemmer.setCurrent(st);
            stemmer.stem();
            stopwords.add(stemmer.getCurrent());
        }
        WhiteSpaceTokenizer pipe = new WhiteSpaceTokenizer(stopwords);
        TokenSequence2FeatureSequence ts2fPipe = new TokenSequence2FeatureSequence();
        pipeList.add(pipe);
        pipeList.add(ts2fPipe);
        return pipeList;
    }


    public static void saveTopicInferencer(TopicInferencer inferencer, FastList<Pipe> pipes, String filename) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename));
        oos.writeInt(pipes.size());
        for (Pipe p : pipes) {
            oos.writeObject(p);
        }
        oos.writeObject(inferencer);
        oos.close();
    }

    public static MalletInferencerWrapper loadTopicInferencer(String filename) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename));
        FastList<Pipe> pipes = new FastList<Pipe>();
        int numPipes = ois.readInt();
        for (int i = 0; i < numPipes; i++) {
            pipes.add((Pipe) ois.readObject());
        }
        TopicInferencer inferencer = (TopicInferencer) ois.readObject();
        return new MalletInferencerWrapper(inferencer, pipes);
    }

    public static void replacePreprocessTextPipe(FastList<Pipe> pipes, Pipe replacement) {
        int preIndex = -1;
        for (int i = 0; i < pipes.size(); i++) {
            Pipe p = pipes.get(i);
            if (p.getClass() == PreprocessText.class) {
                preIndex = i;
            }
        }
        if (preIndex != -1) {
            pipes.set(preIndex, replacement);
        }
    }

    public static void saveTopicDistributions(ParallelTopicModel model, String filename, FastList<String> docKeys) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename));
        oos.writeInt(docKeys.size());
        oos.writeInt(model.getNumTopics());
        for (int i = 0; i < docKeys.size(); i++) {
            String docId = docKeys.get(i);
            double[] topics = model.getTopicProbabilities(i);
            oos.writeObject(docId);
            for (double d : topics) {
                oos.writeDouble(d);
            }
        }
        oos.close();
    }

    public static UnifiedMap<String, double[]> readTopicDistributionMap(String filename) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename));
        int docSize = ois.readInt();
        int numTopics = ois.readInt();
        UnifiedMap<String, double[]> topicMap = new UnifiedMap<String, double[]>(docSize);
        for (int i = 0; i < docSize; i++) {
            String docId = (String) ois.readObject();
            double[] topics = new double[numTopics];
            for (int j = 0; j < numTopics; j++) {
                topics[j] = ois.readDouble();
            }
            topicMap.put(docId, topics);
        }
        ois.close();
        return topicMap;
    }

    public static class PreprocessText extends Pipe implements Serializable {

        private static final long serialVersionUID = 5257280494429618235L;
        private Tokenizer tokenizer;
        private boolean stem = false;
        private boolean lowercase = false;
        private boolean stopwords = false;

        @Deprecated
        public PreprocessText() {
        }

        public PreprocessText(Tokenizer tokenizer) {
            this.tokenizer = tokenizer;
        }

        public PreprocessText(Tokenizer tokenizer, boolean stem, boolean lowercase, boolean stopwords) {
            this.tokenizer = tokenizer;
            this.stem = stem;
            this.lowercase = lowercase;
            this.stopwords = stopwords;
        }

        public Instance pipe(Instance carrier) {
            CharSequence string = (CharSequence) carrier.getData();
            tokenizer.initialize(string.toString());
            List<Token> tokens = tokenizer.getAllTokens();
            Set<String> stopwordSet = null;
            if (stopwords) stopwordSet = StopWords._words;
            if (stem || lowercase || stopwords) {
                tokens = TokenUtil.normalize(tokens, lowercase, stem, stopwordSet);
                string = TokenUtil.tokens2StrOffset(tokens);
            }
            StringTokenization ts = new StringTokenization(string);
            for (Token t : tokens) {
                ts.add(new StringSpan(string, t.getStartOffset(), t.getStartOffset() + t.getText().length()));
            }
            carrier.setData(ts);
            return carrier;
        }

        private void writeObject(ObjectOutputStream out) throws IOException {
            out.writeBoolean(stem);
            out.writeBoolean(lowercase);
            out.writeBoolean(stopwords);
            out.writeObject(tokenizer.getClass().getName());
        }

        private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
            stem = in.readBoolean();
            lowercase = in.readBoolean();
            stopwords = in.readBoolean();
            Class<?> clazz = Class.forName((String) in.readObject());
            try {
                this.tokenizer = (Tokenizer) clazz.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public static class WhiteSpaceTokenizer extends Pipe implements Serializable {

        private static final long serialVersionUID = 8140812048716145434L;

        Set<String> stopwords;

        public WhiteSpaceTokenizer() {
        }

        public WhiteSpaceTokenizer(Set<String> stopwords) {
            this.stopwords = stopwords;
        }

        public Instance pipe(Instance carrier) {
            CharSequence string = (CharSequence) carrier.getData();
            StringTokenization ts = new StringTokenization(string);
            int start = 0;
            for (int i = 0; i < string.length(); i++) {
                char c = string.charAt(i);
                if ((c == ' ' || c == '\t' || c == '\n')
                        && start != -1) {
                    String word = (String) string.subSequence(start, i);
                    if (stopwords != null && !stopwords.contains(word)) {
                        ts.add(new StringSpan(string, start, i));
                    }
                    start = -1;
                } else if (start == -1) {
                    start = i;
                }
            }
            if (string.length() > 0 && start != -1) {
                String word = (String) string.subSequence(start, string.length());
                if (stopwords != null && !stopwords.contains(word)) {
                    ts.add(new StringSpan(string, start, string.length()));
                }
            }

            carrier.setData(ts);
            return carrier;
        }
    }
}
