package edu.northwestern.websail.text.chunker;

import edu.northwestern.websail.text.data.TokenSpan;

public interface NounPhraseFoundDelegate {
    void found(TokenSpan nounPhrase) throws Exception;

    void foundSub(TokenSpan cover, TokenSpan nounPhrase);

    void setSentence(TokenSpan sentence);
}
