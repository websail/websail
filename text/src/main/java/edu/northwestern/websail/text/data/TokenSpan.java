package edu.northwestern.websail.text.data;

/**
 * @author NorThanapon
 */

import java.util.Comparator;
import java.util.List;

public class TokenSpan {

    private final int index;
    private int startOffset;
    private int endOffset;
    private List<? extends Token> tokens;

    public TokenSpan(int startOffset, int endOffset, List<? extends Token> tokens, int index) {
        this.startOffset = startOffset;
        this.endOffset = endOffset;
        this.tokens = tokens;
        this.index = index;
    }

    public static TokenSpan copyOfRange(TokenSpan origin, int from, int to) {
        Token start = origin.tokens.get(from);
        Token end = origin.tokens.get(to - 1);
        List<? extends Token> tokens = origin.tokens.subList(from, to);
        return new TokenSpan(start.getStartOffset(), end.getEndOffset(), tokens, origin.index);
    }

    public int getStartOffset() {
        return startOffset;
    }

    public void setStartOffset(int startOffset) {
        this.startOffset = startOffset;
    }

    public int getEndOffset() {
        return endOffset;
    }

    public void setEndOffset(int endOffset) {
        this.endOffset = endOffset;
    }

    public List<? extends Token> getTokens() {
        return tokens;
    }

    public void setTokens(List<Token> tokens) {
        this.tokens = tokens;
    }

    public int getIndex() {
        return index;
    }

    public TokenSpan subSpan(int start, int end) {
        if (start >= end)
            return null;
        List<? extends Token> subTokens = this.getTokens().subList(start, end);
        return new TokenSpan(subTokens.get(0).getStartOffset(), subTokens
                .get(end - start - 1).getEndOffset(), subTokens, 0);
    }

    public String toString() {
        return this.getTokens().toString();
    }

    public static class OffsetComarator implements Comparator<TokenSpan> {

        @Override
        public int compare(TokenSpan arg0, TokenSpan arg1) {
            if (arg0.getStartOffset() == arg1.getStartOffset()) {
                return ((Integer) arg0.getEndOffset()).compareTo(arg1.getEndOffset());
            } else {
                return ((Integer) arg0.getStartOffset()).compareTo(arg1.getStartOffset());
            }
        }

    }
}
