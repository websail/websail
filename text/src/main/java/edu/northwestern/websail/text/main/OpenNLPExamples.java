package edu.northwestern.websail.text.main;

import edu.northwestern.websail.text.chunker.Chunker;
import edu.northwestern.websail.text.chunker.OpenNLPShallowChunker;
import edu.northwestern.websail.text.tokenizer.OpenNLPSSplit;
import edu.northwestern.websail.text.tokenizer.OpenNLPTokenizer;
import edu.northwestern.websail.text.util.TokenUtil;

/**
 * @author NorThanapon
 * @since 11/10/14
 */
public class OpenNLPExamples {
    public static void main(String[] args) throws Exception {
        OpenNLPTokenizer tokenizer = new OpenNLPTokenizer();
        tokenizer.setToLower(true);
        System.out.println("Tokenizer: ");
        String text = "Hi. My name is Thanapon Noraset.";
        tokenizer.initialize(text);
        System.out.println(TokenUtil.indexOfCharOffset(tokenizer.getAllTokens(), 2));
        System.out.println(TokenUtil.indexOfCharOffset(tokenizer.getAllTokens(), 3));
        System.out.println(TokenUtil.indexOfCharOffset(tokenizer.getAllTokens(), 4));
        System.out.println(TokenUtil.indexOfCharOffset(tokenizer.getAllTokens(), text.length()));
        System.out.println(tokenizer.getAllTokens());

        OpenNLPSSplit sSplit = new OpenNLPSSplit(true);
        System.out.println("Sentence Split:");
        System.out.println(sSplit.sentenceSplit("Open Information Extraction Using Wikipedia"));

        OpenNLPShallowChunker chunker = new OpenNLPShallowChunker();
        System.out.println("Chunker:");
        chunker.initialize("combsum and combmnz strategy", "");
        chunker.extractNounPhrase(new Chunker.PrintNounPhraseDelegate());

        String text2 = "The standardization of grids based on web " +
                "services has resulted in the need for scalable web service " +
                "discovery mechanisms to be deployed in grids.";
        System.out.println("Chunker:");
        chunker.initialize(text2, "");
        chunker.extractNounPhrase(new Chunker.PrintNounPhraseDelegate());

    }
}
