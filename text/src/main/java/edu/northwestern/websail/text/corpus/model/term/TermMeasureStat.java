package edu.northwestern.websail.text.corpus.model.term;

/**
 * @author NorThanapon
 */

public class TermMeasureStat extends TermStat {

    private static final long serialVersionUID = -8052573077676294615L;
    private Double idf;
    private Double rIDF;
    private Double adapt2;

    public TermMeasureStat(String term) {
        super(term);
    }

    public TermMeasureStat() {
    }

    public TermMeasureStat(String term, long tf) {
        super(term, tf);
    }

    public TermMeasureStat(TermStat t) {
        super(t);
    }

    public Double getIdf() {
        return idf;
    }

    public void setIdf(Double idf) {
        this.idf = idf;
    }

    public Double getrIDF() {
        return rIDF;
    }

    public void setrIDF(Double rIDF) {
        this.rIDF = rIDF;
    }

    public Double getAdapt2() {
        return adapt2;
    }

    public void setAdapt2(Double adapt2) {
        this.adapt2 = adapt2;
    }

    public String toString() {
        return this.getTerm() + " (" + this.getTf() + "/" + this.getIdf() + ")";
    }

}
