package edu.northwestern.websail.text.topics;

import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.InstanceList;
import com.gs.collections.impl.list.mutable.FastList;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @author NorThanapon
 * @since 7/20/15
 */
public class TestMalletWrapper {

    @Test
    public void testBasicPipeline() {
        FastList<Pipe> pipeList = MalletHelper.makeBasicPipeList();
        InstanceList instances = new InstanceList(new SerialPipes(pipeList));
        instances.addThruPipe(MalletHelper.makeInstance("Hello World. My name is Nor.", "a"));
        instances.addThruPipe(MalletHelper.makeInstance("LDA is the best model.", "b"));
        instances.addThruPipe(MalletHelper.makeInstance("I am using mallet to build topic models.", "c"));
        ParallelTopicModel.logger.setLevel(Level.SEVERE);
        ParallelTopicModel model = new ParallelTopicModel(2, 1.0, 0.01);
        model.addInstances(instances);
        model.setNumIterations(50);
        try {
            model.estimate();
        } catch (IOException e) {
            fail(e.getMessage());
        }
        InstanceList test = new InstanceList(instances.getPipe());
        test.addThruPipe(MalletHelper.makeInstance("This is our model.", "test"));
        TopicInferencer inferencer = model.getInferencer();
        double[] testProbabilities = inferencer.getSampledDistribution(test.get(0), 10, 1, 5);
        assertTrue("Topic distribution is not uniform.", testProbabilities[0] != testProbabilities[1]);
        assertTrue("Topic distribution sums to 1.", testProbabilities[0] + testProbabilities[1] == 1);
    }

    @Test
    public void testSerialization() {
        FastList<Pipe> pipeList = MalletHelper.makeBasicPipeList();
        InstanceList instances = new InstanceList(new SerialPipes(pipeList));
        instances.addThruPipe(MalletHelper.makeInstance("Hello World. My name is Nor.", "a"));
        instances.addThruPipe(MalletHelper.makeInstance("LDA is the best model.", "b"));
        instances.addThruPipe(MalletHelper.makeInstance("I am using mallet to build topic models.", "c"));
        ParallelTopicModel.logger.setLevel(Level.SEVERE);
        ParallelTopicModel model = new ParallelTopicModel(2, 1.0, 0.01);
        model.addInstances(instances);
        model.setNumIterations(50);
        try {
            model.estimate();
        } catch (IOException e) {
            fail(e.getMessage());
        }
        InstanceList test = new InstanceList(instances.getPipe());
        test.addThruPipe(MalletHelper.makeInstance("This is our model.", "test"));
        TopicInferencer inferencer = model.getInferencer();
        double[] testProbabilities = inferencer.getSampledDistribution(test.get(0), 1000, 1, 50);
        try {
            MalletHelper.saveTopicInferencer(inferencer, pipeList, "temp_mallet.model");
        } catch (IOException e) {
            fail(e.getMessage());
        }
        File f = new File("temp_mallet.model");
        if (!f.exists()) {
            fail("Mallet model file does not exists.");
            return;
        }
        try {
            MalletInferencerWrapper wrapper = MalletHelper.loadTopicInferencer("temp_mallet.model");
            InstanceList test2 = new InstanceList(new SerialPipes(wrapper.getPipes()));
            test2.addThruPipe(MalletHelper.makeInstance("This is our model.", "test"));
            double[] loadProbs = wrapper.getInferencer().getSampledDistribution(test2.get(0), 1000, 1, 50);
            assertTrue("Topic distribution of serialized model is the same.", Math.abs(testProbabilities[0] - loadProbs[0]) < 10e-3);
            assertTrue("Topic distribution of serialized model is the same.", Math.abs(testProbabilities[1] - loadProbs[1]) < 10e-3);
        } catch (Exception e) {
            fail(e.getMessage());
        }
        if (f.exists()) {
            f.deleteOnExit();
        }
    }
}
