package edu.northwestern.websail.core.ws.model;

/**
 * @author csbhagav on 2/4/15.
 */
public class Response {
    private Integer status;
    private Object response;
    private String message;

    public Response(Integer status, Object response, String message) {
        this.status = status;
        this.response = response;
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }
    public Object getResponse() {
        return response;
    }
    public String getMessage() {
        return message;
    }
    public void setStatus(Integer status) {
        this.status = status;
    }
    public void setResponse(Object response) {
        this.response = response;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}