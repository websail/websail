package edu.northwestern.websail.core.ws.model;

import edu.northwestern.websail.core.ws.model.Response;

/**
 * @author csbhagav on 2/4/15.
 */
public class ResponseFactory {
    public static Response ok(Object response) {
        return new Response(200, response, "OK");
    }

    public static Response notFound(String message) {
        return new Response(404, null, message);
    }

    public static Response error(String message) {
        return new Response(500, null, message);
    }
}
