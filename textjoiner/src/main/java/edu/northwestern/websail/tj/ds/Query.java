package edu.northwestern.websail.tj.ds;


import edu.northwestern.websail.text.ngram.ARPALMWrapper;

import java.util.*;

/**
 * @author csbhagav on 9/15/14.
 */
public class Query {
    public static final int VARIABLE_ID = -1108;
    public static final int WILDCARD_ID = -86;
    public Integer[] variablePositions;
    String query;
    List<String> queryWords;
    final QueryORGroup orGroup;
    Set<String> variablesSet;
    int[] ngram;

    public Query(String query, QueryORGroup orGroup) {
        this.query = query;
        this.queryWords = Arrays.asList(this.query.split(" "));
        this.variablesSet = new HashSet<String>();
        this.variablePositions = getVariablePositions(this.queryWords);
        this.orGroup = orGroup;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public List<String> getQueryWords() {
        return queryWords;
    }

    public void setQueryWords(List<String> queryWords) {
        this.queryWords = queryWords;
    }

    public Integer getNumVariables() {
        return this.variablePositions.length;
    }

    public Integer[] getVariablePositions() {
        return variablePositions;
    }

    public void setVariablePositions(Integer[] variablePositions) {
        this.variablePositions = variablePositions;
    }

    public Set<String> getVariablesSet() {
        return variablesSet;
    }

    public void setVariablesSet(Set<String> variablesSet) {
        this.variablesSet = variablesSet;
    }

    public int[] getNgramCopy() {
        int[] newNgram = new int[ngram.length];
        System.arraycopy(ngram, 0, newNgram, 0, ngram.length);
        return newNgram;
    }

    public int[] getNgram(int[] queryNgram, boolean reverse) {
        if (!reverse) {
            return queryNgram;
        }

        int[] revNGram = new int[queryNgram.length];
        for (int i = 0; i < queryNgram.length; i++) {
            revNGram[i] = queryNgram[queryNgram.length - 1 - i];
        }
        return revNGram;
    }

    public void setNgram(int[] ngram) {
        this.ngram = ngram;
    }

    public Integer[] getVariablePositions(List<String> query) {
        ArrayList<Integer> positions = new ArrayList<Integer>();
        for (int i = 0; i < query.size(); i++) {
            if (query.get(i).startsWith("$")) {
                positions.add(i);
                variablesSet.add(query.get(i).trim());
            }
        }
        return positions.toArray(new Integer[positions.size()]);
    }

    public void setQueryNgramArr(ARPALMWrapper lmWrapper) {
        ngram = new int[queryWords.size()];
        for (int i = 0; i < queryWords.size(); i++) {
            if (queryWords.get(i).startsWith("$")) {
                ngram[i] = VARIABLE_ID;
            } else if (queryWords.get(i).equalsIgnoreCase("*")) {
                ngram[i] = WILDCARD_ID;
            } else {
                ngram[i] = lmWrapper.getWordIndexer().getIndexPossiblyUnk(queryWords.get(i));
            }
        }
    }

    public int[] getAssignmentsNgram(ARPALMWrapper lmWrapper, VariableAssignment assignment) {
        int[] newAssignment = new int[ngram.length];
        for (int i = 0; i < ngram.length; i++) {
            if (ngram[i] == Query.VARIABLE_ID) {
                String word = assignment.getVariableValue(queryWords.get(i));
                newAssignment[i] = lmWrapper.getWordIndexer().getIndexPossiblyUnk(word);
            } else {
                newAssignment[i] = ngram[i];
            }
        }
        return newAssignment;
    }


}
