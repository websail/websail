package edu.northwestern.websail.tj.ds;

/**
 * @author csbhagav on 10/3/14.
 */
public enum TJScorerType {
    CLASSIC_SCORE,
    LAST_QUERY_SCORE,
    RANK_PRODUCT_SCORE
}
