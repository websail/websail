package edu.northwestern.websail.tj.main;

import edu.berkeley.nlp.lm.map.NgramMap;
import edu.berkeley.nlp.lm.util.StrUtils;
import edu.berkeley.nlp.lm.values.ProbBackoffPair;
import edu.northwestern.websail.datastructure.trie.ds.Node;
import edu.northwestern.websail.datastructure.trie.ds.Trie;
import edu.northwestern.websail.text.ngram.ARPALMWrapper;
import org.apache.commons.lang.ArrayUtils;

import java.io.*;
import java.util.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @author csbhagav on 9/10/14.
 */
public class NgramTrie {

    ARPALMWrapper arpalmWrapper;
    Trie<Integer, Integer, Void> trie;

    public NgramTrie(ARPALMWrapper arpalmWrapper, String trieFile) throws IOException {
        this.arpalmWrapper = arpalmWrapper;

        File f = new File(trieFile);
        if (f.exists()) {
            readTrie(f);
            System.out.println("# of nodes = " + this.trie.getNumNodes());
        } else {
            createTrie(f);
        }
    }

    private void readTrie(File trieSerializedFile) throws IOException {
        GZIPInputStream zip = new GZIPInputStream(new FileInputStream(trieSerializedFile));
        this.trie = NGramTrieSerializer.readFromFile(new BufferedReader(new InputStreamReader(zip, "UTF-8")));
    }

    private void createTrie(File trieSerializedFile) throws IOException {
        System.out.println("Loading trie ... ");
        int numWords = arpalmWrapper.getLm().getWordIndexer().numWords();
        System.out.println("num words  = " + numWords);

        HashSet<Integer> pageIdWords = new HashSet<Integer>(4000000);
        for (int i = 0; i < arpalmWrapper.getWordIndexer().numWords(); i++) {
            final String word = arpalmWrapper.getWordIndexer().getWord(i);
            if (word.startsWith("page_id_")) {
                pageIdWords.add(i);
            }
        }
        Trie<Integer, Integer, Void> trie = new Trie<Integer, Integer, Void>();
        int order = arpalmWrapper.getLm().getLmOrder();
        int numUseFulNgrams = 0;
        for (int i = 0; i < order; i++) {
            System.out.println("\nOrder = " + i);
            for (NgramMap.Entry<ProbBackoffPair> obj : arpalmWrapper.getLm().getNgramMap().getNgramsForOrder(i)) {
                Integer[] ngram = ArrayUtils.toObject(obj.key);
                if (containsIdWord(ngram, pageIdWords)) {
                    trie.insertNodes(ngram, ngram);
                    numUseFulNgrams++;
                }
            }
        }

        this.trie = trie;
        GZIPOutputStream zip = new GZIPOutputStream(new FileOutputStream(trieSerializedFile));
        NGramTrieSerializer.writeToFile(new BufferedWriter(new OutputStreamWriter(zip, "UTF-8")), this);
        zip.close();
        System.out.println("Done loading trie ...: " + trie.getNumNodes());
        System.out.println("Useful ngrams = " + numUseFulNgrams);
    }

    private boolean containsIdWord(Integer[] ngram, HashSet<Integer> pageIdWords) {
        for (Integer id : ngram) {
            if (pageIdWords.contains(id)) {
                return true;
            }
        }
        return false;
    }

    public Trie<Integer, Integer, Void> getTrie() {
        return trie;
    }

    public ARPALMWrapper getArpalmWrapper() {
        return arpalmWrapper;
    }

    public void setArpalmWrapper(ARPALMWrapper arpalmWrapper) {
        this.arpalmWrapper = arpalmWrapper;
    }

    public HashSet<Integer> getRelevantIds(List<String> words) {
        System.out.println("Trying trie traversal ... ");
        HashSet<Integer> relevantIds = new HashSet<Integer>();
        Node<Integer, Integer, Void> root = this.trie.getRoot();
        traverse(root, words, 0, relevantIds);
        System.out.println(StrUtils.join(words) + " : trie traverse. Relevant IDs:" + relevantIds.size());
        return relevantIds;
    }

    private boolean traverse(Node<Integer, Integer, Void> root, List<String> words, int idx,
                             HashSet<Integer> relevantIds) {
        boolean foundPath;
        if (idx >= words.size())
            return true;
        if (words.get(idx).startsWith("$")) {
            HashMap<Integer, Node<Integer, Integer, Void>> children = root.getChildren();
            for (Map.Entry<Integer, Node<Integer, Integer, Void>> entry : children.entrySet()) {
                String nextWord = this.arpalmWrapper.getWordIndexer().getWord(entry.getValue().getValue());
                foundPath = traverse(entry.getValue(), words, idx + 1, relevantIds);
                if (foundPath && nextWord.startsWith("page_id_")) {
                    relevantIds.add(Integer.valueOf(nextWord.split("_")[2]));
                }
            }
            return true;
        } else {
            int wordIndex = this.arpalmWrapper.getWordIndexer().getIndexPossiblyUnk(words.get(idx));
            Node<Integer, Integer, Void> nextNode = root.getChildren().get(wordIndex);
            if (nextNode == null) {
                return false;
            }
            foundPath = traverse(nextNode, words, idx + 1, relevantIds);
//            String nextWord = this.arpalmWrapper.getWordIndexer().getWord(nextNode.getValue());
//            if (foundPath && nextWord.startsWith("page_id_")) {
//                relevantIds.add(Integer.valueOf(nextWord.split("_")[2]));
//            }
            return foundPath;
        }
    }
}
