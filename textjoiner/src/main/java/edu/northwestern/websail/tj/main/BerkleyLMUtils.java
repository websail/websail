package edu.northwestern.websail.tj.main;

import edu.berkeley.nlp.lm.io.LmReaders;
import edu.northwestern.websail.text.ngram.ARPALMWrapper;

import java.io.*;

/**
 * @author csbhagav on 9/19/14.
 */
public class BerkleyLMUtils {

    public static void main(String[] args) throws IOException {

        System.out.println("1 - Generate reverted ARPA format file for LM");
        System.out.println("2 - Generate Binary LM file from ARPA format file");

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line = in.readLine();
        int choice = Integer.valueOf(line);
        switch (choice) {
            case 1:
                reverseArpaLMData(args[0]);
                break;
            case 2:
                serialize(args[0], args[1]);
                break;
        }

    }

    private static void serialize(String arpaLmFile, String binaryArpaLmFile) {
        System.out.println("Loading ARPA Model...");
        ARPALMWrapper model = new ARPALMWrapper(arpaLmFile, false, false);
        System.out.println("Writing binary file to " + binaryArpaLmFile);
        LmReaders.writeLmBinary(model.getLm(), binaryArpaLmFile);
        System.out.println("done");
    }

    private static void reverseArpaLMData(String inputFile) throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter(new File(inputFile + "-inverted")));
        BufferedReader in = new BufferedReader(new FileReader(new File(inputFile)));

        String line;
        while ((line = in.readLine()) != null) {
            if (line.equalsIgnoreCase("") || line.startsWith("\\") || line.startsWith("ngram")) {
                out.write(line + "\n");
            } else {
                String[] parts = line.split("\t");
                String[] words = parts[1].split(" ");
                String ngram = "";
                for (int i = words.length - 1; i >= 0; i--) {
                    ngram += words[i] + " ";
                }
                out.write(parts[0] + "\t" + ngram.trim() + "\n");
            }
        }
        in.close();
        out.close();
    }
}
