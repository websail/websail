package edu.northwestern.websail.tj.main;


import edu.northwestern.websail.datastructure.trie.ds.Node;
import edu.northwestern.websail.datastructure.trie.ds.Trie;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.*;

/**
 * @author csbhagav on 9/10/14.
 */
public class NGramTrieSerializer {


    public static void writeToFile(BufferedWriter out, NgramTrie ngramTrie)
            throws IOException {

        Trie<Integer, Integer, Void> trie = ngramTrie.getTrie();
        Node<Integer, Integer, Void> root = trie.getRoot();
        ArrayList<String> edgeList = new ArrayList<String>();

        System.out.println("Number of nodes = " + trie.getNumNodes());
        out.write("==Nodes==\n");
        dfsWriteTrieNodes(out, root, edgeList);
        out.write("==Edges==\n");
        for (String e : edgeList) {
            out.write(e + "\n");
        }
        out.flush();
        out.close();
    }

    private static void dfsWriteTrieNodes(BufferedWriter out,
                                          Node<Integer, Integer, Void> root,
                                          ArrayList<String> edgeList) throws IOException {

        Queue<Node<Integer, Integer, Void>> queue = new LinkedList<Node<Integer, Integer, Void>>();
        HashMap<Integer, Node<Integer, Integer, Void>> children;
        Node<Integer, Integer, Void> n;
        queue.add(root);

        while (!queue.isEmpty()) {
            n = queue.poll();
            out.write(n.toString() + "\n");
            children = n.getChildren();
            for (Map.Entry<Integer, Node<Integer, Integer, Void>> e : children
                    .entrySet()) {
                edgeList.add(e.getValue().getId() + "\t" + n.getId() + "\t" + e
                        .getKey());
                queue.add(e.getValue());
            }
        }
    }

    public static Trie<Integer, Integer, Void> readFromFile(BufferedReader in) throws IOException {

        HashMap<Integer, Node<Integer, Integer, Void>> nodes = new HashMap<Integer, Node<Integer, Integer, Void>>();

        String line;
        // Read all nodes and mentions + candidates
        while ((line = in.readLine()) != null) {
            if (line.equalsIgnoreCase("==Edges==")) {
                break;
            } else if (line.equalsIgnoreCase("==Nodes=="))
                continue;

            String[] parts = line.split("\t");

            Integer val = parts[1].equals("null") ? -1 : Integer.valueOf(parts[1]);
            Node<Integer, Integer, Void> n = new Node<Integer, Integer, Void>(
                    Integer.valueOf(parts[0]), val);
//            if (parts[2].equalsIgnoreCase("true")) {
//
//                W2CMentionNode mention = new W2CMentionNode();
//
//                for (int i = 3; i < parts.length; i += 2) {
//
//                    String key = parts[i];
//                    String candidatesSet = parts[i + 1];
//
//                    mention.addCandidateFromSerializedString(key, candidatesSet);
//                }
//                n.setEndpoint(mention);
//            }

            nodes.put(n.getId(), n);
        }

        // Read all Edges
        int childId;
        int parentId;
        Integer edgeIdentifier;
        while ((line = in.readLine()) != null) {
            String[] parts = line.split("\t");
            childId = Integer.valueOf(parts[0]);
            parentId = Integer.valueOf(parts[1]);
            edgeIdentifier = Integer.valueOf(parts[2]);
            nodes.get(childId).setParent(nodes.get(parentId));
            nodes.get(parentId).addChild(edgeIdentifier, nodes.get(childId));
        }

        Trie<Integer, Integer, Void> trie = new Trie<Integer, Integer, Void>();
        trie.setRoot(nodes.get(0));
        trie.setNumNodes(nodes.size());
        in.close();
        return trie;
    }
}