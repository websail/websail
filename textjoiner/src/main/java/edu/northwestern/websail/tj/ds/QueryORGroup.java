package edu.northwestern.websail.tj.ds;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * @author csbhagav on 9/15/14.
 */
public class QueryORGroup {

    String orGroupQuery;
    Integer groupNumVariables;
    ArrayList<Query> groupQueries;
    HashSet<String> groupVariables;

    QueryORGroup(String orGroupQuery) {
        this.orGroupQuery = orGroupQuery;
        groupQueries = new ArrayList<Query>();
        String[] orQueries = this.orGroupQuery.split("\\+");
        for (int i = 0; i < orQueries.length; i++) {
            orQueries[i] = orQueries[i].trim();
            groupQueries.add(new Query(orQueries[i], this));
        }
        groupVariables = new HashSet<String>();
        for(Query q : groupQueries){
            groupVariables.addAll(q.getVariablesSet());
        }
    }

    public String getOrGroupQuery() {
        return orGroupQuery;
    }

    public void setOrGroupQuery(String orGroupQuery) {
        this.orGroupQuery = orGroupQuery;
    }

    public Integer getGroupNumVariables() {
        return groupNumVariables;
    }

    public void setGroupNumVariables(Integer groupNumVariables) {
        this.groupNumVariables = groupNumVariables;
    }

    public ArrayList<Query> getGroupQueries() {
        return groupQueries;
    }

    public void setGroupQueries(ArrayList<Query> groupQueries) {
        this.groupQueries = groupQueries;
    }

    public HashSet<String> getGroupVariables() {
        return groupVariables;
    }

    public void setGroupVariables(HashSet<String> groupVariables) {
        this.groupVariables = groupVariables;
    }

    public String toString() {
        String orGroupStr = "";
        for (Query q : groupQueries) {
            orGroupStr += q.getQuery() + " OR ";
        }
        return orGroupStr.substring(0, orGroupStr.length() - 3);
    }

    public boolean hasSameNumVariables() {

        for (int i = 0; i < groupQueries.size() - 1; i++) {
            if (!groupQueries.get(i).getNumVariables().equals(groupQueries.get(i + 1).getNumVariables())
                    ||
                    !(groupQueries.get(i).getVariablesSet().containsAll(groupQueries.get(i + 1).getVariablesSet())
                            && groupQueries.get(i + 1).getVariablesSet().containsAll(groupQueries.get(i).getVariablesSet()))
                    ) {
                return false;
            }
        }
        this.groupNumVariables = this.groupQueries.get(0).getNumVariables();
        return true;
    }
}
