package edu.northwestern.websail.tj.ds;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * @author csbhagav on 9/8/14.
 */
public class VariableAssignment {

    HashMap<String, String> variableValues = new HashMap<String, String>();
    HashMap<String, Double> variableValueUnigramLogProb = new HashMap<String, Double>();
    Double score;
    Double logProb;
    int[] ngram;
    String[] ngramWords;
    String source;
    public static final String SOURCE_LM = "LM";
    public static final String SOURCE_W2V = "W2V";
    String description = "";

    public void addVariable(String key) {
        if (!variableValues.containsKey(key)) {
            variableValues.put(key, null);
        }
    }

    public void setVariableValue(String key, String value) {
        variableValues.put(key, value);
    }

    public int numVariables() {
        return variableValues.size();
    }

    public HashSet<String> getUnsetVariables() {
        HashSet<String> unsetVariables = new HashSet<String>();
        for (Map.Entry<String, String> entry : variableValues.entrySet()) {
            if (entry.getValue() == null)
                unsetVariables.add(entry.getKey());
        }
        return unsetVariables;
    }

    public String getVariableValue(String key) {
        return variableValues.get(key);
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public HashMap<String, String> getVariableValues() {
        return variableValues;
    }

    public void setVariableValues(HashMap<String, String> variableValues) {
        this.variableValues = variableValues;
    }

    public String[] getNgramWords() {
        return ngramWords;
    }

    public void setNgramWords(String[] ngramWords) {
        this.ngramWords = ngramWords;
    }

    public int[] getNgram() {
        return ngram;
    }

    public void setNgram(int[] ngram) {
        this.ngram = ngram;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public HashMap<String, Double> getVariableValueUnigramLogProb() {
        return variableValueUnigramLogProb;
    }

    public void setVariableValueUnigramLogProb(HashMap<String, Double> variableValueUnigramLogProb) {
        this.variableValueUnigramLogProb = variableValueUnigramLogProb;
    }

    public Double getLogProb() {
        return logProb;
    }

    public void setLogProb(Double logProb) {
        this.logProb = logProb;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : variableValues.entrySet()) {
            sb.append(entry.getKey()).append(":").append(entry.getValue()).append(" ; ");
        }
        sb.append(this.getScore());
        return sb.toString();
    }

    public String toString(HashMap<Integer, String> idToTitleMap, boolean pageIdOnly) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : variableValues.entrySet()) {
            if (entry.getValue().startsWith("page_id")) {
                String[] parts = entry.getValue().split("_");
                Integer pgId = Integer.valueOf(parts[parts.length - 1]);
                sb.append(entry.getKey()).append(":").append(idToTitleMap.get(pgId)).append(":").append("\t");
            } else if (!pageIdOnly) {
                sb.append(entry.getKey()).append(":").append(entry.getValue()).append(":");
            }
        }
        sb.append(this.score).append(" - ");
        sb.append(this.source);
        sb.append("\t").append(this.description);
        return sb.toString();
    }

    public boolean valuesHavePgIdsOnly() {
        for (Map.Entry<String, String> entry : variableValues.entrySet()) {
            if (!entry.getValue().startsWith("page_id_"))
                return false;
        }
        return true;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String toString(QueryORGroup group) {
        StringBuilder sb = new StringBuilder();
        for (String var : group.getGroupVariables()) {
            sb.append(var).append(":").append(this.variableValues.get(var)).append(";");
        }
        return sb.toString();
    }
}
