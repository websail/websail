package edu.northwestern.websail.tj.main;

import edu.berkeley.nlp.lm.ArrayEncodedProbBackoffLm;
import edu.berkeley.nlp.lm.map.HashNgramMap;
import edu.northwestern.websail.text.ngram.ARPALMWrapper;
import edu.northwestern.websail.tj.ds.Query;
import org.apache.commons.lang.ArrayUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Logger;

/**
 * @author csbhagav on 9/13/14.
 */
public class BerkleyLMTrie {
    final Logger logger = Logger.getLogger(BerkleyLMTrie.class.getName());
    final ARPALMWrapper lmWrapper;
    long[][] startoffsets;
    long[][] endoffsets;

    public BerkleyLMTrie(ARPALMWrapper lmWrapper, String offsetFilePrefix) throws IOException, ClassNotFoundException {
        this.lmWrapper = lmWrapper;
        readOffsets(lmWrapper.getLm(), offsetFilePrefix);
    }

    @SuppressWarnings("unchecked")
    private void readOffsets(ArrayEncodedProbBackoffLm<String> revLM, String arraySerializedFile) throws IOException, ClassNotFoundException {
        File startOffsetFile = new File(arraySerializedFile + "-start");
        File endOffsetFile = new File(arraySerializedFile + "-end");

        if (startOffsetFile.exists()) {
            logger.info("Found serialized offset files:" + startOffsetFile + " ; " + endOffsetFile);
            ObjectInputStream ois1 = new ObjectInputStream(new FileInputStream(startOffsetFile));
            startoffsets = (long[][]) ois1.readObject();

            ObjectInputStream ois2 = new ObjectInputStream(new FileInputStream(endOffsetFile));
            endoffsets = (long[][]) ois2.readObject();
        } else {
            logger.info("Serialized offset files not found. Creating :" + startOffsetFile + " ; " + endOffsetFile);
            startoffsets = new long[revLM.getLmOrder()][revLM.getWordIndexer().numWords()];
            endoffsets = new long[revLM.getLmOrder()][revLM.getWordIndexer().numWords()];
            boolean[][] started = new boolean[revLM.getLmOrder()][revLM.getWordIndexer().numWords()];
            HashNgramMap map = ((HashNgramMap) revLM.getNgramMap());

            System.out.println("setting offsets ... ");
            for (int order = 0; order < revLM.getLmOrder(); order++) {
                System.out.println("Order = " + order);
                Iterator<Long> it = map.getNgramOffsetsForOrder(order).iterator();
                int prev = -1;
                while (it.hasNext()) {
                    long offset = it.next();
                    int[] ngram = map.getNgramForOffset(offset, order);
                    if (ngram[0] != prev) {
                        if (prev != -1 && started[order][prev]) {
                            endoffsets[order][prev] = offset;
                        }
                        startoffsets[order][ngram[0]] = offset;
                        started[order][ngram[0]] = true;
                    }
                    prev = ngram[0];
                }
            }

            ObjectOutputStream oos1 = new ObjectOutputStream(new FileOutputStream(startOffsetFile));
            oos1.writeObject(startoffsets);
            ObjectOutputStream oos2 = new ObjectOutputStream(new FileOutputStream(endOffsetFile));
            oos2.writeObject(endoffsets);
            logger.info("Done creating serialized offset files");
        }
    }

    public ArrayList<int[]> getSatisfyingNgrams(int[] ngram, Boolean reverse) {
        ArrayList<int[]> listOfNgrams = new ArrayList<int[]>();

        int ngramOrder = ngram.length - 1;
        long startOffset = startoffsets[ngramOrder][ngram[0]];
        long endOffset = endoffsets[ngramOrder][ngram[0]];

        for (long offset = startOffset; offset < endOffset; offset++) {
            int[] words = ((HashNgramMap) lmWrapper.getLm().getNgramMap()).getNgramForOffset(offset,
                    ngramOrder);
            if (isValidPrefix(ngram, words)) {
                if (reverse) {
                    ArrayUtils.reverse(words);
                }
                listOfNgrams.add(words);
            }
        }
        return listOfNgrams;
    }

    private static boolean isValidPrefix(int[] ngram, int[] words) {
        for (int i = 0; i < ngram.length; i++) {
            if (ngram[i] == Query.VARIABLE_ID || ngram[i] == Query.WILDCARD_ID)
                continue;
            if (ngram[i] != words[i])
                return false;
        }
        return true;
    }
}
