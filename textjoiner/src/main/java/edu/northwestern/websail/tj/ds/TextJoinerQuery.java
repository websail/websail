package edu.northwestern.websail.tj.ds;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author csbhagav on 9/11/14.
 */
public class TextJoinerQuery {

    String originalQuery;
    ArrayList<QueryORGroup> queries = new ArrayList<QueryORGroup>();
    HashSet<String> variables = new HashSet<String>();

    public TextJoinerQuery(String queryString) {
        this.originalQuery = queryString;
        Pattern andSplitPattern = Pattern.compile("<(.*?)>");
        Matcher andMatcher = andSplitPattern.matcher(queryString);
        while (andMatcher.find()) {
            String andQuery = andMatcher.group(1).trim();
            QueryORGroup orGroup = new QueryORGroup(andQuery);
            queries.add(orGroup);

        }
        if (!sanityCheck()) {
            System.out.println("Unparsable !");
        } else {

            Collections.sort(queries, new Comparator<QueryORGroup>() {
                @Override
                public int compare(QueryORGroup o1, QueryORGroup o2) {
                    return o1.getGroupNumVariables() - o2.getGroupNumVariables();
                }
            });

            for (QueryORGroup orGroup : queries) {
                for (Query q : orGroup.getGroupQueries()) {
                    variables.addAll(q.getVariablesSet());
                }
            }

            System.out.println("Parsed Query = ");
            StringBuilder sb = new StringBuilder();
            for (QueryORGroup group : queries) {
                sb.append(group.toString()).append("\nAND\n");
            }
            System.out.println(sb.toString());
        }
    }

    private boolean sanityCheck() {

        for (QueryORGroup orGroup : queries) {
            if (!orGroup.hasSameNumVariables()) {
                return false;
            }
        }
        return true;
    }

    public String getOriginalQuery() {
        return originalQuery;
    }

    public void setOriginalQuery(String originalQuery) {
        this.originalQuery = originalQuery;
    }

    public ArrayList<QueryORGroup> getQueries() {
        return queries;
    }

    public void setQueries(ArrayList<QueryORGroup> queries) {
        this.queries = queries;
    }

    public int getNumQueries() {
        return queries.size();
    }

    public HashSet<String> getVariables() {
        return variables;
    }

    public void setVariables(HashSet<String> variables) {
        this.variables = variables;
    }
}

