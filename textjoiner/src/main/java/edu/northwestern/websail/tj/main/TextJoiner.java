package edu.northwestern.websail.tj.main;

import edu.northwestern.websail.core.io.FileReadUtils;
import edu.northwestern.websail.core.util.TimerUtil;
import edu.northwestern.websail.lOntology.adapter.EntityVectorsAdapter;
import edu.northwestern.websail.lOntology.ds.PairIntDouble;
import edu.northwestern.websail.lOntology.model.EntityLatentRepresentation;
import edu.northwestern.websail.text.ngram.ARPALMWrapper;
import edu.northwestern.websail.tj.ds.*;
import org.la4j.vector.Vector;
import org.la4j.vector.dense.BasicVector;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author csbhagav on 9/8/14.
 */
public class TextJoiner {

    private static final Pattern entityTagPattern = Pattern.compile("\\[.*?\\]");

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        TimerUtil util = new TimerUtil();

        Properties config = new Properties();
        config.load(new FileInputStream(new File(args[0])));

        String dataDir = config.getProperty("dataDir");
        String lmFile = dataDir + "/" + config.getProperty("lmFile");
        String revLmFile = dataDir + "/" + config.getProperty("revLmFile");
        String trieOffsetsFile = dataDir + "/" + config.getProperty("trieOffsetsFile");
        String revTrieOffsetsFile = dataDir + "/" + config.getProperty("revTrieOffsetsFile");
        String w2vFile = dataDir + "/" + config.getProperty("w2vFile");
        Boolean enableW2vExpansion = Boolean.valueOf(config.getProperty("enableW2vExpansion"));
        TJScorerType scorerType = TJScorerType.valueOf(config.getProperty("scorer"));
        Boolean stopAtMaxGap = Boolean.valueOf(config.getProperty("enableGapStop"));
        Boolean enableUnigramDiscount = Boolean.valueOf(config.getProperty("enableUnigramDiscount"));

        int w2vExpansionLimit = 100;

        System.out.println("load model...");
        util.start();
        ARPALMWrapper w = new ARPALMWrapper(lmFile, false, true);
        BerkleyLMTrie lmTrie = new BerkleyLMTrie(w, trieOffsetsFile);
        ARPALMWrapper revW = new ARPALMWrapper(revLmFile, false, true);
        BerkleyLMTrie revLmTrie = new BerkleyLMTrie(revW, revTrieOffsetsFile);

        HashMap<Integer, String> idToTitleMap = FileReadUtils.getMapIntToString("/websail/common/wikification/data/en/en_id_title.map");
        HashMap<String, Integer> redirectMap = FileReadUtils.getMapStringToInt("/websail/common/wikification/data/en/redirect.map");

        EntityVectorsAdapter vectorsAdapter = null;
        if (enableW2vExpansion) {
            vectorsAdapter = new EntityVectorsAdapter();
            vectorsAdapter.readVectorsFile(w2vFile);
        }

        util.end();
        System.out.println("Done loading all resources... " + util.totalTime());

        /**
         * Command line interface to TJ
         */
        while (true) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter query(ies). Format : '<Q1 OR Q2><Q3 OR Q4>' ...\n");
            String ipLine = br.readLine();
            if (ipLine.equalsIgnoreCase("exit")) {
                break;
            }
            TextJoinerQuery query = new TextJoinerQuery(ipLine);
            List<VariableAssignment> variableAssignments = solve(w, query, redirectMap, true,
                    lmTrie, revLmTrie, enableW2vExpansion, vectorsAdapter, w2vExpansionLimit, scorerType,
                    stopAtMaxGap, enableUnigramDiscount);

            for (VariableAssignment assignment : variableAssignments) {
                if (!assignment.valuesHavePgIdsOnly())
                    continue;
                String strVal = assignment.toString(idToTitleMap, true);
                if (!strVal.equalsIgnoreCase(""))
                    System.out.println(strVal);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static List<VariableAssignment> solve(ARPALMWrapper lmWrapper, TextJoinerQuery query,
                                                 HashMap<String, Integer> redirectMap,
                                                 boolean pageIdOnly, BerkleyLMTrie lmTrie, BerkleyLMTrie revLmTrie,
                                                 boolean expandUsingW2v, EntityVectorsAdapter vectorsAdapter,
                                                 int w2vExpansionLimit, TJScorerType scorer, Boolean stopAtMaxGap,
                                                 Boolean enableUnigramDiscount) {
        boolean gapStopPreProc = enableUnigramDiscount;

        ArrayList<VariableAssignment> variableAssignments = new ArrayList<VariableAssignment>();

        boolean first = true;
        for (QueryORGroup orGroup : query.getQueries()) {
            expandOrGroup(orGroup, redirectMap);
            ArrayList<VariableAssignment> newVariableAssignments = new ArrayList<VariableAssignment>();
            if (first) {
                newVariableAssignments.addAll(solve(lmWrapper, orGroup, lmTrie, revLmTrie, pageIdOnly,
                        null));
                first = false;
            } else {
                for (VariableAssignment assignment : variableAssignments) {
                    if (assignment.getVariableValues().size() == 0)
                        continue;
                    newVariableAssignments.addAll(solve(lmWrapper, orGroup, lmTrie, revLmTrie, pageIdOnly,
                            assignment));
                }
            }
            variableAssignments.clear();
            variableAssignments = newVariableAssignments;
        }

        variableAssignments = dedup(variableAssignments, query);

        switch (scorer) {
            case LAST_QUERY_SCORE:
                TJScoringFunctions.lastQueryScorer(variableAssignments, lmWrapper, query, gapStopPreProc);
                break;
            case CLASSIC_SCORE:
                TJScoringFunctions.classicScorer(variableAssignments, lmWrapper, query, gapStopPreProc);
                break;
            case RANK_PRODUCT_SCORE:
                TJScoringFunctions.rankProdScorer(variableAssignments, lmWrapper, query, gapStopPreProc);
                break;

        }
        Collections.sort(variableAssignments, new Comparator<VariableAssignment>() {
            @Override
            public int compare(VariableAssignment o1, VariableAssignment o2) {
                if (o1.getScore() < o2.getScore())
                    return 1;
                else if (o1.getScore() > o2.getScore())
                    return -1;
                else return 0;
            }
        });

        if (variableAssignments.size() == 0)
            return variableAssignments;

        Set<String> variables = variableAssignments.get(0).getVariableValues().keySet();

        if (expandUsingW2v) {
            ArrayList<VariableAssignment> w2vAssignments = generateW2VAssignments(query, vectorsAdapter, variableAssignments,
                    variables, lmWrapper, w2vExpansionLimit);
            for (VariableAssignment assignment : w2vAssignments) {
                assignment.setSource("W2V");
            }
            variableAssignments.addAll(w2vAssignments);
            variableAssignments = dedup(variableAssignments, query);

            switch (scorer) {
                case LAST_QUERY_SCORE:
                    TJScoringFunctions.lastQueryScorer(variableAssignments, lmWrapper, query, gapStopPreProc);
                    break;
                case CLASSIC_SCORE:
                    TJScoringFunctions.classicScorer(variableAssignments, lmWrapper, query, gapStopPreProc);
                    break;
                case RANK_PRODUCT_SCORE:
                    TJScoringFunctions.rankProdScorer(variableAssignments, lmWrapper, query, gapStopPreProc);
                    break;
            }

            Collections.sort(variableAssignments, new Comparator<VariableAssignment>() {
                @Override
                public int compare(VariableAssignment o1, VariableAssignment o2) {
                    if (o1.getScore() < o2.getScore())
                        return 1;
                    else if (o1.getScore() > o2.getScore())
                        return -1;
                    else return 0;
                }
            });
        }

        if (stopAtMaxGap) {
            System.out.println("Gap Stop enabled. Filtering and re-ranking !! Initial size = " + variableAssignments.size());
            variableAssignments = filterByMaxGap(variableAssignments);
            System.out.println("Filtered size = " + variableAssignments.size());
            switch (scorer) {
                case LAST_QUERY_SCORE:
                    TJScoringFunctions.lastQueryScorer(variableAssignments, lmWrapper, query, enableUnigramDiscount);
                    break;
                case CLASSIC_SCORE:
                    TJScoringFunctions.classicScorer(variableAssignments, lmWrapper, query, enableUnigramDiscount);
                    break;
                case RANK_PRODUCT_SCORE:
                    TJScoringFunctions.rankProdScorer(variableAssignments, lmWrapper, query, enableUnigramDiscount);
                    break;
            }


            Collections.sort(variableAssignments, new Comparator<VariableAssignment>() {
                @Override
                public int compare(VariableAssignment o1, VariableAssignment o2) {
                    if (o1.getScore() < o2.getScore())
                        return 1;
                    else if (o1.getScore() > o2.getScore())
                        return -1;
                    else return 0;
                }
            });

            return variableAssignments;
        }

        return variableAssignments;
    }

    private static ArrayList<VariableAssignment> filterByMaxGap(ArrayList<VariableAssignment> variableAssignments) {

        double maxGap = 0.0;
        int maxGapIdx = 0;
        for (int i = 1; i < variableAssignments.size(); i++) {
            double gap = Math.abs(variableAssignments.get(i).getScore() - variableAssignments.get(i - 1).getScore());
//            gap = gap / Math.abs(variableAssignments.get(i - 1).getScore());
            if (gap > maxGap) {
                maxGap = Math.abs(gap);
                maxGapIdx = i;
            }

        }
        return new ArrayList<VariableAssignment>(variableAssignments.subList(0, maxGapIdx));
    }

    private static ArrayList<VariableAssignment> generateW2VAssignments(TextJoinerQuery query, EntityVectorsAdapter vectorsAdapter, List<VariableAssignment> assignments, Set<String> variables, ARPALMWrapper lmWrapper, Integer limit) {

        HashMap<String, ArrayList<Integer>> variableValueSet = new HashMap<String, ArrayList<Integer>>();

        for (int i = 0; i < assignments.size() && i < 15; i++) {
            VariableAssignment assignment = assignments.get(i);
            for (String variable : variables) {
                if (!variableValueSet.containsKey(variable)) {
                    variableValueSet.put(variable, new ArrayList<Integer>());
                }
                Integer pgId = Integer.valueOf(assignment.getVariableValue(variable).split("_")[2]);
                variableValueSet.get(variable).add(pgId);
            }
        }

        HashMap<String, ArrayList<PairIntDouble>> expandSet = new HashMap<String, ArrayList<PairIntDouble>>();

        for (String variable : variables) {
            int numValues = variableValueSet.get(variable).size();
            ArrayList<PairIntDouble> expanded = vectorsAdapter.setExpansion(variableValueSet.get(variable).toArray(new
                    Integer[numValues]));
            expanded = new ArrayList<PairIntDouble>(
                    expanded.subList(0,
                            expanded.size() > limit ? limit : expanded.size()));

            for (PairIntDouble anExpanded : expanded) {
                if (!expandSet.containsKey(variable)) {
                    expandSet.put(variable, new ArrayList<PairIntDouble>());
                }
                expandSet.get(variable).add(anExpanded);
            }
        }

        ArrayList<QueryORGroup> orGroups = query.getQueries();
        String[] variablesArray = variables.toArray(new String[variables.size()]);
        QueryORGroup allVariablesQuery = orGroups.get(orGroups.size() - 1);

        ArrayList<VariableAssignment> newAssignments = new ArrayList<VariableAssignment>();

        /**
         * Try all assignments
         */

//        recurseAssignments(expandSet, variablesArray, 0, newAssignments);

        /**
         * Get assignments by relation vector
         */
        getAssignmentsByRelationVector(vectorsAdapter, expandSet, variablesArray, newAssignments, assignments);

        for (VariableAssignment expSetAssignments : newAssignments) {
            for (Query q : allVariablesQuery.getGroupQueries()) {

                int[] ngram = q.getNgramCopy();
                Integer[] varPositions = q.getVariablePositions();
                List<String> words = q.getQueryWords();
                boolean allVariableValuesFound = true;
                for (Integer varPosition : varPositions) {
                    final String var = words.get(varPosition);
                    if (!expSetAssignments.getVariableValues().containsKey(var)) {
                        allVariableValuesFound = false;
                        break;
                    } else {
                        ngram[varPosition] = lmWrapper.getWordIndexer().getIndexPossiblyUnk(expSetAssignments
                                .getVariableValue(var));
                    }
                }
                expSetAssignments.setScore(allVariableValuesFound ? lmWrapper.getScore(ngram, 0,
                        ngram.length) : -100.0);
            }
        }

        return newAssignments;
    }

    private static void getAssignmentsByRelationVector(EntityVectorsAdapter vectorsAdapter, HashMap<String, ArrayList<PairIntDouble>> expandSet, String[] variablesArray, ArrayList<VariableAssignment> newAssignments, List<VariableAssignment> lmAssignments) {

        ArrayList<VariableAssignment> tempAssignments = new ArrayList<VariableAssignment>();

        int length = vectorsAdapter.getEntityVectors().get(6886).getRepresentation().length();

        ArrayList<PairIntDouble> firstSet = expandSet.get(variablesArray[0]);
        String firstVar = variablesArray[0];

        for (PairIntDouble pair : firstSet) {
            VariableAssignment firstVarAssignment = new VariableAssignment();
            firstVarAssignment.setVariableValue(firstVar, "page_id_" + pair.getIntElement());
            tempAssignments.add(firstVarAssignment);
        }

        for (int i = 1; i < variablesArray.length; i++) {
            int count = 0;
            Vector meanRelationVector = new BasicVector(length);
            for (int j = 0; j < lmAssignments.size() && j < 15; j++) {
                Integer firstPgId = Integer.valueOf(lmAssignments.get(j).getVariableValue(firstVar).split("_")[2]);
                Integer secondPgId = Integer.valueOf(lmAssignments.get(j).getVariableValue(variablesArray[i]).split("_")[2]);
                if (!(vectorsAdapter.getEntityVectors().containsKey(firstPgId) && vectorsAdapter.getEntityVectors()
                        .containsKey(secondPgId))) {
                    continue;
                }
                Vector relationVector = vectorsAdapter.getEntityVectors().get(secondPgId).getRepresentation().
                        subtract(vectorsAdapter.getEntityVectors().get(firstPgId).getRepresentation());
                meanRelationVector = meanRelationVector.add(relationVector);
                count++;
            }
            meanRelationVector = meanRelationVector.divide(count);
            ArrayList<PairIntDouble> secondSet = expandSet.get(variablesArray[i]);
            for (VariableAssignment newAssignment : tempAssignments) {
                Integer pgId = Integer.valueOf(newAssignment.getVariableValue(firstVar).split("_")[2]);
                Vector targetVector = meanRelationVector.add(vectorsAdapter.getEntityVectors().get(pgId).getRepresentation());
                double targetNorm = Math.sqrt(targetVector.innerProduct(targetVector));
                EntityLatentRepresentation targetRepresentation = new EntityLatentRepresentation(targetVector,
                        targetNorm);

                ArrayList<PairIntDouble> pairs = vectorsAdapter.
                        mostSimilaryEntity(targetRepresentation, secondSet);

                newAssignment.setVariableValue(variablesArray[i], "page_id_" + pairs.get(0).getIntElement());
            }
        }

        for (VariableAssignment tempAssignment : tempAssignments) {
            if (tempAssignment.getVariableValues().size() == variablesArray.length) {
                newAssignments.add(tempAssignment);
            }
        }
    }

    private static ArrayList<VariableAssignment> dedup(ArrayList<VariableAssignment> variableAssignments, TextJoinerQuery query) {
        if (variableAssignments.size() == 0)
            return variableAssignments;
        ArrayList<VariableAssignment> newAssignments = new ArrayList<VariableAssignment>();
        HashSet<String> keys = new HashSet<String>();
        HashSet<String> variables = query.getVariables();

        for (VariableAssignment variableAssignment : variableAssignments) {
            String key = "";
            for (String variable : variables) {
                key += variable + ":" + variableAssignment.getVariableValue(variable) + "-";
            }
            if (!keys.contains(key)) {
                keys.add(key);
                newAssignments.add(variableAssignment);
            }
        }

        return newAssignments;
    }

    private static void expandOrGroup(QueryORGroup orGroup, HashMap<String, Integer> redirectMap) {
        ArrayList<Query> expandedQueries = new ArrayList<Query>();
        for (Query orQuery : orGroup.getGroupQueries()) {
            String queryString = orQuery.getQuery();
            Matcher m = entityTagPattern.matcher(queryString);
            boolean matchedEntity = false;
            while (m.find()) {
                matchedEntity = true;
                String entityStr = m.group();

                final String entityCleanQuery = queryString.replace(entityStr, entityStr.replaceAll("\\[|\\]",
                        "").replaceAll("_", " "));
                expandedQueries.add(new Query(entityCleanQuery, orGroup));

                String pageTitle = entityStr.replaceAll("\\[|\\]", "").replaceAll(" ", "_");
                if (redirectMap.containsKey(pageTitle)) {
                    final String entityToPageIdQuery = queryString.replace(entityStr,
                            "page_id_" + redirectMap.get(pageTitle));
                    expandedQueries.add(new Query(entityToPageIdQuery, orGroup));
                }
            }
            if (!matchedEntity) {
                expandedQueries.add(orQuery);
            }
        }

        orGroup.getGroupQueries().clear();
        orGroup.getGroupQueries().addAll(expandedQueries);
    }

    //XXX: Needs re-factoring
    @SuppressWarnings("ConstantConditions")
    private static ArrayList<VariableAssignment> solve(ARPALMWrapper lmWrapper, QueryORGroup orGroup, BerkleyLMTrie lmTrie,
                                                       BerkleyLMTrie revLmTrie, boolean pageIdOnly,
                                                       VariableAssignment oldAssignment) {
        ArrayList<int[]> relevantNgrams;
        ArrayList<VariableAssignment> variableAssignments = new ArrayList<VariableAssignment>();
        for (Query orQ : orGroup.getGroupQueries()) {
            orQ.setQueryNgramArr(lmWrapper);

            int[] queryNgram = orQ.getNgramCopy();
            if (oldAssignment != null) {
                int numUnsetVar = orQ.variablePositions.length;
                for (int i = 0; i < orQ.variablePositions.length; i++) {
                    int varIdx = orQ.getVariablePositions()[i];
                    String varName = orQ.getQueryWords().get(varIdx);
                    if (oldAssignment.getVariableValues().containsKey(varName)) {
                        int val = lmWrapper.getWordIndexer().getIndexPossiblyUnk(oldAssignment.getVariableValue
                                (varName));
                        queryNgram[varIdx] = val;
                        numUnsetVar--;
                    }
                }

                if (numUnsetVar == 0) {
                    double oldAssignmentNewQueryScore = lmWrapper.getScore(queryNgram, 0, queryNgram.length);
                    oldAssignment.setScore(oldAssignment.getScore() + oldAssignmentNewQueryScore);
                    variableAssignments.add(oldAssignment);
                    continue;
                }
            }

            if (queryNgram[0] == Query.VARIABLE_ID || queryNgram[0] == Query.WILDCARD_ID) {
                if (queryNgram[queryNgram.length - 1] == Query.VARIABLE_ID || queryNgram[queryNgram.length - 1] == Query
                        .WILDCARD_ID) {
                    int[] tempNgram = new int[queryNgram.length - 1];
                    System.arraycopy(queryNgram, 1, tempNgram, 0, queryNgram.length - 1);
                    relevantNgrams = new ArrayList<int[]>();
                    ArrayList<int[]> tempRelevantNgrams = lmTrie.getSatisfyingNgrams(tempNgram, false);
                    for (int[] tempRelevant : tempRelevantNgrams) {
                        String word = lmWrapper.getWordIndexer().getWord(tempRelevant[tempRelevant.length - 1]);
                        if (pageIdOnly && !word.startsWith("page_id_")) {
                            continue;
                        }
                        int[] appendedTempRelevant = new int[tempRelevant.length + 1];
                        int i = 0;
                        for (; i < tempRelevant.length; i++) {
                            appendedTempRelevant[i] = tempRelevant[tempRelevant.length - 1 - i];
                        }
                        appendedTempRelevant[i] = queryNgram[0];
                        relevantNgrams.addAll(revLmTrie.getSatisfyingNgrams(appendedTempRelevant, true));
                    }
                } else {
                    relevantNgrams = revLmTrie.getSatisfyingNgrams(orQ.getNgram(queryNgram, true), true);
                }
            } else {
                relevantNgrams = lmTrie.getSatisfyingNgrams(orQ.getNgram(queryNgram, false), false);
            }

            int numVars = getNumVars(queryNgram);
            if (relevantNgrams.size() > 0) {
                for (int[] ngram : relevantNgrams) {
                    float score = lmWrapper.getScore(ngram, 0, ngram.length);

                    boolean foundNewVar = false;
                    int numVarsValuesFound = 0;
                    VariableAssignment assignment = new VariableAssignment();
                    assignment.setSource(VariableAssignment.SOURCE_LM);
                    if (oldAssignment != null) assignment.getVariableValues().putAll(oldAssignment.getVariableValues());
                    for (int var = 0; var < orQ.variablePositions.length; var++) {
                        if (queryNgram[orQ.variablePositions[var]] != Query.VARIABLE_ID) {
                            continue;
                        }
                        String word = lmWrapper.getWordIndexer().getWord(ngram[orQ.variablePositions[var]]);
                        if (pageIdOnly) {
                            if (word.startsWith("page_id_")) {
                                assignment.setVariableValue(orQ.getQueryWords().get(orQ.variablePositions[var]),
                                        word);
                                foundNewVar = true;
                                numVarsValuesFound++;
                            }
                        } else {
                            assignment.setVariableValue(orQ.getQueryWords().get(orQ.variablePositions[var]),
                                    word);
                        }
                    }
                    if (foundNewVar && numVars == numVarsValuesFound) {
                        assignment.setScore((double) score);
                        assignment.setNgram(ngram);
                        variableAssignments.add(assignment);
                    }
                }
            }
        }

        return variableAssignments;
    }

    private static int getNumVars(int[] queryNgram) {
        int count = 0;
        for (int i : queryNgram) {
            if (i == Query.VARIABLE_ID)
                count++;
        }
        return count;
    }

}