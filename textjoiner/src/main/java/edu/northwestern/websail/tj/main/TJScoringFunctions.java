package edu.northwestern.websail.tj.main;

import edu.northwestern.websail.ds.tuple.StringDoublePair;
import edu.northwestern.websail.text.ngram.ARPALMWrapper;
import edu.northwestern.websail.tj.ds.Query;
import edu.northwestern.websail.tj.ds.QueryORGroup;
import edu.northwestern.websail.tj.ds.TextJoinerQuery;
import edu.northwestern.websail.tj.ds.VariableAssignment;

import java.util.*;

/**
 * @author csbhagav on 10/3/14.
 */
public class TJScoringFunctions {

    public static void lastQueryScorer(ArrayList<VariableAssignment> variableAssignments,
                                       ARPALMWrapper lmWrapper, TextJoinerQuery query, Boolean enableUnigramDiscount) {
        int lastIdx = query.getNumQueries() - 1;
        QueryORGroup orGroup = query.getQueries().get(lastIdx);
        for (VariableAssignment assignment : variableAssignments) {
            double score = 0.0;
            double logProb = 0.0;

            for (Query eachQuery : orGroup.getGroupQueries()) {
                int[] ngram = eachQuery.getNgramCopy();
                Integer[] varPositions = eachQuery.getVariablePositions();
                List<String> words = eachQuery.getQueryWords();

                int[] assignmentNgram = eachQuery.getAssignmentsNgram(lmWrapper, assignment);
                double p = lmWrapper.getScore(assignmentNgram, 0, assignmentNgram.length);
                double unigramProb = enableUnigramDiscount ? getAssigmentUnigramProb(lmWrapper, assignment,
                        eachQuery.getVariablesSet()) : 1.0;
                double unigramLogProb = Math.log10(unigramProb);

                logProb += p;
                score += enableUnigramDiscount ? p - unigramLogProb : p;


                for (Integer varPosition : varPositions) {
                    final String var = words.get(varPosition);
                    if (!assignment.getVariableValues().containsKey(var)) {
                        break;
                    } else {
                        ngram[varPosition] = lmWrapper.getWordIndexer().getIndexPossiblyUnk(assignment
                                .getVariableValue(var));
                    }
                }
            }
            assignment.setScore(score);
            assignment.setLogProb(logProb);
        }
    }


    public static void classicScorer(ArrayList<VariableAssignment> variableAssignments, ARPALMWrapper lmWrapper,
                                     TextJoinerQuery query, Boolean enableUnigramDiscount) {

        for (VariableAssignment assignment : variableAssignments) {
            // For each assignment, product of each AND (sum in log scale)
            double allQueryProbProd = 0.0;

            for (QueryORGroup group : query.getQueries()) {

                // For each or group, sum of probabilities (cannot use log scale)
                double groupProbSum = 0.0;
                for (Query eachQuery : group.getGroupQueries()) {
                    int[] assignmentNgram = eachQuery.getAssignmentsNgram(lmWrapper, assignment);
                    double logProb = lmWrapper.getScore(assignmentNgram, 0, assignmentNgram.length);
                    double prob = Math.exp(logProb * Math.log(10));
                    double unigramProb = enableUnigramDiscount ? getAssigmentUnigramProb(lmWrapper, assignment,
                            eachQuery.getVariablesSet()) : 1.0;
                    groupProbSum += prob / unigramProb;
                }
                allQueryProbProd += Math.log10(groupProbSum);
            }
            assignment.setScore(allQueryProbProd);
            String desc = assignment.getDescription();
            assignment.setDescription(desc + "\t" + allQueryProbProd);
        }
    }

    private static double getAssigmentUnigramProb(ARPALMWrapper lmWrapper, VariableAssignment assignment,
                                                  Set<String> variables) {

        double unigramLogProb = 0.0;

        for (String var : variables) {
            String word = assignment.getVariableValue(var);
            int[] unigram = new int[1];
            unigram[0] = lmWrapper.getWordIndexer().getIndexPossiblyUnk(word);
            unigramLogProb += lmWrapper.getScore(unigram, 0, 1);
        }
        return Math.exp(unigramLogProb * Math.log(10));
    }

    public static void rankProdScorer(ArrayList<VariableAssignment> variableAssignments, ARPALMWrapper lmWrapper,
                                      TextJoinerQuery query, Boolean enableUnigramDiscount) {

        double[] rankProd = new double[variableAssignments.size()];
        for (int i = 0; i < rankProd.length; i++) {
            rankProd[i] = 0.0;
        }


        int groupIdx = 1;

        for (QueryORGroup group : query.getQueries()) {

            HashMap<String, Integer> queryVariableIndex = new HashMap<String, Integer>();
            HashSet<String> doneVar = new HashSet<String>();

            ArrayList<StringDoublePair> sdPairs = new ArrayList<StringDoublePair>();


            for (VariableAssignment variableAssignment : variableAssignments) {
                if (doneVar.contains(variableAssignment.toString(group))) {
                    continue;
                }

                double groupProbSum = 0.0;
                for (Query eachQuery : group.getGroupQueries()) {
                    int[] assignmentNgram = eachQuery.getAssignmentsNgram(lmWrapper, variableAssignment);
                    double logProb = lmWrapper.getScore(assignmentNgram, 0, assignmentNgram.length);
                    double prob = Math.exp(logProb * Math.log(10));
                    double unigramProb = enableUnigramDiscount ? getAssigmentUnigramProb
                            (lmWrapper,
                                    variableAssignment,
                                    eachQuery.getVariablesSet()) : 1.0;
                    groupProbSum += prob / unigramProb;
                }

                doneVar.add(variableAssignment.toString(group));
                sdPairs.add(new StringDoublePair(variableAssignment.toString(group), groupProbSum));
            }

            Collections.sort(sdPairs, StringDoublePair.comparator(false));
            for (int i = 0; i < sdPairs.size(); i++) {
                queryVariableIndex.put(sdPairs.get(i).getStringElement(), i + 1);
            }

            for (int i = 0; i < variableAssignments.size(); i++) {
                String key = variableAssignments.get(i).toString(group);
                Integer rank = queryVariableIndex.get(key);

                String desc = groupIdx == 1 ? "" : variableAssignments.get(i).getDescription();
                desc += groupIdx + ":" + rank + " - ";
                variableAssignments.get(i).setDescription(desc);
                rankProd[i] += Math.log(rank);
            }
            groupIdx++;
        }

        for (int i = 0; i < variableAssignments.size(); i++) {
            variableAssignments.get(i).setScore(rankProd[i]);
        }

    }

}
